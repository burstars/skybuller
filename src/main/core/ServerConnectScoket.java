/*
 *  Licensed to the Apache Software Foundation (ASF) under one
 *  or more contributor license agreements.  See the NOTICE file
 *  distributed with this work for additional information
 *  regarding copyright ownership.  The ASF licenses this file
 *  to you under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */
package com.chess.core;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import com.chess.common.msg.processor.SystemDecoder;
import com.chess.common.msg.struct.system.LinkValidationMsg;
import com.chess.core.constant.NetConstant;
import com.chess.core.net.code.NetMsgEncoder;
import com.chess.core.net.mina2.Mina2ProtocolCodecFactory;
import com.chess.core.net.msg.IMsgRev;
import com.chess.core.net.msg.MsgBase;

public class ServerConnectScoket extends IoHandlerAdapter {

	private static Logger logger = LoggerFactory.getLogger(ServerConnectScoket.class);
	/**
	 * The connector
	 */
	private IoConnector connector = null;

	/**
	 * The session
	 */
	private IoSession session = null;
	//
	private String connectAddress = "";
	private int connectPort = 0;
	//
	public static final int IO_BUFF_INIT_SIZE = 512;
	protected IMsgRev msgProc = null;// 负责处理收到的消息

	// 上一次检查网络情况的毫秒数
	private long lastTimeLinkCheckMs = 0;

	//
	protected long msgSendNum = 0;
	protected long msgReceivedNum = 0;
	//
	private String checkKey = "";

	private String connectUniqueName = "";

	// serverAddress是ip加端口，类似192.168.1.200:12345

	// connectUniqueName这个名字必须在某个游戏服务器里面，名字是唯一的
	public ServerConnectScoket(String serverAddress, IMsgRev msg_Proc,
			String check_Key, String connect_UniqueName) {

		checkKey = check_Key;
		//
		String[] strs = serverAddress.split(":");
		if (strs.length != 2)
			return;
		//
		String ip = strs[0];
		String str_port = strs[1];

		msgProc = msg_Proc;
		connectAddress = ip;
		//
		connectUniqueName = connect_UniqueName;
		//
		if (connectAddress == null || connectAddress.length() < 1
				|| str_port == null || str_port.length() < 1)
			return;

		int port = Integer.parseInt(str_port);
		connectPort = port;
	}

	//
	public void linkCheck() {
		long currentms = System.currentTimeMillis();
		if (lastTimeLinkCheckMs == 0) {
			lastTimeLinkCheckMs = currentms;
			return;
		}

		if (currentms - lastTimeLinkCheckMs > 15000)// 弄个20秒检测一次,为了测试方便改成5秒
		{
			lastTimeLinkCheckMs = currentms;
			//
			if (session == null)// 如果没有链接，尝试重新链接
			{
				reconnect();
			}
		}
	}

	//
	public void closeLink() {
		if (session != null) {
			session.close();
		}
		//
		if (connector != null) {
			connector.dispose();
		}
	}

	//
	public void reconnect() {
		if (connectAddress == null || connectAddress.length() < 1
				|| connectPort == 0 || connectPort > 65535)
			return;

		String[] ipseg = connectAddress.split("\\.");
		int ipseg1 = Integer.parseInt(ipseg[0]);
		int ipseg2 = Integer.parseInt(ipseg[1]);
		int ipseg3 = Integer.parseInt(ipseg[2]);
		int ipseg4 = Integer.parseInt(ipseg[3]);
		//
		if (ipseg1 >= 0 && ipseg1 <= 255 && ipseg2 >= 0 && ipseg2 <= 255
				&& ipseg3 >= 0 && ipseg3 <= 255 && ipseg4 >= 0 && ipseg4 <= 255) {
			if (connector == null) {
				connector = new NioSocketConnector();
				connector.getSessionConfig().setReadBufferSize(
						NetConstant.MAX_MSG_SIZE);
				connector.setHandler(this);
				connector.getFilterChain().addLast(
						"codec",
						new ProtocolCodecFilter(new Mina2ProtocolCodecFactory(
								Charset.forName("UTF-8"))));
			}
			// SocketSessionConfig dcfg = (SocketSessionConfig)
			// connector.getSessionConfig();

			try {
				byte ipbytes[] = new byte[] { (byte) ipseg1, (byte) ipseg2,
						(byte) ipseg3, (byte) ipseg4 };
				InetAddress addr = InetAddress.getByAddress(ipbytes);
				ConnectFuture connFuture = connector
						.connect(new InetSocketAddress(addr, connectPort));

				connFuture.awaitUninterruptibly();

				session = connFuture.getSession();
				if (session != null) {
					connector.getSessionConfig()
							.setMinReadBufferSize(16 * 1024);
					connector.getSessionConfig().setReadBufferSize(
							NetConstant.MAX_MSG_SIZE);
					connector.getSessionConfig().setMaxReadBufferSize(
							512 * 1024);

					// 链接之后，立马发送一个确认链接的消息
					LinkValidationMsg msg = new LinkValidationMsg();
					msg.checkKey = this.checkKey;
					msg.linkName = this.connectUniqueName;
					this.send(msg);
					//
					//
					logger.debug("key=" + this.checkKey + ",name="
							+ this.connectUniqueName);
					// System.out.println("key=" + this.checkKey + ",name="+ this.connectUniqueName);
				}
			} catch (Exception e) {
				logger.error("reconnect error, address:" + connectAddress
						+ ",port=" + connectPort + ", will try later.");
			}
		}
	}

	//

	public boolean isValid() {
		if (session != null)
			return true;
		//
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void exceptionCaught(IoSession session, Throwable cause)
			throws Exception {
		cause.printStackTrace();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void messageReceived(IoSession session, Object message)
			throws Exception {

		IoBuffer buf = (IoBuffer) message;
		if (msgProc != null) {
			msgReceivedNum++;

			MsgBase msg = SystemDecoder.decode(buf);
			if (msg != null) {
				msgProc.msgReceived(msg, session);
			}
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		// System.out.println("messageSent");
	}

	//
	public void send(MsgBase msg) {
		IoBuffer buf = NetMsgEncoder.encode(msg);
		this.send(buf);
	}

	public void send(IoBuffer buf) {
		if (buf == null) {
			// logger.error("sendMsg, buf null");
			return;
		}
		//
		if (session == null) {
			// logger.error("sendMsg, session null");
			return;
		}
		//
		if (buf.limit() > NetConstant.MAX_MSG_SIZE) {
			logger.error("sendMsg failed, msg size=" + buf.limit()
					+ "> max msg size " + NetConstant.MAX_MSG_SIZE);
			return;
		}
		//
		msgSendNum++;
		// System.out.println("sendMsg len="+buf.limit());

		WriteFuture future = session.write(buf);
	}

	public Long getSessionID() {
		if (session != null)
			return session.getId();
		//
		return 0L;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void sessionClosed(IoSession session) throws Exception {
		// System.out.println("sessionClosed");

		// 如果断开，清理掉session引用
		this.session = null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void sessionCreated(IoSession session) throws Exception {
		// System.out.println("sessionCreated");
		//
		if (msgProc != null) {
			msgProc.connectInited(session);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void sessionIdle(IoSession session, IdleStatus status)
			throws Exception {
		// System.out.println("sessionIdle");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void sessionOpened(IoSession session) throws Exception {
		// System.out.println("sessionOpened");
	}

	public long getMsgSendNum() {
		return msgSendNum;
	}

	public long getMsgReceivedNum() {
		return msgReceivedNum;
	}

}
