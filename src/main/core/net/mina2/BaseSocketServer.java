package com.chess.core.net.mina2;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executors;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.executor.ExecutorFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import com.chess.common.DateService;
import com.chess.common.bean.User;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.struct.system.HeartBeatingMsg;
import com.chess.core.constant.NetConstant;
import com.chess.core.net.code.NetMsgEncoder;
import com.chess.core.net.mina2.Mina2ProtocolCodecFactory;
import com.chess.core.net.msg.IMsgFilter;
import com.chess.core.net.msg.IMsgRev;
import com.chess.core.net.msg.MsgBase;


/**
 * 游戏Socket服务 服务端与客户端数据传输(基于Apache mina)
 *
 * @author
 * @version 1.0
 */
public class BaseSocketServer {

    //private static final int PORT = 16762;

    /**
     * 最大进程数
     */
    private static final int maxProcessors = 8;

    //原先用final
    /**
     * cpu数 和 最大进程数 选择一个小的
     */
    private IoAcceptor acceptor = new NioSocketAcceptor(Runtime.getRuntime().availableProcessors() > maxProcessors ? maxProcessors : Runtime.getRuntime().availableProcessors());

    private static Logger logger = LoggerFactory.getLogger(BaseSocketServer.class);

    private long lastLinkCheckTime = 0;
    private long lastIdleCheckTime = 0;

    private int gameSocketPort = 0;
    private IMsgRev msgProc = null;
    private IMsgFilter msgFilterProc = null;
    //
    private String serverName = "";
    //
    protected long msgSendNum = 0;
    //
    protected boolean heart_beating_supported = true;
    //
    private GameSession gs_sh = null;
    //key为playerID或者在gs中是ParticipantID，value为这个玩家连接本game server的session引用
    private Map<String, IoSession> playerSessionMap = new HashMap<String, IoSession>();

    //上次更新客户端时间的时刻
    private long lastUpdateSystemTime = 0L;

    public BaseSocketServer(int port, String server_Name, IMsgRev in_msgProc) {
        gameSocketPort = port;
        //
        this.serverName = server_Name;
        //
        msgProc = in_msgProc;
        //
        initScoket();
    }

    public void initScoket() {

        gs_sh = new GameSession();
        gs_sh.setMsgProc(msgProc);

        //
        acceptor.setHandler(gs_sh);
        acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new Mina2ProtocolCodecFactory(Charset.forName("UTF-8"))));
        acceptor.getFilterChain().addLast("threadPool", new ExecutorFilter(Executors.newCachedThreadPool()));
        //acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new ObjectSerializationCodecFactory()));
        try {
            int port = gameSocketPort;
            acceptor.bind(new InetSocketAddress(port));
            //
            String server_ip = "";
            logger.info("Game Socket Server Started ip=" + server_ip + " port=" + port + " ...");
            //
        } catch (IOException e) {
            logger.error("init socket failed,port=" + gameSocketPort, e);
        }

    }

    public void shutdown() {
        acceptor.unbind();
        acceptor.dispose();

    }

    /**
     * 重启Scoket
     */
    public void resetSocket() {
        shutdown();

        acceptor = new NioSocketAcceptor(Runtime.getRuntime().availableProcessors() > maxProcessors ? maxProcessors : Runtime.getRuntime().availableProcessors());
        initScoket();

    }


    /**
     * 当前Scoket链接数
     */
    public int getManagedSessionCount() {
        return acceptor.getManagedSessionCount();
    }


    /**
     * 清除链接超时的链接
     */
    @SuppressWarnings("unused")
    public void handleClearFailLine() {

    }

    //
    public void addPlayerSession(String playerID, IoSession session) {
        if (playerID == null || session == null)
            return;

        playerSessionMap.put(playerID, session);
    }

    public void removePlayerSession(String playerID) {
        if (playerID == null)
            return;

        IoSession s = playerSessionMap.remove(playerID);
    }

    //
    //通过玩家id，或者participantid获取session
    public IoSession getPlayerSession(String playerID) {
        if (playerID == null)
            return null;
        IoSession s = playerSessionMap.get(playerID);
        return s;
    }

    public void removeSessionBySessionID(Long sessionID) {
        try {
            if (acceptor != null && acceptor.isActive()) {
                Map<Long, IoSession> map = acceptor.getManagedSessions();
                map.remove(sessionID);
            }
        } catch (Exception e) {
            logger.error("check link failed:", e);
        }
    }

    public IoSession getSessionBySessionID(Integer sessionID) {
        if (sessionID == null || sessionID == 0)
            return null;
        //
        IoSession session = null;
        try {
            if (acceptor != null && acceptor.isActive()) {
                Map<Long, IoSession> map = acceptor.getManagedSessions();

                session = map.get(sessionID.longValue());
            }
        } catch (Exception e) {
            logger.error("getSessionByServerID failed: sessionID=" + sessionID, e);
        }

        return session;
    }
    //


    //给所有链接都发送消息，这个尽量少用
    public void sendMsgToAllSession(MsgBase msg) {
        if (msg == null)
            return;

        try {
            if (acceptor != null && acceptor.isActive()) {

                for (Iterator<Map.Entry<Long, IoSession>> iter = acceptor.getManagedSessions().entrySet().iterator(); iter.hasNext(); ) {
                    Map.Entry<Long, IoSession> currentEntry = iter.next();
                    IoSession ioSession = currentEntry.getValue();
                    //
                    if (ioSession == null || (ioSession.isClosing() == true) || (ioSession.isConnected() == false))
                        continue;

                    IoBuffer buf = NetMsgEncoder.encode(msg);
                    ioSession.write(buf);
                }
            }
        } catch (Exception e) {
            logger.error("check link failed:", e);
        }

    }

    //给所有指定玩家类别发消息
    public void sendMsgToTypePlayer(MsgBase msg, int playerType) {
        if (msg == null)
            return;
        try {
            if (acceptor != null && acceptor.isActive()) {

                for (Iterator<Map.Entry<Long, IoSession>> iter = acceptor.getManagedSessions().entrySet().iterator(); iter.hasNext(); ) {
                    Map.Entry<Long, IoSession> currentEntry = iter.next();
                    IoSession ioSession = currentEntry.getValue();
                    //
                    if (ioSession == null || (ioSession.isClosing() == true) || (ioSession.isConnected() == false))
                        continue;

                    User pl = (User) ioSession.getAttribute(NetConstant.PLAYER_SESSION_KEY);
                    if (pl == null || pl.getPlayerType() != playerType)
                        continue;

                    IoBuffer buf = NetMsgEncoder.encode(msg);
                    ioSession.write(buf);
                }
            }
        } catch (Exception e) {
            logger.error("check link failed:", e);
        }

    }

    public void heartBeatingCheck_emtpy() {

    }

    //
    //将链接但不发消息的链接移除，主要用于入口服务器
    public void break_center_idle_session() {
        long nowTime = System.currentTimeMillis();
        if (nowTime - lastIdleCheckTime < 2000)//120秒检测一次
            return;
        lastIdleCheckTime = nowTime;
        //
        try {
            if (acceptor != null && acceptor.isActive()) {

                for (Iterator<Map.Entry<Long, IoSession>> iter = acceptor.getManagedSessions().entrySet().iterator(); iter.hasNext(); ) {
                    Map.Entry<Long, IoSession> currentEntry = iter.next();
                    IoSession ioSession = currentEntry.getValue();
                    //
                    if (ioSession == null || (ioSession.isClosing() == true) || (ioSession.isConnected() == false))
                        continue;
                    //
                    long createTime = ioSession.getCreationTime();
                    if (nowTime < (createTime + 200000)) {//创建链接后的200秒内，不进行检查
                        continue;
                    }

                    Long msgTime = (Long) ioSession.getAttribute(NetConstant.ENTRANCE_SERVER_LAST_MSG_TIME);
                    //
                    if (msgTime == null || (nowTime - msgTime) > 600000) {
                        // 超过600秒的链接,非法链接
                        ioSession.close();
                        logger.info("center server清除超时链接");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("break_center_idle_session failed:", e);
        }
    }

    //将不发消息的链接杀掉，主要用于补丁服务器
    public void breakIdleSession() {
        long nowTime = System.currentTimeMillis();
        if (nowTime - lastIdleCheckTime < 15000)//15秒检测一次
            return;
        lastIdleCheckTime = nowTime;
        //
        try {
            if (acceptor != null && acceptor.isActive()) {

                for (Iterator<Map.Entry<Long, IoSession>> iter = acceptor.getManagedSessions().entrySet().iterator(); iter.hasNext(); ) {
                    Map.Entry<Long, IoSession> currentEntry = iter.next();
                    IoSession ioSession = currentEntry.getValue();
                    //
                    if (ioSession == null || (ioSession.isClosing() == true) || (ioSession.isConnected() == false))
                        continue;
                    //
                    long createTime = ioSession.getCreationTime();
                    if (nowTime < (createTime + 20000)) {//创建链接后的10秒内，不进行检查，给对方以时间进行合法性验证
                        continue;
                    }

                    //看看上一次消息的时间
                    Long idleCheckTime = (Long) ioSession.getAttribute(NetConstant.PATCH_SERVER_LAST_MSG_TIME);
                    //
                    if (idleCheckTime == null || (nowTime - idleCheckTime) > 45000) {
                        // 超过45秒的链接,非法链接
                        ioSession.close();
                        logger.info("清除idle链接");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("idle link failed:", e);
        }
    }

    //
    public void heartBeatingCheck(boolean islay) {
        if (heart_beating_supported == false)
            return;
        //
        long nowTime = System.currentTimeMillis();
        //if(nowTime-lastLinkCheckTime<4000)//4秒检测一次
        if (nowTime - lastLinkCheckTime < 8000)//4秒检测一次
            return;

        lastLinkCheckTime = nowTime;

        //
        try {
            if (acceptor != null && acceptor.isActive()) {

                for (Iterator<Map.Entry<Long, IoSession>> iter = acceptor.getManagedSessions().entrySet().iterator(); iter.hasNext(); ) {
                    Map.Entry<Long, IoSession> currentEntry = iter.next();
                    IoSession ioSession = currentEntry.getValue();
                    //
                    if (ioSession == null || (ioSession.isClosing() == true)) {
                        continue;
                    }
                    //
                    long createTime = ioSession.getCreationTime();
                    if (islay) {
                        //if (nowTime < (createTime + 2000)) {//创建链接后的10秒内，不进行检查，给对方以时间进行合法性验证
                        if (nowTime < (createTime + 10000)) {//创建链接后的10秒内，不进行检查，给对方以时间进行合法性验证
                            continue;
                        }
                    }

                    //看看心跳返回包
                    Long ackTime = (Long) ioSession.getAttribute(NetConstant.HEART_BEATING_ACK_TIME);
                    //
                    if (ackTime == null) {
                        ackTime = new Long(nowTime);
                        ioSession.setAttribute(NetConstant.HEART_BEATING_ACK_TIME, ackTime);
                        continue;
                    }

                    if (nowTime > (ackTime + 60000)) {
                        //测试阶段，暂时不用这个，因为客户端可能在debug
                        // 超过60秒的链接,非法链接
                        User pl = (User) ioSession.getAttribute(NetConstant.PLAYER_SESSION_KEY);
                        if (pl != null) {
                            logger.info("清除==" + pl.getPlayerIndex() + "," + pl.getPlayerName());
                            pl.setOnline(false);
                            pl.setTwoVipOnline(false);
                        }
                        logger.info("heartBeatingCheck iosession close! inteval > 60000");
                        close_session(ioSession);

                    } else {
                        //链接，发送心跳包
                        HeartBeatingMsg msg = new HeartBeatingMsg();
                        IoBuffer buf = NetMsgEncoder.encode(msg);
                        ioSession.write(buf);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("check link failed:", e);
        }
    }
    //

    //
    public void closeSession(IoSession ioSession) {
        if (ioSession == null)
            return;
        //
        close_session(ioSession);
    }

    //
    private void close_session(IoSession ioSession) {

        String msg1 = "IP:" + ioSession.getRemoteAddress() + " createTime:" + ioSession.getCreationTime();
        ioSession.close();
        logger.info("清除非法链接:" + msg1);
    }


    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }


    public void send(IoSession session, MsgBase msg) {

        if (session == null || msg == null) {
            //logger.error("GameSocketServer sendMsg error----"+Integer.toHexString(msg.msgCMD));
            return;
        }
        //
        msgSendNum++;
        //
        IoBuffer buf = NetMsgEncoder.encode(msg);

        if (buf != null) {
            session.write(buf);
        }
    }


    //强制刷新客户端时间
    public void updateSystemTime() {
        Date date = DateService.getCurrentUtilDate();

    }

    public IMsgFilter getMsgFilterProc() {
        return msgFilterProc;
    }

    public void setMsgFilterProc(IMsgFilter msgFilterProc) {
        this.msgFilterProc = msgFilterProc;
        gs_sh.setMsgFilter(msgFilterProc);
    }

    public long getMsgSendNum() {
        return msgSendNum;
    }


}
