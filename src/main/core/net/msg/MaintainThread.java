package com.chess.core.net.msg;

import java.util.Date;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import com.chess.common.DateService;


/***
 * 游戏维护现成，如果服务器主消息处理线程卡死了，这个线程还在跑，可以将基本信息发到dbgate，这样客户端可以通过debug查看哪里出错了
 * 还可以加上重启主游戏线程之类的功能
 **/
public class MaintainThread extends Thread {
    private static Logger logger = LoggerFactory.getLogger(BaseMsgThread.class);

    //只有当游戏需要停服的时候这个才是false
    private boolean keep_runing = true;

    private BaseMsgThread target = null;

    private Long currentTimeMs = 0L;
    private Long oldLogTime = 0L;

    public MaintainThread() {
    }

    public void setMaintainTarget(BaseMsgThread t) {
        target = t;
    }


    private void maintain() {
        if (target != null) {
            Date date = DateService.getCurrentUtilDate();
            currentTimeMs = date.getTime();
            //2mins
            if (currentTimeMs - this.oldLogTime > 120000) {
                oldLogTime = currentTimeMs;
                target.mt();
                //logger.info("maintain");
            }
        }
    }

    //线程函数，主循环
    @Override
    public void run() {
        while (keep_runing) {
            try {
                maintain();
                Thread.sleep(500);
            } catch (Exception e) {
                logger.error("MaintainThread loop error", e);
            }
        }
    }
}
