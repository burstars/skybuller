package com.chess.core.net.msg;

import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;

import com.chess.common.DateService;
import com.chess.common.bean.User;
import com.chess.common.msg.struct.system.LinkBrokenMsg;
import com.chess.common.msg.struct.system.ServerStatusMsg;
import com.chess.core.constant.NetConstant;


/***
 * 游戏主循环，游戏里面所有需要定时处理或者，每帧处理的时间，都从这里发起
 **/
public abstract class BaseMsgThread extends Thread implements IMsgRev {
    private static Logger logger = LoggerFactory.getLogger(BaseMsgThread.class);

    //只有当游戏需要停服的时候这个才是false
    protected boolean keep_runing = true;

    //一个循环处理多少条消息
    private final int MAX_MSG_PER_LOOP = 400;
    //
    protected BaseMsgProcessor msgProc = null;


    private ConcurrentLinkedQueue<RevMsg> msgReceived = new ConcurrentLinkedQueue<RevMsg>();

    protected String localServerAddress = "";//类似这样的本地地址"192.168.1.109:16789"

    //系统统计
    protected ServerStatusMsg serverStatus = new ServerStatusMsg();

    private MaintainThread mtThread = null;

    public BaseMsgThread() {
        //创建一个维护线程
        mtThread = new MaintainThread();
        mtThread.setMaintainTarget(this);
        mtThread.start();
    }

    public void setLocalServerAddress(String add) {
        localServerAddress = add;
    }

    //
    private void pushMsg(RevMsg msg) {
        msgReceived.offer(msg);
    }

    //
    private RevMsg popMsg() {
        return msgReceived.poll();
    }
    //


    public void connectInited(IoSession session) {
        //logger.info( "connectInited");
    }

    //
    //链接建立，连接断开
    public void sessionOpened(IoSession session) {
        serverStatus.linkNum++;
    }

    public void sessionClosed(IoSession session) {
        serverStatus.linkNum--;
        User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);
        logger.info("-->Socket连接关闭：玩家ID【"+(pl!=null? pl.getPlayerIndex() : "null")+"】，玩家昵称【"+(pl!=null ? pl.getPlayerName():"")+"】");
        LinkBrokenMsg msg = new LinkBrokenMsg();
		this.msgReceived(msg, session);
    }

    //
    public void msgReceived(MsgBase msg, IoSession session) {
        if (msg == null || session == null)
            return;
        RevMsg recv_msg = new RevMsg();
        recv_msg.msg = msg;
        recv_msg.session = session;
        Date ct = DateService.getCurrentUtilDate();
        recv_msg.recvMs = ct.getTime();
        pushMsg(recv_msg);
    }

    private void processMsg() throws Exception {
        for (int i = 0; i < MAX_MSG_PER_LOOP; i++) {
            try {
                RevMsg msg = popMsg();
                if (msg == null) break;
                msgProc.processMsg(msg);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                continue;
            }
        }
        /*
        while(true) {
            try {
                ReceivedMsg msg = popMsg();
                if (msg == null) break;

                msgProc.processMsg(msg);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                continue;
            }
        }
        */
        /*
        if (msgProc == null || msgReceived.size() <= 0)
            return;
        //
        for (int i = 0; i < MAX_MSG_PER_LOOP; i++) {
            ReceivedMsg msg = popMsg();
            if (msg != null) {
                msgProc.processMsg(msg);
            }
        }
        */
    }

    //线程维护
    public abstract void mt() ;

    //
    public abstract void loop() ;


    //线程函数，主循环
    @Override
    public void run() {
        while (keep_runing) {
            try {
                serverStatus.loopCounter++;
                //
                this.loop();
                //
                this.processMsg();
                //
                Thread.sleep(1);
            } catch (Exception e) {
                logger.error("msg loop error", e);
                logger.error(e.getMessage(), e);
            }
        }
    }

    public ServerStatusMsg getServerStatus() {
        return serverStatus;
    }
    
	public MsgBase msgDeserialize(IoBuffer ioBuffer, IoSession session) {
		return this.msgProc.msgDeserialize(ioBuffer,session);
	}
}
