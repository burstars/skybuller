package com.chess.core.net.msg;

import java.net.SocketAddress;
import java.nio.ByteOrder;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
















import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.common.msg.processor.SystemDecoder;
import com.chess.common.msg.struct.system.HeartBeatingAckMsg;
import com.chess.common.msg.struct.system.HeartBeatingMsg;
import com.chess.common.msg.struct.system.LinkBrokenMsg;
import com.chess.common.msg.struct.system.LinkValidationMsg;
import com.chess.common.msg.struct.system.LinkValidationMsgAck;
import com.chess.core.constant.NetConstant;
import com.chess.core.net.code.NetMsgEncoder;


public abstract class BaseMsgProcessor {
    private static Logger logger = LoggerFactory.getLogger(BaseMsgProcessor.class);

    //校验合法性时需要的参数
    private String checkKey = "";
    //
    protected long msgSendNum = 0;

    //
    private void maintainMsgProc(MsgBase msg, IoSession session) {
        Long now = System.currentTimeMillis();
        int cmd = msg.msgCMD;

        if (cmd == MsgCmdConstant.HEART_BEATING) {
            HeartBeatingAckMsg h_ack = new HeartBeatingAckMsg();
            this.send(session, h_ack);//心跳包发回去
            
        } else if (cmd == MsgCmdConstant.HEART_BEATING_ACK) {
            //设置收到心跳包反馈的时间，如果长时间没有反馈，认为链接已经无效，服务器主动断开
            session.setAttribute(NetConstant.HEART_BEATING_ACK_TIME, now);
            // System.out.println("heart beating ack..."+now/1000);
        }
        //验证消息,验证不通过不返回任何消息
        else if (cmd == MsgCmdConstant.LINK_VALIDATION) {
            LinkValidationMsg lvMsg = (LinkValidationMsg) msg;
            if (lvMsg != null && lvMsg.checkKey.equals(this.checkKey)) {
                session.setAttribute(NetConstant.LINK_CHECK_RESULT, NetConstant.LINK_CHECK_OK);
                session.setAttribute(NetConstant.LINK_NAME, lvMsg.linkName);

                //
                LinkValidationMsgAck ackMsg = new LinkValidationMsgAck();
                ackMsg.linkName = lvMsg.linkName;
                this.send(session, ackMsg);
                //
                player_link_validation_passed(session);
            } else {
                session.setAttribute(NetConstant.LINK_CHECK_RESULT, NetConstant.LINK_CHECK_FAILED);
            }
        }
        //验证通过才会收到此消息，验证不通过不返回任何消息
        else if (cmd == MsgCmdConstant.LINK_VALIDATION_ACK) {
            LinkValidationMsgAck lnkack = (LinkValidationMsgAck) msg;
            this.link_validation_passed(session, lnkack.serverName, lnkack.linkName);
        } else if (cmd == MsgCmdConstant.LINK_BROKEN) {
            playerLinkBroken((LinkBrokenMsg) msg, session);
        } else {
            logger.error("maintainMsgProc error,unkonw msg,cmd=" + cmd);
        }
    }

    //
    protected void playerLinkBroken(LinkBrokenMsg msg, IoSession session) {
        logger.error("playerLinkBroken,id=" + session.getId());
    }

    //继承类会覆盖
    protected void link_validation_passed(IoSession session, String server_name, String linkName) {
        SocketAddress add = session.getRemoteAddress();
        logger.info("link_validation_passed");
    }

    //
    //继承类会覆盖
    protected void player_link_validation_passed(IoSession session) {
        SocketAddress add = session.getRemoteAddress();
        logger.info("player_link_validation_passed");
    }

    //
    protected void systemMsgProcess(MsgBase msg, IoSession session) {

        if (msg.msgCMD > MsgCmdConstant.GROUP_SYSTEM_MAINTAIN_START && msg.msgCMD < MsgCmdConstant.GROUP_SYSTEM_MAINTAIN_END) {
            maintainMsgProc(msg, session);
        } else {
            logger.error("systemMsgProcess error,unkonw msg,cmd=" + msg.msgCMD);
        }

    }

    //
    public void send(IoSession session, MsgBase msg) {
        if (session == null || msg == null) {
            //logger.error("sendMsg error");
            return;
        }
        msgSendNum++;
        //
        IoBuffer buf = NetMsgEncoder.encode(msg);

        if (buf != null) {
            session.write(buf);
        }
    }


    protected void gameMsgProcess(MsgBase msg, IoSession session) {
    	logger.debug("gameMsgProcess");//        System.out.println("gameMsgProcess");
    }

    protected void udpateMsgProcess(MsgBase msg, IoSession session) {
    	logger.debug("udpateMsgProcess");// System.out.println("udpateMsgProcess");
    }

    protected void entranceServerMsgProcess(MsgBase msg, IoSession session) {
    	logger.debug("entranceServerMsgProcess");// System.out.println("entranceServerMsgProcess");
    }

    //
    //真正的每条消息进行解析处理，在这里
    public void processMsg(RevMsg recvMsg) throws Exception {
        if (recvMsg == null || recvMsg.msg == null || recvMsg.session == null)
            return;

        // a是系统消息  b是更新消息  c是游戏逻辑消息  d是入口消息 
        int cmd = recvMsg.msg.msgCMD;
        if (cmd >= MsgCmdConstant.GROUP_SYSTEM_MAINTAIN_START && cmd < MsgCmdConstant.GROUP_SYSTEM_MAINTAIN_END) {
            systemMsgProcess(recvMsg.msg, recvMsg.session);
        } else if (cmd > MsgCmdConstant.GROUP_PATCH_SERVER_START && cmd < MsgCmdConstant.GROUP_PATCH_SERVER_END) {
            udpateMsgProcess(recvMsg.msg, recvMsg.session);
        } else if (cmd > MsgCmdConstant.GROUP_GAME_START && cmd < MsgCmdConstant.GROUP_GAME_END) {
            gameMsgProcess(recvMsg.msg, recvMsg.session);
        } else if (cmd > MsgCmdConstant.ENTRANCE_SERVER_START && cmd < MsgCmdConstant.ENTRANCE_SERVER_END) {
            entranceServerMsgProcess(recvMsg.msg, recvMsg.session);
        } else {
            logger.error("unkown message cmd:" + cmd);
        }

    }


    public String getCheckKey() {
        return checkKey;
    }

    public void setCheckKey(String checkKey) {
        this.checkKey = checkKey;
    }

	public  MsgBase msgDeserialize(IoBuffer buf, IoSession session){
//	        return this.msgDeserialize(buf,session,cmd);
		return SystemDecoder.decode(buf);
	}
	
}