package com.chess.core.util;

import java.util.Date;

import org.apache.commons.beanutils.Converter;

public class DateConvert implements Converter {  
	  
    public Object convert(Class arg0, Object arg1) {  
        if(arg1 == null){  
            return null;  
        }    
        
        if(arg1 instanceof Date){
        	Date d1 = (Date) arg1;
        	return new Date(d1.getTime());
        }
        
        if(arg1 instanceof java.sql.Date){
        	java.sql.Date d2 = (java.sql.Date) arg1;
        	return new java.sql.Date(d2.getTime());
        }
        return null;
//        try{  
//            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
//            return df.parse(p.trim());  
//        }  
//        catch(Exception e){  
//            try {  
//                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");  
//                return df.parse(p.trim());  
//            } catch (ParseException ex) {  
//                return null;  
//            }  
//        }  
         
    }  
  
} 
