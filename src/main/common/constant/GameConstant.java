package com.chess.common.constant;

import java.nio.ByteOrder;

public class GameConstant {
	public static final ByteOrder GAME_ENCODER_ENDIAN = ByteOrder.LITTLE_ENDIAN;// ByteOrder.BIG_ENDIAN

	// 游戏游戏压缩标准
	public static final int COMPRESSED_MSG_FLAG = 0x10000000;

	/** 客户端请求刷新玩家数据* */
	public static final int OPT_REQUEST_UPDATE_PALYER_DATA = 1002;

	/** 服务器通知客户端，桌子上坐上一个新玩家* */
	public static final int OPT_TABLE_ADD_NEW_PLAYER = 1004;
	/** 服务器通知客户端，桌子上有玩家离开* */
	public static final int OPT_PLAYER_LEFT_TABLE = 1005;
	/** 客户端通知服务器，购买物品* */
	public static final int OPT_BUY_ITEM = 1007;
	/** 客户端通知服务器，使用道具* */
	public static final int OPT_USE_ITEM = 1008;
	/** 客户端通知服务器，更换头像* */
	public static final int OPT_CHANGE_HEAD = 1009;

	/** 客户端通知服务器，游戏结束，玩家继续游戏* */
	public static final int OPT_CONTINUE_GAME = 1010;
	/** 客户端通知服务器，游戏结束，玩家返回大厅* */
	public static final int OPT_BACK_TO_LOBBY = 1011;
	
	// 发送桌上玩家状态到前端
	public static final int OPT_TABLE_PLAYERS_STATE = 1012;

	/** 客户端通知服务器，修改用户昵称信息 */
	public static final int OPT_CHANGEPLAYERNAME = 1015;

	/** 客户端通知服务器，死了重生 */
	public static final int OPT_DEAD_ALIVE = 1016;

	/** 客户端请求添加好友 */
	public static final int OPT_ADDFRIEND = 1020;

	/** 客户端请求删除好友 */
	public static final int OPT_DELFRIEND = 1021;

	/** 客户端请求返回给被删除好友 */
	public static final int OPT_BDELFRIEND = 1022;

	/** 玩家查询通过玩家索引查询好友 */
	public static final int OPT_SEARCHFRIENDBYINDEX = 1023;

	/** 玩家查询通过名称查询好友 */
	public static final int OPT_SEARCHFRIENDBYNAME = 1024;

	/** 客户端请求修改密码 */
	public static final int OPT_UPDATE_PASSWORD = 1025;

	/** 客户端通知服务器，修改用户头像和性别信息 */
	public static final int OPT_CHANGEPLAYERHEADIMGSEX = 1026;

	/** 客户端通知服务器，修改用户加好友设置 */
	public static final int OPT_CHANGEPLAYERCANFRIEND_FLAG = 1027;

	public static final int OPT_GOT_GOLD_AUTO_SAVE = 1028; // 系统救济，赠送金币

	public static final int OPT_SET_TUOGUAN = 1029; // 设置托管状态
	public static final int OPT_ROOM_DISMISS = 1030; // 房主离开，房间解散

	/** 客户端通知服务器，修改用户账户信息 */
	public static final int OPT_CHANGEPLAYERACCOUNT = 1031;

	/** 玩家刷新好友信息 */
	public static final int OPT_REFLESHFRIENDBYNAME = 1032;

	/** 补全帐号和密码 */
	public static final int OPERATION_COMPLETE_ACCOUNT_AND_PASSWORD = 1033;

	/** 房主申请解散房间 */
	public static final int OPERATION_APPLY_CLOSE_VIP_ROOM = 1034;

	/** 同意好友验证消息 */
	public static final int OPERATON_AGREE_FRIEND_APPLY_RESULT = 1035;

	/** 拒绝好友验证消息 */
	public static final int OPERATON_REJECT_FRIEND_APPLY_RESULT = 1036;

	/** 绑定手机号码 */
	public static final int OPERATION_COMPLETE_PHONE_NUMBER = 1037;

	/** 客户端上传位置信息 */
	public static final int OPERATION_UPLOAD_CITY_NAME = 1038;

	// 解散房间
	public static final int OPERATION_PLAYER_APPLY_CLOSE_VIP_ROOM = 1039;
	public static final int OPERATION_CLOSE_VIP_ROOM_FEEDBACK = 1040;
	
	/**成员管理 */
	public static final int CLUB_MEMGER_OPT_ADD = 1041;
	public static final int CLUB_MEMGER_OPT_DELETE = 1042;
	public static final int CLUB_MEMGER_OPT_UPDATE_STATE = 1043;
	public static final int CLUB_MEMGER_OPT_UPDATE_REMARK= 1044;
	public static final int CLUB_MEMGER_NOTICE_ADD_MANAGE = 1045;
	public static final int CLUB_MEMGER_NOTICE_ADD_WAIT = 1046;
	public static final int CLUB_MEMGER_OPT_GET_LIST = 1047;
	public static final int CLUB_MEMBER_OPT_IGNORE = 1048;
	public static final int CLUB_MEMGER_NOTICE_OPT_SUC = 1049;
	public static final int CLUB_MEMGER_NOTICE_IGNORE_ADD = 1050;
	public static final int CLUB_MEMGER_NOTICE_REMOVE = 1051;
	public static final int CLUB_MEMGER_NOTICE_ADD_AGREE = 1052;
	public static final int CLUB_MEMGER_NOTICE_REMARK = 1053;
	public static final int CLUB_MEMBER_OPT_QUIT = 1054;
	public static final int CLUB_MEMBER_NOTICE_QUIT = 1056;
	
	/**申请加入俱乐部*/
	public static final int CLUB_MEMGER_OPT_APPLY = 1055;
	
	public static final int FORCE_EXIT_GAME_FAILED = 1998;
	public static final int FORCE_EXIT_GAME_SUCCESS = 1999;
	
	/**固定牌桌的KEY*/
	public static final String STATIC_CLUB_TABLE = "staticClubTable";
	/**固定牌桌，已经处理玩牌桌*/
	public static final String STATIC_CLUB_PLAYING_TABLE = "staticClubPlayingTable";
	/**用户创建的私密牌桌*/
	public static final String USER_CLUB_TABLE = "userClubTable";
	
	/** 状态非法 */
	public static final int STATE_INVALID = 0;

	/** 桌子状态非法 */
	public static final int TABLE_STATE_INVALID = 0;

	/** 玩家进入桌子，等待中* */
	public static final int USER_GAME_STATE_IN_TABLE_READY = 1;
	/** 玩家进入桌子，游戏中* */
	public static final int USER_GAME_STATE_IN_TABLE_PLAYING = 2;
	/** 玩家进入桌子，已经离开，暂停中* */
	public static final int USER_GAME_STATE_IN_TABLE_PAUSED = 3;
	/** 玩家进入桌子，牌局结束，等待玩家点继续或者离开* */
	public static final int USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE = 4;

	public static final int USER_GAME_OVERTIME_STATE_IN_TABLE_NOWAITING_TO_OVERTIMECHU = 0;
	/** 玩家超时，自动出牌 */
	public static final int USER_GAME_OVERTIME_STATE_IN_TABLE_WAITING_TO_OVERTIMECHU = 1;

	// //////////////////////////////////////////////////////////////////////
	/** 等待玩家进入桌子* */
	public static final int TABLE_STATE_WAITING_PLAYER = 1;
	/** 玩家已经满，准备开始* */
	public static final int TABLE_STATE_READY_GO = 2;

	/** 客户端收到起手牌，播放动画，给客户端一定时间* */
	public static final int TABLE_WAITING_CLIENT_SHOW_INIT_CARDS = 3;

	/** 玩家玩牌中* */
	public static final int TABLE_STATE_PLAYING = 4;
	/** 游戏结束，等待客户端显示game over界面* */
	public static final int TABLE_STATE_SHOW_GAME_OVER_SCREEN = 5;
	/** 游戏结束，等待玩家选择是否继续玩一把* */
	public static final int TABLE_STATE_WAITING_PALYER_TO_CONTINUE = 6;

	/** 有玩家离开桌子，暂停游戏 */
	public static final int TABLE_STATE_PAUSED = 7;

	/** vip桌子圈数用光，等房主续卡 */
	public static final int TABLE_STATE_WAITING_VIP_EXTEND_CARD = 8;

	/** 玩家吃碰之类的操作，服务器等客户端播个动画 */
	public static final int TABLE_SUB_STATE_IDLE = 0;// 无任何操作
	public static final int TABLE_SUB_STATE_PLAYING_CHI_PENG_ANIMATION = 1;// 客户端在播吃碰牌动画
	public static final int TABLE_SUB_STATE_PLAYING_HU_ANIMATION = 2;// 客户端在播胡牌动画
	public static final int TABLE_SUB_STATE_PLAYING_CHU_ANIMATION = 3;// 客户端在播出牌动画
	public static final int TABLE_SUB_STATE_PLAYING_TING_ANIMATION = 4;// 客户端在播听牌动画
	public static final int TABLE_SUB_STATE_AUTO_GANG = 5; // 自动杠牌操作 add by
															//  2016.8.17
	public static final int TABLE_SUB_STATE_WAIT_PLAYER_OP_HU = 6; // 等待玩家点击胡牌的过程
	public static final int TABLE_SUB_STATE_WAIT_ACTION_ANIMATION = 7;// 等待事件动画

	public static final int TABLE_SUB_STATE_WAIT_APPLY_CLOSE_ROOM = 9;// 等待选择同意解散房间

	// //////////////////////////////////////////////////////////////////////
	public static final int MJ_OPT_OFFLINE = 0x100;// 断线
	public static final int MJ_OPT_ONLINE = 0x200;// 断线后又上线

	/** VIP场 4人 */
	public static final int ROOM_TYPE_VIP = 4;
	/** 单机场 */
	public static final int ROOM_TYPE_SINGLE = 5;
	/** VIP 2人 */
	// kn add for 2人
	public static final int ROOM_TYPE_COUPLE = 6;
	/** VIP 3人 */
	public static final int ROOM_TYPE_THREE = 7;

	// /////////////////////////////////////////////////////////////////////////////
	// 加好友的几种类型
	/** 已经是好友了 */
	public static final int FRIEND_BOTH_IS_FRIEND = 0;
	/** 等待对方同意成为好友 */
	public static final int FRIEND_WAIT_PLAYER_AGREE = 1;
	/** 别人请求加我为好友 */
	public static final int FRIEND_RECEIVE_APPLY = 2;
	/** 黑名单 */
	public static final int FRIEND_IN_BLACK_LIST = 3;

	// /////////////////////////////////////////////////////////////////////////////
	// 获取手机验证码场景
	/** 绑定手机号码 */
	public static final int GET_CODE_COMPLETE_PHONE_NUMBER = 1;
	/** 找回密码 */
	public static final int GET_CODE_FIND_PASS_WORD = 2;
	/** 注册验证 **/
	public static final int GET_CODE_REG_VALIDATE = 3;

	public static final String IPA_PACKAGE_PREFIX = "www.baidu.www_";

	public static final int OPERATION_PLAYER_LEFT_TABLE = 1050;
	public static final int OPERATION_RE_NOTIFY_CURRENT_OPERATION_PLAYER = 1051;
	public static final int OPERATION_VIP_TABLE_END = 1052;

	// 入口服务器操作返回码
	public static final int ENTRANCE_SERVER_OPERATION_RESULT_FAILED = 0;

	public static final String LOG_TAG = "5";

	public static boolean ZHI_DUI_SUPPORT = false;
	public static boolean TWO_PERSON_SUPPORT = false;

	//ddz
	public static final int SPECIAL_CARD_XIAOWANG = 0x4E;
	public static final int SPECIAL_CARD_DAWANG = 0x4F;
	public static final int DDZ_COLOR_STEP = 16;
	public static final int DDZ_CARDTYPE_MASK = 0xF0;	//牌型掩码
	
	public static final int CARD_NUM_XIAOWANG = 16;
	public static final int CARD_NUM_DAWANG = 17;
	
	public static final int DDZ_COLOR_SHIFTS = 4;// 花色部分的移位，花色，【0，1，2】

	public static final int DDZ_RESULT_WIN = 0x100000;// 赢
	public static final int DDZ_RESULT_LOSE = 0x200000;// 输
	public static final int DDZ_OPT_OFFLINE = 0x100;// 断线
	public static final int DDZ_OPT_ONLINE = 0x200;// 断线后又上线
	public static final int DDZ_OPT_GAME_OVER_CHANGE_TABLE = 0x1000;// 牌局结束，玩家选择换桌
	public static final int DDZ_OPT_GAME_OVER_CONTINUE = 0x2000;// 牌局结束，玩家选择继续开始游戏

	public static final int DDZ_OPT_SEARCH_VIP_ROOM = 0x4000;
	
	public static final int DDZ_OPT_OVERTIME_AUTO_CALL_SCORE = 0x40000;// 超时自动叫分
	public static final int DDZ_OPT_OVERTIME_AUTO_CHU = 0x80000;// 超时自动出牌
	public static final int DDZ_OPT_EXTEND_CARD_REMIND = 0x100000;// 提醒房主续卡
	public static final int DDZ_OPT_EXTEND_CARD_SUCCESSFULLY = 0x200000;// 提醒房主续卡成功
	public static final int DDZ_OPT_WAITING_OR_CLOSE_VIP = 0x400000;// VIP房间有人逃跑，是否继续等待
	public static final int DDZ_OPT_NO_START_CLOSE_VIP = 0x800000;// VIP房间超时未开始游戏，房间结束
	public static final int DDZ_OPT_OVERTIME_AUTO_DOUBLE = 0x1000000;// 超时自动加倍
	public static final int DDZ_OPT_EXTEND_CARD_FAILED = 0x8000000;// 提醒房主续卡失败	
	public static final int DDZ_OPT_EXTEND_CARD_NUMBER_FAILED = 0x8800000;// 房卡不够
	
	// 异常提示
	public static final int DDZ_OPT_ERROR_TIP = 0x26000;

	public static final int DDZ_OPT_TIP = 0x20000;
	
	//----------------------扑克新增常量----------------------------------------------
	public static final int CARDTYPE_MASK = 0xF0;	//通用牌型掩码
	public static final int DDZ_OPT_TIP_WIN = 0x20000000;	//玩家手牌为0
	public static final int DDZ_OPT_TIP_HIDE_YOU = 0x40000000; //正式开始游戏，隐藏玩家上局头游二游三游末游
	
	public static final int WELFARE_PART_NO_XIAOER = 2;
	public static final int WELFARE_PART_NO_DAER = 3;
	public static final int WELFARE_PART_NO_OTHER = 1;
	
	/**代理开房记录：房间状态 2017-9-26**/
	public static final int PROXY_NO_START_GAME = 0;	//未开局
	public static final int PROXY_START_GAME = 1;	//开始游戏
	public static final int PROXY_END_GAME = 2;	//游戏正常结束(已开始游戏)
	public static final int PROXY_OUTTIME_END_GAME = 3;	//房间未开局，超时解散
	public static final int PROXY_FORCE_END_GAME = 4;	//未开始游戏，代理强制解散
	public static final int PROXY_PLAYER_END_GAME = 5;	//未开局玩家解散 2017-9-26
	public static final int PROXY_SERVER_END_GAME = 6;	//游戏未开始，服务器重启解散房间
	public static final int PROXY_FORCE_END_GAME_SERVER = 7;	//群主强制解散未扣卡代开房间
	public static final int PROXY_PLAYER_END_GAME_START = 8;	//开局后玩家申请解散
	public static final int PROXY_OUTTIME_END_GAME_START = 9;	//已开局超时解散
	public static final int PROXY_FORCE_END_GAME_SERVER_START = 10;	//群主强制解散已扣卡代开房间
	public static final int PROXY_SERVER_END_GAME_START = 11;	//游戏已开始，服务器重启解散房间

	
	//---------------------------牛牛牌型常量
	
	//电子庄牛牛相关操作常量
	public static final int NNDZZ_MAX_PLAYER_COUNT = 5;
	/** 通知平倍下注操作 */
	public static final int NNDZZ_OPT_PINGBEI = 0x10000; 
	/** 通知翻倍下注操作 */
	public static final int NNDZZ_OPT_FANBEI = 0x400;
	/** 执行“亮牛”结果 */
	public static final int NNDZZ_OPT_LIANGNIU = 0x40000;
	/** 玩家选择是否当庄 */
	public static final int NNDZZ_OPT_ZHUANG = 0x2000000;
	
	/** 赢牌 */
	public static final int NNDZZ_OPT_WIN = 0x40;// 赢牌
	
	/** 比牌结果，赢了 */
	public static final int NNDZZ_COMPARE_WIN = 1;
	/** 比牌结果，输了 */
	public static final int NNDZZ_COMPARE_LOSE = 2; 
	
	
	//---------------------------电子庄牛牛牌型常量
	/** 牌型错误 */
	public static final int NNDZZ_CARD_TYPE_ERROR = 0;
	/** 没牛 */
	public static final int NNDZZ_CARD_TYPE_NO_NIU = 1;
	/** 有牛 */
	public static final int NNDZZ_CARD_TYPE_HAS_NIU = 2;
	/** 牛牛 */
	public static final int NNDZZ_CARD_TYPE_NIU_NIU = 3;
	/** 五公牛 */
	public static final int NNDZZ_CARD_TYPE_FIVE_GONG_NIU = 4;
	/** 炸弹牛:有四张一样的牌 */
	public static final int NNDZZ_CARD_TYPE_BOMB_NIU = 6;
	
	// 下注
	public static final int TABLE_SUB_STATE_XIAZHU = 1;
	// 发牌
	public static final int TABLE_SUB_STATE_SEND_CARD = 2;
	// 亮牛
	public static final int TABLE_SUB_STATE_LIANGNIU = 3;
	// 选庄
	public static final int TABLE_SUB_STATE_ZHUANG = 4;
	
	
	
}
