package com.chess.common.constant;

public class ConfigConstant {
	
	//ios
	public static final int DEVICE_TYPE_IOS =1;
	//安卓
	public static final int DEVICE_TYPE_ANDROID =2;
	
	// //////////////////////////////参数ID：1000+///////////////////////////////////////////
	/** 默认在线人数控制值 - 1001 */
	public static final int DEFAULT_ONLINE_COUNT = 1001;

	/** 单服务器最大注册人数 - 1002 */
	public static final int SINGLE_SERVER_MAX_COUNT = 1002;

	/** 新手金币数量 - 1010 */
	public static final int COIN_FOR_NEW_PLAYER = 1010;

	/** 下个新玩家注册的索引号 - 1012 */
	public static final int INDEX_FOR_NEXT_REG_PLAYER = 1012;
	
	/**机器人数量限制*/
	public static final int ROTBOT_LIMITNUM = 1013;
	/**等待多长时间机器人时间机器人进来*/
	public static final int ROTBOT_TIME_ENTER = 1014;

	// //////////////////////////////参数ID：2000+///////////////////////////////////////////
	/** 初级场 - 2001 */
	public static final int ROOM_OF_PRIMARY = 2001;

	/** 中级场 - 2002 */
	public static final int ROOM_OF_INTERMEDIATE = 2002;

	/** 高级场 - 2003 */
	public static final int ROOM_OF_ADVANCED = 2003;

	/** vip场 - 2004 */
	public static final int ROOM_OF_VIP = 2004;

	/** 单机场 - 2005 */
	public static final int ROOM_OF_SINGLE = 2005;

	/** vip场2 - 2006 */
	public static final int ROOM_OF_VIP2 = 2006;

	/** 高级场2 - 2007 */
	public static final int ROOM_OF_ADVANCED2 = 2007;

	// //////////////////////////////参数ID：3000+///////////////////////////////////////////
	/** 每日救济的最大次数 - 3333 */
	public static final int MAX_SAVE_TIME = 3333;

	/** 一次救济赠送多少金币 - 3334 */
	public static final int HANDSEL_COIN_EACH_TIME = 3334;

	/** 签到奖励金币数 - 3003 */
	public static final int COIN_FOR_SIGN = 3003;

	// //////////////////////////////参数ID：4000+///////////////////////////////////////////


	/** 一卡通商户ID - 4007 */
	public static final int YI_KA_TONG_PARTNER_ID = 4007;

	/** 一卡通平台地址 - 4008 */
	public static final int YI_KA_TONG_PLATFORM = 4008;

	/** 一卡通密钥 - 4009 */
	public static final int YI_KA_TONG_KEY = 4009;
	
	/**支付开关*/
	public static final int PAY_SWITCH = 4020;
	
	/** 互亿无限短信提交地址 - 4040 */
	public static final int MSG_VERTIFY_SUBMIT_ADDRESS = 4040;
	
	/** 互亿无限短信验证帐号 - 4041 */
	public static final int MSG_VERTIFY_ACCOUNT = 4041;

	/** 互亿无限短信验证密码 - 4042 */
	public static final int MSG_VERTIFY_PWD = 4042;

	
	
	// //////////////////////////////参数ID：5000+///////////////////////////////////////////
	/** Vip场有玩家中途离场，房间存活时间(s) - 5001 */
	public static final int VIP_PAUSED_EXIST_TIME = 5001;

	/** 操作超时时间(s) - 5002 */
	public static final int OPERATION_OVER_TIME = 5002;

	/** VIP场有玩家中途离场，玩家选择等待时间(s) - 5005 */
	public static final int VIP_PAUSED_CHOOSE_WAIT_TIME = 5005;

	/** vip圈数结束，继续等待时间(s) - 5006 */
	public static final int VIP_GAME_OVER_WAITING_START_GAME_TIME = 5006;

	/** vip牌局结束,进入下一把的时间(s) - 5007 */
	public static final int VIP_GAME_OVER_ENTER_NEXT_TURN = 5007;

	/** 创建Vip房间未开始游戏，房间的存活时间(s) - 5008 */
	public static final int VIP_ROOM_CREATE_WAIT_START = 5008;
	
	/**VIP房不续卡结果显示时间 */
	public static final int VIP_GAME_NOT_EXTEND_CARD_WAIT_TIME = 5009;
	
	/**VIP房玩家请求解散房间，超时时间 */
	public static final int VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME = 5010;

	// //////////////////////////////参数ID：8000+///////////////////////////////////////////
	//版本信息 安卓
	public static final int UPATCHE_VERSION_ANDROID = 8001;
	public static final int UPATCHE_VERSION_IOS = 8002;

	//绑定手机号开关
	public static final int NEED_BIND_PHONE_NUMBER = 8010;

	public static final int CHU_DELAY_BEFORE_TING = 8011;
	public static final int CHU_DELAY_AFTER_TING = 8012;

	public static final int PRIMARY_PLAYER_NUM = 8013;
	public static final int INTERMEDIATE_PLAYER_NUM = 8014;
	public static final int ADVANCED_PLAYER_NUM = 8015;

	public static final int SUPPORT_ZHIDUI = 8016;

	public static final int SHOW_NOTICE_ONCREATEVIP = 8017;

	public static final int SHOW_NOTICE_ONLOGIN = 8018;

	public static final int BAO_AT_LAST_OFCARDS = 8019;

	//是否随机发好牌
	public static final int GOOD_CARDS = 8050;
	//二人麻将是否随机发好牌
	public static final int COUPLE_GOOD_CARDS = 8052;
	//二人麻将支持
	public static final int TWO_PEOPLE_SUPPORT = 8051;
	//二人麻将红中个数
	public static final int COUPLE_HONGZHONG_COUNT = 8053;
	//每日分享次数
	public static final int EVERYDAY_SHARE_NUM=9000;
	/**首次登录赠卡个数*/
	public static final int LOGIN_FIRST_SEND_FANGKA_NUM = 9090;
	//是否显示消息
	public static final int IS_ALLOW_SHOW_MSG = 9110;
	//是否允许续卡
	public static final int IS_ALLOW_XUKA = 9100;
	//是否启用积分商城
	public static final int IS_START_CREDITS_EXCHANGE = 9120;
	/** 主产品id */
	public static final int MAIN_PRODUCT_ID = 9200;
	/** 关闭产品 */
	public static final int CLOSED_PRODUCTS = 9201;
	/** 俱乐部上限 */
	public static final int CLUB_COUNT = 9301;
}
