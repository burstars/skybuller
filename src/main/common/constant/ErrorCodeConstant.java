package com.chess.common.constant;

public class ErrorCodeConstant {

	public static final int CMD_EXE_OK = 0;// 命令执行成功

	/** 卡号错误或不存在 -100 */
	public static final int YIKATONG_CARDNO_ERROR = -100;
	/** 密码错误 -200 */
	public static final int YIKATONG_CARDPWD_ERROR = -200;
	/** 卡未生效 -300 */
	public static final int YIKATONG_CARD_DISABLE = -300;
	/** 卡余额不足付费 -400 */
	public static final int YIKATONG_CARD_MONEY_NOT_ENOUGH = -400;
	/** 接口不支持此卡付费 -500 */
	public static final int YIKATONG_CARD_NOT_SUPPORTED = -500;
	/** 商家付费流水号重复 -600 */
	public static final int YIKATONG_OUTTRADENO_REPEAT = -600;
	/** 参数错误 -700 */
	public static final int YIKATONG_PARA_ERROR = -700;
	/** 未提供此商品ID 付费功能 -800 */
	public static final int YIKATONG_PARTNER_ID_ERROR = -800;
	/** md5 校验码验证错误 -900 */
	public static final int YIKATONG_MD5_ERROR = -900;

	public static final int CMD_EXE_FAILED = 1000;// 命令执行失败

	public static final int WRONG_PASSWORD = 1001;// 密码错误
	public static final int USER_NOT_FOUND = 1002;// 玩家未找到
	public static final int USER_ALREADY_EXIST = 1004;// 同名玩家已经存在


	public static final int GOLD_NOT_ENOUGH = 1006;// 金币不够


	public static final int ACCOUNT_ALREADY_EXIST = 1012;// 帐号重复
	public static final int MACHINE_CODE_ALREADY_EXIST = 1013;// 机器码重复
	public static final int SERVER_IS_BUSY = 1014;// 服务器忙碌
	public static final int SERVER_IS_FULL = 1015;// 服务器注册量已满

	public static final int FANGKIA_NOT_FOUND = 1100;// 房卡不足
	public static final int GOLD_LOW_THAN_MIN_LIMIT = 1101;// 金币低于下限
	public static final int GOLD_HIGH_THAN_MAX_LIMIT = 1102;// 金币超过上限

	public static final int CAN_ENTER_VIP_ROOM = 1103;// 可以进入VIP房间

	public static final int VIP_TABLE_IS_FULL=1104;	//vip桌子已经满座了
	
	public static final int VIP_TABLE_IS_GAME_OVER = 1105;	//VIP桌子已经结束了
	
	public static final int ITEM_NOT_FOUND = 1107;	//道具不足
	//代开优化 cc modify 2017-9-26
	public static final int FANGKIA_LOCKED = 1108;// 代开房间，房卡锁定中
}