package com.chess.common.constant;


/**所有有关日志类型的参数也放这里**/
public class LogConstant 
{
	
	/**主操作类型 获得  消耗  operation_type*/
	public static final int OPERATION_TYPE_ADD_GOLD = 1;//增加金币
	public static final int OPERATION_TYPE_SUB_GOLD = 2;//减少金币
	public static final int OPERATION_TYPE_ADD_PRO = 5;//增加功能道具
	public static final int OPERATION_TYPE_SUB_PRO = 6;//消耗功能道具
	public static final int OPERATION_TYPE_ADD_PACKS = 7;//增加大礼包
	public static final int OPERATION_TYPE_ADD_DIAMOND = 9;//增加钻石
	public static final int OPERATION_TYPE_ADD_LIFE=11;//添加生命力
	
	public static final int OPERATION_TYPE_USE_RMB=13;//使用人民币
	
	public static final int OPERATION_TYPE_APPLY_CLOSE_VIP_ROOM = 14;  //玩家申请解散房间
	public static final int OPERATION_TYPE_ADD_DRAWCARD = 15;  //增加抽奖卡
	
	
	public static final int OPERATION_BUY_BIG_GIFT=4;//购买大礼包，附带赠送
	public static final int OPERATION_CONTINUE_LOGIN=5;//连续登录获得
	public static final int OPERATION_DEAD_ALIVE=8;//游戏内快速复活
	public static final int OPERATION_PAY =10;//充值行为
	public static final int OPERATION_DIAMOND_SEND=11;//钻石，每日赠送

	public static final int OPERATION_CHANGE_ITEM_BY_DEVICE=20;	//安卓和IOS道具相互转换
	
	public static final int OPERATION_TYPE_SUB_GOLD_GAME_LOSE=100;//打牌输金币
	public static final int OPERATION_TYPE_ADD_GOLD_GAME_WIN=200;//打牌赢金币
	public static final int OPERATION_TYPE_ADD_GOLD_SAVE=300;//救济
	public static final int OPERATION_SUBTYPE_WELFARE = 301; //转盘所得
	
	public static final int OPERATION_TYPE_SUB_PRO_CREATE_2_VIP_TABLE=401;//2人VIP房间消耗道具
	public static final int OPERATION_TYPE_SUB_PRO_CREATE_4_VIP_TABLE=402;//4人VIP房间消耗道具

	public static final int OPERATION_TYPE_SUB_GOLD_TAKE_TO_GAME_TABLE=101;//金币带到桌子上
	public static final int OPERATION_TYPE_ADD_GOLD_GAME_WIN_GOLD_BACK=201;//桌子金币回到帐号
	
	public static final int OPERATION_TYPE_SUB_GOLD_GAME_SERVICE_FEE=102;//每局扣金币
	
	public static final int OPERATION_TYPE_ADMIN_CHANGE_USER_DATA = 1000;		//管理员手动修改玩家数据
	public static final int OPERATION_TYPE_ADMIN_EXCHANGE_PLAYER_GOLD = 1001;	//后台转赠
	public static final int OPERATION_TYPE_WEB_REQUEST_SUB_GOLD = 1002;			//网页消费扣除金币
	
	/**操作的物品类型 金币   */
	public static final int MONEY_TYPE_GOLD = 1;//金币
	
	
	public static final int OPERATION_TYPE_PLAYER_AGREE_CLOSE_VIP_ROOM = 1400; //玩家同意解散房间
	public static final int OPERATION_TYPE_PLAYER_DISAGREE_CLOSE_VIP_ROOM = 1600;  //玩家不同意解散房间
	public static final int OPERATION_TYPE_SUB_PRO_ITEM = 1601;  //使用道具
	public static final int OPERATION_TYPE_SUB_DRAWCARD = 1602;  //抽奖卡
	
	public static final int SEND_CARD_FIRST_LOGIN = 1;  //首次登陆赠卡
	public static final int SEND_CARD_WECHAT_SHARE = 2;  //首次登陆赠卡
	
	//===================俱乐部============================
	/** 403 = 转卡到俱乐部消耗道具*/
	public static final int OPERATION_TYPE_SUB_PRO_TO_CLUB=403;//转卡到俱乐部消耗道具
	/** 404 = 俱乐部-开局扣卡*/
	public static final int OPERATION_TYPE_SUB_PRO_TO_CLUB_START_GAME=404;//俱乐部-开局扣卡
	/**修改玩家俱乐部权限  2018-08-28*/
	public static final int OPERATION_TYPE_UPDATE_PLAYER_CLUB_TYPE = 21;
	/**修改玩家俱乐部状态  2018-08-29*/
	public static final int OPERATION_TYPE_UPDATE_CLUB_STATE = 22;
	
}
