package com.chess.common.constant;

public class PayConstant {

	/** oss 平台 */
	public static final int HISTORY_PLATFORM_TYPE_TEST = 0;
	/** 联通平台 */
	public static final int HISTORY_PLATFORM_TYPE_UNI_PAY = 1;

	/** 电信天翼平台 */
	public static final int HISTORY_PLATFORM_TYPE_E_SURFING = 2;

	/** OPPO平台 */
	public static final int HISTORY_PLATFORM_TYPE_OPPO = 3;
	/** UUCun平台 */
	public static final int HISTORY_PLATFORM_TYPE_UUCUN = 4;

	/** 支付宝 */
	public static final int HISTORY_PLATFORM_TYPE_ALIPAY = 5;

	/** 微信平台 */
	public static final int HISTORY_PLATFORM_TYPE_WX_PAY = 6;

	/** 一卡通 */
	public static final int HISTORY_PLATFORM_TYPE_YI_KA_TONG = 7;

	/** ios内支付 */
	public static final int HISTORY_PLATFORM_TYPE_IPA_PAY = 8;

	public static final int COMPLETE_TYPE_IPA_PAY = 21;

	/** 充值记录当前状态 */
	/** 0 充值回调通知接收到未实际发放到玩家账号 */
	public static final int HISTORY_CUR_STATE_NOTICE_REACH = 0;
	/** 1 充值回调通知接收到已实际发放到玩家账号 */
	public static final int HISTORY_CUR_STATE_SUCCESS = 1;
}
