package com.chess.common.constant;


public class DBConstant
{
	

	public static final int 	USER_DAO =1005;
	public static final int 	USER_LOG_DAO =1006;
	public static final int 	USER_ITEM_DAO =1007;
	//public static final int 	PLAYER_EXT_DAO =4;
	public static final int 	LOGIN_LOG_DAO =1008;
	public static final int		USER_FRIEND_DAO=1009;
	public static final int   	USER_HUTYPE_DAO =1010;
	public static final int     VIP_ROOM_DAO = 1011;
	public static final int		VIP_GAME_DAO = 1012;
	public static final int		USER_PLAY_LOG_DAO = 1013;	
	public static final int     PROXY_VIP_ROOM_DAO = 1015;//代开优化 cc modify 2017-9-26
	public static final int		CLUB_MEMBER_DAO=1016;
	//
	public static final int USER_DAO_updateOfflineTime=1;
	public static final int USER_DAO_updatePlayerGold=2;
	public static final int USER_DAO_updatePlayerRecord=3;
	public static final int USER_DAO_updatePlayerHead=4;
	public static final int USER_DAO_updatePlayerLife=5;
	public static final int USER_DAO_updatePlayerLifeAndGold=6;
	public static final int USER_DAO_updatePlayerLoginMsg=7;
	
	public static final int USER_DAO_updatePlayerHistoryBestScore=8;
	public static final int USER_DAO_updatePlayerTodayBestScore=9;
	public static final int USER_DAO_updatePlayerVipAndExp=10;
	
	public static final int USER_DAO_updatePlayerGem=11;
	
	/**修改用户*/
	public static final int USER_DAO_updatePlayer = 12;
	
	/**添加好友*/
	public static final int USER_FRIEND_AddPLAYERFRIEND = 13;
	
	/**删除好友*/
	public static final int USER_FRIEND_DELPLAYERFRIEND = 14;
	
	/**修改密码*/
	public static final int USER_DAO_updatePlayerPassWorld = 15;
	
	/**修改加好友设置*/
	public static final int USER_DAO_updatePlayerCanFriend = 16;
	
	/**修改好友备注名称*/
	public static final int USER_DAO_updatePlayerRemark = 17;
	
	/**修改玩家类别*/
	public static final int USER_DAO_updatePlayerType = 18;
	
	/**更新玩家手机号码*/
	public static final int USER_DAO_updatePlayerPhoneNumber = 19;
	
	/**更新玩家位置信息*/
	public static final int USER_DAO_updatePlayerLocation = 20;
	
	public static final int USER_DAO_updatePlayerJanJiGameResult = 21;
	
	public static final int USER_DAO_update_share_num = 22;
	/**更新好友验证信息*/
	public static final int USER_FRIEND_DAO_updateApplyResult = 30;
	

	/**创建一条普通场游戏记录*/
	public static final int NORMAL_GAME_DAO_create_normal_game = 70;
	
	/**创建VIP一条房间房间记录*/
	public static final int VIP_ROOM_DAO_create_vip_room = 80;
	/**更新VIP房间记录*/
	public static final int VIP_ROOM_DAO_update_vip_room = 81;
	/**解散VIP房间记录*/
	public static final int VIP_ROOM_DAO_end_vip_room = 82;
	
	/**创建VIP一条代理开房记录*/
	public static final int PROXY_VIP_ROOM_DAO_create_vip_room = 83;
	/**更新代理开房VIP房间记录*/
	public static final int PROXY_VIP_ROOM_DAO_update_vip_room = 84;
	/**解散代理开房VIP房间记录*/
	public static final int PROXY_VIP_ROOM_DAO_end_vip_room = 85;
	
	
	/**创建一条VIP房间游戏记录*/
	public static final int VIP_GAME_DAO_create_vip_game = 90;
	
	public static final int USER_PLAY_DAO_inc_vip_count = 100;
	public static final int USER_PLAY_DAO_inc_normal_count = 101;
	public static final int USER_PLAY_DAO_inc_create_vip_count = 102;
	public static final int USER_PLAY_DAO_update_vip_count = 103;
	public static final int USER_PLAY_DAO_update_normal_count = 104;
	public static final int USER_PLAY_DAO_delete_player_log = 105;
	



	public static final int USER_LOG_DAO_insert=1;
	
	public static final int USER_ITEM_DAO_deleteByPlayerItemID=1;
	public static final int USER_ITEM_DAO_updatePlayerItemNum=2;
	
	
	//public static final int 	PLAYER_EXT_DAO_ADD_NUM_BY_PARAM =1;
	
	public static final int 	LOGIN_LOG_DAO_CREATE_LOG =1;
	public static final int 	LOGIN_LOG_DAO_CREATE_UPDATE_LOGIN_OUT_TIME =2;
	
	/**俱乐部 添加成员*/
	public static final int CLUB_MEMBER_add_member = 1;
	/**俱乐部 删除成员*/
	public static final int CLUB_MEMBER_delete_member = 2;
	/**俱乐部 更新成员状态*/
	public static final int CLUB_MEMBER_update_member_state = 3;
	/**俱乐部 更新成员备注*/
	public static final int CLUB_MEMBER_update_member_remark = 4;
}
