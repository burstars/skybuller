
package com.chess.common.constant;

public class VipConstant {
	
	
	
	/**vip等级经验*/
	public static final int VIPL0_EXP=0;
	public static final int VIPL1_EXP=100;
	public static final int VIPL2_EXP=300;
	public static final int VIPL3_EXP=600;
	public static final int VIPL4_EXP=1000;
	public static final int VIPL5_EXP=1500;
	/**vip等级*/
	public static final int VIPL0=0;
	public static final int VIPL1=1;
	public static final int VIPL2=2;
	public static final int VIPL3=3;
	public static final int VIPL4=4;
	public static final int VIPL5=5;
	/**经验基数，1元等于10经验*/
	public static final int EXP_BASE=10;
	
}
