package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.oss.OssRole;

/**管理员角色*/
public interface IOssRoleDAO {
	
	public Integer createOssRole(OssRole ossRole);

	public Integer updateOssRole(OssRole ossRole);

	public Integer deleteOssRoleByID(Integer ossRoleID);

	public OssRole getOssRoleByID(Integer ossRoleID);

	public List<OssRole> getOssRoleList();

}
