package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.oss.OssMonitor;

;

public interface IOssMonitorDAO {
	public void createOssMonitor(OssMonitor ossMonitor);

	public void updateOssMonitor(OssMonitor ossMonitor);

	public void deleteOssMonitorByID(Integer ossMonitorID);

	public OssMonitor getOssMonitorByID(Integer ossMonitorID);

	public List<OssMonitor> getOssMonitorList();
	
	/**根据时间获取系统信息列表*/
	public List<OssMonitor> getOssMonitorMinutesListByMinute(Long begin, Long end);


}
