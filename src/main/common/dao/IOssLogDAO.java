package com.chess.common.dao;

import java.util.Date;
import java.util.List;

import com.chess.common.bean.oss.OssLog;



public interface IOssLogDAO {

	public String createOssLog(OssLog ossLog);

	public void updateOssLog(OssLog ossLog);

	public void deleteOssLogByID(String ossLogID);

	public OssLog getOssLogByID(String ossLogID);

	public List<OssLog> getOssLogList();
	
	
	/**
	 * 获取记录总数
	 * @param beginTime
	 * @param endTime
	 * @param optionType
	 * @param beginNum
	 * @param pageNum
	 * @return
	 */
	public Integer getOssLogListCount(Date beginTime,Date endTime,String optionType);
	/**
	 * 获取记录列表（分页）
	 * @param beginTime
	 * @param endTime
	 * @param optionType
	 * @param beginNum
	 * @param pageNum
	 * @return
	 */
	public List<OssLog> getOssLogList(Date beginTime,Date endTime,String optionType,Integer beginNum,Integer pageNum);

}
