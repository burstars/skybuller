package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.Reward;

public interface IRewardDAO {
	
	public void deleteByName(String name);
	
	public void deleteByID(int id);
	
	public void update(Reward r);
	
	public List<Reward> getRewardList();
	
	public void insert(Reward r);
	
	public Reward getRewardByID(int id);
	
	
}
