package com.chess.common.dao;

import java.util.Date;
import java.util.List;

import com.chess.common.bean.User;
 
 

public interface IUserDAO {

	/**
	 * 创建玩家
	 * @param player
	 * @return
	 */
	public String createPlayer(User player);
	
	public void updatePlayerRecord(String playerID,int wons,int loses,int escape);
	
	  /**
     * 获取指定玩家数据
     * @param playerID
     * @return
     */
    public User getPlayerByID(String playerID);
    
    /**
     * 根据id和游戏id获取玩家数据
     * @param playerID
     * @param diamond
     */
    public User getPlayerByIDAndGameID(String playerID,String gameID);
    
    public void updatePlayerDiamonds(String playerID,int diamond);
    public void updatePlayerHistoryBestScore(String playerID,int score);
    public void updatePlayerTodayBestScore(String playerID,int score);
    
    
    public void updateSaveTime(String playerID,int saveTime); 
	/**
	 * 更新金币值
	 */
	public void updatePlayerGold(String playerID,int gold);
	
	public void updatePlayerGem(String playerID,int gemNum);
	/**
	 * 更新生命值
	 */
	public void updatePlayerLife(String playerID,int life);
	
	
	public void updatePlayerLifeAndGold(String playerID,int gold,int life);
 
	/**
	 * 更新头像
	 */
	public void updatePlayerHead(String playerID,int headImg);
	
	/**更新玩家类别*/
	public void updatePlayerType(String playerID, int playerType);
	
	/**更新玩手机号码*/
	public void updatePlayerPhoneNumber(String playerID, String phoneNumber);

	//完善玩家资料
	public void updatePlayerIdentifyCard(String PlayerID, String phoneNumber,
										 String identifyCard, String weixinNum, String qqNum, String name,
										 String birthday, String address);

	/**更新玩家位置信息*/
	public void updatePlayerLocation(String playerID, String location);
	//分享次数更新
	public void updatePlayerShareNum(String playerID, int shareNum , long shareDate);
	
	/**获取数据库中是否有该手机号码*/
	public int getPhoneNumberCount(String phoneNumber);
	
	/**更新用户*/
	public int updatePlayer(User player);
	
	/**
	 * 修改密码
	 * @author dong
	 * @param playerID  用户名称
	 * @param password  密码
	 * */
	public int updatePlayerPassword(String playerID,String password);
	
	/**修改是否可以被对方添加为好友设置*/
	public int updatePlayerCanFriend(String playerID,int canflag);
	
	//系统调度，添加生命值
	public void addLifeNoOverflow();
	
  
    
    public User getPlayerByAccount(String account);
    
    public User getPlayerByName(String playerName);
    
	public User getPlayerByMachineCode(String machineCode);
	
	public User getPlayerByQQOpenID(String qqOpenID);
	public User getPlayerByWXOpenID(String qqOpenID);
	public User getPlayerByWXUnionID(String unionID);//cc modidfy 2017-12-5
	
	public void updateContinueLanding(String playerID,int days);
	
	public void updateLastLoginTime(String playerID,Date date);
	
	public void updateOfflineTime(String playerID,Date date) ;
	 /**同时更新玩家金币，最后登录时间，持续登录天数，抽奖次数和钻石 */
	public void updatePlayerLoginMsg(String playerID,int gold,Date lastLoginTime,int continueLanding,int luckyDrawsTimes,int diamond);
	
	public void updateVIP(String playerID,int vipExp,int vipLevel);
	
    /**获得所有玩家信息*/
	public List<User> getAllPlayer();
	/**获得所有玩家信息,分页*/
	 public List<User> getAllPlayerByPage(String startRownum,String endRownum, String startTime, String endTime, int order, int queryPlayerType);
	 /**获得所有记录总数*/
	 public int getTotalCount(String startTime, String endTime, int queryPlayerType);

	 /**每日调度清理掉玩家每天的最高积分*/
		public void clearTodayBestScoreAllPlayer();
	/**更新玩家索引号*/
	public void updatePlayerIndex(String playerID,int playerIndex) ;
	
	public User getPlayerByPlayerIndex(int playerIndex);
	
	/**按渠道获得玩家数目*/
	 public int getPlayerNumByPlatformType(int platformType);

	public int getCustomTotalCount(String account);

	public List<User> getAllCustomPlayerByPage(String startRownum,
			String endRownum, String account);
	
	/**根据ID查询所有玩家*/
	public List<User> getPlayersByPlayerIDByPage(String startRownum,String endRownum,String account, String startTime, String endTime, int order);
	
	/**根据ID统计玩家数量*/
	public int getPlayersByPlayerIDCount(String account, String startTime, String endTime);
	
	/**根据帐号查询所有玩家*/
	public List<User> getPlayersByPlayerAccountByPage(String startRownum,String endRownum,String account, String startTime, String endTime, int order);
	
	/**根据帐号统计玩家数量*/
	public int getPlayersByPlayerAccountCount(String account, String startTime, String endTime);
	
	/**根据昵称查询所有玩家*/
	public List<User> getPlayersByPlayerNameByPage(String startRownum,String endRownum,String account, String startTime, String endTime, int order);
	
	/**根据昵称统计玩家数量*/
	public int getPlayersByPlayerNameCount(String account, String startTime, String endTime);

	/**更新用户unionid*/
	public void updatePlayerUnionID(String playerID,String unionID);
	
	/**更新用户网络头像地址*/
	public void updatePlayerHeadImgUrl(String playerID,String headImgUrl);
	
	/**更新用户昵称*/
	public void updatePlayerName(String playerID,String playerName);
	
	/**更新用户param01列  20161203*/
	public void updatePlayerParam01(String playerID,String param01);
	/**更新用户代开房间权限*/
	public void updatePlayerProxyOpenRoom(String playerID, int proxyOpenRoom);
	//玩家积分增加
	public void addPlayerCredits(String playerID, int credits, int totalCredits);
	
	//玩家花费积分
	public void costPlayerCredits(String playerID, int credits) ;
	/**更新用户性别*/
	public void updatePlayerSex(String playerID, int sex);
	
	/**更新用户抽奖次数*/
	public void updatePlayerFreeCj(String playerID, int cjcs);
	
	/**更新所有用户抽奖次数和param01*/
	public void updateAllPlayerFreeCjAndParam01();
	/** 修改俱乐部权限  2018-08-28 */
	public void updateClubType(String playerID,int clubType);
	
	public void updateClubPlayerGold(int goldNum, String playerId);
	
	/**更新用户推荐人*/
	public void updatePlayerMgrID(String playerID, String mgrID,String mgrIndex);
	
	/**查询推荐人index*/
	public String getPlayerMgrIndex(String playerID);
	
}