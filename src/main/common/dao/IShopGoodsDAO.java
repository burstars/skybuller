package com.chess.common.dao;

import java.io.IOException;
import java.util.List;

import com.chess.common.bean.ShopGoods;

public interface IShopGoodsDAO {
	public List<ShopGoods> getShowShopGoods() throws IOException;
	
	public List<ShopGoods> getAllShopGoods();

	public void UpdateShowGoods(ShopGoods sg);

	public void insertShopGood(ShopGoods sg);

	public void updateGoodState(String goodId, int state);

	public void UpdateShowGoodsExceptPicture(ShopGoods sg);

	public ShopGoods unZipPicture(ShopGoods sg) throws IOException;

}



