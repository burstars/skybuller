package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.UserBackpack;
 
 

public interface IUserBackpackDAO {

 
	public void createPlayerItem(UserBackpack item,String gameID);
	
    public List<UserBackpack> getPlayerItemListByPlayerID(String playerID);
    
    public void deleteByPlayerItemID(String playerItemID);
    //
    public void updatePlayerItemNum(String playerID,Integer itemBaseID,int itemNum,String gameID);
    
    public void addPlayerItemNum(String playerID,Integer itemBaseID,int itemNum);
    
    public List<UserBackpack> getPlayerItemListByItemID(String itemID);
  
    public List<UserBackpack> getAllItemByPlayer(String gameID,String playerID);
}