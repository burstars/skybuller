/**
 * Author:Wangds
 */
package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.NormalGameRecord;

public interface INormalGameRecordDAO {

	/**
	 * 创建一张表
	 * 
	 * @param tableNameSuffix
	 *            表明后缀
	 */
	public void createTable(String tableNameSuffix);
	
	/**
	 * 创建玩家的VIP房间每锅记录
	 * 
	 * @param roomRecord
	 *            Vip房间记录的实例对象
	 */
	public void createNormalGameRecord(NormalGameRecord normalGameRecord);
	
	/**
	 * 获取普通场游戏记录
	 * @param date	日期，格式：20140529
	 * @return
	 */
	public List<NormalGameRecord> getNormalGameRecordByDate(String date, String player_index);
	
	public String checkNormalGameRecordTableIsExists(String tableName);
	
	public List<NormalGameRecord> getAllNormalRecordByPage(String date, String player_index,String startRownum, String endRownum);
}
