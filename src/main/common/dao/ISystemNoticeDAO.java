package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.SystemNotice;

public interface ISystemNoticeDAO {

	public void createSystemNotice(SystemNotice msg);
	
	public void deleteSystemNotice(String type);
	
	public SystemNotice getSystemNotice();
	
	public int updateSystemNotice(SystemNotice notice);
	//获取固定跑马灯
	public SystemNotice getForeverMsg() ;
//	获取消息表中所有数据-lxw20180903
	public List<SystemNotice> getNoticeAll();
	
}
