package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.Prize;

public interface IPrizeDAO {

	/**插入*/
	public void insert(Prize prize);
 
	/**获取所有prize*/
	public List<Prize> getPrizeList();
	
	/**通过ID获得prize*/
	public Prize getPrizeByID(int id);
	
	/**更新prize*/
	public void update(Prize p);
	
	public void deleteByID(int id);
	
	
	
}
