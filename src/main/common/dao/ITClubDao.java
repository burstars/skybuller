package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.TClub;

public interface ITClubDao {
	/**获取我的俱乐部：我创建的*/
	public List<TClub> getMyClubs(String playerId);
	/**获取我的俱乐部：我加入的*/
	public List<TClub> getJoinClubs(String playerId);
	/**创建俱乐部*/
	public TClub createClub(TClub club);
	/**更新俱乐部状态*/
	public void updateClubState(int clubCode, int clubState);
	/**根据玩家ID获取所有俱乐部*/
	public List<TClub> getAllClubs(String playerId);
	/**查询所有俱乐部*/
	public List<TClub> getAllClubs();
	/**根据俱乐部编号查询*/
	public TClub getClubByClubCode(int clubCode,String memberId);
	
	public TClub getClubByClubCode(int clubCode);
	
	public List<TClub> getClubBySearch(String searchTxt);
	/**更新俱乐部库存*/
	public void updateClubFangkaNum(int clubCode, int fangkaNum);
	
	public void updateClubLevel(int clubCode, String roomLevel);
	
	void addClubGold(int clubCode, int goldNum);
}



