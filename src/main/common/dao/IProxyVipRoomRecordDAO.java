/**
 * Description:代开房间记录的DAO接口
 */
package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.ProxyVipRoomRecord;
import com.chess.common.bean.VipRoomRecord;

public interface IProxyVipRoomRecordDAO {

	/**
	 * 创建一张表
	 * 
	 * @param tableNameSuffix
	 *            表明后缀
	 */
	public void createTable(String tableNameSuffix);

	/**
	 * 创建代开房间记录
	 * 
	 * @param proxyVipRoomRecord
	 *            代开房间记录的实例对象
	 */
	public void createProxyVipRoomRecord(ProxyVipRoomRecord proxyVipRoomRecord);

	/**
	 * 通过代开玩家ID，获取代开房间记录（前x条记录）
	 * 
	 * @param playerID
	 *            玩家ID
	 * @param count
	 *            指定条数记录
	 */
	public List<ProxyVipRoomRecord> getMyProxyVipRoomRecord(String dateStr, String playerID);

	//
	public void updateRecord(ProxyVipRoomRecord record);

	//
	public void endRecord(ProxyVipRoomRecord record);

	public String checkVipRoomRecordTableIsExists(String tableName);

	public List<ProxyVipRoomRecord> getAllProxyVipRoomRecordByPage(String dateStr, String playerID, String startRownum, String endRownum);
	
	public int getVipRoomIndexCount(String tableName);
}
