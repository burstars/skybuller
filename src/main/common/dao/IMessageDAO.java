package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.Message;

public interface IMessageDAO {
   /**添加信息*/
   public void createMessage(Message message);
   
   /**删除信息*/
   public int deleteMessage(String playerID);
   
   /**阅读信息，更新阅读状态*/
   public int updateMessageState(String playerID);  
   
   /**取得信息列表*/
   public List<Message> getMessageByPlayerID(String playerID);  
}
