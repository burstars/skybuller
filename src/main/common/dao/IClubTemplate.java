package com.chess.common.dao;

import java.util.Date;
import java.util.List;

import org.jacorb.idl.runtime.int_token;

import com.chess.common.bean.ClubTemplate;
import com.chess.common.bean.User;
 
 

public interface IClubTemplate {

	/**
	 * 创建模板
	 * @param clubTemplate
	 */
	public void insert(ClubTemplate clubTemplate);
	
	/**
	 * 更新模板
	 * @param clubTemplate
	 */
	public void update(ClubTemplate clubTemplate);
	
	/**
	 * 删除一条模板
	 * @param templateIndex
	 */
	public void delate(int templateIndex);
	
	/**
	 * 删除当前俱乐部下所有模板
	 * @param clubCode
	 */
	public void delateAll(int clubCode);
	
	/**
	 * 获取当前俱乐部下所有模板
	 * @param clubCode
	 * @return
	 */
	public List<ClubTemplate> getAll(int clubCode);
	
	/**
	 * 获取一条模板
	 * @param id
	 * @return
	 */
	public ClubTemplate getClubTemplate(int clubCode,int tempIndex);
	
}