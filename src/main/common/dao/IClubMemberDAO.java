package com.chess.common.dao; 

import java.util.List;

import org.jacorb.idl.runtime.int_token;

import com.chess.common.bean.ClubMember;
import com.chess.common.bean.UserFriend;

/** 
 * @author wangjia
 * @version 创建时间：2018-8-29 
 * 类说明 :
 */
public interface IClubMemberDAO {
	/**
	 * 添加成员
	 * @param playerid
	 * @param memberId
	 * @param clubCode
	 * @return
	 */
	public String createClubMember(String playerId, String memberId,String clubCode);
	
	/**
	 * 删除成员
	 * @param playerID  玩家ID
	 * @param memberId  成员ID
	 * */
	public int deleteClubMember(String playerId,String memberId,String clubCode);
	
	/**
	 * 同意添加
	 * @param playerID  玩家ID
	 * @param memberId  成员ID
	 * */
	public int updateClubMemberState(String playerId,String memberId,String clubCode,int applyState);
	
	/**
	 * 所有好友
	 * */
	public List<ClubMember> getClubMembers(String playerId,String clubCode,int applyState);
	
	/**
	 * 亲有圈管理员
	 * */
	public ClubMember getPlayerByClubCode(String clubCode);
	
	/**
	 * 修改备注
	 * @param playerID  玩家ID
	 * @param memberId  成员ID
	 * */
	public int updateClubMemberRemark(String playerId,String memberId,String clubCode,String remark);
	
	public int updateClubMemberGold(int clubCode,String memberId,int goldNum);
    public int updateClubMemberTotalPay(int clubCode,String memberId,int goldNum);
    
	/**
	 * 添加成员
	 * @param playerid
	 * @param memberId
	 * @param clubCode
	 * @return
	 */
	public boolean isExistsMember(String playerId, String memberId,String clubCode);
	
	/**查询俱乐部成员数量*/
	public int queryMemberCount(int clubCode);
	/**查询玩家是否已加入当前俱乐部*/
	public ClubMember queryMemberClub(int clubCode,String memberId);
	/**
	 * @description 根据俱乐部编号获取该俱乐部下的所有成员
	 * /
	 * @param clubCode
	 * @return
	 */
	public List<String> getClubMembersByClubCode(int clubCode);
	
	public ClubMember getClubMemberByPlayerId(int clubCode, String playerId);
}
 
