package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.SystemConfigPara;
import com.chess.common.dao.ISystemParamDAO;

public class SystemParamDAO extends SqlMapClientDaoSupport implements ISystemParamDAO
{

	public SystemParamDAO()
	{
		
	}
	public void updatePara(SystemConfigPara para)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("valueInt", para.getValueInt());
		params.put("valueStr", para.getValueStr());
		params.put("paraID", para.getParaID());
		params.put("pro_1", para.getPro_1());
		params.put("pro_2", para.getPro_2());
		params.put("pro_3", para.getPro_3());
		params.put("pro_4", para.getPro_4());
		params.put("pro_5", para.getPro_5());
		params.put("paraDesc", para.getParaDesc());
		params.put("isclient", para.getIsclient());
		this.getSqlMapClientTemplate().update("SystemParam.updatePara", params);
	}
	
	
	public SystemConfigPara getPara(int paraID)
	{
		return (SystemConfigPara)this.getSqlMapClientTemplate().queryForObject("SystemParam.getPara",paraID);
	}
 
	public List<SystemConfigPara> getAllConfigPara()
	{
	    List<SystemConfigPara> bases = this.getSqlMapClientTemplate().queryForList("SystemParam.getAllConfigPara");
	    return bases;
	}
	   
	public List<SystemConfigPara> getAllClientConfigPara()
	{
	    List<SystemConfigPara> bases = this.getSqlMapClientTemplate().queryForList("SystemParam.getAllClientConfigPara");
	    return bases;
	}
	 
}
