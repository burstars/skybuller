package com.chess.common.dao.impl;

import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.ShopGoods;
import com.chess.common.dao.IShopGoodsDAO;


public class ShopGoodsDAO extends SqlMapClientDaoSupport implements IShopGoodsDAO {
	public List<ShopGoods> getAllShopGoods() {
		
		return this.getSqlMapClientTemplate().queryForList("ShopGoods.getAllShopGoods");
	}

	public List<ShopGoods> getShowShopGoods()  {
		List<ShopGoods> shopGoodResultGoods =new ArrayList<ShopGoods>();
		try {
			shopGoodResultGoods = unZipByte();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return shopGoodResultGoods;
	}
	
	public List<ShopGoods> unZipByte() throws IOException{
		List<ShopGoods> listGoods = (List<ShopGoods>)this.getSqlMapClientTemplate().queryForList("ShopGoods.getShowShopGoods");
 		
		for(int j = 0 ; j< listGoods.size();j++){   
			                
             ShopGoods shopGoods = listGoods.get(j); 
             byte[] goodsPicure = shopGoods.goodsPicure; 

             goodsPicure = unzipBytes(goodsPicure);
             shopGoods.goodsPicure = goodsPicure;
             
         } 
		return listGoods;
	}
	
	public ShopGoods unZipPicture(ShopGoods good) throws IOException{
		
         byte[] goodsPicure = good.goodsPicure; 

         goodsPicure = unzipBytes(goodsPicure);
         good.goodsPicure = goodsPicure;
		
		return good;
	}
	
	public void UpdateShowGoods(ShopGoods sg) {
		
		this.getSqlMapClientTemplate().update("ShopGoods.updateShopGood", sg);
	}
	
	public void UpdateShowGoodsExceptPicture(ShopGoods sg) {
		
		this.getSqlMapClientTemplate().update("ShopGoods.UpdateShowGoodsExceptPicture", sg);
	}
	
	public void insertShopGood(ShopGoods sg){
		
		this.getSqlMapClientTemplate().insert("ShopGoods.insertShopGood", sg);
	}
	
	public void updateGoodState(String goodId ,int state){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("goodId", goodId);
		params.put("state", state);
		
		this.getSqlMapClientTemplate().update("ShopGoods.updateGoodState", params);
	}
	
	
	public static byte[] unzipBytes(byte[] bytes) throws IOException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		unzipBytes(bytes, os);
		return os.toByteArray();
	}
	
	public static void unzipBytes(byte[] bytes, OutputStream os)
									throws IOException {
		ByteArrayInputStream tempIStream = null;
		BufferedInputStream tempBIStream = null;
		ZipInputStream tempZIStream = null;
		ZipEntry tempEntry = null;
		long tempDecompressedSize = -1;
		byte[] tempUncompressedBuf = null;
	
		tempIStream = new ByteArrayInputStream(bytes, 0, bytes.length);
		tempBIStream = new BufferedInputStream(tempIStream);
		tempZIStream = new ZipInputStream(tempBIStream);
		tempEntry = tempZIStream.getNextEntry();
	
		if (tempEntry != null) {
			tempDecompressedSize = tempEntry.getCompressedSize();
			if (tempDecompressedSize < 0) {
				tempDecompressedSize = Long.parseLong(tempEntry.getName());
			}
		
			int size = (int) tempDecompressedSize;
			tempUncompressedBuf = new byte[size];
			int num = 0, count = 0;
			while (true) {
				count = tempZIStream.read(tempUncompressedBuf, 0, size - num);
				num += count;
				os.write(tempUncompressedBuf, 0, count);
				os.flush();
				if (num >= size)
					break;
			}
		}
		tempZIStream.close();
	}
}
