package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.GameActiveCode;
import com.chess.common.dao.IExchangePrizeDAO;

public class ExchangePrizeDAO  extends SqlMapClientDaoSupport implements IExchangePrizeDAO {

	public void exchangePrize(int userId,int value, int id) {
		// TODO Auto-generated method stub
		Map<String ,Object> params = new HashMap<String,Object>();
		params.put("userID", userId);
		params.put("value", value);
		params.put("id", id);
		this.getSqlMapClientTemplate().update("GameActiveCode.updateCode",params);
	}
	
	public GameActiveCode searchCode(String code) {
		// TODO Auto-generated method stub
		Map<String ,Object> params = new HashMap<String,Object>();
		params.put("code", code);
		return  (GameActiveCode)this.getSqlMapClientTemplate().queryForObject("GameActiveCode.searchCode",params);
	
		
	}

}
