package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.PlayerHuType;
import com.chess.common.dao.IPlayerHuTypeDAO;
 

public class PlayerHuTypeDAO extends SqlMapClientDaoSupport implements IPlayerHuTypeDAO {
	
 
	private static Logger logger = LoggerFactory.getLogger(UserDAO.class);
	
	public void createPlayerHuType(PlayerHuType huType)	{
		this.getSqlMapClientTemplate().insert("PlayerHuType.insert", huType);
	}
	
    public List<PlayerHuType> getPlayerHuTypeListByPlayerID(String playerID) {
    	return this.getSqlMapClientTemplate().queryForList("PlayerHuType.getPlayerHuTypeListByPlayerID",playerID);
    }
	 
    //
    public void updatePlayerHuTypeCount(String playerID,Integer huType,int count) {
    	Map<String, Object> params = new HashMap<String, Object>();
		params.put("huID", playerID+"_"+huType);
		params.put("count", count);

		this.getSqlMapClientTemplate().update("PlayerHuType.updatePlayerHuTypeCount", params);
    }
	
 
}