package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.oss.OssRole;
import com.chess.common.dao.IOssRoleDAO;

public class OssRoleDAO extends SqlMapClientDaoSupport implements IOssRoleDAO{

	public Integer createOssRole(OssRole ossRole) {
		return (Integer)this.getSqlMapClientTemplate().insert("OssRole.createOssRole", ossRole);
	}

	public Integer updateOssRole(OssRole ossRole) {
		return this.getSqlMapClientTemplate().update("OssRole.updateOssRole", ossRole);
	}

	public Integer deleteOssRoleByID(Integer ossRoleID) {
		return this.getSqlMapClientTemplate().delete("OssRole.deleteOssRoleByID", ossRoleID);
	}

	public OssRole getOssRoleByID(Integer ossRoleID) {
		return (OssRole)this.getSqlMapClientTemplate().queryForObject("OssRole.getOssRoleByID", ossRoleID);
	}
	
	@SuppressWarnings("unchecked")
	public List<OssRole> getOssRoleList() {
		return this.getSqlMapClientTemplate().queryForList("OssRole.getOssRoleList");
	}

}
