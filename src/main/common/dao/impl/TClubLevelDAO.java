package com.chess.common.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.TClubLevel;
import com.chess.common.dao.ITClubLevelDao;

/**
 * 俱乐部等级
 */
public class TClubLevelDAO extends SqlMapClientDaoSupport implements ITClubLevelDao {

	public List<TClubLevel> getAllClubLevels(){
		return this.getSqlMapClientTemplate().queryForList("TClubLevel.getAllClubLevels","");
	}
	
}
