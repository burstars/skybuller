package com.chess.common.dao.impl;

import java.sql.SQLException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.bean.ItemOpStaBase;
import com.chess.common.bean.PlayerOperationLog;
import com.chess.common.bean.SendCardLog;
import com.chess.common.dao.IPlayerLogDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ISystemConfigService;

public class PlayerLogDAO extends SqlMapClientDaoSupport implements IPlayerLogDAO {

	private static String lastTableSuffixName = "";
	private static List<String> lastTablePrefixNameList = new ArrayList<String>();
	protected static Format format = new SimpleDateFormat("yyyyMMdd");

	/**
	 * 创建一条记录
	 */
	public void insert(PlayerOperationLog playerLog) {
		if (lastTableSuffixName.equals(playerLog.getTableNameSuffix()) == false ){
			lastTablePrefixNameList.clear();
		}
		
		if (lastTableSuffixName.equals(playerLog.getTableNameSuffix()) == false || lastTablePrefixNameList.contains(playerLog.getTableNamePrefix()) == false) {// 

			lastTableSuffixName = playerLog.getTableNameSuffix();
			if(!lastTablePrefixNameList.contains(playerLog.getTableNamePrefix())){
				lastTablePrefixNameList.add(playerLog.getTableNamePrefix());
			}
			Map<String, String> parmMap = new HashMap<String, String>();
			parmMap.put("tableNamePrefix", playerLog.getTableNamePrefix());
			parmMap.put("tableNameSuffix", playerLog.getTableNameSuffix());
			createTable(parmMap);
		}

		this.getSqlMapClientTemplate().insert("PlayerLog.insert", playerLog);

	}

	/**
	 * 创建一张表
	 */
	public void createTable(Map<String, String> parmMap) {
		this.getSqlMapClientTemplate().insert("PlayerLog.createTable", parmMap);

	}

	public static String getLastTableSuffixName() {
		return lastTableSuffixName;
	}

	public static void setLastTableSuffixName(String lastTableSuffixName) {
		PlayerLogDAO.lastTableSuffixName = lastTableSuffixName;
	}
	
	public static List<String> getLastTablePrefixNameList() {
		return lastTablePrefixNameList;
	}

	public static void setLastTablePrefixNameList(List<String> lastTablePrefixNameList) {
		PlayerLogDAO.lastTablePrefixNameList = lastTablePrefixNameList;
	}

	public List<PlayerOperationLog> queryByPlayerIDAndOpType(HashMap maps) throws SQLException, ParseException {

		String startTime = (String) maps.get("beginTime");
		String endTime = (String) maps.get("endTime");
		Calendar cal = Calendar.getInstance();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date begin = sdf.parse(startTime.substring(0, 10));
		Date end = sdf.parse(endTime.substring(0, 10));
		cal.setTime(begin);
		long daysBetween = (end.getTime() - begin.getTime() + 1000000) / (3600 * 24 * 1000);// 相隔的天数
		logger.debug(startTime + "与 " + endTime + " 相隔 " + daysBetween + " 天");//System.out.println(startTime + "与 " + endTime + " 相隔 " + daysBetween + " 天");
		List<String> days = new ArrayList<String>();
		String gameId = GameContext.gameId;
		for (int i = 0; i <= daysBetween; i++) {
			if (daysBetween == 0) {
				String tableName = "t_player_log__" + gameId + "__" + startTime.substring(0, 4) + startTime.substring(5, 7) + startTime.substring(8, 10);
				if (this.checkPlayerLogTableIsExists(tableName) != null) {
					days.add(startTime.substring(0, 4) + startTime.substring(5, 7) + startTime.substring(8, 10));
				}
			} else {
				cal.add(Calendar.DAY_OF_MONTH, 1);
				String a = format.format(cal.getTime());
				String tableName = "t_player_log__" + gameId + "__" + a;
				if (this.checkPlayerLogTableIsExists(tableName) != null) {
					days.add(a);
				}
			}
		}
		if (days.size() > 0) {
			maps.put("days", days);
		}

		maps.put("tableNamePrefix", GameContext.gameId);
		return this.getSqlMapClientTemplate().queryForList("PlayerLog.getLogByCondition", maps);

	}

	public int queryByPlayerIDAndOpTypeTotal(HashMap maps) throws SQLException, ParseException {
		String startTime = (String) maps.get("beginTime");
		String endTime = (String) maps.get("endTime");
		Calendar cal = Calendar.getInstance();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date begin = sdf.parse(startTime.substring(0, 10));
		Date end = sdf.parse(endTime.substring(0, 10));
		cal.setTime(begin);
		long daysBetween = (end.getTime() - begin.getTime() + 1000000) / (3600 * 24 * 1000);// 相隔的天数
		logger.debug(startTime + "与 " + endTime + " 相隔 " + daysBetween + " 天");//System.out.println(startTime + "与 " + endTime + " 相隔 " + daysBetween + " 天");
		List<String> days = new ArrayList<String>();
		String gameId = GameContext.gameId;
		for (int i = 0; i <= daysBetween; i++) {
			if (daysBetween == 0) {
				String tableName = "t_player_log__" + gameId + "__" + startTime.substring(0, 4) + startTime.substring(5, 7) + startTime.substring(8, 10);
				if (this.checkPlayerLogTableIsExists(tableName) != null) {
					days.add(startTime.substring(0, 4) + startTime.substring(5, 7) + startTime.substring(8, 10));
				}
			} else {
				cal.add(Calendar.DAY_OF_MONTH, 1);
				String a = format.format(cal.getTime());
				String tableName = "t_player_log__" + gameId + "__" + a;
				if (this.checkPlayerLogTableIsExists(tableName) != null) {
					days.add(a);
				}
			}
		}
		if (days.size() > 0) {
			maps.put("days", days);
			maps.put("tableNamePrefix", GameContext.gameId);
			Object tcount = this.getSqlMapClientTemplate().queryForObject("PlayerLog.getLogByConditionCounts", maps);
			if(tcount!=null)
			{
				return ((Integer)tcount).intValue();
			}
			
			return 0;
		} else {
			return 0;
		}
	}

	public List<ItemOpStaBase> countItemOperationStatistic() {

		// return
		// this.getSqlMapClientTemplate().queryForList("PlayerLog.countItemOperationStatistic");
		return null;

	}

	public List<ItemOpStaBase> countItemOperationStatisticPay(Date time, int platformType) {
		String tableNameSuffix = lastTableSuffixName;
		if (time != null) {
			tableNameSuffix = DateService.getTableSuffixByTypeAndDate("", DateService.DATE_BY_DAY, time);
		}
		String gameId = GameContext.gameId;
		String tableName = "t_player_log__" + gameId + "_" + tableNameSuffix;
		if (this.checkPlayerLogTableIsExists(tableName) != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("tableNameSuffix", tableNameSuffix); 
			params.put("tableNamePrefix", GameContext.gameId);
			if (platformType != -1) {
				params.put("platformType", platformType);
				return this.getSqlMapClientTemplate().queryForList("PlayerLog.countItemOperationStatisticPay", params);
			} else {
				return this.getSqlMapClientTemplate().queryForList("PlayerLog.countItemOperationStatisticPayAll", params);
			}

		} else {
			return null;
		}

	}

	private String checkPlayerLogTableIsExists(String tableName) {
		
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		Map<String, String> map = new HashMap<String, String>();
		map.put("tableName", tableName);
		map.put("DB", cfgService.getPara(8052).getValueStr());
		String trueName = (String) this.getSqlMapClientTemplate().queryForObject("PlayerLog.checkTableIsExists", map);
		return trueName;
	}

	public  void insertSendCard(SendCardLog sendLog) {
		
		this.getSqlMapClientTemplate().insert("SendCardLog.insertSendCardLog", sendLog);
		
	}
}
