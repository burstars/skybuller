package com.chess.common.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.SpringService;
import com.chess.common.UUIDGenerator;
import com.chess.common.bean.ItemOpStaBase;
import com.chess.common.bean.MallHistory;
import com.chess.common.dao.IMallHistoryDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ISystemConfigService;



public class MallHistoryDAO extends SqlMapClientDaoSupport implements IMallHistoryDAO{

	public Integer createPayHistory(MallHistory payHistory,String gameID) {
		payHistory.setPayHistoryId(UUIDGenerator.generatorUUID());
//		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
//		IPayConfigService ips = (IPayConfigService) SpringService.getBean("payConfigService");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("payHistoryId", payHistory.getPayHistoryId());
		map.put("playerId", payHistory.getPlayerId());
		map.put("amount", payHistory.getAmount());
		map.put("orderNo", payHistory.getOrderNo());
		map.put("payTime", payHistory.getPayTime());
		map.put("flagAccess", payHistory.getFlagAccess());
		map.put("payScript", payHistory.getPayScript());
		map.put("state", payHistory.getState());
		map.put("buyItemID", payHistory.getBuyItemID());
		map.put("transaction_id", payHistory.getTransaction_id());
		map.put("DB", GameContext.getPayConfig(gameID)+"."+"t_mall_history");
		Object obj = this.getSqlMapClientTemplate().insert("MallHistory.createPayHistory", map);
		if(obj==null)
			return Integer.valueOf(0);
		return (Integer)obj;
	}

	public Integer updatePayHistory(MallHistory payHistory,String gameID) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("payHistoryId", payHistory.getPayHistoryId());
		map.put("playerId", payHistory.getPlayerId());
		map.put("amount", payHistory.getAmount());
		map.put("orderNo", payHistory.getOrderNo());
		map.put("payTime", payHistory.getPayTime());
		map.put("flagAccess", payHistory.getFlagAccess());
		map.put("payScript", payHistory.getPayScript());
		map.put("state", payHistory.getState());
		map.put("buyItemID", payHistory.getBuyItemID());
		map.put("transaction_id", payHistory.getTransaction_id());
		map.put("payHistoryId", payHistory.getPayHistoryId());
		map.put("DB", GameContext.getPayConfig(gameID)+"."+"t_mall_history");
		return this.getSqlMapClientTemplate().update("MallHistory.updatePayHistory", map);
	}

	public Integer deletePayHistoryByID(String payHistoryId) {
		return this.getSqlMapClientTemplate().delete("MallHistory.deletePayHistoryByID", payHistoryId);
	}

	public MallHistory getPayHistoryByID(String payHistoryId) {
		return (MallHistory)this.getSqlMapClientTemplate().queryForObject("MallHistory.getPayHistoryByID", payHistoryId);
	}
	
	public MallHistory getPayHistoryByOrderID(String OrderID) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("OrderID", OrderID);
		map.put("DB", GameContext.getPayConfig(GameContext.getPayMapValue(OrderID).get("gameID"))+"."+"t_mall_history");
		return (MallHistory)this.getSqlMapClientTemplate().queryForObject("MallHistory.getPayHistoryByOrderNo", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<MallHistory> getPayHistoryList() {
		return this.getSqlMapClientTemplate().queryForList("MallHistory.getPayHistoryList");
	}
	
	public boolean isPayHistoryExistByOrderNo(String orderNo){
		Object numobject = this.getSqlMapClientTemplate().queryForObject("MallHistory.getPayHistoryByOrderNo", orderNo);
		
		if(numobject!=null){
			return true;
		}else{
			return false;
		}
	}
	
	/**根据交易号查询订单信息*/
	public MallHistory getPayHistoryByTransactionID(String transaction_id)
	{
		return (MallHistory)this.getSqlMapClientTemplate().queryForObject("MallHistory.getPayHistoryByTransactionID", transaction_id);
	}
	
	
	/**
	 * 根据玩家编号获得总共充值数
	 * @param playerID
	 * @return
	 */
	public Integer getOnlyPayMoneyAll(String playerID){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		return  (Integer)this.getSqlMapClientTemplate().queryForObject("MallHistory.getOnlyPayMoneyAll", params);
	}
	
	/**
	 * 获取指定时间充值的玩家数
	 * @param playerID
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public Integer getOnlyPayMoneyByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, Date endTime, Integer payState){
		Map<String, Object> params = new HashMap<String, Object>();
		if(flagAccess!=null&&flagAccess!=-1){
			params.put("flagAccess", flagAccess);
		}
		
		params.put("endTime", endTime);
		params.put("playerID", playerID);
		params.put("beginTime", beginTime);
		if(payState != -1)
		{
			params.put("payState", payState);
		}
		if (order != null&&order.intValue()==1) {
			params.put("order", "ASC");
		} else {
			params.put("order", "DESC");
		}
		return  (Integer)this.getSqlMapClientTemplate().queryForObject("MallHistory.getOnlyPayMoneyByTime", params);
	}
	
	public List<MallHistory> getPayHistoryListByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, 
			Date endTime, Integer beginNum,Integer onePageNum,Integer payState){
		Map<String, Object> params = new HashMap<String, Object>();
		if(flagAccess!=null&&flagAccess!=-1){
			params.put("flagAccess", flagAccess);
		}
		params.put("endTime", endTime);
		params.put("beginNum", beginNum);
		params.put("pageNum", onePageNum);
		params.put("playerID", playerID);
		params.put("beginTime", beginTime);
		if(payState != -1)
		{
			params.put("payState", payState);
		}
		if (order != null&&order.intValue()==1) {
			params.put("order", "ASC");
		} else {
			params.put("order", "DESC");
		}
		
		return this.getSqlMapClientTemplate().queryForList("MallHistory.searchPayHistoryList",params);
	}
	public Integer getPayMoneySumByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, Date endTime){
		Map<String, Object> params = new HashMap<String, Object>();

		if(flagAccess!=null&&flagAccess!=-1){
			params.put("flagAccess", flagAccess);
		}
		params.put("endTime", endTime);
		params.put("playerID", playerID);
		params.put("beginTime", beginTime);
		if (order != null&&order.intValue()==1) {
			params.put("order", "ASC");
		} else {
			params.put("order", "DESC");
		}
		return  (Integer)this.getSqlMapClientTemplate().queryForObject("MallHistory.getPayMoneySumByTime", params);
	}
	/**分渠道获得一定时间内的充值玩家数  去掉重复玩家*/
	public Integer getPayPlayerNumByTime(Date start ,Date end,int platformType){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("start", start);
		params.put("end", end);
		if(platformType!=-1){
  			params.put("platformType", platformType);
		}
		Integer returnNum = (Integer)this.getSqlMapClientTemplate().queryForObject("MallHistory.getPayPlayerNumByTime", params);
		if(returnNum==null){
			returnNum=0;
		}
		return  returnNum;
	}
	/**分渠道 获得一定时间内的充值总数 */
	public Integer getPayTotalNumByTime(Date start ,Date end,int platformType){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("start", start);
		params.put("end", end);
		if(platformType!=-1){
  			params.put("platformType", platformType);
		}
		Integer returnNum =  (Integer)this.getSqlMapClientTemplate().queryForObject("MallHistory.getPayTotalNumByTime", params);
		if(returnNum==null){
			returnNum=0;
		}
		return  returnNum;
		
	}
	
	/**分渠道 统计玩家商品购买情况*/
	public List<ItemOpStaBase> countItemOperationStatisticPay(Date start ,Date end,int platformType){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("start", start);
		params.put("end", end);
		if(platformType!=-1){
  			params.put("platformType", platformType);
		}
		List<ItemOpStaBase> items = this.getSqlMapClientTemplate().queryForList("MallHistory.countItemOperationStatisticPay", params);
		return  items;
	}
}