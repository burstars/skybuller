package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.WelFare;
import com.chess.common.dao.IWelFareDAO;
 
 

public class WelFareDAO extends SqlMapClientDaoSupport implements IWelFareDAO 
{

	public List<WelFare> getAll() {
		// TODO Auto-generated method stub
		List<WelFare> bases = this.getSqlMapClientTemplate().queryForList("WelFare.getAll");
    	
    	return bases;
	}

	public WelFare getWelFareById(int id) {
		// TODO Auto-generated method stub
		WelFare base = (WelFare) this.getSqlMapClientTemplate().queryForObject("WelFare.getwelfarebyid",id);
    	
    	return base;
	}
	
   public List<WelFare> getFirstGl(){
	   List<WelFare> bases = this.getSqlMapClientTemplate().queryForList("WelFare.getfirstGl");
	   return bases;
   }
    
    public List<WelFare> getTwoGl(int part_no){
    	 List<WelFare> bases = this.getSqlMapClientTemplate().queryForList("WelFare.getTwoGl",part_no);
  	   	return bases;
    }
}