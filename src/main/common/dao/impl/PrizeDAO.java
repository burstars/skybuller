package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.Prize;
import com.chess.common.dao.IPrizeDAO;

public class PrizeDAO extends SqlMapClientDaoSupport implements IPrizeDAO {

	public List<Prize> getPrizeList() {
              
//		Prize prize =new Prize();
//		prize.setCount(5000);
//		prize.setImageName("1234");
//		prize.setProbability(50000);
//		prize.setPrizeName("1234");
//      this.getSqlMapClientTemplate().insert("Prize.insert",prize);
		
		return this.getSqlMapClientTemplate().queryForList("Prize.getList");
	}

	public void insert(Prize prize) {
		
		this.getSqlMapClientTemplate().insert("Prize.insert",prize);
	}

	public void deleteByID(int id) {
		this.getSqlMapClientTemplate().delete("Prize.deleteByID",id);
		
	}

	public Prize getPrizeByID(int id) {
		 
		return (Prize) this.getSqlMapClientTemplate().queryForObject("Prize.getByID",id);
	}

	public void update(Prize p) {
		this.getSqlMapClientTemplate().update("Prize.update",p);
		
	}

 


}
