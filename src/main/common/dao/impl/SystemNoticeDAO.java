package com.chess.common.dao.impl;



import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.SystemNotice;
import com.chess.common.dao.ISystemNoticeDAO;


public class SystemNoticeDAO extends SqlMapClientDaoSupport implements ISystemNoticeDAO {

	
	public void createSystemNotice(SystemNotice msg) {		
		this.getSqlMapClientTemplate().insert("SystemNotice.createSystemNotice", msg);
	}
	
	
	public SystemNotice getSystemNotice() 
	{			
		SystemNotice SysMsgRecords = (SystemNotice)this.getSqlMapClientTemplate().queryForObject("SystemNotice.getSystemNotice");
		
		return SysMsgRecords;
	}
	
	public int updateSystemNotice(SystemNotice notice)
	{
		return this.getSqlMapClientTemplate().update("SystemNotice.updateSystemNotice", notice);
	}
	//获取固定跑马灯
	public SystemNotice getForeverMsg() 
	{		
		Object res=this.getSqlMapClientTemplate().queryForObject("SystemNotice.getForeverMsg");
		if (res != null){
			SystemNotice SysMsgRecords = (SystemNotice)res;
			return SysMsgRecords;
		}else{
			return null;
		}
	}

//获取全部消息数据-lxw20180903
	public List<SystemNotice> getNoticeAll() {
		List<SystemNotice> res=this.getSqlMapClientTemplate().queryForList("SystemNotice.getNoticeAll");
		return res;
	}


	@Override
	public void deleteSystemNotice(String type) {
		this.getSqlMapClientTemplate().delete("SystemNotice.deleteSystemNotice", type);	
	}
	
}
