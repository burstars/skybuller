package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.oss.OssMonitor;
import com.chess.common.dao.IOssMonitorDAO;

public class OssMonitorDAO extends SqlMapClientDaoSupport implements IOssMonitorDAO{

	public void createOssMonitor(OssMonitor ossMonitor) {
		this.getSqlMapClientTemplate().insert("OssMonitor.createOssMonitor", ossMonitor);
	}

	public void updateOssMonitor(OssMonitor ossMonitor) {
		this.getSqlMapClientTemplate().update("OssMonitor.updateOssMonitor", ossMonitor);
	}

	public void deleteOssMonitorByID(Integer ossMonitorID) {
		this.getSqlMapClientTemplate().delete("OssMonitor.deleteOssMonitorByID", ossMonitorID);
	}

	public OssMonitor getOssMonitorByID(Integer ossMonitorID) {
		return (OssMonitor)this.getSqlMapClientTemplate().queryForObject("OssMonitor.getOssMonitorByID", ossMonitorID);
	}
	

	@SuppressWarnings("unchecked")
	public List<OssMonitor> getOssMonitorList() {
		return this.getSqlMapClientTemplate().queryForList("OssMonitor.getOssMonitorList");
	}

	
	public List<OssMonitor> getOssMonitorMinutesListByMinute(Long begin, Long end){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("begin", begin);
		params.put("end", end);
		return getSqlMapClientTemplate().queryForList("OssMonitor.getOssMonitorMinutesListByMinute", params);
	}

}
