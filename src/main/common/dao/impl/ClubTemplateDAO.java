package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import bsh.This;

import com.chess.common.bean.ClubTemplate;
import com.chess.common.bean.WelFare;
import com.chess.common.dao.IClubTemplate;
import com.chess.common.dao.IWelFareDAO;
 
 

public class ClubTemplateDAO extends SqlMapClientDaoSupport implements IClubTemplate 
{

	public void delate(int id) {
		// TODO Auto-generated method stub
		this.getSqlMapClientTemplate().delete("ClubTemplate.delate",id);
	}

	public void delateAll(int clubCode) {
		// TODO Auto-generated method stub
		this.getSqlMapClientTemplate().delete("ClubTemplate.delateAll",clubCode);
	}

	public List<ClubTemplate> getAll(int clubCode) {
		// TODO Auto-generated method stub
		List<ClubTemplate> clubTemplates = this.getSqlMapClientTemplate().queryForList("ClubTemplate.getAll",clubCode);
		return clubTemplates;
	}

	public void insert(ClubTemplate clubTemplate) {
		// TODO Auto-generated method stub
		this.getSqlMapClientTemplate().insert("ClubTemplate.insert",clubTemplate);
	}

	public void update(ClubTemplate clubTemplate) {
		// TODO Auto-generated method stub
		this.getSqlMapClientTemplate().update("ClubTemplate.update",clubTemplate);
	}

	public ClubTemplate getClubTemplate(int clubCode,int tempIndex) {
		// TODO Auto-generated method stub
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("clubCode", clubCode);
		map.put("tempIndex", tempIndex);
		ClubTemplate clubTemplate = (ClubTemplate) this.getSqlMapClientTemplate().queryForObject("ClubTemplate.getClubTemplate",map);
		return clubTemplate;
	}

	
}