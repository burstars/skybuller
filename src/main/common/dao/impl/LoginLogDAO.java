package com.chess.common.dao.impl;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.UUIDGenerator;
import com.chess.common.bean.LoginLog;
import com.chess.common.bean.LoginLogInfo;
import com.chess.common.dao.ILoginLogDAO;
import com.chess.common.service.ISystemConfigService;


/**
 * 用户登录 日志DAO 每天记录一张表 
 * @author 
 * @since  
 */
public class LoginLogDAO extends SqlMapClientDaoSupport implements ILoginLogDAO {
	
	private static String lastTableSuffixName = "";
	protected static Format format = new SimpleDateFormat("yyyyMMdd");
	
	
	/**
	 * 创建一张表
	 */
	private void createTable(String tableNameSuffix) {
		this.getSqlMapClientTemplate().insert("LoginLog.createTable",tableNameSuffix);
		
	}
	public String createLoginLog(LoginLog loginLog) {
		if(lastTableSuffixName.equals(loginLog.getTableNameSuffix())==false){// 
			createTable(loginLog.getTableNameSuffix());
			lastTableSuffixName=loginLog.getTableNameSuffix();
		}
		loginLog.setLoginLogID(UUIDGenerator.generatorUUID());
		this.getSqlMapClientTemplate().insert("LoginLog.createLoginLog", loginLog);
		return loginLog.getLoginLogID();
	}
	/**按表获得玩家最后登录记录*/
	private LoginLog getPlayerLastLoginLog(String playerID,String lastTableSuffixName){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("playerID", playerID);
		params.put("lastTableSuffixName", lastTableSuffixName);
		return (LoginLog)this.getSqlMapClientTemplate().queryForObject("LoginLog.getPlayerLastLoginLog", params);
	}
	
	/**更新玩家下线时间*/
	public void updatePlayerLoginOutTime(String loginLogID,String TableSuffixName,Date loginOutTime){
		if(lastTableSuffixName.equals(TableSuffixName)==false){
			return;
		}
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("loginLogID", loginLogID);
		params.put("lastTableSuffixName", lastTableSuffixName);
		params.put("loginOutTime", loginOutTime);
		this.getSqlMapClientTemplate().update("LoginLog.updatePlayerLoginOutTime", params);
	}
	
	
	/**获得所有没有下线时间的记录,即登出时间为null  每天凌晨调度处理用*/
	private List<LoginLog> getLoginLogNoLoginOut(String lastTableSuffixName){
		
		
		String checkTableName = "t_login_log_"+lastTableSuffixName;
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		Map<String, String> map = new HashMap<String, String>();
		map.put("tableName", checkTableName);
		map.put("DB", cfgService.getPara(8052).getValueStr());
		String tableName = (String)this.getSqlMapClientTemplate().queryForObject("LoginLog.checkTableIsExists", map);
		if(tableName!=null){
			return this.getSqlMapClientTemplate().queryForList("LoginLog.getLoginLogNoLoginOut", lastTableSuffixName);
		}else {
			return null;
		}
		
	}
	
	/**获得玩家今天最后一次登录的记录*/
	public  LoginLog getPlayerLastLoginLogToday(String playerID){
		if("".equals(lastTableSuffixName)){
			return null;
		}else {
			return this.getPlayerLastLoginLog(playerID, lastTableSuffixName);
		}
	}
	/**昨天登录的玩家一直没下线的情况，处理下  昨天没下线的玩家默认在昨天的23：59:59 统一下线*/
	public void repairYesTodayNoLoginOut(){
		Date today = DateService.getCurrentUtilDate();
		Date todayLastTime = DateService.getCurrentDayLastUtilDate();
		Date yestoday = DateService.dateIncreaseByDay(today, -1);
		Date yestodayLastTime = DateService.dateIncreaseByDay(todayLastTime, -1);
		String yestodayTableSuffixName = DateService.getTableSuffixByTypeAndDate("",DateService.DATE_BY_DAY,yestoday);
		List<LoginLog> yesTodayNoLoginOut = this.getLoginLogNoLoginOut(yestodayTableSuffixName);
		if(yesTodayNoLoginOut!=null&&yesTodayNoLoginOut.size()>0){
			for(LoginLog log:yesTodayNoLoginOut ){
				this.updatePlayerLoginOutTime(log.getLoginLogID(), yestodayTableSuffixName, yestodayLastTime);
			}
		}
	}
	/**按时间统计玩家的登录情况*/
	public List<LoginLogInfo> countPlayerLoginInfoByTime(Date time,int platformType){
		String  tableSuffixName = lastTableSuffixName;
		if(time!=null){//查询具体某一天的情况
			tableSuffixName = DateService.getTableSuffixByTypeAndDate("", DateService.DATE_BY_DAY, time);
		}
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("lastTableSuffixName", tableSuffixName);
		if(platformType!=-1){
			params.put("platformType", platformType);
		}
		String checkTableName = "t_login_log_"+tableSuffixName;
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		Map<String, String> map = new HashMap<String, String>();
		map.put("tableName", checkTableName);
		map.put("DB", cfgService.getPara(8052).getValueStr());
		
		String tableName = (String)this.getSqlMapClientTemplate().queryForObject("LoginLog.checkTableIsExists", map);
		if(tableName!=null){
			return this.getSqlMapClientTemplate().queryForList("LoginLog.countPlayerLoginInfoByTime", params);
		}else {
			return null;
		}
		
		

	}
	/**统计在某个时间段注册的玩家在某天登录的去重人数*/
	public Integer countLoginNumByRegisterTime(Date time ,Date regStartTime,Date regEndTime,int platformType){
		String  tableSuffixName  = DateService.getTableSuffixByTypeAndDate("", DateService.DATE_BY_DAY, time);
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("lastTableSuffixName", tableSuffixName);
		params.put("regStartTime", regStartTime);
		params.put("regEndTime", regEndTime);
		
		String checkTableName = "t_login_log_"+tableSuffixName;
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		Map<String, String> map = new HashMap<String, String>();
		map.put("tableName", checkTableName);
		map.put("DB", cfgService.getPara(8052).getValueStr());
		String tableName = (String)this.getSqlMapClientTemplate().queryForObject("LoginLog.checkTableIsExists", map);
		if(tableName!=null){
			if(platformType!=-1){
				params.put("platformType", platformType);
				return (Integer)this.getSqlMapClientTemplate().queryForObject("LoginLog.countLoginNumByRegisterTime", params);
			}else{
				return (Integer)this.getSqlMapClientTemplate().queryForObject("LoginLog.countLoginNumByRegisterTimeAll", params);
			}
			
		}else {
			return null;
		}
	}
	
}
