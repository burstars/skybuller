package com.chess.common.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.AvgPeopleOnline;
import com.chess.common.dao.IAvgPeopleOnlineDAO;

public class AvgPeopleOnlineDAO extends SqlMapClientDaoSupport implements IAvgPeopleOnlineDAO {

	

	public List<AvgPeopleOnline> getList(Date start ,Date end){
		Map<String ,Object> params = new HashMap<String,Object>();
		params.put("start", start);
		params.put("end", end);
		return this.getSqlMapClientTemplate().queryForList("AvgPeopleOnline.getList", params);
	}



	public void insert(AvgPeopleOnline a) {
		this.getSqlMapClientTemplate().insert("AvgPeopleOnline.insert", a);

	}

	public Integer getMaxPeoNums(Date start ,Date end) {
		Map<String ,Object> params = new HashMap<String,Object>();
		params.put("start", start);
		params.put("end", end);
		return (Integer)this.getSqlMapClientTemplate().queryForObject("AvgPeopleOnline.getMaxPeopleNums",params);
	}

	public Float getAvgPeoNums(Date start ,Date end) {
		
		int x=getTotal(start,end);
		int y=getTotalPeo(start,end);
		
		float temp=0;
		if(x!=0){
			temp=y/x;
		}
		
		return temp;
	}

	public Integer getTotalPeo(Date start ,Date end) {
		Map<String ,Object> params = new HashMap<String,Object>();
		params.put("start", start);
		params.put("end", end);
		
		return (Integer) this.getSqlMapClientTemplate().queryForObject("AvgPeopleOnline.countTotalPeo",params);
	}
	public Integer getTotal(Date start ,Date end) {
		Map<String ,Object> params = new HashMap<String,Object>();
		params.put("start", start);
		params.put("end", end);
		return (Integer)this.getSqlMapClientTemplate().queryForObject("AvgPeopleOnline.countTotal",params);
	}
	/**获得指定渠道的在线数据 分页*/
	public List<AvgPeopleOnline> getOnlineDateByPlatformByPage(Date start ,Date end, Integer beginNum,Integer onePageNum,int platformType){
		Map<String ,Object> params = new HashMap<String,Object>();
		params.put("start", start);
		params.put("end", end);
		params.put("beginNum", beginNum);
		params.put("pageNum", onePageNum);
		params.put("platformType", platformType);
		return this.getSqlMapClientTemplate().queryForList("AvgPeopleOnline.getOnlineDateByPlatformByPage", params);
	}
	/**获得指定渠道的在线数据记录数*/
	public Integer getOnlineDateNumByPlatform(Date start ,Date end,int platformType){
		Map<String ,Object> params = new HashMap<String,Object>();
		params.put("start", start);
		params.put("end", end);
		params.put("platformType", platformType);
		return (Integer)this.getSqlMapClientTemplate().queryForObject("AvgPeopleOnline.getOnlineDateNumByPlatform",params);
	}

}
