package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.ClubRankingList;
import com.chess.common.dao.IClubRankingListDAO;

/**
 * 俱乐部排名
 * @author  2018-8-29
 */
public class ClubRankingListDAO extends SqlMapClientDaoSupport implements IClubRankingListDAO {

	public void updateGameCount(String playerId, int clubCode, String stateStr,
			int gameCount) {
		if(stateStr == null || "".equals(stateStr))
			stateStr = "1,2,3,4,10";
		String[] aryState = stateStr.split(",");
		for (int i = 0; i < aryState.length; i++) {
			int state = Integer.parseInt(aryState[i]);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("playerId", playerId);
			params.put("clubCode", clubCode);
			params.put("state", state);
			ClubRankingList clubRankingList = (ClubRankingList) this.getSqlMapClientTemplate().queryForObject("ClubRankingList.queryDataByState",params);
			if(clubRankingList == null){
				clubRankingList = new ClubRankingList();
				clubRankingList.setPlayerId(playerId);
				clubRankingList.setClubCode(clubCode);
				clubRankingList.setState(state);
				if(state != 2){
					clubRankingList.setGameCount(gameCount);//昨日数据不维护，但是需要新建
				}
				this.getSqlMapClientTemplate().insert("ClubRankingList.insert",clubRankingList);
			}else{
				if(state == 2){
					continue;//昨日数据不维护，但是需要新建
				}
				params.put("gameCount", clubRankingList.getGameCount() + gameCount);
				this.getSqlMapClientTemplate().update("ClubRankingList.updateGameCount",params);
			}
		}
	}

	public void updateWinnerCount(String playerId, int clubCode, String stateStr,
			int winnerCount) {
		if(stateStr == null || "".equals(stateStr))
			stateStr = "1,2,3,4,10";
		String[] aryState = stateStr.split(",");
		for (int i = 0; i < aryState.length; i++) {
			int state = Integer.parseInt(aryState[i]);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("playerId", playerId);
			params.put("clubCode", clubCode);
			params.put("state", state);
			ClubRankingList clubRankingList = (ClubRankingList) this.getSqlMapClientTemplate().queryForObject("ClubRankingList.queryDataByState",params);
			if(clubRankingList == null){
				clubRankingList = new ClubRankingList();
				clubRankingList.setPlayerId(playerId);
				clubRankingList.setClubCode(clubCode);
				clubRankingList.setState(state);
				if(state != 2){
					clubRankingList.setGameCount(winnerCount);//昨日数据不维护，但是需要新建
				}
				
				this.getSqlMapClientTemplate().insert("ClubRankingList.insert",clubRankingList);
			}else{
				if(state == 2){
					continue;//昨日数据不维护，但是需要新建
				}
				params.put("winnerCount", clubRankingList.getWinnerCount() + winnerCount);
				this.getSqlMapClientTemplate().update("ClubRankingList.updateWinnerCount",params);
			}
		}
	}

	public void updateGunnerCount(String playerId, int clubCode, String stateStr,
			int gunnerCount) {
		if(stateStr == null || "".equals(stateStr))
			stateStr = "1,2,3,4,10";
		String[] aryState = stateStr.split(",");
		for (int i = 0; i < aryState.length; i++) {
			int state = Integer.parseInt(aryState[i]);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("playerId", playerId);
			params.put("clubCode", clubCode);
			params.put("state", state);
			ClubRankingList clubRankingList = (ClubRankingList) this.getSqlMapClientTemplate().queryForObject("ClubRankingList.queryDataByState",params);
			if(clubRankingList == null){
				clubRankingList = new ClubRankingList();
				clubRankingList.setPlayerId(playerId);
				clubRankingList.setClubCode(clubCode);
				clubRankingList.setState(state);
				if(state != 2){
					clubRankingList.setGameCount(gunnerCount);//昨日数据不维护，但是需要新建
				}
				
				this.getSqlMapClientTemplate().insert("ClubRankingList.insert",clubRankingList);
			}else{
				if(state == 2){
					continue;//昨日数据不维护，但是需要新建
				}
				params.put("gunnerCount", clubRankingList.getGunnerCount() + gunnerCount);
				this.getSqlMapClientTemplate().update("ClubRankingList.updateGunnerCount",params);
			}
		}
	}
	
	public void updateFangkaCount(String playerId, int clubCode, String stateStr,
			int fangkaCount) {
		if(stateStr == null || "".equals(stateStr))
			stateStr = "1,2,3,4,10";
		String[] aryState = stateStr.split(",");
		for (int i = 0; i < aryState.length; i++) {
			int state = Integer.parseInt(aryState[i]);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("playerId", playerId);
			params.put("clubCode", clubCode);
			params.put("state", state);
			ClubRankingList clubRankingList = (ClubRankingList) this.getSqlMapClientTemplate().queryForObject("ClubRankingList.queryDataByState",params);
			if(clubRankingList == null){
				clubRankingList = new ClubRankingList();
				clubRankingList.setPlayerId(playerId);
				clubRankingList.setClubCode(clubCode);
				clubRankingList.setState(state);
				if(state != 2){
					clubRankingList.setFangkaCount(fangkaCount);//昨日数据不维护，但是需要新建
				}
				
				this.getSqlMapClientTemplate().insert("ClubRankingList.insert",clubRankingList);
			}else{
				if(state == 2){
					continue;//昨日数据不维护，但是需要新建
				}
				params.put("fangkaCount", clubRankingList.getFangkaCount() + fangkaCount);
				this.getSqlMapClientTemplate().update("ClubRankingList.updateFangkaCount",params);
			}
		}
	}

	public List<ClubRankingList> getClubRankingList(int clubCode, int state) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("clubCode", clubCode);
		params.put("state", state);
		return this.getSqlMapClientTemplate().queryForList("ClubRankingList.getClubRankingList", params);
	}

	public void updateClubRankState(int state, boolean isToday) {
		if(isToday){
//			更新昨天的数据为0，并且状态调整99
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("state", 99);
			params.put("state2", 2);
			this.getSqlMapClientTemplate().update("ClubRankingList.updateClubRankYestoday",params);
//			更新今日数据为昨日数据
			params.put("state", 1);
			this.getSqlMapClientTemplate().update("ClubRankingList.updateClubRankDay",params);
//			更新状态为99的数据为今日数据
			params.put("state", 1);
			params.put("state2", 99);
			this.getSqlMapClientTemplate().update("ClubRankingList.updateClubRankYestoday",params);
		}else {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("state", state);
			this.getSqlMapClientTemplate().update("ClubRankingList.updateClubRankState", params);
		}
		
	}
}
