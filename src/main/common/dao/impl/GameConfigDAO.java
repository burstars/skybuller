package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.GameConfig;
import com.chess.common.dao.IGameConfigDAO;

/**
 * 麻将配置加载
 *
 */
public class GameConfigDAO extends SqlMapClientDaoSupport implements IGameConfigDAO {
	
	/**
	 * 获取全部麻将游戏配置
	 */
	public List<GameConfig> getAllGameConfigs() {
    	return this.getSqlMapClientTemplate().queryForList("GameConfig.getAllGameConfigs");
	}
	
}
