package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.oss.OssMenu;
import com.chess.common.bean.oss.OssRoleMenu;
import com.chess.common.dao.IOssRoleMenuDAO;

public class OssRoleMenuDAO extends SqlMapClientDaoSupport implements IOssRoleMenuDAO{

	public void createOssRoleMenu(OssRoleMenu ossRoleMenu) {
		this.getSqlMapClientTemplate().insert("OssRoleMenu.createOssRoleMenu", ossRoleMenu);
	}

	public Integer deleteOssRoleMenu(OssRoleMenu ossRoleMenu) {
		return this.getSqlMapClientTemplate().delete("OssRoleMenu.deleteOssRoleMenu",ossRoleMenu);
	}

	public Integer deleteOssRoleMenuByRoleId(Integer roleId) {
		return this.getSqlMapClientTemplate().delete("OssRoleMenu.deleteOssRoleMenuByRoleId", roleId);
	}

	public List<OssMenu> getOssMenuByRoleId(Integer roleId){
		return this.getSqlMapClientTemplate().queryForList("OssRoleMenu.getOssMenuByRoleId",roleId);
	}

	public List<OssRoleMenu> getOssRoleMenuList() {
		return this.getSqlMapClientTemplate().queryForList("OssRoleMenu.getOssRoleMenuList");
	}

	public Integer updateOssRoleMenu(OssRoleMenu ossRoleMenu) {
		return this.getSqlMapClientTemplate().update("OssRoleMenu.updateOssRoleMenu",ossRoleMenu);
	}

	



}
