/**
 VIP战绩记录实现类
 */
package com.chess.common.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.GameVipRecord;
import com.chess.common.dao.IGameVipRecordDAO;

public class GameVipRecordDAO extends SqlMapClientDaoSupport implements IGameVipRecordDAO {

	public void createVipRecord(GameVipRecord record) {
		// TODO Auto-generated method stub
		record.setRecordDate(new Date());
		this.getSqlMapClientTemplate().insert("GameVipRecord.insert", record);
	}

	public List<GameVipRecord> getGameVipRecordByDate(String playerID, Date date) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("recordDate", date);
		return this.getSqlMapClientTemplate().queryForList("GameVipRecord.getGameVipRecordByID",params);
	}

	public List<GameVipRecord> getGameVipRecordByID(String playerID) {
		// TODO Auto-generated method stub
		return this.getSqlMapClientTemplate().queryForList("GameVipRecord.getGameVipRecordByDate", playerID);
	}

}
