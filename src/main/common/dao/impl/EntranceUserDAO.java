package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

 



import com.chess.common.bean.EntranceUser;
import com.chess.common.bean.SensitiveWords;
import com.chess.common.dao.IEntranceUserDAO;
 



public class EntranceUserDAO extends SqlMapClientDaoSupport implements IEntranceUserDAO {

	private   String lastTableSuffixName = "";
 

	private Integer num_records=0;//当前表里面有多少条记录
	private Integer num_tables=0;//当前有多少张表


	public void init()
	{
		Integer num_tb=1;
		do
		{
			
			String tableSuffixName="_"+num_tb;
			
			String table_name=getTableByName(tableSuffixName);
			if(table_name==null || table_name.length()<1)
				break;
			//
			num_tb++;
			//
		}while(true);
		
		//如果一张表都没有就创建一张
		if(num_tb==1)
		{
			num_tables=1;
			lastTableSuffixName="_"+num_tables;
			num_records=0;
			//
			createTable(lastTableSuffixName);
		}else
		{
			num_tables=num_tb-1;
			//
			lastTableSuffixName="_"+num_tables;
			num_records=getUserNum(lastTableSuffixName);
		}
	}
	/**
	 * 创建一条记录
	 */
	public void insert(EntranceUser user) 
	{
		if(num_records>400000)//20w条一张表
		{// 
			num_tables++;
			num_records=1;
			//
			lastTableSuffixName="_"+num_tables;
			//
			createTable(lastTableSuffixName);
		}else
		{
			num_records++;
		}
		//
		user.setTableNameSuffix(lastTableSuffixName);
		this.getSqlMapClientTemplate().insert("EntranceUser.insert", user);

	}
	/**
	 * 创建一张表
	 */
	public void createTable(String tableNameSuffix) 
	{
		this.getSqlMapClientTemplate().insert("EntranceUser.createTable",tableNameSuffix);
		
	}

	public EntranceUser getUserByAccount(String account)
	{
		EntranceUser usr=null;
		//
		for(int i=1;i<=num_tables;i++)
		{
			String tableSuffixName="_"+i;
			//
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("account", account);
			params.put("tableNameSuffix", tableSuffixName);
			usr= (EntranceUser)this.getSqlMapClientTemplate().queryForObject("EntranceUser.getUserByAccount", params);
			//
			if(usr!=null)
			{
				break;
			}
		}
		return usr;
	}
	
	//获取敏感词，
	public SensitiveWords getWords(String word)
	{
		SensitiveWords words=null;
		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("words", word);
		words= (SensitiveWords)this.getSqlMapClientTemplate().queryForObject("SensitiveWords.searchWords", params);
		
		return words;
	}
	
	public EntranceUser getUserByMachineCode(String machineCode)
	{
		EntranceUser usr=null;
		//逐一查找没张表
		for(int i=1;i<=num_tables;i++)
		{
			String tableSuffixName="_"+i;
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("machineCode", machineCode);
			params.put("tableNameSuffix", tableSuffixName);
			logger.debug("machineCode="+machineCode);//System.out.println("machineCode="+machineCode);
			usr= (EntranceUser)this.getSqlMapClientTemplate().queryForObject("EntranceUser.getUserByMachineCode", params);
			
			if(usr!=null)
			{
				break;
			}
		}
		//
		return usr;
	}
	
	public EntranceUser getUserByQQOpenID(String openid)
	{
		EntranceUser usr=null;
		//逐一查找没张表
		for(int i=1;i<=num_tables;i++)
		{
			String tableSuffixName="_"+i;
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("openid", openid);
			params.put("tableNameSuffix", tableSuffixName);
			usr= (EntranceUser)this.getSqlMapClientTemplate().queryForObject("EntranceUser.getUserByQQOpenID", params);
			
			if(usr!=null)
			{
				break;
			}
		}
		//
		return usr;
	}
	
	public EntranceUser getUserByWXOpenID(String openid)
	{
		EntranceUser usr=null;
		//逐一查找没张表
		for(int i=1;i<=num_tables;i++)
		{
			String tableSuffixName="_"+i;
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("openid", openid);
			params.put("tableNameSuffix", tableSuffixName);
			usr= (EntranceUser)this.getSqlMapClientTemplate().queryForObject("EntranceUser.getUserByWXOpenID", params);
			
			if(usr!=null)
			{
				break;
			}
		}
		//
		return usr;
	}
	
	public   String getLastTableSuffixName() {
		return lastTableSuffixName;
	}



	public   void setLastTableSuffixName(String lastTableSuffixName) {
		this.lastTableSuffixName = lastTableSuffixName;
	}
	
	/**查询表是否存在，存在返回名字，不存在返回null**/
	public String getTableByName(String tableNameSuffix)
	{
		try {
			return (String)this.getSqlMapClientTemplate().queryForObject("EntranceUser.getTableByName", tableNameSuffix);

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}


	public Integer getUserNum(String tableNameSuffix)
	{
		return (Integer)this.getSqlMapClientTemplate().queryForObject("EntranceUser.getUserNum", tableNameSuffix);
	}
	
	/**根据昵称查找用户*/
	public EntranceUser getUserByNickName(String nickName) {
		//EntranceUser usr=null;
		List<EntranceUser> users = null;
		
		for(int i=1;i<=num_tables;i++)
		{
			String tableSuffixName="_"+i;
			
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("nickName", nickName);
			params.put("tableNameSuffix", tableSuffixName);

			users = (List<EntranceUser>)this.getSqlMapClientTemplate().queryForList("EntranceUser.getUserByNickName", params);


			//usr= (EntranceUser)this.getSqlMapClientTemplate().queryForObject("EntranceUser.getUserByNickName",params);
			
			if(users.size() > 0)
			{
				break;
			}
		}
		if(users.size() <= 0) {
			return null;
		} else {
			return users.get(0);
		}
	}
	
	/**
	 * 根据微信unionid获取用户
	 * cc modify
	 * 2017-12-5
	 */
	public EntranceUser getUserByWXUnionID(String unionid) {
		EntranceUser usr=null;
		//逐一查找没张表
		for(int i=1;i<=num_tables;i++)
		{
			String tableSuffixName="_"+i;
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("unionid", unionid);
			params.put("tableNameSuffix", tableSuffixName);
			usr= (EntranceUser)this.getSqlMapClientTemplate().queryForObject("EntranceUser.getUserByWXUnionID", params);
			
			if(usr!=null)
			{
				break;
			}
		}
		return usr;
	}





}


