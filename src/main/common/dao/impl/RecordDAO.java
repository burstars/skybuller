package com.chess.common.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.SpringService;
import com.chess.common.StringUtil;
import com.chess.common.bean.RecordBean;
import com.chess.common.bean.RoomRecord;
import com.chess.common.bean.SimpleRecord;
import com.chess.common.dao.IRecordDAO;
import com.chess.common.service.ISystemConfigService;
import com.ibm.icu.text.SimpleDateFormat;

public class RecordDAO extends SqlMapClientDaoSupport implements IRecordDAO {
	
	
	private static String lastTableSuffixName = "";
	private static List<String> lastTablePrefixNameList = new ArrayList<String>();

	/**
	 * 创建表
	 */
	public void createTable(Map<String, String> parmMap) {
		this.getSqlMapClientTemplate().insert("RecordBean.createTable", parmMap);
	}
	/**
	 * 创建一条战绩
	 */
	public void createVipRoomRecord(RecordBean roomRecord) {
		// TODO Auto-generated method stub
		if (lastTableSuffixName.equals(roomRecord.getTableNameSuffix()) == false ){
			lastTablePrefixNameList.clear();
		}
		if (lastTableSuffixName.equals(roomRecord.getTableNameSuffix()) == false || lastTablePrefixNameList.contains(roomRecord.getTableNamePrefix()) == false) {// 
			lastTableSuffixName = roomRecord.getTableNameSuffix();
			if(lastTablePrefixNameList.contains(roomRecord.getTableNamePrefix()) == false){
				lastTablePrefixNameList.add(roomRecord.getTableNamePrefix());
			}
			Map<String, String> parmMap = new HashMap<String, String>();
			parmMap.put("tableNamePrefix", roomRecord.getTableNamePrefix());
			parmMap.put("tableNameSuffix", roomRecord.getTableNameSuffix());
			createTable(parmMap);
		}
		roomRecord.setEnd_Time(new Date());
		this.getSqlMapClientTemplate().insert("RecordBean.insert", roomRecord);
	}
	/**
	 * 通过玩家id获取七天内所有战绩
	 */
	public List<SimpleRecord> getMyVipRoomRecord(String dateStr, String playerID, String gameID) {
		List<String> tableNames = new ArrayList<String>();
		long timestamp = new Date().getTime();
		
		if(dateStr == null || dateStr.length() < 2)
		{
			// 获取过去一周的表名
			for (int i = 0; i < 7; i++) {
				String date = StringUtil.timestamp2String(timestamp);
				String tableName = "t_vip_room_record_standard__" + gameID + "__" + date;
				if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
					tableNames.add(tableName);
				}
				timestamp = timestamp - 24 * 60 * 60 * 1000;
			}
		}
		else
		{
			String tableName = "t_vip_room_record_standard__" + gameID + "__" + dateStr;
			if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
				tableNames.add(tableName);
			}
		}
		
		if (tableNames.size() == 0) {
			return null;
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames);
		params.put("tableNames1", tableNames);
		params.put("player_id", playerID);
		List<SimpleRecord> records = new ArrayList<SimpleRecord>();
		List<RecordBean> recordBeans = this.getSqlMapClientTemplate().queryForList("RecordBean.getMyVipRoomRecord", params);
		Map<String,List<RecordBean>> roomRecord = new LinkedHashMap<String,List<RecordBean>>(); 
		for(RecordBean record : recordBeans) {
			String roomId = record.getRoom_Id();
			List<RecordBean> rbs = roomRecord.get(roomId);
			if(rbs == null) {
				rbs = new ArrayList<RecordBean>();
				roomRecord.put(roomId, rbs);
			}
			rbs.add(record);
		}
		for(String key : roomRecord.keySet()) {
			SimpleRecord simpleRecord = new SimpleRecord();
			for (RecordBean rb : roomRecord.get(key)) {
				RoomRecord rr = new RoomRecord();
				rr.playerId = rb.getPlayer_Id();
				rr.playerIndex = rb.getPlayer_Index();
				rr.playerName = rb.getPlayer_Name();
				rr.score = rb.getScore();
				simpleRecord.room_Indsx = rb.getRoom_Index();
				simpleRecord.gameStartTime =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rb.getStart_Time());
				simpleRecord.endWay = rb.getEnd_Way();
				simpleRecord.hostIndex = rb.getHost_Index();
				simpleRecord.roomRecords.add(rr);
			}
			records.add(simpleRecord);
		}
		return records;
	}
	
	/**
	 * 更新当前战绩结束时间、结束方法
	 */
	public void updateRecordEndWay(RecordBean record) {
		this.getSqlMapClientTemplate().update("RecordBean.updateRecordEndWay", record);
	}
	/**
	 * 更新玩家分数
	 */
	public void updateRecordScore(RecordBean record){
		this.getSqlMapClientTemplate().update("RecordBean.updateRecordScore", record);
	}
	/**
	 * 检查表是否存在
	 */
	public String checkVipRoomRecordTableIsExists(String tableName) {
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		Map<String, String> map = new HashMap<String, String>();
		map.put("tableName", tableName);
		map.put("DB", cfgService.getPara(8052).getValueStr());
		String trueName = (String) this.getSqlMapClientTemplate().queryForObject("RecordBean.checkTableIsExists", map);
		return trueName;
	}
}