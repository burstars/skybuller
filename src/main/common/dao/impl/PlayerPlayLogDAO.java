package com.chess.common.dao.impl;

import com.chess.common.bean.PlayerPlayLog;
import com.chess.common.dao.IPlayerPlayLogDAO;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import java.util.HashMap;
import java.util.Map;

/**
 */
public class PlayerPlayLogDAO extends SqlMapClientDaoSupport implements IPlayerPlayLogDAO {
    public String createPlayerPlayLog(PlayerPlayLog playerPlayLog) {
        this.getSqlMapClientTemplate().insert("PlayerPlayLog.createPlayerPlayLog", playerPlayLog);
        return playerPlayLog.getPlayerID();
    }

    public void updateVipCountByPlayerID(String playerID, int totalVipCount) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerID", playerID);
        params.put("totalVipCount", totalVipCount);
        this.getSqlMapClientTemplate().update("PlayerPlayLog.updateVipCountByPlayerID", params);
    }

    public void incVipCountByPlayerID(String playerID) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerID", playerID);
        this.getSqlMapClientTemplate().update("PlayerPlayLog.incVipCountByPlayerID", params);
    }

    public void updateNormalCountByPlayerID(String playerID, int totalNormalCount) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerID", playerID);
        params.put("totalNormalCount", totalNormalCount);
        this.getSqlMapClientTemplate().update("PlayerPlayLog.updateNormalCountByPlayerID", params);
    }

    public void incNormalCountByPlayerID(String playerID) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerID", playerID);
        this.getSqlMapClientTemplate().update("PlayerPlayLog.incNormalCountByPlayerID", params);
    }

    public void incCreateVipCountByPlayerID(String playerID) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerID", playerID);
        this.getSqlMapClientTemplate().update("PlayerPlayLog.incCreateVipCountByPlayerID", params);
    }

    public void deletePlayerPlayerLogByPlayerID(String playerID) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerID", playerID);
        this.getSqlMapClientTemplate().delete("PlayerPlayLog.deletePlayerPlayerLogByPlayerID", params);
    }
}
