package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.oss.OssUserRole;
import com.chess.common.dao.IOssUserRoleDAO;

public class OssUserRoleDAO extends SqlMapClientDaoSupport implements IOssUserRoleDAO{

	public void createOssUserRole(OssUserRole ossUserRole) {
		this.getSqlMapClientTemplate().insert("OssUserRole.createOssUserRole", ossUserRole);
	}

	public Integer deleteOssUserRoleByRoleId(Integer ossUserRoleID) {
		return this.getSqlMapClientTemplate().delete("OssUserRole.deleteOssUserRoleByRoleId", ossUserRoleID);
		
	}

	public List getOssUserRoleByRoleID(Integer ossUserRoleID) {
		return this.getSqlMapClientTemplate().queryForList("OssUserRole.getOssUserRoleByRoleID",ossUserRoleID);
	}

	public List<OssUserRole> getOssUserRoleList() {
		return this.getSqlMapClientTemplate().queryForList("OssUserRole.getOssUserRoleList");
	}

	public List getOssUserByRoleUserID(String username) {
		return this.getSqlMapClientTemplate().queryForList("OssUserRole.getOssUserByRoleUserID",username);
	}

	public Integer updateOssUserRole(OssUserRole ossUserRole) {
		return  this.getSqlMapClientTemplate().update("OssUserRole.updateOssUserRole",ossUserRole);
		
	}
	
	public Integer deleteOssUserRoleByUserID(String username){
		return this.getSqlMapClientTemplate().delete("OssUserRole.deleteOssUserRoleByUserID", username);
	}

}
