package com.chess.common.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.UUIDGenerator;
import com.chess.common.bean.oss.OssLog;
import com.chess.common.dao.IOssLogDAO;



public class OssLogDAO extends SqlMapClientDaoSupport implements IOssLogDAO{
	
	public String createOssLog(OssLog ossLog) {
		ossLog.setOssLogID(UUIDGenerator.generatorUUID());
		this.getSqlMapClientTemplate().insert("OssLog.createOssLog", ossLog);
		return ossLog.getOssLogID();
	}

	public void updateOssLog(OssLog ossLog) {
		this.getSqlMapClientTemplate().update("OssLog.updateOssLog", ossLog);
	}

	public void deleteOssLogByID(String ossLogID) {
		this.getSqlMapClientTemplate().delete("OssLog.deleteOssLogByID", ossLogID);
	}

	public OssLog getOssLogByID(String ossLogID) {
		return (OssLog)this.getSqlMapClientTemplate().queryForObject("OssLog.getOssLogByID", ossLogID);
	}
	
	@SuppressWarnings("unchecked")
	public List<OssLog> getOssLogList() {
		return this.getSqlMapClientTemplate().queryForList("OssLog.getOssLogList");
	}
	
	
	public Integer getOssLogListCount(Date beginTime,Date endTime,String optionType){
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("beginTime", beginTime);
		params.put("endTime", endTime);
		params.put("optionType", optionType);
		return (Integer)this.getSqlMapClientTemplate().queryForObject("OssLog.getOssLogListCount",params);

	}

	@SuppressWarnings("unchecked")
	public List<OssLog> getOssLogList(Date beginTime,Date endTime,String optionType,Integer beginNum,Integer pageNum){
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("beginTime", beginTime);
		params.put("endTime", endTime);
		params.put("optionType", optionType);
		params.put("beginNum", beginNum);
		params.put("pageNum", pageNum);
		return this.getSqlMapClientTemplate().queryForList("OssLog.getOssLogList",params);
		
	}


}
