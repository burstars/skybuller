package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.Message;
import com.chess.common.dao.IMessageDAO;

public class MessageDAO extends SqlMapClientDaoSupport implements IMessageDAO{

	public void createMessage(Message message) {
		this.getSqlMapClientTemplate().insert("Message.createMessage",message);
	}

	public int deleteMessage(String playerID) {
	   return this.getSqlMapClientTemplate().delete("Message.deleteMessage", playerID);
	}

	public int updateMessageState(String playerID) {
		return this.getSqlMapClientTemplate().delete("Message.updateMessageState", playerID);
	}
	
	 /**取得信息列表*/
	 public List<Message> getMessageByPlayerID(String playerID)
	 {
		 return this.getSqlMapClientTemplate().queryForList("Message.getMessageByPlayerID", playerID);
	 }

}
