package com.chess.common.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.TClub;
import com.chess.common.dao.ITClubDao;

/**
 * 俱乐部
 * @author  2018-8-29
 */
public class TClubDAO extends SqlMapClientDaoSupport implements ITClubDao {

	public List<TClub> getMyClubs(String playerId){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("createPlayerId", playerId);
		return this.getSqlMapClientTemplate().queryForList("TClub.getMyClubByPlayerId",playerId);
	}

	public List<TClub> getJoinClubs(String playerId){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("createPlayerId", playerId);
		return this.getSqlMapClientTemplate().queryForList("TClub.getJoinClubByPlayerId",playerId);
	}

	public TClub createClub(TClub club) {
		int max = 8999;
		int min = 1000;
		Random random = new Random();
		int clubCode= random.nextInt(max) + min;
		club.setClubCode(randomClubCode(clubCode));
		String time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		club.setCreateTime(time);
		this.getSqlMapClientTemplate().insert("TClub.insertTClub",club);
		return club;
	}
	
	private int randomClubCode(int clubCode){
		TClub club = (TClub) this.getSqlMapClientTemplate().queryForObject("TClub.getClubByClubCode",clubCode);
		if(club == null){
			return clubCode;
		}
		int max = 8999;
		int min = 1000;
		Random random = new Random();
		clubCode= random.nextInt(max) + min;
		return randomClubCode(clubCode);
	}

	public void updateClubState(int clubCode, int clubState) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("clubCode", clubCode);
		params.put("clubState", clubState);
		this.getSqlMapClientTemplate().update("TClub.updateClubState",params);
	}

	public List<TClub> getAllClubs(String playerId) {
		List<TClub> rtnClubs = new ArrayList<TClub>();
		List<TClub> clubs = getMyClubs(playerId);
		if(clubs != null){
			rtnClubs.addAll(clubs);
		}
		List<TClub> joinClubs = getJoinClubs(playerId);
		if(joinClubs != null){
			rtnClubs.addAll(joinClubs);
		}
		return rtnClubs;
	}

	public List<TClub> getAllClubs() {
		return this.getSqlMapClientTemplate().queryForList("TClub.getAllClubs");
	}

	public TClub getClubByClubCode(int clubCode,String memberId) {
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("clubCode",clubCode);
		params.put("memberId",memberId);
		return (TClub) this.getSqlMapClientTemplate().queryForObject("TClub.getClubByClubCode2",params);
	}
	
	public TClub getClubByClubCode(int clubCode) {
		return (TClub) this.getSqlMapClientTemplate().queryForObject("TClub.getClubByClubCode",clubCode);
	}
	public List<TClub> getClubBySearch(String searchTxt) {
		return (List<TClub>) this.getSqlMapClientTemplate().queryForList("TClub.getClubBySearch",searchTxt);
	}

	public void updateClubFangkaNum(int clubCode, int fangkaNum) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("clubCode", clubCode);
		params.put("fangkaNum", fangkaNum);
		this.getSqlMapClientTemplate().update("TClub.updateClubFangkaNum",params);
	}
	
	public void updateClubLevel(int clubCode, String roomLevel) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("clubCode", clubCode);
		params.put("roomLevel", roomLevel);
		this.getSqlMapClientTemplate().update("TClub.updateClubLevel",params);
	}
	
	public void addClubGold(int clubCode, int goldNum){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("clubCode", clubCode);
		params.put("goldNum", goldNum);
		this.getSqlMapClientTemplate().update("TClub.addClubGold",params);
	}
	
}
