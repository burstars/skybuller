package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.GoodNumber;
import com.chess.common.dao.IGoodNumberDAO;

public class GoodNumberDAO extends SqlMapClientDaoSupport implements IGoodNumberDAO{

	public void createGoodNumber(GoodNumber goodNumber) {
		this.getSqlMapClientTemplate().insert("GoodNumber.insert", goodNumber);
	}

	public int getAllGoodNumberTotal() {
		Integer row = (Integer)this.getSqlMapClientTemplate().queryForObject("GoodNumber.getAllGoodNumberTotal");
    	return row;
	}
	
	public List<GoodNumber> getAllGoodNumber(String begin,String end) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("start", begin);
		params.put("end", end);
		List<GoodNumber> numbers = this.getSqlMapClientTemplate().queryForList("GoodNumber.getAllGoodNumber",params);
    	return numbers;
	}

	public void deleteGoodNumberByID(int ID) {
		this.getSqlMapClientTemplate().delete("GoodNumber.deleteGoodNumberByID", ID);
	}
	
	public int getGoodNumberByNumberTotal(String number){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("number",number);
		Integer row = (Integer)this.getSqlMapClientTemplate().queryForObject("GoodNumber.getGoodNumberByNumberTotal",params);
		
		return row.intValue();
	} 
	
	public int getGoodNumberTotal(String number){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("number",number);
		Integer row = (Integer)this.getSqlMapClientTemplate().queryForObject("GoodNumber.getGoodNumberTotal",params);
		
		return row.intValue();
	}
	
	 public GoodNumber getAnGoodNumberByNumber(String number)
	 {
		 Map<String, Object> params = new HashMap<String, Object>();
			params.put("number",number);
		 return (GoodNumber)this.getSqlMapClientTemplate().queryForObject("GoodNumber.getAnGoodNumberByNumber", params);
	 }
	
	public List<GoodNumber> getGoodNumberByNumber(String number,String start,String end){
		 Map<String, Object> params = new HashMap<String, Object>();
			params.put("number",number);
			params.put("start", start);
			params.put("end", end);
		 return this.getSqlMapClientTemplate().queryForList("GoodNumber.getGoodNumberByNumber", params);
	}
	
	public GoodNumber getGoodNumberByPlayerId(String playerId) {
		return (GoodNumber)this.getSqlMapClientTemplate().queryForObject("GoodNumber.getGoodNumberByPlayerID", playerId);
	}

	public void update(GoodNumber goodNumber) {
		this.getSqlMapClientTemplate().update("GoodNumber.update",goodNumber);
	}


}
