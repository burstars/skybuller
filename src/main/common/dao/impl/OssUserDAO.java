package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.oss.OssUser;
import com.chess.common.dao.IOssUserDAO;

public class OssUserDAO extends SqlMapClientDaoSupport implements IOssUserDAO {

	public void createOssUser(OssUser ossUser) {
		this.getSqlMapClientTemplate().insert("OssUser.createOssUser", ossUser);
	}

	public void updateOssUserLastLoginInfo(OssUser ossUser) {
		this.getSqlMapClientTemplate().update("OssUser.updateOssUserLastLoginInfo", ossUser);
	}

	public void updateOssUserPassword(OssUser ossUser) {
		this.getSqlMapClientTemplate().update("OssUser.updateOssUserPassword", ossUser);
	}
	
	public OssUser getOssUserByID(String username) {
		return (OssUser)this.getSqlMapClientTemplate().queryForObject("OssUser.getOssUserByID", username);
	}
	
	public List<OssUser> getOssUserList(){
		return this.getSqlMapClientTemplate().queryForList("OssUser.getOssUserList");
	}
	
	public Integer deleteOssUserByID(String username){
		return this.getSqlMapClientTemplate().delete("OssUser.deleteOssUserByID",username);
	}
	
	public Integer updateOssUser(OssUser ossUser){
		return this.getSqlMapClientTemplate().update("OssUser.updateOssUser", ossUser);
	}

}
