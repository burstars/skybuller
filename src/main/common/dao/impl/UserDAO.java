package com.chess.common.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.MD5Service;
import com.chess.common.bean.User;
import com.chess.common.dao.IUserDAO;
import com.chess.common.framework.GameContext;

public class UserDAO extends SqlMapClientDaoSupport implements IUserDAO {

	private static Logger logger = LoggerFactory.getLogger(UserDAO.class);

	public String createPlayer(User player) {
		//
		String md5 = MD5Service.encryptString(player.getGold()
				+ GameContext.verifyKey);
		player.setGoldVerify(md5);
		//
		player.setRegisTime(new Date());
		player.setLastLoginTime(player.getRegisTime());
		player.setOfflineTime(player.getRegisTime());
		this.getSqlMapClientTemplate().insert("User.insert", player);

		return "";
	}

	public User getPlayerByID(String playerID) {
		User player = (User) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerByID", playerID);

		return player;
	}

	public User getPlayerByIDAndGameID(String playerID, String gameID) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("playerID", playerID);
		if (gameID.equals("")) {
			map.put("DB", "t_user");
		} else {
			map.put("DB", GameContext.getPayConfig(gameID) + ".t_user");
		}

		return (User) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerByIDAndGameID", map);
	}

	public void updatePlayerGold(String playerID, int gold) {
		String md5 = MD5Service.encryptString(gold + GameContext.verifyKey);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("gold", gold);
		params.put("goldVerify", md5);
		this.getSqlMapClientTemplate().update("User.updatePlayerGold", params);
	}

	//
	public void updatePlayerGem(String playerID, int gemNum) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("gemNum", gemNum);

		//
		this.getSqlMapClientTemplate().update("User.updatePlayerGem", params);
	}

	/**
	 * @param User
	 *            用户
	 * */
	public int updatePlayer(User player) {
		return this.getSqlMapClientTemplate().update("User.updatePlayer",
				player);
	}

	/**
	 * 修改密码
	 * 
	 * @author dong
	 * @param playerID
	 *            用户名称
	 * @param password
	 *            密码
	 * */
	public int updatePlayerPassword(String playerID, String password) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("password", password);
		return this.getSqlMapClientTemplate().update(
				"User.updatePlayerPassword", params);
	}

	/** 修改是否可以被对方添加为好友设置 */
	public int updatePlayerCanFriend(String playerID, int canflag) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("canFirend", canflag);
		return this.getSqlMapClientTemplate().update(
				"User.updatePlayerCanFriend", params);
	}

	public void updatePlayerRecord(String playerID, int wons, int loses,
			int escape) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("wons", wons);
		params.put("loses", loses);
		params.put("escape", escape);

		this.getSqlMapClientTemplate()
				.update("User.updatePlayerRecord", params);
	}

	/**
	 * @param playerID
	 *            标示
	 * @param headImg
	 *            头像ID
	 * */
	public void updatePlayerHead(String playerID, int headImg) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("headImg", headImg);

		this.getSqlMapClientTemplate().update("User.updatePlayerHead", params);
	}

	/** 更新玩家类别 */
	public void updatePlayerType(String playerID, int playerType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("playerType", playerType);

		this.getSqlMapClientTemplate().update("User.updatePlayerType", params);
	}

	/** 更新玩手机号码 */
	public void updatePlayerPhoneNumber(String playerID, String phoneNumber) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("phoneNumber", phoneNumber);
		this.getSqlMapClientTemplate().update("User.updatePlayerPhoneNumber",
				params);
	}

	public void updatePlayerIdentifyCard(String playerID, String phoneNumber,
			String identifyCard, String weixinNum, String qqNum, String name,
			String birthday, String address) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("phoneNumber", phoneNumber);
		params.put("identifyCard", identifyCard);
		params.put("weixinNum", weixinNum);
		params.put("qqNum", qqNum);
		params.put("birthday", birthday);
		params.put("address", address);
		params.put("name", name);
		this.getSqlMapClientTemplate().update("User.updatePlayerIdentifyCard",
				params);
	}

	/** 更新玩家位置信息 */
	public void updatePlayerLocation(String playerID, String location) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("location", location);
		this.getSqlMapClientTemplate().update("User.updatePlayerLocation",
				params);
	}

	/** 获取数据库中是否有该手机号码 */
	public int getPhoneNumberCount(String phoneNumber) {
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"User.getPhoneNumberCount", phoneNumber);
	}

	/**
	 * 更新生命值
	 */
	public void updatePlayerLife(String playerID, int life) {

		//
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("life", life);

		//
		//
		this.getSqlMapClientTemplate().update("User.updatePlayerLife", params);
	}

	public void updatePlayerLifeAndGold(String playerID, int gold, int life) {
		//
		String md5 = MD5Service.encryptString(gold + GameContext.verifyKey);

		//
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("life", life);
		params.put("gold", gold);
		params.put("goldVerify", md5);
		//
		//
		this.getSqlMapClientTemplate().update("User.updatePlayerLifeAndGold",
				params);
	}

	// 系统调度，添加生命值
	public void addLifeNoOverflow() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("addNum", 1);
		params.put("mLife", 5);

		//
		//
		this.getSqlMapClientTemplate().update("User.addLifeNoOverflow", params);
	}

	public User getPlayerByAccount(String account) {
		User player = (User) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerByAccount", account);
		return player;
	}

	public User getPlayerByName(String playerName) {
		User player = (User) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerByName", playerName);
		return player;
	}

	public User getPlayerByMachineCode(String machineCode) {
		User player = (User) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerByMachineCode", machineCode);

		return player;
	}

	public void updateContinueLanding(String playerID, int days) {

		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("days", days);
		maps.put("playerID", playerID);
		this.getSqlMapClientTemplate().update("User.updateContinueLanding",
				maps);
	}

	public void updatePlayerTodayBestScore(String playerID, int score) {

		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("score", score);
		maps.put("playerID", playerID);
		this.getSqlMapClientTemplate().update(
				"User.updatePlayerTodayBestScore", maps);
	}

	public void updateSaveTime(String playerID, int saveTime) {

		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("saveTime", saveTime);
		maps.put("playerID", playerID);
		this.getSqlMapClientTemplate().update("User.updateSaveTime", maps);
	}

	public void updatePlayerHistoryBestScore(String playerID, int score) {

		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("score", score);
		maps.put("playerID", playerID);
		this.getSqlMapClientTemplate().update(
				"User.updatePlayerHistoryBestScore", maps);
	}

	public void updatePlayerDiamonds(String playerID, int diamond) {

		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("diamond", diamond);
		maps.put("playerID", playerID);
		this.getSqlMapClientTemplate()
				.update("User.updatePlayerDiamonds", maps);
	}

	public void updateLastLoginTime(String playerID, Date date) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("date", date);
		maps.put("playerID", playerID);
		this.getSqlMapClientTemplate().update("User.updateLastLoginTime", maps);

	}

	public void updateOfflineTime(String playerID, Date date) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("date", date);
		maps.put("playerID", playerID);
		this.getSqlMapClientTemplate().update("User.updateOfflineTime", maps);

	}

	public void updatePlayerLoginMsg(String playerID, int gold,
			Date lastLoginTime, int continueLanding, int luckyDrawsTimes,
			int diamond) {
		Map<String, Object> maps = new HashMap<String, Object>();

		maps.put("playerID", playerID);
		maps.put("gold", gold);
		maps.put("lastLoginTime", lastLoginTime);
		maps.put("continueLanding", continueLanding);
		maps.put("luckyDrawsTimes", luckyDrawsTimes);
		maps.put("diamond", diamond);

		this.getSqlMapClientTemplate()
				.update("User.updatePlayerLoginMsg", maps);
	}

	/** 获得所有玩家信息 */
	public List<User> getAllPlayer() {
		return this.getSqlMapClientTemplate().queryForList("User.getAllPlayer");
	}

	public List<User> getAllPlayerByPage(String startRownum, String endRownum,
			String startTime, String endTime, int order, int queryPlayerType) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("start", startRownum);
		maps.put("end", endRownum);
		maps.put("startTime", startTime);
		maps.put("endTime", endTime);
		maps.put("order", order);

		if (queryPlayerType != 0) {
			maps.put("queryPlayerType", queryPlayerType);
		}

		return this.getSqlMapClientTemplate().queryForList(
				"User.getAllPlayerByPage", maps);
	}

	public int getTotalCount(String startTime, String endTime,
			int queryPlayerType) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("startTime", startTime);
		maps.put("endTime", endTime);
		if (queryPlayerType != 0) {
			maps.put("queryPlayerType", queryPlayerType);
		}
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"User.getTotalCount", maps);
	}

	public void updateVIP(String playerID, int vipExp, int vipLevel) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("playerID", playerID);
		maps.put("vipExp", vipExp);
		maps.put("vipLevel", vipLevel);

		this.getSqlMapClientTemplate().update("User.updateVIP", maps);

	}

	public void clearTodayBestScoreAllPlayer() {
		this.getSqlMapClientTemplate().update(
				"User.clearTodayBestScoreAllPlayer");
	}

	public void updatePlayerIndex(String playerID, int playerIndex) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("playerIndex", playerIndex);

		//
		this.getSqlMapClientTemplate().update("User.updatePlayerIndex", params);
	}

	public User getPlayerByPlayerIndex(int playerIndex) {
		if (playerIndex == 1000) {
			return null;
		}
		return (User) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerByPlayerIndex", playerIndex);
	}

	/** 按渠道获得玩家数目 */
	public int getPlayerNumByPlatformType(int platformType) {
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerNumByPlatformType", platformType);
	}

	/** 按渠道统计晶石剩余数目 */
	public int getGemTotalCount(Integer platformType) {
		Map<String, Object> params = new HashMap<String, Object>();
		if (platformType == -1) {
			platformType = null;
		}
		params.put("platformType", platformType);
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"User.getGemTotalCount", params);
	}

	public int getRegPlayerNumByPlatformType(Date beginTime, Date endTime,
			Integer platformType) {
		Map<String, Object> params = new HashMap<String, Object>();
		if (platformType == -1) {
			platformType = null;
		}
		params.put("platformType", platformType);
		params.put("endTime", endTime);
		params.put("beginTime", beginTime);
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"User.getRegPlayerNumByPlatformType", params);
	}

	/** 根据qqopenid获取用户 */
	public User getPlayerByQQOpenID(String qqOpenID) {
		User player = (User) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerByQQOpenID", qqOpenID);
		return player;
	}

	/** 根据wxopenid获取用户 */
	public User getPlayerByWXOpenID(String qqOpenID) {
		User player = (User) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerByWXOpenID", qqOpenID);
		return player;
	}
	
	/** 
	 * 根据wxunionid获取用户 
	 * cc add 2017-12-5*/
	public User getPlayerByWXUnionID(String unionID) {
		User player = (User) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerByWXUnionID", unionID);
		return player;
	}

	/** 获得自定义头像所有玩家信息,分页 */
	public List<User> getAllCustomPlayerByPage(String startRownum,
			String endRownum, String account) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("start", startRownum);
		maps.put("end", endRownum);
		maps.put("account", account);
		return this.getSqlMapClientTemplate().queryForList(
				"User.getAllCustomPlayerByPage", maps);
	}

	/** 获取自定义头像用户个数 */
	public int getCustomTotalCount(String account) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("account", account);
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"User.getCustomTotalCount");
	}

	/** 根据ID查询所有玩家 */
	public List<User> getPlayersByPlayerIDByPage(String startRownum,
			String endRownum, String account, String startTime, String endTime,
			int order) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("start", startRownum);
		maps.put("end", endRownum);
		maps.put("account", account);
		maps.put("startTime", startTime);
		maps.put("endTime", endTime);
		maps.put("order", order);
		return this.getSqlMapClientTemplate().queryForList(
				"User.getPlayersByPlayerIDByPage", maps);
	}

	/** 根据ID统计玩家数量 */
	public int getPlayersByPlayerIDCount(String account, String startTime,
			String endTime) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("account", account);
		maps.put("startTime", startTime);
		maps.put("endTime", endTime);
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayersByPlayerIDCount", maps);
	}

	/** 根据帐号查询所有玩家 */
	public List<User> getPlayersByPlayerAccountByPage(String startRownum,
			String endRownum, String account, String startTime, String endTime,
			int order) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("start", startRownum);
		maps.put("end", endRownum);
		maps.put("account", account);
		maps.put("startTime", startTime);
		maps.put("endTime", endTime);
		maps.put("order", order);
		return this.getSqlMapClientTemplate().queryForList(
				"User.getPlayersByPlayerAccountByPage", maps);
	}

	/** 根据帐号统计玩家数量 */
	public int getPlayersByPlayerAccountCount(String account, String startTime,
			String endTime) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("account", account);
		maps.put("startTime", startTime);
		maps.put("endTime", endTime);
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayersByPlayerAccountCount", maps);
	}

	/** 根据昵称查询所有玩家 */
	public List<User> getPlayersByPlayerNameByPage(String startRownum,
			String endRownum, String account, String startTime, String endTime,
			int order) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("start", startRownum);
		maps.put("end", endRownum);
		maps.put("account", account);
		maps.put("startTime", startTime);
		maps.put("endTime", endTime);
		maps.put("order", order);
		return this.getSqlMapClientTemplate().queryForList(
				"User.getPlayersByPlayerNameByPage", maps);
	}

	/** 根据昵称统计玩家数量 */
	public int getPlayersByPlayerNameCount(String account, String startTime,
			String endTime) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("account", account);
		maps.put("startTime", startTime);
		maps.put("endTime", endTime);
		return (Integer) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayersByPlayerNameCount", maps);
	}

	public void updatePlayerShareNum(String playerID, int shareNum,
			long shareDate) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("playerID", playerID);
		maps.put("shareNum", shareNum);
		maps.put("shareDate", shareDate);
		this.getSqlMapClientTemplate().update("User.updateShareNum", maps);

	}

	public void updatePlayerUnionID(String playerID, String unionID) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("unionID", unionID);
		map.put("playerID", playerID);
		this.getSqlMapClientTemplate().update("User.updatePlayerUnionID", map);
	}

	/** 更新用户网络头像地址 */
	public void updatePlayerHeadImgUrl(String playerID, String headImgUrl) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("headImgUrl", headImgUrl);
		map.put("playerID", playerID);
		this.getSqlMapClientTemplate().update("User.updatePlayerHeadImgUrl",
				map);
	}

	/** 更新用户昵称 */
	public void updatePlayerName(String playerID, String playerName) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("playerName", playerName);
		map.put("playerID", playerID);
		this.getSqlMapClientTemplate().update("User.updatePlayerName", map);
	}

	/** 更新用户param01列  20161203 */
	public void updatePlayerParam01(String playerID, String param01) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("param01", param01);
		map.put("playerID", playerID);
		this.getSqlMapClientTemplate().update("User.updatePlayerParama01", map);
	}

	// 更新用户代开房间权限
	public void updatePlayerProxyOpenRoom(String playerID, int proxyOpenRoom) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("proxyOpenRoom", proxyOpenRoom);
		this.getSqlMapClientTemplate().update("User.updatePlayerProxyOpenRoom",
				params);
	}
	
	//玩家积分增加
	public void addPlayerCredits(String playerID, int credits ,int totalCredits) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("credits", credits);
		params.put("totalCredits", totalCredits);
		this.getSqlMapClientTemplate().update("User.addPlayerCredit", params);
	}
	
	//玩家花费积分
	public void costPlayerCredits(String playerID, int credits) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("credits", credits);
		this.getSqlMapClientTemplate().update("User.costPlayerCredit", params);
	}

	public void updatePlayerSex(String playerID, int sex) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("playerID", playerID);
		map.put("sex", String.valueOf(sex));
		this.getSqlMapClientTemplate().update("User.updatePlayerSex", map);
	}
	public void updatePlayerFreeCj(String playerID, int cjcs) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("playerID", playerID);
		map.put("cjcs", cjcs);
		this.getSqlMapClientTemplate().update("User.updatePlayerFreeCj", map);
	}

	public void updatePlayerMgrID(String playerID, String mgrID,String mgrIndex) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("playerID", playerID);
		map.put("mgrID", mgrID);
		map.put("mgrIndex", mgrIndex);
		map.put("mgrTime", new Date());
		this.getSqlMapClientTemplate().update("User.updatePlayerMgrID", map);
	}
	
	public void updateAllPlayerFreeCjAndParam01(){
		this.getSqlMapClientTemplate().update("User.updateAllPlayerFreeCjAndParam01", "");
	}
	/** 修改俱乐部权限  2018-08-28 */
	public void updateClubType(String playerID, int clubType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerID", playerID);
		params.put("clubType", clubType);
		this.getSqlMapClientTemplate().update("User.updateClubType", params);
	}

	@Override
	public void updateClubPlayerGold(int goldNum, String playerId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerId", playerId);
		params.put("goldNum", goldNum);
		this.getSqlMapClientTemplate().update("User.updateClubPlayerGold", params);
		
	}

	@Override
	public String getPlayerMgrIndex(String playerID) {
		String mgrIndex= (String) this.getSqlMapClientTemplate().queryForObject(
				"User.getPlayerMgrIndex", playerID);

		return mgrIndex;
	}
}