package com.chess.common.dao.impl; 

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.UUIDGenerator;
import com.chess.common.bean.ClubMember;
import com.chess.common.dao.IClubMemberDAO;

/** 
 * @author wangjia
 * @version 创建时间：2018-8-29 
 * 类说明 :
 */
@SuppressWarnings("unchecked")
public class ClubMemberDao extends SqlMapClientDaoSupport implements IClubMemberDAO{
	
	public String createClubMember(String playerId, String memberId,String clubCode) {
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("id", UUIDGenerator.generatorUUID());
        maps.put("playerId", playerId);
        maps.put("memberId", memberId);
        maps.put("clubCode", clubCode);
        maps.put("applyState", 0);
        this.getSqlMapClientTemplate().insert("ClubMember.createClubMember", maps);
		return "";
	}

	public int deleteClubMember(String playerId, String memberId,String clubCode) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("playerId", playerId);
		maps.put("memberId", memberId);
		maps.put("clubCode", clubCode);
		if (memberId == null || memberId.equals("")) {
			return this.getSqlMapClientTemplate().delete("ClubMember.deleteAllClubMember", maps);
		}else{
			return this.getSqlMapClientTemplate().delete("ClubMember.deleteClubMember", maps);
		}
	}

	public int updateClubMemberState(String playerId, String memberId,String clubCode,int applyState) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("playerId", playerId);
		maps.put("memberId", memberId);
        maps.put("clubCode", clubCode);
        maps.put("applyState", applyState);
		return this.getSqlMapClientTemplate().update("ClubMember.updateClubMemberApplyState", maps);
	}
	
	public int updateClubMemberRemark(String playerId, String memberId,String clubCode,String remark) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("playerId", playerId);
		maps.put("memberId", memberId);
        maps.put("clubCode", clubCode);
        maps.put("remark", remark);
		return this.getSqlMapClientTemplate().update("ClubMember.updateClubMemberRemark", maps);
	}

	public List<ClubMember> getClubMembers(String playerId, String clubCode,int applyState) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("playerId", playerId);
        maps.put("clubCode", clubCode);
        if(applyState == 2){
        	 maps.put("applyState", 0);
             return this.getSqlMapClientTemplate().queryForList("ClubMember.getClubMembersByApplyState", maps);
        }else{
            return this.getSqlMapClientTemplate().queryForList("ClubMember.getClubMembers", maps);
        }
	}

	public boolean isExistsMember(String playerId, String memberId,
			String clubCode) {
		Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("playerId", playerId);
        maps.put("memberId", memberId);
        maps.put("clubCode", clubCode);
        int size = this.getSqlMapClientTemplate().queryForList("ClubMember.isExitsMember", maps).size();
        return (size > 0);
	}

	public int queryMemberCount(int clubCode) {
		Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("clubCode", clubCode);
		int count = (Integer) this.getSqlMapClientTemplate().queryForObject("ClubMember.getClubMembersCountByClubCode", maps, "memberCount");
		return count;
	}

	public ClubMember queryMemberClub(int clubCode, String memberId) {
		Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("memberId", memberId);
        maps.put("clubCode", clubCode);
		ClubMember clubMember = (ClubMember) this.getSqlMapClientTemplate().queryForObject("ClubMember.getMemberClub",maps);
		return clubMember;
	}
	
	public ClubMember getPlayerByClubCode(String clubCode) {
		Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("clubCode", clubCode);
        List<ClubMember> list = this.getSqlMapClientTemplate().queryForList("ClubMember.getPlayerByClubCode", maps);
        if(list.size()>0){
        	return list.get(0);
        }
        return null;
	}
	/**
	 * @see IClubMemberDAO#getClubMembersByClubCode
	 */
	public List<String> getClubMembersByClubCode(int clubCode) {
		Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("clubCode", clubCode);
		List<String> list = this.getSqlMapClientTemplate().queryForList("ClubMember.getClubMembersByClubCode", maps);
        return list;
	}

	@Override
	public int updateClubMemberGold(int clubCode, String memberId, int goldNum) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("goldNum", goldNum);
		maps.put("memberId", memberId);
        maps.put("clubCode", clubCode);
		return this.getSqlMapClientTemplate().update("ClubMember.updateClubMemberGold", maps);
	}
	
    @Override
    public int updateClubMemberTotalPay(int clubCode, String memberId, int goldNum) {
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("totalPay", goldNum);
        maps.put("memberId", memberId);
        maps.put("clubCode", clubCode);
        return this.getSqlMapClientTemplate().update("ClubMember.updateClubMemberTotalPay", maps);
    }

	@Override
	public ClubMember getClubMemberByPlayerId(int clubCode, String playerId) {
		Map<String, Object> maps = new HashMap<String, Object>();
		maps.put("playerId", playerId);
        maps.put("clubCode", clubCode);
		return (ClubMember) this.getSqlMapClientTemplate().queryForObject("ClubMember.getClubMemberByPlayerId", maps);
	}

}
 
