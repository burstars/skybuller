package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.UUIDGenerator;
import com.chess.common.bean.UserFriend;
import com.chess.common.dao.IUserFriendDAO;

public class UserFriendDAO extends SqlMapClientDaoSupport implements IUserFriendDAO {

    public String createPlayerFriend(String playerid, String friendid, int applyResult) {
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("ID", UUIDGenerator.generatorUUID());
        maps.put("playerID", playerid);
        maps.put("friendID", friendid);
        maps.put("applyResult", applyResult);
        this.getSqlMapClientTemplate().insert("UserFriend.createUserFriend", maps);
        return "";
    }

    /**
     * 删除好友
     *
     * @param playerid 玩家ID
     * @param friendid 好友ID
     */
    public int deletePlayerFriend(String playerid, String friendid) {
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("playerID", playerid);
        maps.put("friendID", friendid);
        return this.getSqlMapClientTemplate().delete("UserFriend.deleteUserFriend", maps);
    }

    public List<UserFriend> getPlayerFriends(String playerid) {
        return this.getSqlMapClientTemplate().queryForList("UserFriend.getUserFriends", playerid);
    }

    /**
     * 根据索引号查找索引
     */
    public List<UserFriend> findPlayersByIndex(int index) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("index", index);

        return this.getSqlMapClientTemplate().queryForList("UserFriend.getUserFriendsByIndex", params);

    }

    /**
     * 根据昵称
     */
    public List<UserFriend> findPlayersByName(String nickName) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerName", nickName);

        return this.getSqlMapClientTemplate().queryForList("UserFriend.getUserFriendsByName", params);
    }

    /**
     * 是否已存在好友
     */
    public boolean isExistsFriend(String playerID, String friendID) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerID", playerID);
        params.put("friendID", friendID);

        int size = this.getSqlMapClientTemplate().queryForList("UserFriend.isExitsPlayer", params).size();
        return (size > 0);
    }

    /**
     * 更新好友备注名称
     */
    public void updateFriendRemark(String playerID, String friendID, String remark) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerID", playerID);
        params.put("friendID", friendID);
        params.put("remark", remark);

        this.getSqlMapClientTemplate().update("UserFriend.updateUserFriendRemark", params);
    }

    /**
     * 更新好友申请标志
     */
    public void updateFriendApplyResult(String playerID, String friendID, int applyResult) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("playerID", playerID);
        params.put("friendID", friendID);
        params.put("applyResult", applyResult);

        this.getSqlMapClientTemplate().update("UserFriend.updateUserFriendApplyResult", params);
    }
}
