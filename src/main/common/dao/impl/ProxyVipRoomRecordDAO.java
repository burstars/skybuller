/**
 * Description:Vip房间记录的DAO实现类
 */
package com.chess.common.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.SpringService;
import com.chess.common.StringUtil;
import com.chess.common.bean.ProxyVipRoomRecord;
import com.chess.common.bean.VipRoomRecord;
import com.chess.common.dao.IProxyVipRoomRecordDAO;
import com.chess.common.dao.IVipRoomRecordDAO;
import com.chess.common.service.ISystemConfigService;

public class ProxyVipRoomRecordDAO extends SqlMapClientDaoSupport implements IProxyVipRoomRecordDAO {

	private static String lastTableSuffixName = "";

	public void createTable(String tableNameSuffix) {
		// TODO Auto-generated method stub
		this.getSqlMapClientTemplate().insert("ProxyVipRoomRecord.createTable", tableNameSuffix);
	}

	public void createProxyVipRoomRecord(ProxyVipRoomRecord roomRecord) {
		// TODO Auto-generated method stub
		if (lastTableSuffixName.equals(roomRecord.getTableNameSuffix()) == false) {// 
			lastTableSuffixName = roomRecord.getTableNameSuffix();
			createTable(roomRecord.getTableNameSuffix());
		}
		this.getSqlMapClientTemplate().insert("ProxyVipRoomRecord.insert", roomRecord);
	}

	@SuppressWarnings("unchecked")
	public List<ProxyVipRoomRecord> getMyProxyVipRoomRecord(String dateStr, String proxyID) {
		// TODO Auto-generated method stub
		List<String> tableNames = new ArrayList<String>();
		long timestamp = new Date().getTime();
		
		if(dateStr == null || dateStr.length() < 2)
		{
			// 获取过去2天的表名
			for (int i = 0; i < 2; i++) {
				String date = StringUtil.timestamp2String(timestamp);
				String tableName = "t_proxy_vip_room_record__" + date;
				if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
					tableNames.add(tableName);
				}
				timestamp = timestamp - 24 * 60 * 60 * 1000;
			}
		}
		else
		{
			String tableName = "t_proxy_vip_room_record__" + dateStr;
			if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
				tableNames.add(tableName);
			}		
		}
		
		if (tableNames.size() == 0) {
			return null;
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames);
		params.put("proxyID", proxyID);
		List<ProxyVipRoomRecord> roomRecords = this.getSqlMapClientTemplate().queryForList("ProxyVipRoomRecord.getMyProxyVipRoomRecord", params);
		// System.out.println("### roomRecords size:" + roomRecords.size());
		return roomRecords;
	}

	public void updateRecord(ProxyVipRoomRecord record) {
		this.getSqlMapClientTemplate().update("ProxyVipRoomRecord.updateRecord", record);
	}
	
	public void endRecord(ProxyVipRoomRecord record) {
		this.getSqlMapClientTemplate().update("ProxyVipRoomRecord.endRecord", record);
	}

	public String checkVipRoomRecordTableIsExists(String tableName) {
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		Map<String, String> map = new HashMap<String, String>();
		map.put("tableName", tableName);
		map.put("DB", cfgService.getPara(8052).getValueStr());
		String trueName = (String) this.getSqlMapClientTemplate().queryForObject("ProxyVipRoomRecord.checkTableIsExists", map);
		return trueName;
	}

	public List<ProxyVipRoomRecord> getAllProxyVipRoomRecordByPage(String dateStr, String playerID, String startRownum, String endRownum) {
		// TODO Auto-generated method stub
		List<String> tableNames = new ArrayList<String>();
		long timestamp = new Date().getTime();
		
		if(dateStr == null || dateStr.length() < 2)
		{
			// 获取过去一周的表名
			for (int i = 0; i < 7; i++) {
				String date = StringUtil.timestamp2String(timestamp);
				String tableName = "t_proxy_vip_room_record__" + date;
				if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
					tableNames.add(tableName);
				}
				timestamp = timestamp - 24 * 60 * 60 * 1000;
			}
		}
		else
		{
			String tableName = "t_proxy_vip_room_record__" + dateStr;
			if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
				tableNames.add(tableName);
			}
		}
		
		if (tableNames.size() == 0) {
			return null;
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames);
		params.put("player1ID", playerID);
		params.put("start", startRownum);
		params.put("end", endRownum);
		List<ProxyVipRoomRecord> roomRecords = this.getSqlMapClientTemplate().queryForList("ProxyVipRoomRecord.getAllProxyVipRoomRecordByPage", params);
		return roomRecords;
	}
	
	public int getVipRoomIndexCount(String tableName){
		return (Integer) this.getSqlMapClientTemplate().queryForObject("ProxyVipRoomRecord.getVipRoomIndexCount", tableName);
	}
}
