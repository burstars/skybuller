package com.chess.common.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.ActiveCode;
import com.chess.common.dao.IActiveCodeDAO;

public class ActiveCodeDAO extends SqlMapClientDaoSupport implements IActiveCodeDAO {
	
 
	private static Logger logger = LoggerFactory.getLogger(ActiveCodeDAO.class);
	
	public String createActiveCode(ActiveCode ac){
		//		
		this.getSqlMapClientTemplate().insert("ActiveCode.insert", ac);
 
		return "";
	}

}