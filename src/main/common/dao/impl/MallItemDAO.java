package com.chess.common.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.MallItem;
import com.chess.common.dao.IMallItemDAO;
 
 

public class MallItemDAO extends SqlMapClientDaoSupport implements IMallItemDAO 
{
	
 
	public void createItemBase(MallItem item)
	{
	
		this.getSqlMapClientTemplate().insert("MallItem.insert", item);
	}
		

    public List<MallItem> getAll()
    {
    	List<MallItem> bases = this.getSqlMapClientTemplate().queryForList("MallItem.getAll");
    	
    	return bases;
    }
    
	public void update(MallItem item) {

		this.getSqlMapClientTemplate().update("MallItem.update",item);
	}


	public MallItem getItemBaseByItemID(int itemID) {
		// TODO Auto-generated method stub
		return (MallItem) this.getSqlMapClientTemplate().queryForObject("MallItem.getMallItemByItemID", itemID);
	}


	public void deleteItemBaseByID(int itemID) {
		// TODO Auto-generated method stub
		this.getSqlMapClientTemplate().delete("MallItem.deleteMallItemByID", itemID);
	}
	
	 public Integer getSellCard(Map<String, String> map){
		Integer sell_card= (Integer) this.getSqlMapClientTemplate().queryForObject("MallItem.getSellCard", map);
		return sell_card;
	 }
 
}