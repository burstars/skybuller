/**
 * Description:Vip房间记录的DAO实现类
 */
package com.chess.common.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.SpringService;
import com.chess.common.StringUtil;
import com.chess.common.bean.VipRoomRecord;
import com.chess.common.dao.IVipRoomRecordDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ISystemConfigService;

public class VipRoomRecordDAO extends SqlMapClientDaoSupport implements IVipRoomRecordDAO {

	private static String lastTableSuffixName = "";
	private static List<String> lastTablePrefixNameList = new ArrayList<String>();

	public void createTable(Map<String, String> parmMap) {
		// TODO Auto-generated method stub
		this.getSqlMapClientTemplate().insert("VipRoomRecord.createTable", parmMap);
	}

	public void createVipRoomRecord(VipRoomRecord roomRecord) {
		// TODO Auto-generated method stub
		if (lastTableSuffixName.equals(roomRecord.getTableNameSuffix()) == false ){
			lastTablePrefixNameList.clear();
		}
		if (lastTableSuffixName.equals(roomRecord.getTableNameSuffix()) == false || lastTablePrefixNameList.contains(roomRecord.getTableNamePrefix()) == false) {// 
			lastTableSuffixName = roomRecord.getTableNameSuffix();
			if(lastTablePrefixNameList.contains(roomRecord.getTableNamePrefix()) == false){
				lastTablePrefixNameList.add(roomRecord.getTableNamePrefix());
			}
			Map<String, String> parmMap = new HashMap<String, String>();
			parmMap.put("tableNamePrefix", roomRecord.getTableNamePrefix());
			parmMap.put("tableNameSuffix", roomRecord.getTableNameSuffix());
			createTable(parmMap);
		}
		roomRecord.setEndTime(new Date());
		this.getSqlMapClientTemplate().insert("VipRoomRecord.insert", roomRecord);
	}

	@SuppressWarnings("unchecked")
	public List<VipRoomRecord> getMyVipRoomRecord(String dateStr, String playerID, String gameID) {
		List<String> tableNames=getTableNameList(dateStr, gameID, "t_vip_room_record__",3);
		if (tableNames.size() == 0) {
			return null;
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames);
		params.put("player1ID", playerID);
		List<VipRoomRecord> roomRecords = this.getSqlMapClientTemplate().queryForList("VipRoomRecord.getMyVipRoomRecord", params);
		// System.out.println("### roomRecords size:" + roomRecords.size());
		return roomRecords;
	}

	private List<String> getTableNameList(String dateStr,String gameID,String tablePrefix,int days){
		List<String> tableNames = new ArrayList<String>();
		long timestamp = new Date().getTime();
		
		if(dateStr == null || dateStr.length() < 2)
		{
			// 获取过去一周的表名
			for (int i = 0; i < days; i++) {
				String date = StringUtil.timestamp2String(timestamp);
				String tableName = tablePrefix + gameID + "__" + date;
				if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
					tableNames.add(tableName);
				}
				timestamp = timestamp - 24 * 60 * 60 * 1000;
			}
		}
		else
		{
			String tableName = tablePrefix + gameID + "__" + dateStr;
			if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
				tableNames.add(tableName);
			}		
		}
		return tableNames;
		
	}
	public void updateRecord(VipRoomRecord record) {
		this.getSqlMapClientTemplate().update("VipRoomRecord.updateRecord", record);
	}
	
	public void endRecord(VipRoomRecord record) {
		this.getSqlMapClientTemplate().update("VipRoomRecord.endRecord", record);
	}

	public String checkVipRoomRecordTableIsExists(String tableName) {
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		Map<String, String> map = new HashMap<String, String>();
		map.put("tableName", tableName);
		map.put("DB", cfgService.getPara(8052).getValueStr());
		String trueName = (String) this.getSqlMapClientTemplate().queryForObject("VipRoomRecord.checkTableIsExists", map);
		return trueName;
	}

	public List<VipRoomRecord> getAllVipRoomRecordByPage(String dateStr, String playerID, String startRownum, String endRownum) {
		// TODO Auto-generated method stub
		List<String> tableNames = new ArrayList<String>();
		long timestamp = new Date().getTime();
		if(dateStr == null || dateStr.length() < 2)
		{
			// 获取过去一周的表名
			for (int i = 0; i < 7; i++) {
				String date = StringUtil.timestamp2String(timestamp);
				String gameId = GameContext.gameId;
				String tableName = "t_vip_room_record__" + gameId + "__" + date;
				if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
					tableNames.add(tableName);
				}
				timestamp = timestamp - 24 * 60 * 60 * 1000;
			}
		}
		else
		{
			String gameId = GameContext.gameId;
			String tableName = "t_vip_room_record__" + gameId + "__" + dateStr;
			if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
				tableNames.add(tableName);
			}
		}
		
		if (tableNames.size() == 0) {
			return null;
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames);
		params.put("player1ID", playerID);
		params.put("start", startRownum);
		params.put("end", endRownum);
		List<VipRoomRecord> roomRecords = this.getSqlMapClientTemplate().queryForList("VipRoomRecord.getAllVipRoomRecordByPage", params);
		return roomRecords;
	}
	
	public int getVipRoomIndexCount(String tableName){
		return (Integer) this.getSqlMapClientTemplate().queryForObject("VipRoomRecord.getVipRoomIndexCount", tableName);
	}

//	更新大赢家已读状态-lxw20180829
	public void updateRecordReadState(List<String> recordIds,String gameID) {
		List<String> tableNames = new ArrayList<String>();
		long timestamp = new Date().getTime();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("roomIds", recordIds);
		// 获取过去3天的表名
		for (int i = 0; i < 3; i++) {
			String date = StringUtil.timestamp2String(timestamp);
			String tableName = "t_vip_room_record__" + gameID + "__" + date;
			if (this.checkVipRoomRecordTableIsExists(tableName) != null) {
				tableNames.add(tableName);
				params.put("tableName", tableName);
				this.getSqlMapClientTemplate().update("VipRoomRecord.updateRecordReadState", params);
			}
			timestamp = timestamp - 24 * 60 * 60 * 1000;
		}	
	}
	//	获取未读大赢家数据
	public List<VipRoomRecord> getAllVipRoomRecord(String dateStr,String clubCode,String gameId) {
		// TODO Auto-generated method stub
		List<String> tableNames=getTableNameList(dateStr, gameId, "t_vip_room_record__",3);
		if (tableNames.size() == 0) {
			return null;
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames);
		params.put("clubCode", clubCode);
		List<VipRoomRecord> roomRecords = this.getSqlMapClientTemplate().queryForList("VipRoomRecord.getAllVipRoomRecord", params);
		return roomRecords;
	}

	public List<VipRoomRecord> getQyqRoomRecord(String dateStr,String playerID, String gameID, int clubCode) {
		List<String> tableNames=getTableNameList(dateStr, gameID, "t_vip_room_record__",3);
		if (tableNames.size() == 0) {
			return null;
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames.subList(0, 1));
		params.put("clubCode", clubCode);
		params.put("playerID", playerID);
		List<VipRoomRecord> roomRecords = this.getSqlMapClientTemplate().queryForList("VipRoomRecord.getQyqRoomRecord", params);
		//删除多余数据，每个房间保留一条数据
		String roomId="";
		for(int i=roomRecords.size()-1;i>=0;i--){
			VipRoomRecord record=roomRecords.get(i);
			if(roomId.equals(record.getRoomID())){
				roomRecords.remove(i);
			}else{
				roomId=record.getRoomID();
			}
		}
		return roomRecords;
	}
}
