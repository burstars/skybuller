/**
 * Description:普通场游戏记录DAO
 */
package com.chess.common.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.SpringService;
import com.chess.common.bean.NormalGameRecord;
import com.chess.common.dao.INormalGameRecordDAO;
import com.chess.common.service.ISystemConfigService;

public class NormalGameRecordDAO extends SqlMapClientDaoSupport implements INormalGameRecordDAO {

	private static String lastTableSuffixName = "";
	
	public void createTable(String tableNameSuffix) {
		// TODO Auto-generated method stub
		this.getSqlMapClientTemplate().insert("NormalGameRecord.createTable", tableNameSuffix);
	}

	public void createNormalGameRecord(NormalGameRecord normalGameRecord) {
		// TODO Auto-generated method stub
		if (lastTableSuffixName.equals(normalGameRecord.getTableNameSuffix()) == false) {// 

			lastTableSuffixName = normalGameRecord.getTableNameSuffix();
			createTable(normalGameRecord.getTableNameSuffix());
		}
		normalGameRecord.setEndTime(new Date());
		this.getSqlMapClientTemplate().insert("NormalGameRecord.insert", normalGameRecord);
	}

	public List<NormalGameRecord> getNormalGameRecordByDate(String date, String player_index) {
		// TODO Auto-generated method stub
		String tableName = "t_normal_game_record__" + date;
		if (this.checkNormalGameRecordTableIsExists(tableName) != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("tableName", tableName);
			params.put("date", date);
			params.put("player_index", player_index);
			return this.getSqlMapClientTemplate().queryForList("NormalGameRecord.getNormalGameRecordByDate", params);
		}else{
			return null;
		}
	}

	public String checkNormalGameRecordTableIsExists(String tableName) {
		// TODO Auto-generated method stub
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		Map<String, String> map = new HashMap<String, String>();
		map.put("tableName", tableName);
		map.put("DB", cfgService.getPara(8052).getValueStr());
		String trueName = (String) this.getSqlMapClientTemplate().queryForObject("NormalGameRecord.checkTableIsExists", map);
		return trueName;
	}

	public List<NormalGameRecord> getAllNormalRecordByPage(String date, String player_index,String startRownum, String endRownum) {
		// TODO Auto-generated method stub
		String tableName = "t_normal_game_record__" + date;
		if (this.checkNormalGameRecordTableIsExists(tableName) != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("tableName", tableName);
			params.put("start", startRownum);
			params.put("end", endRownum);
			params.put("player_index", player_index);
			return this.getSqlMapClientTemplate().queryForList("NormalGameRecord.getAllNormalRecordByPage", params);
		}else{
			return null;
		}
	}

}
