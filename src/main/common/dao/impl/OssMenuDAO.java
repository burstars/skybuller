package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.oss.OssMenu;
import com.chess.common.dao.IOssMenuDAO;

public class OssMenuDAO extends SqlMapClientDaoSupport implements IOssMenuDAO{
	
	public Integer createOssMenu(OssMenu ossMenu) {
		return (Integer)this.getSqlMapClientTemplate().insert("OssMenu.createOssMenu", ossMenu);
	}

	public Integer updateOssMenu(OssMenu ossMenu) {
		return this.getSqlMapClientTemplate().update("OssMenu.updateOssMenu", ossMenu);
	}

	public Integer deleteOssMenuByID(String ossMenuID) {
		return this.getSqlMapClientTemplate().delete("OssMenu.deleteOssMenuByID", ossMenuID);
	}

	public OssMenu getOssMenuByID(String ossMenuID) {
		return (OssMenu)this.getSqlMapClientTemplate().queryForObject("OssMenu.getOssMenuByID", ossMenuID);
	}
	
	@SuppressWarnings("unchecked")
	public List<OssMenu> getOssMenuList() {
		return this.getSqlMapClientTemplate().queryForList("OssMenu.getOssMenuList");
	}


}
