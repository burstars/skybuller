package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.UUIDGenerator;
import com.chess.common.bean.StatisticUser;
import com.chess.common.dao.IUserStatisticDAO;



public class StatisticUserDAO extends SqlMapClientDaoSupport implements IUserStatisticDAO{

	public Integer createStatisticUser(StatisticUser StatisticUser) {
		StatisticUser.setStatisticId(UUIDGenerator.generatorUUID());
		return (Integer)this.getSqlMapClientTemplate().insert("StatisticUser.createStatisticUser", StatisticUser);
	}

	public Integer updateStatisticUser(StatisticUser StatisticUser) {
		return this.getSqlMapClientTemplate().update("StatisticUser.updateStatisticUser", StatisticUser);
	}
	
	public Integer updateSecdayLeftNumByID(String statisticId,Integer secdayLeftNum ){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("statisticId", statisticId);
		params.put("secdayLeftNum", secdayLeftNum);
		return this.getSqlMapClientTemplate().update("StatisticUser.updateSecdayLeftNumByID", params);
	}
	
	public Integer updateThirdDayLeftNumByID(String statisticId,Integer thirdDayLeftNum ){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("statisticId", statisticId);
		params.put("thirdDayLeftNum", thirdDayLeftNum);
		return this.getSqlMapClientTemplate().update("StatisticUser.updateThirdDayLeftNumByID", params);
	}
	public Integer updateSevDayLeftNumByID(String statisticId,Integer sevDayLeftNum ){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("statisticId", statisticId);
		params.put("sevDayLeftNum", sevDayLeftNum);
		return this.getSqlMapClientTemplate().update("StatisticUser.updateSevDayLeftNumByID", params);
	}

	public Integer deleteStatisticUserByID(String statisticId) {
		return this.getSqlMapClientTemplate().delete("StatisticUser.deleteStatisticUserByID", statisticId);
	}

	public StatisticUser getStatisticUserByID(String statisticId) {
		return (StatisticUser)this.getSqlMapClientTemplate().queryForObject("StatisticUser.getStatisticUserByID", statisticId);
	}
	public StatisticUser getStatisticUserByTimeStr(String timeStr,int platformType){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("platformType", platformType);
		params.put("timeStr", timeStr);
//		if(platformType!=-1){
//			params.put("platformType", platformType);
//		}
		return (StatisticUser)this.getSqlMapClientTemplate().queryForObject("StatisticUser.getStatisticUserByTimeStr", params);
	}
	
	@SuppressWarnings("unchecked")
	public List<StatisticUser> getStatisticUserList() {
		return this.getSqlMapClientTemplate().queryForList("StatisticUser.getStatisticUserList");
	}

}