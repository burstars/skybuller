package com.chess.common.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.jacorb.idl.runtime.int_token;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.UserBackpack;
import com.chess.common.dao.IUserBackpackDAO;
import com.chess.common.framework.GameContext;
 

public class UserBackpackDAO extends SqlMapClientDaoSupport implements IUserBackpackDAO {
	
 
	private static Logger logger = LoggerFactory.getLogger(UserDAO.class);
	
	public void createPlayerItem(UserBackpack item,String gameID)
	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("itemID", item.getItemID());
		map.put("itemBaseID", item.getItemBaseID());
		map.put("itemNum", item.getItemNum());
		map.put("playerID", item.getPlayerID());
//		if(gameID.equals("")){
			map.put("DB", "t_user_backpack");
//		}else{
//			map.put("DB", GameContext.getPayConfig(gameID)+".t_user_backpack");
//		}
		this.getSqlMapClientTemplate().insert("UserBackpack.insert", map);
	}
	
    public List<UserBackpack> getPlayerItemListByPlayerID(String playerID)
    {
    	List<UserBackpack> items = this.getSqlMapClientTemplate().queryForList("UserBackpack.getPlayerItemListByPlayerID",playerID);
    	
    	return items;
    }
    
    public List<UserBackpack> getPlayerItemListByItemID(String itemID)
    {
    	List<UserBackpack> items = this.getSqlMapClientTemplate().queryForList("UserBackpack.getPlayerItemListByItemID",itemID);
    	
    	return items;
    }
    
    public void deleteByPlayerItemID(String playerItemID)
    {
    	 this.getSqlMapClientTemplate().delete("UserBackpack.deleteByPlayerItemID",playerItemID);
    }
	 
    //
    public void updatePlayerItemNum(String playerID,Integer itemBaseID,int itemNum,String gameID)
    {
    	Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerItemID", playerID+"_"+itemBaseID);
		params.put("itemNum", itemNum);
		if("".equals(gameID)){
			params.put("DB", "t_user_backpack");
		}else{
			params.put("DB", GameContext.getPayConfig(gameID)+".t_user_backpack");
		}
		
		this.getSqlMapClientTemplate().update("UserBackpack.updatePlayerItemNum", params);
    }
    
    //2016.8.26 增加房卡
    public void addPlayerItemNum(String playerID,Integer itemBaseID,int itemNum)
    {
    	Map<String, Object> params = new HashMap<String, Object>();
		params.put("playerItemID", playerID+"_"+itemBaseID);
		params.put("itemNum", itemNum);
		
		this.getSqlMapClientTemplate().update("UserBackpack.addPlayerItemNum", params);
    }
 
    public List<UserBackpack> getAllItemByPlayer(String gameID,String playerID){
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("playerID", playerID);
//    	map.put("DB", GameContext.getPayConfig(gameID)+".t_user_backpack");
    	if("".equals(gameID)){
			map.put("DB", "t_user_backpack");
		}else{
			map.put("DB", GameContext.getPayConfig(gameID)+".t_user_backpack");
		}
		return this.getSqlMapClientTemplate().queryForList("UserBackpack.getAllByPlayerID",map);
    }
 
}