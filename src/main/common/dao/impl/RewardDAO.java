package com.chess.common.dao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.bean.Reward;
import com.chess.common.dao.IRewardDAO;

public class RewardDAO extends SqlMapClientDaoSupport implements IRewardDAO {

	
	
	public void deleteByID(int id) {
		
       this.getSqlMapClientTemplate().delete("Reward.deleteByID",id);
	}

	public void deleteByName(String name) {
		

	}

	public List<Reward> getRewardList() {
		return this.getSqlMapClientTemplate().queryForList("Reward.getList");
		
	}

	public void update(Reward r) {
		this.getSqlMapClientTemplate().update("Reward.update",r);
		
	}
	public void insert(Reward r){
		this.getSqlMapClientTemplate().insert("Reward.insert",r);
	}

	public Reward getRewardByID(int id) {
		
		return (Reward) this.getSqlMapClientTemplate().queryForObject("Reward.getRewardByID",id);
	}

	

}
