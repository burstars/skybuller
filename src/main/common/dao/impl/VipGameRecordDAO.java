/**
 * Description:VIP游戏记录DAO实现类
 */
package com.chess.common.dao.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.StringUtil;
import com.chess.common.bean.VipGameRecord;
import com.chess.common.dao.IVipGameRecordDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ISystemConfigService;

public class VipGameRecordDAO extends SqlMapClientDaoSupport implements IVipGameRecordDAO {

	private static String lastTableSuffixName = "";

	public void createTable(String tableNameSuffix) {
		// TODO Auto-generated method stub
		this.getSqlMapClientTemplate().insert("VipGameRecord.createTable", tableNameSuffix);
	}

	public void createVipGameRecord(VipGameRecord gameRecord) {
		// TODO Auto-generated method stub
		if (lastTableSuffixName.equals(gameRecord.getTableNameSuffix()) == false) {// 
			createTable(gameRecord.getTableNameSuffix());
		}
		gameRecord.setRecordDate(new Date());
		this.getSqlMapClientTemplate().insert("VipGameRecord.insert", gameRecord);
	}

	@SuppressWarnings("unchecked")
	public List<VipGameRecord> getVipGameRecordByRoomID(String roomID, String datetime) {
		// TODO Auto-generated method stub
		List<String> tableNames = new ArrayList<String>();
		try {
			long timestamp = DateService.getDateByStrToTime(datetime).getTime();
			// 获取过去2天的表名
			for (int i = 0; i < 3; i++) {
				String date = StringUtil.timestamp2String(timestamp);
				String gameIdString = GameContext.gameId;
				String tableName = gameIdString+"__t_vip_game_record__" + date;
				if (this.checkVipGameRecordTableIsExists(tableName) != null) {
					tableNames.add(tableName);
				}
				timestamp = timestamp - 24 * 60 * 60 * 1000;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if(tableNames.size() < 1){
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames);
		params.put("roomID", roomID);
		List<VipGameRecord> gameRecords = this.getSqlMapClientTemplate().queryForList("VipGameRecord.getVipGameRecordByRoomID", params);
		return gameRecords;
	}

	@SuppressWarnings("unchecked")
	public List<VipGameRecord> getMyVipGameRecordRoomID(String roomID, String playerID) {
		// TODO Auto-generated method stub
		List<String> tableNames = new ArrayList<String>();
		long timestamp = new Date().getTime();
		// 获取过去2天的表名
		for (int i = 0; i < 2; i++) {
			String date = StringUtil.timestamp2String(timestamp);
			String gameId = GameContext.gameId;
			String tableName = "t_vip_room_record__" + gameId + "__" + date;
			if (this.checkVipGameRecordTableIsExists(tableName) != null) {
				tableNames.add(tableName);
				timestamp = timestamp - 24 * 60 * 60 * 1000;
				continue;
			} else {
				break;
			}
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames);
		params.put("roomID", roomID);
		params.put("playerID", playerID);
		List<VipGameRecord> gameRecords = this.getSqlMapClientTemplate().queryForList("VipGameRecord.getMyVipGameRecordRoomID", params);
		return gameRecords;
	}

	public String checkVipGameRecordTableIsExists(String tableName) {
		// TODO Auto-generated method stub
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		Map<String, String> map = new HashMap<String, String>();
		map.put("tableName", tableName);
		map.put("DB", cfgService.getPara(8052).getValueStr());
		String trueName = (String) this.getSqlMapClientTemplate().queryForObject("VipGameRecord.checkTableIsExists", map);
		return trueName;
	}

	public List<VipGameRecord> getAllVipGameRecordByPage(String roomID, String datetime, String startRownum, String endRownum) {
		// TODO Auto-generated method stub
		List<String> tableNames = new ArrayList<String>();
		try {
			long timestamp = DateService.getDateByStrToTime(datetime).getTime();
			// 获取过去2天的表名
			for (int i = 0; i < 3; i++) {
				String date = StringUtil.timestamp2String(timestamp);
				String gameIdString = GameContext.gameId;
				String tableName = gameIdString+"__t_vip_game_record__" + date;
				if (this.checkVipGameRecordTableIsExists(tableName) != null) {
					tableNames.add(tableName);
				} 
				timestamp = timestamp - 24 * 60 * 60 * 1000;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if(tableNames.size() < 1){
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tableNames", tableNames);
		params.put("roomID", roomID);
		params.put("start", startRownum);
		params.put("end", endRownum);
		List<VipGameRecord> gameRecords = this.getSqlMapClientTemplate().queryForList("VipGameRecord.getAllVipGameRecordByPage", params);
		return gameRecords;
	}

}
