package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.WelFare;
 
 

public interface IWelFareDAO {
 
	
    public List<WelFare> getAll();
    
    public WelFare getWelFareById(int id);
    
    public List<WelFare> getFirstGl();
    
    public List<WelFare> getTwoGl(int part_no);
    
}