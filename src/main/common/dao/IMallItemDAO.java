package com.chess.common.dao;

import java.util.List;
import java.util.Map;

import com.chess.common.bean.MallItem;
 
 

public interface IMallItemDAO {
 
	public void createItemBase(MallItem item);
	
    public List<MallItem> getAll();
 
    public void update(MallItem item);
 
    public MallItem getItemBaseByItemID(int itemID);
    
    public void deleteItemBaseByID(int itemID);
    
    public Integer getSellCard(Map<String, String> map);
 
}