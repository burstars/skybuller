package com.chess.common.dao;
import com.chess.common.bean.EntranceUser;
import com.chess.common.bean.SensitiveWords;

//
public interface IEntranceUserDAO {
 
	public void init();
    /**
     * 插入一条数据
     * @param playerTable
     */
	public void insert(EntranceUser user);
	 /**
     * 创建一张表
     * @param tableNameSuffix
     */
	public void createTable(String tableNameSuffix);
	 /**
     * 根据玩家ID和操作类型查询相关操作（操作类型，日期范围）
     * @param tableNameSuffix
     */
	public EntranceUser getUserByAccount(String account);
	
	public EntranceUser getUserByMachineCode(String machineCode);
	
	public EntranceUser getUserByQQOpenID(String openid);
	
	public EntranceUser getUserByWXOpenID(String openid);
	
	public EntranceUser getUserByWXUnionID(String unionid);//cc modify 2017-12-5
	
	/**查询表是否存在，存在返回名字，不存在返回null**/
	public String getTableByName(String tableNameSuffix);
	
	public Integer getUserNum(String tableNameSuffix);
	
	/**通过昵称查找用户*/
	public EntranceUser getUserByNickName(String nickName);
	/**敏感词库是否有这个词*/
	public SensitiveWords getWords(String word);
}


