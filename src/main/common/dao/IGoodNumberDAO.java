package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.GoodNumber;

public interface IGoodNumberDAO {
	public void createGoodNumber(GoodNumber goodNumber);
	
	public int getAllGoodNumberTotal();
    public List<GoodNumber> getAllGoodNumber(String begin,String end);
 
    public void update(GoodNumber goodNumber);
 
    public GoodNumber getGoodNumberByPlayerId(String playerid);
    
    public int getGoodNumberByNumberTotal(String number);
    
    public int getGoodNumberTotal(String number);
    
    public GoodNumber getAnGoodNumberByNumber(String number);
    public List<GoodNumber> getGoodNumberByNumber(String number,String start,String end);
    
    public void deleteGoodNumberByID(int ID);
    
}
