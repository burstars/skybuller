/**
 * Description:VIP战绩DAO
 */
package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.GameVipRecord;

public interface IGameVipRecordDAO {

	/**
	 * 创建Vip战绩记录
	 * 
	 * @param record
	 * @return
	 */
	public void createVipRecord(GameVipRecord record);

	/**
	 * 通过指定玩家ID，获取该玩家的所有战绩记录
	 * 
	 * @param playerID
	 *            玩家ID
	 * @return
	 */
	public List<GameVipRecord> getGameVipRecordByID(String playerID);

	/**
	 * 通过指定日期，获取指定玩家的战绩记录
	 * 
	 * @param playerID
	 *            玩家ID
	 * @param date
	 *            指定日期
	 * @return
	 */
	public List<GameVipRecord> getGameVipRecordByDate(String playerID, java.util.Date date);
}
