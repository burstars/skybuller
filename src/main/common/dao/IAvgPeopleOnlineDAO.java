package com.chess.common.dao;
import java.util.Date;
import java.util.List;

import com.chess.common.bean.AvgPeopleOnline;

//
public interface IAvgPeopleOnlineDAO {
 
	public List<AvgPeopleOnline> getList(Date start ,Date end);
	public void insert(AvgPeopleOnline a);

	public Integer getMaxPeoNums(Date start ,Date end) ;

	public Float getAvgPeoNums(Date start ,Date end);

	public Integer getTotalPeo(Date start ,Date end);
	public Integer getTotal(Date start ,Date end);

 
}


