package com.chess.common.dao;

import org.apache.xpath.operations.Bool;

import com.chess.common.bean.GameActiveCode;



public interface IExchangePrizeDAO {

	/**查询礼品码，判断礼品码的合法性
	 * 
	 * 2016.8.24
	 ***/
	public GameActiveCode searchCode(String code);
	
	/***兑换礼品
	 * 
	 * 2016.8.24
	 * ***/
	public void exchangePrize(int userId,int value, int id);
	
}
