package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.PlayerHuType;
 
 

public interface IPlayerHuTypeDAO {

 
	public void createPlayerHuType(PlayerHuType huType);
	
    public List<PlayerHuType> getPlayerHuTypeListByPlayerID(String playerID);
    //
    public void updatePlayerHuTypeCount(String playerID,Integer huType,int count);
}