package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.GameConfig;

/**
 *  麻将配置加载
 *
 */
public interface IGameConfigDAO {

	/**
	 * 获取全部游戏配置
	 */
	public List<GameConfig> getAllGameConfigs();

}
