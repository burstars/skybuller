/**
 * Description:Vip房间记录的DAO接口
 */
package com.chess.common.dao;

import java.util.List;
import java.util.Map;

import com.chess.common.bean.VipRoomRecord;

public interface IVipRoomRecordDAO {

	/**
	 * 创建一张表
	 * 
	 * @param tableNameSuffix
	 *            表明后缀
	 */
	public void createTable(Map<String, String> parmMap);

	/**
	 * 创建玩家的VIP房间每锅记录
	 * 
	 * @param roomRecord
	 *            Vip房间记录的实例对象
	 */
	public void createVipRoomRecord(VipRoomRecord roomRecord);

	/**
	 * 通过玩家ID，获取玩家VIP房间的战绩记录（前x条记录）
	 * 
	 * @param playerID
	 *            玩家ID
	 * @param count
	 *            指定条数记录
	 */
	public List<VipRoomRecord> getMyVipRoomRecord(String dateStr, String playerID, String gameID);
	
	/**
	 * 获取俱乐部战绩记录-lxw20180831
	 */
	public List<VipRoomRecord> getQyqRoomRecord(String dateStr, String playerID, String gameID,int clubCode);

	//
	public void updateRecord(VipRoomRecord record);

	//
	public void endRecord(VipRoomRecord record);

	public String checkVipRoomRecordTableIsExists(String tableName);

	public List<VipRoomRecord> getAllVipRoomRecordByPage(String dateStr, String playerID, String startRownum, String endRownum);
	
	public int getVipRoomIndexCount(String tableName);
	
	/**
	 * 更新战绩表的读取状态（大赢家用）-lxw20180828
	 */
	public void updateRecordReadState(List<String> recordIds,String gameID);
//	获取未读大赢家数据
	public List<VipRoomRecord> getAllVipRoomRecord(String dateStr,String clubCode,String gameId);

}
