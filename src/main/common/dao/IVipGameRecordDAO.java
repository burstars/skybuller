/**
 * File:IGameVipRecordDAO.java
 */
package com.chess.common.dao;

import java.util.List;
import java.util.Map;

import com.chess.common.bean.VipGameRecord;

public interface IVipGameRecordDAO {

	/**
	 * 创建一张表
	 * 
	 * @param tableNameSuffix
	 *            表明后缀
	 */
	public void createTable(String tableNameSuffix);

	/**
	 * 创建Vip战绩记录
	 * 
	 * @param record
	 * @return
	 */
	public void createVipGameRecord(VipGameRecord gameRecord);

	/**
	 * 通过VIP房间ID，获取房间所有的游戏记录
	 * 
	 * @param roomID
	 *            VIP房间ID
	 * @return
	 */
	public List<VipGameRecord> getVipGameRecordByRoomID(String roomID, String datetime);

	/**
	 * 获取指定玩家的VIP游戏记录
	 * 
	 * @param roomID
	 *            VIP房间ID
	 * @param playerID
	 *            玩家ID
	 * @return
	 */
	public List<VipGameRecord> getMyVipGameRecordRoomID(String roomID, String playerID);

	public String checkVipGameRecordTableIsExists(String tableName);

	public List<VipGameRecord> getAllVipGameRecordByPage(String roomID, String datetime,String startRownum, String endRownum);
}
