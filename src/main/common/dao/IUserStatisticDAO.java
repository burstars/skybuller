package com.chess.common.dao;

import com.chess.common.bean.StatisticUser;

public interface IUserStatisticDAO {
	
	public Integer createStatisticUser(StatisticUser StatisticUser);
	
	public StatisticUser getStatisticUserByID(String statisticId);
	
	public StatisticUser getStatisticUserByTimeStr(String timeStr,int platformType);
	
	
	public Integer updateSecdayLeftNumByID(String statisticId,Integer secdayLeftNum );
	public Integer updateThirdDayLeftNumByID(String statisticId,Integer thirdDayLeftNum );
	public Integer updateSevDayLeftNumByID(String statisticId,Integer sevDayLeftNum );
}
