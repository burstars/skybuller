package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.UserFriend;


public interface IUserFriendDAO {
	/**
	 * 添加好友
	 * @param playerid
	 * @param friendid
	 * @return
	 */
	public String createPlayerFriend(String playerid, String friendid, int applyResult);
	
	/**
	 * 删除好友
	 * @param playerID  玩家ID
	 * @param friendID  好友ID
	 * */
	public int deletePlayerFriend(String playerID,String friendID);
	
	/**
	 * 所有好友
	 * */
	public List<UserFriend> getPlayerFriends(String playerid);
	
	/**
	 * 根据索引查找好友
	 * */
	public List<UserFriend> findPlayersByIndex(int index);
	
	/**
	 * 根据昵称查找好友
	 * */
	public List<UserFriend> findPlayersByName(String nickName);
	
	/**是否已存在好友*/
	public boolean isExistsFriend(String playerID,String friendID);
	
	/**更新好友备注名称*/
	public void updateFriendRemark(String playerID, String friendID, String remark);
	
	/**更新好友申请标志*/
	public void updateFriendApplyResult(String playerID, String friendID, int applyResult);
}
