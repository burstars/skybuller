package com.chess.common.dao;

import java.util.Date;
import java.util.List;

import com.chess.common.bean.LoginLog;
import com.chess.common.bean.LoginLogInfo;


/**
 * 用户登录 日志DAO接口
 * @author 
 * @since 
 */
public interface ILoginLogDAO {
	/**
	 * 记录日志
	 * @param loginLog
	 */
	public String createLoginLog(LoginLog loginLog);
	
	/**获得玩家今天最后一次登录的记录*/
	public  LoginLog getPlayerLastLoginLogToday(String playerID);
	
	/**昨天登录的玩家一直没下线的情况，处理下  昨天没下线的玩家默认在昨天的23：59:59 统一下线*/
	public void repairYesTodayNoLoginOut();
	
	/**按时间统计玩家的登录情况*/
	public List<LoginLogInfo> countPlayerLoginInfoByTime(Date time,int platformType);
	/**更新玩家下线时间*/
	public void updatePlayerLoginOutTime(String loginLogID,String TableSuffixName,Date loginOutTime);
	
	/**统计在某个时间段注册的玩家在某天登录的去重人数*/
	public Integer countLoginNumByRegisterTime(Date time ,Date regStartTime,Date regEndTime,int platformType);
	
	
}
