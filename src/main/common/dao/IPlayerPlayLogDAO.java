package com.chess.common.dao;

import com.chess.common.bean.PlayerPlayLog;

/**
 */
public interface IPlayerPlayLogDAO {
    public String createPlayerPlayLog(PlayerPlayLog playerPlayLog);

    public void updateVipCountByPlayerID(String playerID, int totalVipCount);
    public void updateNormalCountByPlayerID(String playerID, int totalNormalCount);
    public void deletePlayerPlayerLogByPlayerID(String playerID);
    public void incNormalCountByPlayerID(String playerID);
    public void incVipCountByPlayerID(String playerID);
    public void incCreateVipCountByPlayerID(String playerID);
}
