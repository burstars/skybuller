package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.SystemConfigPara;

public interface ISystemParamDAO {
	public void updatePara(SystemConfigPara para);
	public SystemConfigPara getPara(int paraID);
	public List<SystemConfigPara> getAllConfigPara();
	public List<SystemConfigPara> getAllClientConfigPara();
}
