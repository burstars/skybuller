package com.chess.common.dao;

import com.chess.common.bean.ActiveCode;

public interface IActiveCodeDAO {

	/**
	 * 创建兑换码
	 * @param ac
	 * @return
	 */
	public String createActiveCode(ActiveCode ac);
	
	

}