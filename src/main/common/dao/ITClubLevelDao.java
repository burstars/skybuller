package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.TClubLevel;

public interface ITClubLevelDao {
	/**获取所有俱乐部等级*/
	public List<TClubLevel> getAllClubLevels();
}



