package com.chess.common.dao;

import java.util.Date;
import java.util.List;

import com.chess.common.bean.ItemOpStaBase;
import com.chess.common.bean.MallHistory;


public interface IMallHistoryDAO {

	/**
	 * 创建PayHistory
	 * @param payHistory
	 * @return 数据影响条数
	 */
	public Integer createPayHistory(MallHistory payHistory,String gameID);

	/**
	 * 修改PayHistory
	 * @param payHistory
	 * @return 数据影响条数
	 */
	public Integer updatePayHistory(MallHistory payHistory,String gameID);

	/**
	 * 通过主键ID删除PayHistory
	 * @param payHistoryId
	 * @return 数据影响条数
	 */
	public Integer deletePayHistoryByID(String payHistoryId);
	
	/**
	 * 通过主键ID查询PayHistory
	 * @param payHistoryId
	 * @return PayHistory
	 */
	public MallHistory getPayHistoryByID(String payHistoryId);

	/**
	 * 查询所有的PayHistory
	 * @return PayHistory的集合
	 */
	public List<MallHistory> getPayHistoryList();
	
	/**
	 * 根据订单号判断充值记录是否存在
	 */
	public boolean isPayHistoryExistByOrderNo(String orderNo);
	
	
	/**
	 * 根据玩家编号获得总共充值数
	 * @param playerID
	 * @return
	 */
	public Integer getOnlyPayMoneyAll(String playerID);
	
	
	/**
	 * 获取指定时间充值的玩家数目
	 * @param playerID
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public Integer getOnlyPayMoneyByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, Date endTime, Integer payState);
	/**
	 * 分页获取指定时间指定类型玩家的充值
	 * @param playerID
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public List<MallHistory> getPayHistoryListByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, Date endTime, Integer beginNum,Integer onePageNum,Integer payState);
	
	/**
	 * 获取指定时间指定类型的玩家充值或赠送的云币总数
	 * @param playerID
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public Integer getPayMoneySumByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, Date endTime);
	/**根据订单号去查询充值记录*/
	public MallHistory getPayHistoryByOrderID(String OrderID);
	/**获得一定时间内的充值玩家数  去掉重复玩家*/
	public Integer getPayPlayerNumByTime(Date start ,Date end,int platformType);
	/**获得一定时间内的充值总数 */
	public Integer getPayTotalNumByTime(Date start ,Date end,int platformType);
	
	/**分渠道 统计玩家商品购买情况*/
	public List<ItemOpStaBase> countItemOperationStatisticPay(Date start ,Date end,int platformType);
	
	/**根据交易号查询订单信息*/
	public MallHistory getPayHistoryByTransactionID(String transaction_id);
}