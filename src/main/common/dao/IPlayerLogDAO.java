package com.chess.common.dao;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.chess.common.bean.ItemOpStaBase;
import com.chess.common.bean.PlayerOperationLog;
import com.chess.common.bean.SendCardLog;

//
public interface IPlayerLogDAO {
 
    /**
     * 插入一条数据
     * @param playerTable
     */
	public void insert(PlayerOperationLog playerLog);
	 /**
     * 创建一张表
     * @param tableNameSuffix
     */
	public void createTable(Map<String, String> parmMap);
	 /**
     * 根据玩家ID和操作类型查询相关操作（操作类型，日期范围）
     * @param tableNameSuffix
     */
	public List<PlayerOperationLog> queryByPlayerIDAndOpType(HashMap maps)throws SQLException, ParseException;
	
	
	public int queryByPlayerIDAndOpTypeTotal(HashMap maps)throws SQLException, ParseException;
	
	 public List<ItemOpStaBase> countItemOperationStatisticPay(Date time,int platformType);
	 public void insertSendCard(SendCardLog sCLog);
}


