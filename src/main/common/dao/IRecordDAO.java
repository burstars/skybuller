package com.chess.common.dao;

import java.util.List;
import java.util.Map;

import com.chess.common.bean.RecordBean;
import com.chess.common.bean.SimpleRecord;

public interface IRecordDAO {

	/**
	 * 创建一张表
	 * 
	 * @param tableNameSuffix
	 *            表明后缀
	 */
	public void createTable(Map<String, String> parmMap);

	/**
	 * 创建玩家的VIP房间每锅记录
	 * 
	 * @param roomRecord
	 *            Vip房间记录的实例对象
	 */
	public void createVipRoomRecord(RecordBean roomRecord);

	/**
	 * 通过玩家ID，获取玩家VIP房间的战绩记录（前x条记录）
	 * 
	 * @param playerID
	 *            玩家ID
	 * @param count
	 *            指定条数记录
	 */
	public List<SimpleRecord> getMyVipRoomRecord(String dateStr, String playerID, String gameID);
	/**
	 * 更新结束方式、结束时间
	 * @param record
	 */
	public void updateRecordEndWay(RecordBean record);
	/**
	 * 跟新玩家战绩分数
	 * @param record
	 */
	public void updateRecordScore(RecordBean record);
	/**
	 * 检查该表是否存在
	 * @param tableName
	 * @return
	 */
	public String checkVipRoomRecordTableIsExists(String tableName);

}