package com.chess.common.dao;

import java.util.List;

import com.chess.common.bean.ClubRankingList;

/**
 * 俱乐部排名
 * @author  2018-9-6
 */
public interface IClubRankingListDAO {
	/**
	 * 更新俱乐部排名数据:参与局数
	 * IClubRankingListDao.java
	 * @param playerId
	 * @param clubCode
	 * @param state (1,2,3)
	 * @param gameCount
	 * void
	 */
	public void updateGameCount(String playerId,int clubCode,String stateStr,int gameCount);
	/**
	 * 更新俱乐部排名数据:大赢家局数
	 * IClubRankingListDao.java
	 * @param playerId
	 * @param clubCode
	 * @param state (1,2,3)
	 * @param gameCount
	 * void
	 */
	public void updateWinnerCount(String playerId,int clubCode,String stateStr,int winnerCount);
	/**
	 * 更新俱乐部排名数据:点炮局数
	 * IClubRankingListDao.java
	 * @param playerId
	 * @param clubCode
	 * @param state (1,2,3)
	 * @param gameCount
	 * void
	 */
	public void updateGunnerCount(String playerId,int clubCode,String stateStr,int gunnerCount);
	
	/**
	 * 更新俱乐部排名数据:耗卡数
	 * @param playerId
	 * @param clubCode
	 * @param stateStr
	 * @param fangkaCount
	 */
	void updateFangkaCount(String playerId, int clubCode, String stateStr,
			int fangkaCount);
	
//	根据状态查询俱乐部排行数据-lxw20180906
	public List<ClubRankingList> getClubRankingList(int clubCode,int state);
//	定时调度更新俱乐部排行数据
	public void updateClubRankState(int state,boolean isToday);
}



