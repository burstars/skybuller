package com.chess.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.chess.common.bean.User;
import com.chess.common.framework.GameContext;

/**
 * 扑克通用配牌工具类（-20170927）
 */
public class ControlCardsUtil {

	// 配牌发牌次数
	private  int peiCardSendTime = 0;
	
	public  Map<Integer,List<Byte>> getControlCards(List<User> users){
		// 处理配牌
		if(GameContext.isDebugMode()){
			// 调试模式每次都读取配置文件
			GameContext.loadCardsProperties();
		}
		if(!GameContext.hasCardsConfig("enabled_handcards")){
			return null;
		}else {
			String sEnable = GameContext.getCardsConfig("enabled_handcards");
			if (!"1".equals(sEnable.trim()))
				return null;
		}
		// 配牌次数
		Integer peiCount = Integer.parseInt(GameContext.getCardsConfig("pei_count"));
		// 配牌玩家个数
		Integer playerCount = Integer.parseInt(GameContext.getCardsConfig("player_count"));
		// 取当前发到配牌的第几次的下一次
		peiCardSendTime++;

		if (peiCount <= 0 || peiCardSendTime > peiCount) {
			return null;
		}
		Map<Integer,List<Byte>> cardsMap =null;
		for(int i=1;i<=playerCount;i++){
			String id = GameContext.getCardsConfig("id" + i);
			Integer playerId = Integer.parseInt(id);
			for (User pl : users) {
				if (pl.getPlayerIndex().intValue() != playerId.intValue()){
					continue;
				}
				if(cardsMap==null){
					cardsMap = new HashMap<Integer, List<Byte>>();
				}
				String pcards = GameContext.getCardsConfig("cards"+ peiCardSendTime + "_" + i);
				String[] sCards = pcards.split(",");
				List<Byte> hcards = new ArrayList<Byte>();
				for (String sCard : sCards) {
					byte c=0;
					if(sCard.contains("0x")){
						sCard=sCard.replace("0x", "");
						c=(byte)Integer.parseInt(sCard,16);
					}else {
						c=Byte.parseByte(sCard);
					}
					hcards.add(c);
				}
				cardsMap.put(pl.getTablePos(), hcards);
				break;
			}
		}
		return cardsMap;
		
	}

	public int getPeiCardSendTime() {
		return peiCardSendTime;
	}

	public void setPeiCardSendTime(int peiCardSendTime) {
		this.peiCardSendTime = peiCardSendTime;
	}
}
