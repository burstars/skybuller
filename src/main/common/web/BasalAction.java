package com.chess.common.web;

import com.opensymphony.xwork2.ActionSupport;

/**基本Action2*/
@SuppressWarnings("serial")
public class BasalAction extends ActionSupport{
	
	/**action对象之间通信*/
	protected  String actionMsg;
		

	public String getActionMsg() {
		return actionMsg;
	}

	public void setActionMsg(String actionMsg) {
		this.actionMsg = actionMsg;
	}
	

}
