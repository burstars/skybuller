/**
 * 用于分页的action（struts2）
 */
package com.chess.common.web;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;




import com.chess.common.StringUtils;
import com.opensymphony.xwork2.ActionContext;

/**
 */
public class QueryForPage extends BasalAction {
	protected int page; // 当前的页码

	protected int pageSize = 20; // 设置每页显示多少条记录

	protected String startRownum; // 用于分页的开始数

	protected String endRownum; // 用于分页的结束数

	protected int totalPage; // 用于分页的总页数
	/**
	 * E.FLY 2012-06-12 15:12:22
	 */
	protected int totalPage2; // 用于首页分页的总页数，因为一个页面存在两个totalPage

	protected int total; // 用于分页的总记录条数


	/**
	 * 用于分页的方法
	 * 
	 * @param count
	 *            查询数据库后获得的返回结果
	 */
	protected void doPage(int count) {
		total = count; // 总记录数=参数count
		totalPage = total / pageSize; // 计算出一共有多少页
		int remainder = total % pageSize; // 记录总记录数除以页面大小，是否有余数
		if (total % pageSize != 0) { // 如果有余数
			totalPage += 1; // 总页数+1
		}

		if (page >= totalPage) { // 如果页面大于等于总页
			page = totalPage; // 当前页是末页
			if (remainder != 0) { // 如果没有余数
				startRownum = String.valueOf(total - remainder); // 起始数=总记录数-余数
				endRownum = String.valueOf(total); // 结束数=最大记录数
				return;
			}
		}
		if (page <= 0) { // 如果当前页数小于或者等于0
			page = 1; // 当前页码是首页
		}

		startRownum = String.valueOf((page - 1) * pageSize); // 起始数=(页码-1)*页面大小

		endRownum = String.valueOf(pageSize); // 结束数=页面大小
	}

	protected void doPage2(int count) {
		total = count; // 总记录数=参数count
		totalPage2 = total / pageSize; // 计算出一共有多少页
		int remainder = total % pageSize; // 记录总记录数除以页面大小，是否有余数
		if (total % pageSize != 0) { // 如果有余数
			totalPage2 += 1; // 总页数+1
		}

		if (page >= totalPage2) { // 如果页面大于等于总页
			page = totalPage2; // 当前页是末页
			if (remainder != 0) { // 如果没有余数
				startRownum = String.valueOf(total - remainder); // 起始数=总记录数-余数
				endRownum = String.valueOf(total); // 结束数=最大记录数
				return;
			}
		}
		if (page <= 0) { // 如果当前页数小于或者等于0
			page = 1; // 当前页码是首页
		}

		startRownum = String.valueOf((page - 1) * pageSize); // 起始数=(页码-1)*页面大小

		endRownum = String.valueOf(pageSize); // 结束数=页面大小
	}

	/**
	 * 重载用于分页的方法，添加设置页面大小的参数
	 * 
	 * @param count
	 *            查询数据库后获得的返回结果
	 * @param pageSize
	 *            设置页面大小
	 */
	protected void doPage(int count, int pageSize) {
		this.pageSize = pageSize;
		doPage(count);
	}

	protected void doPage2(int count, int pageSize) {
		this.pageSize = pageSize;
		doPage2(count);
	}


	

	/**
	 * 获取session
	 * 
	 * @throws IOException
	 */
	protected Map getSessionMap() {
		ActionContext context = ActionContext.getContext();
		return context.getSession();
	}



	// 新建文件夹目录
	public void newFolder(String folderPath) {
		String filePath = folderPath.toString();
		java.io.File myFilePath = new java.io.File(filePath);
		try {
			if (myFilePath.isDirectory()) {
//				System.out.println("the directory is exists!");
			} else {
				myFilePath.mkdir();
//				System.out.println("新建目录成功");
			}
		} catch (Exception e) {
//			System.out.println("新建目录操作出错");
			e.printStackTrace();
		}
	}


	/**
	 * 负责为ajax提供json信息
	 * 
	 * @param xmlStr
	 */
	protected void print(String xmlStr) {

		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("text/xml");
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new OutputStreamWriter(
					response.getOutputStream(), "UTF-8"));
			pw.print(xmlStr);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 关闭流
			if (!StringUtils.isEmptyOrNull(pw))
				pw.close();
		}
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getStartRownum() {
		return startRownum;
	}

	public void setStartRownum(String startRownum) {
		this.startRownum = startRownum;
	}

	public String getEndRownum() {
		return endRownum;
	}

	public void setEndRownum(String endRownum) {
		this.endRownum = endRownum;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getTotalPage2() {
		return totalPage2;
	}

	public void setTotalPage2(int totalPage2) {
		this.totalPage2 = totalPage2;
	}


}
