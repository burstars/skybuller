package com.chess.common.web.constant;

public class AdminSystemConstant {

	/** session KEY */
	public static final String ADMIN_SYSTEM_SESSION_KEY = "ADMIN_SYSTEM_USER_NAME";
	
	/**管理员之间的Session通信信息*/
	public static final String ADMIN_SESSION_COMMUNICATION_MESSAGE =  "adminMsg";
}
