package com.chess.common.web;

import java.util.HashMap;
import java.util.Map;

import com.chess.common.bean.oss.OssUser;

 

/**管理员登录环境*/
public class BaseAdminContext {
	
	/**管理员*/
	private OssUser ossUser;
	/**需要过滤的路径与权限*/
	private Map<String,Boolean> menuMap = new HashMap<String,Boolean>();
	
	/**SSO端的的管理员sessionId*/
	private String sessionId;
	
	
	/**判断用户是否有权限*/
	public Boolean allow(String key){
		
		Map<String,Boolean> temMap = new HashMap<String,Boolean>();//所有匹配的
		for(String url : menuMap.keySet()){
			if(key.indexOf(url)>=0){
				temMap.put(url, menuMap.get(url));
			}
		}
		
		if(temMap.size()!=0){
			//取出字符最长的与之匹配
			String overKey = null;
			for(String u:temMap.keySet()){
				if(overKey ==null) overKey = u;
				if(overKey.length()<u.length()) overKey = u;
			}
			return temMap.get(overKey);
		}else{//不需要过滤的请求
			return true;
		}
	}

 
	public Map<String, Boolean> getMenuMap() {
		return menuMap;
	}

	public void setMenuMap(Map<String, Boolean> menuMap) {
		this.menuMap = menuMap;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	public OssUser getOssUser() {
		return ossUser;
	}


	public void setOssUser(OssUser ossUser) {
		this.ossUser = ossUser;
	}


	

}
