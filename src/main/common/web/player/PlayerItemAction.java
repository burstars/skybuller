package com.chess.common.web.player;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpUtils;

import net.sf.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.struts2.ServletActionContext;

import com.chess.common.HttpUtil;
import com.chess.common.SpringService;
import com.chess.common.bean.MallItem;
import com.chess.common.bean.SystemConfigPara;
import com.chess.common.bean.User;
import com.chess.common.bean.UserBackpack;
import com.chess.common.bean.alipay.util.AlipayNotify;
import com.chess.common.bean.thirdpay.ThirdPayService;
import com.chess.common.bean.wxpay.ResponseHandler;
import com.chess.common.bean.wxpay.WxConstantUtil;
import com.chess.common.bean.wxpay.WxPayService;
import com.chess.common.constant.ConfigConstant;
import com.chess.common.constant.LogConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.ISystemConfigService;
import com.chess.common.service.IUserService;
import com.chess.common.web.QueryForPage;

@SuppressWarnings("serial")
public class PlayerItemAction extends QueryForPage {
	private static Logger logger = LoggerFactory
			.getLogger(PlayerItemAction.class);

	private List<UserBackpack> playerItems;

	private UserBackpack playerItem;

	private String account;

	private int index;

	private String playerName;

	// -----批量增加房卡-----------
	// 房卡类型，itemBaseID
	private int itemType = 0;
	// 每人增加数量
	private int itemNum = 0;
	// 人数
	private int playerNum = 0;
	// 玩家ID
	private String playerIndexStr = "";

	// 成功增加房卡玩家数量
	private int successPlayerNum = 0;
	// 成功增加房卡的玩家ID
	private String successPlayerIDs = "";

	// 失败玩家数量
	private int failedPlayerNum = 0;
	// 失败ID
	private String failedPlayerIDs = "";

	// 错误信息
	private String errorMsg = "";

	private String itemNumStr = "";

	private String gameID = "";

	// public String queryPlayerItem() {
	// return SUCCESS;
	// }

	// public String browsePlayerItem() {
	// IUserService playerBaseService = (IUserService) SpringService
	// .getBean("playerBaseService");
	// // Player player = playerBaseService.getPlayerByAccount(account);
	// User player = playerBaseService.getPlayerByPlayerIndex(index);
	//
	// if (player == null) {
	// return "failed";
	// }
	// playerItems = playerBaseService.getPlayerItemByID(player.getPlayerID());
	//
	// return "browsePlayerItem";
	// }
	public String getItemNumStr() {
		return itemNumStr;
	}

	public void setItemNumStr(String itemNumStr) {
		this.itemNumStr = itemNumStr;
	}

	public String playerItems() throws IOException {
		logger.info("----代理或管理开始充卡----充卡人：" + playerIndexStr + "充卡数量"
				+ itemNumStr);
		String returnstrString = "";
		String playerid = ServletActionContext.getRequest().getParameter(
				"playerID");
		;
		List<MallItem> allItemBaseList;
		ICacheUserService playerService = (ICacheUserService) SpringService
				.getBean("playerService");

		allItemBaseList = playerService.getAllItemBaseTable();

		User pl = playerService.getPlayerByPlayerID(playerid);
		Map<Integer, UserBackpack> playerItemMap = new HashMap<Integer, UserBackpack>();
		if (pl != null) {
			playerItems = new ArrayList<UserBackpack>();
			playerName = pl.getPlayerName();
		} else {
			returnstrString = "failed";
		}

		if (playerItems != null) {
			// 新注册的玩家，下线后，
			// 群主后台给该玩家充卡，游戏后台显示卡为0
			// 这里添加刷新数据库的功能
			playerService.load_player_items(pl);
			// add end
			for (UserBackpack pitem : pl.getItems()) {
				playerItemMap.put(pitem.getItemBaseID(), pitem);
				if (pitem.getItemBaseID() > 3000
						&& pitem.getItemBaseID() < 4000) {
					playerItems.add(pitem);
				}
			}
		}

		for (MallItem item : allItemBaseList) {
			if (item.getBase_id() < 3000 || item.getBase_id() > 4000
					|| item.getProperty_4() > 1)
				continue;

			if (!playerItemMap.containsKey(item.getBase_id())) {
				UserBackpack newPItem = new UserBackpack();
				newPItem.setPlayerID(playerid);
				newPItem.setItemID(playerid + "_" + item.getBase_id());
				newPItem.setItemBaseID(item.getBase_id());
				newPItem.setItemNum(0);
				newPItem.setItemName(item.getName());
				newPItem.setDescription(item.getDescription());
				playerItems.add(newPItem);
			} else {
				UserBackpack newPItem = playerItemMap.get(item.getBase_id());
				newPItem.setItemName(item.getName());
				newPItem.setDescription(item.getDescription());
			}

		}

		JSONArray json = JSONArray.fromObject(playerItems);
		returnstrString += json;
		returnstrString = URLEncoder.encode(returnstrString, "UTF-8");
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
		return null;
	}

//	public String playerThirdPayBack() throws IOException {
//		HttpServletRequest request = ServletActionContext.getRequest();
//		// 订单号
//		String orderid = request.getParameter("orderid");
//		// 操作结果
//		String opstate = request.getParameter("opstate");
//		// 金额
//		String ovalue = request.getParameter("ovalue");
//		// md5签名
//		String sign = request.getParameter("sign");
//		// 支付系统订单ID
//		String sysorderid = request.getParameter("sysorderid");
//		// 支付系统订单时间
//		String completiontime = request.getParameter("completiontime");
//		// 备注信息
//		String attach = request.getParameter("attach");
//		// 结果说明
//		String msg = request.getParameter("msg");
//		
//		logger.info("接到第三方支付回调信息：订单号【"+orderid+"】，操作结果【"+opstate+"】，金额【"+ovalue+"】，MD5签名【"+sign
//				+"】，支付系统订单ID【"+sysorderid+"】，支付系统订单时间【"+completiontime+"】，备注信息【"+attach+"】，结果说明【"+msg+"】");
//		
//		String ropstate = "0";
//		if("0".equals(opstate.trim()) || "-3".equals(opstate.trim())){
//			// 成功
//			ThirdPayService.updateOrder(orderid);
//			ropstate = "0";
//			
//			ServletActionContext.getResponse().getWriter().write(ropstate);
//			ServletActionContext.getResponse().getWriter().flush();
//		}
//		return null;
//	}
	
	public String playerThirdPayBack() throws IOException {
		HttpServletRequest request = ServletActionContext.getRequest();
		
		try{
			// 订单号
			String merchantTradeNo = request.getParameter("merchantTradeNo");
			// 操作结果
			String payStatus = request.getParameter("payStatus");
			// 金额
			String payFee = request.getParameter("payFee");
			// md5签名
			String sign = request.getParameter("sign");
			// 支付系统订单ID
			String callbackTradeNo = request.getParameter("callbackTradeNo");
			// 支付类型
			String payType = request.getParameter("payType");
			
			logger.info("接到第三方支付回调信息：订单号【"+merchantTradeNo+"】，操作结果【"+payStatus+"】，金额【"+payFee+"】，MD5签名【"+sign
					+"】，支付系统订单ID【"+callbackTradeNo+"】，支付类型【"+payType+"】");
			
			Map<String, String> params = new TreeMap<String, String>();
			params.put("payStatus", payStatus);
			params.put("payFee", payFee);
			params.put("callbackTradeNo", callbackTradeNo);
			params.put("payType", payType);
			params.put("merchantTradeNo", merchantTradeNo);
			logger.info("第三方支付回调处理，准备生成签名，params=" + params);
			String ssign = ThirdPayService.prepareSign(params);
			logger.info("第三方支付请求处理，签名，sign=" + ssign);
			
			logger.info("接到第三方支付回调信息：订单号【"+merchantTradeNo+"】，操作结果【"+payStatus+"】，金额【"+payFee+"】，MD5签名【"+sign
					+"】，支付系统订单ID【"+callbackTradeNo+"】，支付类型【"+payType+"】");
			
			if(ssign.equals(sign)){
				// 签名校验通过
				logger.info("接到第三方支付回调信息：订单号【"+merchantTradeNo+"】，操作结果【"+payStatus+"】，金额【"+payFee+"】，MD5签名【"+sign
						+"】，支付系统订单ID【"+callbackTradeNo+"】，支付类型【"+payType+"】，签名校验通过，系统生成签名【"+ssign+"】");
				
				if("SUCCESS".equals(payStatus.trim())){
					logger.info("接到第三方支付回调信息：订单号【"+merchantTradeNo+"】，操作结果【"+payStatus+"】，金额【"+payFee+"】，MD5签名【"+sign
							+"】，支付系统订单ID【"+callbackTradeNo+"】，支付类型【"+payType+"】，支付成功，开始发卡");
					
					// 成功
					ThirdPayService.updateOrder(merchantTradeNo);
					
					ServletActionContext.getResponse().getWriter().write("SUCCESS");
					ServletActionContext.getResponse().getWriter().flush();
				}else{
					logger.error("接到第三方支付回调信息：订单号【"+merchantTradeNo+"】，操作结果【"+payStatus+"】，金额【"+payFee+"】，MD5签名【"+sign
							+"】，支付系统订单ID【"+callbackTradeNo+"】，支付类型【"+payType+"】，支付失败");
					
					ServletActionContext.getResponse().getWriter().write("FAIL");
					ServletActionContext.getResponse().getWriter().flush();
				}
			}else{
				ServletActionContext.getResponse().getWriter().write("FAIL");
				ServletActionContext.getResponse().getWriter().flush();
				
				logger.error("接到第三方支付回调信息：订单号【"+merchantTradeNo+"】，操作结果【"+payStatus+"】，金额【"+payFee+"】，MD5签名【"+sign
						+"】，支付系统订单ID【"+callbackTradeNo+"】，支付类型【"+payType+"】，签名校验未通过，系统生成签名【"+ssign+"】");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * 批量增加房卡
	 * 
	 * @throws IOException
	 */
	public String addItemForPlayers() throws IOException {
		errorMsg = "增加房卡失败";

		if (playerNum <= 0 || playerIndexStr == null || playerIndexStr == "") {
			errorMsg = "玩家ID参数错误";
			errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
			ServletActionContext.getResponse().getWriter().write(errorMsg);
			ServletActionContext.getResponse().getWriter().flush();
			return null;
		}

		if (playerNum > 50) {
			errorMsg = "玩家数量超过50";
			errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
			ServletActionContext.getResponse().getWriter().write(errorMsg);
			ServletActionContext.getResponse().getWriter().flush();
			return null;
		}

		ICacheUserService playerService = (ICacheUserService) SpringService
				.getBean("playerService");
		IUserService playerBaseService = (IUserService) SpringService
				.getBean("playerBaseService");

		// 判断道具信息
		MallItem item = playerService.getMallItem(itemType);
		if (item == null) {
			errorMsg = "道具类型错误";
			errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
			ServletActionContext.getResponse().getWriter().write(errorMsg);
			ServletActionContext.getResponse().getWriter().flush();
			return null;
		}

		// 拆分玩家ID
		String[] playerIDs = playerIndexStr.split("\\,");
		// 成功增加房卡的玩家ID
		successPlayerIDs = "";
		successPlayerNum = 0;
		failedPlayerIDs = "";
		failedPlayerNum = 0;
		String[] itemNums = itemNumStr.split("\\,");

		if (playerNum != playerIDs.length) {
			errorMsg = "玩家数量不对";
			errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
			ServletActionContext.getResponse().getWriter().write(errorMsg);
			ServletActionContext.getResponse().getWriter().flush();
			return null;
		}
		if (gameID == null) {
			gameID = "";
			/*
			 * 处理主产品ID cc 2017-12-20
			 */
			ISystemConfigService cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
			SystemConfigPara configProduct = cfgService
					.getPara(ConfigConstant.MAIN_PRODUCT_ID);
			if (configProduct != null && configProduct.getValueStr() != null
					&& !"".equals(configProduct.getValueStr())) {
				gameID = configProduct.getValueStr();
			} else {
				gameID = "ddz";
			}
		}
		for (int i = 0; i < playerIDs.length; i++) {
			int playerIndex = Integer.parseInt(playerIDs[i]);
			User plm = playerBaseService.getPlayerByPlayerIndex(playerIndex); // 只为获取玩家ID
			if (plm == null) {
				failedPlayerNum++;
				failedPlayerIDs += playerIndex + ",";
				errorMsg = "没找到玩家";
				errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
				ServletActionContext.getResponse().getWriter().write(errorMsg);
				ServletActionContext.getResponse().getWriter().flush();
				return null;
			}

			// 读取缓存中的玩家信息,不能用plm
			User pl = playerService.getPlayerByPlayerID(plm.getPlayerID());
			if (pl == null) {
				failedPlayerNum++;
				failedPlayerIDs += playerIndex + ",";
				errorMsg = "没找到玩家";
				errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
				ServletActionContext.getResponse().getWriter().write(errorMsg);
				ServletActionContext.getResponse().getWriter().flush();
				return null;
			}

			int num = Integer.parseInt(itemNums[i]);
			if (num < 0) {
				UserBackpack itemCard = playerService.getAllItemByPlayerId(
						gameID, pl.getPlayerID(), item.getBase_id());
				if (itemCard == null) {
					errorMsg = "代理房卡不足";
					errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
					ServletActionContext.getResponse().getWriter().write(
							errorMsg);
					ServletActionContext.getResponse().getWriter().flush();
					return null;
				} else {
					if (itemCard.getItemNum() + num < 0) {
						errorMsg = "代理房卡不足";
						errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
						ServletActionContext.getResponse().getWriter().write(
								errorMsg);
						ServletActionContext.getResponse().getWriter().flush();
						return null;
					}
				}
			}

			successPlayerNum++;
			successPlayerIDs += pl.getPlayerIndex() + ",";
		}

		playerService.addItemForPlayer(playerIndexStr, itemNumStr, itemType,
				gameID);
		ServletActionContext.getResponse().getWriter().write("success");
		ServletActionContext.getResponse().getWriter().flush();
		return null;
	}

	/**
	 * 批量设置用户代理开房权限
	 * 
	 * @throws IOException
	 */
	public String setProxyOpenRoomForPlayers() throws Exception {
		errorMsg = "设置代开房间权限失败";

		if (playerNum <= 0 || playerIndexStr == null || playerIndexStr == "") {
			errorMsg = "玩家ID参数错误";
			errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
			ServletActionContext.getResponse().getWriter().write(errorMsg);
			ServletActionContext.getResponse().getWriter().flush();
			return null;
		}

		if (playerNum > 50) {
			errorMsg = "玩家数量超过50";
			errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
			ServletActionContext.getResponse().getWriter().write(errorMsg);
			ServletActionContext.getResponse().getWriter().flush();
			return null;
		}

		ICacheUserService playerService = (ICacheUserService) SpringService
				.getBean("playerService");
		IUserService playerBaseService = (IUserService) SpringService
				.getBean("playerBaseService");

		// 拆分玩家ID
		String[] playerIDs = playerIndexStr.split("\\,");
		String[] itemNums = itemNumStr.split("\\,");
		// 成功设置权限的玩家ID
		successPlayerIDs = "";
		failedPlayerIDs = "";
		if (playerNum != playerIDs.length) {
			errorMsg = "玩家数量不对";
			errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
			ServletActionContext.getResponse().getWriter().write(errorMsg);
			ServletActionContext.getResponse().getWriter().flush();
			return null;
		}

		for (int i = 0; i < playerIDs.length; i++) {
			int playerIndex = Integer.parseInt(playerIDs[i]);
			User plm = playerBaseService.getPlayerByPlayerIndex(playerIndex); // 只为获取玩家ID
			if (plm == null) {
				failedPlayerNum++;
				failedPlayerIDs += playerIndex + ",";
				errorMsg = "没找到玩家";
				errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
				ServletActionContext.getResponse().getWriter().write(errorMsg);
				ServletActionContext.getResponse().getWriter().flush();
				return null;
			}

			// 读取缓存中的玩家信息,不能用plm
			User pl = playerService.getPlayerByPlayerID(plm.getPlayerID());
			if (pl == null) {
				failedPlayerNum++;
				failedPlayerIDs += playerIndex + ",";
				errorMsg = "没找到玩家";
				errorMsg = URLEncoder.encode(errorMsg, "UTF-8");
				ServletActionContext.getResponse().getWriter().write(errorMsg);
				ServletActionContext.getResponse().getWriter().flush();
				return null;
			}
			successPlayerNum++;
			successPlayerIDs += pl.getPlayerIndex() + ",";
		}

		playerService.setProxyOpenRoomForPlayer(playerIndexStr, itemNumStr);
		ServletActionContext.getResponse().getWriter().write("success");
		ServletActionContext.getResponse().getWriter().flush();
		return null;
	}
	
	/**
     * 修改俱乐部权限
     * 参数：clubType:玩家id，
     * **/
    public String updateClubType() throws IOException {
    	String returnstrString="failed";
    	int playerIndex = Integer.parseInt(ServletActionContext.getRequest().getParameter("playerIndex"));
    	int clubType = Integer.parseInt(ServletActionContext.getRequest().getParameter("clubType"));
		
		ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
		boolean isSuccess =false;
        if (playerService != null) {
        	logger.info("----修改俱乐部权限：playerIndex : "+playerIndex+" ----- clubCode：" + clubType + "  ------");
            isSuccess = playerService.updateClubType(playerIndex, clubType);
        }
        if (isSuccess) {
        	returnstrString="success";
        }

		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
        return null;
    }
    
    /**
     * 修改俱乐部状态
     * 参数：clubCode:玩家id，
     * clubState：状态；1=有效;2=代理解散;3=系统收回
     * **/
    public String updateClubState() throws IOException {
    	String returnstrString="failed";
    	int playerIndex = Integer.parseInt(ServletActionContext.getRequest().getParameter("playerIndex"));
    	int clubCode = Integer.parseInt(ServletActionContext.getRequest().getParameter("clubCode"));
		int clubState = Integer.parseInt(ServletActionContext.getRequest().getParameter("clubState"));
		
		ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
		boolean isSuccess =false;
        if (playerService != null) {
        	logger.info("----修改俱乐部状态：clubCode：" + clubCode + "  ------ clubState : " + clubState + " ------");
            isSuccess = playerService.updateClubState(clubCode, clubState, playerIndex);
        }
        if (isSuccess) {
        	returnstrString="success";
        }

		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
        return  null;
    }

	public List<UserBackpack> getPlayerItems() {
		return playerItems;
	}

	public void setPlayerItems(List<UserBackpack> playerItems) {
		this.playerItems = playerItems;
	}

	public UserBackpack getPlayerItem() {
		return playerItem;
	}

	public void setPlayerItem(UserBackpack playerItem) {
		this.playerItem = playerItem;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getItemType() {
		return itemType;
	}

	public void setItemType(int itemType) {
		this.itemType = itemType;
	}

	public int getItemNum() {
		return itemNum;
	}

	public void setItemNum(int itemNum) {
		this.itemNum = itemNum;
	}

	public int getPlayerNum() {
		return playerNum;
	}

	public void setPlayerNum(int playerNum) {
		this.playerNum = playerNum;
	}

	public String getPlayerIndexStr() {
		return playerIndexStr;
	}

	public void setPlayerIndexStr(String playerIndexStr) {
		this.playerIndexStr = playerIndexStr;
	}

	public String getSuccessPlayerIDs() {
		return successPlayerIDs;
	}

	public void setSuccessPlayerIDs(String successPlayerIDs) {
		this.successPlayerIDs = successPlayerIDs;
	}

	public int getSuccessPlayerNum() {
		return successPlayerNum;
	}

	public void setSuccessPlayerNum(int successPlayerNum) {
		this.successPlayerNum = successPlayerNum;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public int getFailedPlayerNum() {
		return failedPlayerNum;
	}

	public void setFailedPlayerNum(int failedPlayerNum) {
		this.failedPlayerNum = failedPlayerNum;
	}

	public String getFailedPlayerIDs() {
		return failedPlayerIDs;
	}

	public void setFailedPlayerIDs(String failedPlayerIDs) {
		this.failedPlayerIDs = failedPlayerIDs;
	}
}
