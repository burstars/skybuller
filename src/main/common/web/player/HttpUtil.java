package com.chess.common.web.player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpUtil {
	private static Logger logger = LoggerFactory.getLogger(HttpUtil.class);

	public static String post(String action,Map<String, String>map) throws HttpException, IOException{
		String url = action;
		HttpClient httpclient = new HttpClient();
		String host =map.get("_HOST");
		httpclient.getHostConfiguration().setHost(host,Integer.parseInt(map.get("_PORT")),"http");
		PostMethod pt = new PostMethod(url);
		
		Iterator it = map.entrySet().iterator();
		 
		  while (it.hasNext())
		  {
			  Map.Entry pairs = (Map.Entry) it.next();
			  pt.addParameter(pairs.getKey().toString(),pairs.getValue().toString());
		  }
		  
		  HttpMethod method = pt;
		  
		  String response = null;
		  try {
			    httpclient.executeMethod(method);
			  	int statusCode=method.getStatusCode();
			  	if(statusCode==200){
			  		response = method.getResponseBodyAsString();
			  	}else{
			  		response ="<html>";
			  	}
			} catch (Exception e) {
				response ="<html>";
				e.printStackTrace();
			}finally{
				method.releaseConnection();
			}
		  
		return response;
	}
	
	public static String get(String urlStr, String charset) {
		logger.info(" url = " + urlStr);
        StringBuffer resultBuffer = null;  
        HttpURLConnection conn = null;  
        BufferedReader br = null;  
        try {  
            URL url = new URL(urlStr);  
            conn = (HttpURLConnection) url.openConnection();  
            conn.setRequestMethod("GET");  
            conn.setRequestProperty("contentType", charset);
            conn.setDoOutput(true);  
            conn.setDoInput(true);  
            conn.setUseCaches(false);  
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");  
            conn.connect();  
            
            resultBuffer = new StringBuffer();  
            br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));  
            String result = "";  
            while ((result = br.readLine()) != null) {  
                resultBuffer.append(result);  
            }  
            
        } catch (Exception e) {  
            throw new RuntimeException(e);  
        } finally {  
            if (br != null) {  
                try {  
                    br.close();  
                } catch (IOException e) {  
                    br = null;  
                    throw new RuntimeException(e);  
                } 
            }  
            
            if (conn != null) {  
                conn.disconnect();  
                conn = null;  
            }  
        }  
        return resultBuffer.toString();  
    }
}
