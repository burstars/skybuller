package com.chess.common.web.player;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.MD5Service;
import com.chess.common.SpringService;
import com.chess.common.bean.ActiveCode;
import com.chess.common.bean.MallItem;
import com.chess.common.bean.ShopGoods;
import com.chess.common.bean.SystemConfigPara;
import com.chess.common.bean.User;
import com.chess.common.bean.UserBackpack;
import com.chess.common.constant.ConfigConstant;
import com.chess.common.constant.GameConstant;
import com.chess.common.dao.IVipRoomRecordDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.service.IActiveCodeService;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.IMallItemService;
import com.chess.common.service.ISystemConfigService;
import com.chess.common.service.IUserService;
import com.chess.common.web.QueryForPage;
import com.ibm.icu.util.Calendar;

import flex.messaging.util.URLDecoder;

/**
 * 玩家信息 如果struts 使用通配符*等xwork2的特性的话，必须继承BasalAction 类（继承xwork2 的框架ActionSupport
 * 类），不能用继承BaseAction
 */
public class PlayerAction extends QueryForPage {
	private static Logger logger = LoggerFactory
    .getLogger(PlayerAction.class);
	
	private List<User> playerList;

	private User playerBean;
	
	private int index;

	private String id;

	private String account;
	
	private int queryType = 0;
	
	private int order = 0;
	
	private String startTime = null;
	
	private String endTime = null;
	
	private int receiveIndex = 0;
	
	private int gold = 0;
	
	private int queryPlayerType = 0;
	
	private String password;
	
	/**统计卡数
	 * @throws ParseException */
	public String getSellCard() throws IOException, ParseException{
		String returnstrString="";
		//充值卡数
		String startDate = ServletActionContext.getRequest().getParameter("startDate");
		String endDate = ServletActionContext.getRequest().getParameter("endDate");
		String days = ServletActionContext.getRequest().getParameter("days");
		int daynum=Integer.parseInt(days);
		//售卡数量;天数参数内，消耗卡数
		ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");
		IVipRoomRecordDAO vipRoomRecordDAO=dbplayerService.getVipRoomRecordDAO();
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
		Calendar startTime=Calendar.getInstance();
		startTime.setTime(sdf.parse(startDate));
		Map<String, String> mmap =new HashMap<String, String>();
		for(int i=0;i<daynum;i++){
			if(i>0)
				startTime.add(Calendar.DATE, 1);
			Date sTime=startTime.getTime();
			String sDate=sdf.format(sTime);
			mmap.put("sDate", sDate);
			returnstrString+=sDate+"_";
			sDate=sDate.replace("-", "");
			//售卡数量
			IMallItemService mallItemService = (IMallItemService) SpringService.getBean("mallItemService");
			int sell_card =mallItemService.getSellCard(mmap);
			//天数参数内，消耗卡数
			String gameId = GameContext.gameId;
			String tableName = "t_vip_room_record__" + gameId + "__" + sDate;
			String vipRoom=vipRoomRecordDAO.checkVipRoomRecordTableIsExists(tableName);
			int use_card=0;
			if(vipRoom!=null && !"".equals(vipRoom)){
				try {
					use_card=vipRoomRecordDAO.getVipRoomIndexCount(tableName);
				} catch (Exception e) {
					
				}
			}
			returnstrString+=sell_card+"_"+use_card;
			if(i!=daynum-1)
				returnstrString+=",";
		}
		
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
		return null;
	}
	
	/**创建兑换码*/
	public String createActiveCode() throws IOException{
		String returnstrString="sueecss";
		//获取参数
		String active_code = ServletActionContext.getRequest().getParameter("active_code");
		String is_value = ServletActionContext.getRequest().getParameter("is_value");
		String rule_id = ServletActionContext.getRequest().getParameter("rule_id");
		String start_date = ServletActionContext.getRequest().getParameter("start_date");
		String end_date = ServletActionContext.getRequest().getParameter("end_date");
		String card = ServletActionContext.getRequest().getParameter("card");
		String money = ServletActionContext.getRequest().getParameter("money");
		String exchange_code = ServletActionContext.getRequest().getParameter("exchange_code");
		//
		String[] excodes=exchange_code.split(",");
		for(int i=0;i<excodes.length;i++){
			ActiveCode activeCode = new ActiveCode();
			activeCode.setActive_code(active_code);
			activeCode.setIs_value(is_value);
			activeCode.setRule_id(rule_id);
			activeCode.setStart_date(start_date);
			activeCode.setEnd_date(end_date);
			activeCode.setCard(Integer.parseInt(card));
			activeCode.setMoney(Integer.parseInt(money));
			activeCode.setExchange_code(excodes[i]);						
			IActiveCodeService activeCodeService = (IActiveCodeService)SpringService.getBean("activeCodeService");
			try {
				activeCodeService.createActiveCode(activeCode);
			} catch (Exception e) {
				returnstrString="failed";
				break;
			}
		}

		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
		return null;
		
	}

	public String getMallItemInfo() throws IOException{
		
		String type = ServletActionContext.getRequest().getParameter("type");
		String returnstrString="";
		IMallItemService mallItemService = (IMallItemService) SpringService.getBean("mallItemService");
		List<MallItem> mallList =  mallItemService.refreshItemBase();
		for(int i=0;i<mallList.size();i++){
			if(type.equals(Integer.toString(mallList.get(i).getBase_id()))){
				returnstrString = Integer.toString(mallList.get(i).getPrice());
			}
		}
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
		return null;
	}
	public String addCard() throws IOException{
		String playerID = ServletActionContext.getRequest().getParameter("playerID");
		int itemBaseID =  Integer.parseInt(ServletActionContext.getRequest().getParameter("itemBaseID"));
		int itemNum =  Integer.parseInt(ServletActionContext.getRequest().getParameter("itemNum"));
		IUserService userbackpace = (IUserService) SpringService.getBean("playerBaseService");
		User user=null;
		String returnstrString="sueecss";
		if(queryType == 0){//ID
			try {
				user=userbackpace.getPlayerByPlayerIndex(Integer.parseInt(playerID));
			} catch (Exception e) {
				returnstrString="failed";
			}
			
		}else{//帐号
			user=userbackpace.getPlayerByAccount(playerID);
		}
		
		if(user!=null){
			playerID=user.getPlayerID();
			userbackpace.updatePlayerItemNum(playerID, itemBaseID, itemNum);
		}else{
			returnstrString="failed";
		}
		
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
		return null;
		
	}
	/**插入代理卡数*/
	public String insertProxyCard() throws IOException{
		String playerID = ServletActionContext.getRequest().getParameter("playerID");
		int itemBaseID =  Integer.parseInt(ServletActionContext.getRequest().getParameter("itemBaseID"));
		int itemNum =  Integer.parseInt(ServletActionContext.getRequest().getParameter("itemNum"));
		String itemID = playerID+"_"+itemBaseID; 
		IUserService userbackpace = (IUserService) SpringService.getBean("playerBaseService");

		ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");
		if(!dbplayerService.buy_item_rmb(playerID, itemBaseID, itemNum,GameConstant.OPT_BUY_ITEM,"")){
			UserBackpack item  = new UserBackpack();
			item.setItemID(itemID);
			item.setPlayerID(playerID);
			item.setItemNum(itemNum);
			item.setItemBaseID(itemBaseID);
			userbackpace.createPlayerItem(item);
		}else{
			int itemnum=0;
			List<UserBackpack> userBackpacks=userbackpace.getPlayerItemByID(playerID);
			if(userBackpacks.size()>0){
				UserBackpack userBackpack =userBackpacks.get(0);
				itemnum=userBackpack.getItemNum();
			}
			userbackpace.updatePlayerItemNum(playerID,itemBaseID,itemnum+itemNum);
		}
		
		ServletActionContext.getResponse().getWriter().write("sueecss");
		ServletActionContext.getResponse().getWriter().flush();
		return null;
		
	}
	
	public String getCard1() throws IOException {
		String playerID = ServletActionContext.getRequest().getParameter("playerID");
		String itemBaseID=ServletActionContext.getRequest().getParameter("itemBaseID");
		String returnstrString="";
		IUserService userbackpaceBackpackDAO = (IUserService) SpringService.getBean("playerBaseService");
		User user=null;
		if(queryType == 0){
			try {
				user=userbackpaceBackpackDAO.getPlayerByPlayerIndex(Integer.parseInt(playerID));
			} catch (Exception e) {
				returnstrString="failed";
			}
			
		}else{
			user=userbackpaceBackpackDAO.getPlayerByAccount(playerID);
		}
		if(user!=null){
			playerID=user.getPlayerID();
			List<UserBackpack> list =  userbackpaceBackpackDAO.getPlayerItemByID(playerID);
			for(int i =0 ;i<list.size();i++)
			{
				if(itemBaseID==null){
					returnstrString="0";
					break;
				}else{
					if(list.get(i).getItemBaseID()==Integer.parseInt(itemBaseID))
					{
						returnstrString = Integer.toString(list.get(i).getItemNum());
					}
				}
			}
			if(list.size()==0){
				returnstrString="0";
			}
		}else{
			returnstrString="failed";
		}
		
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
		return null;
	}
	
	/**获取帐号信息*/
//	public String getUsersInfo() throws IOException {
//		IUserService playerService = (IUserService) SpringService.getBean("playerBaseService");
//		String itemBaseID=ServletActionContext.getRequest().getParameter("itemBaseID");
//		String returnstrString="failed";
//		if(account!=null && !account.equals(""))
//		{
//			returnstrString="";
//			String accounts[] =account.split(",");
//			for(int i=0;i<accounts.length;i++){
////				User user=playerService.getPlayerByAccount(accounts[i].replace(" ", ""));
////				if(user!=null){
////					returnstrString+=user.getAccount()+"_";
////					returnstrString+= URLEncoder.encode(user.getPlayerName(),"UTF-8")+",";
////				}
//				User user=null;
//				if(queryType == 0){//ID
//					user=playerService.getPlayerByPlayerIndex(Integer.parseInt(accounts[i].replace(" ", "")));
//				}else{//帐号
//					user=playerService.getPlayerByAccount(accounts[i].replace(" ", ""));
//				}
//				if(user!=null){
//					if(queryType == 0){
//						returnstrString+=user.getPlayerIndex()+"_";
//					}else{
//						returnstrString+=user.getAccount()+"_";
//					}
//					String cardnum="0";
//					List<UserBackpack> list =  playerService.getPlayerItemByID(user.getPlayerID());
//					for(int j =0 ;j<list.size();j++)
//					{
//						if(itemBaseID==null){
//							cardnum = Integer.toString(list.get(j).getItemNum());
//							break;
//						}else{
//							if(list.get(j).getItemBaseID() == Integer.parseInt(itemBaseID))
//							{
//								cardnum = Integer.toString(list.get(j).getItemNum());
//							}
//						}
//					}
//					returnstrString+= URLEncoder.encode(user.getPlayerName(),"UTF-8")+"_"+cardnum+",";
//				}
//			}
//		}
//		ServletActionContext.getResponse().getWriter().write(returnstrString);
//		ServletActionContext.getResponse().getWriter().flush();
//		return null;
//		
//	}
	
//	public String browsePlayer1() throws IOException{
//		IUserService playerService = (IUserService) SpringService.getBean("playerBaseService");
//		
//		if(this.pageSize == 0)
//		{
//			this.setPageSize(20);
//		}	
//		
//		String playerIndex=account;
//		if(account!=null && !account.equals(""))
//		{
//			/*
//			total = playerService.getCustomTotalCount(account);
//			doPage(total, this.getPageSize());
//			playerList = playerService.getAllCustomPlayerByPage(this.startRownum, this.endRownum,account);
//			*/
//			if(queryType == 0)
//			{
//				
////				User user = playerService.getPlayerByPlayerIndex(Integer.parseInt(account));
////				account=user.getAccount();
//				//ID查询
//				total = playerService.getPlayersByPlayerIndexCount(account, startTime, endTime);
//				doPage(total, this.getPageSize());
//				//playerList = playerService.getPlayersByPlayerIDByPage(this.startRownum, this.endRownum, account, startTime, endTime, order);
//				playerList = playerService.getPlayersByPlayerIndexByPage(this.startRownum, this.endRownum,account, startTime, endTime, order);
//			}
//			else if (queryType == 1)
//			{
//				//帐号查询
//				total = playerService.getPlayersByPlayerIndexCount(account, startTime, endTime);
//				doPage(total, this.getPageSize());
//				playerList = playerService.getPlayersByPlayerAccountByPage(this.startRownum, this.endRownum,account, startTime, endTime, order);
//
//			}
//			else if (queryType == 2)
//			{
//				//昵称查询
//				total = playerService.getPlayersByPlayerIndexCount(account, startTime, endTime);
//				doPage(total, this.getPageSize());
//				playerList = playerService.getPlayersByPlayerNameByPage(this.startRownum, this.endRownum,account, startTime, endTime, order);
//			}
//			
//			else if (queryType == 3)
//			{
//				//微信openId查询
//				User user = playerService.getPlayerByWXOpenID(account);
//				account=user.getPlayerIndex()+"";
//				total = playerService.getPlayersByPlayerIndexCount(account, startTime, endTime);
//				doPage(total, this.getPageSize());
//				playerList = playerService.getPlayersByPlayerNameByPage(this.startRownum, this.endRownum,account, startTime, endTime, order);
//			}
//			else if (queryType == 4)
//			{
//				//微信openId查询
//				User user = playerService.getPlayerByWXUnionID(account);
//				account=user.getPlayerIndex()+"";
//				total = playerService.getPlayersByPlayerIndexCount(account, startTime, endTime);
//				doPage(total, this.getPageSize());
//				playerList = playerService.getPlayersByPlayerNameByPage(this.startRownum, this.endRownum,account, startTime, endTime, order);
//			}
//		}
//		else
//		{
//			total = playerService.getTotalCount(startTime, endTime, queryPlayerType);
//			doPage(total, this.getPageSize());
//			playerList = playerService.getAllPlayerByPage(this.startRownum, this.endRownum , startTime, endTime, order, queryPlayerType);
//		}
//		if(playerList.size()>0){
//			for(int i=playerList.size()-1;i>=0;i--){
//				Integer accString=playerList.get(i).getPlayerIndex();
//				if(Integer.parseInt(playerIndex)!=accString){
//					playerList.remove(i);
//				}
//			}
//		}
//		String returnstrString="";
//		returnstrString+= URLEncoder.encode(playerList.get(0).getPlayerName(),"UTF-8")+",";
//		returnstrString+=playerList.get(0).getSex()+",";
//		returnstrString+= playerList.get(0).getPassword() + ",";
//		returnstrString += playerList.get(0).getPlayerIndex()+",";
//		returnstrString += playerList.get(0).getAccount()+",";
//		returnstrString +=playerList.get(0).getPlayerID();
//		
//	//AD19B08E8CE24B448E1A6CB66E706079
//		ServletActionContext.getResponse().getWriter().write(returnstrString);
//		ServletActionContext.getResponse().getWriter().flush();
//		return null;
//	}
//	public String browsePlayer() {
//		IUserService playerService = (IUserService) SpringService.getBean("playerBaseService");
//		
//		if(this.pageSize == 0)
//		{
//			this.setPageSize(20);
//		}	
//		
//		if(account!=null && !account.equals(""))
//		{
//			/*
//			total = playerService.getCustomTotalCount(account);
//			doPage(total, this.getPageSize());
//			playerList = playerService.getAllCustomPlayerByPage(this.startRownum, this.endRownum,account);
//			*/
//			if(queryType == 0)
//			{
//				//ID查询
//				total = playerService.getPlayersByPlayerIDCount(account, startTime, endTime);
//				doPage(total, this.getPageSize());
//				playerList = playerService.getPlayersByPlayerIDByPage(this.startRownum, this.endRownum, account, startTime, endTime, order);
//
//			}
//			else if (queryType == 1)
//			{
//				//帐号查询
//				total = playerService.getPlayersByPlayerAccountCount(account, startTime, endTime);
//				doPage(total, this.getPageSize());
//				playerList = playerService.getPlayersByPlayerAccountByPage(this.startRownum, this.endRownum,account, startTime, endTime, order);
//
//			}
//			else if (queryType == 2)
//			{
//				//昵称查询
//				total = playerService.getPlayersByPlayerNameCount(account, startTime, endTime);
//				doPage(total, this.getPageSize());
//				playerList = playerService.getPlayersByPlayerNameByPage(this.startRownum, this.endRownum,account, startTime, endTime, order);
//			}
//		}
//		else
//		{
//			total = playerService.getTotalCount(startTime, endTime, queryPlayerType);
//			doPage(total, this.getPageSize());
//			playerList = playerService.getAllPlayerByPage(this.startRownum, this.endRownum , startTime, endTime, order, queryPlayerType);
//		}
//		return SUCCESS;
//	}

//	public String queryPlayerByAccount() {
//
//		IUserService playerService = (IUserService) SpringService.getBean("playerBaseService");
//		
//		if(account!=null && !account.equals(""))
//		{
//			if(queryType == 0)
//			{
//				//ID查询
//				if((account.length() < 10) && StringUtils.isInteger(account) && Integer.parseInt(account)>0)
//				{
//					playerBean = playerService.getPlayerByPlayerIndex(Integer.parseInt(account));
//				}
//			}
//			else if (queryType == 1)
//			{
//				//帐号查询
//				playerBean = playerService.getPlayerByAccount(account);
//			}
//			else if (queryType == 2)
//			{
//				//昵称查询
//				playerBean = playerService.getPlayerByPlayerName(account);
//			}	
//		}
//		
//		if (playerBean == null) 
//		{
//			return "failed";
//		}
//
//		
//		return "detial";	
//	}
	
//	public String queryPlayerByIndex() {
//
//		IUserService playerService = (IUserService) SpringService.getBean("playerBaseService");
//
//		playerBean = playerService.getPlayerByPlayerIndex(index);
//		if (playerBean == null) {
//			return "failed";
//		}
//		return "detial";
//
//	}
	
	public String alterPlayerPassword()
	{
		String playerID =  ServletActionContext.getRequest().getParameter("playerID");
		String pwd =  ServletActionContext.getRequest().getParameter("pwd");
		
		if(pwd==null && playerID==null){
			playerID=account;
			pwd=password;
		}
		if(pwd.length()<3||pwd.length()>12)
			return null;
		
		ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");
		User pl = dbplayerService.getPlayerByPlayerID(playerID);
		
		if(pl!=null){
			dbplayerService.updatePlayerPassword(pl, pl.getPassword(),MD5Service.encryptString(pwd));
		}
		
		try {
			ServletActionContext.getResponse().getWriter().write("success");
			ServletActionContext.getResponse().getWriter().flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String onLineState()
	{
		String playerID =  ServletActionContext.getRequest().getParameter("playerID");
		ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");
		User pl = dbplayerService.getPlayerByPlayerIDFromCache(playerID);
		
		
		int isOnline = 0;
		if(pl!=null&&pl.isOnline()){
			isOnline =1;
		}
		
		try {
			ServletActionContext.getResponse().getWriter().write(isOnline+"");
			ServletActionContext.getResponse().getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}


	public String allOnlineNum()
	{
		ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");
		int num = dbplayerService.getCacheOnlineNum();
		try {
			ServletActionContext.getResponse().getWriter().write(num+"");
			ServletActionContext.getResponse().getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

//	//2人VIP游戏人数
//	public String allTwoManOnlineNum()
//	{
//		ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");
//		int num = dbplayerService.getCacheTwoOnline();
//		try {
//			ServletActionContext.getResponse().getWriter().write(num+"");
//			ServletActionContext.getResponse().getWriter().flush();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		return "ajax";
//	}

//	public String detialPlayer() {
//		IUserService playerService = (IUserService) SpringService.getBean("playerBaseService");
//		playerBean = playerService.getPlayerByID(id);
//		return "detial";
//	}
	
//	public String updatePlayerGold() {		
//		
//		String method = ServletActionContext.getRequest().getMethod();
//		HttpServletRequest request =  ServletActionContext.getRequest();
//		HttpSession session = request.getSession();
//		BaseAdminContext baseAdminContext = (BaseAdminContext)(session.getAttribute(AdminSystemConstant.ADMIN_SYSTEM_SESSION_KEY));
//		
//		
//		if(method.equals("POST"))
//		{
//			String playerID = request.getParameter("playerID");
//			String gold = request.getParameter("playerGold");
//		
//			if(!StringUtils.isInteger(gold) || Integer.parseInt(gold)<0){
//				actionMsg = "金币必须为1-11正整数字符!";
//				return queryPlayerByAccount();
//			}			
//			
//			//更新缓存
//			ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");	
//			
//			User pl = dbplayerService.getPlayerByPlayerID(playerID);
//			
//			int plGold = pl.getGold();
//			
//			if (null != dbplayerService)
//			{
//				//Player pl = dbplayerService.getPlayerByPlayerIDFromCache(playerID);			
//				if(pl!=null)
//				{
//					pl.setGold(Integer.parseInt(gold));
//					
//					//修改记录写入日志
//					String detail = "修改缓存：管理员" + baseAdminContext.getOssUser().getUsername()+"手动重置玩家金币, 原始金币=" + plGold +"修改后=" + gold;
//					dbplayerService.createPlayerLog(playerID, pl.getPlayerIndex(), pl.getPlayerName(), pl.getGold(), LogConstant.OPERATION_TYPE_ADD_GOLD, LogConstant.OPERATION_TYPE_ADMIN_CHANGE_USER_DATA, Integer.parseInt(gold), detail, LogConstant.MONEY_TYPE_GOLD);
//				}
//			}
//			
//			//更新数据库
//			IUserService playerService = (IUserService)SpringService.getBean("playerBaseService");
//			
//			if (null != playerService)
//			{
//				playerService.updatePlayerGold(playerID, Integer.parseInt(gold));		
//				
//				//修改记录写入日志
//				String detail = "修改数据库：管理员" + baseAdminContext.getOssUser().getUsername()+"手动重置玩家金币, 原始金币=" + plGold +"修改后=" + gold;
//				dbplayerService.createPlayerLog(playerID, pl.getPlayerIndex(), pl.getPlayerName(), Integer.parseInt(gold), LogConstant.OPERATION_TYPE_ADD_GOLD, LogConstant.OPERATION_TYPE_ADMIN_CHANGE_USER_DATA, Integer.parseInt(gold), detail, LogConstant.MONEY_TYPE_GOLD);
//				
//				playerBean = playerService.getPlayerByID(playerID);
//				if (playerBean == null) {
//					return "failed";
//				}
//				
//				return "detial";
//			}			
//		}
//		
//		return "failed";
//	}
	
	/**更新玩家类别*/
//	public String updatePlayerType()
//	{
//		String method = ServletActionContext.getRequest().getMethod();
//		HttpServletRequest request =  ServletActionContext.getRequest();
//		//HttpSession session = request.getSession();
//		//BaseAdminContext baseAdminContext = (BaseAdminContext)(session.getAttribute(AdminSystemConstant.ADMIN_SYSTEM_SESSION_KEY));
//		
//		
//		if(method.equals("POST"))
//		{
//			String playerID = request.getParameter("playerID");
//			String playerType = request.getParameter("playerType");
//		
//			if(!StringUtils.isInteger(playerType) || Integer.parseInt(playerType)<0){
//				actionMsg = "玩家类别必须为1-11正整数字符!";
//				return queryPlayerByAccount();
//			}			
//			
//			
//			ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");
//			IUserService playerService = (IUserService)SpringService.getBean("playerBaseService");
//			
//			//更新缓存
//			User pl = dbplayerService.getPlayerByPlayerIDFromCache(playerID);
//			if(pl != null)
//			{
//				pl.setPlayerType(Integer.parseInt(playerType));
//				
//				playerBean = pl;
//			}
//			
//			//更新数据库
//			dbplayerService.updatePlayerType(playerID, Integer.parseInt(playerType));
//			
//			if(pl != null)
//			{
//				playerBean = pl;
//			}
//			else
//			{
//				playerBean = playerService.getPlayerByID(playerID);
//				playerBean.setPlayerType(Integer.parseInt(playerType));
//			}
//			
//			if (playerBean == null) {
//				return "failed";
//			}
//			
//			return "detial";
//		}
//		
//		return "failed";
//	}
	
	/**清理玩家游戏信息
	 * @throws IOException */
	public String clearplayerstation() throws IOException
	{
		String returnstrString="failed";
		String method = ServletActionContext.getRequest().getMethod();
		HttpServletRequest request =  ServletActionContext.getRequest();
		
		
		if(method.equals("POST"))
		{
			String playerID = request.getParameter("playerID");
					
			
			//更新缓存
			ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");		
			
			if (null != dbplayerService)
			{				
				User pl = dbplayerService.getPlayerByPlayerID(playerID);			
				if(pl!=null)
				{
					dbplayerService.clearPlayerStaion(pl);
					
					playerBean = pl;
					returnstrString="success";
				}				
			}
		}
		
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
		
		return null;
	}
	
	/**
	 * 清理卡桌
	 * @return
	 * @throws IOException
	 */
	public String clearTableStation() throws IOException
	{
		String returnstrString="failed";
		String method = ServletActionContext.getRequest().getMethod();
		HttpServletRequest request =  ServletActionContext.getRequest();
		
		
		if(method.equals("POST"))
		{
			String vipTableId = request.getParameter("vipTableID");
			String gameId = request.getParameter("gameId");
			
			//更新缓存
			ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");		
			
			if (null != dbplayerService)
			{				
				dbplayerService.clearVipTableStation(vipTableId, gameId);
				
				returnstrString="success";
			}
		}
		
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
		
		return null;
	}
	
	/***修改积分商城是否开启的开关
	 *  2017.6.28
	 * param 1.int isOpen
	 * @throws IOException 
	 * **/
	public String updatePointShopOpen() throws IOException
	{
		int isOpen =  Integer.parseInt(ServletActionContext.getRequest().getParameter("isOpen"));
		
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		SystemConfigPara para = cfgService.getPara(ConfigConstant.IS_START_CREDITS_EXCHANGE);
		
		para.setValueInt(isOpen);
		cfgService.updatePara(para);
		
		ServletActionContext.getResponse().getWriter().write("success");
		ServletActionContext.getResponse().getWriter().flush();
		return null;
	}
	
	/***修改个人积分
	 *  2017.6.28
	 * param 1.int point（扣除的分数）2.playerId
	 * @throws IOException 
	 * **/
	public String updatePlayerPoint() throws IOException
	{
		boolean isSuccess = false;
		String playerID = ServletActionContext.getRequest().getParameter("playerId");
		int credits =  Integer.parseInt(ServletActionContext.getRequest().getParameter("point"));
		
		if (playerID  != null && credits != 0) {
			
			ICacheUserService userService = (ICacheUserService) SpringService.getBean("playerService");
			isSuccess= userService.costPlayerCredits(playerID, credits );
		}
		
		if (isSuccess) {
			ServletActionContext.getResponse().getWriter().write("success");
			ServletActionContext.getResponse().getWriter().flush();
		}else{
			ServletActionContext.getResponse().getWriter().write("fail");
			ServletActionContext.getResponse().getWriter().flush();
		}
		return null;
	}
	/***修改积分商城商品信息
	 *  2017.6.28
	 * param 
	 * @throws UnsupportedEncodingException 
	 * **/
	public String updateGoodsInfo() throws UnsupportedEncodingException
	{
		String game_id = ServletActionContext.getRequest().getParameter("game_id");
		String goods_id = ServletActionContext.getRequest().getParameter("goods_id");
		String goods_name =  ServletActionContext.getRequest().getParameter("goods_name");
		goods_name = URLDecoder.decode(goods_name,"UTF-8");
		String goods_picture =  ServletActionContext.getRequest().getParameter("goods_picture");
		String goods_remark =  ServletActionContext.getRequest().getParameter("goods_remark");
		goods_remark = URLDecoder.decode(goods_remark,"UTF-8");
		String goods_class =  ServletActionContext.getRequest().getParameter("goods_class");
		int expense_score =  Integer.parseInt(ServletActionContext.getRequest().getParameter("expense_score"));
		int open_state =  Integer.parseInt(ServletActionContext.getRequest().getParameter("goods_state"));
		
		
		ShopGoods shopGoods = new ShopGoods();
		shopGoods.setGameID(game_id);
		shopGoods.setGoodsId(goods_id);
		shopGoods.setGoodsName(goods_name);
		
		shopGoods.setGoodsRemark(goods_remark);
		shopGoods.setGoodClass(goods_class);
		shopGoods.setExpenseScore(expense_score);
		shopGoods.setOpenState(open_state);
		
		
		ICacheUserService userService = (ICacheUserService) SpringService.getBean("playerService");
		if (goods_picture != null  && goods_picture != ""){
//			String gp = URLDecoder.decode(goods_picture,"gbk");
			byte[] picture = goods_picture.getBytes();
			
			shopGoods.setGoodsPicure(picture);
			
			userService.UpdateShowGoods(shopGoods);
		}else {
			userService.UpdateShowGoodsExceptPicture(shopGoods);
		}
		
		
		return null;
	}
	
	/***添加积分商城商品信息
	 *  2017.6.28
	 * param 
	 * @throws UnsupportedEncodingException 
	 * **/
	public String addGoodsInfo() throws UnsupportedEncodingException
	{
		String game_id = ServletActionContext.getRequest().getParameter("game_id");
		String goods_id = ServletActionContext.getRequest().getParameter("goods_id");
		String goods_name =  ServletActionContext.getRequest().getParameter("goods_name");
		goods_name = URLDecoder.decode(goods_name,"UTF-8");
		String goods_picture =  ServletActionContext.getRequest().getParameter("goods_picture");
		String goods_remark =  ServletActionContext.getRequest().getParameter("goods_remark");
		goods_remark = URLDecoder.decode(goods_remark,"UTF-8");
		String goods_class =  ServletActionContext.getRequest().getParameter("goods_class");
		int expense_score =  Integer.parseInt(ServletActionContext.getRequest().getParameter("expense_score"));
		int open_state =  Integer.parseInt(ServletActionContext.getRequest().getParameter("goods_state"));
		
		byte[] picture = goods_picture.getBytes("gbk");
		
		ShopGoods shopGoods = new ShopGoods();
		shopGoods.setGameID(game_id);
		shopGoods.setGoodsId(goods_id);
		shopGoods.setGoodsName(goods_name);
		shopGoods.setGoodsPicure(picture);
		shopGoods.setGoodsRemark(goods_remark);
		shopGoods.setGoodClass(goods_class);
		shopGoods.setExpenseScore(expense_score);
		shopGoods.setOpenState(open_state);
		
		ICacheUserService userService = (ICacheUserService) SpringService.getBean("playerService");
		userService.insertShopGood(shopGoods);
		return null;
	}
	
	/***修改积分商城商品上架信息
	 *  2017.6.28
	 * param goodId,state
	 * @throws IOException 
	 * **/
	public String updateGoodState() throws IOException
	{
		String goodId = ServletActionContext.getRequest().getParameter("goodId");
		int state = Integer.parseInt(ServletActionContext.getRequest().getParameter("state"));
		
		ICacheUserService userService = (ICacheUserService) SpringService.getBean("playerService");
		userService.updateGoodState(goodId ,state);
		
		ServletActionContext.getResponse().getWriter().write("success");
		ServletActionContext.getResponse().getWriter().flush();
		return null;
	}
	
	
//	
//	public String showExchangePage()
//	{
//		return "exchangePage";
//	}
	
//	public String sumbmitExchangeGold()
//	{
//		//setErrorInfo("账号或密码错误");
//		if(this.index <=0 || this.receiveIndex <= 0 || this.gold <= 0)
//		{
//			return "exchangePage";
//		}
//		
//		//更新缓存
//		ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");
//		IUserService playerService = (IUserService)SpringService.getBean("playerBaseService");
//		
//		User pl = playerService.getPlayerByPlayerIndex(index);
//		if(pl == null)
//		{
//			//找不到赠送玩家或者金币不足
//			return "exchangePage";
//		}
//		
//		if(pl.getGold() < this.gold)
//		{
//			return "exchangePage";
//		}
//		
//		User pl2 = playerService.getPlayerByPlayerIndex(this.receiveIndex);
//		if(pl2 == null)
//		{
//			//找不到接收玩家
//			return "exchangePage";
//		}	
//		
//		//转出玩家
//		User sendPl = dbplayerService.getPlayerByPlayerID(pl.getPlayerID());
//		if(sendPl == null)
//		{
//			return "exchangePage";
//		}
//		
//		//转入玩家
//		User getPl = dbplayerService.getPlayerByPlayerID(pl2.getPlayerID());
//		if(getPl == null)
//		{
//			return "exchangePage";
//		}
//		
//		HttpServletRequest request = ServletActionContext.getRequest();
//		HttpSession session = request.getSession();
//		BaseAdminContext baseAdminContext = (BaseAdminContext)(session.getAttribute(AdminSystemConstant.ADMIN_SYSTEM_SESSION_KEY));
//		
//		String remark = "管理员：" + baseAdminContext.getOssUser().getUsername() + "操作玩家" + this.index + "后台转赠给玩家：" + this.receiveIndex +",金币：" + this.gold;
//		
//		//减金币
//		dbplayerService.sub_player_gold(sendPl, this.gold, LogConstant.OPERATION_TYPE_ADMIN_EXCHANGE_PLAYER_GOLD, remark);
//		//加金币
//		dbplayerService.add_player_gold(getPl, this.gold, LogConstant.OPERATION_TYPE_ADMIN_EXCHANGE_PLAYER_GOLD, remark);
//		
//		
//		return "exchangeSuccess";
//	}
//	
//	/**解除绑定手机号码*/
//	public String clearPlayerPhoneNumber()
//	{
//		String method = ServletActionContext.getRequest().getMethod();
//		HttpServletRequest request =  ServletActionContext.getRequest();
//		
//		
//		if(method.equals("POST"))
//		{
//			String playerID = request.getParameter("playerID");			
//		
//			
//			
//			ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");
//			IUserService playerService = (IUserService)SpringService.getBean("playerBaseService");
//			
//			//更新缓存
//			User pl = dbplayerService.getPlayerByPlayerIDFromCache(playerID);
//			if(pl != null)
//			{
//				pl.setPhoneNumber("");
//				
//				playerBean = pl;
//			}
//			
//			//更新数据库
//			dbplayerService.updatePlayerPhoneNumber(playerID, "");
//			
//			if(pl != null)
//			{
//				playerBean = pl;
//			}
//			else
//			{
//				playerBean = playerService.getPlayerByID(playerID);
//				playerBean.setPhoneNumber("");
//			}
//			
//			if (playerBean == null) {
//				return "failed";
//			}
//			
//			return "detial";
//		}
//		
//		return "failed";
//	}

	public List<User> getPlayerList() {
		return playerList;
	}

	public void setPlayerList(List<User> playerList) {
		this.playerList = playerList;
	}

	public User getPlayerBean() {
		return playerBean;
	}

	public void setPlayerBean(User playerBean) {
		this.playerBean = playerBean;
	}
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getQueryType() {
		return queryType;
	}

	public void setQueryType(int queryType) {
		this.queryType = queryType;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getReceiveIndex() {
		return receiveIndex;
	}

	public void setReceiveIndex(int receiveIndex) {
		this.receiveIndex = receiveIndex;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public int getQueryPlayerType() {
		return queryPlayerType;
	}

	public void setQueryPlayerType(int queryPlayerType) {
		this.queryPlayerType = queryPlayerType;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
