package com.chess.common.web;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

public class BaseAction {

	/** 错误信息 */
	private String successInfo;
	
	/** 错误信息 */
	private String errorInfo;

	
	public String getSuccessInfo() {
		return successInfo;
	}

	public void setSuccessInfo(String successInfo) {
		this.successInfo = successInfo;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}
	
	protected void resetInfos(){
		this.successInfo = "";
		this.errorInfo = "";
	}
	
	public void writeString(String result) throws Exception{
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().write(result);
		response.getWriter().flush();
	}
	
}
