package com.chess.common.web.login;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.MD5Service;
import com.chess.common.SpringService;
import com.chess.common.bean.oss.OssUser;
import com.chess.common.service.IOssUserService;
import com.chess.common.web.BaseAction;

public class LoginAction extends BaseAction {
	private static Logger logger = LoggerFactory
    .getLogger(LoginAction.class);
	
	/**登录
	 **/
	public String login() throws Exception{
		HttpServletRequest request = ServletActionContext.getRequest();
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		
		String result = "ok";
		//验证账号密码
		IOssUserService ossUserService = (IOssUserService)SpringService.getBean("ossUserService");
		OssUser ossUser = ossUserService.getOssUserByID(userName);
		String md5Password = MD5Service.encryptString(password);
		if (ossUser == null || !ossUser.getPassword().equals(md5Password)) {
			result = "账号或密码错误";
		}else{
			//修改登录信息
			ossUser.setLastLoginIp(request.getRemoteAddr());
			ossUser.setLastLoginTime(DateService.getCurrentUtilDate());
			ossUser.setLoginNum(ossUser.getLoginNum()+1);
			ossUserService.updateOssUserLastLoginInfo(ossUser);
		}
		
		writeString(result);
		return null;
	}
	
}
