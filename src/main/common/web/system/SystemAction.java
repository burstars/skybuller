package com.chess.common.web.system;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.SpringService;
import com.chess.common.bean.SystemConfigPara;
import com.chess.common.bean.SystemNotice;
import com.chess.common.msg.struct.system.ScrollMsg;
import com.chess.common.service.ICacheUserService;
import com.chess.common.web.BasalAction;
import com.chess.nndzz.table.GameTable;

import flex.messaging.util.URLDecoder;

public class SystemAction extends BasalAction {
    private static Logger logger = LoggerFactory
            .getLogger(SystemAction.class);

    private List<SystemConfigPara> configList = new ArrayList<SystemConfigPara>();
    private SystemConfigPara cfg = null;
    private String msgs = "";
    private Integer loops = 1;
    private boolean shouldclear = false;
    private boolean clearOntimeList = false;

    private String index = "";
    private List<GameTable> playingTablesMap = new ArrayList<GameTable>();
    private String table_id = "";

    //定时跑马灯时间
    private String sendTime = "";
    //发送到哪个玩家组
    private int sendPlayerType = 0;
    //定时发送频率 0:一次性 1：5分钟一次，2：1小时一次
    private int ontimeType = 0;
    //服务端定时发送次数
    private int sendTimes = 1;

    //待发送跑马灯信息
    private List<ScrollMsg> systemMsgList = new ArrayList<ScrollMsg>();

    /**
     * 系统公告
     */
    private String notice_title = null;
    private String notice_content = null;
    private int isExit = 0;

    /**
     * 管理员相关
     */
    private String user_name = "";
    private String user_account = "";
    private String user_password = "";
    private int user_type = 0;
    //错误信息
    private String error_msg = "";
    
    private int clubCode = 0;

//    //显示发跑马灯消息的页面
//    public String initPage() {
//        //IDBServerPlayerService	playerService = (IDBServerPlayerService) SpringService.getBean("playerService");
//
//        return "initPage";
//    }
//
//    //显示补丁包添加的页面
//    public String showPatchAddPage() {
//        //IDBServerPlayerService	playerService = (IDBServerPlayerService) SpringService.getBean("playerService");
//
//        return "showPatchAddPage";
//    }
//
//    //添加一个补丁包
//    public String addPatch() {
//        GameContext.updateServerMsgProc.add_new_patch(msgs, shouldclear);
//        //playerService.sendSystemScrollMsg(msgs, loops, shouldclear?1:0);
//        return "showPatchAddPage";
//    }
//
//    //
//    public String showSystemParaPage() {
//        ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
//        configList = cfgService.getAllConfigPara();
//
//        //SystemConfig.updateServerMsgProc.add_new_patch(msgs, shouldclear);
//        //playerService.sendSystemScrollMsg(msgs, loops, shouldclear?1:0);
//        return "showSystemParaPage";
//    }
//
//    public String updatePara() {
//        ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
//
//        configList = cfgService.getAllConfigPara();
//        //SystemConfig.updateServerMsgProc.add_new_patch(msgs, shouldclear);
//        //playerService.sendSystemScrollMsg(msgs, loops, shouldclear?1:0);
//        return "updatePara";
//    }
//
//    public String commitUpdatePara() {
//        ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
//        cfgService.updatePara(cfg);
//        configList = cfgService.getAllConfigPara();
//
//        GobalConfig confMsg = new GobalConfig();
//        confMsg.para = cfg;
//        if (cfg.getIsclient() == 1) {
//            GameContext.gameSocket.sendMsgToAllSession(confMsg);
//        }
//
//        //SystemConfig.updateServerMsgProc.add_new_patch(msgs, shouldclear);
//        //playerService.sendSystemScrollMsg(msgs, loops, shouldclear?1:0);
//        return "modifyConfigSuccess";
//    }

    /**
     * 发送跑马灯消息
     * @throws UnsupportedEncodingException 
     */
    public String sendMsg() throws UnsupportedEncodingException {
        if (sendPlayerType == 7)
            sendPlayerType = 4;
        msgs=URLDecoder.decode(msgs,"UTF-8");
        ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
        playerService.sendSystemScrollMsg(msgs, loops, shouldclear ? 1 : 0, sendPlayerType);
        return null;
    }

    /**
     * 发送定时跑马灯消息
     * @throws UnsupportedEncodingException 
     */
    public String sendOntimeMsg() throws UnsupportedEncodingException {
        ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
        if (sendPlayerType == 7)
            sendPlayerType = 4;
        msgs=URLDecoder.decode(msgs,"UTF-8");
        playerService.addOntimeSystemScrollMsg(msgs, loops, shouldclear ? 1 : 0, sendTime, clearOntimeList, sendPlayerType, ontimeType, sendTimes);
        logger.info("sendOntimeMsg:" + msgs + ":" + loops + ":" + shouldclear + ":" + sendPlayerType);
        return  null;
    }

    /**
     * 查看待发送跑马灯
     * @throws IOException 
     */
    @SuppressWarnings("deprecation")
	public String getSystemMsgInfo() throws IOException {
        ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");

        if (playerService != null) {
            systemMsgList = playerService.getOnTimeScrollMsgs();
        }
        String returnstrString="";
        List<Map<String, Object>> list=new ArrayList<Map<String,Object>>();
        for(int i=0;i<systemMsgList.size();i++){
        	Map<String, Object> map1 = new HashMap<String, Object>();
        	map1.put("createDate",systemMsgList.get(i).createDate);
        	map1.put("loopNum",systemMsgList.get(i).loopNum);
        	map1.put("msg",systemMsgList.get(i).msg);
        	map1.put("msgCMD",systemMsgList.get(i).msgCMD);
        	map1.put("neverCompressedMe",systemMsgList.get(i).neverCompressedMe);
        	map1.put("ontimeType",systemMsgList.get(i).ontimeType);
        	map1.put("removeAllPreviousMsg",systemMsgList.get(i).removeAllPreviousMsg);
        	map1.put("sendPlayerType",systemMsgList.get(i).sendPlayerType);
        	map1.put("sendTime",systemMsgList.get(i).sendTime);
        	map1.put("sendTimes",systemMsgList.get(i).sendTimes);
        	list.add(map1);
        }
        JSONArray json = JSONArray.fromObject(list);
		returnstrString+=json;
		returnstrString=URLEncoder.encode(returnstrString,"UTF-8");
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
        return null;
    }

    /***
     * 创建和修改默认操作永久跑马灯那条信息
     *  2017.6.16
     * param : 1.isOpen:0=不开启，1=开启 ；  2.msgContent 永久跑马灯内容 3.msgId 要修改的跑马灯id （当id=9999的时候为创建）
     * */
    public String setForeverMsg() throws UnsupportedEncodingException {
       
    	int isOpen = Integer.parseInt(ServletActionContext.getRequest().getParameter(
		"isOpen"));
		String msgContent = ServletActionContext.getRequest().getParameter(
		"msgContent");
		
		msgContent=URLDecoder.decode(msgContent,"UTF-8");
		int msgId = Integer.parseInt(ServletActionContext.getRequest().getParameter(
		"msgId"));
		
		 ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");

        if (playerService != null) {
            playerService.setForeverMsg(isOpen, msgContent , msgId);
        }
        return  null;
    }
    
    
    /***
     * 查看永久跑马灯内容
     *  2017.6.16
     * 返回值：1.isOpen:0=不开启，1=开启 ；  2.msgContent 永久跑马灯内容3.msgId 要修改的跑马灯id （当id=9999的时候为创建）
     * @throws IOException 
     * */
    public String getForeverMsg() throws IOException {
       
    	ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
    	SystemNotice sysNotice = null;
        if (playerService != null) {
        	sysNotice = playerService.getForeverMsg();
        }
        String returnstrString="";
        List<Map<String, Object>> list=new ArrayList<Map<String,Object>>();
        Map<String, Object> map1 = new HashMap<String, Object>();
       if (sysNotice != null){
	       	map1.put("msgId",sysNotice.getNotice_id());
	       	map1.put("msgContent",sysNotice.getNotice_content());
	       	map1.put("isOpen",sysNotice.getIs_open());
       }else{
    	   	map1.put("msgId",9999);
	       	map1.put("msgContent","");
	       	map1.put("isOpen",0);
       }
    	
    	
    	list.add(map1);
      
        JSONArray json = JSONArray.fromObject(list);
		returnstrString+=json;
		returnstrString=URLEncoder.encode(returnstrString,"UTF-8");
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
        
        
        return  null;
    }
    
    public String getSystemNotice() throws IOException {
    	
    	ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
    	List<SystemNotice> sysNotice = null;
    	if (playerService != null) {
    		sysNotice = playerService.getSysNoticeList();
    	}  	
    	JSONArray json = JSONArray.fromObject(sysNotice);
    	ServletActionContext.getResponse().setCharacterEncoding("UTF-8");
    	ServletActionContext.getResponse().getWriter().print(json); 	
    	return  null;
    }
    
    public String saveSystemNotice() throws IOException {
    	HttpServletRequest request = ServletActionContext.getRequest();
		String type = request.getParameter("type");
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		SystemNotice systemNotice=new SystemNotice();
		systemNotice.setIs_exit(1);
		systemNotice.setIs_open(1);
		systemNotice.setNotice_type(Integer.parseInt(type));
		systemNotice.setNotice_title(title);
		systemNotice.setNotice_content(content);
		
		String result = "ok";
		
    	ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
    	if (playerService != null) {
    		playerService.deleteSystemNotice(type);
    		playerService.createSystemNotice(systemNotice);
    		playerService.updateNoticeCache(systemNotice);
    	}  	
    	HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().write(result);
		response.getWriter().flush();
		
    	return  null;
    }
    
//    /**
//     * 显示当前所有桌子信息
//     */
//    public String getCurrentTableInfo() {
//        ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
//
//        if (playerService != null) {
//            int player_index = 0;
//            if (index != null && !index.equals("") && StringUtils.isInteger(index) && Integer.parseInt(index) > 0) {
//                player_index = Integer.parseInt(index);
//            }
//            playingTablesMap = playerService.getPlayingTable(player_index);
//        }
//
//        return "showTableInfo";
//    }

//    //tieguo
//    public String getTwoManVipTableInfo() {
//        ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
//
//        if (playerService != null) {
//            int player_index = 0;
//            if (index != null && !index.equals("") && StringUtils.isInteger(index) && Integer.parseInt(index) > 0) {
//                player_index = Integer.parseInt(index);
//            }
//            List<GameTable> table = playerService.getPlayingTable(player_index);
//            List<GameTable> temp = new ArrayList<GameTable>();
//            for (int i = 0; i < table.size(); i++) {
//                GameTable gt = table.get(i);
//                if (gt.getRoomType() == GameConstant.ROOM_TYPE_COUPLE)
//                    temp.add(gt);
//            }
//            playingTablesMap = temp;
//        }
//        return "showTableInfo";
//    }
//
//    /**
//     * 强制结束桌子
//     */
//    public String forceClearTalbe() {
//        if (table_id != null && !table_id.equals("")) {
//            ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
//
//            if (playerService != null) {
//                playerService.clearOneTable(table_id);
//            }
//        }
//
//        return "showTableInfo";
//    }
//
//    /**
//     * 显示系统公告
//     */
//    public String showSystemNoticeMsg() {
//        ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
//
//        if (playerService != null) {
//            SystemNotice sysNotice = playerService.getSysNotice();
//
//            this.notice_title = sysNotice.getNotice_title();
//            this.notice_content = sysNotice.getNotice_content();
//            this.isExit = sysNotice.getIs_exit();
//        }
//
//        return "updateSysNotice";
//    }
//
    /**
     * 更新系统公告
     * @throws UnsupportedEncodingException 
     */
    public String updateNoticeMsg() throws UnsupportedEncodingException {
        ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
        notice_title=URLDecoder.decode(notice_title,"UTF-8");
        notice_content=URLDecoder.decode(notice_content,"UTF-8");
//增加消息类型参数-lxw20180904
        int noticeType=0;
        try {
        	String type = ServletActionContext.getRequest().getParameter("notice_type");
			noticeType=Integer.parseInt(type);
		} catch (Exception e) {
			logger.debug("notice_type 参数未传递");
			noticeType=0;
		}
        if (playerService != null && notice_title != null && notice_content != null) {
            //去掉尾部的空格
            String content = new String(("A" + notice_content).trim().substring(1));
            notice_content = content;
            SystemNotice sysNotice = new SystemNotice();
            sysNotice.setNotice_type(noticeType);
            sysNotice.setNotice_title(notice_title);
            sysNotice.setNotice_content(notice_content);
            sysNotice.setIs_exit(1);
            int msgId = Integer.parseInt(ServletActionContext.getRequest().getParameter("msgId"));
            if(msgId==9999){
            	playerService.createSystemNotice(sysNotice);
            }else {
            	sysNotice.setNotice_id(msgId);
			}   
            playerService.updaeSystemNotice(sysNotice);
        }

        logger.debug("notice_content len=" + notice_content.length());//System.out.println("notice_content len=" + notice_content.length());
        return null;
    }
    
    /**
     * 获取俱乐部桌数
     * @throws IOException 
     */
    public String queryClubTableCount() throws IOException {
        ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
        String no = ServletActionContext.getRequest().getParameter("clubCode");
        String returnstrString = "";
        if(no == null || "".equals(no)){
        	return null;
        }
        int count = playerService.getClubTableCount(Integer.parseInt(no));
    	
		returnstrString=URLEncoder.encode(String.valueOf(count),"UTF-8");
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
        return  null;
    }
//
//    /**
//     * 显示增加管理员界面
//     */
//    public String showAddOssUser() {
//        return "showAddOssUser";
//    }
//
//    /**
//     * 增加管理员
//     */
//    public String AddOssUser() {
//        if (user_name == null || user_account == null || user_password == null) {
//            error_msg = "参数不能为空";
//
//            return "showAddOssUser";
//        }
//
//        IOssUserService ossUserService = (IOssUserService) SpringService.getBean("ossUserService");
//
//        OssUser tmpUser = ossUserService.getOssUserByID(user_account);
//        if (tmpUser != null) {
//            //帐号已存在
//            error_msg = "帐号已存在";
//
//            return "showAddOssUser";
//        }
//
//        OssUser tempOssUser = new OssUser();
//        tempOssUser.setCreateTime(new Date());
//        tempOssUser.setLastLoginIp(null);
//        tempOssUser.setLastLoginTime(null);
//        tempOssUser.setLoginNum(0);
//        tempOssUser.setPassword(MD5Service.encryptString(user_password));
//        tempOssUser.setRealnames(user_name);
//        tempOssUser.setStatus(1);
//        tempOssUser.setUsername(user_account);
//        tempOssUser.setRoleType(user_type);
//        ossUserService.createOssUser(tempOssUser);
//
//        return "success";
//    }
    
	public void updateClubState() throws IOException{
		String returnstrString = "更新失败";
		String clubCode = ServletActionContext.getRequest().getParameter(
				"clubCode");
		String clubState = ServletActionContext.getRequest().getParameter(
				"clubState");
		String playerIndex = ServletActionContext.getRequest().getParameter(
				"playerIndex");
		ICacheUserService playerService = (ICacheUserService) SpringService
				.getBean("playerService");
		logger.info("----更新俱乐部状态-------clubCode:"+clubCode+",clubState:"+clubState+",playerIndex:"+playerIndex);
		boolean isUpdate = playerService.updateClubState(
				Integer.parseInt(clubCode), Integer.parseInt(clubState),
				Integer.parseInt(playerIndex));
		if(!isUpdate){
			returnstrString = "更新失败";
			returnstrString=URLEncoder.encode(returnstrString,"UTF-8");
			ServletActionContext.getResponse().getWriter().write(returnstrString);
			ServletActionContext.getResponse().getWriter().flush();
			return;
		}
		if(clubState.equals("2")){//停用俱乐部，删除所有成员
			playerService.DelAllClubMember(clubCode);
		}
		returnstrString = "更新成功";
		returnstrString=URLEncoder.encode(returnstrString,"UTF-8");
		ServletActionContext.getResponse().getWriter().write(returnstrString);
		ServletActionContext.getResponse().getWriter().flush();
	}
    
    public String getMsgs() {
        return msgs;
    }


    public void setMsgs(String msgs) {
        this.msgs = msgs;
    }


    public Integer getLoops() {
        return loops;
    }


    public void setLoops(Integer loops) {
        this.loops = loops;
    }


    public boolean getShouldclear() {
        return shouldclear;
    }


    public void setShouldclear(boolean shouldclear) {
        this.shouldclear = shouldclear;
    }

    public List<SystemConfigPara> getConfigList() {
        return configList;
    }

    public void setConfigList(List<SystemConfigPara> configList) {
        this.configList = configList;
    }

    public SystemConfigPara getCfg() {
        return cfg;
    }

    public void setCfg(SystemConfigPara cfg) {
        this.cfg = cfg;
    }

    public List<GameTable> getPlayingTablesMap() {
        return playingTablesMap;
    }

    public void setPlayingTablesMap(List<GameTable> playingTablesMap) {
        this.playingTablesMap = playingTablesMap;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public boolean isClearOntimeList() {
        return clearOntimeList;
    }

    public void setClearOntimeList(boolean clearOntimeList) {
        this.clearOntimeList = clearOntimeList;
    }

    public int getSendPlayerType() {
        return sendPlayerType;
    }

    public void setSendPlayerType(int sendPlayerType) {
        this.sendPlayerType = sendPlayerType;
    }

    public List<ScrollMsg> getSystemMsgList() {
        return systemMsgList;
    }

    public void setSystemMsgList(List<ScrollMsg> systemMsgList) {
        this.systemMsgList = systemMsgList;
    }

    public int getOntimeType() {
        return ontimeType;
    }

    public void setOntimeType(int ontimeType) {
        this.ontimeType = ontimeType;
    }

    public int getSendTimes() {
        return sendTimes;
    }

    public void setSendTimes(int sendTimes) {
        this.sendTimes = sendTimes;
    }

    public String getNotice_title() {
        return notice_title;
    }

    public void setNotice_title(String notice_title) {
        this.notice_title = notice_title;
    }

    public String getNotice_content() {
        return notice_content;
    }

    public void setNotice_content(String notice_content) {
        this.notice_content = notice_content;
    }

    public int getIsExit() {
        return isExit;
    }

    public void setIsExit(int isExit) {
        this.isExit = isExit;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public String getUser_account() {
        return user_account;
    }

    public void setUser_account(String user_account) {
        this.user_account = user_account;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}
}
