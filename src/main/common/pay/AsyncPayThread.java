package com.chess.common.pay;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.bean.alipay.util.AliPayService;
import com.chess.common.bean.thirdpay.ThirdPayService;
import com.chess.common.bean.wxpay.WxPayService;
import com.chess.common.constant.PayConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.pay.PayOperation;

public class AsyncPayThread extends Thread {
	
	private static Logger logger = LoggerFactory.getLogger(AsyncPayThread.class);	
	//private static AsyncPayThread myInstance=null;
	private Lock msgLock = new ReentrantLock();
	private List<PayOperation> msgQueue=new ArrayList<PayOperation>();

	public void push(PayOperation msg){
		try{
			msgLock.lock();
			msgQueue.add(msg);
		} catch (Exception e) {
			logger.error( "AsyncPayThread addMsg failed", e );
		} finally {
			msgLock.unlock();
		}
	}
	
	private PayOperation pop()
	{
		if(msgQueue.size()<=0)
			return null;
		
		PayOperation msg=null;
		try{
			msgLock.lock();
			msg = msgQueue.remove(0);
		} catch (Exception e) 
		{
			logger.error( "popMsg failed", e );
		} finally {
			msgLock.unlock();
		}
		return msg;
	}
	
	//
	private void msgProcessor()
	{
		PayOperation msg= pop();
		if(msg == null)
			return;
		
		long delta = 0;
		long begin = 0;
		Date ct = DateService.getCurrentUtilDate();
		
		logger.error("支付处理，取出支付请求，时间:" + ct.getTime());
		
		switch(msg.payPlatformType){
		//支付宝支付
		case PayConstant.HISTORY_PLATFORM_TYPE_ALIPAY:{
			begin = ct.getTime();
			
//			String orderInfo = AliPayService.generateOrderForAliPay(msg.itemBase,
//					msg.outTradeNo, msg.itemCount);
//			msg.gbiam.order = orderInfo;
//			GameContext.gameSocket.send(msg.player.getSession(), msg.gbiam);
			
			String url = ThirdPayService.generateOrder(ThirdPayService.THIRD_PAY_CODE_ALIPAY, msg.player, msg.itemBase, msg.outTradeNo);
			msg.gbiam.order = url;
			GameContext.gameSocket.send(msg.player.getSession(), msg.gbiam);
			
			delta = ct.getTime() - begin;
			logger.error("支付宝支付处理时间:"+delta);
			break;
		}
		//微信支付
		case PayConstant.HISTORY_PLATFORM_TYPE_WX_PAY:{
			begin = ct.getTime();
//			if(WxPayService.genProductArgs(msg.player, msg.outTradeNo, msg.itemBase, msg.gbiam)){
//				GameContext.gameSocket.send(msg.player.getSession(), msg.gbiam);
//				delta = ct.getTime() - begin;
//				logger.error("微信支付处理时间:"+delta);
//			} else {
//				logger.error("微信支付处理错误!");
//			}
			
			String url = ThirdPayService.generateOrder(ThirdPayService.THIRD_PAY_CODE_WXPAY, msg.player, msg.itemBase, msg.outTradeNo);
			msg.gbiam.order = url;
			GameContext.gameSocket.send(msg.player.getSession(), msg.gbiam);
			
			delta = ct.getTime() - begin;
			logger.error("微信支付处理时间:"+delta);
			
			break;
		}
		//完成苹果支付
		/*
		case PayConstant.COMPLETE_TYPE_IPA_PAY:
			begin = ct.getTime();
			IpaService ipaService = (IpaService)SpringService.getBean("ipaService");
			IDBServerPlayerService playerService = (IDBServerPlayerService)SpringService.getBean("playerService");
			if (null != ipaService) {
				boolean bl = ipaService.verifyReceipt(msg.completeIPAMsg.payResult, msg.completeIPAMsg.orderNo);
				playerService.completePayDeal(msg.player, msg.completeIPAMsg.orderNo, bl);
				delta = ct.getTime() - begin;
				logger.error("完成苹果支付处理时间:"+delta);
			}
			
			break;
			*/
		}
		
	}
	
	//线程函数，主循环
	@Override
	public void run() {
		while(true) {
			try {
				msgProcessor();
				Thread.sleep(100); 
			} catch (Exception e) {
				logger.error("wx pay thread loop error", e);
				logger.error(e.getMessage(), e);
				continue;
			}
		}
	}
}
