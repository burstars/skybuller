package com.chess.common.pay;

import com.chess.common.bean.MallItem;
import com.chess.common.bean.User;
import com.chess.common.msg.struct.pay.GameBuyItemAckMsg;
import com.chess.common.msg.struct.pay.GameIPABuyItemCompleteMsg;

public class PayOperation {
	//payPlatformType如果是支付操作则对应PayConstant里面的常量
	//payPlatformType如果是完成苹果操作则为11
	public int payPlatformType = -1;
	
	public User player = null;
	
	public GameBuyItemAckMsg gbiam = null;
	
	public MallItem itemBase = null;
	
	public String outTradeNo = "";
	
	public int itemCount = -1;
	
	public GameIPABuyItemCompleteMsg completeIPAMsg = null;
	
}
