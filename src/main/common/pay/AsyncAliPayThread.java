package com.chess.common.pay;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.bean.alipay.util.AliPayService;
import com.chess.common.constant.PayConstant;
import com.chess.common.framework.GameContext;

public class AsyncAliPayThread extends Thread {
	private static Logger logger = LoggerFactory.getLogger(AsyncAliPayThread.class);	
    //private static AsyncAliPayThread myInstance=null;
	private Lock msgLock = new ReentrantLock();
	private List<PayOperation> msgQueue=new ArrayList<PayOperation>();

	public void push(PayOperation msg){
		try{
			msgLock.lock();
			msgQueue.add(msg);
		} catch (Exception e) {
			logger.error( "AsyncPayThread addMsg failed", e );
		} finally {
			msgLock.unlock();
		}
	}
	
	private PayOperation pop()
	{
		if(msgQueue.size()<=0)
			return null;
		
		PayOperation msg=null;
		try{
			msgLock.lock();
			msg = msgQueue.remove(0);
		} catch (Exception e) 
		{
			logger.error( "popMsg failed", e );
		} finally {
			msgLock.unlock();
		}
		return msg;
	}
	
	//
	private void msgAliPayProcessor()
	{
		PayOperation msg= pop();
		if(msg == null)
			return;
		long delta = 0;
		long begin = 0;
		Date ct = DateService.getCurrentUtilDate();
		switch(msg.payPlatformType){
		//支付宝支付
		case PayConstant.HISTORY_PLATFORM_TYPE_ALIPAY:			
			begin = ct.getTime();
			String orderInfo = AliPayService.generateOrderForAliPay(msg.itemBase,
					msg.outTradeNo, msg.itemCount);
			msg.gbiam.order = orderInfo;
			GameContext.gameSocket.send(msg.player.getSession(), msg.gbiam);
			delta = ct.getTime() - begin;
			logger.error("支付宝支付处理时间:"+delta);
			break;
		}

	}
	
	//线程函数，主循环
	@Override
	public void run() {
		while(true) {
			try {
				msgAliPayProcessor();
				Thread.sleep(100); 
			} catch (Exception e) {
				logger.error("ali pay processor loop error", e);
				logger.error(e.getMessage(), e);
				continue;
			}
		}
	}

}
