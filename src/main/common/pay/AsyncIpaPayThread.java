package com.chess.common.pay;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.bean.ipapay.IpaService;
import com.chess.common.constant.PayConstant;
import com.chess.common.service.ICacheUserService;

public class AsyncIpaPayThread extends Thread {
	private static Logger logger = LoggerFactory.getLogger(AsyncIpaPayThread.class);	
	//private static AsyncIpaPayThread myInstance=null;
	private Lock msgLock = new ReentrantLock();
	private List<PayOperation> msgQueue=new ArrayList<PayOperation>();

	public void push(PayOperation msg){
		try{
			msgLock.lock();
			msgQueue.add(msg);
		} catch (Exception e) {
			logger.error( "AsyncPayThread addMsg failed", e );
		} finally {
			msgLock.unlock();
		}
	}
	
	private PayOperation pop()
	{
		if(msgQueue.size()<=0)
			return null;
		
		PayOperation msg=null;
		try{
			msgLock.lock();
			msg = msgQueue.remove(0);
		} catch (Exception e) 
		{
			logger.error( "popMsg failed", e );
		} finally {
			msgLock.unlock();
		}
		return msg;
	}
	
	//
	private void msgIpaPayProcessor()
	{
		PayOperation msg= pop();
		if(msg == null)
			return;
		long delta = 0;
		long begin = 0;
		Date ct = DateService.getCurrentUtilDate();
		switch(msg.payPlatformType){
		//完成苹果支付
		case PayConstant.COMPLETE_TYPE_IPA_PAY:
			begin = ct.getTime();
			IpaService ipaService = (IpaService)SpringService.getBean("ipaService");
			ICacheUserService playerService = (ICacheUserService)SpringService.getBean("playerService");
			if (null != ipaService) {
				boolean bl = ipaService.verifyReceipt(msg.completeIPAMsg.payResult, msg.completeIPAMsg.orderNo);
				playerService.completePayDeal(msg.player, msg.completeIPAMsg.orderNo, bl);
				delta = ct.getTime() - begin;
				logger.error("完成苹果支付处理时间:"+delta);
			}
			
			break;
		}
	}
	
	//线程函数，主循环
	@Override
	public void run() {
		while(true) {
			try {
				msgIpaPayProcessor();
				Thread.sleep(100); 
			} catch (Exception e) {
				logger.error("Ipa Pay Processor loop error", e);
				logger.error(e.getMessage(), e);
				continue;
			}
		}
	}
}
