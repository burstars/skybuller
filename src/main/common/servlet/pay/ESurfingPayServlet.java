package com.chess.common.servlet.pay;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import com.chess.common.SpringService;
import com.chess.common.service.impl.PayInterService;

public class ESurfingPayServlet extends HttpServlet {
	private static Logger logger = LoggerFactory.getLogger(ESurfingPayServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		
		logger.debug(req.getRequestURI());//System.out.println(req.getRequestURI());
		req.getRequestURI();
		String testStr = "";
		Map<String,String> params = new HashMap<String,String>() ;
		try{
			 req.getParameterMap();
			for(Object ob:req.getParameterMap().keySet()){
				String obs = (String)ob;
				testStr+=obs+"="+req.getParameter(obs)+"&";
				params.put(obs, req.getParameter(obs));
			}
			//System.out.println(testStr);
		}catch(Exception e){
			e.printStackTrace();
		}
		Map temp = req.getParameterMap();
		String sig = req.getParameter("sig");
		if(params.containsKey("sig")){
			params.remove("sig");
		}
		if(params.containsKey("reservedInfo")){
			params.remove("reservedInfo");
		}
		
		PayInterService payInterService  = (PayInterService)SpringService.getBean("payInterService");
		String backStr = payInterService.payByESurfing2(params, sig);
		resp.getWriter().write(backStr);
		
	}
	@Override 
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {
		doGet(req, resp);
	}
}
