package com.chess.common.servlet.pay;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import com.chess.common.JSONService;
import com.chess.common.MD5Service;
import com.chess.common.SpringService;
import com.chess.common.bean.unipay.PayCallbackReq;
import com.chess.common.service.IPayService;


/**联通短信充值 接口*/
public class UniPayServlet extends HttpServlet {
	private static Logger logger = LoggerFactory.getLogger(UniPayServlet.class);
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
        InputStream in = req.getInputStream();
        ByteArrayOutputStream baos = null;
        String xmlStr = null;
        try{
          //循环读取直到结束 
            baos = new ByteArrayOutputStream(1024);
            byte[] buffer = new byte[1024];
            int read = 0;
            while((read = in.read(buffer)) > 0) {
                baos.write(buffer, 0, read);
            }
            xmlStr = new String(baos.toByteArray(),"utf-8");
            //System.out.println("read xmlStr: "+xmlStr);
            String serviceid = req.getParameter("serviceid");
            String backStr = "";
            if(serviceid==null){//充值回调接口 
            	 Map<String,String> eleMap = JSONService.xmlElements(xmlStr);
                 PayCallbackReq payCallbackReq = new PayCallbackReq();
                 payCallbackReq.init(eleMap);
                 backStr = "<callbackRsp>0</callbackRsp>";
                 logger.debug("read xmlStr: "+xmlStr);//System.out.println("read xmlStr: "+xmlStr);
                 if(payCallbackReq.isChecked()){//验证通过
                 	//
                	 if(payCallbackReq.getHRet().equals("0")){
                		 IPayService payService  = (IPayService)SpringService.getBean("payService");
                      	boolean isOk = payService.payByUniPay(payCallbackReq);
                      	if(isOk){//购买成功
                      		backStr = "<callbackRsp>1</callbackRsp>";
                      	} 
                	 }
                 	
                 }
            }else {  //订单号查询接口
            	Map<String,String> eleMap = JSONService.xmlElements(xmlStr);
            	logger.debug("联通订单查询信息: "+xmlStr);//System.out.println("联通订单查询信息: "+xmlStr);
            	if(eleMap!=null){
            		String orderid = eleMap.get("orderid");
                	String signMsg = eleMap.get("signMsg");
                	String sign = "orderid="+orderid+"&Key="+"9813b270ed0288e7c038";
                	sign = MD5Service.encryptStringLower(sign);
                	if(sign.equals(signMsg)){
	            		IPayService payService  = (IPayService)SpringService.getBean("payService");
	            		backStr = payService.payByUniPayValidateOderID(orderid);
	            		logger.debug("返回的联通信息: "+backStr);//System.out.println("返回的联通信息: "+backStr);
                	}
            	}
            	

            	
            }
         String respXml="<?xml version='1.0' encoding='UTF-8'?>"+backStr;
          writeDataResponse(respXml,resp);
        }catch(Exception e){
            e.printStackTrace();//System.out.println(e);
        }finally { if (baos != null)  baos.close(); }
	}
	
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	    doGet(req, resp);
	}
	
	 public void writeDataResponse(String data,HttpServletResponse response) throws IOException {
	       OutputStream os = null;
	       try {
	           byte[] dataByte = data.getBytes("UTF-8");
	           os = response.getOutputStream();
	           response.setContentType("text/xml;charset=UTF-8");
	           os.write(dataByte);
	           os.flush();
	       }
	       catch (IOException e) {
	           throw e;
	       }
	       finally {
	           if (os != null)  os.close(); 
	       }
	   }

}

