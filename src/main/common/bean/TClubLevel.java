package com.chess.common.bean;

public class TClubLevel {
	/** ID */
	private int id = 0;
	/** 名称 */
	private String name = "";
	/** 最少金币 */
	private int minGold = 0;
	/** 最多金币 */
	private int maxGold = 0;
	/** 押注金币 */
	private String goldLimit = "";

	private String desc = null;
	private String param01 = "";
	private String param02 = "";
	private String param03 = "";

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMinGold() {
		return minGold;
	}

	public void setMinGold(int minGold) {
		this.minGold = minGold;
	}

	public int getMaxGold() {
		return maxGold;
	}

	public void setMaxGold(int maxGold) {
		this.maxGold = maxGold;
	}

	public String getGoldLimit() {
		return goldLimit;
	}

	public void setGoldLimit(String goldLimit) {
		this.goldLimit = goldLimit;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getParam01() {
		return param01;
	}

	public void setParam01(String param01) {
		this.param01 = param01;
	}

	public String getParam02() {
		return param02;
	}

	public void setParam02(String param02) {
		this.param02 = param02;
	}

	public String getParam03() {
		return param03;
	}

	public void setParam03(String param03) {
		this.param03 = param03;
	}

}
