package com.chess.common.bean;

import com.chess.core.net.msg.NetObject;


public class GiftCode extends NetObject
{

	/**礼品码ID*/
	private String giftCodeID="";
	/**礼品码密码*/
	private String giftCodePwd="";
	/**礼品码名称*/
	private String giftCodeName="";
	/**道具ID*/
	private String itemIDAndNums="";
	/**金币*/
	private int gold=0;
	/**礼品码有效期*/
	private String lift="";
	/**最多使用数量*/
	private int maxUse=0;
	/**服务器限制*/
	private int limit=0;
	/**礼品码生成数量*/
	private int giftCodeQuentity=0;






	public String getGiftCodeID() {
		return giftCodeID;
	}
	public void setGiftCodeID(String giftCodeID) {
		this.giftCodeID = giftCodeID;
	}
	public String getGiftCodeName() {
		return giftCodeName;
	}
	public void setGiftCodeName(String giftCodeName) {
		this.giftCodeName = giftCodeName;
	}

	public String getItemIDAndNums() {
		return itemIDAndNums;
	}
	public void setItemIDAndNums(String itemIDAndNums) {
		this.itemIDAndNums = itemIDAndNums;
	}
 
 
	public int getGold() {
		return gold;
	}
	public void setGold(int gold) {
		this.gold = gold;
	}
	public String getLift() {
		return lift;
	}
	public void setLift(String lift) {
		this.lift = lift;
	}
	public int getMaxUse() {
		return maxUse;
	}
	public void setMaxUse(int maxUse) {
		this.maxUse = maxUse;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getGiftCodeQuentity() {
		return giftCodeQuentity;
	}
	public void setGiftCodeQuentity(int giftCodeQuentity) {
		this.giftCodeQuentity = giftCodeQuentity;
	}
	public String getGiftCodePwd() {
		return giftCodePwd;
	}
	public void setGiftCodePwd(String giftCodePwd) {
		this.giftCodePwd = giftCodePwd;
	}




}
