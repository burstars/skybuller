package com.chess.common.bean; 

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

/** 
 * 类说明 :
 */
public class ClubMember extends NetObject{
	
	public String id = "";
	public String clubCode = "";
	public String playerId = "";
	public int playerIndex = 0;
	public String playerName = "";
	public String memberId = "";
	public int memberIndex = 0;
	public String memberName = "";
	public int applyState = 0;
	public String createTime = "";
	
	public String remark = "";
	/**好友头像网络地址*/
	public String headImgUrl = "";

	public int goldNum = 0;
    public int totalPay = 0;

	@Override
	public void serialize(ObjSerializer ar)
	{
		id = ar.sString(id);
		clubCode=ar.sString(clubCode);
		playerId=ar.sString(playerId);
		playerIndex = ar.sInt(playerIndex);
		playerName = ar.sString(playerName);
		memberId=ar.sString(memberId);
		memberIndex = ar.sInt(memberIndex);
		memberName = ar.sString(memberName);
		applyState=ar.sInt(applyState);
		createTime = ar.sString(createTime);
		remark = ar.sString(remark);
		headImgUrl = ar.sString(headImgUrl);
		goldNum = ar.sInt(goldNum);
        totalPay = ar.sInt(totalPay);
	}
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getClubCode() {
		return clubCode;
	}


	public void setClubCode(String clubCode) {
		this.clubCode = clubCode;
	}


	public String getPlayerId() {
		return playerId;
	}


	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}


	public int getPlayerIndex() {
		return playerIndex;
	}


	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}


	public String getPlayerName() {
		return playerName;
	}


	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}


	public String getMemberId() {
		return memberId;
	}


	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}


	public int getMemberIndex() {
		return memberIndex;
	}


	public void setMemberIndex(int memberIndex) {
		this.memberIndex = memberIndex;
	}


	public String getMemberName() {
		return memberName;
	}


	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}


	public int getApplyState() {
		return applyState;
	}


	public void setApplyState(int applyState) {
		this.applyState = applyState;
	}


	public String getCreateTime() {
		return createTime;
	}


	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getHeadImgUrl() {
		return headImgUrl;
	}


	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public int getGoldNum() {
		return goldNum;
	}

    public int getTotalPay() {
        return totalPay;
    }
    
    public void setTotalPay(int totalPay) {
        this.totalPay = totalPay;
    }

	public void setGoldNum(int goldNum) {
		this.goldNum = goldNum;
	}


}
 
