package com.chess.common.bean;

import java.util.Date;

import com.chess.common.DateService;
import com.chess.common.constant.PayConstant;

/**
 * 登录日志
 * 
 * @author
 * @since
 */
public class LoginLog {
	/** 日志记录的id */
	private String loginLogID;

	/** 玩家id */
	private String playerID;

	// /** 登录结果 */
	// private String result;

	/** 客户端登录时使用的ip地址(aaa.bbb.ccc.ddd) */
	private String ip;

	/** 登录时间 */
	private Date loginTime;

	/** 登出时间 */
	private Date quitTime;
	/** 该玩家注册时间 注册时间冗余下，方便数据统计 */
	private Date registerTime;
	private int platformType = PayConstant.HISTORY_PLATFORM_TYPE_E_SURFING;
	/**
	 * 客户端手机设备的相关信息
	 */
	private String clientIp;// 客户端ip
	private String httpClientPort = "";
	/** ip 地址对应的区域 */
	private String ipAddress = "";
	private String deviceBrand = ""; // 手机厂商
	private String systemVersion = "";// 系统版本
	private String phone = "";// 客户端手机号码
	/** 玩家名称 */
	private String playName;
	/** 玩家索引 */
	private Integer playIndex;
	private int deviceFlag = 0; // 设备标识，1 IOS，2 ANDROID

	/** 表后缀 */
	private String tableNameSuffix = DateService.getTableSuffixByType("",
			DateService.DATE_BY_DAY);

	public LoginLog() {
	}

	public String getLoginLogID() {
		return loginLogID;
	}

	public void setLoginLogID(String loginLogID) {
		this.loginLogID = loginLogID;
	}

	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	// public String getResult() {
	// return result;
	// }
	//
	// public void setResult(String result) {
	// this.result = result;
	// }

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getQuitTime() {
		return quitTime;
	}

	public void setQuitTime(Date quitTime) {
		this.quitTime = quitTime;
	}

	public String getTableNameSuffix() {
		return tableNameSuffix;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}

	public void setTableNameSuffix(String tableNameSuffix) {
		this.tableNameSuffix = tableNameSuffix;
	}

	public int getPlatformType() {
		return platformType;
	}

	public void setPlatformType(int platformType) {
		this.platformType = platformType;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getHttpClientPort() {
		return httpClientPort;
	}

	public void setHttpClientPort(String httpClientPort) {
		this.httpClientPort = httpClientPort;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getDeviceBrand() {
		return deviceBrand;
	}

	public void setDeviceBrand(String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}

	public String getSystemVersion() {
		return systemVersion;
	}

	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPlayName() {
		return playName;
	}

	public void setPlayName(String playName) {
		this.playName = playName;
	}

	public Integer getPlayIndex() {
		return playIndex;
	}

	public void setPlayIndex(Integer playIndex) {
		this.playIndex = playIndex;
	}

	public int getDeviceFlag() {
		return deviceFlag;
	}

	public void setDeviceFlag(int deviceFlag) {
		this.deviceFlag = deviceFlag;
	}

}
