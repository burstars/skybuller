/**
 * Description:VIP战绩记录
 */
package com.chess.common.bean;

import java.util.Date;

public class GameVipRecord {

	/** 玩家ID */
	private String playerID = "";
	/** 房间ID */
	private int roomID = 0;
	/** 是否房主 */
	private int isHost = 0;
	/** 番数 */
	private int gameMultiple = 0;
	/** 番型-【庄】 */
	private int gameType1 = 0;

	/** 本局得分 */
	private int gameScore = 0;
	/** 结算时间 */
	private Date recordDate = null;

	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public int getRoomID() {
		return roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	public int getIsHost() {
		return isHost;
	}

	public void setIsHost(int isHost) {
		this.isHost = isHost;
	}

	public int getGameMultiple() {
		return gameMultiple;
	}

	public void setGameMultiple(int gameMultiple) {
		this.gameMultiple = gameMultiple;
	}

	public int getGameType1() {
		return gameType1;
	}

	public void setGameType1(int gameType1) {
		this.gameType1 = gameType1;
	}



	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public int getGameScore() {
		return gameScore;
	}

	public void setGameScore(int gameScore) {
		this.gameScore = gameScore;
	}

}
