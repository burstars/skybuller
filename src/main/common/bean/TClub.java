package com.chess.common.bean;

import java.util.Date;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class TClub extends NetObject{
	/**游戏ID*/
	private String id ="";
	/**俱乐部编号*/
	private int clubCode = 0;
	/**俱乐部名称*/
	private String clubName = "";
	/**俱乐部创建人*/
	private String createPlayerId = "";
	private String createPlayerName = "";
	/**库存：房卡数量*/
	private int fangkaNum = 0 ;
	/**俱乐部状态1=有效;2=代理解散;3=系统收回*/
	private int clubState = 0;
	
	private String createTime = null;
	/** 俱乐部类型：1=我创建的；2=我加入的 */
	private int clubType = 0;
	/** 俱乐部人数 */
	private int memberCount = 0;
	/**头像*/
	private String headUrl = "";
	
	private String roomSwtich="";
	private int goldNum=0;
	private int memberGold=0;
	private int tableNum=0;
	
	@Override
	public void serialize(ObjSerializer ar) {
		id = ar.sString(id);
		clubCode = ar.sInt(clubCode);
		clubName = ar.sString(clubName);
		createPlayerId = ar.sString(createPlayerId);
		createPlayerName = ar.sString(createPlayerName);
		fangkaNum = ar.sInt(fangkaNum);
		clubState = ar.sInt(clubState);
		clubType = ar.sInt(clubType);
		
		memberCount = ar.sInt(memberCount);
		headUrl = ar.sString(headUrl);
		roomSwtich = ar.sString(roomSwtich);
		goldNum = ar.sInt(goldNum);
		tableNum = ar.sInt(tableNum);
		createTime=ar.sString(createTime);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public String getClubName() {
		return clubName;
	}

	public void setClubName(String clubName) {
		this.clubName = clubName;
	}

	public String getCreatePlayerId() {
		return createPlayerId;
	}

	public void setCreatePlayerId(String createPlayerId) {
		this.createPlayerId = createPlayerId;
	}

	public int getFangkaNum() {
		return fangkaNum;
	}

	public void setFangkaNum(int fangkaNum) {
		this.fangkaNum = fangkaNum;
	}

	public int getClubState() {
		return clubState;
	}

	public void setClubState(int clubState) {
		this.clubState = clubState;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public int getClubType() {
		return clubType;
	}

	public void setClubType(int clubType) {
		this.clubType = clubType;
	}

	public int getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	public String getRoomSwtich() {
		return roomSwtich;
	}

	public int getGoldNum() {
		return goldNum;
	}

	public void setGoldNum(int goldNum) {
		this.goldNum = goldNum;
	}

	public int getTableNum() {
		return tableNum;
	}

	public void setTableNum(int tableNum) {
		this.tableNum = tableNum;
	}

	public void setRoomSwtich(String roomSwtich) {
		this.roomSwtich = roomSwtich;
	}

	public String getCreatePlayerName() {
		return createPlayerName;
	}

	public void setCreatePlayerName(String createPlayerName) {
		this.createPlayerName = createPlayerName;
	}

	@Override
	public String toString() {
		return "TClub [id=" + id + ", clubCode=" + clubCode + ", clubName="
				+ clubName + ", createPlayerId=" + createPlayerId
				+ ", fangkaNum=" + fangkaNum + ", clubState=" + clubState
				+ ", clubType=" + clubType + ", memberCount=" + memberCount
				+ ", headUrl=" + headUrl 
				+ ", createPlayerName=" + createPlayerName 
				+ ", roomSwtich=" + roomSwtich
				+ ", goldNum=" + goldNum 
				+ ", memberGold=" + memberGold 
				+ ", tableNum=" + tableNum + "]";
	}

	public int getMemberGold() {
		return memberGold;
	}

	public void setMemberGold(int memberGold) {
		this.memberGold = memberGold;
	}

	
}
