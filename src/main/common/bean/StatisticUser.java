package com.chess.common.bean;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.DateService;
import com.chess.common.constant.PayConstant;



/**数据统计分析类*/
public class StatisticUser {
	private String statisticId ;
	/**统计类型id  每天更新一条  时间字符串(yyyy-MM-dd)*/
	private String statisticTime = DateService.getCurrentDate2AsString();;
	private Integer regisNum=0;//当日注册人数
	private Integer loginNum=0;//当天去重登录人数
	private Integer secdayLeftNum;//次日留存数  今天注册玩家明天登陆人数
	private Integer thirdDayLeftNum;//三日留存数  今天注册玩家后天登陆人数
	private Integer sevDayLeftNum;//七日留存数    今天注册玩家下周的今天的登陆人数
	/**当日付费玩家数量 去重*/
	private Integer payPlayerDayNum = 0;
	/** 当日付费总额*/
	private Integer payTotalDayNum = 0;
	//下面几个数据压缩在dataStr 中存储
	/**当日在线总时长*/
	private Integer onlineMinTotal = 0;
	/**当日在线时长在5分钟内的人数*/
	private Integer  numOnlineMin_5 = 0;
	/**当日在线时长在5m~30m的人数*/
	private Integer  numOnlineMin_5_30 = 0;
	/**当日在线时长在30m~60m的人数*/
	private Integer  numOnlineMin_30_60 = 0;
	/**当日在线时长在60m~120m的人数*/
	private Integer  numOnlineMin_60_120 = 0;
	/**当日在线时长在12m~300m的人数*/
	private Integer  numOnlineMin_120_300 = 0;
	/**当日在线时长在300m以上的人数*/
	private Integer  numOnlineMin_300 = 0;
	/**部分统计数据压缩串*/
	private String dataStr ="";
	
	/**以下数据通过计算得到，不存数据库*/
	private Float secDayLeftRate ;//次日留存率
	private Float thirdDayLeftRate;//三日留存率
	private Float sevDayLeftRate;//七日留存率
	private Integer onlinMinRate = 0;// 平均在线时长
	/** 平均每个用户的付费金额*/
	private Integer arpu  = 0 ;
	/**付费率 付费用户数/活跃用户数 四位小数*/
	private Double payRate =0.0000 ;
	/**各个道具统计情况*/
	private List<ItemOpStaBase> itemStaList ;
	
	private int platformType = PayConstant.HISTORY_PLATFORM_TYPE_E_SURFING;
	
	public void  cate(){
		if(secdayLeftNum!=null&&regisNum!=0){
			secDayLeftRate = secdayLeftNum.floatValue()/regisNum.floatValue();
		}
		if(thirdDayLeftNum!=null&&regisNum!=0){
			thirdDayLeftRate = thirdDayLeftNum.floatValue()/regisNum.floatValue();
		}
		if(sevDayLeftNum!=null&&regisNum!=0){
			sevDayLeftRate = sevDayLeftNum.floatValue()/regisNum.floatValue();
		}
		
		if(payPlayerDayNum!=null&&loginNum!=0){
			payRate = payPlayerDayNum.doubleValue()/loginNum.doubleValue();
		}
		if(payTotalDayNum!=null&& payPlayerDayNum!=0){
			arpu = payTotalDayNum/payPlayerDayNum;
		}
		if(onlineMinTotal!=null&&loginNum!=0){
			onlinMinRate = onlineMinTotal/loginNum;
		}
	}
	
	public  void enCodeData(){
		this.dataStr+=this.onlineMinTotal+"_";
		this.dataStr+=this.numOnlineMin_5+"_";
		this.dataStr+=this.numOnlineMin_5_30+"_";
		this.dataStr+=this.numOnlineMin_30_60+"_";
		this.dataStr+=this.numOnlineMin_60_120+"_";
		this.dataStr+=this.numOnlineMin_120_300+"_";
		this.dataStr+=this.numOnlineMin_300+"#";
		String itemStr = "";
		if(this.itemStaList!=null&&this.itemStaList.size()>0){
			for(int i=0;i<this.itemStaList.size();i++){
				ItemOpStaBase io = this.itemStaList.get(i);
				if(io!=null){
					itemStr +=io.getBase_id()+"_"+io.getUseNum()+"--";
				}
			}
		}
		this.dataStr+=itemStr;
	}
	public void deCodeData(){
		if(this.dataStr!=null&&!"".equals(this.dataStr)){
			String [] par = this.dataStr.split("#");
			if(par!=null&&par[0]!=null){
				String [] par2 = par[0].split("_");
				if(par2!=null&&par2.length==7){
					this.onlineMinTotal = Integer.valueOf(par2[0]);
					this.numOnlineMin_5 = Integer.valueOf(par2[1]);
					this.numOnlineMin_5_30 =Integer.valueOf(par2[2]);
					this.numOnlineMin_30_60 = Integer.valueOf(par2[3]);
					this.numOnlineMin_60_120 = Integer.valueOf(par2[4]);
					this.numOnlineMin_120_300 = Integer.valueOf(par2[5]);
					this.numOnlineMin_300 = Integer.valueOf(par2[6]);
				}
			}
			if(par.length>1){
				if(par[1]!=null&&par[1].length()>2){
					String [] par2 = par[1].split("--");
					if(par2!=null&&par2.length>0){
						this.itemStaList = new ArrayList<ItemOpStaBase>();
						for(int i=0;i<par2.length;i++){
							String str =par2[i];
							String [] par3 = str.split("_");
							if(par3!=null&&par3.length==2){
								ItemOpStaBase io = new ItemOpStaBase();
								io.setBase_id(Integer.valueOf(par3[0]));
								io.setUseNum(Integer.valueOf(par3[1]));
								this.itemStaList.add(io);
							}
						}
					}
				}
			}
			
		}
	}
	
	
	public String getStatisticTime() {
		return statisticTime;
	}
	public void setStatisticTime(String statisticTime) {
		this.statisticTime = statisticTime;
	}


	public Integer getArpu() {
		return arpu;
	}
	public void setArpu(Integer arpu) {
		this.arpu = arpu;
	}
	public Double getPayRate() {
		return payRate;
	}
	public void setPayRate(Double payRate) {
		this.payRate = payRate;
	}
	public Integer getRegisNum() {
		return regisNum;
	}
	public void setRegisNum(Integer regisNum) {
		this.regisNum = regisNum;
	}
	public Integer getLoginNum() {
		return loginNum;
	}
	public void setLoginNum(Integer loginNum) {
		this.loginNum = loginNum;
	}
	public Integer getSecdayLeftNum() {
		return secdayLeftNum;
	}
	public void setSecdayLeftNum(Integer secdayLeftNum) {
		this.secdayLeftNum = secdayLeftNum;
	}
	public Integer getThirdDayLeftNum() {
		return thirdDayLeftNum;
	}
	public void setThirdDayLeftNum(Integer thirdDayLeftNum) {
		this.thirdDayLeftNum = thirdDayLeftNum;
	}
	public Integer getSevDayLeftNum() {
		return sevDayLeftNum;
	}
	public void setSevDayLeftNum(Integer sevDayLeftNum) {
		this.sevDayLeftNum = sevDayLeftNum;
	}
	public Integer getPayPlayerDayNum() {
		return payPlayerDayNum;
	}
	public void setPayPlayerDayNum(Integer payPlayerDayNum) {
		this.payPlayerDayNum = payPlayerDayNum;
	}
	public Integer getPayTotalDayNum() {
		return payTotalDayNum;
	}
	public void setPayTotalDayNum(Integer payTotalDayNum) {
		this.payTotalDayNum = payTotalDayNum;
	}
	public Float getSecDayLeftRate() {
		return secDayLeftRate;
	}
	public void setSecDayLeftRate(Float secDayLeftRate) {
		this.secDayLeftRate = secDayLeftRate;
	}
	public Float getThirdDayLeftRate() {
		return thirdDayLeftRate;
	}
	public void setThirdDayLeftRate(Float thirdDayLeftRate) {
		this.thirdDayLeftRate = thirdDayLeftRate;
	}
	public Float getSevDayLeftRate() {
		return sevDayLeftRate;
	}
	public void setSevDayLeftRate(Float sevDayLeftRate) {
		this.sevDayLeftRate = sevDayLeftRate;
	}
	public String getStatisticId() {
		return statisticId;
	}
	public void setStatisticId(String statisticId) {
		this.statisticId = statisticId;
	}
	public List<ItemOpStaBase> getItemStaList() {
		return itemStaList;
	}
	public void setItemStaList(List<ItemOpStaBase> itemStaList) {
		this.itemStaList = itemStaList;
	}
	public Integer getOnlineMinTotal() {
		return onlineMinTotal;
	}
	public void setOnlineMinTotal(Integer onlineMinTotal) {
		this.onlineMinTotal = onlineMinTotal;
	}
	public Integer getNumOnlineMin_5() {
		return numOnlineMin_5;
	}
	public void setNumOnlineMin_5(Integer numOnlineMin_5) {
		this.numOnlineMin_5 = numOnlineMin_5;
	}
	public Integer getNumOnlineMin_5_30() {
		return numOnlineMin_5_30;
	}
	public void setNumOnlineMin_5_30(Integer numOnlineMin_5_30) {
		this.numOnlineMin_5_30 = numOnlineMin_5_30;
	}
	public Integer getNumOnlineMin_30_60() {
		return numOnlineMin_30_60;
	}
	public void setNumOnlineMin_30_60(Integer numOnlineMin_30_60) {
		this.numOnlineMin_30_60 = numOnlineMin_30_60;
	}
	public Integer getNumOnlineMin_60_120() {
		return numOnlineMin_60_120;
	}
	public void setNumOnlineMin_60_120(Integer numOnlineMin_60_120) {
		this.numOnlineMin_60_120 = numOnlineMin_60_120;
	}
	public Integer getNumOnlineMin_120_300() {
		return numOnlineMin_120_300;
	}
	public void setNumOnlineMin_120_300(Integer numOnlineMin_120_300) {
		this.numOnlineMin_120_300 = numOnlineMin_120_300;
	}
	public Integer getNumOnlineMin_300() {
		return numOnlineMin_300;
	}
	public void setNumOnlineMin_300(Integer numOnlineMin_300) {
		this.numOnlineMin_300 = numOnlineMin_300;
	}
	public Integer getOnlinMinRate() {
		return onlinMinRate;
	}
	public void setOnlinMinRate(Integer onlinMinRate) {
		this.onlinMinRate = onlinMinRate;
	}

	public int getPlatformType() {
		return platformType;
	}

	public void setPlatformType(int platformType) {
		this.platformType = platformType;
	}
	
	
	
}
