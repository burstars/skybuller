package com.chess.common.bean;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 房间战绩记录
 * @author Administrator
 *
 */
public class RoomRecord extends NetObject{
	
	public String playerId;
	public int playerIndex;
	public String playerName;
	public int score;
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		//TODO 序列化
		playerId = ar.sString(playerId);
		playerIndex = ar.sInt(playerIndex);
		playerName = ar.sString(playerName);
		score = ar.sInt(score);
	}
	
}
