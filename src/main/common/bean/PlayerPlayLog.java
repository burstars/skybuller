package com.chess.common.bean;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class PlayerPlayLog extends NetObject {
    private String playerID = "";
    private Integer totalPlay = 0;
    private Integer totalWin = 0;
    private Integer totalLose = 0;
    /**
     * -->TODO 屏蔽大风、红中满天飞玩法 cuiweiqing 2016-07-27
     */
//    private Integer totalWinHongZong = 0;
//    private Integer totalWinDafeng = 0;
    private Integer totalWinBaozhongbao = 0;
    private Integer totalWinLou = 0;
    private Integer totalWinGold = 0;
    private Integer totalLoseGold = 0;
    private Integer totalVipCount = 0;
    private Integer totalNormalCount = 0;

    @Override
    public void serialize(ObjSerializer ar) {
        playerID = ar.sString(playerID);
        totalPlay = ar.sInt(totalPlay);
        totalWin = ar.sInt(totalWin);
        totalLose = ar.sInt(totalLose);
//        totalWinHongZong = ar.sInt(totalWinHongZong);
//        totalWinDafeng = ar.sInt(totalWinDafeng);
        totalWinBaozhongbao = ar.sInt(totalWinBaozhongbao);
        totalWinLou = ar.sInt(totalWinLou);
        totalWinGold = ar.sInt(totalWinGold);
        totalLoseGold = ar.sInt(totalLoseGold);
        totalVipCount = ar.sInt(totalVipCount);
        totalNormalCount = ar.sInt(totalNormalCount);
    }

    public String getPlayerID() {
        return playerID;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }

    public Integer getTotalPlay() {
        return totalPlay;
    }

    public void setTotalPlay(Integer totalPlay) {
        this.totalPlay = totalPlay;
    }

    public Integer getTotalWin() {
        return totalWin;
    }

    public void setTotalWin(Integer totalWin) {
        this.totalWin = totalWin;
    }

    public Integer getTotalLose() {
        return totalLose;
    }

    public void setTotalLose(Integer totalLose) {
        this.totalLose = totalLose;
    }

//    public Integer getTotalWinHongZong() {
//        return totalWinHongZong;
//    }
//
//    public void setTotalWinHongZong(Integer totalWinHongZong) {
//        this.totalWinHongZong = totalWinHongZong;
//    }
//
//    public Integer getTotalWinDafeng() {
//        return totalWinDafeng;
//    }
//
//    public void setTotalWinDafeng(Integer totalWinDafeng) {
//        this.totalWinDafeng = totalWinDafeng;
//    }

    public Integer getTotalWinBaozhongbao() {
        return totalWinBaozhongbao;
    }

    public void setTotalWinBaozhongbao(Integer totalWinBaozhongbao) {
        this.totalWinBaozhongbao = totalWinBaozhongbao;
    }

    public Integer getTotalWinLou() {
        return totalWinLou;
    }

    public void setTotalWinLou(Integer totalWinLou) {
        this.totalWinLou = totalWinLou;
    }

    public Integer getTotalWinGold() {
        return totalWinGold;
    }

    public void setTotalWinGold(Integer totalWinGold) {
        this.totalWinGold = totalWinGold;
    }

    public Integer getTotalLoseGold() {
        return totalLoseGold;
    }

    public void setTotalLoseGold(Integer totalLoseGold) {
        this.totalLoseGold = totalLoseGold;
    }

    public Integer getTotalVipCount() {
        return totalVipCount;
    }

    public void setTotalVipCount(Integer totalVipCount) {
        this.totalVipCount = totalVipCount;
    }

    public Integer getTotalNormalCount() {
        return totalNormalCount;
    }

    public void setTotalNormalCount(Integer totalNormalCount) {
        this.totalNormalCount = totalNormalCount;
    }
}