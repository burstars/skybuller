package com.chess.common.bean;

import java.util.Date;

import com.chess.common.bean.Task;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

/**玩家任务定义*/
public class PlayerTask  extends NetObject{
	
	/** 编号 */
	private String playerTaskID;
	/** 玩家编号 */
	private String playerID;
	/** 任务基础编号 */
	private Integer taskID = 0;

	/** 任务状态(0.未接受 1.已经接受未完成，2已经完成，未领奖 3 已领奖) */
	private Integer state = 1;
	/**接任务时间**/
	private Date acceptTime;
	/**任务 中的具体信息  当前只有完成度*/
	private String stateDec = "";
	

	/** 任务类型  任务类型0.累积任务 1.每日任务 2.每日随机任务（每天只开放一个） 3 进阶场随机任务 */
	private Integer taskType = 1;
	
	//以下部分就是task的具体内容
	/**任务难度 未使用 **/
	private Integer difficu = 0;
	
	/** 任务名称 */
	private String name;
	/** 任务描述 */
	private String description;
	/** 任务详细描述 */
	private String detailDesc;


	/** 奖励信息*/
	private String reward;
	/** 触发的新任务基础id序列，逗号分割，类似"2001,3333,4005"*/
	private String triggerTaskIDs;

	//任务标准位，1表示这个任务由客户端判断是否已经完成，其他标准位待添加 未使用
	private Integer taskFlag = 0;
	
	/** 任务最大完成数量，用于活跃度任务 */
	private Integer maxFinish = 0;
	
	/**已经完成的次数，用于活跃度任务 */
	private Integer finishNum = 0;
	

	
	//引导任务有这2个数据 未使用
	private Integer taskIndex=0;
	private Integer nextTask=0;
	
	 /** 前往界面 0无前往界面  1大厅 2单人战斗大厅 3商城购买道具 4彩票 5抽奖
	  * 如果任务类型是3 进阶场随机任务的话，该值表示 任务需要记录的数值类型 客户端提交的*/
	private Integer goToPanelNum = 0;
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		taskID = ar.sInt(taskID);
		state = ar.sInt(state);
		taskType = ar.sInt(taskType);
		name = ar.sString(name);
		description = ar.sString(description);
		reward = ar.sString(reward);
		finishNum = ar.sInt(finishNum);
		maxFinish = ar.sInt(maxFinish);
		goToPanelNum = ar.sInt(goToPanelNum);
	}
	
	
	/***任务基础数据添加*/
	public void copyTask(Task task){
		taskID = task.getTaskID();
		difficu = task.getDifficu();
		name = task.getName();
		description = task.getDescription();
		detailDesc = task.getDetailDesc();
		taskType  = task.getType();
		reward = task.getReward();
		taskFlag = task.getTaskFlag();
		triggerTaskIDs = task.getTriggerTaskID();
		maxFinish = task.getMaxFinish();
		taskIndex = task.getTaskIndex();
		nextTask = task.getNextTaskID();
		if(task.getDetailDesc()!=null){
			goToPanelNum = Integer.valueOf(task.getDetailDesc());
		}
		
	}
	
	public String getPlayerTaskID() {
		return playerTaskID;
	}
	public void setPlayerTaskID(String playerTaskID) {
		this.playerTaskID = playerTaskID;
	}
	public String getPlayerID() {
		return playerID;
	}
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
	public Integer getTaskID() {
		return taskID;
	}
	public void setTaskID(Integer taskID) {
		this.taskID = taskID;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Date getAcceptTime() {
		return acceptTime;
	}
	public void setAcceptTime(Date acceptTime) {
		this.acceptTime = acceptTime;
	}
	public String getStateDec() {
		return stateDec;
	}
	public void setStateDec(String stateDec) {
		this.stateDec = stateDec;
	}
	public Integer getTaskType() {
		return taskType;
	}
	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}
	public Integer getDifficu() {
		return difficu;
	}
	public void setDifficu(Integer difficu) {
		this.difficu = difficu;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDetailDesc() {
		return detailDesc;
	}
	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}
	public String getReward() {
		return reward;
	}
	public void setReward(String reward) {
		this.reward = reward;
	}
	public String getTriggerTaskIDs() {
		return triggerTaskIDs;
	}
	public void setTriggerTaskIDs(String triggerTaskIDs) {
		this.triggerTaskIDs = triggerTaskIDs;
	}
	public Integer getTaskFlag() {
		return taskFlag;
	}
	public void setTaskFlag(Integer taskFlag) {
		this.taskFlag = taskFlag;
	}
	public Integer getMaxFinish() {
		return maxFinish;
	}
	public void setMaxFinish(Integer maxFinish) {
		this.maxFinish = maxFinish;
	}
	public Integer getFinishNum() {
		return finishNum;
	}
	public void setFinishNum(Integer finishNum) {
		this.finishNum = finishNum;
	}
	public Integer getTaskIndex() {
		return taskIndex;
	}
	public void setTaskIndex(Integer taskIndex) {
		this.taskIndex = taskIndex;
	}
	public Integer getNextTask() {
		return nextTask;
	}
	public void setNextTask(Integer nextTask) {
		this.nextTask = nextTask;
	}


	public Integer getGoToPanelNum() {
		return goToPanelNum;
	}


	public void setGoToPanelNum(Integer goToPanelNum) {
		this.goToPanelNum = goToPanelNum;
	}

}
