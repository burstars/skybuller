package com.chess.common.bean;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;


public class UserFriend  extends NetObject{
	/**ID*/
	public String playerID = "";
	
	/**玩家游戏中昵称**/
	public String playerName = "";

	/**头像索引**/
	public int headImg = 0;
	
	/** 玩家货币0 金币*/
	public int gold = 0;
  
	/**性别*/
	public int sex = 0;
	
	/**索引号,类似qq号*/
	public int palyerIndex = 0;
	
	/**是否在线*/
	public int isOnline = 0;
	
	//是否可以加为好友(0可以，1不可以)
	public int canFriend=0;
	
	/**备注名称*/
	public String remark = "";
	
	/**加好友标志*/
	public int applyResult = 0;
	
	/**好友头像网络地址*/
	public String headImgUrl = "";
	
	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	
	@Override
	public void serialize(ObjSerializer ar)
	{
		playerID = ar.sString(playerID);
		playerName=ar.sString(playerName);
		headImg=ar.sInt(headImg);
		gold=ar.sInt(gold);
		sex = ar.sInt(sex);
		palyerIndex = ar.sInt(palyerIndex);
		isOnline = ar.sInt(isOnline);
		canFriend = ar.sInt(canFriend);
		applyResult = ar.sInt(applyResult);
		remark = ar.sString(remark);
		headImgUrl = ar.sString(headImgUrl);
	}

	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getHeadImg() {
		return headImg;
	}

	public void setHeadImg(int headImg) {
		this.headImg = headImg;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getPalyerIndex() {
		return palyerIndex;
	}

	public void setPalyerIndex(int palyerIndex) {
		this.palyerIndex = palyerIndex;
	}

	public int getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(int isOnline) {
		this.isOnline = isOnline;
	}
}
