package com.chess.common.bean;

 
import org.apache.mina.core.session.IoSession;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;



public class GameLogicServerInfo extends NetObject
{
	public String serverID="";
	//
	public String serverName="";
	//
	public String telecomIP="";
	public String unicomIP="";
	public int gamePort=0;
	public int patchPort=0;
	
	//
	public int room0PlayerNum=0;
	public int room1PlayerNum=0;
	public int room2PlayerNum=0;
	public int room3PlayerNum=0;
	public int onlinePlayerNum=0;
	//
	public IoSession session;
	//
	public GameLogicServerInfo()
	{
		
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
 
    }
	 
 
}
