package com.chess.common.bean;

/**各种获得的奖励*/
public class Reward {

	private int id;
	
	private String name;
	
	private String value;
	
	private String property_1;
	
	private String property_2;
	
	private String property_3;

	
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getProperty_1() {
		return property_1;
	}

	public void setProperty_1(String property_1) {
		this.property_1 = property_1;
	}

	public String getProperty_2() {
		return property_2;
	}

	public void setProperty_2(String property_2) {
		this.property_2 = property_2;
	}

	public String getProperty_3() {
		return property_3;
	}

	public void setProperty_3(String property_3) {
		this.property_3 = property_3;
	}
}
