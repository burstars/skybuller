package com.chess.common.bean;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.constant.GameConstant;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

//对战发给客户端的
public class SimplePlayer extends NetObject {

	// ID
	public String playerID = "";

	/** 玩家游戏中昵称 **/
	public String playerName = "";

	/** 头像索引 **/
	public int headImg = 0;
	/** 玩家货币0 金币 */
	public int gold = 0;

	public int totalPoint = 0;

	// /////多个得分项目//////
	public List<Integer> calcTypes = new ArrayList<Integer>();
	public List<Integer> points = new ArrayList<Integer>();
	public List<Integer> fanTypes = new ArrayList<Integer>();
	public List<Integer> fanNums = new ArrayList<Integer>();
	// ///////////

	public int tablePos = 0;

	/** 性别 */
	public int sex = 0;

	/** 索引号,类似qq号 */
	public int palyerIndex = 0;

	public String desc = "";

	public int fan = 0;// 输赢的番
	// 牌局结局
	public int gameResult = 0;

	// 是否可以加为好友
	public int canFriend = 0;

	/** 是否坐在桌子上，1：在桌上；0：在大厅或者离线 */
	public int inTable = 0;

	/** VIP房间统计数据 */
	public int zhuangCount = 0;
	public int winCount = 0;
	public int dianpaoCount = 0;
	public int mobaoCount = 0;
	public int baozhongbaoCount = 0;
	public int mingdanCount = 0;//明蛋次数（瓦房店麻将）
	public int andanCount = 0;//暗蛋次数（瓦房店麻将）

	public String ip = "";
	/** 开局准备状态  20170209 */
	public int readyFlag = 0;

	public int isHu = 0;
	// 胡的牌
	public int huCard = 0;

	// 胡牌顺序 1、2、3；0是没胡牌的人
	public int hupaiOrder = 0;
	// 谁给点的炮，记录点炮玩家座位号,
	public String paoPlayerName = "";

	public String headImgUrl = "";
	public String location = "";

	public SimplePlayer() {
	}

	@Override
	public void serialize(ObjSerializer ar) {
		playerID = ar.sString(playerID);
		playerName = ar.sString(playerName);
		headImg = ar.sInt(headImg);
		sex = ar.sInt(sex);
		palyerIndex = ar.sInt(palyerIndex);
		gold = ar.sInt(gold);

		totalPoint = ar.sInt(totalPoint);

		// 多个得分项目
		calcTypes = (List<Integer>) ar.sIntArray(calcTypes);
		points = (List<Integer>) ar.sIntArray(points);
		fanTypes = (List<Integer>) ar.sIntArray(fanTypes);
		fanNums = (List<Integer>) ar.sIntArray(fanNums);

		tablePos = ar.sInt(tablePos);
		desc = ar.sString(desc);
		fan = ar.sInt(fan);
		gameResult = ar.sInt(gameResult);
		canFriend = ar.sInt(canFriend);
		inTable = ar.sInt(inTable);

		zhuangCount = ar.sInt(zhuangCount);
		winCount = ar.sInt(winCount);
		dianpaoCount = ar.sInt(dianpaoCount);
		mobaoCount = ar.sInt(mobaoCount);
		baozhongbaoCount = ar.sInt(baozhongbaoCount);
		
		mingdanCount = ar.sInt(mingdanCount);
		andanCount = ar.sInt(andanCount);
		
		ip = ar.sString(ip);
		readyFlag = ar.sInt(readyFlag);

		isHu = ar.sInt(isHu);
		huCard = ar.sInt(huCard);

		hupaiOrder = ar.sInt(hupaiOrder);
		paoPlayerName = ar.sString(paoPlayerName);

		headImgUrl = ar.sString(headImgUrl);
		location = ar.sString(location);
	}

	
}