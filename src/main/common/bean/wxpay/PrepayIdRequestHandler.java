package com.chess.common.bean.wxpay;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import com.chess.common.MD5Service;

public class PrepayIdRequestHandler extends RequestHandler {

	public PrepayIdRequestHandler(HttpServletRequest request,
			HttpServletResponse response) {
		super(request, response);
	}

	/**
	 * 创建签名SHA1
	 * 
	 * @param signParams
	 * @return
	 * @throws Exception
	 */
	public String createSHA1Sign() {
		StringBuffer sb = new StringBuffer();
		Set es = super.getAllParameters().entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = (String) entry.getKey();
			String v = (String) entry.getValue();
			sb.append(k + "=" + v + "&");
		}
		String params = sb.substring(0, sb.lastIndexOf("&"));
		String appsign = Sha1Util.getSha1(params);
		// this.setDebugInfo(this.getDebugInfo() + "\r\n" + "sha1 sb:" +
		// params);
		// this.setDebugInfo(this.getDebugInfo() + "\r\n" + "app sign:" +
		// appsign);
		return appsign;
	}

	/**
	 * 创建签名MD5 STiV modify
	 * 
	 * @param signParams
	 * @return
	 * @throws Exception
	 */
	public String createMD5Sign() {
		StringBuffer sb = new StringBuffer();
		Set es = super.getAllParameters().entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = (String) entry.getKey();
			String v = (String) entry.getValue();
			sb.append(k + "=" + v + "&");
		}

		sb.append("key=" + "12345678123456781234567812345678");

		String param = sb.toString();

		String appsign = MD5Service.encryptString(param);
		// this.setDebugInfo(this.getDebugInfo() + "\r\n" + "sha1 sb:" +
		// params);
		// this.setDebugInfo(this.getDebugInfo() + "\r\n" + "app sign:" +
		// appsign);
		return appsign;
	}

	// STiV modify
	public String toXml() throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		sb.append("<xml>");

		Set es = super.getAllParameters().entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();

			sb.append("<" + (String) entry.getKey() + ">");

			sb.append((String) entry.getValue());
			sb.append("</" + (String) entry.getKey() + ">");
		}
		sb.append("</xml>");

		// return sb.toString();
		return new String(sb.toString().getBytes("UTF-8"), "ISO8859-1");
	}

	/**
	 * 创建签名SHA1
	 * 
	 * @param prepayid
	 * @return
	 * @throws Exception
	 */
	public String createSHA1SignAgain(String prepayid) {
		SortedMap map = super.getAllParameters();
		List<BasicNameValuePair> signParams = new LinkedList<BasicNameValuePair>();
		signParams.add(new BasicNameValuePair("appid", map.get("appid")
				.toString()));
		signParams
				.add(new BasicNameValuePair("appkey", WxConstantUtil.APP_KEY));
		signParams.add(new BasicNameValuePair("noncestr", map.get("noncestr")
				.toString()));
		signParams.add(new BasicNameValuePair("package", "Sign=WXPay"));
		signParams.add(new BasicNameValuePair("partnerid",
				WxConstantUtil.PARTNER));
		signParams.add(new BasicNameValuePair("prepayid", prepayid));
		signParams.add(new BasicNameValuePair("timestamp", map.get("timestamp")
				.toString()));

		return genSign(signParams);
	}

	private String genSign(List<BasicNameValuePair> params) {
		StringBuilder sb = new StringBuilder();

		int i = 0;
		for (; i < params.size() - 1; i++) {
			sb.append(params.get(i).getName());
			sb.append('=');
			sb.append(params.get(i).getValue());
			sb.append('&');
		}
		sb.append(params.get(i).getName());
		sb.append('=');
		sb.append(params.get(i).getValue());
		String sha1 = Sha1Util.getSha1(sb.toString());
		return sha1;
	}

	// 返回于支付参数
	public String GetPrePayStr() {
		StringBuffer sb = new StringBuffer("{");
		Set es = super.getAllParameters().entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = (String) entry.getKey();
			String v = (String) entry.getValue();
			if (null != v && !"".equals(v) && !"appkey".equals(k)) {
				sb.append("\"" + k + "\":\"" + v + "\",");
			}
		}
		String params = sb.substring(0, sb.lastIndexOf(","));
		params += "}";

		this.setDebugInfo(this.getDebugInfo() + "\r\n" + "post data:" + params);
		return params;
	}

	// 提交预支付
	public String sendPrepay() throws JSONException {
		String prepayid = "";
		String requestUrl = super.getGateUrl();
		this.setDebugInfo(this.getDebugInfo() + "\r\n" + "requestUrl:"
				+ requestUrl);
		TenpayHttpClient httpClient = new TenpayHttpClient();
		httpClient.setReqContent(requestUrl);
		String params = GetPrePayStr();

		String resContent = "";
		if (httpClient.callHttpPost(requestUrl, params)) {
			resContent = httpClient.getResContent();
			if (2 == resContent.indexOf("prepayid")) {
				prepayid = JsonUtil.getJsonValue(resContent, "prepayid");
			}
			this.setDebugInfo(this.getDebugInfo() + "\r\n" + "resContent:"
					+ resContent);
		}
		return prepayid;
	}

	// 判断access_token是否失效
	public String sendAccessToken() {
		String accesstoken = "";
		StringBuffer sb = new StringBuffer("{");
		Set es = super.getAllParameters().entrySet();
		Iterator it = es.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String k = (String) entry.getKey();
			String v = (String) entry.getValue();
			if (null != v && !"".equals(v) && !"appkey".equals(k)) {
				sb.append("\"" + k + "\":\"" + v + "\",");
			}
		}
		String params = sb.substring(0, sb.lastIndexOf(","));
		params += "}";

		String requestUrl = super.getGateUrl();
		// this.setDebugInfo(this.getDebugInfo() + "\r\n" + "requestUrl:"
		// + requestUrl);
		TenpayHttpClient httpClient = new TenpayHttpClient();
		httpClient.setReqContent(requestUrl);
		String resContent = "";
		// this.setDebugInfo(this.getDebugInfo() + "\r\n" + "post data:" +
		// params);
		if (httpClient.callHttpPost(requestUrl, params)) {
			resContent = httpClient.getResContent();
			if (2 == resContent.indexOf(WxConstantUtil.ERRORCODE)) {
				accesstoken = resContent.substring(11, 16);// 获取对应的errcode的值
			}
		}
		return accesstoken;
	}
}
