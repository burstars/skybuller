package com.chess.common.bean.wxpay;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PackageRequestHandler extends RequestHandler {

	public PackageRequestHandler(HttpServletRequest request,
			HttpServletResponse response) {
		super(request, response);
	}
	
	public String bakpack="";

	/**
	 * 获取带参数的请求URL
	 * @return String
	 * @throws UnsupportedEncodingException 
	 */
	@Override
	public String getRequestURL() {
		
		this.createSign();
		
		StringBuffer sb = new StringBuffer();
		String enc = TenpayUtil.getCharacterEncoding(this.request, this.response);
		Set es = super.getAllParameters().entrySet();
		Iterator it = es.iterator();
		while(it.hasNext()) {
			Map.Entry entry = (Map.Entry)it.next();
			String k = (String)entry.getKey();
			String v = (String)entry.getValue();
			
			try {
				if(!k.equals("sign"))
					sb.append(k + "=" + URLEncoder.encode(v, enc) + "&");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		
		//去掉最后一个&
		String reqPars = sb.substring(0, sb.lastIndexOf("&"));
		reqPars +="&sign="+super.getAllParameters().get("sign").toString().toUpperCase();;
		// 设置debug信息
		return reqPars;
		
	}
}
