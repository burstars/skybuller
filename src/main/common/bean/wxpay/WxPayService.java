package com.chess.common.bean.wxpay;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.chess.common.HttpUtil;
import com.chess.common.JSONService;
import com.chess.common.SpringService;
import com.chess.common.bean.MallItem;
import com.chess.common.bean.User;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.struct.pay.GameBuyItemAckMsg;
import com.chess.common.service.IPayService;

import org.apache.commons.httpclient.NameValuePair;

public class WxPayService {
	public static void updateOrder(String orderno, String total_fee,
			String discount) {
		IPayService payService = (IPayService) SpringService
				.getBean("payService");
		payService.payCallBack(orderno, total_fee, discount);
	} 

	// 生成订单
	public static void createOrder(User player, Integer money,
			String orderNo, Integer flagAccess, Date payTime, int buyItemID) {
		IPayService payService = (IPayService) SpringService
				.getBean("payService");
		payService.addPlayerMoneyBase(player, money, orderNo, flagAccess,
				payTime, buyItemID);
	}

	// STiV modify V3版本支付
	// 生成购买的商品信息
	public static boolean genProductArgs(User player, String out_trade_no,
			MallItem item, GameBuyItemAckMsg gbiam) {
		boolean result = false;
		// 接收财付通通知的URL
		String notify_url = WxConstantUtil.NOTIFY_URL.trim();

		PrepayIdRequestHandler prepayReqHandler = new PrepayIdRequestHandler(
				null, null);// 获取prepayid的请求类

		try {

			String noncestr = WXUtil.getNonceStr();
			String timestamp = WXUtil.getTimeStamp();

			prepayReqHandler.setParameter("appid", (String)GameContext.getPayConfig("APP_ID"));
			prepayReqHandler.setParameter("mch_id", (String)GameContext.getPayConfig("PARTNER"));
			//prepayReqHandler.setParameter("mch_id", WxConstantUtil.PARTNER);
			prepayReqHandler.setParameter("body", item.getName().trim());
			prepayReqHandler.setParameter("nonce_str", noncestr);
			prepayReqHandler.setParameter("out_trade_no", out_trade_no); // 商家订单号
			prepayReqHandler.setParameter("total_fee", item.getPrice() * 100
					+ ""); // 商品金额,以分为单位
			prepayReqHandler.setParameter("spbill_create_ip", "192.168.1.1"); // 订单生成的机器IP，指用户浏览器端IP
			prepayReqHandler.setParameter("notify_url", (String) GameContext.getPayConfig("wx_notifyUrl")); // 接收财付通通知的URL
		
			prepayReqHandler.setParameter("trade_type", "APP"); // 接收财付通通知的URL


			String sign = prepayReqHandler.createMD5Sign();
			prepayReqHandler.setParameter("sign", sign);


			String sendXmlStr = prepayReqHandler.toXml();

			byte[] buf = HttpUtil.httpPost(
					"https://api.mch.weixin.qq.com/pay/unifiedorder",
					sendXmlStr);
			
			if(buf == null) {
				return false;
			}

			String preXMLResultStr = new String(buf, WxConstantUtil.INPUT_CHARSET);


			Map<String, String> xmlResult = JSONService.xmlElements(preXMLResultStr);
			
			//栾天宇 临时修改
			String prepayIDString = xmlResult.get("prepay_id");
			JSONObject preJsonResult = new JSONObject();
			preJsonResult.put("appid", (String)GameContext.getPayConfig("APP_ID"));
			preJsonResult.put("noncestr", noncestr);
			preJsonResult.put("package", WxConstantUtil.packageValue);
			preJsonResult.put("partnerid", (String)GameContext.getPayConfig("PARTNER"));
			preJsonResult.put("prepayid", prepayIDString);
			preJsonResult.put("timestamp", timestamp);

			PrepayIdRequestHandler respHandler = new PrepayIdRequestHandler(
					null, null);

			respHandler.setParameter("appid", (String)GameContext.getPayConfig("APP_ID"));
			respHandler.setParameter("noncestr", noncestr);
			respHandler.setParameter("package", WxConstantUtil.packageValue);
			respHandler.setParameter("partnerid", (String)GameContext.getPayConfig("PARTNER"));
			respHandler.setParameter("prepayid", prepayIDString);
			respHandler.setParameter("timestamp", timestamp);

			String respSign = respHandler.createMD5Sign();
			respHandler.setParameter("sign", respSign);

			preJsonResult.put("sign", respSign);

			gbiam.order = out_trade_no;
			gbiam.otherstr = preJsonResult.toString();
			result = true;
		} catch (JSONException e) {
			result = false;
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}

}
