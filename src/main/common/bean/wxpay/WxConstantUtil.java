package com.chess.common.bean.wxpay;

import com.chess.common.framework.GameContext;

public class WxConstantUtil {
	/**
	 * 商家可以考虑读取配置文件
	 */  
	
	public static String APP_ID = "wx86b80aa1cfccf809";// 微信开发平台应用id
	public static String APP_SECRET = "d92ca73d8397b656f1de80a03b521003";// 应用对应的凭证
	
	// 应用对应的密钥
	public static String APP_KEY = "";
	public static String PARTNER = "";// 财付通商户号
	public static String PARTNER_KEY = "";// 商户号对应的密钥
	public static String TOKENURL = "";// 获取access_token对应的url
	public static String GRANT_TYPE = "";// 常量固定值
	public static String EXPIRE_ERRCODE = "";// access_token失效后请求返回的errcode
	public static String FAIL_ERRCODE = "";// 重复获取导致上一次获取的access_token失效,返回错误码
	public static String GATEURL = "";// 获取预支付id的接口url
	public static String ACCESS_TOKEN = "";// access_token常量值
	public static String ERRORCODE = "";// 用来判断access_token是否失效的值
	public static String SIGN_METHOD = "";// 签名算法常量值

	public static String INPUT_CHARSET = "UTF-8"; // 字符集
	// modify
	public static String  NOTIFY_URL = (String) GameContext.getPayConfig("wx_notifyUrl");
	
	// package常量值
	public static String packageValue = "Sign=WXPay";
	public static String traceid = "testtraceid001";// 测试用户id
}
