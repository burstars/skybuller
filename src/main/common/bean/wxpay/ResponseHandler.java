package com.chess.common.bean.wxpay;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.dom4j.io.SAXReader;
import org.jacorb.idl.parser;
import org.jdom.*; 
import org.jdom.input.*;
import org.xml.sax.InputSource;

import com.chess.common.bean.wxpay.MD5Util;
import com.chess.common.bean.wxpay.TenpayUtil;
import com.thoughtworks.xstream.XStream;


/**
 * 应答处理类
 * 应答处理类继承此类，重写isTenpaySign方法即可。
 * @author miklchen
 *
 */
public class ResponseHandler { 
	
	private static Logger logger = LoggerFactory.getLogger(ResponseHandler.class);
	/** 密钥 */
	private String key;
	
	/** 应答的参数 */
	private SortedMap parameters; 
	
	/** debug信息 */
	private String debugInfo;
	
	private HttpServletRequest request;
	
	private HttpServletResponse response;
	
	private String uriEncoding;
	
	public static final byte[] readBytes(InputStream is, int contentLen) {
         if (contentLen > 0) {
                 int readLen = 0;
                 int readLengthThisTime = 0;
                 byte[] message = new byte[contentLen];
                 try {
                         while (readLen != contentLen) {
                                 readLengthThisTime = is.read(message, readLen, contentLen
                                                 - readLen);
                                 if (readLengthThisTime == -1) {// Should not happen.
                                         break;
                                 }
                                 readLen += readLengthThisTime;
                         }
                         return message;
                 } catch (IOException e) {
                         // Ignore
                         // e.printStackTrace();
                 }
         }
         return new byte[] {};
 }
	
	/**
	 * 构造函数
	 * 
	 * @param request
	 * @param response
	 */
	public ResponseHandler(HttpServletRequest request,
			HttpServletResponse response)  {	

		this.request = request;
		this.response = response;			

		this.key = "";
		this.parameters = new TreeMap();
		this.debugInfo = "";
		
		this.uriEncoding = "";
		try {
			this.request.setCharacterEncoding("UTF-8");
	        int size = request.getContentLength();
	        System.out.println(size);
	
	        InputStream is = request.getInputStream();
	
	        byte[] reqBodyBytes = readBytes(is, size);

	        String res = new String(reqBodyBytes);
	
	        System.out.println(res);
	       
			//SAXBuilder saxBuilder = new SAXBuilder(false);
			
		//	InputStream in = this.request.getInputStream();
		
			log("ssssssssssssss"+res);
			// by 
			SAXBuilder builder = new SAXBuilder();
			 Document doc = (Document) builder.build(new ByteArrayInputStream(res.getBytes()));
			 Element rootElement = doc.getRootElement();//根节点 
			
			//InputSource inputSource = new InputSource(in);
			//Document doc = saxBuilder.build(inputSource);
			//Element rootElement = doc.getRootElement();//根节点
			List<Element> list = rootElement.getChildren();
			if(null != list && 0 != list.size()){  
		        for(Element e:list){  
		        	this.setParameter(e.getName(), e.getText()); 
		        }
			}
			
			
		
		}catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage()); 
        }
		


		Map m = this.request.getParameterMap();
		Iterator it = m.keySet().iterator();
		String logData ="";
		while (it.hasNext()) {
			String k = (String) it.next();
			String v = ((String[]) m.get(k))[0];	
			logData += k.trim()+"="+v+"&";
			this.setParameter(k, v);
		}
		
		log(logData);
	}
	
	private void log(String log){
		logger.info("微信回调数据:"+log);
	}
	
	/**
	*获取密钥
	*/
	public String getKey() {
		return key;
	}

	/**
	*设置密钥
	*/
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * 获取参数值
	 * @param parameter 参数名称
	 * @return String 
	 */
	public String getParameter(String parameter) {
		String s = (String)this.parameters.get(parameter); 
		return (null == s) ? "" : s;
	}
	
	/**
	 * 设置参数值
	 * @param parameter 参数名称
	 * @param parameterValue 参数值
	 */
	public void setParameter(String parameter, String parameterValue) {
		String v = "";
		if(null != parameterValue) {
			v = parameterValue.trim();
		}
		this.parameters.put(parameter, v);
	}
	
	/**
	 * 返回所有的参数
	 * @return SortedMap
	 */
	public SortedMap getAllParameters() {
		return this.parameters;
	}
	
	/**
	 * 是否财付通签名,规则是:按参数名称a-z排序,遇到空值的参数不参加签名。
	 * @return boolean
	 */
	public boolean isTenpaySign() {
		StringBuffer sb = new StringBuffer();
		Set es = this.parameters.entrySet();
		Iterator it = es.iterator();
		while(it.hasNext()) {
			Map.Entry entry = (Map.Entry)it.next();
			String k = (String)entry.getKey();
			String v = (String)entry.getValue();
			
			logger.debug("key:"+k+" v:" + v+"\n");//System.out.print("key:"+k+" v:" + v+"\n");
			if(!"sign".equals(k) && null != v && !"".equals(v)) {
				sb.append(k + "=" + v + "&");
			}
		}
		
		sb.append("key=" + this.getKey());
		
		//算出摘要
		String enc = TenpayUtil.getCharacterEncoding(this.request, this.response);
		String sbb = sb.toString();
		String sign = MD5Util.MD5Encode(sb.toString(), enc).toLowerCase();
		
		String tenpaySign = this.getParameter("sign").toLowerCase();
		
		//debug信息
		//System.out.print(sb.toString() + " => sign:" + sign + " tenpaySign:" + tenpaySign);
		
		return tenpaySign.equals(sign);
	}
	
	/**
	 * 返回处理结果给财付通服务器。
	 * @param msg: Success or fail。
	 * @throws IOException 
	 */
	public void sendToCFT(String msg) throws IOException {
		String strHtml = msg;
		PrintWriter out = this.getHttpServletResponse().getWriter();
		out.println(strHtml);
		out.flush();
		out.close();

	}
	
	/**
	 * 获取uri编码
	 * @return String
	 */
	public String getUriEncoding() {
		return uriEncoding;
	}

	/**
	 * 设置uri编码
	 * @param uriEncoding
	 * @throws UnsupportedEncodingException
	 */
	public void setUriEncoding(String uriEncoding)
			throws UnsupportedEncodingException {
		if (!"".equals(uriEncoding.trim())) {
			this.uriEncoding = uriEncoding;

			// 编码转换
			String enc = TenpayUtil.getCharacterEncoding(request, response);
			Iterator it = this.parameters.keySet().iterator();
			while (it.hasNext()) {
				String k = (String) it.next();
				String v = this.getParameter(k);
				v = new String(v.getBytes(uriEncoding.trim()), enc);
				this.setParameter(k, v);
			}
		}
	}

	/**
	*获取debug信息
	*/
	public String getDebugInfo() {
		return debugInfo;
	}
	
	/**
	*设置debug信息
	*/
	protected void setDebugInfo(String debugInfo) {
		this.debugInfo = debugInfo;
	}
	
	protected HttpServletRequest getHttpServletRequest() {
		return this.request;
	}
	
	protected HttpServletResponse getHttpServletResponse() {
		return this.response;
	}
	
}
	
