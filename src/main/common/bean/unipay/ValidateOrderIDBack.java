package com.chess.common.bean.unipay;

import java.util.HashMap;
import java.util.Map;

import com.chess.common.JSONService;



/** unipay 订单验证返回*/
public class ValidateOrderIDBack {
	//0-验证成功 1-验证失败，必填
	private  String checkOrderIdRsp = "1";
	//应用名称，必填，长度<=64
	private  String appname ="幸运德州" ;
	//计费点名称，必填，长度<=64  1元头像包等
	private String feeename ;
	 //计费点金额（分），必填，长度<=5
	private String payfee ;
	//应用开发商名称，必填，长度<=64
	private String appdeveloper ="深圳市麟云科技有限公司" ;
	//游戏账号，长度<=64，联网支付必填
	private String gameaccount ;
	//MAC地址去掉冒号，联网支付必填，
	private String macaddress;
	  //沃商店应用id，必填
	private String appid ="905608778920131226103520737000";
	 //IP地址，去掉点号，补零到每地址段3位  如：192168000001，联网必填，单机尽量
	private String ipaddress;
	//沃商店计费点，必填
	private String serviceid;
	 //渠道ID，必填
	private String channelid ="00012243";
	//沃商店CPID，必填
	private String cpid  ="86006185";
	//订单时间戳，14位时间格式，联网必填， 单机尽量
	private String ordertime ;
	 //设备标识，联网必填，单机尽量上报
	private String imei ;
	//应用版本号，必填，长度<=32
	private String appversion;
	
	public void init(String gameaccount,String macaddress,String ipaddress,String channelid,String ordertime,String imei,String appversion){
		this.gameaccount = gameaccount;
		this.macaddress = macaddress;
		this.ipaddress = ipaddress;
		this.channelid = channelid;
		this.ordertime = ordertime;
		this.imei = imei;
		this.appversion = appversion;
	}
	
	public String encodeXmlStr(){
		Map<String,Class> map = new HashMap<String,Class>();
		map.put("paymessages", ValidateOrderIDBack.class);
		String backStr = JSONService.ObjectToXmlStr(this, map);
		return backStr;
	}
	
	public String getCheckOrderIdRsp() {
		return checkOrderIdRsp;
	}
	public void setCheckOrderIdRsp(String checkOrderIdRsp) {
		this.checkOrderIdRsp = checkOrderIdRsp;
	}
	public String getAppname() {
		return appname;
	}
	public void setAppname(String appname) {
		this.appname = appname;
	}
	public String getFeeename() {
		return feeename;
	}
	public void setFeeename(String feeename) {
		this.feeename = feeename;
	}
	public String getPayfee() {
		return payfee;
	}
	public void setPayfee(String payfee) {
		this.payfee = payfee;
	}
	public String getAppdeveloper() {
		return appdeveloper;
	}
	public void setAppdeveloper(String appdeveloper) {
		this.appdeveloper = appdeveloper;
	}
	public String getGameaccount() {
		return gameaccount;
	}
	public void setGameaccount(String gameaccount) {
		this.gameaccount = gameaccount;
	}
	public String getMacaddress() {
		return macaddress;
	}
	public void setMacaddress(String macaddress) {
		this.macaddress = macaddress;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	public String getServiceid() {
		return serviceid;
	}
	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}
	public String getChannelid() {
		return channelid;
	}
	public void setChannelid(String channelid) {
		this.channelid = channelid;
	}
	public String getCpid() {
		return cpid;
	}
	public void setCpid(String cpid) {
		this.cpid = cpid;
	}
	public String getOrdertime() {
		return ordertime;
	}
	public void setOrdertime(String ordertime) {
		this.ordertime = ordertime;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getAppversion() {
		return appversion;
	}
	public void setAppversion(String appversion) {
		this.appversion = appversion;
	}
	
	
	
	
}
