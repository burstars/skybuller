package com.chess.common.bean.unipay;

import java.util.Map;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import com.chess.common.MD5Service;
import com.chess.common.bean.alipay.util.Result;


/**联通充值回调基础类*/
public class PayCallbackReq {
	private String orderid;
	private String ordertime;
	private String cpid;
	//private String appid="905608778920131226103520737000";
	private String appid="";
	private String fid;
	private String consumeCode;
	private String payfee ="100";
	private String payType = "0";
	private String hRet ="1";
	private String status = "00001";
	private String signMsg ;
	
	private String Key = "9813b270ed0288e7c038";
	private static Logger logger = LoggerFactory.getLogger(PayCallbackReq.class);
	private boolean isChecked = false;
	
	/**获得加密验证串*/
	private String getSign(){
		String sign = "orderid="+this.orderid+"&ordertime="+this.ordertime+"&cpid="+this.cpid+"&appid="+this.appid+
		"&fid="+this.fid+"&consumeCode="+this.consumeCode+"&payfee="+this.payfee+"&payType="+this.payType+"&hRet="+this.hRet+"&status="+this.status+"&Key="+this.Key;
		logger.debug("联通加密前的串："+sign);//System.out.println("联通加密前的串："+sign);
		sign = MD5Service.encryptStringLower(sign) ;
		logger.debug("联通加密获得串："+sign);//System.out.println("联通加密获得串："+sign);
		return sign;
	}

	public void init(Map<String,String> eleMap){
		if(eleMap!=null){
			if(eleMap.get("orderid")!=null){
				this.orderid = eleMap.get("orderid");
			}
			if(eleMap.get("ordertime")!=null){
				this.ordertime = eleMap.get("ordertime");
			}
			if(eleMap.get("cpid")!=null){
				this.cpid = eleMap.get("cpid");
			}
			if(eleMap.get("appid")!=null&&!"".equals(eleMap.get("appid"))){
				this.appid = eleMap.get("appid");
			}
			if(eleMap.get("fid")!=null){
				this.fid = eleMap.get("fid");
			}
			if(eleMap.get("consumeCode")!=null){
				this.consumeCode = eleMap.get("consumeCode");
			}
			if(eleMap.get("payfee")!=null){
				this.payfee = eleMap.get("payfee");
			}
			if(eleMap.get("payType")!=null){
				this.payType = eleMap.get("payType");
			}
			if(eleMap.get("hRet")!=null){
				this.hRet = eleMap.get("hRet");
			}
			if(eleMap.get("status")!=null){
				this.status = eleMap.get("status");
			}
			if(eleMap.get("signMsg")!=null){
				this.signMsg = eleMap.get("signMsg");
			}
			if(this.signMsg!=null){
				String MD5Sign = this.getSign();
				if(MD5Sign.equals(this.signMsg)){
					this.isChecked = true;
				}
			}
		}
	}
	public String getOrderid() {
		return orderid;
	}


	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}


	public String getOrdertime() {
		return ordertime;
	}


	public void setOrdertime(String ordertime) {
		this.ordertime = ordertime;
	}


	public String getCpid() {
		return cpid;
	}


	public void setCpid(String cpid) {
		this.cpid = cpid;
	}


	public String getAppid() {
		return appid;
	}


	public void setAppid(String appid) {
		this.appid = appid;
	}


	public String getFid() {
		return fid;
	}


	public void setFid(String fid) {
		this.fid = fid;
	}


	public String getConsumeCode() {
		return consumeCode;
	}


	public void setConsumeCode(String consumeCode) {
		this.consumeCode = consumeCode;
	}




	public String getSignMsg() {
		return signMsg;
	}


	public void setSignMsg(String signMsg) {
		this.signMsg = signMsg;
	}


	public boolean isChecked() {
		return isChecked;
	}

	public String getPayfee() {
		return payfee;
	}

	public void setPayfee(String payfee) {
		this.payfee = payfee;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getHRet() {
		return hRet;
	}

	public void setHRet(String ret) {
		hRet = ret;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getKey() {
		return Key;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
}
