package com.chess.common.bean;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class SystemNotice extends NetObject {

	private int notice_id = 0;
	
	private String notice_title = "";
	
	private String notice_content = "";
	
	private int is_exit = 0;
	
	//0=��Ϣ��1= �̶�����ƣ�5=���ֲ���Ϣ
	private int notice_type  = 0 ;
	
	//��Թ̶�����Ƶ��Ƿ�������0= δ������1= ����
	private int is_open = 0 ;
	
	@Override
	public void serialize(ObjSerializer ar) {
		
		notice_id = ar.sInt(notice_id);
		notice_title = ar.sString(notice_title);
		notice_content = ar.sString(notice_content);
		notice_type = ar.sInt(notice_type);
		is_open = ar.sInt(is_open);
	}
	

	public String getNotice_title() {
		return notice_title;
	}

	public void setNotice_title(String notice_title) {
		this.notice_title = notice_title;
	}

	public String getNotice_content() {
		return notice_content;
	}

	public void setNotice_content(String notice_content) {
		this.notice_content = notice_content;
	}


	public int getNotice_id() {
		return notice_id;
	}


	public void setNotice_id(int notice_id) {
		this.notice_id = notice_id;
	}


	public int getIs_exit() {
		return is_exit;
	}


	public void setIs_exit(int is_exit) {
		this.is_exit = is_exit;
	}
	


	public int getNotice_type() {
		return notice_type;
	}


	public void setNotice_type(int noticeType) {
		notice_type = noticeType;
	}


	public int getIs_open() {
		return is_open;
	}


	public void setIs_open(int isOpen) {
		is_open = isOpen;
	}


}
