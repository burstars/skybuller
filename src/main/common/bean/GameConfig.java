package com.chess.common.bean;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.xwork.StringUtils;

import com.chess.common.constant.GameConstant;

// 游戏玩法类
public class GameConfig {
	private int id;
	private String name;
	private String game_id;
	private int player_count;
	private String vip_wanfa;
	private String club_wanfa;
	private int opt_time1;
	private int opt_time2;
	private int opt_time3;
	private int opt_time4;
	private int opt_time5;
	private int opt_time6;
	private int opt_time7;
	private int opt_time8;
	private int opt_time9;
	private int opt_time10;
	private String descript;
	private String param01;
	private String param02;
	private String param03;
	private String param04;
	private String param05;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGame_id() {
		return game_id;
	}

	public void setGame_id(String game_id) {
		this.game_id = game_id;
	}

	public String getVip_wanfa() {
		return vip_wanfa;
	}

	public void setVip_wanfa(String vip_wanfa) {
		this.vip_wanfa = vip_wanfa;
	}

	public int getOpt_time1() {
		return opt_time1;
	}

	public void setOpt_time1(int opt_time1) {
		this.opt_time1 = opt_time1;
	}

	public int getOpt_time2() {
		return opt_time2;
	}

	public void setOpt_time2(int opt_time2) {
		this.opt_time2 = opt_time2;
	}

	public int getOpt_time3() {
		return opt_time3;
	}

	public void setOpt_time3(int opt_time3) {
		this.opt_time3 = opt_time3;
	}

	public int getOpt_time4() {
		return opt_time4;
	}

	public void setOpt_time4(int opt_time4) {
		this.opt_time4 = opt_time4;
	}

	public int getOpt_time5() {
		return opt_time5;
	}

	public void setOpt_time5(int opt_time5) {
		this.opt_time5 = opt_time5;
	}

	public int getOpt_time6() {
		return opt_time6;
	}

	public void setOpt_time6(int opt_time6) {
		this.opt_time6 = opt_time6;
	}

	public int getOpt_time7() {
		return opt_time7;
	}

	public void setOpt_time7(int opt_time7) {
		this.opt_time7 = opt_time7;
	}

	public int getOpt_time8() {
		return opt_time8;
	}

	public void setOpt_time8(int opt_time8) {
		this.opt_time8 = opt_time8;
	}

	public int getOpt_time9() {
		return opt_time9;
	}

	public void setOpt_time9(int opt_time9) {
		this.opt_time9 = opt_time9;
	}

	public int getOpt_time10() {
		return opt_time10;
	}

	public void setOpt_time10(int opt_time10) {
		this.opt_time10 = opt_time10;
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}

	public String getParam01() {
		return param01;
	}

	public void setParam01(String param01) {
		this.param01 = param01;
	}

	public String getParam02() {
		return param02;
	}

	public void setParam02(String param02) {
		this.param02 = param02;
	}

	public String getParam03() {
		return param03;
	}

	public void setParam03(String param03) {
		this.param03 = param03;
	}

	public String getParam04() {
		return param04;
	}

	public void setParam04(String param04) {
		this.param04 = param04;
	}

	public String getParam05() {
		return param05;
	}

	public void setParam05(String param05) {
		this.param05 = param05;
	}

	public int getPlayer_count() {
		return player_count;
	}

	public void setPlayer_count(int player_count) {
		this.player_count = player_count;
	}

	/**
	 * 获取vip房间创建信息 说明:1@12,1#24,2;0@2,1#4,2
	 * 2人按局走@12局,扣1张卡#24局,扣2张卡#4人按圈走@2圈,扣1张卡#4圈,扣2张卡
	 * 
	 * @author 2017-3-15
	 * @param roomType
	 * @param index
	 * @return
	 */
	public Map<String, Integer> getVipInfo(int roomType) {
		Map<String, Integer> param = new HashMap<String, Integer>();
		String selString = "";
		String[] aryWf = this.vip_wanfa.split(";");
		if (roomType == GameConstant.ROOM_TYPE_COUPLE) {
			selString = aryWf[0];
		} else if (roomType == GameConstant.ROOM_TYPE_VIP) {
			selString = aryWf[1];
		} else {
			selString = aryWf[2];
		}
		String[] aryBase = selString.split("@");
		param.put("unit", Integer.parseInt(aryBase[0]));// 圈or局
		String[] aryinfo = aryBase[1].split("#");

		param.put("before_key", Integer.parseInt(aryinfo[0].split(",")[0]));
		String beforeKa = aryinfo[0].split(",")[1];
		String[] aryBeforeKa = beforeKa.split("a");
		param.put("before_ka_0", Integer.parseInt(aryBeforeKa[0]));
		param.put("before_ka_1", Integer.parseInt(aryBeforeKa[1]));
		param.put("before_ka_2", Integer.parseInt(aryBeforeKa[2]));

		param.put("after_key", Integer.parseInt(aryinfo[1].split(",")[0]));
		String afterKa = aryinfo[1].split(",")[1];
		String[] aryAfterKa = afterKa.split("a");
		param.put("after_ka_0", Integer.parseInt(aryAfterKa[0]));
		param.put("after_ka_1", Integer.parseInt(aryAfterKa[1]));
		param.put("after_ka_2", Integer.parseInt(aryAfterKa[2]));

		return param;
	}

	/**
	 * 获取扑克开房所需福豆或卡个数，支持两种配置：
	 * 
	 * 12@0&2:2:2-4:4:4-8:8:8-16:16:16;30@6&4:4:4-8:8:8-16:16:16-32:32:32;60@12&
	 * 8:8:8-16:16:16-32:32:32-64:64:64
	 * 局数@赠送局数&难度1单人扣卡:难度1代开扣卡:难度1AA扣卡-难度2单人扣卡:难度2代开扣卡:难度2AA扣卡-... 以此类推
	 * 
	 * 8@1:1:1;16@2:2:2;24@3:3:3 局数@单人扣卡:代开扣卡:AA扣卡;局数@... 以此类推
	 * 
	 * @param key
	 * @return
	 */
	public Map<String, Integer> getPokerWanfa(String wanfa, int key) {
		Map<String, Integer> param = new HashMap<String, Integer>();
		String[] aryWf = wanfa.split(";");
		for (int i = 0; i < aryWf.length; i++) {
			String[] wf = aryWf[i].split("@");
			if (key == Integer.parseInt(wf[0])) {
				String str = wf[1];
				// 有赠送局数
				if (StringUtils.contains(str, "&")) {
					String[] aryZs = str.split("&");
					// 赠送局数
					int zs = Integer.parseInt(aryZs[0]);

					// 难度
					String strKa = aryZs[1];

					if (StringUtils.contains(strKa, "-")) {
						// 有场次难度
						String[] aryLvl = strKa.split("-");
						for (int j = 0; j < aryLvl.length; j++) {
							String strLvl = aryLvl[j];
							String[] aryKa = strLvl.split(":");
							int lvl = j + 1;
							param.put("cost_self_" + lvl,
									Integer.parseInt(aryKa[0]));
							param.put("cost_proxy_" + lvl,
									Integer.parseInt(aryKa[1]));
							param.put("cost_aa_" + lvl,
									Integer.parseInt(aryKa[2]));
						}
					} else {
						String[] aryKa = strKa.split(":");
						param.put("cost_self", Integer.parseInt(aryKa[0]));
						param.put("cost_proxy", Integer.parseInt(aryKa[1]));
						param.put("cost_aa", Integer.parseInt(aryKa[2]));
					}
				} else {
					String[] aryKa = str.split(":");
					param.put("cost_self", Integer.parseInt(aryKa[0]));
					param.put("cost_proxy", Integer.parseInt(aryKa[1]));
					param.put("cost_aa", Integer.parseInt(aryKa[2]));
				}
				break;
			}
		}
		return param;
	}

	/**
	 * 获取扑克开房所需福豆或卡个数，支持两种配置：
	 * 
	 * 12@0&2:2:2-4:4:4-8:8:8-16:16:16;30@6&4:4:4-8:8:8-16:16:16-32:32:32;60@12&
	 * 8:8:8-16:16:16-32:32:32-64:64:64
	 * 局数@赠送局数&难度1单人扣卡:难度1代开扣卡:难度1AA扣卡-难度2单人扣卡:难度2代开扣卡:难度2AA扣卡-... 以此类推
	 * 
	 * 8@1:1:1;16@2:2:2;24@3:3:3 局数@单人扣卡:代开扣卡:AA扣卡;局数@... 以此类推
	 * 
	 * @param key
	 * @return
	 */
	public Map<String, Integer> getPokerClubWanfa(int key) {
		return this.getPokerWanfa(this.club_wanfa, key);
	}

	/**
	 * 获取扑克开房所需福豆或卡个数，支持两种配置：
	 * 
	 * 12@0&2:2:2-4:4:4-8:8:8-16:16:16;30@6&4:4:4-8:8:8-16:16:16-32:32:32;60@12&
	 * 8:8:8-16:16:16-32:32:32-64:64:64
	 * 局数@赠送局数&难度1单人扣卡:难度1代开扣卡:难度1AA扣卡-难度2单人扣卡:难度2代开扣卡:难度2AA扣卡-... 以此类推
	 * 
	 * 8@1:1:1;16@2:2:2;24@3:3:3 局数@单人扣卡:代开扣卡:AA扣卡;局数@... 以此类推
	 * 
	 * @param key
	 * @return
	 */
	public Map<String, Integer> getPokerVipWanfa(int key) {
		return this.getPokerWanfa(this.vip_wanfa, key);
	}

	public String getClub_wanfa() {
		return club_wanfa;
	}

	public void setClub_wanfa(String club_wanfa) {
		this.club_wanfa = club_wanfa;
	}

}
