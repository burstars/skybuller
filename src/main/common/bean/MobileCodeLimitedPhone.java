/**
 * Description:手机注册信息管理
 */
package com.chess.common.bean;

import java.util.Date;

public class MobileCodeLimitedPhone {

	/** 待获取验证码的手机号 */
	private String phoneNO;

	/** 请求验证码的IP地址 */
	private String ip;

	/** 上次获取验证码的时间 */
	private Date lastRegTime;

	/** 当天相同手机号，相同IP的次数 */
	private int phoneAndIpSameTimes;

	/** 当天手机号可接收验证码次数 */
	private int limtedPhoneTime;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getLastRegTime() {
		return lastRegTime;
	}

	public void setLastRegTime(Date lastRegTime) {
		this.lastRegTime = lastRegTime;
	}

	public String getPhoneNO() {
		return phoneNO;
	}

	public void setPhoneNO(String phoneNO) {
		this.phoneNO = phoneNO;
	}

	public int getPhoneAndIpSameTimes() {
		return phoneAndIpSameTimes;
	}

	public void setPhoneAndIpSameTimes(int phoneAndIpSameTimes) {
		this.phoneAndIpSameTimes = phoneAndIpSameTimes;
	}

	public int getLimtedPhoneTime() {
		return limtedPhoneTime;
	}

	public void setLimtedPhoneTime(int limtedPhoneTime) {
		this.limtedPhoneTime = limtedPhoneTime;
	}

}
