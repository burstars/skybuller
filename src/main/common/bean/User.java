package com.chess.common.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.constant.GameConstant;
import com.chess.common.constant.PayConstant;
import com.chess.common.service.ICacheGameConfigService;
import com.chess.core.constant.NetConstant;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

import edu.emory.mathcs.backport.java.util.Collections;

public class User extends NetObject {

	private static Logger logger = LoggerFactory.getLogger(User.class);
	/**
	 * 玩家ID
	 */
	private String playerID = "";
	/**
	 * 玩家索引号，类似于QQ号，系统唯一 1000 是默认值，如果玩家的值为1000的话，则表示他是老玩家需重新分配号码
	 */
	private Integer playerIndex = 1000;

	/**
	 * qq标识
	 */
	private String qqOpenID = "";

	/**
	 * 微信标识
	 */
	private String wxOpenID = "";

	/**
	 * 玩家账户
	 */
	private String account = "";

	/**
	 * 玩家密码
	 */
	private String password = "";

	/**
	 * 玩家游戏中昵称*
	 */
	private String playerName = "";

	/**
	 * 匿名登录的机器码*
	 */
	private String machineCode = "";

	/**
	 * 头像索引*
	 */
	private int headImg = 0;
	/**
	 * 玩家货币0 金币
	 */
	private int gold = 0;

	private String goldVerify = "";

	/**
	 * 手机号码
	 */
	private String phoneNumber = "";

	/**
	 * 钻石
	 */
	private int diamond = 0;
	/**
	 * 积分
	 */
	private int score = 0;
	/**
	 * 胜利记录
	 */
	private int wons = 0;
	/**
	 * 失败记录
	 */
	private int loses = 0;
	/**
	 * 逃跑记录
	 */
	private int escape = 0;

	private int life = 0;// 生命值，玩一次掉1个，10分钟加一个，自动加满最多5个，可购买
	/**
	 * 注册时间
	 */
	private Date regisTime = null;
	/**
	 * 最后登录时间
	 */
	private Date lastLoginTime = null;
	/**
	 * 连续登陆天数
	 */
	private int continueLanding = 0;
	/**
	 * 每日赠送抽奖的次数
	 */
	private int luckyDrawsTimes = 0;
	/**
	 * 历史最高分数
	 */
	private int hisBestScore = 0;
	/**
	 * 今天最高分数
	 */
	private int todayBestScore = 0;
	/**
	 * 历史最高分数
	 */
	private int diamondTotalCost = 0;
	/**
	 * vip等级
	 */
	private int vipLevel = 0;
	/**
	 * vip经验
	 */
	private int vipExp = 0;

	/**
	 * 晶石数量*
	 */
	private int gemNum = 0;
	// 下线时间
	private Date offlineTime = null;

	private boolean isRobot = false;

	// 机器人级别（0托管，1普通）
	private int robotlevel = 0;
	// 机器人玩随机在一个房间玩的局数
	private int robotgamenum = 0;
	//
	private String clientIP = "";// 玩家的ip
	// 玩家的端口号
	private String httpClientPort = "";
	//
	private int roomID = NetConstant.INVALID_ID;// 如果玩家在房间中，这个id不是-1
	private int tablePos = 0;// 坐在桌子的那个位置

	// 性别(0：女；1男；2：人妖)
	private int sex = 0;

	// session
	private IoSession session = null;
	//
	private int firstLogin = 0;
	//
	// 当前游戏局的数据
	private int gameScore = 0;// 当前游戏局的得分

	private int taskScore = 0;// 进阶场任务完成后的分数
	private String tableID = "";
	private boolean playingSingleTable = false;// 是否在玩单桌
	private int gameState = GameConstant.STATE_INVALID;
	private long robotUseItemTime = 0L;// 机器人使用道具的时间，防止使用太频繁

	// 我的被攻击记录
	private List<AttackInfo> attackInfos = new ArrayList<AttackInfo>();

	// 玩家的道具
	private List<UserBackpack> items = new ArrayList<UserBackpack>();

	// 玩家是否在线
	private boolean isOnline = true;

	private int platformType = PayConstant.HISTORY_PLATFORM_TYPE_E_SURFING;

	private List<Integer> bk_cardsDown = new ArrayList<Integer>();
	private List<Byte> bk_cardsInHand = new ArrayList<Byte>();

	// 吃碰杠的牌
	private List<Integer> cardsDown = new ArrayList<Integer>();
	// 手里的牌,从小到大排序
	private List<Byte> cardsInHand = new ArrayList<Byte>();
	// 玩家门前的牌
	private List<Byte> cardsBefore = new ArrayList<Byte>();
	// 玩家出过的牌
	private List<Byte> cardsOut = new ArrayList<Byte>();
	// 玩家听以后出的牌
	private List<Byte> cardsOutAfterTing = new ArrayList<Byte>();
	// 刚抓起来的牌
	private Byte cardGrab = 0;

	// 如果玩家在vip房间，就负值这个id
	private String vipTableID = "";

	// vip房间log记录
	private VipRoomRecord vrr = null;
	// vip房间log记录
	private RecordBean recordBean = null;
	/**vip房间log记录 cc modify 2017-9-26*/
	private List<ProxyVipRoomRecord> pvrrs = new ArrayList<ProxyVipRoomRecord>();

	// 在房间在等待其他玩家的时间
	private long inRoomWaitOtherTime = 0;

	/**
	 * 是否可加为好友(0可以，1不可以)
	 */
	private int canFriend = 0;
	private int saveTime = 0;// 今日输光救济次数，每日0点调度清0

	// 是否托管状态(1托管状态)
	private int autoOperation = 0;

	private int vipTableGold = 0;// vip桌，玩家当前在桌子上有多少金币

	/**
	 * 关于宝箱
	 **/
	// 今天对战局数
	private int td_Zhan_count = 0;
	// 宝箱按钮数
	private int bx_hand_count = 0;
	// 前途可以选择宝箱的数
	private int extra_box_count = 0;

	// 是否在桌上
	private boolean isOnTable = false;

	/**
	 * 玩家登录设备【1-IOS】【2-Android】
	 */
	private int deviceFlag = 0;

	// 超时出牌状态
	private int overTimeState = 0;

	// 测试字段
	private int nextCard = 0;

	// 断线重连异常，需要恢复桌子上的游戏数据
	private boolean needCopyGameState = false;

	// 玩家类别，用于后台用户管理
	private int playerType = 0;

	// 玩家位置信息
	private String location = "";

	// 身份证
	private String identify_card = "";

	private boolean isTwoVipOnline = false;
	// 上次请求加好友的时间
	public long prev_add_friend_time = 0;

	// 进入游戏时显示绑定手机号
	public boolean show_bind_phone = true;

	// 发好牌的局数
	public List<Integer> good_cards_idx_list = new ArrayList<Integer>();
	public int good_cards_left_count = 0;

	public boolean show_bind_phone_onenter_room = true;

	// 参加的游戏局数(一张房卡为一局)
	public int game_total_count = 0;

	public String unionID = "";
	/** 处理微信分享送卡的标识  20161203 */
	private String param01;
	/** 是否是后面摸牌  20161215 */
	// private boolean isLeftMo = false;
	/** 玩家开局准备状态  20170208 */
	public int readyFlag = 0;

	// 是否操作了续卡
	private boolean chooseExtendCard = false;
	// 是否同意续卡
	private boolean agreeExtendCard = false;
	// 是否同意解散 ---0：等待选择， 1：同意解散 ，2：不同意解散
	private int agreeCloseRoom = 0;
	// 解散房间的发起人
	private boolean isApplyCloseRoom = false;

	public List<Byte> duizhiCards = new ArrayList<Byte>();

	private boolean isXuanPiao = false;
	private boolean isTipPiao = false;

	private boolean isTingFeng = false;// 是否听东南西北风（不打牌-岫岩麻将）
	// 记录跟听事件，已上听的人
	private List<Integer> tingPos = new ArrayList<Integer>();
	// 取消换宝
	private List<Byte> cancelChangeBao = new ArrayList<Byte>();
	// 取消胡
	private byte cancelHuCard = 0;
	//是否点击继续游戏
	private boolean bAgreeContinue = false;
	/***
	 * 客户端手机设备的相关信息
	 */
	private String deviceBrand = ""; // 手机厂商
	private String systemVersion = ""; // -- 系统版本
	private String phone = ""; // 客户端手机号码
	private String proxyIp = ""; // 代理服ip
	
	private int proxyOpenRoom = 0;// 代开房间权限
	
	private int credits = 0;//信用积分
	private int totalCredits = 0 ;//总信用积分

	//玩家头像网络地址
	private String HEADIMGURL = "";
	
	/**俱乐部参与权限： 0=不能加入；1=可以加入；2=可以创建*/
	private int clubType = 1;

	//ddz
	// 这一锅里面当前的输赢总和
	private int winLoseTotal = 0;
	private int winLoseGoldNum = 0;// 正数
	// 是不是地主
	private boolean isLord = false;
	// 加倍
	private int doubleResult = 0;
	// 是否叫过分
	private boolean isCallScore = false;
	private int fanType = 0;// 当前这把牌的输赢牌型，庄，门清，放炮
	// 是否叫过加倍
	private boolean isCallDouble = false;
	// 轮到他操作的开始时间
	private long opStartTime = 0L;
	
	// 出牌次数
	private int outCardTime = 0;
	
	private int fanNum = 0;// 几番
	
	private int zhuangCount = 0;
	private int winCount = 0;
	private int dianpaoCount = 0;
	private int mobaoCount = 0;
	private int baozhongbaoCount = 0;
	// 叫分
	private int callScorePoint = 0;
	/**玩家当前所在游戏gameId*/
	private String gameId = "";
	/** 玩家是否赢了 1=赢了，0=输了 */
	private int winState = 0;
	/** 玩家总分 */
	private int winLoseTotalCoinNum = 0;
	/** 玩家单局得分 */
	private int winLoseCurrCoinNum = 0;
	/** 特殊得分 */
	private int specialCoinNum = 0;
	
	/**电子庄牛牛userGameHander*/
	private com.chess.nndzz.bean.UserGameHander userGameHanderForNNDZZ;
	
	/** 上级推荐人编号 */
	private String mgrIndex = "";
	
	@Override
	public void serialize(ObjSerializer ar) {
		playerID = ar.sString(playerID);

		account = ar.sString(account);
		password = ar.sString(password);
		playerName = ar.sString(playerName);
		machineCode = ar.sString(machineCode);

		headImg = ar.sInt(headImg);
		//
		gold = ar.sInt(gold);
		goldVerify = ar.sString(goldVerify);
		sex = ar.sInt(sex);

		diamond = ar.sInt(diamond);
		score = ar.sInt(score);
		wons = ar.sInt(wons);
		loses = ar.sInt(loses);
		escape = ar.sInt(escape);
		life = ar.sInt(life);

		tablePos = ar.sInt(tablePos);
		//

		continueLanding = ar.sInt(continueLanding);
		luckyDrawsTimes = ar.sInt(luckyDrawsTimes);

		vipLevel = ar.sInt(vipLevel);
		vipExp = ar.sInt(vipExp);
		//
		gemNum = ar.sInt(gemNum);
		playerIndex = ar.sInt(playerIndex);
		//
		// 一般只写入
		if (ar.isReadMode() == false) {
			int item_num = items.size();
			ar.sInt(item_num);
			for (int i = 0; i < item_num; i++) {
				UserBackpack pi = items.get(i);
				//
				int item_base_id = pi.getItemBaseID();
				int item_numx = pi.getItemNum();
				//
				ar.sInt(item_base_id);
				ar.sInt(item_numx);
			}
		}
		//
		//
		cardsDown = (List<Integer>) ar.sIntArray(cardsDown);
		cardsInHand = (List<Byte>) ar.sByteArray(cardsInHand);
		cardGrab = ar.sByte(cardGrab);

		canFriend = ar.sInt(canFriend);
		phoneNumber = ar.sString(phoneNumber);
		param01 = ar.sString(param01);
		proxyOpenRoom = ar.sInt(proxyOpenRoom);
		
		HEADIMGURL =ar.sString(HEADIMGURL);
		location = ar.sString(location);
		credits = ar.sInt(credits);
		vipTableID =ar.sString(vipTableID);
		
		clubType = ar.sInt(clubType);
		mgrIndex = ar.sString(mgrIndex);
	}

	public List<Byte> convertArrayToList(byte[] array) {
		List<Byte> list = new ArrayList();
		for (int i = 0; i < array.length; i++) {
			list.add(array[i]);
		}
		return list;
	}

	// 查询手里的第几张牌
	public Byte findCardInHand(int value) {
		int idx = Collections.binarySearch(cardsInHand, (byte) value);
		if (idx < 0) {
			return 0;
		} else {
			return cardsInHand.get(idx);
		}
	}

	// 查询手里某张牌，有多少张
	public int getXCardNumInHand(int value) {
		value &= 0xFF;
		int num = 0;
		for (int i = 0; i < cardsInHand.size(); i++) {
			byte bb = cardsInHand.get(i);
			if ((bb & 0xFF) == value) {
				num++;
			}
		}
		//
		return num;
	}

	//
	public void addCardInHand(byte b) {
		if (b == 0) {
			// logger.error("addCardInHand=0");
			return;
		}

		// if (this.playerName.equals("111333")) {
		// System.out.println("------->给玩家【" + this.playerName + "】加手牌【" + b
		// + "】前 :" + cardsInHand);
		// }

		//
		boolean inserted = false;
		//
		for (int i = 0; i < cardsInHand.size(); i++) {
			byte bb = cardsInHand.get(i);
			if (b < bb) {
				cardsInHand.add(i, b);
				inserted = true;
				break;
			}
		}
		//
		if (inserted == false) {
			cardsInHand.add(b);
		}

		// if (this.playerName.equals("111333")) {
		// System.out.println("------->给玩家【" + this.playerName + "】加手牌 :" + b
		// + "			加完后手牌：" + cardsInHand);
		// }

	}

	// 从玩家手里取走一张牌
	public Byte removeCardInHand(int in_card) {
		byte value = (byte) (in_card & 0xff);
		if (value == 0)
			return 0;

		// if (this.playerName.equals("111333")) {
		// System.out.println("------->删除玩家【" + this.playerName + "】手牌【"
		// + in_card + "】前 :" + cardsInHand);
		// }

		Byte b = 0;

		for (int i = 0; i < cardsInHand.size(); i++) {
			byte bb = cardsInHand.get(i);
			if (bb == value) {
				b = bb;
				cardsInHand.remove(i);
				break;
			}
		}

		// if (this.playerName.equals("111333")) {
		// System.out.println("------->删除玩家【" + this.playerName + "】手牌 :"
		// + in_card + "			删完后手牌：" + cardsInHand);
		// }

		return b;
	}

	// 门前放两张牌（粘对）
	public int addCardDown(int c1, int c2) {
		int card = c2 << 8 | c1;

		cardsDown.add(card);
		return card;
	}

	// 在玩家门前放一张吃碰杠牌
	public int addCardDown(int c1, int c2, int c3, int type) {
		int card = 0;
		if (c3 >= c2 && c2 >= c1)// 321
		{
			card = (c3 << 16) | (c2 << 8) | c1;
		}
		if (c3 >= c1 && c1 >= c2)// 312
		{
			card = (c3 << 16) | (c1 << 8) | c2;
		} else if (c1 >= c2 && c2 >= c3)// 123
		{
			card = (c1 << 16) | (c2 << 8) | c3;
		} else if (c1 >= c3 && c3 >= c2)// 132
		{
			card = (c1 << 16) | (c3 << 8) | c2;
		} else if (c2 >= c3 && c3 >= c1)// 231
		{
			card = (c2 << 16) | (c3 << 8) | c1;
		} else if (c2 >= c1 && c1 >= c3)// 213
		{
			card = (c2 << 16) | (c1 << 8) | c3;
		}

		cardsDown.add(card);
		return card;
	}

	/**
	 * -->TODO 移除碰牌，杠牌用
	 * 
	 * @author cuiweiqing 2016-08-01
	 * @param c1
	 */
	public void removePengCardDown(int c1) {
		byte b = (byte) (c1 & 0xff);
		int idx = -1;
		for (int i = 0; i < cardsDown.size(); i++) {
			int bb = cardsDown.get(i);
			byte b1 = (byte) (bb & 0xff);
			byte b2 = (byte) ((bb >> 8) & 0xff);
			byte b3 = (byte) ((bb >> 16) & 0xff);

			if (b1 == b2 && b2 == b3 && b == b1) {
				idx = i;
				break;
			}
		}
		if (idx != -1) {
			cardsDown.remove(idx);
		}
	}

	/*
	 * -->TODO: 在玩家门前杠的牌 add by  2016.7.21
	 */
	public int addGangCardDown(int c1, int c2, int c3, int c4) {
		int card = (c4 << 24) | (c3 << 16) | (c2 << 8) | c1;
		cardsDown.add(card);
		return card;
	}

	//
	public void clear_cards() {
		cardsDown.clear();
		cardsInHand.clear();
		cardsBefore.clear();
		cardsOut.clear();
		cardsOutAfterTing.clear();
		cardGrab = 0;

		this.fanType = 0;
		winState = 0;
		winLoseCurrCoinNum = 0;
//		winLoseTotalCoinNum = 0;
		specialCoinNum = 0;
	}

	public void clear_game_state() {
		gameScore = 0;
		tableID = "";

		playingSingleTable = false;

		gameState = GameConstant.STATE_INVALID;
		attackInfos.clear();

		tablePos = 0;

		taskScore = 0;

		//
		cardsDown.clear();
		cardsInHand.clear();
		cardsBefore.clear();
		cardsOut.clear();
		cardsOutAfterTing.clear();
		cardGrab = 0;

		autoOperation = 0;

		// isLeftMo = false;
		readyFlag = 0;

		isApplyCloseRoom = false;
		agreeCloseRoom = 0;

		isXuanPiao = false;
		isTipPiao = false;
		isTingFeng = false;
		tingPos.clear();
		cancelChangeBao.clear();
		cancelHuCard = 0;
		
		isCallScore = false;

		isLord = false;
		doubleResult = 0;
		winLoseTotal = 0;
		isCallDouble = false;
		fanType = 0;
		outCardTime = 0;
		fanNum = 0;
		callScorePoint = 0;
		if(userGameHanderForNNDZZ != null){
			userGameHanderForNNDZZ.clear_game_state();
		}
		winState = 0;
		winLoseCurrCoinNum = 0;
		winLoseTotalCoinNum = 0;
		specialCoinNum = 0;
	}

	// 玩家再玩一局，把当前局的信息清理调
	public void playerContinue() {
		gameScore = 0;

		playingSingleTable = false;

		gameState = GameConstant.USER_GAME_STATE_IN_TABLE_READY;
		attackInfos.clear();

		taskScore = 0;

		cardsDown.clear();
		cardsInHand.clear();
		cardsBefore.clear();
		cardsOut.clear();
		cardsOutAfterTing.clear();
		cardGrab = 0;

		autoOperation = 0;
		isApplyCloseRoom = false;
		agreeCloseRoom = 0;
		isXuanPiao = false;
		isTipPiao = false;
		isTingFeng = false;
		tingPos.clear();
		cancelChangeBao.clear();
		cancelHuCard = 0;
		
		
		isLord=false;
		doubleResult = 0;
		isCallScore = false;
		isCallDouble = false;
		fanType = 0;
		outCardTime = 0;
		fanNum = 0;
		callScorePoint = 0;
	}

	// 复制玩家当前的游戏数据
	public void copy_game_state(User pl) {
		gameScore = pl.getGameScore();
		roomID = pl.getRoomID();
		tableID = pl.getTableID();
		this.vipTableID = pl.getVipTableID();

		tablePos = pl.getTablePos();

		playingSingleTable = pl.isPlayingSingleTable();

		gameState = pl.getGameState();
		// attackInfos.clear();

		taskScore = pl.getTaskScore();

		//
		cardsDown = pl.getCardsDown();
		cardsInHand = pl.getCardsInHand();
		cardsBefore = pl.getCardsBefore();
		cardsOut = pl.getCardsOut();
		cardsOutAfterTing = pl.getCardsOutAfterTing();
		cardGrab = pl.getCardGrab();

		autoOperation = pl.getAutoOperation();

		this.vipTableGold = pl.getVipTableGold();

		vrr = pl.getVrr();
		recordBean = pl.getRecordBean();
		overTimeState = pl.getOverTimeState();
		isApplyCloseRoom = pl.isApplyCloseRoom();
		agreeCloseRoom = pl.isAgreeCloseRoom();

		isXuanPiao = pl.isXuanPiao();
		isTipPiao = pl.isTipPiao();
		isTingFeng = pl.isTingFeng();
		cancelChangeBao.clear();
		cancelHuCard = 0;
		
		isLord = pl.isLord();
		isCallScore = pl.isCallScore();
		doubleResult = pl.getDoubleResult();
		isCallDouble = pl.isCallDouble();
		fanType = pl.getFanType();
		opStartTime = pl.getOpStartTime();
		outCardTime = pl.getOutCardTime();
		fanNum = pl.getFanNum();
		winCount = pl.getWinCount();
		zhuangCount = pl.getZhuangCount();
		dianpaoCount = pl.getDianpaoCount();
		mobaoCount = pl.getMobaoCount();
		baozhongbaoCount = pl.getBaozhongbaoCount();
		callScorePoint = pl.getCallScorePoint();
		
		winState = pl.getWinState();
		winLoseCurrCoinNum = pl.getWinLoseCurrCoinNum();
		winLoseTotalCoinNum = pl.getWinLoseTotalCoinNum();
		specialCoinNum = pl.getSpecialCoinNum();
		score = pl.getScore();
		userGameHanderForNNDZZ = pl.getUserGameHanderForNNDZZ();
	}

	public void clearVipTableInfo() {
		this.zhuangCount = 0;
		this.winCount = 0;
		this.dianpaoCount = 0;
		this.mobaoCount = 0;
		this.baozhongbaoCount = 0;
	}

	public void clearVipRoomID() {
		vipTableID = "";
	}

	//
	public void backupCards() {
		bk_cardsInHand.clear();
		bk_cardsDown.clear();
		//
		for (int i = 0; i < cardsInHand.size(); i++) {
			byte bb = cardsInHand.get(i);
			bk_cardsInHand.add(bb);
		}
		//
		for (int i = 0; i < cardsDown.size(); i++) {
			int cc = cardsDown.get(i);
			bk_cardsDown.add(cc);
		}

	}

	public void recoverCards() {
		cardsInHand.clear();
		cardsDown.clear();
		//
		for (int i = 0; i < bk_cardsInHand.size(); i++) {
			byte bb = bk_cardsInHand.get(i);
			cardsInHand.add(bb);
		}
		//
		for (int i = 0; i < bk_cardsDown.size(); i++) {
			int cc = bk_cardsDown.get(i);
			cardsDown.add(cc);
		}
	}

	// 在玩家门前放一张牌
	public void addCardBefore(Byte b) {
		cardsBefore.add(b);
	}

	//
	public int getTotalBattles() {
		return this.wons + this.loses + this.escape;
	}

	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public String getUnionID() {
		return unionID;
	}

	public void setUnionID(String unionID) {
		this.unionID = unionID;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getMachineCode() {
		return machineCode;
	}

	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		if (gold >= 0)
			this.gold = gold;
	}

	public String getGoldVerify() {
		return goldVerify;
	}

	public void setGoldVerify(String goldVerify) {
		this.goldVerify = goldVerify;
	}

	public int getDiamond() {
		return diamond;
	}

	// 把5种钻石的数量放在一个int数据里面
	public int get_diamonds_data() {
		int data = 0;
		// 这里显示用，最多63颗和31颗，多了放不下
		int num1 = getPlayerItemNumByBaseID(3333);
		if (num1 > 127)
			num1 = 127;
		int num2 = getPlayerItemNumByBaseID(3334);
		if (num2 > 127)
			num2 = 127;
		int num3 = getPlayerItemNumByBaseID(3335);
		if (num3 > 63)
			num3 = 63;
		int num4 = getPlayerItemNumByBaseID(3336);
		if (num4 > 63)
			num4 = 63;
		int num5 = getPlayerItemNumByBaseID(3337);
		if (num5 > 63)
			num5 = 63;
		//
		data = (num1) | (num2 << 7) | (num3 << 14) | (num4 << 20)
				| (num5 << 26);
		//
		return data;
	}

	public void setDiamond(int diamond) {
		this.diamond = diamond;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getWons() {
		return wons;
	}

	public void setWons(int wons) {
		this.wons = wons;
	}

	public int getLoses() {
		return loses;
	}

	public void setLoses(int loses) {
		this.loses = loses;
	}

	public Date getRegisTime() {
		return regisTime;
	}

	public void setRegisTime(Date regisTime) {
		this.regisTime = regisTime;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public int getEscape() {
		return escape;
	}

	public void setEscape(int escape) {
		this.escape = escape;
	}

	public Date getOfflineTime() {
		return offlineTime;
	}

	public void setOfflineTime(Date offlineTime) {
		this.offlineTime = offlineTime;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public IoSession getSession() {
		return session;
	}

	public void setSession(IoSession session) {
		this.session = session;
	}

	public int getContinueLanding() {
		return continueLanding;
	}

	public void setContinueLanding(int continueLanding) {
		this.continueLanding = continueLanding;
	}

	public int getRoomID() {
		return roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	public int getGameScore() {
		return gameScore;
	}

	public void setGameScore(int gameScore) {
		if (gameScore < 0)
			gameScore = 0;
		//

		//
		this.gameScore = gameScore;
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public int getLuckyDrawsTimes() {
		return luckyDrawsTimes;
	}

	public void setLuckyDrawsTimes(int luckyDrawsTimes) {
		this.luckyDrawsTimes = luckyDrawsTimes;
	}

	public int getGameState() {
		return gameState;
	}

	public void setGameState(int gameState) {
		this.gameState = gameState;
	}

	public boolean isRobot() {
		return isRobot;
	}

	public void setRobot(boolean isRobot) {
		this.isRobot = isRobot;
	}

	public int getHeadImg() {
		return headImg;
	}

	public void setHeadImg(int headImg) {
		this.headImg = headImg;
	}

	public int getHisBestScore() {
		return hisBestScore;
	}

	public void setHisBestScore(int hisBestScore) {
		this.hisBestScore = hisBestScore;
	}

	public int getTodayBestScore() {
		return todayBestScore;
	}

	public void setTodayBestScore(int todayBestScore) {
		this.todayBestScore = todayBestScore;
	}

	public int getDiamondTotalCost() {
		return diamondTotalCost;
	}

	public void setDiamondTotalCost(int diamondTotalCost) {
		this.diamondTotalCost = diamondTotalCost;
	}

	public List<UserBackpack> getItems() {
		return items;
	}

	//
	public UserBackpack getPlayerItemByBaseID(int base_item_id) {
		UserBackpack pit = null;
		for (int i = 0; i < items.size(); i++) {
			UserBackpack pi = items.get(i);
			if (pi.getItemBaseID() == base_item_id) {
				pit = pi;
				break;
			}
		}
		//
		return pit;
	}

	public int getPlayerItemNumByBaseID(int base_item_id) {
		int num = 0;
		for (int i = 0; i < items.size(); i++) {
			UserBackpack pi = items.get(i);
			if (pi.getItemBaseID() == base_item_id) {
				num = pi.getItemNum();
				break;
			}
		}
		//
		return num;
	}

	public boolean attack(String enemyPlayerName, int base_item_id, long time) {
		boolean could_succ = true;
		for (int i = 0; i < attackInfos.size(); i++) {
			AttackInfo att = attackInfos.get(i);
			// 同类型不能在5秒内攻击2次
			if (att.item_base_id == base_item_id && (time - att.time < 5000)) {
				could_succ = false;
				break;
			}
		}
		//
		if (could_succ) {
			AttackInfo att = new AttackInfo();
			att.item_base_id = base_item_id;
			att.time = time;
			att.playerName = enemyPlayerName;
			//
			attackInfos.add(att);
		}
		//
		return could_succ;
	}

	public void robotInitDiamond(int vipLevel) {
		int num = 1;
		int itemBaseID = 3333;
		while (num <= vipLevel && num <= 5) {
			UserBackpack playerItem = new UserBackpack();
			playerItem.setPlayerID(this.playerID);
			playerItem.setItemBaseID(itemBaseID);
			playerItem.setItemNum(1);
			this.items.add(playerItem);
			itemBaseID++;
			num++;
		}
		// System.out.println("机器人钻石"+this.items.size());
	}

	public void setItems(List<UserBackpack> items) {
		this.items = items;
	}

	public int getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(int firstLogin) {
		this.firstLogin = firstLogin;
	}

	public List<AttackInfo> getAttackInfos() {
		return attackInfos;
	}

	public void setAttackInfos(List<AttackInfo> attackInfos) {
		this.attackInfos = attackInfos;
	}

	public boolean isPlayingSingleTable() {
		return playingSingleTable;
	}

	public void setPlayingSingleTable(boolean playingSingleTable) {
		this.playingSingleTable = playingSingleTable;
	}

	public long getRobotUseItemTime() {
		return robotUseItemTime;
	}

	public void setRobotUseItemTime(long robotUseItemTime) {
		this.robotUseItemTime = robotUseItemTime;
	}

	public int getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(int vipLevel) {
		this.vipLevel = vipLevel;
	}

	public int getVipExp() {
		return vipExp;
	}

	public void setVipExp(int vipExp) {
		this.vipExp = vipExp;
	}

	public int getTaskScore() {
		return taskScore;
	}

	public void setTaskScore(int taskScore) {
		this.taskScore = taskScore;
	}

	public int getGemNum() {
		return gemNum;
	}

	public void setGemNum(int gemNum) {
		this.gemNum = gemNum;
	}

	public Integer getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(Integer playerIndex) {
		this.playerIndex = playerIndex;
	}

	public boolean isOnline() {
		return isOnline;
	}

	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}

	public int getPlatformType() {
		return platformType;
	}

	public void setPlatformType(int platformType) {
		this.platformType = platformType;
	}

	public byte getCard(int idx) {
		if (idx < 0 || idx >= cardsInHand.size())
			return 0;
		//
		byte b = cardsInHand.get(idx);

		return b;
	}

	// 玩家手里有几张牌
	public int getCardNumInHand() {
		return cardsInHand.size();
	}

	public List<Integer> getCardsDown() {
		return cardsDown;
	}

	public void setCardsDown(List<Integer> cardsDown) {
		this.cardsDown = cardsDown;
	}

	/**
	 * 获取倒牌数量
	 * 
	 * @return
	 */
	public int getDownCardsNum() {
		int count = 0;
		for (Integer cd : cardsDown) {
			int v1 = cd & 0xff;
			int v2 = (cd >> 8) & 0xff;
			int v3 = (cd >> 16) & 0xff;
			if (v1 > 0 && v2 == 0 && v3 == 0) {
				// 只有一张为特殊的蛋，不计数量
			} else {
				if (v1 > 0) {
					count++;
				}
				if (v2 > 0) {
					count++;
				}
				if (v3 > 0) {
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * 获取现在手牌+倒牌的数量（不算杠牌第四张）
	 * 
	 * @return
	 */
	public int getCardsCount() {
		int numInHand = cardsInHand.size();
		if (cardGrab != 0) {
			numInHand += 1;
		}
		int numInDown = this.getDownCardsNum();
		return numInHand + numInDown;
	}

	public List<Byte> getCardsBefore() {
		return cardsBefore;
	}

	public void setCardsBefore(List<Byte> cardsBefore) {
		this.cardsBefore = cardsBefore;
	}

	public List<Byte> getCardsInHand() {
		return cardsInHand;
	}

	public void setCardsInHand(List<Byte> cardsInHand) {
		this.cardsInHand = cardsInHand;
	}

	public List<Byte> getCardsOut() {
		return cardsOut;
	}

	public void setCardsOut(List<Byte> cardsOut) {
		this.cardsOut = cardsOut;
	}

	public void addOutCard(Byte b) {
		// 记录出牌
		cardsOut.add(b);
	}

	public List<Byte> getCardsOutAfterTing() {
		return cardsOutAfterTing;
	}

	public void setCardsOutAfterTing(List<Byte> cardsOutAfterTing) {
		this.cardsOutAfterTing = cardsOutAfterTing;
	}

	public Byte getCardGrab() {
		return cardGrab;
	}

	public void setCardGrab(Byte cardGrab) {
		this.cardGrab = cardGrab;
	}

	public int getTablePos() {
		return tablePos;
	}

	public void setTablePos(int tablePos) {
		this.tablePos = tablePos;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public List<Integer> getBk_cardsDown() {
		return bk_cardsDown;
	}

	public void setBk_cardsDown(List<Integer> bk_cardsDown) {
		this.bk_cardsDown = bk_cardsDown;
	}

	public List<Byte> getBk_cardsInHand() {
		return bk_cardsInHand;
	}

	public void setBk_cardsInHand(List<Byte> bk_cardsInHand) {
		this.bk_cardsInHand = bk_cardsInHand;
	}

	// 庄家返回2，其他1
	public int getDealerFanNum(int dealerPos) {
		if (dealerPos == this.tablePos)
			return 2;
		//
		return 1;
	}

	public String getVipTableID() {
		return vipTableID;
	}

	public void setVipTableID(String vipTableID) {
		this.vipTableID = vipTableID;
	}

	public long getInRoomWaitOtherTime() {
		return inRoomWaitOtherTime;
	}

	public void setInRoomWaitOtherTime(long inroomWaitOtherTime) {
		this.inRoomWaitOtherTime = inroomWaitOtherTime;
	}

	public int getRobotLevel() {
		return robotlevel;
	}

	public void setRobotlevel(int robotlevel) {
		this.robotlevel = robotlevel;
	}

	public VipRoomRecord getVrr() {
		return vrr;
	}

	public void setVrr(VipRoomRecord vrr) {
		this.vrr = vrr;
	}
	public RecordBean getRecordBean() {
		return recordBean;
	}
    public void setRecordBean(RecordBean recordBean) {
		this.recordBean = recordBean;
	}
	public int getRobotgamenum() {
		return robotgamenum;
	}

	public void setRobotgamenum(int robotgamenum) {
		this.robotgamenum = robotgamenum;
	}

	// 递减机器人局数
	public void dcRobotgamenum() {
		this.robotgamenum--;
	}

	public int getCanFriend() {
		return canFriend;
	}

	public void setCanFriend(int canFriend) {
		this.canFriend = canFriend;
	}

	public int getSaveTime() {
		return saveTime;
	}

	public void setSaveTime(int saveTime) {
		this.saveTime = saveTime;
	}

	public String getQqOpenID() {
		return qqOpenID;
	}

	public void setQqOpenID(String qqOpenID) {
		this.qqOpenID = qqOpenID;
	}

	// 设置托管状态
	public void setAutoOperation(int auto) {
		this.autoOperation = auto;
		if (auto == 1) {
			setOverTimeState(GameConstant.USER_GAME_OVERTIME_STATE_IN_TABLE_WAITING_TO_OVERTIMECHU);
		} else {
			setOverTimeState(GameConstant.USER_GAME_OVERTIME_STATE_IN_TABLE_NOWAITING_TO_OVERTIMECHU);
		}
	}

	public int getAutoOperation() {
		return this.autoOperation;
	}

	public int getVipTableGold() {
		return vipTableGold;
	}

	public void setVipTableGold(int vipTableGold) {

		this.vipTableGold = vipTableGold;
		// if (this.vipTableGold < 0)
		// this.vipTableGold = 0;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getWxOpenID() {
		return wxOpenID;
	}

	public void setWxOpenID(String wxOpenID) {
		this.wxOpenID = wxOpenID;
	}

	public void setOnTable(boolean ret) {
		this.isOnTable = ret;
	}

	public boolean getOnTable() {
		return this.isOnTable;
	}

	public int getDeviceFlag() {
		return deviceFlag;
	}

	public void setDeviceFlag(int deviceFlag) {
		this.deviceFlag = deviceFlag;
	}

	public int getOverTimeState() {
		return overTimeState;
	}

	public void setOverTimeState(int overTimeState) {
		this.overTimeState = overTimeState;
	}

	public int getNextCard() {
		return nextCard;
	}

	public void setNextCard(int nextCard) {
		this.nextCard = nextCard;
	}

	public boolean isNeedCopyGameState() {
		return needCopyGameState;
	}

	public void setNeedCopyGameState(boolean needCopyGameState) {
		this.needCopyGameState = needCopyGameState;
	}

	public int getPlayerType() {
		return playerType;
	}

	public void setPlayerType(int playerType) {
		this.playerType = playerType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		User.logger = logger;
	}

	public int getRobotlevel() {
		return robotlevel;
	}

	public boolean isOnTable() {
		return isOnTable;
	}

	public int getTd_Zhan_count() {
		return td_Zhan_count;
	}

	public void setTd_Zhan_count(int td_Zhan_count) {
		this.td_Zhan_count = td_Zhan_count;
	}

	public int getBx_hand_count() {
		return bx_hand_count;
	}

	public void setBx_hand_count(int bx_hand_count) {
		this.bx_hand_count = bx_hand_count;
	}

	public int getExtra_box_count() {
		return extra_box_count;
	}

	public void setExtra_box_count(int extra_box_count) {
		this.extra_box_count = extra_box_count;
	}

	public boolean isTwoVipOnline() {
		return isTwoVipOnline;
	}

	public void setTwoVipOnline(boolean isTwoVipOnline) {
		this.isTwoVipOnline = isTwoVipOnline;
	}

	public String getIdentify_card() {
		return identify_card;
	}

	public String getParam01() {
		return param01;
	}

	public void setParam01(String param01) {
		this.param01 = param01;
	}

	public boolean isAgreeExtendCard() {
		return agreeExtendCard;
	}

	public void setAgreeExtendCard(boolean agreeExtendCard) {
		this.agreeExtendCard = agreeExtendCard;
	}

	public boolean isChooseExtendCard() {
		return chooseExtendCard;
	}

	public void setChooseExtendCard(boolean chooseExtendCard) {
		this.chooseExtendCard = chooseExtendCard;
	}

	public int getReadyFlag() {
		return readyFlag;
	}

	public void setReadyFlag(int readyFlag) {
		this.readyFlag = readyFlag;
	}

	public void setAgreeCloseRoom(int agreeCloseRoom) {
		this.agreeCloseRoom = agreeCloseRoom;
	}

	public int isAgreeCloseRoom() {
		return agreeCloseRoom;
	}

	public void setApplyCloseRoom(boolean isApplyCloseRoom) {
		this.isApplyCloseRoom = isApplyCloseRoom;
	}

	public boolean isApplyCloseRoom() {
		return isApplyCloseRoom;
	}

	public List<Byte> getDuizhiCards() {
		return duizhiCards;
	}

	public void setDuizhiCards(List<Byte> duizhiCards) {
		this.duizhiCards = duizhiCards;
	}

	@Override
	public String toString() {
		return super.toString() + "，idx=" + playerIndex + "，name=" + playerName
				+ "，pos=" + tablePos;
	}

	public boolean isXuanPiao() {
		return isXuanPiao;
	}

	public void setXuanPiao(boolean isXuanPiao) {
		this.isXuanPiao = isXuanPiao;
	}

	public boolean isTipPiao() {
		return isTipPiao;
	}

	public void setTipPiao(boolean isTipPiao) {
		this.isTipPiao = isTipPiao;
	}

	public boolean isTingFeng() {
		return isTingFeng;
	}

	public void setTingFeng(boolean isTingFeng) {
		this.isTingFeng = isTingFeng;
	}

	public List<Integer> getTingPos() {
		return tingPos;
	}

	public void setTingPos(List<Integer> tingPos) {
		this.tingPos = tingPos;
	}

	public List<Byte> getCancelChangeBao() {
		return cancelChangeBao;
	}

	public void setCancelChangeBao(List<Byte> cancelChangeBao) {
		this.cancelChangeBao = cancelChangeBao;
	}

	public byte getCancelHuCard() {
		return cancelHuCard;
	}

	public void setCancelHuCard(byte cancelHuCard) {
		this.cancelHuCard = cancelHuCard;
	}

	public String getDeviceBrand() {
		return deviceBrand;
	}

	public void setDeviceBrand(String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}

	public String getSystemVersion() {
		return systemVersion;
	}

	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getHttpClientPort() {
		return httpClientPort;
	}

	public void setHttpClientPort(String httpClientPort) {
		this.httpClientPort = httpClientPort;
	}

	public String getProxyIp() {
		return proxyIp;
	}

	public void setProxyIp(String proxyIp) {
		this.proxyIp = proxyIp;
	}
	
	public void setbAgreeContinue(boolean bAgreeContinue) {
		this.bAgreeContinue = bAgreeContinue;
	}

	public boolean isbAgreeContinue() {
		return bAgreeContinue;
	}
	
	
	//ddz
	public int getWinLoseTotal() {
		return winLoseTotal;
	}

	public void setWinLoseTotal(int winLoseTotal) {
		this.winLoseTotal = winLoseTotal;
	}
	
	public int getWinLoseGoldNum() {
		return winLoseGoldNum;
	}

	public void setWinLoseGoldNum(int winLoseGoldNum) {
		this.winLoseGoldNum = winLoseGoldNum;
	}
	
	public boolean isLord() {
		return isLord;
	}

	public void setLord(boolean isLord) {
		this.isLord = isLord;
	}
	
	public int getDoubleResult() {
		return doubleResult;
	}

	public void setDoubleResult(int doubleResult) {
		this.doubleResult = doubleResult;
	}

	public boolean isCallScore() {
		return isCallScore;
	}

	public void setCallScore(boolean isCallScore) {
		this.isCallScore = isCallScore;
	}
	
	public String getFanDesc() {
		String str = "";

		if ((fanType & GameConstant.DDZ_RESULT_WIN) != 0) {
			str += "赢";
		} else {
			str += "输";
		}
		return str;
	}
	
	public int getFanType() {
		return fanType;
	}

	public boolean isCallDouble() {
		return isCallDouble;
	}

	public void setCallDouble(boolean isCallDouble) {
		this.isCallDouble = isCallDouble;
	}
	
	public long getOpStartTime() {
		return opStartTime;
	}

	public void setOpStartTime(long opStartTime) {
		this.opStartTime = opStartTime;
	}
	public int getOutCardTime() {
		return outCardTime;
	}

	public void setOutCardTime(int outCardTime) {
		this.outCardTime = outCardTime;
	}
	public int getFanNum() {
		return fanNum;
	}

	public void setFanNum(int fanNum) {
		this.fanNum = fanNum;
	}
	
	public void setFanType(int fanType) {
		this.fanType = fanType;
	}
	
	public int getWinCount() {
		return winCount;
	}

	public void setWinCount(int winCount) {
		this.winCount = winCount;
	}
	
	public int getZhuangCount() {
		return zhuangCount;
	}

	public void setZhuangCount(int zhuangCount) {
		this.zhuangCount = zhuangCount;
	}


	public int getDianpaoCount() {
		return dianpaoCount;
	}

	public void setDianpaoCount(int dianpaoCount) {
		this.dianpaoCount = dianpaoCount;
	}

	public int getMobaoCount() {
		return mobaoCount;
	}

	public void setMobaoCount(int mobaoCount) {
		this.mobaoCount = mobaoCount;
	}

	public int getBaozhongbaoCount() {
		return baozhongbaoCount;
	}

	public void setBaozhongbaoCount(int baozhongbaoCount) {
		this.baozhongbaoCount = baozhongbaoCount;
	}
	
	public void addOutCardTime(){
		this.outCardTime++;
	}
	
	public int getCallScorePoint() {
		return callScorePoint;
	}

	public void setCallScorePoint(int callScorePoint) {
		this.callScorePoint = callScorePoint;
	}
	
	public void setCredits(int credits) {
		this.credits = credits;
	}

	public int getCredits() {
		return credits;
	}

	
	// 赢为正的，输是负的，0就呵呵
	public int getWinNum() {
		if (winLoseGoldNum == 0)
			return 0;
		//
		//
		if ((fanType & GameConstant.DDZ_RESULT_WIN) != 0) {
			return winLoseGoldNum;
		}

		return (-1) * winLoseGoldNum;

	}

	public int getWinNumForXZ(){
		return winLoseGoldNum;
	}
	
	public int getWinNumForBC(){
		return winLoseGoldNum;
	}
	
	
	//
	public void clear_cards_nndzz() {
		cardsInHand.clear();
		//
		this.winLoseGoldNum = 0;
		this.fanType = 0;
	}
	
	/**
	 * 玩家再玩一局，把当前局的信息清理调
	 * User.java
	 * void
	 */
	public void playerContinue_nndzz() {
		gameScore = 0;
		playingSingleTable = false;

		gameState = GameConstant.USER_GAME_STATE_IN_TABLE_READY;
		attackInfos.clear();
		taskScore = 0;
		
		outCardTime = 0;
		doubleResult = 0;

		cardsInHand.clear();

		fanNum = 0;
		fanType = 0;
		winLoseGoldNum = 0;// 胡分

		autoOperation = 0;
		
		winState = 0;
		winLoseCurrCoinNum = 0;
		specialCoinNum = 0;
		if(userGameHanderForNNDZZ != null){
			userGameHanderForNNDZZ.playerContinue();
		}
	}
	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	//====================================================
	
	public void create_game_hander(){
		userGameHanderForNNDZZ = new com.chess.nndzz.bean.UserGameHander(this);
	}
	
	public List<ProxyVipRoomRecord> getPvrrs() {
		return pvrrs;
	}

	public void setPvrrs(List<ProxyVipRoomRecord> pvrrs) {
		this.pvrrs = pvrrs;
	}

	public int getWinState() {
		return winState;
	}

	public void setWinState(int winState) {
		this.winState = winState;
	}

	public int getWinLoseTotalCoinNum() {
		return winLoseTotalCoinNum;
	}

	public void setWinLoseTotalCoinNum(int winLoseTotalCoinNum) {
		this.winLoseTotalCoinNum = winLoseTotalCoinNum;
	}

	public int getWinLoseCurrCoinNum() {
		return winLoseCurrCoinNum;
	}

	public void setWinLoseCurrCoinNum(int winLoseCurrCoinNum) {
		this.winLoseCurrCoinNum = winLoseCurrCoinNum;
	}

	public int getSpecialCoinNum() {
		return specialCoinNum;
	}

	public void setSpecialCoinNum(int specialCoinNum) {
		this.specialCoinNum = specialCoinNum;
	}

	public int getProxyOpenRoom() {
		return proxyOpenRoom;
	}

	public void setProxyOpenRoom(int proxyOpenRoom) {
		this.proxyOpenRoom = proxyOpenRoom;
	}
	public String getHeadImgUrl() {
		return HEADIMGURL;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.HEADIMGURL = headImgUrl;
	}
	
	public int getTotalCredits() {
		return totalCredits;
	}

	public void setTotalCredits(int totalCredits) {
		this.totalCredits = totalCredits;
	}

	public com.chess.nndzz.bean.UserGameHander getUserGameHanderForNNDZZ() {
		return userGameHanderForNNDZZ;
	}

	public void setUserGameHanderForNNDZZ(
			com.chess.nndzz.bean.UserGameHander userGameHanderForNNDZZ) {
		this.userGameHanderForNNDZZ = userGameHanderForNNDZZ;
	}
	
	public int getClubType() {
		return clubType;
	}

	public void setClubType(int clubType) {
		this.clubType = clubType;
	}

	public String getMgrIndex() {
		return mgrIndex;
	}

	public void setMgrIndex(String mgrIndex) {
		this.mgrIndex = mgrIndex;
	}
	
}