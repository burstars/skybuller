package com.chess.common.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.chess.core.net.msg.NetObject;

public class MahjongCheckResult extends NetObject {
	public String actionId="";//actionId
	public int opertaion = 0;
	public int playerTableIndex = 0;

	public int chi_card_value = 0;
	public int peng_card_value = 0;

	// 吃碰杠听胡谁的牌 add by  2016.9.6
	public int	 out_card_player_index = -1;
	//-->TODO: 各种杠牌信息 add by  2016.7.21
	public int ming_gang_card_value = 0;
	public int an_gang_card_value = 0;
	public int one_egg_card_value = 0;
	public int nine_egg_card_value = 0;
	public int feng_gang_card_value = 0;
	public int happy_gang_card_value = 0;
	public int big_egg_card_value = 0;
	public int fly_egg_card_value = 0;
	public int special_gang_card_value = 0;
	public int add_gang_card_value = 0;
	public int facai_gang_card_value = 0;
	public int chi_gang_card_value = 0;
	
	// 吃听标位符
	public int chi_flag = 0;

	// 吃碰杠的目标牌
	public byte targetCard = 0;
	//
	// 是否是卡胡，胡卡张
	public boolean isKaHu = false;

	// STiV modify
	// 是否三七边胡
	public boolean is37 = false;
	/**单胡1 9*/
	public boolean is19 = false;
	// 是否单吊
	public boolean isDanDiao = false;

	public boolean isMoBao = false;
	public boolean isHuBao = false;

//	public boolean isDaFeng = false;
//	public boolean isHongZhong = false;
	public boolean isLou = false;
	/**飘胡*/
	public boolean isPiao = false;
	/**普通7对*/
	public boolean is7Dui = false;
	/**豪华7对*/
	public boolean is7MaxDui = false;
	
	/**
	 * -->TODO 抢杠胡 cuiweiqing 2016-08-03
	 */
	public boolean isQiangGang = false;

	// STiV modify end

	// 打出哪些牌可以听
	public List<Byte> tingList = new ArrayList<Byte>();
	
	/**
	 * -->TODO 补杠信息缓存 cuiweiqing 2016-08-04
	 */
	public List<Byte> addNormalGangValues = new ArrayList<Byte>();
	public List<Byte> addSpecialGangValues = new ArrayList<Byte>();
	public List<Integer> normalAddGangIndexes = new ArrayList<Integer>();
	public List<Integer> specialAddGangIndexes = new ArrayList<Integer>();
	
	//预留Param01-int
	public int player0Parm01=0;
	public int player1Parm01=0;
	public int player2Parm01=0;
	public int player3Parm01=0;
	//预留Param02-byte
	public byte player0Parm02=0;
	public byte player1Parm02=0;
	public byte player2Parm02=0;
	public byte player3Parm02=0;
	
	public List<Integer> player0Param01List = new ArrayList<Integer>();
	public List<Integer> player1Param01List = new ArrayList<Integer>();
	public List<Integer> player2Param01List = new ArrayList<Integer>();
	public List<Integer> player3Param01List = new ArrayList<Integer>();
	
	public List<Byte> player0Param02List = new ArrayList<Byte>();
	public List<Byte> player1Param02List = new ArrayList<Byte>();
	public List<Byte> player2Param02List = new ArrayList<Byte>();
	public List<Byte> player3Param02List = new ArrayList<Byte>();
	
	
	//ddz
	public List<Byte> chuCards = new ArrayList<Byte>();
	
	public int opValue = 0;
	
	public int card_value = 0;
	
	public List<Integer> one_gang_list = new ArrayList<Integer>();
	public List<Integer> nine_gang_list = new ArrayList<Integer>();
	public List<Integer> feng_gang_list = new ArrayList<Integer>();
	public boolean isStopAction = false;
	public String huanHuiDowns = "";
	//
	/**扑扛的牌:kezi*/
	public List<Byte> pu_gang_ke_list = new ArrayList<Byte>();
	/**扑扛的牌:shunzi*/
	public List<Byte> pu_gang_shun_list = new ArrayList<Byte>();
	/**扑扛待选择的牌*/
	public List<Byte> pu_gang_sel_list = new ArrayList<Byte>();
	
	public List<Integer> stakeItems = new ArrayList<Integer>();
	public Map<Integer, List<Integer>> allowStakeItems = new HashMap<Integer, List<Integer>>();
	
	public int operationTime = 0;
	
	//
	public MahjongCheckResult() {

	}

	//
	private boolean found_card(int v) {
		int v1 = chi_card_value & 0xff;
		int v2 = (chi_card_value >> 8) & 0xff;
		int v3 = (chi_card_value >> 16) & 0xff;
		int v4 = (chi_card_value >> 24) & 0xff;

		if (v == v1 || v == v2 || v == v3 || v == v4)
			return true;
		//
		return false;
	}

	// 玩家吃牌参数是否正确
	public boolean chi_check(int value1, int value2) {
		if (value1 == 0 || value2 == 0 || value2 == value1)
			return false;
		//
		int delta = Math.abs(value2 - value1);
		if (delta == 0 || delta > 2)
			return false;

		if (found_card(value1) && found_card(value2))
			return true;

		return false;
	}
}
