/**
 * Description:VIP战绩的每局记录
 */
package com.chess.common.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.chess.common.DateService;
import com.chess.common.StringUtil;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class VipGameRecord extends NetObject {

	private String recordID = "";
	/** 房间ID */
	private String roomID = "";
	/** 局号 */
	private int gameID = 0;
	/** 玩家ID */
	private String playerID = "";
	/** 玩家昵称 */
	private String playerName = "";
	/** 番数 */
	private int gameMultiple = 0;
	/** 番型 */
	private int gameType1 = 0;
	private int handIndex=0;//第几把
	/** 本局得分 */
	private long gameScore = 0;
	/** 结算时间 */
	private Date recordDate = null;
	/** 结算时间(字符串格式) */
	private String date;
	/** 表名后缀 */
	private String tableNameSuffix = DateService.getTableSuffixByType("", DateService.DATE_BY_DAY);

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		roomID = ar.sString(roomID);
		gameID = ar.sInt(gameID);
		playerID = ar.sString(playerID);
		playerName = ar.sString(playerName);
		gameMultiple = ar.sInt(gameMultiple);
		gameType1 = ar.sInt(gameType1);
		handIndex=ar.sInt(handIndex);
		gameScore = ar.sLong(gameScore);
		ar.sString(StringUtil.date2String(recordDate));
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public int getGameID() {
		return gameID;
	}

	public void setGameID(int gameID) {
		this.gameID = gameID;
	}

	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getGameMultiple() {
		return gameMultiple;
	}

	public void setGameMultiple(int gameMultiple) {
		this.gameMultiple = gameMultiple;
	}

	public int getGameType1() {
		return gameType1;
	}

	public void setGameType1(int gameType1) {
		this.gameType1 = gameType1;
	}

	public long getGameScore() {
		return gameScore;
	}

	public void setGameScore(long gameScore) {
		this.gameScore = gameScore;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
		this.date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(recordDate);
	}

	public String getRecordID() {
		return recordID;
	}

	public void setRecordID(String recordID) {
		this.recordID = recordID;
	}

	public String getTableNameSuffix() {
		return tableNameSuffix;
	}

	public void setTableNameSuffix(String tableNameSuffix) {
		this.tableNameSuffix = tableNameSuffix;
	}

	public int getHandIndex() {
		return handIndex;
	}

	public void setHandIndex(int handIndex) {
		this.handIndex = handIndex;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
