package com.chess.common.bean.thirdpay;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import net.sf.json.JSONObject;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.SpringService;
import com.chess.common.bean.MallItem;
import com.chess.common.bean.User;
import com.chess.common.framework.GameContext;
import com.chess.common.service.IPayService;
import com.chess.common.web.player.HTTPUtils;
import com.chess.common.web.player.HttpUtil;

public class ThirdPayService {
	private static Logger logger = LoggerFactory
			.getLogger(ThirdPayService.class);

	// // 微信
	// public static final int THIRD_PAY_CODE_WXPAY = 1007;
	// // 支付宝
	// public static final int THIRD_PAY_CODE_ALIPAY = 1006;
	//
	// private static final String THIRD_PAY_URL =
	// "http://payment.axwpay.com/bank";
	// private static final int THIRD_PAY_ID = 2296;
	// private static final String THIRD_PAY_KEY =
	// "5a1f5d79b0d34508a64a0f140e31610c";
	//
	// public static String generateOrder(int payType, User player, MallItem
	// itemBase,String outTradeNo) {
	// int price = itemBase.getPrice();
	// String callbackUrl =
	// GameContext.getPayConfig("third_notifyUrl").toString();
	//
	// StringBuilder unsign = new StringBuilder(512);
	// unsign.append("parter=").append(THIRD_PAY_ID).append("&type=").append(payType).append("&value=").append(0.02);
	// unsign.append("&orderid=").append(outTradeNo).append("&callbackurl=").append(callbackUrl);
	//
	// String unsignStr = unsign.toString() + THIRD_PAY_KEY;
	// String sign = DigestUtils.md5Hex(unsignStr);
	// sign = sign.toLowerCase();
	//
	// StringBuilder url = new StringBuilder(512);
	// url.append(THIRD_PAY_URL).append("?").append(unsign).append("&payerIp=").append(player.getClientIP());
	// url.append("&attach=").append(player.getPlayerIndex()).append("_").append(itemBase.getBase_id());
	// url.append("&sign=").append(sign);
	//
	// logger.info("第三方支付请求处理，玩家ID【"+player.getPlayerIndex()+"】，玩家昵称【"+player.getPlayerName()
	// +"】，payType【"+payType+"】，道具ID【"+itemBase.getBase_id()+"】，道具名称【"+itemBase.getName()
	// +"】，价格【"+price+"】，订单号【"+outTradeNo+"】，回调接口【"+callbackUrl
	// +"】，unsignStr【"+unsignStr+"】，sign【"+sign+"】,url【"+url+"】");
	//
	// // String result = HttpUtil.get(url.toString(), "GB2312");
	// //
	// logger.info("第三方支付请求处理，玩家ID【"+player.getPlayerIndex()+"】，玩家昵称【"+player.getPlayerName()
	// //
	// +"】，payType【"+payType+"】，道具ID【"+itemBase.getBase_id()+"】，道具名称【"+itemBase.getName()
	// // +"】，价格【"+price+"】，订单号【"+outTradeNo+"】，结果【"+result+"】");
	//
	// return url.toString();
	// }
	//
	// public static void updateOrder(String orderno) {
	// logger.info("接到第三方支付回调成功，更新订单信息：订单号【"+orderno+"】");
	//
	// IPayService payService = (IPayService) SpringService
	// .getBean("payService");
	// payService.payCallBack(orderno, "", "");
	// }

	// 微信
	public static final String THIRD_PAY_CODE_WXPAY = "WEPAY_QR";
	// 支付宝
	public static final String THIRD_PAY_CODE_ALIPAY = "ALIPAY_WAP";//ALIPAY_WEB

	public static final String THIRD_PAY_URL = "https://api.Trimepay.com/gateway/pay/go";
	public static final long THIRD_PAY_ID = 15443177419296L;
	public static final String THIRD_PAY_KEY = "9e484a335565b52d76642da08686f7df";

	public static String generateOrder(String payType, User player,
			MallItem itemBase, String outTradeNo) {
		int price = itemBase.getPrice();
		String callbackUrl = GameContext.getPayConfig("third_notifyUrl")
				.toString();
		logger.info("第三方支付请求处理，玩家ID【" + player.getPlayerIndex() + "】，玩家昵称【"
				+ player.getPlayerName() + "】，payType【" + payType + "】，道具ID【"
				+ itemBase.getBase_id() + "】，道具名称【" + itemBase.getName()
				+ "】，价格【" + price + "】，订单号【" + outTradeNo + "】，回调接口【"
				+ callbackUrl + "】");
		
		Map<String, String> params = new TreeMap<String, String>();
		params.put("appId", String.valueOf(THIRD_PAY_ID));
		params.put("appSecret", THIRD_PAY_KEY);
		params.put("merchantTradeNo", outTradeNo);
		params.put("totalFee", String.valueOf(price * 100));
		params.put("notifyUrl", callbackUrl);
		params.put("payType", payType);
		
		logger.info("第三方支付请求处理，准备生成签名，params=" + params);
		String sign = prepareSign(params);
		logger.info("第三方支付请求处理，签名，sign=" + sign);
		params.put("sign", sign);

		logger.info("第三方支付请求处理，url【" + THIRD_PAY_URL + "】，params=" + params);
		String result = HTTPUtils.post(THIRD_PAY_URL, params);
		logger.info("第三方支付请求处理，玩家ID【" + player.getPlayerIndex() + "】，玩家昵称【"
				+ player.getPlayerName() + "】，payType【" + payType + "】，道具ID【"
				+ itemBase.getBase_id() + "】，道具名称【" + itemBase.getName()
				+ "】，价格【" + price + "】，订单号【" + outTradeNo + "】，结果【" + result
				+ "】");

		JSONObject json = JSONObject.fromObject(result);
		logger.info(json.toString());
		String data = json.getString("data");
		logger.info(data);
		return data;
	}

	public static String prepareSign(Map<String, String> params) {
		String unsign = ksort(params);
		logger.info("unsign=" + unsign);

		String sign = DigestUtils.md5Hex(unsign);
		logger.info("sign1=" + sign);
		sign = sign + THIRD_PAY_KEY;
		logger.info("sign2=" + sign);
		sign = DigestUtils.md5Hex(sign);
		logger.info("sign3=" + sign);
		return sign;
	}

	public static String ksort(Map<String, String> map) {
		String sb = "";
		String[] key = new String[map.size()];
		int index = 0;
		for (String k : map.keySet()) {
			key[index] = k;
			index++;
		}
		Arrays.sort(key);
		for (String s : key) {
			sb += s + "=" + map.get(s) + "&";
		}
		sb = sb.substring(0, sb.length() - 1);
		// 将得到的字符串进行处理得到目标格式的字符串

		try {
			sb = URLEncoder.encode(sb, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		sb = sb.replace("%3D", "=").replace("%26", "&");
		return sb.toString();
	}

	public static void updateOrder(String orderno) {
		logger.info("接到第三方支付回调成功，更新订单信息：订单号【" + orderno + "】");

		IPayService payService = (IPayService) SpringService
				.getBean("payService");
		payService.payCallBack(orderno, "", "");
	}

}
