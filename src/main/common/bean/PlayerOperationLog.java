package com.chess.common.bean;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.UUIDGenerator;
import com.chess.common.constant.LogConstant;
import com.chess.common.constant.PayConstant;

public class PlayerOperationLog {
	private static Logger logger = LoggerFactory
			.getLogger(PlayerOperationLog.class);
	/** 日志ID */
	private String logID = "";
	/** 玩家ID */
	private String playerID = "";
	private int playerIndex = 0;
	/** 玩家名字 */
	private String playerName = "";
	/** 行为涉及到的钱或物品的数量 */
	private int opGold = 0;
	/** 行为涉及的的物品类型 金币 晶石 动物彩票 抽奖奖券固定 */
	private int moneyType = LogConstant.MONEY_TYPE_GOLD;

	/** 操作类型 */
	private int operationType = 0;
	private int operationSubType = 0;
	/** 玩家金币数量 */
	private int playerGold = 0;
	/** 操作细节 */
	private String opDetail = "";
	/** 插入时间 */
	private Date createTime = null;

	private int platformType = PayConstant.HISTORY_PLATFORM_TYPE_E_SURFING;
	/** 表后缀 */
	private String tableNameSuffix = DateService.getTableSuffixByType("",
			DateService.DATE_BY_DAY);
	/** 表名前缀 gameid */
	private String tableNamePrefix = "";
	/** 俱乐部编号 */
	private int clubCode = 0;

	public PlayerOperationLog() {
		logID = UUIDGenerator.generatorUUID();
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		PlayerOperationLog.logger = logger;
	}

	public String getLogID() {
		return logID;
	}

	public void setLogID(String logID) {
		this.logID = logID;
	}

	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getOperationType() {
		return operationType;
	}

	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getOpDetail() {
		return opDetail;
	}

	public void setOpDetail(String opDetail) {
		this.opDetail = opDetail;
	}

	public String getTableNameSuffix() {
		return tableNameSuffix;
	}

	public void setTableNameSuffix(String tableNameSuffix) {
		this.tableNameSuffix = tableNameSuffix;
	}

	public String getTableNamePrefix() {
		return tableNamePrefix;
	}

	public void setTableNamePrefix(String tableNamePrefix) {
		this.tableNamePrefix = tableNamePrefix;
	}

	public int getOpGold() {
		return opGold;
	}

	public void setOpGold(int opGold) {
		this.opGold = opGold;
	}

	public int getPlayerGold() {
		return playerGold;
	}

	public void setPlayerGold(int playerGold) {
		this.playerGold = playerGold;
	}

	public int getOperationSubType() {
		return operationSubType;
	}

	public void setOperationSubType(int operationSubType) {
		this.operationSubType = operationSubType;
	}

	public int getMoneyType() {
		return moneyType;
	}

	public void setMoneyType(int moneyType) {
		this.moneyType = moneyType;
	}

	public int getPlatformType() {
		return platformType;
	}

	public void setPlatformType(int platformType) {
		this.platformType = platformType;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

}