package com.chess.common.bean;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class Message  extends NetObject{
   private String id = "";	
   private String playerID = "";
   //信息类型，0为系统信息
   private int infoType=0;
   //信息内容
   private String  msgContent="";
   //信息状态,0未读，1已读
   private int msgState =0;
   //信息写入日期
   private String msgDate = "";
   //信息读取日期
   private String msgReadDate = "";
   
   @Override
   public void serialize(ObjSerializer ar)
   {
	   super.serialize(ar);
	   id = ar.sString(id);	
	   playerID = ar.sString(playerID);
	   infoType= ar.sInt(infoType);
	   msgContent= ar.sString(msgContent);
	   msgState =ar.sInt(msgState);
	   msgDate = ar.sString(msgDate);
	   msgReadDate = ar.sString(msgReadDate);
   }
   
   public String getId() {
		return id;
   }
   
   public void setId(String id) {
		this.id = id;
   }
   
    public String getPlayerID() {
		return playerID;
	}
    
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
	
	public int getInfoType() {
		return infoType;
	}
	
	public void setInfoType(int infoType) {
		this.infoType = infoType;
	}
	public String getMsgContent() {
		return msgContent;
	}
	
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public int getMsgState() {
		return msgState;
	}
	
	public void setMsgState(int msgState) {
		this.msgState = msgState;
	}
	
	public String getMsgDate() {
		return msgDate;
	}
	public void setMsgDate(String msgDate) {
		this.msgDate = msgDate;
	}
	
	public String getMsgReadDate() {
		return msgReadDate;
	}
	
	public void setMsgReadDate(String msgReadDate) {
		this.msgReadDate = msgReadDate;
	}	

}
