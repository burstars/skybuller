package com.chess.common.bean;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class SystemConfigPara  extends NetObject {
	
	private int paraID=0;

	private int valueInt = 0;
	private int pro_1=0;
	private int pro_2=0;
	private int pro_3=0;
	private int pro_4=0;
	private int pro_5=0;
	private int isclient =0;
	private String valueStr="";
	private String paraDesc="";
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		paraID = ar.sInt(paraID);
		valueInt=ar.sInt(valueInt);
		pro_1 = ar.sInt(pro_1);
		pro_2 = ar.sInt(pro_2);
		pro_3 = ar.sInt(pro_3);
		pro_4 = ar.sInt(pro_4);
		pro_5 = ar.sInt(pro_5);
		isclient = ar.sInt(isclient);
		valueStr = ar.sString(valueStr);
		paraDesc = ar.sString(paraDesc);
	}
	
	public int getParaID() {
		return paraID;
	}
	public void setParaID(int paraID) {
		this.paraID = paraID;
	}
	public int getValueInt() {
		return valueInt;
	}
	public void setValueInt(int valueInt) {
		this.valueInt = valueInt;
	}
	public String getValueStr() {
		return valueStr;
	}
	public void setValueStr(String valueStr) {
		this.valueStr = valueStr;
	}
	public String getParaDesc() {
		return paraDesc;
	}
	public void setParaDesc(String paraDesc) {
		this.paraDesc = paraDesc;
	}
	public int getPro_1() {
		return pro_1;
	}
	public void setPro_1(int pro_1) {
		this.pro_1 = pro_1;
	}
	public int getPro_2() {
		return pro_2;
	}
	public void setPro_2(int pro_2) {
		this.pro_2 = pro_2;
	}
	public int getPro_3() {
		return pro_3;
	}
	public void setPro_3(int pro_3) {
		this.pro_3 = pro_3;
	}
	public int getIsclient() {
		return isclient;
	}
	public void setIsclient(int isclient) {
		this.isclient = isclient;
	}
	public int getPro_4() {
		return pro_4;
	}
	public void setPro_4(int pro_4) {
		this.pro_4 = pro_4;
	}

	public int getPro_5() {
		return pro_5;
	}

	public void setPro_5(int pro_5) {
		this.pro_5 = pro_5;
	}

	//
	 
}
