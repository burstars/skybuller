package com.chess.common.bean;

import java.sql.Blob;

import org.jacorb.idl.runtime.int_token;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class ShopGoods extends NetObject{
	/**游戏ID*/
	public String gameID ="";
	
	//商品id
	public String goodsId = "" ;
	public String goodsName = "";
	public byte[] goodsPicure = new byte[0];
	
	public String goodsRemark = "";
	public String goodClass = "";
	public int expenseScore = 0 ;
	public int openState = 0 ;
		
	
	public String getGameID() {
		return gameID;
	}


	public void setGameID(String gameID) {
		this.gameID = gameID;
	}


	public String getGoodsId() {
		return goodsId;
	}


	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}


	public String getGoodsName() {
		return goodsName;
	}


	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}


	public byte[] getGoodsPicure() {
		return goodsPicure;
	}


	public void setGoodsPicure(byte[] goodsPicure) {
		this.goodsPicure = goodsPicure;
	}


	public String getGoodsRemark() {
		return goodsRemark;
	}


	public void setGoodsRemark(String goodsRemark) {
		this.goodsRemark = goodsRemark;
	}


	public String getGoodClass() {
		return goodClass;
	}


	public void setGoodClass(String goodClass) {
		this.goodClass = goodClass;
	}


	public int getExpenseScore() {
		return expenseScore;
	}


	public void setExpenseScore(int expenseScore) {
		this.expenseScore = expenseScore;
	}


	public int getOpenState() {
		return openState;
	}


	public void setOpenState(int openState) {
		this.openState = openState;
	}


	@Override
	public void serialize(ObjSerializer ar)
	{
		gameID = ar.sString(gameID);
		goodsId = ar.sString(goodsId);
		goodsName = ar.sString(goodsName);
		goodsPicure = ar.sBytes(goodsPicure);
		goodsRemark = ar.sString(goodsRemark);
		goodClass = ar.sString(goodClass);
		expenseScore = ar.sInt(expenseScore);
		openState = ar.sInt(openState);
		
	}

}
