package com.chess.common.bean;

import java.util.Date;

import org.jacorb.idl.runtime.int_token;

import com.chess.common.UUIDGenerator;


public class SendCardLog {
	
	//ID
	private String sendCardId;
 
	/**玩家游戏中昵称**/
	private String sendCardPlayerId;

	/**赠送房卡来源
	 * 2，微信分享赠送
	 * 1，首次登陆赠送
	 * **/
	private int sendCardType;
	/** 赠送房卡数量*/
	private int  sendCardNum;
	
	private Date  sendCardDate ;
	
	private int sendCardCardId;
	
	public SendCardLog(){
		
		sendCardId=UUIDGenerator.generatorUUID();
	}
	
	public void setSendCardType(int sendCardType) {
		this.sendCardType = sendCardType;
	}
	public int getSendCardType() {
		return sendCardType;
	}
	public void setSendCardNum(int sendCardNum) {
		this.sendCardNum = sendCardNum;
	}
	public int getSendCardNum() {
		return sendCardNum;
	}
	public void setSendCardPlayerId(String sendCardPlayerId) {
		this.sendCardPlayerId = sendCardPlayerId;
	}
	public String getSendCardPlayerId() {
		return sendCardPlayerId;
	}
	public void setSendCardId(String sendCardId) {
		this.sendCardId = sendCardId;
	}
	public String getSendCardId() {
		return sendCardId;
	}
	public void setSendCardDate(Date sendCardDate) {
		this.sendCardDate = sendCardDate;
	}
	public Date getSendCardDate() {
		return sendCardDate;
	}

	public void setSendCardCardId(int sendCardCardId) {
		this.sendCardCardId = sendCardCardId;
	}

	public int getSendCardCardId() {
		return sendCardCardId;
	}

}
