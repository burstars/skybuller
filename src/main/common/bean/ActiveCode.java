package com.chess.common.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.core.net.msg.NetObject;

public class ActiveCode extends NetObject {

	private static Logger logger = LoggerFactory.getLogger(ActiveCode.class);
	/**
	 * ID
	 */
	private int id = 0;
	/**
	 * 活动码
	 */
	private String active_code = "";

	/**
	 * 是否有效
	 */
	private String is_value = "";

	/**
	 * 规则id
	 */
	private String rule_id = "";

	/**
	 * 开始时间
	 */
	private String start_date = "";

	/**
	 * 结束时间
	 */
	private String end_date = "";

	/**
	 * 房卡
	 */
	private int card = 0;

	/**
	 * 金币
	 */
	private int money = 0;

	/**
	 * 兑换码
	 */
	private String exchange_code = "";
	/**
	 * 用户id
	 */
	private int user_id = 0;
	
	public static Logger getLogger() {
		return logger;
	}
	public static void setLogger(Logger logger) {
		ActiveCode.logger = logger;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getActive_code() {
		return active_code;
	}
	public void setActive_code(String activeCode) {
		active_code = activeCode;
	}
	public String getIs_value() {
		return is_value;
	}
	public void setIs_value(String isValue) {
		is_value = isValue;
	}
	public String getRule_id() {
		return rule_id;
	}
	public void setRule_id(String ruleId) {
		rule_id = ruleId;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String startDate) {
		start_date = startDate;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String endDate) {
		end_date = endDate;
	}
	public int getCard() {
		return card;
	}
	public void setCard(int card) {
		this.card = card;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public String getExchange_code() {
		return exchange_code;
	}
	public void setExchange_code(String exchangeCode) {
		exchange_code = exchangeCode;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int userId) {
		user_id = userId;
	}

}