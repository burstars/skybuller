package com.chess.common.bean;

import java.util.Date;


public class AvgPeopleOnline
{
	
	/**记录ID*/
	private int id=0;
	/**人数*/
	private int peoNums=0;
	/**时间*/
	private Date time=null;
	/**费付人数（累计）*/
	private int totalPaidPeople=0;
	/**累计付费金额*/
	private int totalPaidMoney=0;
	/**登录用户总数*/
	private int totalLoginPlayer=0;
	/**平台标识，2电信*/
	private int platformType=0;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPeoNums() {
		return peoNums;
	}
	public void setPeoNums(int peoNums) {
		this.peoNums = peoNums;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public int getTotalPaidPeople() {
		return totalPaidPeople;
	}
	public void setTotalPaidPeople(int totalPaidPeople) {
		this.totalPaidPeople = totalPaidPeople;
	}
	public int getTotalPaidMoney() {
		return totalPaidMoney;
	}
	public void setTotalPaidMoney(int totalPaidMoney) {
		this.totalPaidMoney = totalPaidMoney;
	}
	public int getTotalLoginPlayer() {
		return totalLoginPlayer;
	}
	public void setTotalLoginPlayer(int totalLoginPlayer) {
		this.totalLoginPlayer = totalLoginPlayer;
	}
	public int getPlatformType() {
		return platformType;
	}
	public void setPlatformType(int platformType) {
		this.platformType = platformType;
	}
	

}
