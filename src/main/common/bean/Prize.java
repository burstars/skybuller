package com.chess.common.bean;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.chess.common.bean.unipay.PayCallbackReq;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;


public class Prize extends NetObject
{
	private static Logger logger = LoggerFactory.getLogger(Prize.class);
	/**奖品ID*/
	private int id=0;
	/**奖品名字*/
	private String prizeName="";
	/**注意，概率是想对1000000(一百万)来算   比如该奖品的中奖概率为百分之2   则在probability中写入 1000000*0.02=20000（2万），不要小数点，方便后面的计算 */
	private int  probability=0;
	/**奖品数量*/
	private int count=0;
//	/**图片名称*/
//	private String imageName="";
	
	
	
	
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		id=ar.sInt(id);
		prizeName=ar.sString(prizeName);
		probability=ar.sInt(probability);
		count=ar.sInt(count);
	//	imageName=ar.sString(imageName);
	}
	public String getPrizeName() {
		return prizeName;
	}
	public void setPrizeName(String prizeName) {
		this.prizeName = prizeName;
	}
	public int getProbability() {
		return probability;
	}
	public void setProbability(int probability) {
		this.probability = probability;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	

	public static void main(String[] args){
		
		List<Prize> prizeList=new ArrayList<Prize>();
		Prize p=new Prize();
		 try {
				Document document = new SAXReader().read("src/Prize.xml");
	             int i=0;
				for(Element prize : (List<Element>)document.getRootElement().elements()){ 
		            //取得txtbook节点下的name节点的内容 
					p.setId(Integer.parseInt(prize.element("id").getTextTrim()));
					p.setPrizeName(prize.element("name").getTextTrim());
					p.setProbability(Integer.parseInt(prize.element("probability").getTextTrim()));
//		            System.out.println(i+"."+prize.element("id").getTextTrim()+"___"+prize.element("name").getTextTrim()+"___"+prize.element("probability").getTextTrim()); 
		            logger.debug(i+"."+prize.element("id").getTextTrim()+"___"+prize.element("name").getTextTrim()+"___"+prize.element("probability").getTextTrim());
					i++; //原来这里少些了这一行，先补上 
		        } 
		    

			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 


	}
	
}
