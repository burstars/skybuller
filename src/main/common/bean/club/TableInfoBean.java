package com.chess.common.bean.club;

import java.util.ArrayList;
import java.util.List;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

/**
 * 牌桌信息的POJO类
 * 
 * /
 * 
 */
public class TableInfoBean extends NetObject {
	/** 牌桌类型，1空桌，2 私有桌，3 玩牌中 */
	public int type;
	public int templateIndex = 0;
	/** 牌桌编号 */
	public int tableNo;
	/** 牌桌ID */
	public String tableId;
	/** 几个人桌 */
	public int playerCount;
	/** 圈或局 */
	public Integer quantityType = -1;
	/** 实际的圈数或局数， 多少圈或局 */
	public Integer quantity = -1;
	/**按配置走圈数或局数*/
	public Integer quanNum = -1;
	/** 索引表示玩家坐的位置，值表示玩家的相关信息 */
	public List<TableUserInfo> players = new ArrayList<TableUserInfo>();
	/** 牌桌规则 */
	public List<Integer> tableRule = new ArrayList<Integer>();

	public String roomLevel="1";
	
	public TableInfoBean() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void serialize(ObjSerializer ar) {
		this.type = ar.sInt(type);
		this.templateIndex = ar.sInt(templateIndex);
		this.tableNo = ar.sInt(tableNo);
		this.tableId = ar.sString(tableId);
		this.playerCount = ar.sInt(playerCount);
		this.quantityType = ar.sInt(quantityType);
		this.quantity = ar.sInt(quantity);
		this.quanNum = ar.sInt(quanNum);
		this.players = (List<TableUserInfo>) ar.sObjArray(players);
		this.tableRule = (List<Integer>) ar.sIntArray(tableRule);
		this.roomLevel=ar.sString(roomLevel);
	}
}
