package com.chess.common.bean.club;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

/**
 * @description 牌桌上的用户对象
 * 
 * /
 * 
 */
public class TableUserInfo extends NetObject {
	public int tablePos = -1;
	public String playerName;
	public String playerId;
	public String headImgUrl;
	public int sex;

	@Override
	public void serialize(ObjSerializer ar) {
		this.tablePos = ar.sInt(tablePos);
		this.playerName = ar.sString(playerName);
		this.playerId = ar.sString(playerId);
		this.headImgUrl = ar.sString(headImgUrl);
		this.sex = ar.sInt(sex);
	}
}
