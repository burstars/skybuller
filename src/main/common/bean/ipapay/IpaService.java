package com.chess.common.bean.ipapay;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.json.JSONObject;

import com.chess.common.bean.MallHistory;
import com.chess.common.constant.GameConstant;
import com.chess.common.dao.IMallHistoryDAO;

public class IpaService {

	private static Logger logger = LoggerFactory.getLogger(IpaService.class);

	private IMallHistoryDAO mallHistoryDAO;

	public boolean verifyReceipt(String result, String orderNo) {
		// https://buy.itunes.apple.com/verifyReceipt //正式
		// https://sandbox.itunes.apple.com/verifyReceipt //测试
		String _url = "https://buy.itunes.apple.com/verifyReceipt";
		String _sandboxUrl = "https://sandbox.itunes.apple.com/verifyReceipt";

		int status = postIpaUrl(_url, result, orderNo);
		if (status == 21007) {
			status = postIpaUrl(_sandboxUrl, result, orderNo);
		}
		return status == 0;
	}

	private int postIpaUrl(String _url, String result, String orderNo) {
		int status = -1;
		try {
			URL url = new URL(_url);

			// make connection, use post mode
			HttpsURLConnection connection = (HttpsURLConnection) url
					.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setAllowUserInteraction(false);

			Map map = new HashMap();
			map.put("receipt-data", result);
			JSONObject jsonObject = new JSONObject(map);

			// Write the JSON query object to the connection output stream
			PrintStream ps = new PrintStream(connection.getOutputStream());
			ps.print(jsonObject.toString());
			ps.close();

			// Call the service
			BufferedReader br = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			// Extract response
			String str;
			StringBuffer sb = new StringBuffer();
			while ((str = br.readLine()) != null) {
				sb.append(str);
			}
			br.close();
			String response = sb.toString();

			JSONObject jresult = new JSONObject(response);

			// 验证结果
			status = jresult.getInt("status");

			if (0 == status) {
				String transaction_id;
				String product_id;

				if (jresult.has("receipt")) {
					transaction_id = jresult.getJSONObject("receipt")
							.getString("transaction_id"); // 交易ID
					product_id = jresult.getJSONObject("receipt").getString(
							"product_id"); // 道具ID
				} else {
					return -1;
				}

				logger.info("IOS IAP Json result: transaction_id = "
						+ transaction_id);
				logger.info("IOS IAP Json result: product_id = " + product_id);

				// 判断交易ID是否已经存在
				MallHistory transactionHistory = mallHistoryDAO
						.getPayHistoryByTransactionID(transaction_id);
				if (null != transactionHistory) {
					// 重复验证
					logger.error("交易ID已经存在 ");
					return -1;
				}

				MallHistory history = mallHistoryDAO
						.getPayHistoryByOrderID(orderNo);
				if (null == history) {
					// 记录不存在
					logger.error("交易记录不存在 ");
					return -1;
				}

				String item_str = GameConstant.IPA_PACKAGE_PREFIX
						+ history.getBuyItemID();
				if (!item_str.equals(product_id)) {
					// 道具ID不对
					logger.error("道具ID不对 ");
					return -1;
				}

				// 验证通过，将交易ID，写入支付记录
				history.setTransaction_id(transaction_id);
				mallHistoryDAO.updatePayHistory(history,"");

				logger.info("交易成功");
			}
		} catch (Exception ex) {
			logger.info("IOS IAP postIpaUrl ERROR:" + ex);
			return -1;
		}

		return status;
	}

	public IMallHistoryDAO getMallHistoryDAO() {
		return mallHistoryDAO;
	}

	public void setMallHistoryDAO(IMallHistoryDAO mallHistoryDAO) {
		this.mallHistoryDAO = mallHistoryDAO;
	}
}
