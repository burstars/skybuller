package com.chess.common.bean;

public class SensitiveWords {
	
	private String words;
	private String id ;

	public void setWords(String words) {
		this.words = words;
	}

	public String getWords() {
		return words;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
}
