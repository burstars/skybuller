package com.chess.common.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.chess.common.constant.PayConstant;

public class MallHistory implements Serializable {

	/** 充值历史编号  我方订单号*/
	private String payHistoryId;
	/** 玩家编号 */
	private String playerId;
	/** 数量 */
	private Integer amount=0;
	/** 订单号 平台方订单号*/
	private String orderNo;
	/** 充值时间 */
	private Date payTime;
	/** 标识充值入口（0 系统赠送  1 移动平台  2 ） */
	private Integer flagAccess = PayConstant.HISTORY_PLATFORM_TYPE_E_SURFING;
	/***充值记录的详细信息 json串 */
	private String payScript ="";
	/**玩家账号*/
	private String payAccount = "";
	/**0 充值回调通知接收到未实际发放到玩家账号  1 充值获得的商品以发放到玩家账号 */
	private int state = 0;
	private int buyItemID=3333;
	
	/**交易ID，针对IOS内支付*/
	private String transaction_id;
	
	/**玩家index*/
	private int playerIndex = 0;
	


	
	public MallHistory(){ }

	public MallHistory(String payHistoryId, String playerId, Integer amount, String orderNo, Date payTime, Integer flagAccess) {
		this.payHistoryId = payHistoryId;
		this.playerId = playerId;
		this.amount = amount;
		this.orderNo = orderNo;
		this.payTime = payTime;
		this.flagAccess = flagAccess;
	}
	

	public String getPayHistoryId() {
		return payHistoryId;
	}

	public void setPayHistoryId(String payHistoryId) {
		this.payHistoryId = payHistoryId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	public Integer getFlagAccess() {
		return flagAccess;
	}

	public void setFlagAccess(Integer flagAccess) {
		this.flagAccess = flagAccess;
	}

	public String getPayScript() {
		return payScript;
	}

	public void setPayScript(String payScript) {
		this.payScript = payScript;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getPayAccount() {
		return payAccount;
	}

	public void setPayAccount(String payAccount) {
		this.payAccount = payAccount;
	}
	/**解析到玩家账号*/
	public void decodePayAccount(){
		if(this.payScript!=null&&!"".equals(this.payScript)){ 
			JSONObject json = JSONObject.fromObject(this.payScript);
			if(json!=null&&json.has("account")){
				 this.payAccount = json.getString("account");
			} 
		}
		 
	}

	public int getBuyItemID() {
		return buyItemID;
	}

	public void setBuyItemID(int buyItemID) {
		this.buyItemID = buyItemID;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

}
