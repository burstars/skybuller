package com.chess.common.bean;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;



public class PlayerHuType  extends NetObject
{
	  
	//
	private String huID=""; 
	private String playerID="";
	private int huType=0;
	private int count=0;

	//
	public PlayerHuType()
	{
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
	}
	
	
	public String getPlayerID() {
		return playerID;
	}
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getHuID() {
		return huID;
	}

	public void setHuID(String huID) {
		this.huID = huID;
	}

	public int getHuType() {
		return huType;
	}
	public void setHuType(int huType) {
		this.huType = huType;
	}
}
