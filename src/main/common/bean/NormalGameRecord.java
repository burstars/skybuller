/**
 * Description:普通场每局的游戏记录
 */
package com.chess.common.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.chess.common.DateService;

public class NormalGameRecord {
	/** 记录ID */
	private String recordID = "";
	/** 房间ID */
	private String roomID = "";
	/** 房间号 */
	private int roomIndex;
	/** 房间类型 */
	private int roomType;
	/** 房主_ID */
	private int hostIndex;

	/** **************玩家1******************** */
	/** 玩家1_ID */
	private int player1Index;
	/** 玩家1_番型 */
	private int gameType1;
	/** 玩家1_分数 */
	private long score1;
	/** 玩家1_番数 */
	private int gameMultiple1;

	/** **************玩家2******************** */
	/** 玩家2_ID */
	private int player2Index;
	/** 玩家2_番型 */
	private int gameType2;
	/** 玩家2_分数 */
	private long score2;
	/** 玩家2_番数 */
	private int gameMultiple2;

	/** **************玩家3******************** */
	/** 玩家3_ID */
	private int player3Index;
	/** 玩家3_番型 */
	private int gameType3;
	/** 玩家3_分数 */
	private long score3;
	/** 玩家3_番数 */
	private int gameMultiple3;

	/** **************玩家4******************** */
	/** 玩家4_ID */
	private int player4Index;
	/** 玩家4_番型 */
	private int gameType4;
	/** 玩家4_分数 */
	private long score4;
	/** 玩家4_番数 */
	private int gameMultiple4;

	/** 开始时间 */
	private Date startTime;
	/** 结束时间 */
	private Date endTime;
	/** 开始时间字符串格式 */
	private String sStartTime;
	/** 结束时间字符串格式 */
	private String sEndTime;
	/** 表名后缀 */
	private String tableNameSuffix = DateService.getTableSuffixByType("", DateService.DATE_BY_DAY);;

	public String getRecordID() {
		return recordID;
	}

	public void setRecordID(String recordID) {
		this.recordID = recordID;
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public int getRoomIndex() {
		return roomIndex;
	}

	public void setRoomIndex(int roomIndex) {
		this.roomIndex = roomIndex;
	}

	public int getRoomType() {
		return roomType;
	}

	public void setRoomType(int roomType) {
		this.roomType = roomType;
	}

	public int getGameType1() {
		return gameType1;
	}

	public void setGameType1(int gameType1) {
		this.gameType1 = gameType1;
	}

	public long getScore1() {
		return score1;
	}

	public void setScore1(long score1) {
		this.score1 = score1;
	}

	public int getGameMultiple1() {
		return gameMultiple1;
	}

	public void setGameMultiple1(int gameMultiple1) {
		this.gameMultiple1 = gameMultiple1;
	}

	public int getGameType2() {
		return gameType2;
	}

	public void setGameType2(int gameType2) {
		this.gameType2 = gameType2;
	}

	public long getScore2() {
		return score2;
	}

	public void setScore2(long score2) {
		this.score2 = score2;
	}

	public int getGameMultiple2() {
		return gameMultiple2;
	}

	public void setGameMultiple2(int gameMultiple2) {
		this.gameMultiple2 = gameMultiple2;
	}

	public int getGameType3() {
		return gameType3;
	}

	public void setGameType3(int gameType3) {
		this.gameType3 = gameType3;
	}

	public long getScore3() {
		return score3;
	}

	public void setScore3(long score3) {
		this.score3 = score3;
	}

	public int getGameMultiple3() {
		return gameMultiple3;
	}

	public void setGameMultiple3(int gameMultiple3) {
		this.gameMultiple3 = gameMultiple3;
	}

	public int getGameType4() {
		return gameType4;
	}

	public void setGameType4(int gameType4) {
		this.gameType4 = gameType4;
	}

	public long getScore4() {
		return score4;
	}

	public void setScore4(long score4) {
		this.score4 = score4;
	}

	public int getGameMultiple4() {
		return gameMultiple4;
	}

	public void setGameMultiple4(int gameMultiple4) {
		this.gameMultiple4 = gameMultiple4;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
		this.sStartTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTime);
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
		this.sEndTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endTime);
	}

	public String getTableNameSuffix() {
		return tableNameSuffix;
	}

	public void setTableNameSuffix(String tableNameSuffix) {
		this.tableNameSuffix = tableNameSuffix;
	}

	public String getSStartTime() {
		return sStartTime;
	}

	public void setSStartTime(String startTime) {
		sStartTime = startTime;
	}

	public String getSEndTime() {
		return sEndTime;
	}

	public void setSEndTime(String endTime) {
		sEndTime = endTime;
	}

	public int getHostIndex() {
		return hostIndex;
	}

	public void setHostIndex(int hostIndex) {
		this.hostIndex = hostIndex;
	}

	public int getPlayer1Index() {
		return player1Index;
	}

	public void setPlayer1Index(int player1Index) {
		this.player1Index = player1Index;
	}

	public int getPlayer2Index() {
		return player2Index;
	}

	public void setPlayer2Index(int player2Index) {
		this.player2Index = player2Index;
	}

	public int getPlayer3Index() {
		return player3Index;
	}

	public void setPlayer3Index(int player3Index) {
		this.player3Index = player3Index;
	}

	public int getPlayer4Index() {
		return player4Index;
	}

	public void setPlayer4Index(int player4Index) {
		this.player4Index = player4Index;
	}
}
