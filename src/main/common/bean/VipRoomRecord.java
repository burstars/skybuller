/**
 * Description:VIP战绩的每锅记录
 */
package com.chess.common.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.chess.common.DateService;
import com.chess.common.StringUtil;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class VipRoomRecord extends NetObject {

	private String recordID = "";
	/** 房间ID */
	private String roomID = "";
	/** 房间号 */
	private int roomIndex = 0;
	/** 玩家ID */
	private String player1ID = "";
	/** 玩家总分 */
	private int score1 = 0;
	/** 下家玩家ID */
	private String player2Name = "";
	/** 下家总分 */
	private int score2 = 0;
	/** 对家玩家ID */
	private String player3Name = "";
	/** 对家总分 */
	private int score3 = 0;
	/** 上家玩家ID */
	private String player4Name = "";
	/** 上家总分 */
	private int score4 = 0;
	/** 上家玩家ID */
	private String player5Name = "";
	/** 上家总分 */
	private int score5 = 0;
	/** 上家玩家ID */
	private String player6Name = "";
	/** 上家总分 */
	private int score6 = 0;
	/** 房主ID */
	private String hostName = "";
	/** 房间开始时间 */
	private Date startTime;
	/** 房间结束时间 */
	private Date endTime;

	private String start;

	private String end;
	/** 表名后缀 */
	private String tableNameSuffix = DateService.getTableSuffixByType("",
			DateService.DATE_BY_DAY);
	/** 表名前缀 gameId */
	private String tableNamePrefix = "";

	/** 结束方式 */
	private String endWay = "";

	/** 俱乐部编号 */
	private int clubCode = 0;
	/** 玩家昵称 */
	private String player1Name = "";
	/** 玩家Index */
	private int player1Index = 0;
	/** 玩家Index */
	private int player2Index = 0;
	/** 玩家Index */
	private int player3Index = 0;
	/** 玩家Index */
	private int player4Index = 0;
	/** 玩家Index */
	private int player5Index = 0;
	/** 玩家Index */
	private int player6Index = 0;
	/** 玩家ID */
	private String player2ID = "";
	/** 玩家ID */
	private String player3ID = "";
	/** 玩家ID */
	private String player4ID = "";
	/** 玩家ID */
	private String player5ID = "";
	/** 玩家ID */
	private String player6ID = "";
	
	/** 是否是大赢家 */
	private int  winner1 = 0;
	/** 是否是大赢家 */
	private int  winner2 = 0;
	/** 是否是大赢家 */
	private int  winner3 = 0;
	/** 是否是大赢家 */
	private int  winner4 = 0;
	/** 是否是大赢家 */
	private int  winner5 = 0;
	/** 是否是大赢家 */
	private int  winner6 = 0;
	
	/** 玩家头像url */
	private String headimgurl1 = "";
	/** 玩家头像url */
	private String headimgurl2 = "";
	/** 玩家头像url */
	private String headimgurl3 = "";
	/** 玩家头像url */
	private String headimgurl4 = "";
	/** 玩家头像url */
	private String headimgurl5 = "";
	/** 玩家头像url */
	private String headimgurl6 = "";
	
	/** 玩法规则 */
	private String rules = "";
	/** 玩家数 */
	private int playerCount = 0;
	/** 耗卡数 */
	private int payCount = 0;
	/** 耗卡类型（局或圈） */
	private int quantityType = 0;
	/** 多少局、圈 */
	private int quantity = 0;
	/** 是否已读 */
	private int isRead = 0;
	
	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		roomID = ar.sString(roomID);
		roomIndex = ar.sInt(roomIndex);
		player1ID = ar.sString(player1ID);
		score1 = ar.sInt(score1);
		player2Name = ar.sString(player2Name);
		score2 = ar.sInt(score2);
		player3Name = ar.sString(player3Name);
		score3 = ar.sInt(score3);
		player4Name = ar.sString(player4Name);
		score4 = ar.sInt(score4);
		player5Name = ar.sString(player5Name);
		score5 = ar.sInt(score5);
		player6Name = ar.sString(player6Name);
		score6 = ar.sInt(score6);
		hostName = ar.sString(hostName);
		ar.sString(StringUtil.date2String(startTime));
		ar.sString(StringUtil.date2String(endTime));
		endWay = ar.sString(endWay);
		clubCode = ar.sInt(clubCode);
		
		player1Name = ar.sString(player1Name);
		player1Index = ar.sInt(player1Index);
		player2Index = ar.sInt(player2Index);
		player3Index = ar.sInt(player3Index);
		player4Index = ar.sInt(player4Index);
		player5Index = ar.sInt(player5Index);
		player6Index = ar.sInt(player6Index);
		player2ID = ar.sString(player2ID);
		player3ID = ar.sString(player3ID);
		player4ID = ar.sString(player4ID);
		player5ID = ar.sString(player5ID);
		player6ID = ar.sString(player6ID);
		winner1 = ar.sInt(winner1);
		winner2 = ar.sInt(winner2);
		winner3 = ar.sInt(winner3);
		winner4 = ar.sInt(winner4);
		winner5 = ar.sInt(winner5);
		winner6 = ar.sInt(winner6);
		headimgurl1 = ar.sString(headimgurl1);
		headimgurl2 = ar.sString(headimgurl2);
		headimgurl3 = ar.sString(headimgurl3);
		headimgurl4 = ar.sString(headimgurl4);
		headimgurl5 = ar.sString(headimgurl5);
		headimgurl6 = ar.sString(headimgurl6);
		rules = ar.sString(rules);
		playerCount = ar.sInt(playerCount);
		payCount = ar.sInt(payCount);
		quantityType = ar.sInt(quantityType);
		quantity = ar.sInt(quantity);
		isRead =ar.sInt(isRead);
		
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public int getRoomIndex() {
		return roomIndex;
	}

	public void setRoomIndex(int roomIndex) {
		this.roomIndex = roomIndex;
	}

	public String getPlayer1ID() {
		return player1ID;
	}

	public void setPlayer1ID(String player1ID) {
		this.player1ID = player1ID;
	}

	public int getScore1() {
		return score1;
	}

	public void setScore1(int score1) {
		this.score1 = score1;
	}

	public String getPlayer2Name() {
		return player2Name;
	}

	public void setPlayer2Name(String player2Name) {
		this.player2Name = player2Name;
	}

	public int getScore2() {
		return score2;
	}

	public void setScore2(int score2) {
		this.score2 = score2;
	}

	public String getPlayer3Name() {
		return player3Name;
	}

	public void setPlayer3Name(String player3Name) {
		this.player3Name = player3Name;
	}

	public int getScore3() {
		return score3;
	}

	public void setScore3(int score3) {
		this.score3 = score3;
	}

	public String getPlayer4Name() {
		return player4Name;
	}

	public void setPlayer4Name(String player4Name) {
		this.player4Name = player4Name;
	}

	public int getScore4() {
		return score4;
	}

	public void setScore4(int score4) {
		this.score4 = score4;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
		this.start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				.format(startTime);
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
		this.end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endTime);
	}

	public String getRecordID() {
		return recordID;
	}

	public void setRecordID(String recordID) {
		this.recordID = recordID;
	}

	public String getTableNameSuffix() {
		return tableNameSuffix;
	}

	public void setTableNameSuffix(String tableNameSuffix) {
		this.tableNameSuffix = tableNameSuffix;
	}

	public String getTableNamePrefix() {
		return tableNamePrefix;
	}

	public void setTableNamePrefix(String tableNamePrefix) {
		this.tableNamePrefix = tableNamePrefix;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getEndWay() {
		return endWay;
	}

	public void setEndWay(String endWay) {
		this.endWay = endWay;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public String getPlayer2ID() {
		return player2ID;
	}

	public void setPlayer2ID(String player2id) {
		player2ID = player2id;
	}

	public String getPlayer3ID() {
		return player3ID;
	}

	public void setPlayer3ID(String player3id) {
		player3ID = player3id;
	}

	public String getPlayer4ID() {
		return player4ID;
	}

	public void setPlayer4ID(String player4id) {
		player4ID = player4id;
	}

	public int getWinner1() {
		return winner1;
	}

	public void setWinner1(int winner1) {
		this.winner1 = winner1;
	}

	public int getWinner2() {
		return winner2;
	}

	public void setWinner2(int winner2) {
		this.winner2 = winner2;
	}

	public int getWinner3() {
		return winner3;
	}

	public void setWinner3(int winner3) {
		this.winner3 = winner3;
	}

	public int getWinner4() {
		return winner4;
	}

	public void setWinner4(int winner4) {
		this.winner4 = winner4;
	}

	public String getHeadimgurl1() {
		return headimgurl1;
	}

	public void setHeadimgurl1(String headimgurl1) {
		this.headimgurl1 = headimgurl1;
	}

	public String getHeadimgurl2() {
		return headimgurl2;
	}

	public void setHeadimgurl2(String headimgurl2) {
		this.headimgurl2 = headimgurl2;
	}

	public String getHeadimgurl3() {
		return headimgurl3;
	}

	public void setHeadimgurl3(String headimgurl3) {
		this.headimgurl3 = headimgurl3;
	}

	public String getHeadimgurl4() {
		return headimgurl4;
	}

	public void setHeadimgurl4(String headimgurl4) {
		this.headimgurl4 = headimgurl4;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}

	public int getPayCount() {
		return payCount;
	}

	public void setPayCount(int payCount) {
		this.payCount = payCount;
	}

	public int getQuantityType() {
		return quantityType;
	}

	public void setQuantityType(int quantityType) {
		this.quantityType = quantityType;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getIsRead() {
		return isRead;
	}

	public void setIsRead(int isRead) {
		this.isRead = isRead;
	}

	public String getPlayer1Name() {
		return player1Name;
	}

	public void setPlayer1Name(String player1Name) {
		this.player1Name = player1Name;
	}

	public int getPlayer1Index() {
		return player1Index;
	}

	public void setPlayer1Index(int player1Index) {
		this.player1Index = player1Index;
	}

	public int getPlayer2Index() {
		return player2Index;
	}

	public void setPlayer2Index(int player2Index) {
		this.player2Index = player2Index;
	}

	public int getPlayer3Index() {
		return player3Index;
	}

	public void setPlayer3Index(int player3Index) {
		this.player3Index = player3Index;
	}

	public int getPlayer4Index() {
		return player4Index;
	}

	public void setPlayer4Index(int player4Index) {
		this.player4Index = player4Index;
	}

	public String getPlayer5Name() {
		return player5Name;
	}

	public void setPlayer5Name(String player5Name) {
		this.player5Name = player5Name;
	}

	public int getScore5() {
		return score5;
	}

	public void setScore5(int score5) {
		this.score5 = score5;
	}

	public String getPlayer6Name() {
		return player6Name;
	}

	public void setPlayer6Name(String player6Name) {
		this.player6Name = player6Name;
	}

	public int getScore6() {
		return score6;
	}

	public void setScore6(int score6) {
		this.score6 = score6;
	}

	public int getPlayer5Index() {
		return player5Index;
	}

	public void setPlayer5Index(int player5Index) {
		this.player5Index = player5Index;
	}

	public int getPlayer6Index() {
		return player6Index;
	}

	public void setPlayer6Index(int player6Index) {
		this.player6Index = player6Index;
	}

	public String getPlayer5ID() {
		return player5ID;
	}

	public void setPlayer5ID(String player5id) {
		player5ID = player5id;
	}

	public String getPlayer6ID() {
		return player6ID;
	}

	public void setPlayer6ID(String player6id) {
		player6ID = player6id;
	}

	public int getWinner5() {
		return winner5;
	}

	public void setWinner5(int winner5) {
		this.winner5 = winner5;
	}

	public int getWinner6() {
		return winner6;
	}

	public void setWinner6(int winner6) {
		this.winner6 = winner6;
	}

	public String getHeadimgurl5() {
		return headimgurl5;
	}

	public void setHeadimgurl5(String headimgurl5) {
		this.headimgurl5 = headimgurl5;
	}

	public String getHeadimgurl6() {
		return headimgurl6;
	}

	public void setHeadimgurl6(String headimgurl6) {
		this.headimgurl6 = headimgurl6;
	}

	
	
}
