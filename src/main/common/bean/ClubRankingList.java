package com.chess.common.bean; 

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

/** 
 * @author 
 * @version 创建时间：2018-9-6
 * 类说明 :俱乐部排名
 */
public class ClubRankingList extends NetObject{
	
	public String playerId = "";
	public int clubCode = 0;
	public int gameCount = 0;
	public int winnerCount = 0;
	public int gunnerCount = 0;
	public int fangkaCount = 0;
	public int state = 0;
	public String playerIndex = "";
	public String playerName = "";
	public String headUrl = "";

	@Override
	public void serialize(ObjSerializer ar)
	{
		playerId=ar.sString(playerId);
		clubCode=ar.sInt(clubCode);
		gameCount = ar.sInt(gameCount);
		winnerCount = ar.sInt(winnerCount);
		gunnerCount = ar.sInt(gunnerCount);
		fangkaCount = ar.sInt(fangkaCount);
		state=ar.sInt(state);
		playerIndex=ar.sString(playerIndex);
		playerName=ar.sString(playerName);
		headUrl=ar.sString(headUrl);
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public int getGameCount() {
		return gameCount;
	}

	public void setGameCount(int gameCount) {
		this.gameCount = gameCount;
	}

	public int getWinnerCount() {
		return winnerCount;
	}

	public void setWinnerCount(int winnerCount) {
		this.winnerCount = winnerCount;
	}

	public int getGunnerCount() {
		return gunnerCount;
	}

	public void setGunnerCount(int gunnerCount) {
		this.gunnerCount = gunnerCount;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(String playerIndex) {
		this.playerIndex = playerIndex;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	public int getFangkaCount() {
		return fangkaCount;
	}

	public void setFangkaCount(int fangkaCount) {
		this.fangkaCount = fangkaCount;
	}

}
 