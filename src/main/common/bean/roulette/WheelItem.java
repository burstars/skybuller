package com.chess.common.bean.roulette;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class WheelItem extends NetObject
{
	public int item_id;
	public int item_count;
	public int item_price;
	public String item_name;
	public int item_percentage;
	
	public WheelItem()
	{
		item_id=0;
		item_count=0;
		item_price=0;
		item_name="";
		item_percentage=0;
	}
	
	public void serialize(ObjSerializer ar)
	{
		item_id=ar.sInt(item_id);
		item_count=ar.sInt(item_count);
		item_price=ar.sInt(item_price);
		item_name=ar.sString(item_name);
		item_percentage=ar.sInt(item_percentage);
    }
}
