package com.chess.common.bean;

 
import org.jacorb.idl.runtime.int_token;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

/**
 * 个人物品信息
 * @author  2018-01-18
 *
 */
public class WelFare extends NetObject
{
	public int welfare_id=0;
	public String welfard_name = "";
	public int welfare_type = -1;
	public int part_no =0;
	public int op_money = 0;
	public int money_type = 0;
	public int win_percent_1 =0;
	public int win_percent_2 =0;
	public int welfare_state =-1;
	
	//
	public WelFare()
	{
	}
	
	public int getWelfare_id() {
		return welfare_id;
	}

	public void setWelfare_id(int welfareId) {
		welfare_id = welfareId;
	}

	public String getWelfard_name() {
		return welfard_name;
	}

	public void setWelfard_name(String welfardName) {
		welfard_name = welfardName;
	}

	public int getWelfare_type() {
		return welfare_type;
	}

	public void setWelfare_type(int welfareType) {
		welfare_type = welfareType;
	}

	public int getPart_no() {
		return part_no;
	}

	public void setPart_no(int partNo) {
		part_no = partNo;
	}

	public int getOp_money() {
		return op_money;
	}

	public void setOp_money(int opMoney) {
		op_money = opMoney;
	}

	public int getMoney_type() {
		return money_type;
	}

	public void setMoney_type(int moneyType) {
		money_type = moneyType;
	}

	public int getWin_percent_1() {
		return win_percent_1;
	}

	public void setWin_percent_1(int winPercent_1) {
		win_percent_1 = winPercent_1;
	}

	public int getWin_percent_2() {
		return win_percent_2;
	}

	public void setWin_percent_2(int winPercent_2) {
		win_percent_2 = winPercent_2;
	}

	public int getWelfare_state() {
		return welfare_state;
	}

	public void setWelfare_state(int welfareState) {
		welfare_state = welfareState;
	}
	
}
