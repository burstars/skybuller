package com.chess.common.bean;

/**道具消耗/购买 统计基础类*/
public class ItemOpStaBase {
	private int base_id=101;
	private String name="超值礼包";
	private int price=10;
	/**道具消耗或购买次数*/
	private int useNum = 0;
	private int total = 0;
	
	public int getBase_id() {
		return base_id;
	}
	public void setBase_id(int base_id) {
		this.base_id = base_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getUseNum() {
		return useNum;
	}
	public void setUseNum(int useNum) {
		this.useNum = useNum;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
}
