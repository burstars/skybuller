/**
 * Description:VIP战绩的每局记录
 */
package com.chess.common.bean;

import java.util.Date;
import com.chess.common.DateService;
import com.chess.core.net.msg.NetObject;
public class RecordBean extends NetObject {

	private String record_Id = "";
	private String room_Id = "";
	private int room_Index = 0;
	private String player_Id = "";
	private int player_Index = 0;
	private String player_Name = "";
	private int score = 0;
	private int host_Index = 0;
	private String host_Name = "";
	private Date start_Time = null;
	private Date end_Time = null;
	private String end_Way = "";
	private String param01 = "";
	private String param02 = "";
	private String param03 = "";
	private String param04 = "";
	private String param05 = "";
	/** 表名后缀 */
	private String tableNameSuffix = DateService.getTableSuffixByType("", DateService.DATE_BY_DAY);
	/** 表名前缀 gameId */
	private String tableNamePrefix = "";
	
	public String getTableNamePrefix() {
		return tableNamePrefix;
	}

	public void setTableNamePrefix(String tableNamePrefix) {
		this.tableNamePrefix = tableNamePrefix;
	}

	public String getTableNameSuffix() {
		return tableNameSuffix;
	}

	public void setTableNameSuffix(String tableNameSuffix) {
		this.tableNameSuffix = tableNameSuffix;
	}

	public String getRecord_Id() {
		return record_Id;
	}


	public void setRecord_Id(String record_Id) {
		this.record_Id = record_Id;
	}


	public String getRoom_Id() {
		return room_Id;
	}


	public void setRoom_Id(String room_Id) {
		this.room_Id = room_Id;
	}


	public int getRoom_Index() {
		return room_Index;
	}


	public void setRoom_Index(int room_Index) {
		this.room_Index = room_Index;
	}


	public String getPlayer_Id() {
		return player_Id;
	}


	public void setPlayer_Id(String player_Id) {
		this.player_Id = player_Id;
	}


	public int getPlayer_Index() {
		return player_Index;
	}


	public void setPlayer_Index(int player_Index) {
		this.player_Index = player_Index;
	}


	public String getPlayer_Name() {
		return player_Name;
	}


	public void setPlayer_Name(String player_Name) {
		this.player_Name = player_Name;
	}


	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	public int getHost_Index() {
		return host_Index;
	}


	public void setHost_Index(int host_Index) {
		this.host_Index = host_Index;
	}


	public String getHost_Name() {
		return host_Name;
	}


	public void setHost_Name(String host_Name) {
		this.host_Name = host_Name;
	}


	public Date getStart_Time() {
		return start_Time;
	}


	public void setStart_Time(Date start_Time) {
		this.start_Time = start_Time;
	}


	public Date getEnd_Time() {
		return end_Time;
	}


	public void setEnd_Time(Date end_Time) {
		this.end_Time = end_Time;
	}


	public String getEnd_Way() {
		return end_Way;
	}


	public void setEnd_Way(String end_Way) {
		this.end_Way = end_Way;
	}


	public String getParam01() {
		return param01;
	}


	public void setParam01(String param01) {
		this.param01 = param01;
	}


	public String getParam02() {
		return param02;
	}


	public void setParam02(String param02) {
		this.param02 = param02;
	}


	public String getParam03() {
		return param03;
	}


	public void setParam03(String param03) {
		this.param03 = param03;
	}


	public String getParam04() {
		return param04;
	}


	public void setParam04(String param04) {
		this.param04 = param04;
	}


	public String getParam05() {
		return param05;
	}


	public void setParam05(String param05) {
		this.param05 = param05;
	}
	
	
}
