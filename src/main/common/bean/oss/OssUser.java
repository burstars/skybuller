package com.chess.common.bean.oss;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OssUser implements Serializable {

	private static final long serialVersionUID = -6899365052450544874L;
	/** 帐号 */
	private String username;
	/** 密码 */
	private String password;
	/** 最后一次登录时间 */
	private Date lastLoginTime;
	/** 最后一次登录IP */
	private String lastLoginIp;
	/** 登录次数 */
	private Integer loginNum;
	/** 创建时间 */
	private Date createTime;
	/** 实名 */
	private String realnames;
	/** 状态 */
	private Integer status;	
	/** 角色类型 */
	private Integer roleType;
	
	/**已分配角色*/
	private List<OssRole> ossRoleList;


	public String getRealnames() {
		return realnames;
	}

	public void setRealnames(String realnames) {
		this.realnames = realnames;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public Integer getLoginNum() {
		return loginNum;
	}

	public void setLoginNum(Integer loginNum) {
		this.loginNum = loginNum;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public List<OssRole> getOssRoleList() {
		return ossRoleList;
	}

	public void setOssRoleList(List<OssRole> ossRoleList) {
		this.ossRoleList = ossRoleList;
	}

	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}


}
