package com.chess.common.bean.oss;

import java.io.Serializable;

public class OssMonitor implements Serializable{
	/**  */
	private Long ossMonitorID;
	/** JVM可用内存 */
	private Long totalMemory;
	/** JVM剩余内存 */
	private Long freeMemory;
	/** JVM最大可使用内存 */
	private Long maxMemory;
	/** OS总的物理内存 */
	private Long osTotalMemorySize;
	/** OS剩余的物理内存 */
	private Long ossFreePhysicalMemory;
	/** 线程总数 */
	private Integer totalThread;
	/** cpu使用率 */
	private double cupRatio;
	
	public Long getOssMonitorID() {
		return ossMonitorID;
	}
	public void setOssMonitorID(Long ossMonitorID) {
		this.ossMonitorID = ossMonitorID;
	}
	public Long getTotalMemory() {
		return totalMemory;
	}
	public void setTotalMemory(Long totalMemory) {
		this.totalMemory = totalMemory;
	}
	public Long getFreeMemory() {
		return freeMemory;
	}
	public void setFreeMemory(Long freeMemory) {
		this.freeMemory = freeMemory;
	}
	public Long getMaxMemory() {
		return maxMemory;
	}
	public void setMaxMemory(Long maxMemory) {
		this.maxMemory = maxMemory;
	}
	public Long getOsTotalMemorySize() {
		return osTotalMemorySize;
	}
	public void setOsTotalMemorySize(Long osTotalMemorySize) {
		this.osTotalMemorySize = osTotalMemorySize;
	}
	public Long getOssFreePhysicalMemory() {
		return ossFreePhysicalMemory;
	}
	public void setOssFreePhysicalMemory(Long ossFreePhysicalMemory) {
		this.ossFreePhysicalMemory = ossFreePhysicalMemory;
	}
	public Integer getTotalThread() {
		return totalThread;
	}
	public void setTotalThread(Integer totalThread) {
		this.totalThread = totalThread;
	}
	public double getCupRatio() {
		return cupRatio;
	}
	public void setCupRatio(double cupRatio) {
		this.cupRatio = cupRatio;
	}





}
