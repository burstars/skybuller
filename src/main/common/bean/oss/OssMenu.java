package com.chess.common.bean.oss;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OssMenu  implements Serializable{
	
	private static final long serialVersionUID = -1369142696282669712L;
	/** 功能编号 */
	private String ossMenuID;
	/** 功能名称 */
	private String name;
	/**  */
	private String parentMenuID;
	/** 功能URL */
	private String pageUrl;
	
	/**下级菜单，用于菜单树*/
	private List childOssMenu = new ArrayList();

	public List getChildOssMenu() {
		return childOssMenu;
	}

	public void setChildOssMenu(List childOssMenu) {
		this.childOssMenu = childOssMenu;
	}

	public String getOssMenuID() {
		return ossMenuID;
	}

	public void setOssMenuID(String ossMenuID) {
		this.ossMenuID = ossMenuID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentMenuID() {
		return parentMenuID;
	}

	public void setParentMenuID(String parentMenuID) {
		this.parentMenuID = parentMenuID;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}



}
