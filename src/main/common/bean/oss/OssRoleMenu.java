package com.chess.common.bean.oss;

import java.io.Serializable;

public class OssRoleMenu implements Serializable {
	
	/** 角色ID */
	private Integer ossRoleID;
	/** 功能ID */
	private String ossMenuID;

	public Integer getOssRoleID() {
		return ossRoleID;
	}

	public void setOssRoleID(Integer ossRoleID) {
		this.ossRoleID = ossRoleID;
	}

	public String getOssMenuID() {
		return ossMenuID;
	}

	public void setOssMenuID(String ossMenuID) {
		this.ossMenuID = ossMenuID;
	}




}
