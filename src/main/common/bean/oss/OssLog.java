package com.chess.common.bean.oss;

import java.io.Serializable;
import java.util.Date;

public class OssLog implements Serializable{

	private static final long serialVersionUID = 9090408414705265165L;
	/** 系统操作日志ID */
	private String ossLogID;
	/** 账号 */
	private String account;
	/** 管理员名字 */
	private String username;
	/** 入口（0本地，1ossCenter） */
	private Integer entrance;
	/** 录登IP */
	private String loginIp;
	/** 操作内容 */
	private String operation;
	/** 操作类型 */
	private String operationType;
	/** 操作日期 */
	private Date operationDate;
	/** 注备 */
	private String remark;
	
	public OssLog(){
		
	}
	
	public OssLog(String operation,String operationType){
		this.operation = operation;
		this.operationType = operationType;
	}

	public String getOssLogID() {
		return ossLogID;
	}

	public void setOssLogID(String ossLogID) {
		this.ossLogID = ossLogID;
	}



	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getEntrance() {
		return entrance;
	}

	public void setEntrance(Integer entrance) {
		this.entrance = entrance;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
