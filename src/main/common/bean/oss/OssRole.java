package com.chess.common.bean.oss;

import java.io.Serializable;


public class OssRole implements Serializable{
	
	private static final long serialVersionUID = -8306538055639627867L;
	/**  */
	private Integer ossRoleID;
	/** 描述 */
	private String roleDesc;
	/** 编码 */
	private String roleCode;
	/** 角色名 */
	private String roleName;
	/** 类型 */
	private Integer roleType;
	
	public Integer getOssRoleID() {
		return ossRoleID;
	}
	public void setOssRoleID(Integer ossRoleID) {
		this.ossRoleID = ossRoleID;
	}
	public String getRoleDesc() {
		return roleDesc;
	}
	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getRoleType() {
		return roleType;
	}
	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}



}
