package com.chess.common.bean.oss;

import java.io.Serializable;

public class OssUserRole implements Serializable {
	/** 用户名 */
	private String username;
	/** 角色ID */
	private Integer ossRoleID;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getOssRoleID() {
		return ossRoleID;
	}

	public void setOssRoleID(Integer ossRoleID) {
		this.ossRoleID = ossRoleID;
	}

}
