package com.chess.common.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户登录详细信息
 * @date 
 * @version 
 */
@SuppressWarnings("unused")
public class LoginLogInfo implements Serializable{
	
	private static final long serialVersionUID = -4377901416022853361L;

	/** 玩家编号 */
	private String playerID;
	
	/** 用户帐号 */
	private String userName;
	
	/** 玩家名称 */
	private String name;
				
	/**当然重复登录次数*/
	private Integer loginAmount;
	/**该玩家的注册时间*/
	private Date registerTime ;
	/**在线时长 以分钟计算*/
	private Integer loginMinTime ;
	
	public String getPlayerID() {
		return playerID;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public Integer getLoginAmount() {
		return loginAmount;
	}

	public void setLoginAmount(Integer loginAmount) {
		this.loginAmount = loginAmount;
	}


	public Integer getLoginMinTime() {
		return loginMinTime;
	}

	public void setLoginMinTime(Integer loginMinTime) {
		this.loginMinTime = loginMinTime;
	}

	public Date getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}
	
	
	
	
	
	
	
	
	
	
	
}
