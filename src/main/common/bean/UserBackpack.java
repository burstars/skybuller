package com.chess.common.bean;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;



public class UserBackpack  extends NetObject
{
	  
	//
	private String playerID="";
	private String itemID="";
	private int itemBaseID=0;
	private int itemNum=0;
	private String itemName="";
	private String description="";
	//
	public UserBackpack()
	{
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		itemBaseID=ar.sInt(itemBaseID);
		itemNum=ar.sInt(itemNum);
	}
	
	
	public String getPlayerID() {
		return playerID;
	}
	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
	public String getItemID() {
		return itemID;
	}
	public void setItemID(String itemID) {
		this.itemID = itemID;
	}
	public int getItemBaseID() {
		return itemBaseID;
	}
	public void setItemBaseID(int itemBaseID) {
		this.itemBaseID = itemBaseID;
	}
	public int getItemNum() {
		return itemNum;
	}
	public void setItemNum(int itemNum) {
		this.itemNum = itemNum;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
