package com.chess.common.bean;

 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;



public class GameMap extends NetObject
{
	//
	List<Integer> pets=new ArrayList<Integer>();
	//
	public GameMap()
	{
		
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
	 pets=(List<Integer>)ar.sIntArray(pets);
    }
	//
	public void rand_gen_pets(int num)
	{
		if(num<=0 || num>22)
			num=22;
		//
		Map<Integer,Integer> pos_map=new HashMap<Integer,Integer>();
		//
		int trap_num=0;
		//
		for(int i=0;i<num;i++)
		{
			int pet=(int)(5*Math.random());
			int trap=(int)(100*Math.random());
			//
			if(trap>30)
			{
				if(trap_num<num/4)
				{
					pet=101;//陷阱
					trap_num++;
				}
			}
			//
			
			//
			
			Integer pos=0;
			//
			boolean found_pos=false;
			//最多200次
			for(int j=0;j<50;j++)
			{
				pos=(int)(22*Math.random());
				Integer p=pos_map.get(pos);
				if(p==null)
				{
					found_pos=true;
					pos_map.put(pos,pos);
					break;
				}
			}
			//
			if(found_pos==false)
			{
				for(int j=0;j<22;j++)
				{
					Integer p=pos_map.get(j);
					if(p==null)
					{
						pos=j;
						found_pos=true;
						pos_map.put(pos,pos);
						break;
					}
				}
			}
			//
			if(found_pos)
			{
				int mix=pet|(pos<<8);
			//
				pets.add(mix);
			}
		}
		//
		pos_map.clear();
		pos_map=null;
		//
		//String xxxx="";
		//
		//for(int i=0;i<pets.size();i++)
		//{
		//	Integer dd=(pets.get(i)>>8)& 0xff;
		//	xxxx +=","+dd;
			
		//}
		//System.out.println("maps="+xxxx);
	}
	 
	public void guide_gen_pets(int num,int mapIndex)
	{
		if(num<=0 || num>22)
			num=22;
		//
		int pet=(int)(5*Math.random());
		//
		for(int i=0;i<num;i++)
		{
			if(mapIndex==0){//第一张地图
				if(i==8){
					int mix=pet|(i<<8);
					pets.add(mix);
				}
			}else if(mapIndex==1){//第二张地图
				if(i>6&&i<10){
					int mix=pet|(i<<8);
					pets.add(mix);
				}
			}
		}
		//
		
	}
}
