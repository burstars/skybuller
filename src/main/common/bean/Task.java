package com.chess.common.bean;
/**任务基础类 */
public class Task {
	
	/** 任务编号 */
	private Integer taskID;

	/**任务难度 未使用**/
	private Integer difficu = 1;
	
	/** 任务名称 */
	private String name;
	/** 任务描述 */
	private String description;
	/** 任务详细描述 */
	private String detailDesc;

	/** 任务类别  任务类型0.累积任务 1.每日任务 2.每日随机任务（每天只开放一个）*/
	private Integer type = 1;

	/** 奖励信息*/
	private String reward;
	
	/** 触发的新任务基础id序列，逗号分割，类似"2001#3333#4005"*/
	private String triggerTaskID;

	//任务标准位，1表示这个任务由客户端判断是否已经完成，其他标准位待添加  未使用
	private Integer taskFlag;
	
	/** 任务最大完成数量，用于活跃度任务  未使用*/
	private Integer maxFinish;
	
	//如果是引导任务，这个显示这个任务在第一页，2个任务一页  未使用
	private Integer taskIndex=0;	
	private Integer nextTaskID=0;//引导任务是单线的，只有一个后续任务  未使用
	
	
	
	public Integer getTaskID() {
		return taskID;
	}
	public void setTaskID(Integer taskID) {
		this.taskID = taskID;
	}
	public Integer getDifficu() {
		return difficu;
	}
	public void setDifficu(Integer difficu) {
		this.difficu = difficu;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDetailDesc() {
		return detailDesc;
	}
	public void setDetailDesc(String detailDesc) {
		this.detailDesc = detailDesc;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getReward() {
		return reward;
	}
	public void setReward(String reward) {
		this.reward = reward;
	}
	public String getTriggerTaskID() {
		return triggerTaskID;
	}
	public void setTriggerTaskID(String triggerTaskID) {
		this.triggerTaskID = triggerTaskID;
	}
	public Integer getTaskFlag() {
		return taskFlag;
	}
	public void setTaskFlag(Integer taskFlag) {
		this.taskFlag = taskFlag;
	}
	public Integer getMaxFinish() {
		return maxFinish;
	}
	public void setMaxFinish(Integer maxFinish) {
		this.maxFinish = maxFinish;
	}
	public Integer getTaskIndex() {
		return taskIndex;
	}
	public void setTaskIndex(Integer taskIndex) {
		this.taskIndex = taskIndex;
	}
	public Integer getNextTaskID() {
		return nextTaskID;
	}
	public void setNextTaskID(Integer nextTaskID) {
		this.nextTaskID = nextTaskID;
	}
}
