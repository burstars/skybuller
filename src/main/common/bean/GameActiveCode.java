package com.chess.common.bean;

import com.chess.common.StringUtil;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class GameActiveCode extends NetObject {
	
	private int ID = 0;
	private String active_code ="";
	private String is_value = "";
	private String rule_id = "";
	private String start_date ="";
	private String end_date = "";
	private int card = 0;
	private int money =0 ;
	private String exchange_code = "";
	private int user_id = 0;
	
	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		ID = ar.sInt(ID);
		card = ar.sInt(card);
		money = ar.sInt(money);
		user_id = ar.sInt(user_id);
		active_code = ar.sString(active_code);
		is_value  = ar.sString(is_value);
		rule_id  = ar.sString(rule_id);
		start_date = ar.sString(start_date);
		end_date = ar.sString(end_date);
		exchange_code = ar.sString(exchange_code);
	}

	
	

	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getActive_code() {
		return active_code;
	}
	public void setActive_code(String activeCode) {
		active_code = activeCode;
	}
	public String getIs_value() {
		return is_value;
	}
	public void setIs_value(String isValue) {
		is_value = isValue;
	}
	public String getRule_id() {
		return rule_id;
	}
	public void setRule_id(String ruleId) {
		rule_id = ruleId;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String startDate) {
		start_date = startDate;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String endDate) {
		end_date = endDate;
	}
	public int getCard() {
		return card;
	}
	public void setCard(int card) {
		this.card = card;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public String getExchange_code() {
		return exchange_code;
	}
	public void setExchange_code(String exchangeCode) {
		exchange_code = exchangeCode;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int userId) {
		user_id = userId;
	}
}
