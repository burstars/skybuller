package com.chess.common.bean;

 
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;



public class MallItem extends NetObject
{
	  
	private int base_id=0;
	private String name="";
	private int price=0;
	private int property_1=0;
	private int property_2=0;
	private int property_3=0;
	private int property_4=0;
	private String description="";
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		base_id=ar.sInt(base_id);
		name=ar.sString(name);
		price=ar.sInt(price);
		property_1=ar.sInt(property_1);
		property_2=ar.sInt(property_2);
		property_3=ar.sInt(property_3);
		property_4=ar.sInt(property_4);
		description=ar.sString(description);
	}
	
	//
	public MallItem()
	{
	}

	public int getBase_id() {
		return base_id;
	}

	public void setBase_id(int base_id) {
		this.base_id = base_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getProperty_1() {
		return property_1;
	}

	public void setProperty_1(int property_1) {
		this.property_1 = property_1;
	}

	public int getProperty_2() {
		return property_2;
	}

	public void setProperty_2(int property_2) {
		this.property_2 = property_2;
	}

	public int getProperty_3() {
		return property_3;
	}

	public void setProperty_3(int property_3) {
		this.property_3 = property_3;
	}

	public int getProperty_4() {
		return property_4;
	}

	public void setProperty_4(int property_4) {
		this.property_4 = property_4;
	}

 

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
 
 
}
