package com.chess.common.bean;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class ClubTemplate extends NetObject {

	private static Logger logger = LoggerFactory.getLogger(ClubTemplate.class);

	/**
	 * 主键id
	 */
	private String id = "";
	
	/**
	 * 模板编号
	 */
	private Integer templateIndex = 0;
	/**
	 * 俱乐部编号
	 */
	private Integer clubCode  = 0;
	
	/**
	 * 玩牌人数
	 */
	private Integer playerCount  = 0;
	/**
	 * 圈或局
	 */
	private Integer quantityType = -1;
	/**
	 * 多少圈或局
	 */
	private Integer quantity = -1;

	/**
	 * 玩法规则
	 */
	private String rules = "";
	
	/**
	 * 创建时间
	 */
	private Date createTime = null; 
	
	private String roomLevel="";
	/**
	 * 备用列1
	 */
	private String param01 = "";
	
	/**
	 * 备用列2
	 */
	private String param02 = "";
	/**
	 * 备用列3
	 */
	private String param03 = "";
	
	@Override
	public void serialize(ObjSerializer ar) {
		templateIndex = ar.sInt(templateIndex);
		clubCode = ar.sInt(clubCode);
		playerCount = ar.sInt(playerCount);
		quantityType = ar.sInt(quantityType);
		quantity = ar.sInt(quantity);
		rules = ar.sString(rules);
		createTime = ar.sDate(createTime);
		roomLevel = ar.sString(roomLevel);
		param01 = ar.sString(param01);
		param02 = ar.sString(param02);
		param03 = ar.sString(param03);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public Integer getTemplateInDex() {
		return templateIndex;
	}
	public void setTemplateInDex(Integer templateInDex) {
		this.templateIndex = templateInDex;
	}
	public Integer getClubCode() {
		return clubCode;
	}
	public void setClubCode(Integer clubCode) {
		this.clubCode = clubCode;
	}
	public Integer getPlayerCount() {
		return playerCount;
	}
	public void setPlayerCount(Integer playerCount) {
		this.playerCount = playerCount;
	}
	public Integer getQuantityType() {
		return quantityType;
	}
	public void setQuantityType(Integer quantityType) {
		this.quantityType = quantityType;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getRules() {
		return rules;
	}
	public void setRules(String rules) {
		this.rules = rules;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getRoomLevel() {
		return roomLevel;
	}

	public void setRoomLevel(String roomLevel) {
		this.roomLevel = roomLevel;
	}

	public String getParam01() {
		return param01;
	}
	public void setParam01(String param01) {
		this.param01 = param01;
	}
	public String getParam02() {
		return param02;
	}
	public void setParam02(String param02) {
		this.param02 = param02;
	}
	public String getParam03() {
		return param03;
	}
	public void setParam03(String param03) {
		this.param03 = param03;
	}
	
	
}