package com.chess.common.bean;

import java.util.ArrayList;
import java.util.List;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

//对战发给客户端的
public class SimpleRecord  extends NetObject {

	
	public int room_Indsx = 0;
	public String gameStartTime = "";
	public String endWay = "";
	public int hostIndex = 0;
	public List<RoomRecord> roomRecords = new ArrayList<RoomRecord>();
	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar)
	{
		room_Indsx = ar.sInt(room_Indsx);
		gameStartTime = ar.sString(gameStartTime);
		endWay = ar.sString(endWay);
		hostIndex = ar.sInt(hostIndex);
		roomRecords = (List<RoomRecord>) ar.sObjArray(roomRecords);
	}
}