/*
 * Copyright (C) 2010 The MobileSecurePay Project
 * All right reserved.
 * author: shiqun.shi@alipay.com
 * 
 *  提示：如何获取安全校验码和合作身份者id
 *  1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
 *  2.点击“商家服务”(https://b.alipay.com/order/myorder.htm)
 *  3.点击“查询合作者身份(pid)”、“查询安全校验码(key)”
 */

package com.chess.common.bean.alipay.util;

import com.chess.common.framework.GameContext;

//
// 请参考 Android平台安全支付服务(msp)应用开发接口(4.2 RSA算法签名)部分，并使用压缩包中的openssl
// RSA密钥生成工具，生成一套RSA公私钥。
// 这里签名时，只需要使用生成的RSA私钥。
// Note: 为安全起见，使用RSA私钥进行签名的操作过程，应该尽量放到商家服务器端去进行。
public final class Keys {

	// 合作身份者id，以2088开头的16位纯数字
	public static String DEFAULT_PARTNER = "2088221710277850";
	//public static String DEFAULT_PARTNER = "2088411191060065"; // STiV
	// modify

	// 收款支付宝账号
	public static String DEFAULT_SELLER = "2088221710277850";
	// modify

	// 商户私钥，自助生成
	public static final String PRIVATE ="MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMUoztXEkKt++rzxNWNlE9WMaP2Wlww7o0+TqsR/pSqNLYqoXDByH2un1QLLgTbgQfgFzMl3oayliF1P3Tk/Fmz3cORpv0YLbuLTPKZdbDVgciwM1YlSo6aOdF5+S5Xe5mJPbiKg02HoN2r19OyBsDSqLBNvGXH8xvRhTNkrsrppAgMBAAECgYA4FcK41sDcuDN1mhw42SEKR3X6ZWM0jKaJVZVnbnQ6pYyF/By1f1Fm24hh0ihAxwydFHFMAmcQaMpg2P4ycbrb5gGya8t8F6rd/2Qq4ZUoAMjDrN1bj7JgE2SHNtTCTHdkyZxnlzWZ0yUw7DboYn9t2FZQj5/1hsRojCXj9ExmzQJBAPnRvGehxp90K7+2vKfTZSQK5kpFx4zQSOB4bgsdUXiRRBKMUUjcjZHdQt7ImcP31Eb6sB/JvnfeMXAcPu/Yk1sCQQDKCYsoeBncLnz81gib7o9QNSxHcVXax/VA1hmgWDq+RkVKaz3l4O1KkfYWfSdJ10urZ9zrJSwPTFNg0pEA5aiLAkEA87CVIV4cu4R9/JhL9UXv3jXHqOHio0i2Hkk7xxnFVGAX0CQSUonai1QTm/CAljoVV1vJHGkWpCUZgqv69MoGCwJAeknUhSZM4MzyskcK5vc6n53Ps16STN6DQD9XO4qr48e84RKQYLgIaP54KZyPT06VaqZvPZU+WfQk85IAWr6niQJAOMQlbk9RLrV6fJWjHMRL6uSr4m/hMbf+h1NypjpGXmeji9uNceA/fEz7M1rqxMHqmTlF4HyfL2ST7ZNZ2IQ==";
	//public static final String PRIVATE = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOhqhwCBqO0yK0Vh5cPwsHDw07HtTnQ+2czry2RS/484EaD6WlmCPdAkVWXgIPHbEHLtKTqIzeKzIznLGcfwUgJ+hRuNjSHmLhAbZH0hgV2GoKIoC6wb/djNYJ9O4urOQRWizVfXDi4zZzZgZcxQFKqwtwLIy7JZlnAH7QrvjaSrAgMBAAECgYBfhikmlCQSGWSfh7Uf63UZdmIL5zUvogVyrBdUjKhs/OaB+2PXFijBu609yfjAbwE7uy4O2NuGKYp43nkiLCwIAd+ZKXXpg1MS3GNhR6TuX+ofI4WcLZPkxiDT15jUZsBL3V5iczl/xCil0/pITkViOe7JrRLzGyfXz8n7QI2qgQJBAPe0peBplS2m6oLciVPBmzfMBRdGSFyGtbJk2CB1wVZXl2lvmjt7YTyKHqBngCxHhjanNJwmlv5XLv2Xbl/5opkCQQDwMtD7FJ6AJQG2u4Y2dlSzHLNw+4b/GoH5Zm6tDSvnyWH2XEw6hNu8nn/etZHumdDsjnDFMoZaAKbGWD5ot4/jAkEAmGzJRfaynY0A8dXTi36vjP42g8Pz8zvRwxzbAd50QjVULsVGGyQpCZ/UiNAnxxecNzVc4lm8wHXVrKbqfJSYwQJAZCfcSgWT/bzwlQBme0kmEjVygTbwWGj2L0dN+OyYFSM8rNxZkMEbWvAAQ54URphurGTMkv15VggVgnkDNOjtnwJAaEnzG/1eb27dl+tN0thzChYeaIWdQIO0Cu1h6bGjc7I4M3dKXIjR/TnXJ1/lvoMjo+AUHKQn5Et3LWzZQ7POSg==";
	//public static final String PRIVATE = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDoaocAgajtMitFYeXD8LBw8NOx7U50PtnM68tkUv+POBGg+lpZgj3QJFVl4CDx2xBy7Sk6iM3isyM5yxnH8FICfoUbjY0h5i4QG2R9IYFdhqCiKAusG/3YzWCfTuLqzkEVos1X1w4uM2c2YGXMUBSqsLcCyMuyWZZwB+0K742kqwIDAQAB";
	// 字符编码格式 目前支持 gbk 或 utf-8
	public static String INPUT_CHARSET = "utf-8";
	public static final String PUBLIC  = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";
	//public static final String PUBLIC =  "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";
	//public static final String PUBLIC =  "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";
	// 签名方式 不需修改
	public static String SIGN_TYPE = "RSA";

	// 调试用，创建TXT日志文件夹路径
	public static String LOG_PATH = "C:\\";

	// 回调url
	// public static String
	// NOTICY_URL="http://www.sh668.com.cn/sh668/pay/alipay/notify_url.php";
	//public static String NOTICY_URL = "http://120.132.61.10/x/Pay/AliPay/PayNotify.jsp";// STiV
//	public static String NOTICY_URL = "http://123.206.209.96/MahjongServer/pay/AliPay/PayNotify.jsp";// STiV
	
	public static String NOTICY_URL = (String) GameContext.getPayConfig("ali_notifyUrl");	// modify
}
