package com.chess.common.bean.alipay.util;

import java.net.URLEncoder;
import java.text.DecimalFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.OrderIdUtil;
import com.chess.common.bean.MallItem;

public class AliPayService {

	private static Logger logger = LoggerFactory.getLogger(AliPayService.class);
	@SuppressWarnings("deprecation")
	public static String generateOrderForAliPay(MallItem itemBase,String outTradeNo,int count) {

		String info = getNewOrderInfo(itemBase,outTradeNo,count);
		logger.debug("### order:" + info);//System.out.println("### order:" + info);
		String sign = Rsa.sign(info, Keys.PRIVATE,Keys.INPUT_CHARSET);
		sign = URLEncoder.encode(sign);
		info += "&sign=\"" + sign + "\"&" + getSignType();
		return info;
	}

	/**
	 * 生成待加密的订单信息
	 * 
	 * @param itemBase
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private static String getNewOrderInfo(MallItem itemBase,String outTradeNo,int count) {
		StringBuilder sb = new StringBuilder();
		sb.append("\"");
		// 商品描述
		sb.append("_input_charset=\"utf-8");
		sb.append("\"&body=\"");
		sb.append("1111");
		sb.append("\"&it_b_pay=\"30m");
		// 异步响应地址
		sb.append("\"&notify_url=\"");
		//sb.append("http://yc.kl78.com/manage/pay/alipay/alipay_mobile.aspx");
		sb.append(Keys.NOTICY_URL);
		//sb.append(URLEncoder.encode(Keys.NOTICY_URL));
		// 商品交易号
		sb.append("\"&out_trade_no=\"");
		sb.append(outTradeNo);
	
		
		sb.append("\"&partner=\"");
		sb.append(Keys.DEFAULT_PARTNER);
		sb.append("\"&payment_type=\"1");
		// 支付宝帐号
		sb.append("\"&seller_id=\"");
		sb.append(Keys.DEFAULT_SELLER);
		// 固定的默认值
		sb.append("\"&service=\"mobile.securitypay.pay");
		
		// 商品名称
		sb.append("\"&subject=\"");
		sb.append(itemBase.getName());
		
		
		// 商品价格
		sb.append("\"&total_fee=\"");
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		String price = decimalFormat.format(itemBase.getPrice() * count);
		sb.append(price);
		sb.append("\"");
		
	
	
		
//
//		sb.append("\"");
//		// 合作者身份
//		sb.append("partner=\"");
//		sb.append(Keys.DEFAULT_PARTNER);
//		// 支付宝帐号
//		sb.append("\"&seller_id=\"");
//		sb.append(Keys.DEFAULT_SELLER);
//		// 商品交易号
//		sb.append("\"&out_trade_no=\"");
//		sb.append(outTradeNo);
//		// 商品名称
//		sb.append("\"&subject=\"");
//		sb.append(itemBase.getName());
//		// 商品描述
//		sb.append("\"&body=\"");
//		sb.append(itemBase.getDescription());
//		// 商品价格
//		sb.append("\"&total_fee=\"");
//		DecimalFormat decimalFormat = new DecimalFormat("#.00");
//		String price = decimalFormat.format(itemBase.getPrice() * count);
//		sb.append(price);
//		// 异步响应地址
//		sb.append("\"&notify_url=\"");
//		sb.append(URLEncoder.encode(Keys.NOTICY_URL));
//		// 固定的默认值
//		sb.append("\"&service=\"mobile.securitypay.pay");
//		sb.append("\"&payment_type=\"1");
//		sb.append("\"&_input_charset=\"UTF-8");
//		sb.append("\"&it_b_pay=\"30m");
//
//		sb.append("\"");
		

		return new String(sb);
	}

	/**
	 * 生成订单号
	 * 
	 * @return
	 */
	public static String getOutTradeNo() {
		return OrderIdUtil.getOutTradeNo();
	}

	private static String getSignType() {
		return "sign_type=\"RSA\"";
	}

}
