/**
 * Description:VIP战绩的每锅记录
 */
package com.chess.common.bean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.chess.common.DateService;
import com.chess.common.StringUtil;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class ProxyVipRoomRecord extends NetObject {

	private String recordID="";
	/** 房间ID */
	private String roomID="";
	/** 房间号 */
	private int roomIndex=0;
	/** 玩家ID */
	private String player1Name="";
	/** 玩家ID */
	private int player1Index=0;
	/** 玩家总分 */
	private int score1=0;
	/** 下家玩家ID */
	private String player2Name="";
	/** 下家玩家ID */
	private int player2Index=0;
	/** 下家总分 */
	private int score2=0;
	/** 对家玩家ID */
	private String player3Name="";
	/** 对家玩家ID */
	private int player3Index=0;
	/** 对家总分 */
	private int score3=0;
	/** 上家玩家ID */
	private String player4Name="";
	/** 对家玩家ID */
	private int player4Index=0;
	/** 上家总分 */
	private int score4=0;
	/** 房主ID */
	private String hostName="";
	/** 房间开始时间 */
	private Date startTime;
	/** 房间结束时间 */
	private Date endTime;
	
	private String start;
	
	private String end;
	/** 结束方式 */
	private String endWay="";
	
	/** 代开人ID **/
	private String proxyID="";
	/** 代开人昵称 **/
	private String proxyName = "";
	/** 房间状态 **/
	private int vipState = -1;
	/** 代开房间时间 **/
	private Date proxyStartTime;
	/** 代开消耗房卡 **/
	private int proxyCardCount = 0;
	/** 房主ID **/
	private String hostID = "";
	/** 房间玩法 **/
	private List<Integer> tableRules = new ArrayList<Integer>();
	/** 房间玩法 **/
	private String tableRule = "";
	/** 房间类型 **/
	private int roomType = 0;
	/** 产品id **/
	private String gameId = "";
	/** 俱乐部编号  2018-9-11 */
	private int clubCode = 0;
	
	/** 表名后缀 */
	private String tableNameSuffix = DateService.getTableSuffixByType("", DateService.DATE_BY_DAY);

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		roomID = ar.sString(roomID);
		roomIndex = ar.sInt(roomIndex);
		player1Name = ar.sString(player1Name);
		score1 = ar.sInt(score1);
		player2Name = ar.sString(player2Name);
		score2 = ar.sInt(score2);
		player3Name = ar.sString(player3Name);
		score3 = ar.sInt(score3);
		player4Name = ar.sString(player4Name);
		score4 = ar.sInt(score4);
		hostName = ar.sString(hostName);
		
		ar.sString(StringUtil.date2String(startTime));
		ar.sString(StringUtil.date2String(endTime));
		endWay =  ar.sString(endWay);
		
		proxyID = ar.sString(proxyID);
		proxyName = ar.sString(proxyName);
		vipState = ar.sInt(vipState);
		ar.sString(StringUtil.date2String(proxyStartTime));
		proxyCardCount = ar.sInt(proxyCardCount);
		hostID = ar.sString(hostID);
		tableRules = (List<Integer>) ar.sIntArray(tableRules);
		player1Index = ar.sInt(player1Index);
		player2Index = ar.sInt(player2Index);
		player3Index = ar.sInt(player3Index);
		player4Index = ar.sInt(player4Index);
		tableRule =  ar.sString(tableRule);
		roomType = ar.sInt(roomType);
		gameId =  ar.sString(gameId);
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public int getRoomIndex() {
		return roomIndex;
	}

	public void setRoomIndex(int roomIndex) {
		this.roomIndex = roomIndex;
	}

	public String getPlayer1Name() {
		return player1Name;
	}

	public void setPlayer1Name(String player1Name) {
		this.player1Name = player1Name;
	}

	public int getScore1() {
		return score1;
	}

	public void setScore1(int score1) {
		this.score1 = score1;
	}

	public String getPlayer2Name() {
		return player2Name;
	}

	public void setPlayer2Name(String player2Name) {
		this.player2Name = player2Name;
	}

	public int getScore2() {
		return score2;
	}

	public void setScore2(int score2) {
		this.score2 = score2;
	}

	public String getPlayer3Name() {
		return player3Name;
	}

	public void setPlayer3Name(String player3Name) {
		this.player3Name = player3Name;
	}

	public int getScore3() {
		return score3;
	}

	public void setScore3(int score3) {
		this.score3 = score3;
	}

	public String getPlayer4Name() {
		return player4Name;
	}
	
	public void setPlayer4Name(String player4Name) {
		this.player4Name = player4Name;
	}

	public int getScore4() {
		return score4;
	}

	public void setScore4(int score4) {
		this.score4 = score4;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
		this.start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTime);
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
		this.end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endTime);
	}

	public String getRecordID() {
		return recordID;
	}

	public void setRecordID(String recordID) {
		this.recordID = recordID;
	}

	public String getTableNameSuffix() {
		return tableNameSuffix;
	}

	public void setTableNameSuffix(String tableNameSuffix) {
		this.tableNameSuffix = tableNameSuffix;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}
	
	public String getEndWay() {
		return endWay;
	}

	public void setEndWay(String endWay) {
		this.endWay = endWay;
	}

	public String getProxyID() {
		return proxyID;
	}

	public void setProxyID(String proxyID) {
		this.proxyID = proxyID;
	}

	public String getProxyName() {
		return proxyName;
	}

	public void setProxyName(String proxyName) {
		this.proxyName = proxyName;
	}

	public int getVipState() {
		return vipState;
	}

	public void setVipState(int vipState) {
		this.vipState = vipState;
	}

	public Date getProxyStartTime() {
		return proxyStartTime;
	}

	public void setProxyStartTime(Date proxyStartTime) {
		this.proxyStartTime = proxyStartTime;
	}

	public int getProxyCardCount() {
		return proxyCardCount;
	}

	public void setProxyCardCount(int proxyCardCount) {
		this.proxyCardCount = proxyCardCount;
	}

	public String getHostID() {
		return hostID;
	}

	public void setHostID(String hostID) {
		this.hostID = hostID;
	}

	public List<Integer> getTableRules() {
		return tableRules;
	}

	public void setTableRules(List<Integer> tableRules) {
		this.tableRules = tableRules;
	}

	public int getPlayer1Index() {
		return player1Index;
	}

	public void setPlayer1Index(int player1Index) {
		this.player1Index = player1Index;
	}

	public int getPlayer2Index() {
		return player2Index;
	}

	public void setPlayer2Index(int player2Index) {
		this.player2Index = player2Index;
	}

	public int getPlayer3Index() {
		return player3Index;
	}

	public void setPlayer3Index(int player3Index) {
		this.player3Index = player3Index;
	}

	public int getPlayer4Index() {
		return player4Index;
	}

	public void setPlayer4Index(int player4Index) {
		this.player4Index = player4Index;
	}

	public int getRoomType() {
		return roomType;
	}

	public void setRoomType(int roomType) {
		this.roomType = roomType;
	}

	public String getTableRule() {
		return tableRule;
	}

	public void setTableRule(String tableRule) {
		this.tableRule = tableRule;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

}
