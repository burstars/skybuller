package com.chess.common.service;

import com.chess.common.bean.GameConfig;

public interface ICacheGameConfigService {

	public void initAllGameConfigs();
	
	public GameConfig getGameConfig(String gameId);
	
}
