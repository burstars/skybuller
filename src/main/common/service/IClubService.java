package com.chess.common.service;

import java.util.List;

import com.chess.common.bean.ClubMember;
import com.chess.common.bean.ClubTemplate;
import com.chess.common.bean.TClub;
import com.chess.common.bean.TClubLevel;
import com.chess.common.bean.User;
import com.chess.common.msg.struct.club.CreateClubMsg;
import com.chess.common.msg.struct.club.FreeTableManagerMsg;
import com.chess.common.msg.struct.club.GetClubsMsg;
import com.chess.common.msg.struct.club.JoinClubMsg;

 
public interface IClubService 
{
//	/**
//	 * 添加俱乐部模板
//	 */
//	public void addClubTemplate(int clubCode);
//	/**
//	 * 更新俱乐部模板
//	 */
//	public void updateClubTemplate(User pl,FreeTableManagerMsg msg);
//	
//	/**
//	 * 删除俱乐部模板
//	 */
//	public void deleteClubTemplate(int id);
//	
//	/**
//	 * 获取俱乐部下所有模板
//	 * @return
//	 */
//	public List<ClubTemplate> getClubTemplateAll(int clubCode);
//	
//	/**查询俱乐部*/
//	public void queryClubs(User pl,GetClubsMsg msg);
//	/**加入俱乐部*/
//	public void joinClub(User pl,JoinClubMsg msg);
	
	List<TClubLevel> getAllClubLevels();
	
	TClub getClub(int clubCode);
	
	List<Integer> getCoinItems(String level);
	
	TClubLevel getClubLevel(String level);
	
	ClubMember getClubMember(int clubCode, String playerId);
}
