package com.chess.common.service;

import com.chess.common.bean.User;
import com.chess.core.net.msg.MsgBase;

public interface IMin2Nodify {
	
	public void nodify(MsgBase obj,User pl);

}
