
package com.chess.common.service;

import java.util.List;
import java.util.Map;

import com.chess.common.bean.MallItem;

public interface IMallItemService {

	public void deleteItemByID(int itemID);
	
	public List<MallItem> refreshItemBase();
	
	public MallItem getItemBaseById(int itemBaseID);
	
	public Integer getSellCard(Map<String, String> map);
	
}
