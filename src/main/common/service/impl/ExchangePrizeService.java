package com.chess.common.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.SpringService;
import com.chess.common.bean.GameActiveCode;
import com.chess.common.bean.User;
import com.chess.common.bean.UserBackpack;
import com.chess.common.constant.LogConstant;
import com.chess.common.dao.IExchangePrizeDAO;
import com.chess.common.dao.IUserBackpackDAO;
import com.chess.common.dao.IUserDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.common.msg.struct.other.ExchangePrizeMsg;
import com.chess.common.msg.struct.other.ExchangePrizeMsgAck;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.IExchangePrizeService;

//8.25 兑换礼品
public class ExchangePrizeService  implements IExchangePrizeService{
	
	private static Logger logger = LoggerFactory.getLogger(ExchangePrizeService.class);
	
	private IExchangePrizeDAO exchangePrizeDAO = null;
	private IUserDAO playerDAO = null;
	private IUserBackpackDAO userBackpackDAO = null;
	
	
	//使用兑换码兑换礼品
	public void useExchangeNum(User pl,ExchangePrizeMsg msg) {
		
		//获取当前时间；
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String regTime = df.format(new Date());
		
		ICacheUserService dbplayerService = (ICacheUserService) SpringService.getBean("playerService");	
		User userInfo = dbplayerService.getPlayerByPlayerID(msg.playerID);
		
		//返回值得消息
		ExchangePrizeMsgAck ack = new ExchangePrizeMsgAck();
		ack.msgCMD = MsgCmdConstant.EXCHANGE_PIRZE_ACK;
		
		/*微信分享赠卡逻辑 20161202*/
		if(msg.changeNum != null && "@wxfx@".equals(msg.changeNum)){
			logger.info("微信分享赠卡");
			String logmsg = "微信分享赠卡";
			int cardNum = 2;
			dbplayerService.createPlayerLog(userInfo.getPlayerID(), userInfo.getPlayerIndex(),
					userInfo.getPlayerName(), userInfo.getGold(),
					LogConstant.OPERATION_TYPE_ADD_PRO,
					LogConstant.OPERATION_PAY, cardNum, logmsg,3333,userInfo.getGameId());			
			 List<UserBackpack> pack =  this.userBackpackDAO.getPlayerItemListByItemID(String.valueOf(userInfo.getPlayerID())+"_3333");
			 if (pack.size()==0){
				  UserBackpack item = new UserBackpack();
				  item.setItemID(String.valueOf(userInfo.getPlayerID())+"_3333");
				  item.setItemBaseID(3333);
				  item.setPlayerID(userInfo.getPlayerID());
				  item.setItemNum(cardNum);
				  this.userBackpackDAO.createPlayerItem(item,"");
			  } else{
				  this.userBackpackDAO.addPlayerItemNum(msg.playerID, 3333, cardNum);
			  }
			  //通用卡处理
			  List<UserBackpack> pis = pl.getItems();
			  for(int i=0;i<pis.size();i++){
					UserBackpack pib = pis.get(i);
//					if(pib.getItemBaseID() == 3333){
					pib.setItemNum(pib.getItemNum() + cardNum);
//					}
			  }
			 this.playerDAO.updatePlayerParam01(userInfo.getPlayerID(), "1");
			 pl.setParam01("1");
	 
			 ack.result = 4;
			 logger.info("微信分享赠卡，送卡成功");
			 
				//TODO 处理微信赠卡添加日志
			 dbplayerService.createSendCardLog(pl, 3333, cardNum ,LogConstant.SEND_CARD_WECHAT_SHARE); 
		}else if(msg.changeNum != null && "@wxfxNoCard@".equals(msg.changeNum)){
			
			if(pl.getParam01().equals("1")){//已经分享过了
				ack.result = 6;
			}else{
				this.playerDAO.updatePlayerFreeCj(userInfo.getPlayerID(), 1);
				 pl.setLuckyDrawsTimes(1);
				 this.playerDAO.updatePlayerParam01(userInfo.getPlayerID(), "1");
				 pl.setParam01("1");
				 ack.result = 5;
			}
			 
		}else{
		
		//数据库取兑换码对应的所有信息；
		GameActiveCode activeCode = this.exchangePrizeDAO.searchCode(msg.changeNum);
		
		int result1 = 0;
		int result2 = 0;
		int result = 0;
		if(activeCode != null){
			
			result1 = regTime.compareTo(activeCode.getStart_date());
			result2 = regTime.compareTo(activeCode.getEnd_date());
			result = activeCode.getIs_value().compareTo("1") ;
		}
		
			//判断兑换码是否合法；
		if(activeCode == null){
			
			//没有找到相应的兑换码
			ack.result = 0;
		}else if (result != 0){
			
			//兑换码已使用
			ack.result = 1;
		}else if (result1 < 0 || result2 > 0){
			
			//兑换码不在有效期内
			ack.result = 2;
		}else{
		
			//兑换码可以正常使用
			//更新缓存
			
			
			int plGold = userInfo.getGold();
			int gold = activeCode.getMoney() + plGold;
			
			if (gold >0 )
			{
				if (null != dbplayerService)
				{			
					if(userInfo!=null)
					{
						userInfo.setGold(gold);
						
						//修改记录写入日志
						String detail = "兑换金币"  + gold;
						dbplayerService.createPlayerLog(userInfo.getPlayerID(), pl.getPlayerIndex(), pl.getPlayerName(), pl.getGold(), LogConstant.OPERATION_TYPE_ADD_GOLD, LogConstant.OPERATION_TYPE_ADMIN_CHANGE_USER_DATA, gold, detail, LogConstant.MONEY_TYPE_GOLD,userInfo.getGameId());
					}
				}
				this.playerDAO.updatePlayerGold(msg.playerID,gold );
			}
			if (activeCode.getCard() >0)
			{
				String detail5 = "兑换房卡=" + activeCode.getCard();
				dbplayerService.createPlayerLog(userInfo.getPlayerID(), userInfo.getPlayerIndex(),
						userInfo.getPlayerName(), userInfo.getGold(),
						LogConstant.OPERATION_TYPE_ADD_PRO,
						LogConstant.OPERATION_PAY, activeCode.getCard(), detail5,
						3333,userInfo.getGameId());
				
				 List<UserBackpack> pack =  this.userBackpackDAO.getPlayerItemListByItemID(String.valueOf(userInfo.getPlayerID())+"_3333");
				  if (pack.size()==0){
					  UserBackpack item = new UserBackpack();
					  item.setItemID(String.valueOf(userInfo.getPlayerID())+"_3333");
					  item.setItemBaseID(3333);
					  item.setPlayerID(userInfo.getPlayerID());
					  item.setItemNum(activeCode.getCard());
					  this.userBackpackDAO.createPlayerItem(item,"");
				  } else{
					  //通用卡处理
					  List<UserBackpack> pis = pl.getItems();
					  for(int i=0;i<pis.size();i++){
							UserBackpack pib = pis.get(i);
//							if(pib.getItemBaseID() == 3333){
							pib.setItemNum(pib.getItemNum() + activeCode.getCard());
//							}
					  }
					  this.userBackpackDAO.addPlayerItemNum(msg.playerID, 3333, activeCode.getCard() );
				  }

				
			}
			
			int userId = userInfo.getPlayerIndex();
			this.exchangePrizeDAO.exchangePrize(userId,0, activeCode.getID());
			ack.result = 3;
			ack.coin = activeCode.getMoney();
			ack.card = activeCode.getCard();
		}
		}
		GameContext.gameSocket.send(pl.getSession(), ack);
		
	}

	public IExchangePrizeDAO getExchangePrizeDAO() {
		return exchangePrizeDAO;
	}


	public void setExchangePrizeDAO(IExchangePrizeDAO exchangePrizeDAO) {
		this.exchangePrizeDAO = exchangePrizeDAO;
	}

	public IUserBackpackDAO getUserBackpackDAO() {
		return userBackpackDAO;
	}


	public void setUserBackpackDAO(IUserBackpackDAO userBackpackDAO) {
		this.userBackpackDAO = userBackpackDAO;
	}
	
	public IUserDAO getPlayerDAO() {
		return playerDAO;
	}

	public void setPlayerDAO(IUserDAO playerDAO) {
		this.playerDAO = playerDAO;
	}

}
