package com.chess.common.service.impl;

import com.chess.common.bean.EntranceUser;
import com.chess.common.bean.SensitiveWords;
import com.chess.common.dao.IEntranceUserDAO;
import com.chess.common.service.IEntranceUserService;

public class EntranceUserService implements IEntranceUserService 
{
	
	public IEntranceUserDAO userDAO;
	
	public void init()
	{
		userDAO.init();
	}
	 public EntranceUser getUserByAccount (String account)
	 {
		 return userDAO.getUserByAccount(account);
	 }
	 public SensitiveWords getSensitiveWords (String word)
	 {
		 return userDAO.getWords(word);
	 }
	 
	 public EntranceUser getUserByNickName(String account)
	 {
		 return userDAO.getUserByNickName(account);
	 }
	 
	 public EntranceUser getUserByMachineCode (String machineCode)
	 {
		 return userDAO.getUserByMachineCode(machineCode);
	 }
	 
	 public EntranceUser getUserByQQOpenID(String openid)
	 {
		 return userDAO.getUserByQQOpenID(openid);
	 }
	 
	 public EntranceUser getUserByWXOpenID(String openid)
	 {
		 return userDAO.getUserByWXOpenID(openid);
	 }
	 /**
		 * cc modify 
		 * 2017-12-5
		 */
	 public EntranceUser getUserByWXUnionID(String unionid) {
			return userDAO.getUserByWXUnionID(unionid);
		}
	 //
	 public void addUser(EntranceUser user)
	 {
		 userDAO.insert(user);
	 }
	 
	
	 
	 //
	public IEntranceUserDAO getUserDAO() {
		return userDAO;
	}
	public void setUserDAO(IEntranceUserDAO userDAO) {
		this.userDAO = userDAO;
	}

}
