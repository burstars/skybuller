package com.chess.common.service.impl;

import java.util.List;

import com.chess.common.SpringService;
import com.chess.common.bean.GoodNumber;
import com.chess.common.dao.IGoodNumberDAO;
import com.chess.common.service.IGoodNumberService;
import com.chess.common.service.ISystemConfigService;

public class GoodNumberService implements IGoodNumberService {

	private IGoodNumberDAO goodNumberDAO;
	private ISystemConfigService sfsConf;
	
	public void createGoodNumber(GoodNumber goodNumber){
		goodNumberDAO.createGoodNumber(goodNumber);
		//sfsConf=(ISystemConfigService) SpringService.getBean("sysConfigService");
		//sfsConf.addGoodNumber(goodNumber.getNumber()+"");
	}
	
	public void updateGoodNumber(GoodNumber goodNumber){
		goodNumberDAO.update(goodNumber);
	}
	
	public int getAllGoodNumberTotal() {
		return goodNumberDAO.getAllGoodNumberTotal();
	}
	
	public List<GoodNumber> getAllGoodNumber(String start,String end) {
		return goodNumberDAO.getAllGoodNumber(start, end);
	}

	public List<GoodNumber> getGoodNumberByNumber(String number,String start,String end) {
		return goodNumberDAO.getGoodNumberByNumber(number, start, end);
	}
	
	public GoodNumber getAnGoodNumberByNumber(String number) {
		return goodNumberDAO.getAnGoodNumberByNumber(number);
	}
	
	public int getGoodNumberByNumberTotal(String number) {
		return goodNumberDAO.getGoodNumberByNumberTotal(number);
	}

	public int getGoodNumberTotal(String number)
	{
		return goodNumberDAO.getGoodNumberTotal(number);
	}
	
	public GoodNumber getGoodNumberByPlayerId(String playerId) {
		return goodNumberDAO.getGoodNumberByPlayerId(playerId);
	}

	
	public void updateGoodNumber(String id,String playerId)
	{
		GoodNumber number = goodNumberDAO.getGoodNumberByPlayerId(id);
		number.setPlayerId(playerId);
		goodNumberDAO.update(number);
	}

	public IGoodNumberDAO getGoodNumberDAO() {
		return goodNumberDAO;
	}

	public void setGoodNumberDAO(IGoodNumberDAO goodNumberDAO) {
		this.goodNumberDAO = goodNumberDAO;
	}

	public void deleteGoodNumber(int id){
		this.goodNumberDAO.deleteGoodNumberByID(id);
	}

	
}
