package com.chess.common.service.impl;

import java.util.Date;
import java.util.List;

import com.chess.common.DateService;
import com.chess.common.bean.ItemOpStaBase;
import com.chess.common.bean.LoginLogInfo;
import com.chess.common.bean.MallItem;
import com.chess.common.bean.StatisticUser;
import com.chess.common.dao.IPlayerLogDAO;
import com.chess.common.dao.IUserStatisticDAO;
import com.chess.common.dao.impl.MallHistoryDAO;
import com.chess.common.service.ICacheUserService;

public class StatisticUserService {
	private IUserStatisticDAO statisticUserDAO;
	private LoginService loginService ;
	private IPlayerLogDAO playerLogDAO;
	private ICacheUserService playerService;
	private MallHistoryDAO mallHistoryDAO ;
	
	/**分渠道统计下每天的数据*/
	public void dataStatisticByTimeAll(Date time){
		this.dataStatisticByTime(time, -1);
		this.dataStatisticByTime(time, 2);
	}
	
	/**按时间统计下运营数据，数据已存在的话直接查询，没有再统计*/
	public StatisticUser dataStatisticByTime(Date time,int platformType){
		String timeStr = DateService.getCurrentDate2AsStringByDate(time);
		StatisticUser todaySta =statisticUserDAO.getStatisticUserByTimeStr(timeStr,platformType);
		Date firstTime = DateService.getCurrentDayFirstUtilDate();
		Date endTime = DateService.getCurrentDayLastUtilDate();
		boolean isBefore = false;
		if(firstTime.getTime()>time.getTime()+2*60*1000){
			isBefore = true;
			firstTime = DateService.getDayFirstUtilDate(time);
			endTime = DateService.getDayLastUtilDate(time);
			time = firstTime;
		}
		if( todaySta==null&&isBefore){// 当天前的数据才能做统计
			List<LoginLogInfo>  loginLogs = loginService.countPlayerLoginInfoByTime(time,platformType);
			 Integer regisnum=0;//当日注册人数
			 Integer loginnum=0;//当天去重登录人数
			 Integer secdayleftnum=0;//昨天的次日留存数  昨天注册玩家今天登陆人数
			 Integer thirddayleftnum=0;//前天的三日留存数  前天注册玩家今天登陆人数
			 Integer sevdayleftnum=0;//上周的今天 七日留存数    上周注册玩家今天的登陆人数
			 
			 int totalOnLineMin =0;
			 Integer numOnlineMin_5 =0;
			 Integer  numOnlineMin_5_30 = 0;
				/**当日在线时长在30m~60m的人数*/
			 Integer  numOnlineMin_30_60 = 0;
				/**当日在线时长在60m~120m的人数*/
			 Integer  numOnlineMin_60_120 = 0;
				/**当日在线时长在12m~300m的人数*/
			 Integer  numOnlineMin_120_300 = 0;
				/**当日在线时长在300m以上的人数*/
			 Integer  numOnlineMin_300 = 0;
			 
			 
			 Date yesterdayFirstTime = DateService.dateIncreaseByDay(time, -1);
			 Date yesterdayEndTime = DateService.dateIncreaseByDay(time, -1);
			 Date  threeDaysAgoFirstTime = DateService.dateIncreaseByDay(time, -2);
			 Date  threeDaysAgoEndTime = DateService.dateIncreaseByDay(time, -2);
			 Date  sevenDaysAgoFirstTime = DateService.dateIncreaseByDay(time, -6);
			 Date  sevenDaysAgoEndTime = DateService.dateIncreaseByDay(time, -6);
			 if(loginLogs!=null&&loginLogs.size()>0){
				 for(int i = 0;i<loginLogs.size();i++){
					 loginnum++; 
					 LoginLogInfo li = loginLogs.get(i);
					 
					 if( (li==null) || (li.getRegisterTime()==null) )
					 {
						 continue;
					 }
					 
					 int onlingMin = -1;
					 if(li.getLoginMinTime() != null){
						 onlingMin = li.getLoginMinTime();
					 }
					 totalOnLineMin = totalOnLineMin+onlingMin;
					 if(onlingMin<5){
						 numOnlineMin_5++;
					 }else if(onlingMin<30){
						 numOnlineMin_5_30++;
					 }else if(onlingMin<60){
						 numOnlineMin_30_60++;
					 }else if(onlingMin<120){
						 numOnlineMin_60_120++;
					 }else if(onlingMin<300){
						 numOnlineMin_120_300++;
					 }else {
						 numOnlineMin_300++;
					 }
					 
					 //留存数统计
					 if(li.getRegisterTime().getTime()>=firstTime.getTime()&&li.getRegisterTime().getTime()<endTime.getTime()){
						 regisnum++; 
					 }else if(li.getRegisterTime().getTime()>=yesterdayFirstTime.getTime()&&li.getRegisterTime().getTime()<yesterdayEndTime.getTime()){
						 secdayleftnum++;
					 }else if(li.getRegisterTime().getTime()>=threeDaysAgoFirstTime.getTime()&&li.getRegisterTime().getTime()<threeDaysAgoEndTime.getTime()){
						 thirddayleftnum++;
					 }else if(li.getRegisterTime().getTime()>=sevenDaysAgoFirstTime.getTime()&&li.getRegisterTime().getTime()<sevenDaysAgoEndTime.getTime()){
						 sevdayleftnum++;
					 }
					 
					 
				 } 
			 }
//			 //更新下昨天的 次日留存数
//			 StatisticUser yesterDaySta = statisticUserDAO.getStatisticUserByTimeStr(DateService.getCurrentDate2AsStringByDate(yesterdayFirstTime));
//			 if(yesterDaySta!=null){
//				 statisticUserDAO.updateSecdayLeftNumByID(yesterDaySta.getStatisticId(), secdayleftnum);
//			 }
//			//更新下前天的 三日留存数
//			 StatisticUser threeDaySta = statisticUserDAO.getStatisticUserByTimeStr(DateService.getCurrentDate2AsStringByDate(threeDaysAgoFirstTime));
//			 if(threeDaySta!=null){
//				 statisticUserDAO.updateThirdDayLeftNumByID(threeDaySta.getStatisticId(), thirddayleftnum);
//			 }
//			//更新下上周的 七日留存数
//			 StatisticUser sevenDaySta = statisticUserDAO.getStatisticUserByTimeStr(DateService.getCurrentDate2AsStringByDate(sevenDaysAgoFirstTime));
//			 if(sevenDaySta!=null){
//				 statisticUserDAO.updateSevDayLeftNumByID(sevenDaySta.getStatisticId(), sevdayleftnum);
//			 }
			 //有可能昨天没有玩家登录记录，同样也加入一条记录
			 todaySta  = new StatisticUser();
			 todaySta.setStatisticTime(timeStr);
			 todaySta.setRegisNum(regisnum);
			 todaySta.setLoginNum(loginnum);
			 //在线总时长
			 todaySta.setOnlineMinTotal(totalOnLineMin);
			 todaySta.setNumOnlineMin_5(numOnlineMin_5);
			 todaySta.setNumOnlineMin_5_30(numOnlineMin_5_30);
			 todaySta.setNumOnlineMin_30_60(numOnlineMin_30_60);
			 todaySta.setNumOnlineMin_60_120(numOnlineMin_60_120);
			 todaySta.setNumOnlineMin_120_300(numOnlineMin_120_300);
			 todaySta.setNumOnlineMin_300(numOnlineMin_300);
			 todaySta.setPlatformType(platformType);
			 
			//统计充值情况
			 Integer payPlayerDayNum = mallHistoryDAO.getPayPlayerNumByTime(firstTime ,endTime,platformType);
			 todaySta.setPayPlayerDayNum(payPlayerDayNum);
			 Integer  payTotalDayNum = mallHistoryDAO.getPayTotalNumByTime(firstTime ,endTime,platformType);
			 todaySta.setPayTotalDayNum(payTotalDayNum);
				//礼包等道具购买记录统计
				List<ItemOpStaBase> itemStaList = null;
				try{
					//itemStaList =playerLogDAO.countItemOperationStatisticPay(time,platformType);
					itemStaList = mallHistoryDAO.countItemOperationStatisticPay(firstTime, endTime, platformType);
					if(itemStaList!=null&&itemStaList.size()>0){
						for(int i=0;i<itemStaList.size();i++){
							ItemOpStaBase io = itemStaList.get(i);
							MallItem ib = playerService.getMallItem(io.getBase_id());
							if(ib!=null){
								io.setName(ib.getName());
								io.setPrice(ib.getPrice());
							}
							int total = io.getPrice()*io.getUseNum();
							io.setTotal(total);
						}
						
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			todaySta.setItemStaList(itemStaList);
			todaySta.enCodeData();
			statisticUserDAO.createStatisticUser(todaySta);
			
		}
		//修复下
		this.repairStatisticByTime(time,platformType);
		todaySta = statisticUserDAO.getStatisticUserByTimeStr(timeStr,platformType);
		if(todaySta!=null){//数据存在，已统计过
			todaySta.deCodeData();

			//礼包等道具购买记录统计
			List<ItemOpStaBase> itemStaList = null;
			try{
				itemStaList =todaySta.getItemStaList();
				if(itemStaList!=null&&itemStaList.size()>0){
					for(int i=0;i<itemStaList.size();i++){
						ItemOpStaBase io = itemStaList.get(i);
						MallItem ib = playerService.getMallItem(io.getBase_id());
						if(ib!=null){
							io.setName(ib.getName());
							io.setPrice(ib.getPrice());
						}
						int total = io.getPrice()*io.getUseNum();
						io.setTotal(total);
					}
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			todaySta.setItemStaList(itemStaList);
		}
		if(todaySta!=null){
			todaySta.cate();
		}
		
	
		return todaySta ;
	}
	/**修复下统计数据*/
	public void repairStatisticByTime(Date time,int platformType){
		String timeStr = DateService.getCurrentDate2AsStringByDate(time);
		StatisticUser todaySta =statisticUserDAO.getStatisticUserByTimeStr(timeStr,platformType);
		Date todayFirstTime = DateService.getCurrentDayFirstUtilDate();
		if( todaySta!=null){// 
			Date regStartTime = DateService.getDayFirstUtilDate(time);
			Date regEndTime = DateService.getDayLastUtilDate(time);
			//修复下次日登录人数
			Date yestodayTime = DateService.dateIncreaseByDay(time, 1);
			Integer secdayLoginNum = loginService.countLoginNumByRegisterTime(yestodayTime, regStartTime, regEndTime,platformType);
			if(secdayLoginNum!=null){
				statisticUserDAO.updateSecdayLeftNumByID(todaySta.getStatisticId(), secdayLoginNum);
			}
			 
			//修复下3日登录人数
			Date thirdDayTime = DateService.dateIncreaseByDay(time, 2);
			Integer thirdDayLoginNum = loginService.countLoginNumByRegisterTime(thirdDayTime, regStartTime, regEndTime,platformType);
			if(thirdDayLoginNum!=null){
				statisticUserDAO.updateThirdDayLeftNumByID(todaySta.getStatisticId(), thirdDayLoginNum);
			}
			//修复下7日登录人数
			Date sevDayTime = DateService.dateIncreaseByDay(time, 6);
			Integer sevDayLoginNum = loginService.countLoginNumByRegisterTime(sevDayTime, regStartTime, regEndTime,platformType);
			if(sevDayLoginNum!=null){
				statisticUserDAO.updateSevDayLeftNumByID(todaySta.getStatisticId(), sevDayLoginNum);
			}
			 
		}
	}
	

	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	public IUserStatisticDAO getStatisticUserDAO() {
		return statisticUserDAO;
	}

	public void setStatisticUserDAO(IUserStatisticDAO statisticUserDAO) {
		this.statisticUserDAO = statisticUserDAO;
	}
	public IPlayerLogDAO getPlayerLogDAO() {
		return playerLogDAO;
	}
	public void setPlayerLogDAO(IPlayerLogDAO playerLogDAO) {
		this.playerLogDAO = playerLogDAO;
	}
	public ICacheUserService getPlayerService() {
		return playerService;
	}
	public void setPlayerService(ICacheUserService playerService) {
		this.playerService = playerService;
	}
	public MallHistoryDAO getMallHistoryDAO() {
		return mallHistoryDAO;
	}
	public void setMallHistoryDAO(MallHistoryDAO mallHistoryDAO) {
		this.mallHistoryDAO = mallHistoryDAO;
	}
}
