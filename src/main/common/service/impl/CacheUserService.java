package com.chess.common.service.impl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.mina.core.session.IoSession;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.chess.common.DateService;
import com.chess.common.MD5Service;
import com.chess.common.RequestHttpService;
import com.chess.common.SpringService;
import com.chess.common.UUIDGenerator;
import com.chess.common.bean.AvgPeopleOnline;
import com.chess.common.bean.ClubMember;
import com.chess.common.bean.ClubRankingList;
import com.chess.common.bean.ClubTemplate;
import com.chess.common.bean.GameConfig;
import com.chess.common.bean.MallHistory;
import com.chess.common.bean.MallItem;
import com.chess.common.bean.MobileCodeLimitedPhone;
import com.chess.common.bean.NormalGameRecord;
import com.chess.common.bean.PlayerHuType;
import com.chess.common.bean.PlayerOperationLog;
import com.chess.common.bean.Prize;
import com.chess.common.bean.ProxyVipRoomRecord;
import com.chess.common.bean.Reward;
import com.chess.common.bean.SendCardLog;
import com.chess.common.bean.SensitiveWords;
import com.chess.common.bean.ShopGoods;
import com.chess.common.bean.SystemConfigPara;
import com.chess.common.bean.SystemNotice;
import com.chess.common.bean.TClub;
import com.chess.common.bean.User;
import com.chess.common.bean.UserBackpack;
import com.chess.common.bean.UserFriend;
import com.chess.common.bean.VipGameRecord;
import com.chess.common.bean.VipRoomRecord;
import com.chess.common.bean.WelFare;
import com.chess.common.bean.alipay.util.AliPayService;
import com.chess.common.bean.alipay.util.Result;
import com.chess.common.bean.club.TableInfoBean;
import com.chess.common.bean.club.TableUserInfo;
import com.chess.common.constant.ConfigConstant;
import com.chess.common.constant.DBConstant;
import com.chess.common.constant.ErrorCodeConstant;
import com.chess.common.constant.GameConstant;
import com.chess.common.constant.LogConstant;
import com.chess.common.constant.PayConstant;
import com.chess.common.constant.RewardConstant;
import com.chess.common.constant.VipConstant;
import com.chess.common.dao.IAvgPeopleOnlineDAO;
import com.chess.common.dao.IClubMemberDAO;
import com.chess.common.dao.IClubRankingListDAO;
import com.chess.common.dao.IClubTemplate;
import com.chess.common.dao.ILoginLogDAO;
import com.chess.common.dao.IMallHistoryDAO;
import com.chess.common.dao.IMallItemDAO;
import com.chess.common.dao.INormalGameRecordDAO;
import com.chess.common.dao.IPlayerHuTypeDAO;
import com.chess.common.dao.IPlayerLogDAO;
import com.chess.common.dao.IPlayerPlayLogDAO;
import com.chess.common.dao.IPrizeDAO;
import com.chess.common.dao.IProxyVipRoomRecordDAO;
import com.chess.common.dao.IRewardDAO;
import com.chess.common.dao.IShopGoodsDAO;
import com.chess.common.dao.ISystemNoticeDAO;
import com.chess.common.dao.ITClubDao;
import com.chess.common.dao.IUserBackpackDAO;
import com.chess.common.dao.IUserDAO;
import com.chess.common.dao.IUserFriendDAO;
import com.chess.common.dao.IVipGameRecordDAO;
import com.chess.common.dao.IVipRoomRecordDAO;
import com.chess.common.dao.IWelFareDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.framework.db.AsyncDatabaseThread;
import com.chess.common.framework.db.DBOperation;
import com.chess.common.framework.security.SignUtil;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.common.msg.struct.club.ClubChargeLevelMsg;
import com.chess.common.msg.struct.club.ClubChargeLevelMsgAck;
import com.chess.common.msg.struct.club.ClubChongZhiMsg;
import com.chess.common.msg.struct.club.ClubChongZhiMsgAck;
import com.chess.common.msg.struct.club.ClubRechargeMsg;
import com.chess.common.msg.struct.club.ClubRechargeMsgAck;
import com.chess.common.msg.struct.club.CreateClubMsg;
import com.chess.common.msg.struct.club.CreateClubMsgAck;
import com.chess.common.msg.struct.club.FreeTableManagerMsg;
import com.chess.common.msg.struct.club.FreeTableManagerMsgAck;
import com.chess.common.msg.struct.club.GetClubsMsg;
import com.chess.common.msg.struct.club.GetClubsMsgAck;
import com.chess.common.msg.struct.club.GetQyqRankListMsg;
import com.chess.common.msg.struct.club.GetQyqRankListMsgAck;
import com.chess.common.msg.struct.club.GetQyqWinnerListMsg;
import com.chess.common.msg.struct.club.GetQyqWinnerListMsgAck;
import com.chess.common.msg.struct.club.GetSingleClubInfoMsg;
import com.chess.common.msg.struct.club.GetSingleClubInfoMsgAck;
import com.chess.common.msg.struct.club.JoinClubMsg;
import com.chess.common.msg.struct.club.JoinClubMsgAck;
import com.chess.common.msg.struct.clubmember.ClubMemberListMsgAck;
import com.chess.common.msg.struct.clubmember.ClubMemberOprateMsgAck;
import com.chess.common.msg.struct.entranc.EntranceUserMsg;
import com.chess.common.msg.struct.entranc.EntranceUserMsgAck;
import com.chess.common.msg.struct.friend.AddFriendMsgAck;
import com.chess.common.msg.struct.friend.FriendMsgAck;
import com.chess.common.msg.struct.friend.FriendOnLineMsgAck;
import com.chess.common.msg.struct.friend.FriendOprateMsgACK;
import com.chess.common.msg.struct.friend.FriendRefleshMsgAck;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomAckMsg;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomMsg;
import com.chess.common.msg.struct.friend.UpdateFriendRemarkMsg;
import com.chess.common.msg.struct.friend.UpdateFriendRemarkMsgAck;
import com.chess.common.msg.struct.login.Login2Msg;
import com.chess.common.msg.struct.login.Login2MsgAck;
import com.chess.common.msg.struct.login.LoginMsg;
import com.chess.common.msg.struct.login.LoginMsgAck;
import com.chess.common.msg.struct.login.OtherLoginMsgAck;
import com.chess.common.msg.struct.login.ReconnectMsg;
import com.chess.common.msg.struct.login.RegisterPlayerMsg;
import com.chess.common.msg.struct.luckydraw.GetPrizeListMsgAck;
import com.chess.common.msg.struct.luckydraw.LuckyDrawMsg;
import com.chess.common.msg.struct.message.RequestCompletePhoneNumber;
import com.chess.common.msg.struct.message.RequestCompletePhoneNumberAck;
import com.chess.common.msg.struct.other.AskRecoverGameMsg;
import com.chess.common.msg.struct.other.AskRecoverPlayerStateMsg;
import com.chess.common.msg.struct.other.CheckOfflineBackMsg;
import com.chess.common.msg.struct.other.CheckOfflineBackMsgAck;
import com.chess.common.msg.struct.other.CreateVipRoomMsg;
import com.chess.common.msg.struct.other.EnterVipRoomMsg;
import com.chess.common.msg.struct.other.EnterVipRoomMsgAck;
import com.chess.common.msg.struct.other.GameReadyMsg;
import com.chess.common.msg.struct.other.GameUpdateMsg;
import com.chess.common.msg.struct.other.HuDongBiaoQingMsg;
import com.chess.common.msg.struct.other.PlayerAskCloseVipRoomMsg;
import com.chess.common.msg.struct.other.PlayerGameOpertaionAckMsg;
import com.chess.common.msg.struct.other.PlayerGameOpertaionMsg;
import com.chess.common.msg.struct.other.PlayerGameOverMsg;
import com.chess.common.msg.struct.other.PlayerOpertaionMsg;
import com.chess.common.msg.struct.other.PlayerTableOperationMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomCloseMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomRecordMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomRecordMsgAck;
import com.chess.common.msg.struct.other.RequestStartGameMsg;
import com.chess.common.msg.struct.other.RequestStartGameMsgAck;
import com.chess.common.msg.struct.other.SearchUserByIndexMsg;
import com.chess.common.msg.struct.other.SearchUserByIndexMsgAck;
import com.chess.common.msg.struct.other.ShowNoticeOnLoginAck;
import com.chess.common.msg.struct.other.TalkingInGameMsg;
import com.chess.common.msg.struct.other.TwoPeopleSupportMsgAck;
import com.chess.common.msg.struct.other.UpdatePlayerItemListMsg;
import com.chess.common.msg.struct.other.UpdatePlayerPropertyMsg;
import com.chess.common.msg.struct.other.UpdatePlayerProxyOpenRoomMsg;
import com.chess.common.msg.struct.other.UserExtendMsg;
import com.chess.common.msg.struct.other.UserExtendMsgAck;
import com.chess.common.msg.struct.other.VipGameRecordAckMsg;
import com.chess.common.msg.struct.other.VipGameRecordMsg;
import com.chess.common.msg.struct.other.VipRoomRecordAckMsg;
import com.chess.common.msg.struct.other.VipRoomRecordMsg;
import com.chess.common.msg.struct.other.WelFareMsg;
import com.chess.common.msg.struct.other.WelFareMsgAck;
import com.chess.common.msg.struct.other.ZhiDuiMsgAck;
import com.chess.common.msg.struct.pay.GameBuyItemAckMsg;
import com.chess.common.msg.struct.pay.GameBuyItemCompleteMsg;
import com.chess.common.msg.struct.pay.GameBuyItemMsg;
import com.chess.common.msg.struct.pay.GameIPABuyItemCompleteMsg;
import com.chess.common.msg.struct.playeropt.ValidateMsgAck;
import com.chess.common.msg.struct.system.MobileCodeMsg;
import com.chess.common.msg.struct.system.MobileCodeMsgAck;
import com.chess.common.msg.struct.system.ScrollMsg;
import com.chess.common.pay.AsyncAliPayThread;
import com.chess.common.pay.AsyncIpaPayThread;
import com.chess.common.pay.AsyncPayThread;
import com.chess.common.pay.PayOperation;
import com.chess.common.service.ICacheGameConfigService;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.IEntranceUserService;
import com.chess.common.service.IGoodNumberService;
import com.chess.common.service.IMin2Nodify;
import com.chess.common.service.IPayService;
import com.chess.common.service.ISystemConfigService;
import com.chess.common.service.IUserService;
import com.chess.core.constant.NetConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.util.BeanUtilsEx;
import com.chess.nndzz.msg.struct.other.HuDongBiaoQingMsgNNDZZ;
import com.chess.nndzz.table.GameRoom;
import com.chess.nndzz.table.GameTable;
import com.chess.nndzz.table.TableLogicProcess;

public class CacheUserService implements ICacheUserService {

	private static Logger logger = LoggerFactory
			.getLogger(CacheUserService.class);

	/**
	 * *,为了方便查找，一些比较操作使用这个map里面的数据就可以了，保证这个map里面的数据跟数据库里面一致
	 * 每次修改数据，先修改这个缓存，再写入数据库 下面2个map引用的是同一份数据
	 */
	// key为playerID
	private Map<String, User> playerMap = new ConcurrentHashMap<String, User>();
	// key为playerAccount
	private Map<String, User> playerAccountMap = new ConcurrentHashMap<String, User>();
	// key为machineCode
	private Map<String, User> playerMachineCodeMap = new ConcurrentHashMap<String, User>();

	// qq Map
	private Map<String, User> playerQQMap = new ConcurrentHashMap<String, User>();
	// wx Map
	private Map<String, User> playerWXMap = new ConcurrentHashMap<String, User>();

	private Map<String, IoSession> oldSession = new ConcurrentHashMap<String, IoSession>();
	@Autowired
	private IMin2Nodify min2Nodify;
	/**
	 * 抽奖奖品列表
	 */
	private List<Prize> prizeLists = new ArrayList<Prize>();

	public IMin2Nodify getMin2Nodify() {
		return min2Nodify;
	}

	public void setMin2Nodify(IMin2Nodify min2Nodify) {
		this.min2Nodify = min2Nodify;
	}

	/**
	 * 奖励物品列表
	 */
	private List<Reward> rewardLists = new ArrayList<Reward>();

	/**
	 * 跑马灯消息
	 */
	private List<ScrollMsg> scrollMsgs = new ArrayList<ScrollMsg>();

	/**
	 * 定时跑马灯消息
	 */
	private List<ScrollMsg> onTimeScrollMsgs = new ArrayList<ScrollMsg>();

	private List<ShopGoods> shopGoods = new ArrayList<ShopGoods>();

	private Map<String, MobileCodeLimitedPhone> limitedPhoneLists = new ConcurrentHashMap<String, MobileCodeLimitedPhone>();

	private AsyncDatabaseThread dbThread = null;

	private AsyncPayThread payThread = null;
	private AsyncAliPayThread aliPayThread = null;
	private AsyncIpaPayThread ipaPayThread = null;

	private IUserDAO userDAO;
	private IUserFriendDAO userFriendDAO;
	private IPlayerLogDAO playerLogDAO;
	private IPrizeDAO prizeDAO;
	private IRewardDAO rewardDAO;
	private ILoginLogDAO loginLogDAO;
	private IUserBackpackDAO userBackpackDAO;
	private IMallItemDAO mallItemDAO;
	private IAvgPeopleOnlineDAO avgPeopleOnlineDAO;
	private IMallHistoryDAO mallHistoryDAO;
	private IVipGameRecordDAO vipGameRecordDAO;
	private IVipRoomRecordDAO vipRoomRecordDAO;
	private IProxyVipRoomRecordDAO proxyVipRoomRecordDAO;// 代开优化 cc modify
															// 2017-9-26
	private INormalGameRecordDAO normalGameRecordDAO;
	private IPlayerPlayLogDAO playerPlayLogDAO;
	private IPlayerHuTypeDAO playerHuTypeDAO;
	private IShopGoodsDAO shopGoodsDAO;
	private IClubMemberDAO clubMemberDAO;

	private IWelFareDAO welFareDAO;
	/** 俱乐部 */
	private ITClubDao tClubDao;
	/** 俱乐部排名 */
	private IClubRankingListDAO clubRankingListDAO;
	/** 俱乐部的DAO对象 */
	private IClubTemplate clubTemplateDao;

	public IWelFareDAO getWelFareDAO() {
		return welFareDAO;
	}

	public void setWelFareDAO(IWelFareDAO welFareDAO) {
		this.welFareDAO = welFareDAO;
	}

	// private IEntranceUserService entranceService = (IEntranceUserService)
	// SpringService
	// .getBean("entranceUserService");

	public IPlayerPlayLogDAO getPlayeruserDAO() {

		return playerPlayLogDAO;
	}

	public void setPlayerPlayLogDAO(IPlayerPlayLogDAO playerPlayLogDAO) {
		this.playerPlayLogDAO = playerPlayLogDAO;
	}

	private List<MallItem> baseItemList = null;

	private LoginService loginService;

	private IPayService payService;

	// 用于客户端显示的大礼包字段
	private String bigRewardStringForClient = "";
	// 上次循环的毫秒数
	private long old_loop_time_ms = 0L;
	//
	private int oldDay = 0;
	// player表里面有多少注册量了
	private int regNum = 0;
	private SystemConfigPara playerIndexConfig;
	//
	private ISystemConfigService cfgService = null;
	// 代开优化 cc modify 2017-9-26
	private Map<String, List<ProxyVipRoomRecord>> proxyVipRoomRecord = new ConcurrentHashMap<String, List<ProxyVipRoomRecord>>();

	/**
	 * 系统公告
	 */
	private ISystemNoticeDAO systemNoticeDAO;
	private SystemNotice sysNotice;
	private SystemNotice foreverNotice;// 固定跑马灯createVipRoom
	private SystemNotice qyqNotice;// 俱乐部消息-lxw20180903
	private SystemNotice hallNotice;// 大厅公告消息

	/**
	 * 首次登录送房卡
	 */
	private Map<String, String> mapFirstLogin = new HashMap<String, String>();

	private com.chess.nndzz.table.TableLogicProcess tableLogic_nndzz = null;

	public CacheUserService() {
		oldDay = DateService.getCurrentDay();
		// nndzz
		tableLogic_nndzz = new com.chess.nndzz.table.TableLogicProcess(this);
		tableLogic_nndzz.init();
	}

	public void updateRoom(SystemConfigPara cfg) {
		tableLogic_nndzz.updateRoom(cfg);
	}

	public void playerTalkingInGame(User pl, TalkingInGameMsg msg) {
		tableLogic_nndzz.playerTalkingInGame(pl, msg);
	}

	public void playerSendHuDongBiaoQing(User pl, HuDongBiaoQingMsg msg) {
	}

	public MallItem getMallItem(int item_base_id) {
		for (int i = 0; i < baseItemList.size(); i++) {
			MallItem it = baseItemList.get(i);
			if (it.getBase_id() == item_base_id)
				return it;
		}

		return null;
	}

	// 返回一个基础道具列表，不包括金币包
	public List<MallItem> getPropertyItemBaseList() {
		List<MallItem> lt = new ArrayList<MallItem>();
		for (int i = 0; i < baseItemList.size(); i++) {
			MallItem it = baseItemList.get(i);
			if (it.getBase_id() > 2000 && it.getBase_id() < 3000) {
				lt.add(it);
			}
		}
		return lt;
	}

	public void min2Nodify(MsgBase msg, User pl) {
		this.min2Nodify.nodify(msg, pl);
	}

	public void refreshItemBaseTable() {
		baseItemList = mallItemDAO.getAll();
	}

	public List<MallItem> getAllItemBaseTable() {
		return baseItemList;
	}

	public String modify_base_item(MallItem base_item) {
		for (int i = 0; i < baseItemList.size(); i++) {
			MallItem itb = baseItemList.get(i);
			if (itb.getBase_id() == base_item.getBase_id()) {
				//
				mallItemDAO.update(base_item);
				//
				baseItemList.set(i, base_item);
				break;
			}
		}

		return "success";
	}

	public String add_base_item(MallItem base_item) {
		baseItemList.add(base_item);
		mallItemDAO.createItemBase(base_item);
		return "success";
	}

	//
	public void load_base_tables() {
		baseItemList = mallItemDAO.getAll();
		prizeLists = prizeDAO.getPrizeList();
		rewardLists = rewardDAO.getRewardList();
		//
		regNum = userDAO.getTotalCount(null, null, 0);

		// sysNotice = systemNoticeDAO.getSystemNotice();
		// foreverNotice = systemNoticeDAO.getForeverMsg();

		// 获取消息类逻辑调整-lxw20180903
		List<SystemNotice> notices = systemNoticeDAO.getNoticeAll();
		if (notices != null) {
			for (SystemNotice n : notices) {
				if (0 == n.getNotice_type()) {// 系统消息
					sysNotice = n;
				} else if (1 == n.getNotice_type()) {// 跑马灯消息
					foreverNotice = n;
				} else if (5 == n.getNotice_type()) {// 俱乐部消息
					qyqNotice = n;
				}else if (10 == n.getNotice_type()) {// 大厅消息
					hallNotice = n;
				}

			}
		}
		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		this.playerIndexConfig = cfgService
				.getPara(ConfigConstant.INDEX_FOR_NEXT_REG_PLAYER);
		this.tableLogic_nndzz.loadRoomSettings();
	}

	private void initPayThread() {

		ipaPayThread = new AsyncIpaPayThread();
		ipaPayThread.start();

		aliPayThread = new AsyncAliPayThread();
		aliPayThread.start();

		payThread = new AsyncPayThread();
		payThread.start();

	}

	// 断线重链接
	public void reconnect(ReconnectMsg msg, IoSession session) {
		if (msg.account != null && msg.account.length() > 1
				&& msg.password != null && msg.password.length() > 1) {
			User pl = playerAccountMap.get(msg.account.toLowerCase());
			if (pl != null && msg.password.equals(pl.getPassword())) {
				pl.setSession(session);
				// 附加到属性上，方便查找
				session.setAttribute(NetConstant.PLAYER_SESSION_KEY, pl);
			}
		} else if (msg.machineCode != null && msg.machineCode.length() > 1) {
			User pl = playerMachineCodeMap.get(msg.machineCode);
			if (pl != null) {
				pl.setSession(session);
				// 附加到属性上，方便查找
				session.setAttribute(NetConstant.PLAYER_SESSION_KEY, pl);
			}
		} else if (msg.qqOpenID != null && !msg.qqOpenID.equals("")) {
			User pl = playerQQMap.get(msg.qqOpenID);
			if (pl != null) {
				pl.setSession(session);
				// 附加到属性上，方便查找
				session.setAttribute(NetConstant.PLAYER_SESSION_KEY, pl);
			}
		}
		// else if (msg.wxOpenID != null && !msg.wxOpenID.equals("")) {
		// User pl = playerWXMap.get(msg.wxOpenID);
		// if (pl != null) {
		// pl.setSession(session);
		// // 附加到属性上，方便查找
		// session.setAttribute(NetConstant.PLAYER_SESSION_KEY, pl);
		// }
		// }

		// cc modify 2017-12-5
		else if (msg.wxUnionID != null && !msg.wxUnionID.equals("")) {
			User pl = playerWXMap.get(msg.wxUnionID);
			if (pl != null) {
				pl.setSession(session);
				// 附加到属性上，方便查找
				session.setAttribute(NetConstant.PLAYER_SESSION_KEY, pl);
			}
		}
	}

	private void initDataBaseThread() {
		dbThread = new AsyncDatabaseThread();
		//
		dbThread.setUserDAO(userDAO);
		dbThread.setPlayerLogDao(playerLogDAO);
		dbThread.setUserBackpackDAO(userBackpackDAO);
		dbThread.setLoginLogDAO(loginLogDAO);
		dbThread.setPlayerFriendDAO(userFriendDAO);

		dbThread.setNormalGameRecordDAO(normalGameRecordDAO);
		dbThread.setVipRoomRecordDAO(vipRoomRecordDAO);
		dbThread.setProxyVipRoomRecordDAO(proxyVipRoomRecordDAO);// 代开优化 cc
																	// modify
																	// 2017-9-26
		dbThread.setVipGameRecordDAO(vipGameRecordDAO);
		dbThread.setPlayerPlayLogDAO(playerPlayLogDAO);
		dbThread.setClubMemberDAO(clubMemberDAO);

		// 线程启动

		dbThread.start();

		/*
		 * palyer_log_thread = new AsyncDBUpdateThread();
		 * palyer_log_thread.setuserDAO(userDAO);
		 * palyer_log_thread.setPlayerLogDao(playerLogDAO);
		 * palyer_log_thread.setuserBackpackDAO(userBackpackDAO);
		 * palyer_log_thread.setPlayerExtDAO(playerExtDAO);
		 * palyer_log_thread.setLoginLogDAO(loginLogDAO);
		 * palyer_log_thread.setPlayerFriendDAO(userFriendDAO);
		 * 
		 * palyer_log_thread.setNormalGameRecordDAO(normalGameRecordDAO);
		 * palyer_log_thread.setVipRoomRecordDAO(vipRoomRecordDAO);
		 * palyer_log_thread.setVipGameRecordDAO(vipGameRecordDAO);
		 * 
		 * // 线程启动 palyer_log_thread.start();
		 */

	}

	public void regNewPlayer(RegisterPlayerMsg msg, IoSession session) {
		if (msg == null || msg.account == null || msg.account.length() < 3
				|| msg.password == null || msg.password.length() < 3
				|| msg.account.length() > 64 || msg.password.length() > 64)
			return;

		User pl = userDAO.getPlayerByAccount(msg.account);
		LoginMsgAck ack = new LoginMsgAck();
		ack.rooms = this.tableLogic_nndzz.getGameRoomList();

		if (pl != null) {
			ack.result = ErrorCodeConstant.USER_ALREADY_EXIST;
			GameContext.gameSocket.send(session, ack);
			logger.error("注册信息: 用户注册信息重复！");
			return;
		}

		/*
		 * pl = userDAO.getPlayerByName(msg.nickname);
		 * 
		 * if (pl != null) { ack.result = ErrorCodeConstant.USER_ALREADY_EXIST;
		 * SystemConfig.gameSocketServer.sendMsg(session, ack); return; }
		 */

		this.createPlayer(msg.account, msg.password, "", msg.nickname,
				msg.deviceFlag, session);
		return;// 创建新用户
	}

	public void show_notic_oncreatevip(User pl, IoSession session) {
		// ShowNoticeOnCreateVipAck ack = new ShowNoticeOnCreateVipAck();
		// if (cfgService.show_notice_oncreatevip() == 1) {
		// ack.show = 1;
		// } else {
		// ack.show = 0;
		// }
		// GameContext.gameSocket.send(session, ack);
	}

	public void show_notic_onlogin(IoSession session) {
		if (cfgService.show_notice_onlogin() == 0)
			return;
		ShowNoticeOnLoginAck ack = new ShowNoticeOnLoginAck();
		ack.show = cfgService.show_notice_onlogin();
		ack.delay = cfgService.get_show_notice_onlogin_delay();
		ack.full_screen = cfgService.get_show_notice_onlogin_fullscreen();
		ack.show_time = cfgService.get_show_notice_onlogin_showtime();
		ack.auto_close = cfgService.get_show_notice_onlogin_autoclose();
		GameContext.gameSocket.send(session, ack);
	}

	public void login(LoginMsg msg, IoSession session) {
		LoginMsgAck ack = new LoginMsgAck();
		ack.rooms = this.tableLogic_nndzz.getGameRoomList();
		ack.result = ErrorCodeConstant.USER_NOT_FOUND;

		ISystemConfigService cfgService = (ISystemConfigService) SpringService
				.getBean("sysConfigService");
		SystemConfigPara configPara = cfgService
				.getPara(ConfigConstant.IS_ALLOW_SHOW_MSG);
		int nPara = 0;
		if (null != configPara) {
			nPara = configPara.getValueInt();
		}
		ack.isShowMsg = nPara;

		// 积分商城 xuyingquan
		SystemConfigPara configPara1 = cfgService
				.getPara(ConfigConstant.IS_START_CREDITS_EXCHANGE);
		int nPara1 = 0;
		if (null != configPara1) {
			nPara1 = configPara1.getValueInt();
		}
		ack.isStartCredits = nPara1;

		ack.shopGoods = ShopShowGoods();
		if (sysNotice != null) {
			ack.noticeIsExit = sysNotice.getIs_exit();
			ack.noticeTitle = sysNotice.getNotice_title();
			ack.noticeContent = sysNotice.getNotice_content();
			// 俱乐部消息默认赋值，如果有俱乐部消息，则覆盖-lxw20180903
			ack.qyqNoticeTitle = sysNotice.getNotice_title();
			ack.qyqNoticeContent = sysNotice.getNotice_content();
		}

		if (foreverNotice != null) {

			ack.foreverContent = foreverNotice.getNotice_content();
			ack.foreverOpen = foreverNotice.getIs_open();
		}

		if (qyqNotice != null) {
			ack.qyqNoticeTitle = qyqNotice.getNotice_title();
			ack.qyqNoticeContent = qyqNotice.getNotice_content();
		}
		
		if (hallNotice != null) {
			ack.hallNoticeTitle = hallNotice.getNotice_title();
			ack.hallNoticeContent = hallNotice.getNotice_content();
		}

		logger.info("playerbane:" + msg.userName);
		if (msg.machineCode != null) {
			logger.info("cc machineCode:" + msg.machineCode);
		}
		//
		int max_online = cfgService.get_max_online_num();

		int linkNum = GameContext.gameSocket.getManagedSessionCount();

		// 看看游戏在线人数是否最大
		if (linkNum >= max_online) {
			ack.result = ErrorCodeConstant.SERVER_IS_BUSY;
			GameContext.gameSocket.send(session, ack);
			logger.error("在线人数已经达到最大!登录失败!");
			return;
		}

		User pl = null;
		boolean login_successfully = false;

		if (msg.account != null && msg.account.length() > 1) {

			pl = playerAccountMap.get(msg.account.toLowerCase());
			if (pl == null) {
				pl = userDAO.getPlayerByAccount(msg.account);

				if (pl == null) {
					// this.createPlayer(msg.account, msg.password,
					// msg.machineCode, msg.userName, session);
					// return;// 创建新用户
					ack.result = ErrorCodeConstant.USER_NOT_FOUND;
					GameContext.gameSocket.send(session, ack);
					return;
				} else if (pl.getPassword().equals(msg.password)) {
					login_successfully = true;
				} else {
					ack.result = ErrorCodeConstant.WRONG_PASSWORD;
				}
			} else if (pl.getPassword().equals(msg.password)) {
				login_successfully = true;
			} else {
				ack.result = ErrorCodeConstant.WRONG_PASSWORD;
			}

		} else if (msg.machineCode != null && msg.machineCode.length() > 1) {
			pl = playerMachineCodeMap.get(msg.machineCode);
			if (pl == null) {
				pl = userDAO.getPlayerByMachineCode(msg.machineCode);
				//
				if (pl == null) {
					// 立即登录创建新用户
					logger.error("立即登录创建新用户请求！");
					mapFirstLogin.put(msg.machineCode, "1");
					this.createPlayer(msg.account, msg.password,
							msg.machineCode, msg.userName, msg.deviceFlag,
							session);
					return;// 创建新用户
				}
				//
				login_successfully = true;
			} else {
				login_successfully = true;
			}
		}
		/*
		 * else if (msg.qqOpenID != null && msg.qqOpenID.length() >= 25) { pl =
		 * playerQQMap.get(msg.qqOpenID); if (pl == null) { pl =
		 * userDAO.getPlayerByQQOpenID(msg.qqOpenID); // if (pl == null) { //
		 * QQ登录创建新用户 logger.error("QQ登录创建新用户请求！");
		 * mapFirstLogin.put(msg.qqOpenID, "1"); this.createPlayer(msg.qqOpenID,
		 * "", msg.userName, msg.deviceFlag, "", session, msg); return;// 创建新用户
		 * 
		 * } // login_successfully = true; } else { login_successfully = true; }
		 * }
		 */
		// else if (msg.wxOpenID != null) {// && msg.wxOpenID.length() >= 25
		// pl = playerWXMap.get(msg.wxOpenID);
		else if (msg.unionId != null) {// cc modify 2017-12-5
			pl = playerWXMap.get(msg.unionId);
			if (pl == null) {
				// pl = userDAO.getPlayerByWXOpenID(msg.wxOpenID);
				pl = userDAO.getPlayerByWXUnionID(msg.unionId);
				//
				if (pl == null) {
					// 微信登录创建新用户
					logger.error("微信登录创建新用户请求！");
					// mapFirstLogin.put(msg.wxOpenID, "1");
					mapFirstLogin.put(msg.unionId, "1");
					this.createPlayer("", msg.wxOpenID,
							filterEmojiUserName(msg.userName), msg.deviceFlag,
							msg.unionId, session, msg);
					return;// 创建新用户

				}
				//
			} else {
				userDAO.updatePlayerUnionID(pl.getPlayerID(), msg.unionId);
				userDAO.updatePlayerHeadImgUrl(pl.getPlayerID(), msg.headImgUrl);
				userDAO.updatePlayerName(pl.getPlayerID(),
						filterEmojiUserName(msg.userName));
				userDAO.updatePlayerSex(pl.getPlayerID(), msg.sex);
				pl.setSex(msg.sex);
			}
			// cc modify 2017-12-5
			login_successfully = true;
			if (pl.getUnionID() == null || "".equals(pl.getUnionID().trim())) {
				userDAO.updatePlayerUnionID(pl.getPlayerID(), msg.unionId);
			}
		}

		//
		if (login_successfully) {

			if (!isSameReLoginIp(pl, session)) {
				if (pl.isOnline() && pl.getSession() != null
						&& pl.getSession().isBothIdle() == false) {
					OtherLoginMsgAck otherAck = new OtherLoginMsgAck();
					GameContext.gameSocket.send(pl.getSession(), otherAck);
					// return;
				} else {

					User temPl = playerMap.get(pl.getPlayerID());
					if (temPl != null && temPl.isOnline()
							&& temPl.getSession() != null
							&& temPl.getSession().isBothIdle() == false) {
						OtherLoginMsgAck otherAck = new OtherLoginMsgAck();
						GameContext.gameSocket.send(pl.getSession(), otherAck);
						// return;
					}
				}
			}

			// 更新ip情况 ,获取到 clientIp 可能是 ip:port 或 ip 格式，需要解析出ip地址和端口号，分别存储到不同列
			String clientIp = msg.ip;
			if (clientIp != null && !"".equals(clientIp.trim())) {
				String[] ipAndPorts = clientIp.split(":");
				pl.setClientIP(ipAndPorts[0]);
				if (ipAndPorts.length >= 2) {
					pl.setHttpClientPort(ipAndPorts[1]);
				}
			}
			pl.setOnline(true);
			// 客户端手机设备相关信息
			pl.setDeviceBrand(msg.deviceBrand);
			pl.setSystemVersion(msg.systemVersion);
			pl.setPhone(msg.phone);
			pl.setHeadImgUrl(msg.headImgUrl);
			pl.setPlayerName(filterEmojiUserName(msg.userName));
			pl.setLocation(msg.location);
			// pl.setSex(msg.sex);
			// 清空离线时间
			pl.setOfflineTime(null);

			playerMap.put(pl.getPlayerID(), pl);

			if (pl.getAccount() != null && !pl.getAccount().equals(""))
				playerAccountMap.put(pl.getAccount().toLowerCase(), pl);
			if (pl.getMachineCode() != null && !pl.getMachineCode().equals(""))
				playerMachineCodeMap.put(pl.getMachineCode(), pl);
			if (pl.getQqOpenID() != null && !pl.getQqOpenID().equals(""))
				playerQQMap.put(pl.getQqOpenID(), pl);
			// if (pl.getWxOpenID() != null && !pl.getWxOpenID().equals(""))
			// playerWXMap.put(pl.getWxOpenID(), pl);
			if (pl.getUnionID() != null && !pl.getUnionID().equals("")) {
				playerWXMap.put(pl.getUnionID(), pl);// cc modify 2017-12-5
			}

			// this.check_save(pl);
			// 记录玩家登录设备【1-IOS】【2-Android】
			pl.setDeviceFlag(msg.deviceFlag);

			IoSession old_ses = pl.getSession();

			post_login_succ(ack, pl, session);

			if (old_ses != null) {
				old_ses.close();// 是否一个玩家同时登录几个地方，先断了老的
				old_ses = null;
			}
		}

		// 二人麻将
		sendTwoChessSupport(session);
		//
		System.out.println(" 登录玩家【" + pl.getPlayerIndex()
				+ "】，getClubType() : " + pl.getClubType());
		GameContext.gameSocket.send(session, ack);
		// show_bind_phone(pl, session);
		// 创建房间时显示信息
		show_notic_oncreatevip(pl, session);
		// 显示公告
		show_notic_onlogin(session);
		// 带支对功能
		sendZhiduiSupport(session);

		/*
		 * if(cfgService.get_zhidui_support() == 1) { ZhiDuiMsgAck zhidui_ack =
		 * new ZhiDuiMsgAck(); zhidui_ack.zhidui_support = 1;
		 * SystemConfig.gameSocketServer.sendMsg(session, zhidui_ack); } else {
		 * if (cfgService.get_zhidui_support() == 0) { ZhiDuiMsgAck zhidui_ack =
		 * new ZhiDuiMsgAck(); zhidui_ack.zhidui_support = 0;
		 * SystemConfig.gameSocketServer.sendMsg(session, zhidui_ack); } }
		 */
		// 测试代码
		// ---------测试代码end
	}

	public void login2(Login2Msg msg, IoSession session) {
		//
		session.setAttribute(NetConstant.LINK_CHECK_RESULT,
				NetConstant.LINK_CHECK_OK);
		session.setAttribute(NetConstant.LINK_NAME, msg.linkName);

		User pl = null;
		if (msg.account != null && msg.account.length() > 1) {
			pl = playerAccountMap.get(msg.account.toLowerCase());
		} else if (msg.machineCode != null && msg.machineCode.length() > 1) {
			pl = playerMachineCodeMap.get(msg.machineCode);
		} else if (msg.qqOpenID != null && msg.qqOpenID.length() >= 25) {
			pl = playerQQMap.get(msg.qqOpenID);
		} else if (msg.wxOpenID != null) {
			pl = playerWXMap.get(msg.wxOpenID);
		}

		Login2MsgAck ack = new Login2MsgAck();
		ack.operate = msg.operate;
		ack.vipTableId = msg.vipTableId;
		if (pl == null) {
			// 出问题了
			ack.result = ErrorCodeConstant.USER_NOT_FOUND;
		} else {

			// IoSession old_ses = pl.getSession();
			// if (old_ses != null) {
			// old_ses.close();
			// old_ses = null;
			// }

			// 附加到属性上，方便查找
			session.setAttribute(NetConstant.PLAYER_SESSION_KEY, pl);
			pl.setSession(session);

			ack.result = ErrorCodeConstant.CMD_EXE_OK;
			ack.player = pl;

			pl.setOnline(true);

			// 清空离线时间
			pl.setOfflineTime(null);

			playerMap.put(pl.getPlayerID(), pl);

			if (pl.getAccount() != null && !pl.getAccount().equals(""))
				playerAccountMap.put(pl.getAccount().toLowerCase(), pl);
			if (pl.getMachineCode() != null && !pl.getMachineCode().equals(""))
				playerMachineCodeMap.put(pl.getMachineCode(), pl);
			if (pl.getQqOpenID() != null && !pl.getQqOpenID().equals(""))
				playerQQMap.put(pl.getQqOpenID(), pl);
			if (pl.getWxOpenID() != null && !pl.getWxOpenID().equals(""))
				playerWXMap.put(pl.getWxOpenID(), pl);

			// this.check_save(pl);
			// 记录玩家登录设备【1-IOS】【2-Android】
			pl.setDeviceFlag(msg.deviceFlag);
		}

		// 发送消息
		GameContext.gameSocket.send(session, ack);
	}

	public void sendTwoChessSupport(IoSession session) {
		TwoPeopleSupportMsgAck ack = new TwoPeopleSupportMsgAck();
		if (cfgService.get_two_people_support() == 1) {
			ack.two_people_support = 1;
		} else {
			ack.two_people_support = 0;
		}
		GameContext.gameSocket.send(session, ack);
	}

	public void sendZhiduiSupport(IoSession session) {
		// 带支对功能
		if (cfgService.get_zhidui_support() == 1) {
			ZhiDuiMsgAck zhidui_ack = new ZhiDuiMsgAck();
			zhidui_ack.zhidui_support = 1;
			GameContext.gameSocket.send(session, zhidui_ack);
		} else {
			if (cfgService.get_zhidui_support() == 0) {
				ZhiDuiMsgAck zhidui_ack = new ZhiDuiMsgAck();
				zhidui_ack.zhidui_support = 0;
				GameContext.gameSocket.send(session, zhidui_ack);
			}
		}
	}

	private boolean isSameReLoginIp(User pl, IoSession newsession) {
		if (pl == null)
			return false;

		if (pl.getSession() == null)
			return false;

		String oldip = "" + pl.getSession().getRemoteAddress();
		String newip = "" + newsession.getRemoteAddress();

		// return (oldip.equals(newip));

		logger.error("old ip:" + oldip + "\n");
		logger.error("new ip:" + newip + "\n");

		/*
		 * oldip = oldip.substring(1); String[] oldips = oldip.split(":");
		 * oldip=oldips[0];
		 * 
		 * newip = newip.substring(1); String[] newips = newip.split(":");
		 * newip=newips[0];
		 */

		return (oldip.equals(newip));

	}

	public List<MallItem> getItemBaseByPhone(User pl, List<MallItem> allBaseList) {
		return allBaseList;
	}

	private void post_login_succ(LoginMsgAck ack, User pl, IoSession session) {

		// 处理登录领奖
		ack.reward = login_reward(pl);
		// 把每天登录领奖的奖励读过去
		ack.goldList.add(Integer
				.parseInt(getRewardValue(RewardConstant.DAY_1_REWARD)));
		ack.goldList.add(Integer
				.parseInt(getRewardValue(RewardConstant.DAY_2_REWARD)));
		ack.goldList.add(Integer
				.parseInt(getRewardValue(RewardConstant.DAY_3_REWARD)));
		ack.goldList.add(Integer
				.parseInt(getRewardValue(RewardConstant.DAY_4_REWARD)));
		ack.goldList.add(Integer
				.parseInt(getRewardValue(RewardConstant.DAY_5_REWARD)));

		ack.result = ErrorCodeConstant.CMD_EXE_OK;
		ack.lifeCD = GameContext.mainThread.get10minLeft();

		//
		ack.continueLanding = pl.getContinueLanding();
		ack.firstLogin = pl.getFirstLogin();
		//
		if (baseItemList == null) {
			refreshItemBaseTable();
		}
		ack.baseItemList = getItemBaseByPhone(pl, this.baseItemList);

		ack.clientParma = cfgService.getAllClientConfigPara();

		//
		/*
		 * if(bigRewardStringForClient==null ||
		 * bigRewardStringForClient.length()<1){ this.update_big_gift(); }
		 */
		// ack.bigGiftContent = this.bigRewardStringForClient;
		//
		ack.player = pl;

		ISystemConfigService cfgService = (ISystemConfigService) SpringService
				.getBean("sysConfigService");
		SystemConfigPara configPara = cfgService
				.getPara(ConfigConstant.IS_ALLOW_SHOW_MSG);
		int nPara = 0;
		if (null != configPara) {
			nPara = configPara.getValueInt();
		}
		ack.isShowMsg = nPara;

		// 积分商城 xuyingquan
		SystemConfigPara configPara1 = cfgService
				.getPara(ConfigConstant.IS_START_CREDITS_EXCHANGE);
		int nPara1 = 0;
		if (null != configPara1) {
			nPara1 = configPara1.getValueInt();
		}
		ack.isStartCredits = nPara1;

		ack.shopGoods = ShopShowGoods();
		if (sysNotice != null) {
			ack.noticeIsExit = sysNotice.getIs_exit();
			ack.noticeTitle = sysNotice.getNotice_title();
			ack.noticeContent = sysNotice.getNotice_content();
		}

		if (foreverNotice != null) {
			ack.foreverContent = foreverNotice.getNotice_content();
			ack.foreverOpen = foreverNotice.getIs_open();
		}
		if (qyqNotice != null) {
			ack.qyqNoticeTitle = qyqNotice.getNotice_title();
			ack.qyqNoticeContent = qyqNotice.getNotice_content();
		}
		if (hallNotice != null) {
			ack.hallNoticeTitle = hallNotice.getNotice_title();
			ack.hallNoticeContent = hallNotice.getNotice_content();
		}

		// 检查下是否老玩家，是的话重新给个索引号
		if (pl.getPlayerIndex() == 1000) {
			int nextPlayerIndex = this.getNextPlayerIndex();
			pl.setPlayerIndex(nextPlayerIndex);
			userDAO.updatePlayerIndex(pl.getPlayerID(), nextPlayerIndex);
		}
		// 加载玩家道具到缓存
		load_player_items(pl);
		/**
		 * 改版处理金币：用户金币小于20000时，重新初始化为20000 20161014 
		 */
		if (pl.getGold() != 20000) {
			logger.debug("----改版处理金币=20000：playerID = " + pl.getPlayerName()
					+ " ------");
			pl.setGold(20000);
			DBOperation msg = new DBOperation();
			msg.dao = DBConstant.USER_DAO;
			msg.opertaion = DBConstant.USER_DAO_updatePlayerGold;
			msg.playerID = pl.getPlayerID();
			msg.gold = 20000;
			dbThread.pushMsg(msg);
		}
		/*
		 * 处理主产品ID cc 2017-12-20
		 */
		SystemConfigPara configProduct = cfgService
				.getPara(ConfigConstant.MAIN_PRODUCT_ID);
		String mainProductId = "";
		if (configProduct != null && configProduct.getValueStr() != null
				&& !"".equals(configProduct.getValueStr())) {
			mainProductId = configProduct.getValueStr();
		}
		if (pl.getGameId() == null || "".equals(pl.getGameId().trim())) {
			pl.setGameId(mainProductId);
		}
		// 关闭产品
		ack.closedProducts = cfgService.getClosedProducts();
		// 俱乐部上限
		ack.clubCountMax = cfgService.getClubCountMax();

		/**
		 * 临时处理：用户VIP房卡 = 0 时，无条件处理成100 2016-08-16 
		 * start======================
		 * ===============================================================
		 */
		logger.info("----重新登录：临时处理房卡：playerID = " + pl.getPlayerName()
				+ " ------");
		List<UserBackpack> listBackpacks = pl.getItems();
		if ("1".equals(mapFirstLogin.get(pl.getMachineCode()))
				// || "1".equals(mapFirstLogin.get(pl.getWxOpenID()))
				|| "1".equals(mapFirstLogin.get(pl.getUnionID()))
				|| "1".equals(mapFirstLogin.get(pl.getPlayerID()))) {
			// 只有首次登录才送卡
			logger.error("---只有首次登录才送卡 cc modify----");
			mapFirstLogin.put(pl.getMachineCode(), "0");
			// mapFirstLogin.put(pl.getWxOpenID(), "0");
			mapFirstLogin.put(pl.getUnionID(), "0");
			mapFirstLogin.put(pl.getPlayerID(), "0");

			int sendKaNum = 0;
			SystemConfigPara configPara2 = cfgService
					.getPara(ConfigConstant.LOGIN_FIRST_SEND_FANGKA_NUM);
			if (configPara2 != null) {
				sendKaNum = configPara2.getValueInt();
				logger.info("----首次登录送卡：playerID = " + pl.getPlayerName()
						+ " = " + sendKaNum + "张------");
			}

			// TODO 首次登陆送卡 添加日志  2017.3.21
			this.createSendCardLog(pl, 3333, sendKaNum,
					LogConstant.SEND_CARD_FIRST_LOGIN);

			if (listBackpacks.size() == 0) {
				createItemForPlayer(pl, 3333, "4圈房卡",
						LogConstant.OPERATION_TYPE_ADMIN_CHANGE_USER_DATA,
						sendKaNum, pl.getGameId());
			} else {
				boolean isExist = false;
				boolean isNeed = false;
				for (int i = 0; i < listBackpacks.size(); i++) {
					UserBackpack userBackpack = listBackpacks.get(i);
					if (userBackpack.getItemBaseID() == 3333) {
						isExist = true;
						int num = userBackpack.getItemNum();
						if (num == 0) {
							isNeed = true;
						}
					}
				}
				if (isExist == false) {
					createItemForPlayer(pl, 3333, "4圈房卡",
							LogConstant.OPERATION_TYPE_ADMIN_CHANGE_USER_DATA,
							sendKaNum, pl.getGameId());
				} else if (isNeed) {
					createItemForPlayer(pl, 3333, "4圈房卡",
							LogConstant.OPERATION_TYPE_ADMIN_CHANGE_USER_DATA,
							sendKaNum, pl.getGameId());
				}
			}
		}
		/**
		 * 临时处理：用户VIP房卡 = 0 时，无条件处理成100 2016-08-16 
		 * end========================
		 * =============================================================
		 */

		// 根据玩家登录的设备，将房卡道具转成该设备的道具
		// refresh_player_items(pl);

		// load player degree

		String ip = "" + session.getRemoteAddress();
		ip = ip.substring(1);
		String[] ips = ip.split(":");
		if (ips != null && ips.length == 2)
			pl.setProxyIp(ips[0]);

		// 附加到属性上，方便查找
		session.setAttribute(NetConstant.PLAYER_SESSION_KEY, pl);
		pl.setSession(session);

		//
		fix_up(pl);

		// 好友列表
		ack.friends = userFriendDAO.getPlayerFriends(pl.getPlayerID());

		FriendOnLineMsgAck onlineAck = new FriendOnLineMsgAck();
		onlineAck.friendID = pl.getPlayerID();
		onlineAck.isOnline = 1;
		for (UserFriend friend : ack.friends) {
			User frPlayer = playerMap.get(friend.playerID);
			if (frPlayer != null) {
				friend.isOnline = frPlayer.isOnline() ? 1 : 0;
				GameContext.gameSocket.send(frPlayer.getSession(), onlineAck);
			}

		}
		// 推荐人
		pl.setMgrIndex(userDAO.getPlayerMgrIndex(pl.getPlayerID()));

		// 登录成功，记录下玩家的登录日志
		long time1 = DateService.getCurrentUtilDate().getTime();
		loginService.createPlayerLoginInRecord(pl);
		long time2 = DateService.getCurrentUtilDate().getTime();
		logger.info("玩家登录成功，玩家ID【" + pl.getPlayerIndex() + "】，玩家昵称【"
				+ pl.getPlayerName() + "】，日志记录时间" + (time2 - time1));
		pl.show_bind_phone = true;

	}

	/**
	 * 登录计算钻石数量并且
	 */
	public List<Integer> countDiamonds(User pl, int reward) {
		List<Integer> list = new ArrayList<Integer>();
		if (reward == 0 || pl.getDiamond() <= 0) {
			return list;
		}

		// 如果当天首登且玩家点亮了钻石，要算出各种钻石数量

		int[] diamonds = { 0, 0, 0, 0, 0 };

		diamonds[0] = pl.getPlayerItemNumByBaseID(3333);
		diamonds[1] = pl.getPlayerItemNumByBaseID(3334);
		diamonds[2] = pl.getPlayerItemNumByBaseID(3335);
		diamonds[3] = pl.getPlayerItemNumByBaseID(3336);
		diamonds[4] = pl.getPlayerItemNumByBaseID(3337);

		//
		for (int i = 0; i < 5; i++) {
			int dia = diamonds[i];
			if (dia > 0) { // 钻石基础数据
				MallItem bi = getMallItem(3333 + i);
				// 道具ID
				int baseID = bi.getProperty_1();
				// 道具基础类
				MallItem pro = getMallItem(baseID);
				// 道具数量
				int counts = bi.getProperty_2();
				//
				createItemForPlayer(pl, baseID, pro.getName(),
						LogConstant.OPERATION_DIAMOND_SEND, counts * dia, "");
				//
				list.add(baseID);
				list.add(counts);
			} else {
				list.add(0);
				list.add(0);
			}
		}

		return list;
	}

	//
	public void scroll_msg_send(IoSession session) {
		long ct = DateService.getCurrentUtilDate().getTime();
		for (int i = scrollMsgs.size() - 1; i >= 0; i--) {
			ScrollMsg smsg = scrollMsgs.get(i);
			if (ct - smsg.createDate > smsg.loopNum * 10000) {
				scrollMsgs.remove(i);
			}
		}

		// 把之前的消息，都发给玩家
		for (int i = 0; i < scrollMsgs.size(); i++) {
			ScrollMsg smsg = scrollMsgs.get(i);
			GameContext.gameSocket.send(session, smsg);
		}

	}

	/**
	 * 从缓存里面取出用户信息
	 */
	public User getPlayerByPlayerIDFromCache(String playerID) {
		// return userDAO.getPlayerByAccount(playerID);
		User pl = this.playerMap.get(playerID);
		return pl;
	}

	public int getCacheOnlineNum() {
		int num = 0;
		// 加缓存
		for (String playerID : playerMap.keySet()) {
			User pl = playerMap.get(playerID);
			if (pl.isOnline())
				num++;
		}
		return num;
	}

	// 2人VIP游戏人数
	public int getCacheTwoOnline() {

		int num = 0;
		for (String playerID : playerMap.keySet()) {
			User pl = playerMap.get(playerID);
			if (pl.isTwoVipOnline())
				num++;
		}
		return num;
	}

	public boolean containsPlayer(String playerID) {
		return this.playerMap.containsKey(playerID);
	}

	public User getPlayerByPlayerIDAndGameID(String playerID, String gameID) {
		User pl = new User();
		pl = userDAO.getPlayerByIDAndGameID(playerID, gameID);
		return pl;
	}

	public User getPlayerByPlayerID(String playerID) {
		User pl = this.playerMap.get(playerID);
		if (pl == null) {// 查询下数据库
			logger.debug("pl catch is null");// System.out.print("pl catch is null");
			pl = userDAO.getPlayerByID(playerID);
			if (pl != null) {
				// 加入缓存
				this.playerMap.put(playerID, pl);
				if (pl.getAccount() != null && !"".equals(pl.getAccount())) {
					this.playerAccountMap
							.put(pl.getAccount().toLowerCase(), pl);
				}
				if (pl.getMachineCode() != null
						&& !"".equals(pl.getMachineCode())) {
					this.playerMachineCodeMap.put(pl.getMachineCode(), pl);
				}
				if (pl.getQqOpenID() != null && !"".equals(pl.getQqOpenID())) {
					this.playerQQMap.put(pl.getQqOpenID(), pl);
				}

				// if (pl.getWxOpenID() != null && !"".equals(pl.getWxOpenID()))
				// {
				// this.playerWXMap.put(pl.getWxOpenID(), pl);
				// }
				if (pl.getUnionID() != null && !"".equals(pl.getUnionID())) {
					this.playerWXMap.put(pl.getUnionID(), pl);// cc modify
																// 2017-12-5
				}

				// 加载玩家道具到缓存
				load_player_items(pl);
			}
		}
		return pl;
	}

	/**
	 * 通过playerIndex获取玩家信息，注意不加入缓存的，只为获取玩家信息
	 */
	public User getPlayerByPlayerIndex(int playerIndex) {
		User pl = this.userDAO.getPlayerByPlayerIndex(playerIndex);

		return pl;
	}

	/**
	 * 发送跑马灯消息
	 */
	public void sendSystemScrollMsg(String str, int loops, int should_clear,
			int sendPlayerType) {
		if (str != null && str.length() > 1 && str.length() < 256) {
			if (should_clear > 1)
				should_clear = 1;
			if (should_clear < 0)
				should_clear = 0;
			//
			Date ct = DateService.getCurrentUtilDate();
			ScrollMsg smsg3 = new ScrollMsg();
			smsg3.msg = str;
			if (loops > 0) {
				smsg3.loopNum = loops;
			} else {
				smsg3.loopNum = 1;
			}
			//
			smsg3.createDate = ct.getTime();
			//
			smsg3.removeAllPreviousMsg = should_clear;
			smsg3.sendPlayerType = sendPlayerType;
			//
			if (should_clear >= 1) {
				scrollMsgs.clear();
			}

			if (sendPlayerType > 0) {
				GameContext.gameSocket.sendMsgToTypePlayer(smsg3,
						sendPlayerType);
			} else {
				GameContext.gameSocket.sendMsgToAllSession(smsg3);
			}

			//
			scrollMsgs.add(smsg3);
		}
	}

	/**
	 * 添加定时跑马灯消息
	 */
	public void addOntimeSystemScrollMsg(String str, int loops,
			int should_clear, String sendTime, boolean clearOntimeList,
			int sendPlayerType, int ontimeType, int sendTimes) {
		if (str != null && str.length() > 1 && str.length() < 256) {
			if (should_clear > 1)
				should_clear = 1;
			if (should_clear < 0)
				should_clear = 0;

			ScrollMsg smsg3 = new ScrollMsg();
			smsg3.msg = str;
			if (loops > 0) {
				smsg3.loopNum = loops;
			} else {
				smsg3.loopNum = 1;
			}

			// 发送时间
			smsg3.sendTime = sendTime;
			smsg3.createDate = DateService.getDateByStrAndFormat(sendTime,
					"yyyy-MM-dd HH:mm:ss").getTime();
			smsg3.removeAllPreviousMsg = should_clear;
			smsg3.sendPlayerType = sendPlayerType;
			smsg3.ontimeType = ontimeType;
			smsg3.sendTimes = sendTimes;

			if (clearOntimeList) {
				onTimeScrollMsgs.clear();
			}
			// 先添加到消息队列，等时间到了再发送给客户端
			onTimeScrollMsgs.add(smsg3);
		}
	}

	/**
	 * 发送定时跑马灯消息(5分钟频率)
	 */
	public void sendOnTimeSystemScrollMsg() {
		if (onTimeScrollMsgs.size() <= 0) {
			return;
		}

		long ct = DateService.getCurrentUtilDate().getTime();
		List<ScrollMsg> sendList = new ArrayList<ScrollMsg>(); // 待发送队列
		sendList.clear();

		for (int i = onTimeScrollMsgs.size() - 1; i >= 0; i--) {
			ScrollMsg smsg = onTimeScrollMsgs.get(i);

			// 遍历一次性跑马灯和5分钟频率跑马灯
			if (ct > smsg.createDate && smsg.ontimeType != 2) {
				// 时间到了，添加到发送队列，准备发送给客户端
				sendList.add(smsg);
				smsg.sendTimes--;

				if (smsg.ontimeType == 0 || smsg.sendTimes <= 0) {
					// 从定时队列中清除掉
					onTimeScrollMsgs.remove(i);
				}
			}
		}

		// 到了时间的全部发送给客户端，注意要从后往前，这样时间顺序才是对的
		for (int i = sendList.size() - 1; i >= 0; i--) {
			ScrollMsg oneMsg = sendList.get(i);

			this.sendSystemScrollMsg(oneMsg.msg, oneMsg.loopNum,
					oneMsg.removeAllPreviousMsg, oneMsg.sendPlayerType);
		}
	}

	/**
	 * 发送30分钟频率定时跑马灯消息
	 */
	public void sendOneHourOnTimeSystemScrollMsg() {
		if (onTimeScrollMsgs.size() <= 0) {
			return;
		}

		long ct = DateService.getCurrentUtilDate().getTime();
		List<ScrollMsg> sendList = new ArrayList<ScrollMsg>(); // 待发送队列
		sendList.clear();

		for (int i = onTimeScrollMsgs.size() - 1; i >= 0; i--) {
			ScrollMsg smsg = onTimeScrollMsgs.get(i);

			// 遍历一次性跑马灯和5分钟频率跑马灯
			if (ct > smsg.createDate && smsg.ontimeType == 2) {
				// 时间到了，添加到发送队列，准备发送给客户端
				sendList.add(smsg);
				smsg.sendTimes--;

				if (smsg.sendTimes <= 0) {
					// 从定时队列中清除掉
					onTimeScrollMsgs.remove(i);
				}
			}
		}

		// 到了时间的全部发送给客户端，注意要从后往前，这样时间顺序才是对的
		for (int i = sendList.size() - 1; i >= 0; i--) {
			ScrollMsg oneMsg = sendList.get(i);

			this.sendSystemScrollMsg(oneMsg.msg, oneMsg.loopNum,
					oneMsg.removeAllPreviousMsg, oneMsg.sendPlayerType);
		}
	}

	// 设置固定跑马灯（创建或者修改）
	public void setForeverMsg(int isOpen, String msgContent, int msgId) {
		SystemNotice foreverMsg = new SystemNotice();
		if (msgId != 9999) {
			foreverMsg.setNotice_content(msgContent);
			foreverMsg.setIs_open(isOpen);
			foreverMsg.setNotice_type(1);
			foreverMsg.setIs_exit(1);
			foreverMsg.setNotice_title("");
			foreverMsg.setNotice_id(msgId);
			// 调用修改
			this.updateForeverNotice(foreverMsg);
		} else {
			foreverMsg.setNotice_content(msgContent);
			foreverMsg.setIs_open(isOpen);
			foreverMsg.setNotice_type(1);
			foreverMsg.setIs_exit(1);
			foreverMsg.setNotice_title("");

			// 调用插入
			systemNoticeDAO.createSystemNotice(foreverMsg);
			foreverNotice = systemNoticeDAO.getForeverMsg();
		}

	}

	// 获取固定跑马灯
	public SystemNotice getForeverMsg() {

		return systemNoticeDAO.getForeverMsg();
	}

	public void load_player_items(User pl) {
		List<UserBackpack> lt = userBackpackDAO.getPlayerItemListByPlayerID(pl
				.getPlayerID());
		pl.setItems(lt);
	}

	/**
	 * 根据玩家登录的设备，将房卡道具转成该设备的道具
	 */
	private void refresh_player_items(User pl) {
		if (null == pl) {
			return;
		}

		List<UserBackpack> allItemList = new ArrayList<UserBackpack>();
		allItemList.addAll(pl.getItems());

		if ((null == allItemList) || (allItemList.size() < 1)) {
			return;
		}

		if (pl.getDeviceFlag() == ConfigConstant.DEVICE_TYPE_IOS) {
			for (UserBackpack itemAndroid : allItemList) {
				int itemNum = itemAndroid.getItemNum();

				// 安卓道具转成IOS道具，3333是2圈房卡，IOS没有
				if ((itemAndroid.getItemBaseID() > 3000)// STiV modify
						// 3001->3000
						&& (itemAndroid.getItemBaseID() < 3200)
						&& (itemNum > 0)) {

					Integer android_BaseID = itemAndroid.getItemBaseID();
					String playerID = pl.getPlayerID();

					MallItem itemIOS = getMallItem(itemAndroid.getItemBaseID() + 0); // ios
					// //STiV
					// modify
					// 199->200
					if (itemIOS != null) {
						// 安卓道具减为0
						itemAndroid.setItemNum(0);
						userBackpackDAO.updatePlayerItemNum(playerID,
								android_BaseID, 0, "");
						// this.udpate_player_item_num(pl.getPlayerID(),
						// itemAndroid.getItemBaseID(), 0);
						// 增加IOS道具
						this.createItemForPlayer(pl, itemIOS.getBase_id(),
								itemIOS.getName(),
								LogConstant.OPERATION_CHANGE_ITEM_BY_DEVICE,
								itemNum, "ddz");
					}
				}
			}
		} else// if(pl.getDeviceFlag()==PlayerConstant.PLAYER_DEVICE_TYPE_ANDROID)
		{
			for (UserBackpack itemIOS : allItemList) {
				if (null != itemIOS) {
					int itemNum = itemIOS.getItemNum();

					// 安卓道具转成IOS道具
					if ((itemIOS.getItemBaseID() > 3200)
							&& (itemIOS.getItemBaseID() < 4000)
							&& (itemNum > 0)) {

						Integer ios_BaseID = itemIOS.getItemBaseID();
						String playerID = pl.getPlayerID();

						MallItem itemAndroid = getMallItem(itemIOS
								.getItemBaseID() - 200);// STiV modify 199->200
						if (itemAndroid != null) {
							// IOS道具减为0
							itemIOS.setItemNum(0);
							userBackpackDAO.updatePlayerItemNum(playerID,
									ios_BaseID, 0, "");
							// this.udpate_player_item_num(pl.getPlayerID(),
							// itemIOS.getItemBaseID(), 0);
							// 增加安卓道具
							this.createItemForPlayer(
									pl,
									itemAndroid.getBase_id(),
									itemAndroid.getName(),
									LogConstant.OPERATION_CHANGE_ITEM_BY_DEVICE,
									itemNum, "");
						}
					}
				}
			}
		}
	}

	private int login_reward(User pl) {
		int reward = 0;
		//
		Date earlyDay = pl.getLastLoginTime();
		Date lateDay = new Date();

		// 更新上次登录时间
		pl.setLastLoginTime(lateDay);
		long betweenDays = DateService.DayBetween(earlyDay, lateDay);

		// 判断是否连续登陆
		if (betweenDays == 1) {
			// 设置抽奖次数
			pl.setLuckyDrawsTimes((int) (Math.floor(Math.random() * 2) + 1));
			//
			// 如果连续登陆次数为5次，则重新计算，即刷新
			if (pl.getContinueLanding() >= 5) {
				pl.setContinueLanding(1);
			} else {
				// 如果相隔1天，则为连续登陆,同步到表中，并且发给客户端
				pl.setContinueLanding(pl.getContinueLanding() + 1);
			}
		} else if (betweenDays > 0) {// 两三天后回来登录的
			// 设置抽奖次数
			pl.setLuckyDrawsTimes((int) (Math.floor(Math.random() * 2) + 1));
			pl.setContinueLanding(1);
		}

		// 当天重复登录没有
		if (betweenDays != 0 || pl.getFirstLogin() == 1) {
			String rewardName = "";
			// 登陆奖励,看连续登录天数
			switch (pl.getContinueLanding()) {
			case 1:
				reward = Integer
						.parseInt(getRewardValue(RewardConstant.DAY_1_REWARD));
				break;
			case 2:
				reward = Integer
						.parseInt(getRewardValue(RewardConstant.DAY_2_REWARD));
				break;
			case 3:
				reward = Integer
						.parseInt(getRewardValue(RewardConstant.DAY_3_REWARD));
				break;
			case 4:
				reward = Integer
						.parseInt(getRewardValue(RewardConstant.DAY_4_REWARD));
				break;
			case 5:
				reward = Integer
						.parseInt(getRewardValue(RewardConstant.DAY_5_REWARD));
				// pl.setLuckyDrawsTimes(pl.getLuckyDrawsTimes()+1);
				break;
			}
			//

			//
			// add_player_gold(pl, reward, LogConstant.OPERATION_CONTINUE_LOGIN,
			// "连续登录" + pl.getContinueLanding() + "天，获得金币=" + reward);

		}

		// userDAO.updatePlayerLoginMsg(pl);
		DBOperation dbmsg = new DBOperation();
		dbmsg.dao = DBConstant.USER_DAO;
		dbmsg.opertaion = DBConstant.USER_DAO_updatePlayerLoginMsg;
		dbmsg.playerID = pl.getPlayerID();
		dbmsg.diamond = pl.getDiamond();
		dbmsg.gold = pl.getGold();
		dbmsg.date = pl.getLastLoginTime();
		dbmsg.continueLanding = pl.getContinueLanding();
		dbmsg.luckyDrawsTimes = pl.getLuckyDrawsTimes();
		AsyncDatabaseThread.pushDBMsg(dbmsg);

		return reward;

	}

	// 登录修复数据
	private void fix_up(User pl) {

	}

	//
	public void playerOffline(String playerID) {

		User pl = playerMap.get(playerID);
		if (pl != null) {
			Date ct = DateService.getCurrentUtilDate();
			//
			pl.setOfflineTime(ct);
			//
			pl.setFirstLogin(0);

			// 处理牌桌中玩家离开
			tableLogic_nndzz.player_left_table(pl, true, false);
			//
			pl.setSession(null);
			//
			// userDAO.updateOfflineTime(pl.getPlayerID(),ct);

			// 给好友发送离线信息
			// 好友列表
			List<UserFriend> friends = userFriendDAO.getPlayerFriends(pl
					.getPlayerID());
			FriendOnLineMsgAck onlineAck = new FriendOnLineMsgAck();
			onlineAck.friendID = pl.getPlayerID();
			onlineAck.isOnline = 0;
			for (UserFriend friend : friends) {
				User frPlayer = playerMap.get(friend.playerID);
				if (frPlayer != null) {
					friend.isOnline = frPlayer.isOnline() ? 1 : 0;
					GameContext.gameSocket.send(frPlayer.getSession(),
							onlineAck);
				}

			}

			DBOperation msg = new DBOperation();
			msg.dao = DBConstant.USER_DAO;
			msg.opertaion = DBConstant.USER_DAO_updateOfflineTime;
			msg.playerID = pl.getPlayerID();
			msg.date = ct;
			dbThread.pushMsg(msg);
		}

	}

	//
	public void schedule_10_minute() {
		UpdatePlayerPropertyMsg msg = new UpdatePlayerPropertyMsg();

		// 加缓存
		for (String playerID : playerMap.keySet()) {
			User pl = playerMap.get(playerID);
			if (pl.getLife() < 5) {
				pl.setLife(pl.getLife() + 1);
				//
				msg.diamond = pl.getDiamond();
				msg.escape = pl.getEscape();
				msg.gold = pl.getGold();
				msg.life = pl.getLife();
				msg.loses = pl.getLoses();
				msg.score = pl.getScore();
				msg.wons = pl.getWons();
				msg.serverCD = GameContext.mainThread.get10minLeft();
				GameContext.gameSocket.send(pl.getSession(), msg);
			}
		}
		// 加数据库
		// userDAO.addLifeNoOverflow();
	}

	//
	private void createPlayer(String account, String password,
			String machineCode, String userName, int deviceFlag,
			IoSession session) {
		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		int max_reg = cfgService.get_max_reg_player_num();
		// 看看游戏注册是否已经达到最大
		if (regNum >= max_reg) {
			LoginMsgAck ack = new LoginMsgAck();
			ack.result = ErrorCodeConstant.SERVER_IS_FULL;
			GameContext.gameSocket.send(session, ack);

			logger.error("快速登录，注册人数达到上限，注册人数:" + regNum + " 限制注册人数:" + max_reg);

			return;
		}

		// 过滤表情字符
		Pattern emoji = Pattern
				.compile(
						"[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\ud83e\udd00-\ud83e\udfff]|[\u2600-\u27ff]",
						Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

		Matcher emojiMatcher = emoji.matcher(userName);

		if (emojiMatcher.find()) {
			userName = "微信用户";
		}

		if (userName.length() > 12) {
			userName = userName.substring(0, 11);
		}

		EntranceUserMsg msg = new EntranceUserMsg();
		msg.account = account;
		msg.playerID = UUIDGenerator.generatorUUID();
		msg.machine_code = machineCode;
		msg.playerName = userName;
		msg.password = password;
		msg.sessionID = Integer.parseInt(String.valueOf(session.getId()));
		msg.serverID = GameContext.serverID;
		msg.playerIndex = this.getNextPlayerIndex();
		msg.deviceFlag = deviceFlag;
		// 发送给入口服务器
		GameContext.entranceServerConnector.send(msg);
	}

	// 根据qqOpenID或wxOpenID创建qq帐号
	private void createPlayer(String qqOpenID, String wxOpenID,
			String userName, int deviceFlag, String unionId, IoSession session,
			LoginMsg loginMsg) {
		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		//
		int max_reg = cfgService.get_max_reg_player_num();

		// 看看游戏注册是否已经达到最大
		if (regNum >= max_reg) {
			LoginMsgAck ack = new LoginMsgAck();
			ack.result = ErrorCodeConstant.SERVER_IS_FULL;
			GameContext.gameSocket.send(session, ack);
			logger.error("根据qqOpenID或wxOpenID创建qq帐号,注册人数达到上限，注册人数:" + regNum
					+ " 限制注册人数:" + max_reg);
			return;
		}

		// 过滤表情字符
		Pattern emoji = Pattern
				.compile(
						"[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\ud83e\udd00-\ud83e\udfff]|[\u2600-\u27ff]",
						Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
		Matcher emojiMatcher = emoji.matcher(userName);
		if (emojiMatcher.find()) {
			if (qqOpenID != "") {
				userName = "QQ用户";
			} else {
				userName = "微信用户";
			}
		}

		if (userName.length() > 12) {
			userName = userName.substring(0, 11);
		}

		EntranceUserMsg msg = new EntranceUserMsg();
		msg.account = "";
		msg.playerID = UUIDGenerator.generatorUUID();
		msg.machine_code = "";
		msg.playerName = userName;
		msg.password = "";
		msg.sessionID = Integer.parseInt(String.valueOf(session.getId()));
		msg.qqOpenID = qqOpenID;
		msg.wxOpenID = wxOpenID;
		msg.serverID = GameContext.serverID;
		msg.playerIndex = this.getNextPlayerIndex();
		msg.deviceFlag = deviceFlag;
		msg.wxUnionId = unionId;
		msg.param01 = "0";
		msg.ip = loginMsg.ip;
		msg.deviceBrand = loginMsg.deviceBrand;
		msg.systemVersion = loginMsg.systemVersion;
		msg.phone = loginMsg.phone;
		msg.location = loginMsg.location;
		msg.sex = loginMsg.sex;
		logger.error("根据qqOpenID或wxOpenID创建qq帐号，向入口服务器注册新帐号");
		// 发送给入口服务器
		GameContext.entranceServerConnector.send(msg);
	}

	public int getNextPlayerIndex() {

		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		this.playerIndexConfig = cfgService
				.getPara(ConfigConstant.INDEX_FOR_NEXT_REG_PLAYER);
		int nextPlayerIndex = this.playerIndexConfig.getValueInt();

		int goodnum = 0;
		String str = "";

		while (true) {
			nextPlayerIndex++;
			User plTmp = this.getPlayerByPlayerIndex(nextPlayerIndex);// 查找当前索引是否被占用
																		// 20170204
																		// 
			if (plTmp == null) {
				str = Integer.toString(nextPlayerIndex);

				// 判断是否是靓号
				IGoodNumberService goodNumberService = (IGoodNumberService) SpringService
						.getBean("goodNumberService");
				// goodnum = 0;
				goodnum = goodNumberService.getGoodNumberTotal(str);

				if (goodnum <= 0) {
					for (int i = 1; i < str.length() - 3; i++) {
						char c = str.charAt(i);
						if (c == str.charAt(i + 1) && c == str.charAt(i + 2)
								&& c == str.charAt(i + 3)) {
							goodnum = 1;
							break;
						}
					}
				}

				if (goodnum <= 0) {
					break;
				}
			}
		}

		this.playerIndexConfig.setValueInt(nextPlayerIndex);
		this.cfgService.updatePara(this.playerIndexConfig);
		return nextPlayerIndex;
	}

	public static int robotPlayerIndex = 0;

	public int getNextRobotIndex() {
		int nextPlayerIndex = this.playerIndexConfig.getValueInt();
		if (nextPlayerIndex > robotPlayerIndex)
			robotPlayerIndex = nextPlayerIndex + 1000;
		else
			robotPlayerIndex++;

		return robotPlayerIndex;
	}

	public void createPlayerAck(EntranceUserMsgAck msg) {
		//
		IoSession session = GameContext.gameSocket
				.getSessionBySessionID(msg.sessionID);
		//
		if (session == null)
			return;

		LoginMsgAck ack = new LoginMsgAck();
		ack.result = msg.result;
		ack.rooms = this.tableLogic_nndzz.getGameRoomList();

		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		//
		User pl = null;
		if (msg.result == ErrorCodeConstant.CMD_EXE_OK) {
			pl = new User();
			pl.setPassword(msg.password);
			pl.setAccount(msg.account);
			pl.setMachineCode(msg.machine_code);
			//
			pl.setHeadImg(4);

			pl.setSex(msg.sex);
			pl.setDeviceFlag(msg.deviceFlag);
			pl.setPlatformType(msg.deviceFlag);

			//
			pl.setPlayerID(msg.playerID);
			pl.setPlayerName(filterEmojiUserName(msg.playerName));
			pl.setPlayerIndex(msg.playerIndex);
			pl.setQqOpenID(msg.qqOpenID);
			pl.setWxOpenID(msg.wxOpenID);
			pl.setLife(5);//
			// 更新ip情况 ,获取到 clientIp 可能是 ip:port 或 ip 格式，需要解析出ip地址和端口号，分别存储到不同列
			String clientIp = msg.ip;
			if (clientIp != null && !"".equals(clientIp.trim())) {
				String[] ipAndPorts = clientIp.split(":");
				pl.setClientIP(ipAndPorts[0]);
				if (ipAndPorts.length >= 2) {
					pl.setHttpClientPort(ipAndPorts[1]);
				}
			}
			// 客户端手机设备相关信息
			pl.setDeviceBrand(msg.deviceBrand);
			pl.setSystemVersion(msg.systemVersion);
			pl.setPhone(msg.phone);
			pl.setLocation(msg.location);
			// 首次登陆赠送金币 客户端要做特效显示
			int gold = cfgService.get_new_player_gold_num();

			pl.setGold(gold);
			pl.setContinueLanding(1);
			//
			pl.setLuckyDrawsTimes((int) (Math.floor(Math.random() * 2) + 1));
			//
			pl.setFirstLogin(1);//

			pl.setUnionID(msg.unionId);

			pl.setParam01(msg.param01);

			pl.setHeadImgUrl(msg.headImgUrl);
			pl.setLocation(msg.location);
			//
			userDAO.createPlayer(pl);
			// janJiDAO.insert(pl);

			regNum++;
			//
			// pl.setOnline(true);
			playerMap.put(pl.getPlayerID(), pl);
			//
			if (msg.machine_code != null && msg.machine_code.length() > 1)
				playerMachineCodeMap.put(pl.getMachineCode(), pl);
			if (msg.account != null && msg.account.length() > 1)
				playerAccountMap.put(pl.getAccount().toLowerCase(), pl);
			if (msg.qqOpenID != null && msg.qqOpenID.length() > 1)
				playerQQMap.put(pl.getQqOpenID(), pl);
			// if (msg.wxOpenID != null && msg.wxOpenID.length() > 1)
			// playerWXMap.put(pl.getWxOpenID(), pl);
			if (msg.unionId != null && msg.unionId.length() > 1) {
				playerWXMap.put(pl.getUnionID(), pl);// cc modify 2017-12-5
			}
			//
			//
			mapFirstLogin.put(pl.getPlayerID(), "1");
			post_login_succ(ack, pl, session);

		}
		sendTwoChessSupport(session);
		//
		GameContext.gameSocket.send(session, ack);
		// show_bind_phone(pl, session);
		// 创建房间时显示信息
		show_notic_oncreatevip(pl, session);
		// 显示公告
		show_notic_onlogin(session);
		sendZhiduiSupport(session);

	}

	//
	private void create_more_test_player() {

	}

	/**
	 * @param operationType
	 *            操作主类型 按物品类型分类
	 * @param operationSubType
	 *            操作子类型 按玩家具体行为分类
	 * @param moneyRelated
	 *            操作涉及到的物品或金币数量
	 * @param moneyType
	 *            操作涉及到的物品类型 金币 晶石 动物彩票 抽奖奖券固定 如果该物品是道具的话，填道具id
	 */
	public void createPlayerLog(String playerID, int playerIndex,
			String playerName, int playerGold, int operationType,
			int operationSubType, int moneyRelated, String detail,
			int moneyType, String gameId) {

		// 测试PlayerOperationLog
		PlayerOperationLog playerLog = new PlayerOperationLog();
		playerLog.setPlayerID(playerID);
		playerLog.setPlayerIndex(playerIndex);
		playerLog.setPlayerName(filterEmojiUserName(playerName));
		playerLog.setOpGold(moneyRelated);

		playerLog.setOperationType(operationType);
		playerLog.setOperationSubType(operationSubType);
		playerLog.setPlayerGold(playerGold);
		playerLog.setOpDetail(detail);
		playerLog.setMoneyType(moneyType);
		//
		playerLog.setCreateTime(new Date());
		playerLog.setTableNamePrefix(gameId);
		//
		// playerLogDAO.insert(playerLog);

		/*
		 * DBOperation dbmsg = new DBOperation(); dbmsg.dao =
		 * DBConstant.PLAYER_LOG_DAO; dbmsg.opertaion =
		 * DBConstant.PLAYER_LOG_DAO_insert; dbmsg.log = playerLog;
		 * dbThread.pushMsg(dbmsg);
		 */

		DBOperation dbmsg = new DBOperation();
		dbmsg.dao = DBConstant.USER_LOG_DAO;
		dbmsg.opertaion = DBConstant.USER_LOG_DAO_insert;
		dbmsg.log = playerLog;
		dbThread.pushMsg(dbmsg);

		// playerLogDAO.insert(playerLog);
		/*
		 * DBOperation dbmsg = new DBOperation(); dbmsg.dao =
		 * DBConstant.PLAYER_LOG_DAO; dbmsg.opertaion =
		 * DBConstant.PLAYER_LOG_DAO_insert; dbmsg.log = playerLog;
		 * palyer_log_thread.pushMsg(dbmsg);
		 */

	}

	/**
	 * 方法重载，增加俱乐部code参数-lxw20180903
	 */
	public void createPlayerLog(String playerID, int playerIndex,
			String playerName, int playerGold, int operationType,
			int operationSubType, int moneyRelated, String detail,
			int moneyType, String gameId, int clubCode) {

		// 测试PlayerOperationLog
		PlayerOperationLog playerLog = new PlayerOperationLog();
		playerLog.setPlayerID(playerID);
		playerLog.setPlayerIndex(playerIndex);
		playerLog.setPlayerName(filterEmojiUserName(playerName));
		playerLog.setOpGold(moneyRelated);
		playerLog.setOperationType(operationType);
		playerLog.setOperationSubType(operationSubType);
		playerLog.setPlayerGold(playerGold);
		playerLog.setOpDetail(detail);
		playerLog.setMoneyType(moneyType);
		playerLog.setCreateTime(new Date());
		playerLog.setTableNamePrefix(gameId);
		playerLog.setClubCode(clubCode);

		DBOperation dbmsg = new DBOperation();
		dbmsg.dao = DBConstant.USER_LOG_DAO;
		dbmsg.opertaion = DBConstant.USER_LOG_DAO_insert;
		dbmsg.log = playerLog;
		dbThread.pushMsg(dbmsg);

	}

	// 检查重连
	public void check_reroom(CheckOfflineBackMsg msg, IoSession session) {
		User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);

		if (pl == null)
			return;

		// 要进的房间
		GameRoom room = this.tableLogic_nndzz.getRoom(msg.roomID);
		// 检测断线
		List<GameTable> oldTables = this.tableLogic_nndzz.getPlayingTables(pl
				.getPlayerIndex());
		//  2017.9.18 代开可以创建多个房间，玩家不会默认进入代开房间
		if (msg.isProxy == 1) {// 代开房间
			// 判断代开数量，未结束的房间超过十，则不能再创建；
			List<ProxyVipRoomRecord> proxyRoomRecord = this
					.getProxyRecordByPlayerId(pl.getPlayerID()).proxyRoomRecord;
			int proxyTemp = 0;
			int hasTable = 0;
			for (int i = 0; i < proxyRoomRecord.size(); i++) {
				ProxyVipRoomRecord record = proxyRoomRecord.get(i);
				if (record.getVipState() <= 1) {
					proxyTemp = proxyTemp + 1;
				}
			}
			if (isHasTable(pl, oldTables) == 1
			) {
				hasTable = 1;
			}

			if (proxyTemp >= 10) {
				// 不能再创建代开房间了，返回消息给提示；
				CheckOfflineBackMsgAck rmsg = new CheckOfflineBackMsgAck();
				rmsg.roomID = msg.roomID;
				rmsg.roomType = room.roomType;
				rmsg.proxyTip = 2;
				GameContext.gameSocket.send(pl.getSession(), rmsg);
			} else {

				if (hasTable == 1) {
					// 可以创建代开房间；返回消息
					CheckOfflineBackMsgAck rmsg = new CheckOfflineBackMsgAck();
					rmsg.roomID = msg.roomID;
					rmsg.roomType = room.roomType;
					rmsg.proxyTip = 3;
					GameContext.gameSocket.send(pl.getSession(), rmsg);
				} else {
					// 可以创建代开房间；返回消息
					CheckOfflineBackMsgAck rmsg = new CheckOfflineBackMsgAck();
					rmsg.roomID = msg.roomID;
					rmsg.roomType = room.roomType;
					rmsg.proxyTip = 1;
					GameContext.gameSocket.send(pl.getSession(), rmsg);
				}
			}
		} else {
			// 点击正常创建房间
			if (oldTables != null && oldTables.size() >= 1) {
				for (int i = 0; i < oldTables.size(); i++) {
					GameTable gt = oldTables.get(i);
					List<User> gtPlayers = gt.getPlayers();
					for (int j = 0; j < gtPlayers.size(); j++) {
						if (gtPlayers.get(j).getPlayerID()
								.equals(pl.getPlayerID())) {
							// 有房间；还需判断玩家是否为代开者，如果是代开者，那么房间内超过两人，则为普通房间，即不能再代开，不能再开房间
							if (gt.getProxyCreator() == null
									|| (gt.getProxyCreator() != null && gtPlayers
											.size() > 0)) {
								// 在其他桌子中不能进入，直接进入老桌子
								this.tableLogic_nndzz.enter_room(pl, oldTables.get(i)
										.getRoomID());
								//, oldTables.get(i)
								//.getGameId()
								return;
							}
						}
					}
				}
			}
			// 不需要重连，通知前台继续
			CheckOfflineBackMsgAck rmsg = new CheckOfflineBackMsgAck();
			rmsg.roomID = msg.roomID;
			rmsg.roomType = room.roomType;
			rmsg.proxyTip = 0;
			rmsg.enterType = msg.enterType;
			rmsg.clickType = msg.clickType;
			GameContext.gameSocket.send(pl.getSession(), rmsg);
		}
	}

	@SuppressWarnings("unchecked")
	public int isHasTable(User pl, List<?> oldTables) {
		try {
			for (int i = 0; i < oldTables.size(); i++) {
				Object gt = oldTables.get(i);
				List<User> gtPlayers = (List<User>) gt.getClass()
						.getDeclaredMethod("getPlayers", null).invoke(gt, null);
				User proxyUser = (User) gt.getClass()
						.getDeclaredMethod("getProxyCreator", null)
						.invoke(gt, null);
				for (int j = 0; j < gtPlayers.size(); j++) {
					if (gtPlayers.get(j).getPlayerID().equals(pl.getPlayerID())) {
						// 有房间；还需判断玩家是否为代开者，如果是代开者，那么房间内超过两人，则为普通房间，即不能再代开，不能再开房间
						if (proxyUser == null
								|| (proxyUser != null && gtPlayers.size() > 0)) {
							// 已有房间
							return 1;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * // 开始游戏（断线重连） 修改：集成版
	 * 
	 * @author  2017-7-12
	 * @param pl
	 */
	public void enter_room(RequestStartGameMsg msg, IoSession session) {
		User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);

		if (pl == null) {
			logger.error("---------->玩家检查开始游戏逻辑，玩家为null，ganeID=" + msg.gameId
					+ ", roomID=" + msg.roomID);
			return;
		}

		if (msg.roomID == -1) {
			// 房间ID为-1表示断线重连消息
			this.reEnterRoom_define(pl);
			return;
		}

		if (!isSameReLoginIp(pl, session))
			return;

		// 检测是否需要救济金
		// check_save(pl);
		//
		tableLogic_nndzz.enter_room(pl, msg.roomID);
	}

	/**
	 * 客户端请求断线恢复
	 * 
	 * @param msg
	 * @param session
	 */
	public void ask_recover(AskRecoverGameMsg msg, IoSession session) {
	}

	/**
	 * 客户端请求断线恢复玩家状态
	 * 
	 * @param msg
	 * @param session
	 */
	public void ask_recover_state(AskRecoverPlayerStateMsg msg,
			IoSession session) {
	}

	// 客户端通知服务器，得分之类
	public void gameUpdate(GameUpdateMsg msg, IoSession session) {

	}

	// 把客户端的大部分数据，重新发一份给客户端
	public void update_player_property_to_client(User pl) {
		UpdatePlayerPropertyMsg upp = new UpdatePlayerPropertyMsg();
		upp.diamond = pl.getDiamond();
		upp.escape = pl.getEscape();
		upp.gold = pl.getGold();
		upp.life = pl.getLife();
		upp.loses = pl.getLoses();
		upp.score = pl.getScore();
		upp.wons = pl.getWons();
		upp.serverCD = GameContext.mainThread.get10minLeft();
		GameContext.gameSocket.send(pl.getSession(), upp);

	}

	// 客户端通知服务器，得分之类
	public void playerGameOperation(PlayerGameOpertaionMsg msg,
			IoSession session) {

		if (msg.gameId.equals("nndzz")) {
			playerGameOperation_nndzz(msg, session);
		} else {
			User pl = (User) session
					.getAttribute(NetConstant.PLAYER_SESSION_KEY);
			if (pl == null || pl.getSession() == null)
				return;

			if (msg.opertaionID == GameConstant.OPT_REQUEST_UPDATE_PALYER_DATA) {
				update_player_property_to_client(pl);
			} else if (msg.opertaionID == GameConstant.OPT_PLAYER_LEFT_TABLE) {
				if (msg.opValue == 1) { // 1为退出房间

					tableLogic_nndzz.player_left_table(pl, false, false);
				} else if(msg.opValue == 2){
					// 处理为掉线
					tableLogic_nndzz.player_left_table(pl, false, true);
				} else {
					// 处理为掉线
					tableLogic_nndzz.player_left_table(pl, true, false);
				}
				//
			} else if (msg.opertaionID == GameConstant.OPT_BUY_ITEM) {
				/** 客户端通知服务器，购买物品* */
				// buy_item(pl, msg);
			} else if (msg.opertaionID == GameConstant.OPT_USE_ITEM) {
				MallItem ib = getMallItem(msg.opValue);
				this.tableLogic_nndzz.use_item(pl, ib);
			} else if (msg.opertaionID == GameConstant.OPT_CHANGE_HEAD)/**
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 客户端通知服务器，更换头像*
			 */
			{
				if (msg.opValue >= 0 && msg.opValue <= 20) {
					pl.setHeadImg(msg.opValue);
					//
					// userDAO.updatePlayerHead(pl);
					DBOperation dbmsg = new DBOperation();
					dbmsg.dao = DBConstant.USER_DAO;
					dbmsg.opertaion = DBConstant.USER_DAO_updatePlayerHead;
					dbmsg.playerID = pl.getPlayerID();
					dbmsg.headImg = pl.getHeadImg();
					dbThread.pushMsg(dbmsg);

					PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();

					axk.result = ErrorCodeConstant.CMD_EXE_OK;
					axk.opertaionID = msg.opertaionID;
					axk.opValue = msg.opValue;
					axk.playerID = pl.getPlayerID();
					axk.playerIndex = pl.getPlayerIndex();
					axk.canFriend = pl.getCanFriend();
					axk.sex = pl.getSex();
					axk.ip = pl.getClientIP();
					axk.headImgUrl = pl.getHeadImgUrl();
					axk.location = pl.getLocation();
					GameContext.gameSocket.send(pl.getSession(), axk);
				}
			} else if (msg.opertaionID == GameConstant.OPT_CONTINUE_GAME)/**
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 客户端通知服务器，游戏结束，玩家继续游戏*
			 */
			{
				//
			} else if (msg.opertaionID == GameConstant.OPT_BACK_TO_LOBBY)/**
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 
			 * 客户端通知服务器，游戏结束，玩家返回大厅*
			 */
			{
				tableLogic_nndzz.player_left_table(pl, true, false);
			} else if (msg.opertaionID == GameConstant.OPT_DEAD_ALIVE) {
				PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();

				axk.opertaionID = msg.opertaionID;

				if (pl.getGold() > 2000)// 复活扣2000金币
				{
					sub_player_gold(pl, 2000, LogConstant.OPERATION_DEAD_ALIVE,
							"立即复活扣金币=2000");
					//
					axk.result = ErrorCodeConstant.CMD_EXE_OK;
				} else {
					axk.result = ErrorCodeConstant.GOLD_NOT_ENOUGH;
				}
				axk.headImgUrl = pl.getHeadImgUrl();
				GameContext.gameSocket.send(pl.getSession(), axk);
			} else if (msg.opertaionID == GameConstant.OPT_SET_TUOGUAN) {
				// 设置托管状态
				pl.setAutoOperation(msg.opValue);
			} else if (msg.opertaionID == GameConstant.OPERATION_APPLY_CLOSE_VIP_ROOM) {
				// 房主申请解散房间
				if (pl != null) {
					tableLogic_nndzz.applyCloseVipTable(pl);
				}
			}
		}

	}

	
	/**
	 * 客户端通知服务器，得分之类
	 * 
	 * @param msg
	 * @param session
	 *            void
	 */
	public void playerGameOperation_nndzz(PlayerGameOpertaionMsg msg,
			IoSession session) {
		User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);
		if (pl == null || pl.getSession() == null)
			return;

		if (msg.opertaionID == GameConstant.OPT_REQUEST_UPDATE_PALYER_DATA) {
			update_player_property_to_client(pl);
		} else if (msg.opertaionID == GameConstant.OPT_PLAYER_LEFT_TABLE) {
			if (msg.opValue == 1) { // 1为退出房间

				tableLogic_nndzz.player_left_table(pl, false, false);
			} else if(msg.opValue == 2){// 2为强行退出房间
				tableLogic_nndzz.player_left_table(pl, false, true);
			}else {
				// 处理为掉线
				tableLogic_nndzz.player_left_table(pl, true, false);
			}
		} else if (msg.opertaionID == GameConstant.OPT_BUY_ITEM) {
			/** 客户端通知服务器，购买物品* */
			// buy_item(pl, msg);
		} else if (msg.opertaionID == GameConstant.OPT_USE_ITEM) {
			MallItem ib = getMallItem(msg.opValue);
			this.tableLogic_nndzz.use_item(pl, ib);
		} else if (msg.opertaionID == GameConstant.OPT_CHANGE_HEAD)/**
		 * 
		 * 
		 * 
		 * 
		 * 客户端通知服务器，更换头像*
		 */
		{
			if (msg.opValue >= 0 && msg.opValue <= 20) {
				pl.setHeadImg(msg.opValue);
				//
				// userDAO.updatePlayerHead(pl);
				DBOperation dbmsg = new DBOperation();
				dbmsg.dao = DBConstant.USER_DAO;
				dbmsg.opertaion = DBConstant.USER_DAO_updatePlayerHead;
				dbmsg.playerID = pl.getPlayerID();
				dbmsg.headImg = pl.getHeadImg();
				dbThread.pushMsg(dbmsg);

				PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();

				axk.result = ErrorCodeConstant.CMD_EXE_OK;
				axk.opertaionID = msg.opertaionID;
				axk.opValue = msg.opValue;
				axk.playerID = pl.getPlayerID();
				axk.playerIndex = pl.getPlayerIndex();
				axk.canFriend = pl.getCanFriend();
				axk.sex = pl.getSex();
				axk.ip = pl.getClientIP();
				GameContext.gameSocket.send(pl.getSession(), axk);
			}
		} else if (msg.opertaionID == GameConstant.OPT_CONTINUE_GAME)/**
		 * 
		 * 
		 * 
		 * 
		 * 客户端通知服务器，游戏结束，玩家继续游戏*
		 */
		{
			tableLogic_nndzz.gameContinue(pl);
			//
		} else if (msg.opertaionID == GameConstant.OPT_BACK_TO_LOBBY)/**
		 * 
		 * 
		 * 
		 * 
		 * 客户端通知服务器，游戏结束，玩家返回大厅*
		 */
		{
			tableLogic_nndzz.player_left_table(pl, true, false);
		} else if (msg.opertaionID == GameConstant.OPT_DEAD_ALIVE) {
			PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();

			axk.opertaionID = msg.opertaionID;

			if (pl.getGold() > 2000)// 复活扣2000金币
			{
				sub_player_gold(pl, 2000, LogConstant.OPERATION_DEAD_ALIVE,
						"立即复活扣金币=2000");
				//
				axk.result = ErrorCodeConstant.CMD_EXE_OK;
			} else {
				axk.result = ErrorCodeConstant.GOLD_NOT_ENOUGH;
			}
			GameContext.gameSocket.send(pl.getSession(), axk);
			// } else if (msg.opertaionID == GameConstant.OPT_SET_TUOGUAN) {
			// // 设置托管状态
			// pl.setAutoOperation(msg.opValue);
		} else if (msg.opertaionID == GameConstant.OPERATION_APPLY_CLOSE_VIP_ROOM) {
			// 房主申请解散房间
			if (pl != null) {
				tableLogic_nndzz.applyCloseVipTable(pl);
			}
		}

	}

	//
	public void udpate_player_item_num(String playerID, int itemBaseID, int num) {
		// this.userBackpackDAO.deleteByPlayerItemID(playerItemID);
		//
		DBOperation dbmsg = new DBOperation();
		dbmsg.dao = DBConstant.USER_ITEM_DAO;
		dbmsg.opertaion = DBConstant.USER_ITEM_DAO_updatePlayerItemNum;
		dbmsg.playerID = playerID;
		dbmsg.itemBaseID = itemBaseID;
		dbmsg.itemNum = num;
		dbThread.pushMsg(dbmsg);
	}

	/**
	 * 人民币购买 礼包或其他 接口
	 */
	public boolean buy_item_rmb(String playerID, int itemID, int itemNum,
			int opertaionID, String gameID) {
		boolean isSend = false;
		User pl = this.getPlayerByPlayerIDAndGameID(playerID, gameID);
		// User pl
		if (pl == null) {
			return isSend;
		}

		MallItem item = getMallItem(itemID);
		if (item == null)
			return isSend;

		PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();

		axk.result = ErrorCodeConstant.CMD_EXE_FAILED;
		axk.opertaionID = opertaionID;
		axk.opValue = itemID;
		axk.playerID = pl.getPlayerID();
		axk.playerIndex = pl.getPlayerIndex();
		axk.sex = pl.getSex();
		axk.headImgUrl = pl.getHeadImgUrl();

		//
		int baseid = item.getBase_id();
		//
		if (item.getProperty_3() == 1)// 都是扣人民币的道具，这里先扣人民币
		{
			axk.result = ErrorCodeConstant.CMD_EXE_OK;
			String detail = "人民币消费，购买物品,类型=" + item.getBase_id() + "，名称="
					+ item.getName() + ",价格=" + item.getPrice() * itemNum
					+ "人民币";
			// 未实现方法
			createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
					pl.getPlayerName(), pl.getGold(),
					LogConstant.OPERATION_TYPE_USE_RMB,
					LogConstant.OPERATION_PAY, 0, detail,
					LogConstant.MONEY_TYPE_GOLD, pl.getGameId());

			// 扣人民币之后，算VIP经验和等级,并且写入数据库
			this.countVIP(pl, item.getPrice() * itemNum);
		}
		// 人民币购买房卡
		if (baseid > 3000 && baseid < 4000) {
			axk.result = ErrorCodeConstant.CMD_EXE_OK;

			// 如果买的是多张道具，添加的时候需要使用基础道具
			if (item.getProperty_4() > 1) {
				String detail5 = "添加组合道具，类型=" + item.getBase_id() + ",道具名称="
						+ item.getName() + ",数量=" + itemNum;
				createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
						pl.getPlayerName(), pl.getGold(),
						LogConstant.OPERATION_TYPE_ADD_PRO,
						LogConstant.OPERATION_PAY, itemNum, detail5,
						item.getBase_id(), pl.getGameId());

				createItemForPlayer(pl, item.getProperty_2(), item.getName(),
						LogConstant.OPERATION_PAY,
						itemNum * item.getProperty_4(), gameID);

			} else {
				createItemForPlayer(pl, item.getBase_id(), item.getName(),
						LogConstant.OPERATION_PAY, itemNum, gameID);
			}

			// 把玩家道具列表更新一下
			UpdatePlayerItemListMsg upilm = new UpdatePlayerItemListMsg();
			upilm.gold = pl.getGold();
			upilm.init_list(pl.getItems());
			upilm.credits = pl.getCredits();
			GameContext.gameSocket.send(pl.getSession(), upilm);
			
			logger.info("玩家购买房卡成功，给玩家发卡，更新玩家房卡信息：玩家ID【"+pl.getPlayerIndex()+"】，玩家昵称【"+pl.getPlayerName()
							+"】，消费金额【"+item.getPrice()+"】，房卡数量【"+item.getProperty_4()+"】");
			
			isSend = true;
		}
		// 人民币购买金币
		else if (baseid > 5000 && baseid < 6000) {
			add_player_gold(pl, item.getProperty_1() * itemNum,
					LogConstant.OPERATION_BUY_BIG_GIFT, "购买礼包" + item.getName()
							+ "，赠送金币=" + item.getProperty_1() * itemNum);
			//
			axk.result = ErrorCodeConstant.CMD_EXE_OK;
			//
			isSend = true;
		}

		axk.gold = pl.getGold();
		GameContext.gameSocket.send(pl.getSession(), axk);

		return isSend;
	}

	//
	public void sub_player_gold(User pl, int num, int logType, String remark) {
		if (num <= 0 || pl.getGold() <= 0)
			return;
		if (pl.getGold() < num) {
			num = pl.getGold();// 有多少，扣多少
		}
		// 玩家消费金币次数加1
		// taskService.updatePlayerExtByTaskID(pl.getPlayerID(), 10008, 1);
		pl.setGold(pl.getGold() - num);

		// userDAO.updatePlayerGold(pl.getPlayerID(),pl.getGold());

		DBOperation msg = new DBOperation();
		msg.dao = DBConstant.USER_DAO;
		msg.opertaion = DBConstant.USER_DAO_updatePlayerGold;
		msg.playerID = pl.getPlayerID();
		msg.gold = pl.getGold();
		dbThread.pushMsg(msg);

		//
		String detail = "消耗金币=" + num + "," + remark;
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_SUB_GOLD, logType, num, detail,
				LogConstant.MONEY_TYPE_GOLD, pl.getGameId());
		//
		// 刷新下玩家具体数据
		this.update_player_property_to_client(pl);

		// 检查救济金
		check_save(pl);
	}

	// 检查下玩家是否需要救济
	public void check_save(User pl) {
		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");

		SystemConfigPara para = cfgService
				.getPara(ConfigConstant.HANDSEL_COIN_EACH_TIME);
		int max_time = cfgService.get_max_save_time();
		int save_gold_num = cfgService.get_max_save_gold_num();

		// VIP房间要加上预先扣除的带入金币
		if (para != null
				&& (pl.getGold() + pl.getVipTableGold()) < para.getValueInt())// 低于初级场下限，救济一次
		{
			if (pl.getSaveTime() < max_time)// 救济次数判断
			{
				pl.setSaveTime(pl.getSaveTime() + 1);
				this.userDAO.updateSaveTime(pl.getPlayerID(), pl.getSaveTime());
				this.add_player_gold(pl, save_gold_num,
						LogConstant.OPERATION_TYPE_ADD_GOLD_SAVE, "救济加金币"
								+ save_gold_num);

				//
				PlayerGameOpertaionAckMsg msg = new PlayerGameOpertaionAckMsg();
				msg.opertaionID = GameConstant.OPT_GOT_GOLD_AUTO_SAVE;
				msg.gold = save_gold_num; // 赠送金额
				msg.playerIndex = para.getValueInt(); // 少于多少金额赠送
				msg.opValue = max_time; // 最大赠送次数
				msg.result = pl.getSaveTime(); // 当前是第几次
				msg.headImgUrl = pl.getHeadImgUrl();
				GameContext.gameSocket.send(pl.getSession(), msg);
			}
		}
	}

	//
	public void add_player_gold(User pl, int num, int logType, String remark) {
		// if (num <= 0 || pl == null)
		// return;
		if (pl == null) {
			return;
		}

		if (pl.isRobot())
			return;

		pl.setGold(pl.getGold() + num);

		// userDAO.updatePlayerGold(pl.getPlayerID(),pl.getGold());

		DBOperation msg = new DBOperation();
		msg.dao = DBConstant.USER_DAO;
		msg.opertaion = DBConstant.USER_DAO_updatePlayerGold;
		msg.playerID = pl.getPlayerID();
		msg.gold = pl.getGold();
		dbThread.pushMsg(msg);

		String detail = "获得金币=" + num + ":" + remark;
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_ADD_GOLD, logType, num, detail,
				LogConstant.MONEY_TYPE_GOLD, pl.getGameId());

		// 玩家累计获得的金币数量记录下
		// taskService.updatePlayerExtByTaskID(pl.getPlayerID(), 20002, num);
		// 刷新下玩家具体数据
		this.update_player_property_to_client(pl);
	}

	//
	public void updatePlayerRecord(User pl) {
		if (pl != null) {
			// userDAO.updatePlayerRecord(pl.getPlayerID(),pl.getWons(),pl.getLoses(),pl.getEscape());

			DBOperation msg = new DBOperation();
			msg.dao = DBConstant.USER_DAO;
			msg.opertaion = DBConstant.USER_DAO_updatePlayerRecord;
			msg.playerID = pl.getPlayerID();
			msg.wons = pl.getWons();
			msg.loses = pl.getLoses();
			msg.escapes = pl.getEscape();
			dbThread.pushMsg(msg);

		}
	}

	/**
	 * 更改用户信息
	 */
	public void updatePlayer(User pl, PlayerOpertaionMsg msg, ValidateMsgAck ack) {

		if (pl != null) {
			if (ack.msgCMD == MsgCmdConstant.GAME_USER_UPDATE_NICKNAME_ACK) {

				// 过滤表情字符
				Pattern emoji = Pattern
						.compile(
								"[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\ud83e\udd00-\ud83e\udfff]|[\u2600-\u27ff]",
								Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

				Matcher emojiMatcher = emoji.matcher(msg.playerName);

				if (emojiMatcher.find()) {
					logger.error("玩家：" + pl.getPlayerIndex()
							+ "昵称包含Emoji字符，修改失败");

					ack.result = 4;
					ack.optStr = msg.playerName;
					GameContext.gameSocket.send(pl.getSession(), ack);
					return;
				}
				// 判断昵称是否重复
				/*
				 * if (!pl.getPlayerName().equals(msg.playerName)) { Player
				 * plNick = userDAO.getPlayerByName(msg.playerName); if (plNick
				 * != null) { // 存在响应的昵称 ack.result = 2;
				 * SystemConfig.gameSocketServer.sendMsg(pl.getSession(), ack);
				 * return; } }
				 */

				// // 判断昵称是否合法
				// String word = msg.playerName;
				// SensitiveWords words = playerDAO.getWords(word);
				// if (words != null) {
				// //昵称不合法；
				// logger.error("玩家：" + pl.getPlayerIndex()
				// + "昵称包含非法内容，修改失败");
				//
				// ack.result = 5;
				// ack.optStr = msg.playerName;
				// GameContext.gameSocket.send(pl.getSession(), ack);
				// return;
				// }
				logger.info("updatePlayer index=" + pl.getPlayerIndex()
						+ " nickName = " + msg.playerName);

				ack.optStr = msg.playerName;

				pl.setPlayerName(msg.playerName);
			} else if (ack.msgCMD == MsgCmdConstant.GAME_USER_UPDATE_LOGO_ACK) {
				ack.optStr = "" + msg.headIndex;
				pl.setSex(msg.sex);
				pl.setHeadImg(msg.headIndex);
			} else if (ack.msgCMD == MsgCmdConstant.GAME_USER_UPDATE_ACCOUNT_ACK) {
				ack.optStr = msg.account;
				if (msg.account.length() < 3 || msg.account.length() > 12) {
					ack.result = 3;
					GameContext.gameSocket.send(pl.getSession(), ack);
					return;
				}

				if (pl.getAccount().endsWith("")) {
					User plAccount = userDAO.getPlayerByAccount(msg.account);
					if (plAccount != null) {
						// 存在响应的account
						ack.result = 2;
						GameContext.gameSocket.send(pl.getSession(), ack);
						return;
					}
					pl.setAccount(msg.account);

					playerAccountMap.put(pl.getAccount().toLowerCase(), pl);
				}
			}

			DBOperation omsg = new DBOperation();
			omsg.dao = DBConstant.USER_DAO;
			omsg.opertaion = DBConstant.USER_DAO_updatePlayer;
			omsg.playerID = pl.getPlayerID();
			omsg.account = pl.getAccount();
			omsg.playerName = pl.getPlayerName();
			omsg.sex = pl.getSex();
			omsg.headImg = pl.getHeadImg();
			omsg.password = pl.getPassword();
			dbThread.pushMsg(omsg);

			ack.result = 1;
			ack.sex = pl.getSex();

			// 修改男声女声，单独处理（20171018）
			if (ack.msgCMD == MsgCmdConstant.GAME_USER_UPDATE_LOGO_ACK) {
				int roomID = pl.getRoomID();
				GameRoom rm = GameContext.tableLogic_nndzz.getRoom(roomID);
				if (rm == null) {
					GameContext.gameSocket.send(pl.getSession(), ack);
				} else {
					ack.tablePos = pl.getTablePos();
					GameTable gt = rm.getTable(pl.getTableID(),
							pl.isPlayingSingleTable());
					if (gt == null) {
						GameContext.gameSocket.send(pl.getSession(), ack);
					} else {
						for (User plx : gt.getPlayers()) {
							GameContext.gameSocket.send(plx.getSession(), ack);
						}
					}
				}
			} else {
				GameContext.gameSocket.send(pl.getSession(), ack);
			}
		}
	}

	public void updatePlayerAccountAndPassword(User pl, PlayerOpertaionMsg msg,
			ValidateMsgAck ack) {
		if (pl != null) {
			if (ack.msgCMD == MsgCmdConstant.GAME_USER_UPDATE_ACCOUNT_ACK) {
				ack.optStr = msg.account;
				if (msg.account.length() < 3 || msg.account.length() > 12) {
					ack.result = 3;
					GameContext.gameSocket.send(pl.getSession(), ack);
					return;
				}

				if (pl.getAccount().endsWith("")) {
					User plAccount = userDAO.getPlayerByAccount(msg.account);
					if (plAccount != null) {
						// 存在响应的account
						ack.result = 2;
						GameContext.gameSocket.send(pl.getSession(), ack);
						return;
					}

					pl.setAccount(msg.account);
					pl.setPassword(msg.newPassWord);

					playerAccountMap.put(pl.getAccount().toLowerCase(), pl);
				} else {
					ack.result = 4;
					GameContext.gameSocket.send(pl.getSession(), ack);
				}
			}

			DBOperation omsg = new DBOperation();
			omsg.dao = DBConstant.USER_DAO;
			omsg.opertaion = DBConstant.USER_DAO_updatePlayer;
			omsg.playerID = pl.getPlayerID();
			omsg.account = pl.getAccount();
			omsg.playerName = pl.getPlayerName();
			omsg.sex = pl.getSex();
			omsg.headImg = pl.getHeadImg();
			omsg.password = pl.getPassword();
			dbThread.pushMsg(omsg);

			ack.result = 1;

			// 帐号修改成功
			GameContext.gameSocket.send(pl.getSession(), ack);

			// 密码修改成功
			ValidateMsgAck newPasswordAck = new ValidateMsgAck(
					MsgCmdConstant.GAME_USER_UPDATE_PASSWORD_ACK);
			newPasswordAck.optStr = pl.getPassword();
			newPasswordAck.result = 1;
			newPasswordAck.sex = pl.getSex();
			GameContext.gameSocket.send(pl.getSession(), newPasswordAck);
		}
	}

	/**
	 * 添加好友
	 * 
	 * @param User
	 *            操作玩家
	 * @param friendPlayID
	 *            添加的好友PlayerID
	 */
	public void AddPlayerFriend(User pl, String friendPlayID) {
		if (pl != null) {
			long ctt = DateService.getCurrentUtilDate().getTime();
			if (pl.prev_add_friend_time == 0) {
				pl.prev_add_friend_time = ctt;
			} else {
				if ((ctt - pl.prev_add_friend_time) > 2000) {
					pl.prev_add_friend_time = ctt;
				} else {
					return;
				}
			}

			if (pl.getPlayerID().equals(friendPlayID))
				return;

			if (userFriendDAO.isExistsFriend(pl.getPlayerID(), friendPlayID))
				return;

			// 判断好友是否存在
			User fr = userDAO.getPlayerByID(friendPlayID);

			AddFriendMsgAck ack = new AddFriendMsgAck();
			ack.msgCMD = MsgCmdConstant.GAME_ADD_FRIEND__ACK;
			if (fr != null) {
				DBOperation msg = new DBOperation();
				msg.dao = DBConstant.USER_FRIEND_DAO;
				msg.opertaion = DBConstant.USER_FRIEND_AddPLAYERFRIEND;
				msg.playerID = pl.getPlayerID();
				msg.extStr = friendPlayID;
				msg.applyResult = GameConstant.FRIEND_WAIT_PLAYER_AGREE;
				dbThread.pushMsg(msg);

				DBOperation msgfriend = new DBOperation();
				msgfriend.dao = DBConstant.USER_FRIEND_DAO;
				msgfriend.opertaion = DBConstant.USER_FRIEND_AddPLAYERFRIEND;
				msgfriend.playerID = friendPlayID;
				msgfriend.extStr = pl.getPlayerID();
				msgfriend.applyResult = GameConstant.FRIEND_RECEIVE_APPLY;

				dbThread.pushMsg(msgfriend);

				UserFriend faPlayer = new UserFriend();
				faPlayer.playerID = fr.getPlayerID();
				faPlayer.playerName = fr.getPlayerName();
				faPlayer.palyerIndex = fr.getPlayerIndex();
				faPlayer.headImg = fr.getHeadImg();
				faPlayer.gold = fr.getGold();
				faPlayer.sex = fr.getSex();
				faPlayer.applyResult = GameConstant.FRIEND_WAIT_PLAYER_AGREE;

				ack.friend = faPlayer;

				fr = getPlayerByPlayerIDFromCache(fr.getPlayerID());
				if (fr != null && fr.getSession() != null) {
					fr.setOnline(true);
					faPlayer.isOnline = fr.isOnline() ? 1 : 0;
					UserFriend fPlayer = new UserFriend();
					fPlayer.playerID = pl.getPlayerID();
					fPlayer.playerName = pl.getPlayerName();
					fPlayer.palyerIndex = pl.getPlayerIndex();
					fPlayer.headImg = pl.getHeadImg();
					fPlayer.gold = pl.getGold();
					fPlayer.sex = pl.getSex();
					fPlayer.isOnline = 1;
					fPlayer.applyResult = GameConstant.FRIEND_RECEIVE_APPLY;
					FriendMsgAck friendMsgAck = new FriendMsgAck();
					friendMsgAck.friend = fPlayer;
					GameContext.gameSocket.send(fr.getSession(), friendMsgAck);
				}

				ack.result = 1;

			} else {
				ack.result = 0;
			}

			GameContext.gameSocket.send(pl.getSession(), ack);
		}
	}

	/**
	 * 删除好友
	 * 
	 * @param User
	 *            操作玩家
	 * @param friendPlayID
	 *            添加的好友PlayerID
	 */
	public void DelPlayerFriend(User pl, String friendPlayID) {
		if (pl != null) {
			if (pl.getPlayerID().equals(friendPlayID))
				return;

			DBOperation msg = new DBOperation();
			msg.dao = DBConstant.USER_FRIEND_DAO;
			msg.opertaion = DBConstant.USER_FRIEND_DELPLAYERFRIEND;
			msg.playerID = pl.getPlayerID();
			msg.extStr = friendPlayID;
			dbThread.pushMsg(msg);

			DBOperation msgfriend = new DBOperation();
			msgfriend.dao = DBConstant.USER_FRIEND_DAO;
			msgfriend.opertaion = DBConstant.USER_FRIEND_DELPLAYERFRIEND;
			msgfriend.playerID = friendPlayID;
			msgfriend.extStr = pl.getPlayerID();

			dbThread.pushMsg(msgfriend);

			// 查找删除的好友
			User fr = userDAO.getPlayerByID(friendPlayID);
			fr = getPlayerByPlayerIDFromCache(fr.getPlayerID());
			if (fr != null && fr.getSession() != null
					&& fr.getSession().isConnected()) {
				FriendOprateMsgACK fomAck = new FriendOprateMsgACK();
				fomAck.opertaionID = GameConstant.OPT_BDELFRIEND;
				fomAck.result = 1;
				fomAck.optStr = pl.getPlayerID();

				if (fr != null) {
					GameContext.gameSocket.send(fr.getSession(), fomAck);
				}
			}
		}
	}

	/**
	 * 修改好友的备注名称
	 */
	public void updatePlayerRemark(User pl, UpdateFriendRemarkMsg msg) {
		if (pl == null || pl.getPlayerID().equals(msg.friendID))
			return;

		UpdateFriendRemarkMsgAck msgAck = new UpdateFriendRemarkMsgAck();
		msgAck.friendID = msg.friendID;

		// 判断备注名称是否包含特殊字符
		Pattern emoji = Pattern
				.compile(
						"[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\ud83e\udd00-\ud83e\udfff]|[\u2600-\u27ff]",
						Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

		Matcher emojiMatcher = emoji.matcher(msg.remark);

		if (emojiMatcher.find()) {
			msgAck.result = -1;
			msgAck.resultDesc = "备注不能包含特殊字符";
			GameContext.gameSocket.send(pl.getSession(), msgAck);

			return;
		}

		DBOperation msgDB = new DBOperation();
		msgDB.dao = DBConstant.USER_FRIEND_DAO;
		msgDB.opertaion = DBConstant.USER_DAO_updatePlayerRemark;
		msgDB.playerID = pl.getPlayerID();
		msgDB.extStr = msg.friendID;
		msgDB.remark = msg.remark;
		dbThread.pushMsg(msgDB);

		msgAck.result = 1;
		msgAck.resultDesc = "备注修改成功";
		msgAck.remark = msg.remark;
		GameContext.gameSocket.send(pl.getSession(), msgAck);
	}

	/**
	 * 同意加为好友
	 */
	public void agreeBecomeFriendApply(User pl, String friendPlayID) {
		if (pl != null) {
			if (pl.getPlayerID().equals(friendPlayID))
				return;

			DBOperation msg = new DBOperation();
			msg.dao = DBConstant.USER_FRIEND_DAO;
			msg.opertaion = DBConstant.USER_FRIEND_DAO_updateApplyResult;
			msg.playerID = pl.getPlayerID();
			msg.extStr = friendPlayID;
			msg.applyResult = GameConstant.FRIEND_BOTH_IS_FRIEND;
			dbThread.pushMsg(msg);

			DBOperation msgfriend = new DBOperation();
			msgfriend.dao = DBConstant.USER_FRIEND_DAO;
			msgfriend.opertaion = DBConstant.USER_FRIEND_DAO_updateApplyResult;
			msgfriend.playerID = friendPlayID;
			msgfriend.extStr = pl.getPlayerID();
			msgfriend.applyResult = GameConstant.FRIEND_BOTH_IS_FRIEND;

			dbThread.pushMsg(msgfriend);

			// 更新客户端好友验证标志
			FriendOprateMsgACK agrAck = new FriendOprateMsgACK();
			agrAck.opertaionID = GameConstant.OPERATON_AGREE_FRIEND_APPLY_RESULT;
			agrAck.result = 1;
			agrAck.optStr = friendPlayID;

			GameContext.gameSocket.send(pl.getSession(), agrAck);

			// 被同意的人
			User fr = getPlayerByPlayerIDFromCache(friendPlayID);
			if (fr != null && fr.getSession() != null
					&& fr.getSession().isConnected()) {
				FriendOprateMsgACK fomAck = new FriendOprateMsgACK();
				fomAck.opertaionID = GameConstant.OPERATON_AGREE_FRIEND_APPLY_RESULT;
				fomAck.result = 1;
				fomAck.optStr = pl.getPlayerID();

				GameContext.gameSocket.send(fr.getSession(), fomAck);
			}
		}
	}

	/**
	 * 拒绝好友申请
	 */
	public void rejectBecomeFriendApply(User pl, String friendPlayID) {
		// 直接删除好友
		this.DelPlayerFriend(pl, friendPlayID);
	}

	/**
	 * 申请加入俱乐部
	 */
	public void applyJoinClub(User pl, String clubCode) {
		ClubMemberOprateMsgAck ack = new ClubMemberOprateMsgAck();
		ack.opertaionID = GameConstant.CLUB_MEMGER_OPT_APPLY;

		if (clubCode == null || "".equals(clubCode)) {
			ack.result = 2;
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}
		TClub club = tClubDao.getClubByClubCode(Integer.parseInt(clubCode));
		if (club == null) {
			ack.result = 2;
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}
		if (pl.getPlayerID().equals(club.getCreatePlayerId())) {
			ack.result = 3;
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}
		// 判断好友是否已有该成员
		if (clubMemberDAO.isExistsMember(club.getCreatePlayerId(),
				pl.getPlayerID(), clubCode)) {
			ack.result = 3;
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}
		// 提交申请
		this.optClubMemberDao(DBConstant.CLUB_MEMBER_add_member,
				club.getCreatePlayerId(), pl.getPlayerID(), clubCode);
		ack.result = 1;
		GameContext.gameSocket.send(pl.getSession(), ack);
		User clubPlayer = userDAO.getPlayerByID(club.getCreatePlayerId());
		// 通知管理员
		this.sendClubMemberOptAck(GameConstant.CLUB_MEMGER_NOTICE_ADD_MANAGE,
				clubPlayer, clubPlayer, pl, clubCode);
	}

	/**
	 * 俱乐部添加成员
	 */
	public void AddClubMember(User pl, String memberId, String clubCode) {
		if (pl == null) {
			return;
		}
		// 根据俱乐部编号获取管理员
		TClub club = tClubDao.getClubByClubCode(Integer.parseInt(clubCode));
		if (club == null) {
			// 俱乐部不存在
			return;
		}
		User manageUser = userDAO.getPlayerByID(club.getCreatePlayerId());
		// 判断成员是否存在
		User fr = userDAO.getPlayerByID(memberId);
		if (manageUser == null || fr == null) {
			return;
		}
		if (memberId.equals(manageUser.getPlayerID()))
			return;
		// 判断好友是否已有该成员
		if (clubMemberDAO.isExistsMember(manageUser.getPlayerID(), memberId,
				clubCode))
			return;

		this.optClubMemberDao(DBConstant.CLUB_MEMBER_add_member,
				manageUser.getPlayerID(), memberId, clubCode);

		this.sendClubMemberOptAck(GameConstant.CLUB_MEMGER_NOTICE_ADD_MANAGE,
				manageUser, manageUser, fr, clubCode);

		this.sendClubMemberOptAck(GameConstant.CLUB_MEMGER_NOTICE_ADD_WAIT, fr,
				manageUser, fr, clubCode);
	}

	public void IgnoreClubMember(User pl, String memberId, String clubCode) {
		User manageUser = userDAO.getPlayerByID(pl.getPlayerID());
		if (pl.getPlayerID().equals(memberId))
			return;
		User fr = userDAO.getPlayerByID(memberId);
		if (manageUser == null || fr == null) {
			return;
		}
		this.optClubMemberDao(DBConstant.CLUB_MEMBER_delete_member,
				pl.getPlayerID(), memberId, clubCode);

		this.sendClubMemberOptAck(GameConstant.CLUB_MEMGER_NOTICE_OPT_SUC,
				manageUser, manageUser, fr, clubCode);

		this.sendClubMemberOptAck(GameConstant.CLUB_MEMGER_NOTICE_IGNORE_ADD,
				fr, manageUser, fr, clubCode);
	}

	/**
	 * 俱乐部 删除成员
	 */
	public void DelClubMember(User pl, String memberId, String clubCode) {
		if (pl == null) {
			return;
		}
		User manageUser = userDAO.getPlayerByID(pl.getPlayerID());

		if (pl.getPlayerID().equals(memberId))
			return;

		User fr = userDAO.getPlayerByID(memberId);

		if (manageUser == null || fr == null) {
			return;
		}

		this.optClubMemberDao(DBConstant.CLUB_MEMBER_delete_member,
				pl.getPlayerID(), memberId, clubCode);

		this.sendClubMemberOptAck(GameConstant.CLUB_MEMGER_NOTICE_OPT_SUC,
				manageUser, manageUser, fr, clubCode);

		this.sendClubMemberOptAck(GameConstant.CLUB_MEMGER_NOTICE_REMOVE, fr,
				manageUser, fr, clubCode);

	}

	/**
	 * 俱乐部 退出俱乐部
	 */
	public void QuitClubMember(User pl, String memberId, String clubCode) {
		if (pl == null) {
			return;
		}
		if (clubCode == null || "".equals(clubCode)) {
			return;
		}
		if (memberId == null || "".equals(memberId)) {
			return;
		}

		TClub club = tClubDao.getClubByClubCode(Integer.parseInt(clubCode));

		User manageUser = userDAO.getPlayerByID(club.getCreatePlayerId());

		this.optClubMemberDao(DBConstant.CLUB_MEMBER_delete_member,
				club.getCreatePlayerId(), memberId, clubCode);

		this.sendClubMemberOptAck(GameConstant.CLUB_MEMBER_NOTICE_QUIT, pl,
				manageUser, pl, clubCode);
	}

	/**
	 * 俱乐部 删除成员
	 */
	public void DelAllClubMember(String clubCode) {
		this.optClubMemberDao(DBConstant.CLUB_MEMBER_delete_member, "", "",
				clubCode);
	}

	/**
	 * 俱乐部 修改成员状态
	 */
	public void UpdateClubMemberApplyState(User pl, String memberId,
			String clubCode, int applyState) {
		if (pl == null) {
			return;
		}
		if (pl.getPlayerID().equals(memberId))
			return;

		User manageUser = userDAO.getPlayerByID(pl.getPlayerID());
		User fr = userDAO.getPlayerByID(memberId);

		if (manageUser == null || fr == null) {
			return;
		}

		DBOperation msg = new DBOperation();
		msg.dao = DBConstant.CLUB_MEMBER_DAO;
		msg.opertaion = DBConstant.CLUB_MEMBER_update_member_state;
		msg.playerID = pl.getPlayerID();
		msg.extStr = memberId;
		msg.playerName = clubCode;
		msg.applyResult = applyState;
		dbThread.pushMsg(msg);

		this.sendClubMemberOptAck(GameConstant.CLUB_MEMGER_NOTICE_OPT_SUC,
				manageUser, manageUser, fr, clubCode);

		this.sendClubMemberOptAck(GameConstant.CLUB_MEMGER_NOTICE_ADD_AGREE,
				fr, manageUser, fr, clubCode);
	}

	/**
	 * 修改俱乐部成员的备注名称
	 */
	public void UpdateClubMemberRemark(User pl, String memberId,
			String clubCode, String remark) {
		if (pl == null) {
			return;
		}
		if (pl.getPlayerID().equals(memberId))
			return;

		User manageUser = userDAO.getPlayerByID(pl.getPlayerID());
		User fr = userDAO.getPlayerByID(memberId);

		if (manageUser == null || fr == null) {
			return;
		}
		// 判断备注名称是否包含特殊字符
		Pattern emoji = Pattern
				.compile(
						"[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\ud83e\udd00-\ud83e\udfff]|[\u2600-\u27ff]",
						Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

		Matcher emojiMatcher = emoji.matcher(remark);

		ClubMemberOprateMsgAck ack = new ClubMemberOprateMsgAck();
		ack.opertaionID = GameConstant.CLUB_MEMGER_NOTICE_REMARK;
		ack.playerId = manageUser.getPlayerID();
		ack.playerName = manageUser.getPlayerName();
		ack.clubCode = clubCode;
		ack.memberId = fr.getPlayerID();
		ack.memberIndex = fr.getPlayerIndex();
		ack.memberName = fr.getPlayerName();

		if (emojiMatcher.find()) {
			ack.result = -1;// 备注不能包含特殊字符
			if (manageUser.getSession() != null) {
				GameContext.gameSocket.send(manageUser.getSession(), ack);
			}
			return;
		}

		DBOperation msg = new DBOperation();
		msg.dao = DBConstant.CLUB_MEMBER_DAO;
		msg.opertaion = DBConstant.CLUB_MEMBER_update_member_remark;
		msg.playerID = pl.getPlayerID();
		msg.extStr = memberId;
		msg.playerName = clubCode;
		msg.remark = remark;
		dbThread.pushMsg(msg);

		manageUser = getPlayerByPlayerIDFromCache(manageUser.getPlayerID());
		if (manageUser == null || manageUser.getSession() == null) {
			return;
		}

		GameContext.gameSocket.send(manageUser.getSession(), ack);
	}

	/**
	 * 俱乐部成员列表
	 */
	public void GetClubMemberList(User pl, String clubCode, int applyState) {
		ClubMemberListMsgAck ack = new ClubMemberListMsgAck();
		ack.members = clubMemberDAO.getClubMembers(pl.getPlayerID(), clubCode,
				applyState);
		if (applyState == 2) {
			ack.type = 1;
		}
		GameContext.gameSocket.send(pl.getSession(), ack);
	}

	private void optClubMemberDao(int opertaion, String playerId,
			String memberId, String clubCode) {
		DBOperation msg = new DBOperation();
		msg.dao = DBConstant.CLUB_MEMBER_DAO;
		msg.opertaion = opertaion;
		msg.playerID = playerId;
		msg.extStr = memberId;
		msg.playerName = clubCode;
		dbThread.pushMsg(msg);
	}

	private void sendClubMemberOptAck(int opertaionID, User pl,
			User manageUser, User fr, String clubCode) {

		pl = getPlayerByPlayerIDFromCache(pl.getPlayerID());
		if (pl == null || pl.getSession() == null) {
			return;
		}

		ClubMemberOprateMsgAck ack = new ClubMemberOprateMsgAck();
		ack.opertaionID = opertaionID;
		ack.playerId = manageUser.getPlayerID();
		ack.playerName = manageUser.getPlayerName();
		ack.clubCode = clubCode;
		ack.memberId = fr.getPlayerID();
		ack.memberIndex = fr.getPlayerIndex();
		ack.memberName = fr.getPlayerName();
		GameContext.gameSocket.send(pl.getSession(), ack);
	}

	// /**游戏结束，***/
	public void playerGameOver(PlayerGameOverMsg msg, IoSession session) {

	}

	//
	// 游戏循环
	public void run_one_loop() {
		Date ct = DateService.getCurrentUtilDate();
		long delta = ct.getTime() - old_loop_time_ms;
		// 200毫秒一个循环吧
		if (delta < 80)
			return;
		//
		old_loop_time_ms = ct.getTime();
		//
		tableLogic_nndzz.run_one_loop();
	}

	//
	public IUserDAO getPlayerDAO() {
		return userDAO;
	}

	public void setPlayerDAO(IUserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public IPlayerLogDAO getPlayerLogDAO() {
		return playerLogDAO;
	}

	public void setPlayerLogDAO(IPlayerLogDAO playerLogDAO) {
		this.playerLogDAO = playerLogDAO;
	}

	public IClubMemberDAO getClubMemberDAO() {
		return clubMemberDAO;
	}

	public void setClubMemberDAO(IClubMemberDAO clubMemberDAO) {
		this.clubMemberDAO = clubMemberDAO;
	}

	public void setClubTemplateDao(IClubTemplate clubTemplateDao) {
		this.clubTemplateDao = clubTemplateDao;
	}

	/**
	 * 根据配置库，取得当前玩家的卡包
	 */
	public UserBackpack getAllItemByPlayerId(String gameID, String playerID,
			int itemBaseID) {
		List<UserBackpack> backpacks = new ArrayList<UserBackpack>();
		if (gameID != null && "".equals(gameID)) {
			backpacks = userBackpackDAO.getAllItemByPlayer(gameID, playerID);
		} else {
			backpacks = userBackpackDAO.getPlayerItemListByPlayerID(playerID);
		}
		UserBackpack pit = null;
		for (int i = 0; i < backpacks.size(); i++) {
			UserBackpack pi = backpacks.get(i);
			if (pi.getItemBaseID() == itemBaseID) {
				pit = pi;
				break;
			}
		}
		return pit;
	}

	public void setProxyOpenRoomForPlayer(String playerIndexStr,
			String itemNumStr) {
		// 拆分玩家ID
		String[] playerIDs = playerIndexStr.split("\\,");
		String[] itemNums = itemNumStr.split("\\,");
		IUserService playerBaseService = (IUserService) SpringService
				.getBean("playerBaseService");

		for (int i = 0; i < playerIDs.length; i++) {
			int playerIndex = Integer.parseInt(playerIDs[i]);

			User plm = playerBaseService.getPlayerByPlayerIndex(playerIndex); // 只为获取玩家ID

			// 读取缓存中的玩家信息,不能用plm
			User pl = this.getPlayerByPlayerID(plm.getPlayerID());
			int num = Integer.parseInt(itemNums[i]);
			// 更新数据库
			userDAO.updatePlayerProxyOpenRoom(pl.getPlayerID(), num);
			// 更新缓存
			pl.setProxyOpenRoom(num);
			// 更新客户端
			UpdatePlayerProxyOpenRoomMsg upm = new UpdatePlayerProxyOpenRoomMsg();
			upm.init(num);
			GameContext.gameSocket.send(pl.getSession(), upm);
		}
	}

	public void addItemForPlayer(String playerIndexStr, String itemNumStr,
			int itemType, String gameID) {

		// 判断道具信息
		MallItem item = this.getMallItem(itemType);

		// 拆分玩家ID
		String[] playerIDs = playerIndexStr.split("\\,");

		String[] itemNums = itemNumStr.split("\\,");

		IUserService playerBaseService = (IUserService) SpringService
				.getBean("playerBaseService");

		for (int i = 0; i < playerIDs.length; i++) {
			int playerIndex = Integer.parseInt(playerIDs[i]);

			User plm = playerBaseService.getPlayerByPlayerIndex(playerIndex); // 只为获取玩家ID

			// 读取缓存中的玩家信息,不能用plm
			User pl = this.getPlayerByPlayerID(plm.getPlayerID());

			int num = Integer.parseInt(itemNums[i]);

			// 为玩家增加道具
			this.createItemForPlayer(pl, item.getBase_id(), item.getName(),
					LogConstant.OPERATION_TYPE_ADMIN_CHANGE_USER_DATA, num,
					gameID);
		}
	}

	// 给玩家创建一个道具
	public UserBackpack createItemForPlayer(User pl, int itemBaseID,
			String itemName, int logSubType, int num, String gameID) {
		// if (num <= 0)
		// return null;
		// UserBackpack item = pl.getPlayerItemByBaseID(itemBaseID);
		UserBackpack item = this.getAllItemByPlayerId(gameID, pl.getPlayerID(),
				itemBaseID);
		if (item == null) {
			item = new UserBackpack();
			item.setPlayerID(pl.getPlayerID());
			item.setItemBaseID(itemBaseID);
			item.setItemNum(num);
			item.setItemID(pl.getPlayerID() + "_" + itemBaseID);
			userBackpackDAO.createPlayerItem(item, gameID);
			//
			pl.getItems().add(item);
		} else {
			item.setItemNum(item.getItemNum() + num);
			userBackpackDAO.updatePlayerItemNum(pl.getPlayerID(), itemBaseID,
					item.getItemNum(), gameID);
			for (int i = 0; i < pl.getItems().size(); i++) {
				if (pl.getItems().get(i).getItemID().equals(item.getItemID())) {
					pl.getItems().get(i).setItemNum(item.getItemNum());
					logger.debug("充卡后卡数量为：" + pl.getItems().get(i).getItemNum());
				}
			}
		}
		// 按道具类型分下
		int operationType = LogConstant.OPERATION_TYPE_ADD_GOLD;
		if (itemBaseID > 2000 && itemBaseID < 3000) {
			operationType = LogConstant.OPERATION_TYPE_ADD_PRO;
		} else if (itemBaseID > 3000 && itemBaseID < 4000) {
			operationType = LogConstant.OPERATION_TYPE_ADD_DIAMOND;
		} else if (itemBaseID > 4000 && itemBaseID < 5000) {
			operationType = LogConstant.OPERATION_TYPE_ADD_LIFE;
		} else if (itemBaseID > 5000 && itemBaseID < 6000) {
			operationType = LogConstant.OPERATION_TYPE_ADD_PACKS;
		} else if (itemBaseID > 7000 && itemBaseID < 8000) {
			operationType = LogConstant.OPERATION_TYPE_ADD_DRAWCARD;
		}
		UpdatePlayerItemListMsg upilm = new UpdatePlayerItemListMsg();
		upilm.gold = pl.getGold();
		upilm.init_list(pl.getItems());
		upilm.credits = pl.getCredits();
		GameContext.gameSocket.send(pl.getSession(), upilm);

		String detail5 = "添加道具，类型=" + itemBaseID + ",道具名称=" + itemName + ",数量="
				+ num;
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(), operationType, logSubType,
				num, detail5, itemBaseID,
				gameID == null || "".equals(gameID) ? (pl.getGameId() == null
						|| "".equals(pl.getGameId()) ? "ddz" : pl.getGameId())
						: gameID);

		// 清除支付缓存
		if (LogConstant.OPERATION_PAY == logSubType) {
			// 清除支付缓存
			Iterator it = GameContext.getPayMap().entrySet().iterator();
			String key = "";
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				Map<String, String> map1 = (Map<String, String>) pairs
						.getValue();
				if (map1.containsValue(gameID)
						&& map1.containsValue(pl.getPlayerID())) {
					key = (String) pairs.getValue();
					break;
				}
			}
			if (!key.equals("")) {
				GameContext.getPayMap().remove(key);
			}
		}
		return item;
	}

	public boolean useItem(User pl, int itemBaseID, int count) {
		UserBackpack pib = pl.getPlayerItemByBaseID(itemBaseID);

		if (pib == null || pib.getItemNum() <= 0
				|| pib.getItemNum() - count < 0) {
			// 返回给玩家，
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
			ack.gold = pl.getGold();
			//
			ack.result = ErrorCodeConstant.ITEM_NOT_FOUND;
			//
			GameContext.gameSocket.send(pl.getSession(), ack);
			return false;
		}
		// 扣道具
		pib.setItemNum(pib.getItemNum() - count);
		this.udpate_player_item_num(pl.getPlayerID(), itemBaseID,
				pib.getItemNum());

		String detail5 = "扣除道具，类型=" + itemBaseID + ",道具名称=" + pib.getItemName()
				+ ",扣除数量=" + count + ",剩余=" + pib.getItemNum();
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_SUB_PRO,
				LogConstant.OPERATION_TYPE_SUB_PRO_ITEM, 1, detail5,
				pib.getItemBaseID(), pl.getGameId());
		try {
			// 把玩家道具列表更新一下
			UpdatePlayerItemListMsg upilm = new UpdatePlayerItemListMsg();
			upilm.gold = pl.getGold();
			upilm.init_list(pl.getItems());
			upilm.credits = pl.getCredits();
			GameContext.gameSocket.send(pl.getSession(), upilm);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}

		//
		return true;
	}

	public void createItemForAdmin(User pl, UserBackpack item, int old_num,
			String adminName) {
		userBackpackDAO.createPlayerItem(item, "");

		String detail5 = "管理员" + adminName + "修改道具数量，类型="
				+ item.getItemBaseID() + ",道具名称=" + item.getItemName()
				+ ",原始数量" + old_num + ",修改后数量=" + item.getItemNum();
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_ADD_PRO,
				LogConstant.OPERATION_TYPE_ADMIN_CHANGE_USER_DATA,
				item.getItemNum(), detail5, item.getItemBaseID(),
				pl.getGameId());
	}

	public void updateItemForAdmin(User pl, UserBackpack item, int old_num,
			String adminName) {
		userBackpackDAO.updatePlayerItemNum(item.getPlayerID(),
				item.getItemBaseID(), item.getItemNum(), "");

		String detail5 = "管理员" + adminName + "更新道具数量，类型="
				+ item.getItemBaseID() + ",道具名称=" + item.getItemName()
				+ ",原始数量" + old_num + ",修改后数量=" + item.getItemNum();
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_ADD_PRO,
				LogConstant.OPERATION_TYPE_ADMIN_CHANGE_USER_DATA,
				item.getItemNum(), detail5, item.getItemBaseID(),
				pl.getGameId());
	}

	/**
	 * 随机赠送道具,第5天登录送的神秘大礼包,返回道具ID
	 */
	public MallItem giveRandomReward() {
		List<MallItem> lists = new ArrayList<MallItem>();

		for (MallItem i : baseItemList) {
			if (i.getProperty_3() == 0) {
				lists.add(i);
			}
		}

		return lists.get((int) (Math.random() * lists.size()));
	}

	//
	public void luckyDraw(IoSession session, LuckyDrawMsg msg) {

	}

	public void sendPrizeListToCS(IoSession session) {
		// PrizeMachine machine=new PrizeMachine();
		List<Prize> prizeList = prizeLists;
		GetPrizeListMsgAck ack = new GetPrizeListMsgAck();
		ack.list = prizeList;
		GameContext.gameSocket.send(session, ack);

	}

	/**
	 * 通过奖励名称获取奖励名称
	 */
	public String getRewardValue(String name) {
		for (Reward r : rewardLists) {
			if (r.getName().equals(name))
				return r.getValue();
		}

		return "0";

	}

	/**
	 * 通过奖励名称获取奖励
	 */
	public Reward getReward(String name) {
		for (Reward r : rewardLists) {
			if (r.getName().equals(name))
				return r;
		}
		return null;

	}

	//
	private void update_big_gift() {

		//
		bigRewardStringForClient = "包括";
	}

	/**
	 * 判断是否自然整数，就是正确的钱币数
	 */
	public boolean isNum(String str) {

		return str.matches("([1-9][0-9]*)|[0]");
	}

	/**
	 * 花费人民币后，计算VIP经验值
	 */
	public void countVIP(User p, int yuan) {

		int exp = p.getVipExp() + yuan * VipConstant.EXP_BASE;
		int vipLevel = 0;
		if (exp > VipConstant.VIPL5_EXP) {
			vipLevel = VipConstant.VIPL5;
		} else if (exp > VipConstant.VIPL4_EXP) {
			vipLevel = VipConstant.VIPL4;
		} else if (exp > VipConstant.VIPL3_EXP) {
			vipLevel = VipConstant.VIPL3;
		} else if (exp > VipConstant.VIPL2_EXP) {
			vipLevel = VipConstant.VIPL2;
		} else if (exp > VipConstant.VIPL1_EXP) {
			vipLevel = VipConstant.VIPL1;
		} else {
			vipLevel = VipConstant.VIPL0;
		}
		if (vipLevel != p.getVipLevel()) {
			p.setVipLevel(vipLevel);
		}
		p.setVipExp(exp);

		DBOperation msg = new DBOperation();
		msg.playerID = p.getPlayerID();
		msg.vipExp = p.getVipExp();
		msg.vipLevel = p.getVipLevel();
		msg.dao = DBConstant.USER_DAO;
		msg.opertaion = DBConstant.USER_DAO_updatePlayerVipAndExp;
		dbThread.pushMsg(msg);
		// userDAO.updateVIP(p.getPlayerID(), p.getVipExp(), p.getVipLevel());
	}

	/**
	 * 更新玩家任务数据
	 */
	// public void updatePlayerExtByTaskID(String playerID, Integer taskID,
	// Integer addNum) {
	// taskService.updatePlayerExtByTaskID(playerID, taskID, addNum);
	// }
	//
	// /**
	// * 随机获取一个进阶场任务
	// */
	// public PlayerTask getRandomPlayerTask(String playerID) {
	// return taskService.getRandomPlayerTask(playerID);
	// }
	//
	// /**
	// * 获取进阶场随机任务加成分数*
	// */
	// public Integer checkPlayerTaskIsFinish(String playerID,
	// PlayerTask playerTask) {
	// return taskService.checkPlayerTaskIsFinish(playerID, playerTask);
	// }

	/**
	 * 玩家每日的最高得分清零
	 */
	public void daySchedClearToDayBestScore() {
		// 清数据库
		userDAO.clearTodayBestScoreAllPlayer();
		// 清缓存
		for (User pl : this.playerMap.values()) {
			pl.setTodayBestScore(0);
		}
	}

	// 查询某种房卡的基础表暑假
	private MallItem getFangkaBaseItem(int quanNum, User pl) {
		int baseID = 3000;
		if (1 == pl.getDeviceFlag()) {
			baseID = 3200; // IOS的房卡ID从3201开始
		}

		for (int i = 0; i < baseItemList.size(); i++) {
			MallItem it = baseItemList.get(i);
			if (it.getBase_id() > baseID && it.getBase_id() < 4000) {
				if (it.getProperty_1() == quanNum)
					return it;
			}
		}

		return null;
	}

	// 通用房卡验证
	public boolean commonPreuse_fanka(User pl, int quanNum, boolean isCardLock) {
		List<UserBackpack> pis = pl.getItems();
		int cardCount = 0;
		for (int i = 0; i < pis.size(); i++) {
			cardCount += pis.get(i).getItemNum();
		}
		logger.info("验证玩家【" + pl.getPlayerIndex() + "】【" + pl.getPlayerName()
				+ "】开房房卡 num = " + quanNum + "/" + cardCount + " 是否充足");
		if (quanNum > cardCount) {
			com.chess.common.msg.struct.other.RequestStartGameMsgAck ack = new com.chess.common.msg.struct.other.RequestStartGameMsgAck();
			ack.gold = pl.getGold();
			// ack.result = ErrorCodeConstant.FANGKIA_NOT_FOUND;
			// 代开优化 cc modify 2017-9-26
			if (isCardLock) {
				ack.result = ErrorCodeConstant.FANGKIA_LOCKED;
			} else {
				ack.result = ErrorCodeConstant.FANGKIA_NOT_FOUND;
			}
			GameContext.gameSocket.send(pl.getSession(), ack);
			return false;
		}
		return true;
	}

	public boolean preuse_fanka(User pl, int itemBaseID) {

		UserBackpack pib = pl.getPlayerItemByBaseID(itemBaseID);

		if (pib == null || pib.getItemNum() < 6) {
			// 返回给玩家，
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
			ack.gold = pl.getGold();
			//
			ack.result = ErrorCodeConstant.FANGKIA_NOT_FOUND;
			//
			GameContext.gameSocket.send(pl.getSession(), ack);
			return false;
		}
		//
		return true;
	}

	// 获取玩家房卡总数
	public int getItemCount(User pl) {
		int cardCount = 0;
		List<UserBackpack> items = pl.getItems();
		for (int i = 0; i < items.size(); i++) {
			UserBackpack pi = items.get(i);
			cardCount += pi.getItemNum();
		}
		return cardCount;
	}

	/** 通用扣卡 */
	public boolean use_CommonFangka(User pl, int roomType, int quanNum,
			int clubCode) {
		int opSubType = 0;
		load_player_items(pl);
		int cardNum = getItemCount(pl);
		if (cardNum < quanNum) { // 如果玩家卡包总数小于要扣的卡数
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
			ack.gold = pl.getGold();
			ack.result = ErrorCodeConstant.FANGKIA_NOT_FOUND;
			GameContext.gameSocket.send(pl.getSession(), ack);
			return false;
		}
		// 循环玩家卡包，挨个扣卡
		List<UserBackpack> pis = pl.getItems();
		for (int i = 0; i < pis.size(); i++) {
			UserBackpack pib = pis.get(i);
			if (pib.getItemNum() > quanNum) {// 如果当前道具包总数大于需要扣的卡
				pib.setItemNum(pib.getItemNum() - quanNum);
				userBackpackDAO.updatePlayerItemNum(pl.getPlayerID(),
						pib.getItemBaseID(), pib.getItemNum(), "");

				addPlayerCredits(pl.getPlayerID(), quanNum);

				String detail5 = "玩家有一种卡包足够使用, VIP开房成功扣除房卡，类型="
						+ pib.getItemBaseID() + ",道具名称=" + pib.getItemName()
						+ ",数量=" + quanNum + "";
				logger.info("use_fanka_roomType:" + roomType);

				if (roomType == GameConstant.ROOM_TYPE_COUPLE)
					opSubType = LogConstant.OPERATION_TYPE_SUB_PRO_CREATE_2_VIP_TABLE;
				else
					opSubType = LogConstant.OPERATION_TYPE_SUB_PRO_CREATE_4_VIP_TABLE;

				logger.info("use_fanka:" + opSubType);
				createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
						pl.getPlayerName(), pl.getGold(),
						LogConstant.OPERATION_TYPE_SUB_PRO, opSubType, 1,
						detail5, pib.getItemBaseID(), pl.getGameId(), clubCode);

				break;
			} else {
				if (quanNum > 0) {
					String detail5 = "玩家有一种卡包刚好够使用, VIP开房成功扣除房卡，类型="
							+ pib.getItemBaseID() + ",道具名称="
							+ pib.getItemName() + ",数量=" + quanNum + "";
					logger.info("use_fanka_roomType:" + roomType);

					quanNum = quanNum - pib.getItemNum();
					pib.setItemNum(0);
					userBackpackDAO.updatePlayerItemNum(pl.getPlayerID(),
							pib.getItemBaseID(), pib.getItemNum(), "");

					if (roomType == GameConstant.ROOM_TYPE_COUPLE)
						opSubType = LogConstant.OPERATION_TYPE_SUB_PRO_CREATE_2_VIP_TABLE;
					else
						opSubType = LogConstant.OPERATION_TYPE_SUB_PRO_CREATE_4_VIP_TABLE;

					logger.info("use_fanka:" + opSubType);
					createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
							pl.getPlayerName(), pl.getGold(),
							LogConstant.OPERATION_TYPE_SUB_PRO, opSubType, 1,
							detail5, pib.getItemBaseID(), pl.getGameId());
				}

			}
		}

		// 把玩家道具列表更新一下
		UpdatePlayerItemListMsg upilm = new UpdatePlayerItemListMsg();
		upilm.gold = pl.getGold();
		upilm.init_list(pl.getItems());
		upilm.credits = pl.getCredits();
		GameContext.gameSocket.send(pl.getSession(), upilm);

		return true;
	}

	public boolean use_fangka(User pl, int itemBaseID, int roomType, int num) {
		UserBackpack pib = pl.getPlayerItemByBaseID(itemBaseID);
		if (pib == null || pib.getItemNum() <= 0) {
			switch (pl.getDeviceFlag()) {
			case ConfigConstant.DEVICE_TYPE_IOS: {
				pib = pl.getPlayerItemByBaseID(itemBaseID + 200);
				itemBaseID = pib.getItemBaseID();
				break;
			}
			case ConfigConstant.DEVICE_TYPE_ANDROID: {
				pib = pl.getPlayerItemByBaseID(itemBaseID - 200);
				itemBaseID = pib.getItemBaseID();
				break;
			}
			}
		}
		if (pib == null || pib.getItemNum() <= 0) {
			// 返回给玩家，
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
			ack.gold = pl.getGold();
			//
			ack.result = ErrorCodeConstant.FANGKIA_NOT_FOUND;
			//
			GameContext.gameSocket.send(pl.getSession(), ack);
			return false;
		}

		// 扣开房卡
		pib.setItemNum(pib.getItemNum() - num);

		userBackpackDAO.updatePlayerItemNum(pl.getPlayerID(), itemBaseID,
				pib.getItemNum(), "");

		String detail5 = "VIP开房成功扣除房卡，类型=" + itemBaseID + ",道具名称="
				+ pib.getItemName() + ",数量=" + num;
		logger.info("use_fanka_roomType:" + roomType);
		int opSubType = 0;
		if (roomType == GameConstant.ROOM_TYPE_COUPLE)
			opSubType = LogConstant.OPERATION_TYPE_SUB_PRO_CREATE_2_VIP_TABLE;
		else
			opSubType = LogConstant.OPERATION_TYPE_SUB_PRO_CREATE_4_VIP_TABLE;

		logger.info("use_fanka:" + opSubType + " detail = " + detail5);
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_SUB_PRO, opSubType, 1, detail5,
				pib.getItemBaseID(), pl.getGameId());

		// 把玩家道具列表更新一下
		UpdatePlayerItemListMsg upilm = new UpdatePlayerItemListMsg();
		upilm.gold = pl.getGold();
		upilm.init_list(pl.getItems());
		upilm.credits = pl.getCredits();
		GameContext.gameSocket.send(pl.getSession(), upilm);

		//
		return true;
	}

	/**
	 * 创建房间-麻将
	 */
	public void createVipRoom(CreateVipRoomMsg msg, User pl) {
		if (msg.roomID == 0) {
			return;
		}
		pl.setGameId(msg.gameID);
		// 验证动态扣卡  20170330
		ICacheGameConfigService service = (ICacheGameConfigService) SpringService
				.getBean("gameConfigService");
		GameConfig config = service.getGameConfig(msg.gameID);
		Map<String, Integer> mapWanfa = config.getVipInfo(this.tableLogic_nndzz
				.getRoom(msg.roomID).roomType);
		/* 扣卡配置修改：cc modify 2017-08-14 */
		int kaKouNum = mapWanfa.get("before_ka_0");
		if (msg.quanNum == 8) {
			kaKouNum = mapWanfa.get("after_ka_0");
			if (msg.isProxy == 1) {
				kaKouNum = mapWanfa.get("after_ka_1");
			} else if (msg.isProxy == 2) {
				kaKouNum = mapWanfa.get("after_ka_2");
			}
		} else {
			if (msg.isProxy == 1) {
				kaKouNum = mapWanfa.get("before_ka_1");
			} else if (msg.isProxy == 2) {
				kaKouNum = mapWanfa.get("before_ka_2");
			}
		}
		/* 俱乐部库存验证  2018-9-6 */
		if (msg.clubCode == 0) {
			// 代开优化 cc modify 2017-9-26
			int kakouNumProxy = kaKouNum;
			boolean isCardLock = false;
			if (null != pl.getPvrrs() && pl.getPvrrs().size() > 0) {
				for (ProxyVipRoomRecord pvrr : pl.getPvrrs()) {
					if (pvrr.getVipState() == GameConstant.PROXY_NO_START_GAME) {
						kakouNumProxy += pvrr.getProxyCardCount();
						isCardLock = true;
					}
				}
			}
			// 验证扣卡
			if (commonPreuse_fanka(pl, kakouNumProxy, isCardLock) == false) {
				return;
			}
		} else {
			// 俱乐部
			TClub club = tClubDao.getClubByClubCode(msg.clubCode);
			if (club != null && kaKouNum > club.getFangkaNum()) {
				// 库存不足
				EnterVipRoomMsgAck ack = new EnterVipRoomMsgAck();
				ack.result = 2;
				ack.currKcNum = club.getFangkaNum();
				GameContext.gameSocket.send(pl.getSession(), ack);
				return;
			}
		}

		logger.info("==>创建VIP房，玩家ID【" + pl.getPlayerIndex() + "】，玩家昵称【"
				+ pl.getPlayerName() + "】，所选规则【" + msg.tableRule + "】");

		// STiV modify
		if (this.tableLogic_nndzz.getRoom(msg.roomID).roomType == GameConstant.ROOM_TYPE_COUPLE)
			pl.setTwoVipOnline(true);
		pl.setGameId(msg.gameID);
//		tableLogic_nndzz.create_vip_table(pl, msg.psw, msg.quanNum, msg.roomID,
//				msg.tableRule, msg.gameID, msg.isRecord, msg.isProxy, kaKouNum,
//				msg.clubCode);

		// test
		// updateClubType(6000609, 2);
	}

	/**
	 * 根据俱乐部编号获取俱乐部
	 * 
	 * @author  2018-9-6 CacheUserService.java
	 * @param clubCode
	 * @return TClub
	 */
	public TClub getClubByClubCode(int clubCode) {
		return tClubDao.getClubByClubCode(clubCode);
	}

	public void enterVipRoom(EnterVipRoomMsg msg, User pl) {

//		// 验证俱乐部库存是否充足 
//		if (msg.clubCode > 0) {
//			TClub club = tClubDao.getClubByClubCode(msg.clubCode);
//			if (club != null) {
//				GameTable gt = tableLogic_nndzz.getGameTableByTableId(msg.tableID);
//				if (gt.getCostKaNum() > club.getFangkaNum()) {
//					// 库存不足
//					EnterVipRoomMsgAck ack = new EnterVipRoomMsgAck();
//					ack.result = 2;
//					ack.currKcNum = club.getFangkaNum();
//					GameContext.gameSocket.send(pl.getSession(), ack);
//					return;
//				}
//			}
//		}
//
//		boolean enterOld = enterVipRoom(msg.roomID, pl);
//		if (enterOld) {
//			return;
//		}
//
//		// pl.setVipTableID(msg.tableID);
//		pl.setTableID(msg.tableID);
//		pl.setRoomID(msg.roomID);
//		pl.setNeedCopyGameState(true);
//
//		tableLogic_nndzz.enterVipRoom(msg, pl);

	}

	// ============代开============
	public void getProxyVipRoomRecord(ProxyVipRoomRecordMsg vrrm,
			IoSession session) {
		ProxyVipRoomRecordMsgAck vrram = new ProxyVipRoomRecordMsgAck();
		vrram = this.getProxyRecordByPlayerId(vrrm.playerID);
		vrram.recordType = vrrm.recordType;
		vrram.pageCurrentNum = vrrm.pageCurrentNum;
		// 根据gameID获得代开记录
		vrram.proxyRoomRecord = getProxyRecordByGameID(vrram.proxyRoomRecord,
				vrrm.gameID);
		if (vrrm.recordType == 0) {// 只获取未开始游戏的代开记录
			vrram.proxyRoomRecord = getProxyRecordUnStart(vrram.proxyRoomRecord);
			if (vrram.proxyRoomRecord.size() > 0) {
				vrram.pageTotalNum = 1;
				vrram.pageCurrentNum = 1;
			} else {
				vrram.pageTotalNum = 0;
			}
		} else {// 分页获取代开记录
			if (vrram.proxyRoomRecord.size() > 0) {
				vrram.pageTotalNum = (int) Math.ceil(vrram.proxyRoomRecord
						.size() * 1.0 / 10);
			} else {
				vrram.pageTotalNum = 0;
			}
			if (vrram.pageCurrentNum > 0) {
				vrram.proxyRoomRecord = getProxyRecordByPage(
						vrram.proxyRoomRecord, vrram.pageCurrentNum);
			} else {
				vrram.proxyRoomRecord = getProxyRecordByPage(
						vrram.proxyRoomRecord, 1);
			}
		}
		GameContext.gameSocket.send(session, vrram);
	}

	// 过滤代开记录集合，只获取未开始游戏的记录
	public List<ProxyVipRoomRecord> getProxyRecordUnStart(
			List<ProxyVipRoomRecord> pvrr) {
		List<ProxyVipRoomRecord> pvrr_ = new ArrayList<ProxyVipRoomRecord>();
		for (ProxyVipRoomRecord p : pvrr) {
			if (p.getVipState() == GameConstant.PROXY_NO_START_GAME) {
				pvrr_.add(p);
			}
		}
		return pvrr_;
	}

	// 过滤代开记录集合，根据gameid返回代开记录
	public List<ProxyVipRoomRecord> getProxyRecordByGameID(
			List<ProxyVipRoomRecord> pvrr, String gameID) {
		List<ProxyVipRoomRecord> pvrr_ = new ArrayList<ProxyVipRoomRecord>();
		for (ProxyVipRoomRecord p : pvrr) {
			if (p.getGameId().equals(gameID)) {
				pvrr_.add(p);
			}
		}
		return pvrr_;
	}

	// 过滤代开记录集合，根据gameid返回代开记录
	public List<ProxyVipRoomRecord> getProxyRecordByPage(
			List<ProxyVipRoomRecord> pvrr, int begin) {
		List<ProxyVipRoomRecord> pvrr_ = new ArrayList<ProxyVipRoomRecord>();
		if ((begin - 1) * 10 + 10 < pvrr.size()) {
			pvrr_ = pvrr.subList((begin - 1) * 10, (begin - 1) * 10 + 10);
		} else {
			pvrr_ = pvrr.subList((begin - 1) * 10, pvrr.size());
		}
		return pvrr_;
	}

	public ProxyVipRoomRecordMsgAck getProxyRecordByPlayerId(String playerID) {
		ProxyVipRoomRecordMsgAck vrram = new ProxyVipRoomRecordMsgAck();
		List<ProxyVipRoomRecord> recordList = this.proxyVipRoomRecord
				.get(playerID);
		if (recordList == null || recordList.size() <= 0) {
			recordList = proxyVipRoomRecordDAO.getMyProxyVipRoomRecord(null,
					playerID);
			if (recordList != null) {
				this.proxyVipRoomRecord.put(playerID, recordList);
				vrram.proxyRoomRecord = recordList;
			}
		} else {
			vrram.proxyRoomRecord = recordList;
		}

		// 代开记录中，房间状态为未开始游戏（vipState==0）,则从牌桌缓存中获取当前牌桌内玩家
		if (vrram.proxyRoomRecord != null && vrram.proxyRoomRecord.size() > 0) {
			// System.out.println("===代开记录："+vrram.proxyRoomRecord.size());
			for (int v = vrram.proxyRoomRecord.size() - 1; v >= 0; v--) {
				ProxyVipRoomRecord proxyVipRoomRecord = vrram.proxyRoomRecord
						.get(v);
				/* 过滤俱乐部牌桌  2018-9-11 */
				if (proxyVipRoomRecord.getClubCode() > 0) {
					// System.out.println("===代开记录mod del club["+proxyVipRoomRecord.getClubCode()+"] table：");
					vrram.proxyRoomRecord.remove(v);
					continue;
				}
				if (proxyVipRoomRecord.getVipState() == GameConstant.PROXY_NO_START_GAME) {
					GameTable gameTable = this.tableLogic_nndzz
							.getGameTable(proxyVipRoomRecord.getRoomIndex()
									+ "");
					if (gameTable != null) {
						List<User> players = gameTable.getPlayers();
						if (players.size() > 0) {
							this.updateProxyPlayerInfo(proxyVipRoomRecord,
									players);
						}
//						proxyVipRoomRecord.setTableRules(gameTable
//								.getTableRule_Option());
					} else {
						vrram.proxyRoomRecord.remove(v);
						proxyVipRoomRecord.setEndWay("游戏未开始，服务器重启解散房间");
						proxyVipRoomRecord
								.setVipState(GameConstant.PROXY_SERVER_END_GAME);
						this.endProxyVipRoomRecord(proxyVipRoomRecord);
					}
				} else if (proxyVipRoomRecord.getVipState() == GameConstant.PROXY_START_GAME) {
					GameTable gameTable = this.tableLogic_nndzz
							.getGameTable(proxyVipRoomRecord.getRoomIndex()
									+ "");
					if (gameTable == null) {
						proxyVipRoomRecord.setEndWay("游戏已开始，服务器重启解散房间");
						proxyVipRoomRecord
								.setVipState(GameConstant.PROXY_SERVER_END_GAME_START);
						this.endProxyVipRoomRecord(proxyVipRoomRecord);
					}
				}
			}
		}
		// 返回代开记录时，按代开时间排序
		Collections.sort(vrram.proxyRoomRecord,
				new Comparator<ProxyVipRoomRecord>() {
					public int compare(ProxyVipRoomRecord o1,
							ProxyVipRoomRecord o2) {
						return o2.getProxyStartTime().compareTo(
								o1.getProxyStartTime());
					}
				});
		// System.out.println("===代开记录mod after："+vrram.proxyRoomRecord.size());
		return vrram;
	}

	public void updateProxyPlayerInfo(ProxyVipRoomRecord proxyVipRoomRecord,
			List<User> players) {
		proxyVipRoomRecord.setPlayer1Name("");
		proxyVipRoomRecord.setPlayer1Index(-1);
		proxyVipRoomRecord.setPlayer2Name("");
		proxyVipRoomRecord.setPlayer2Index(-1);
		proxyVipRoomRecord.setPlayer3Name("");
		proxyVipRoomRecord.setPlayer3Index(-1);
		proxyVipRoomRecord.setPlayer4Name("");
		proxyVipRoomRecord.setPlayer4Index(-1);

		for (int i = 0; i < players.size(); i++) {
			if (i == 0) {
				proxyVipRoomRecord.setPlayer1Name(players.get(i)
						.getPlayerName());
				proxyVipRoomRecord.setPlayer1Index(players.get(i)
						.getPlayerIndex());
			} else if (i == 1) {
				proxyVipRoomRecord.setPlayer2Name(players.get(i)
						.getPlayerName());
				proxyVipRoomRecord.setPlayer2Index(players.get(i)
						.getPlayerIndex());
			} else if (i == 2) {
				proxyVipRoomRecord.setPlayer3Name(players.get(i)
						.getPlayerName());
				proxyVipRoomRecord.setPlayer3Index(players.get(i)
						.getPlayerIndex());
			} else {
				proxyVipRoomRecord.setPlayer4Name(players.get(i)
						.getPlayerName());
				proxyVipRoomRecord.setPlayer4Index(players.get(i)
						.getPlayerIndex());
			}
		}
	}

	public void createProxyVipRoomRecord(ProxyVipRoomRecord vrrm) {
		//
		vrrm.setHostName(filterEmojiUserName(vrrm.getHostName()));
		vrrm.setPlayer1Name(filterEmojiUserName(vrrm.getPlayer1Name()));
		vrrm.setPlayer2Name(filterEmojiUserName(vrrm.getPlayer2Name()));
		vrrm.setPlayer3Name(filterEmojiUserName(vrrm.getPlayer3Name()));
		vrrm.setPlayer4Name(filterEmojiUserName(vrrm.getPlayer4Name()));
		vrrm.setProxyName(filterEmojiUserName(vrrm.getProxyName()));
		vrrm.setEndWay(filterEmojiUserName(vrrm.getEndWay()));

		String proxyID = vrrm.getProxyID();
		List<ProxyVipRoomRecord> records = this.proxyVipRoomRecord.get(proxyID);
		if (records == null) {
			records = new ArrayList<ProxyVipRoomRecord>();
		}
		records.add(vrrm);
		this.proxyVipRoomRecord.put(proxyID, records);
		proxyVipRoomRecordDAO.createProxyVipRoomRecord(vrrm);
		//
	}

	public void updateProxyVipRoomRecord(ProxyVipRoomRecord vrrm) {

		vrrm.setHostName(filterEmojiUserName(vrrm.getHostName()));
		vrrm.setPlayer1Name(filterEmojiUserName(vrrm.getPlayer1Name()));
		vrrm.setPlayer2Name(filterEmojiUserName(vrrm.getPlayer2Name()));
		vrrm.setPlayer3Name(filterEmojiUserName(vrrm.getPlayer3Name()));
		vrrm.setPlayer4Name(filterEmojiUserName(vrrm.getPlayer4Name()));
		vrrm.setProxyName(filterEmojiUserName(vrrm.getProxyName()));
		vrrm.setEndWay(filterEmojiUserName(vrrm.getEndWay()));

		String proxyID = vrrm.getProxyID();
		List<ProxyVipRoomRecord> records = this.proxyVipRoomRecord.get(proxyID);
		if (records != null) {
			for (int i = records.size() - 1; i >= 0; i--) {
				if (records.get(i).getRecordID() == vrrm.getRecordID()) {
					this.proxyVipRoomRecord.get(proxyID).remove(i);
					this.proxyVipRoomRecord.get(proxyID).add(i, vrrm);
				}
			}
		}

		DBOperation omsg = new DBOperation();
		omsg.dao = DBConstant.PROXY_VIP_ROOM_DAO;
		omsg.opertaion = DBConstant.PROXY_VIP_ROOM_DAO_update_vip_room;
		omsg.proxyVipRoomRecord = vrrm;

		dbThread.pushMsg(omsg);

		// vipRoomRecordDAO.updateRecord(vrrm);
	}

	public void endProxyVipRoomRecord(ProxyVipRoomRecord vrrm) {

		vrrm.setHostName(filterEmojiUserName(vrrm.getHostName()));
		vrrm.setPlayer1Name(filterEmojiUserName(vrrm.getPlayer1Name()));
		vrrm.setPlayer2Name(filterEmojiUserName(vrrm.getPlayer2Name()));
		vrrm.setPlayer3Name(filterEmojiUserName(vrrm.getPlayer3Name()));
		vrrm.setPlayer4Name(filterEmojiUserName(vrrm.getPlayer4Name()));
		vrrm.setProxyName(filterEmojiUserName(vrrm.getProxyName()));
		vrrm.setEndWay(filterEmojiUserName(vrrm.getEndWay()));

		String proxyID = vrrm.getProxyID();
		List<ProxyVipRoomRecord> records = this.proxyVipRoomRecord.get(proxyID);
		if (records != null) {
			for (int i = records.size() - 1; i >= 0; i--) {
				if (records.get(i).getRecordID() == vrrm.getRecordID()) {
					this.proxyVipRoomRecord.get(proxyID).remove(i);
					this.proxyVipRoomRecord.get(proxyID).add(i, vrrm);
				}
			}
		}

		DBOperation omsg = new DBOperation();
		omsg.dao = DBConstant.PROXY_VIP_ROOM_DAO;
		omsg.opertaion = DBConstant.PROXY_VIP_ROOM_DAO_end_vip_room;
		omsg.proxyVipRoomRecord = vrrm;

		dbThread.pushMsg(omsg);
	}

	public void proxyCloseVipRoomRecord(ProxyVipRoomCloseMsg pvrcm,
			IoSession session) {
		tableLogic_nndzz.proxyCloseVipTable(pvrcm.playerID, pvrcm.roomID);
	}

	// ===========代开=================
	/**
	 * 判断是否能断线重连
	 */
	public void reEnterRoom(User pl) {
//		List<GameTable> oldTables = this.tableLogic_nndzz.getPlayingTables(pl
//				.getPlayerIndex());
//		for (int i = 0; i < oldTables.size(); i++) {
//			GameTable gt = oldTables.get(i);
//			List<User> gtPlayers = gt.getPlayers();
//			for (int j = 0; j < gtPlayers.size(); j++) {
//				if (gtPlayers.get(j).getPlayerID().equals(pl.getPlayerID())) {
//					// 有房间；还需判断玩家是否为代开者，如果是代开者，那么房间内超过两人，则为普通房间，即不能再代开，不能再开房间
//					if (gt.getProxyCreator() == null
//							|| (gt.getProxyCreator() != null && gtPlayers
//									.size() > 0)) {
//						// 在其他桌子中不能进入，直接进入老桌子
//						this.tableLogic_nndzz.enter_room(pl, pl.getRoomID(),
//								pl.getTableID());
//					}
//				}
//			}
//		}
	}

	//
	public void inviteFriendToVipRoom(InviteFriendEnterVipRoomMsg msg, User pl) {

		tableLogic_nndzz.inviteFriend(msg, pl);
	}

	// 玩家不同意邀请 add by  2016.9.13
	public void beInvitedAck(InviteFriendEnterVipRoomAckMsg msg, User pl) {
//		tableLogic_nndzz.beInvitedAckLogic(msg, pl);
	}

	//
	public IUserBackpackDAO getUserBackpackDAO() {
		return userBackpackDAO;
	}

	public void setUserBackpackDAO(IUserBackpackDAO userBackpackDAO) {
		this.userBackpackDAO = userBackpackDAO;
	}

	public IMallItemDAO getMallItemDAO() {
		return mallItemDAO;
	}

	public void setMallItemDAO(IMallItemDAO mallItemDAO) {
		this.mallItemDAO = mallItemDAO;
	}

	public IPrizeDAO getPrizeDAO() {
		return prizeDAO;
	}

	public void setPrizeDAO(IPrizeDAO prizeDAO) {
		this.prizeDAO = prizeDAO;
	}

	public IRewardDAO getRewardDAO() {
		return rewardDAO;
	}

	public void setRewardDAO(IRewardDAO rewardDAO) {
		this.rewardDAO = rewardDAO;
	}

	public List<Reward> getRewardLists() {
		return rewardLists;
	}

	public void setRewardLists(List<Reward> rewardLists) {
		this.rewardLists = rewardLists;
		update_big_gift();
	}

	//
	// public ITaskService getTaskService() {
	// return taskService;
	// }
	//
	// public void setTaskService(ITaskService taskService) {
	// this.taskService = taskService;
	// }

	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	public ILoginLogDAO getLoginLogDAO() {
		return loginLogDAO;
	}

	public void setLoginLogDAO(ILoginLogDAO loginLogDAO) {
		this.loginLogDAO = loginLogDAO;
	}

	public IAvgPeopleOnlineDAO getAvgPeopleOnlineDAO() {
		return avgPeopleOnlineDAO;
	}

	public void setAvgPeopleOnlineDAO(IAvgPeopleOnlineDAO avgPeopleOnlineDAO) {
		this.avgPeopleOnlineDAO = avgPeopleOnlineDAO;
	}

	public IPayService getPayService() {
		return payService;
	}

	/**
	 * 记录当前在线人数，5分钟一调度
	 */
	public void onlinePlayerNums() {
		Date nowDate = DateService.getCurrentUtilDate();
		// 天翼空间在线统计数据
		AvgPeopleOnline a = new AvgPeopleOnline();
		int onLineNum = 0;
		onLineNum = 0;
		for (User pl : playerMap.values()) {
			if (pl.isOnline() && !pl.isRobot()) {
				onLineNum++;
			}
		}
		a.setPeoNums(onLineNum);
		a.setTime(nowDate);
		a.setTotalPaidMoney(mallHistoryDAO.getPayTotalNumByTime(null, nowDate,
				-1));
		a.setTotalPaidPeople(mallHistoryDAO.getPayPlayerNumByTime(null,
				nowDate, -1));
		a.setTotalLoginPlayer(userDAO.getTotalCount(null, null, 0));
		a.setPlatformType(-1);
		avgPeopleOnlineDAO.insert(a);

	}

	// 清除大于2小时的缓存
	public void clearPlayerCatch() {
		Iterator<Entry<String, User>> accountiter = playerAccountMap.entrySet()
				.iterator();
		Iterator<Entry<String, User>> machineiter = playerMachineCodeMap
				.entrySet().iterator();
		Iterator<Entry<String, User>> qqiter = playerQQMap.entrySet()
				.iterator();
		Iterator<Entry<String, User>> wxiter = playerWXMap.entrySet()
				.iterator();

		while (accountiter.hasNext()) {
			User pl = accountiter.next().getValue();
			if (!pl.isOnline() && pl.getOfflineTime() != null
					&& pl.getVipTableID().length() <= 0) {
				if (System.currentTimeMillis() - pl.getOfflineTime().getTime() > 7200000l) {// 7200000l
					logger.info("playerAccountMap清除大于2小时的缓存, 玩家ID:"
							+ pl.getPlayerIndex() + "，玩家昵称："
							+ pl.getPlayerName());
					accountiter.remove();
					playerMap.remove(pl.getPlayerID());
					playerAccountMap.remove(pl.getAccount());
					pl.setVipTableID("");
					pl = null;
				}
			}
		}

		while (machineiter.hasNext()) {
			User pl = machineiter.next().getValue();
			if (!pl.isOnline() && pl.getOfflineTime() != null
					&& pl.getVipTableID().length() <= 0) {
				if (System.currentTimeMillis() - pl.getOfflineTime().getTime() > 7200000l) {
					logger.info("playerMachineCodeMap清除大于2小时的缓存, 玩家ID:"
							+ pl.getPlayerIndex() + "，玩家昵称："
							+ pl.getPlayerName());
					machineiter.remove();
					playerMap.remove(pl.getPlayerID());
					playerMachineCodeMap.remove(pl.getMachineCode());
					pl.setVipTableID("");
					pl = null;
				}
			}
		}

		while (qqiter.hasNext()) {
			User pl = qqiter.next().getValue();
			if (!pl.isOnline() && pl.getOfflineTime() != null
					&& pl.getVipTableID().length() <= 0) {
				if (System.currentTimeMillis() - pl.getOfflineTime().getTime() > 7200000l) {
					logger.info("playerQQMap清除大于2小时的缓存, 玩家ID:"
							+ pl.getPlayerIndex() + "，玩家昵称："
							+ pl.getPlayerName());
					qqiter.remove();
					playerMap.remove(pl.getPlayerID());
					playerQQMap.remove(pl.getQqOpenID());
					pl.setVipTableID("");
					pl = null;
				}
			}
		}

		while (wxiter.hasNext()) {
			User pl = wxiter.next().getValue();
			if (!pl.isOnline() && pl.getOfflineTime() != null
					&& pl.getVipTableID().length() <= 0) {
				if (System.currentTimeMillis() - pl.getOfflineTime().getTime() > 7200000l) {
					logger.info("playerWXMap清除大于2小时的缓存, 玩家ID:"
							+ pl.getPlayerIndex() + "，玩家昵称："
							+ pl.getPlayerName());
					wxiter.remove();
					playerMap.remove(pl.getPlayerID());
					// playerWXMap.remove(pl.getWxOpenID());
					playerWXMap.remove(pl.getUnionID());// cc modify 2017-12-5
					pl.setVipTableID("");
					pl = null;
				}
			}
		}

	}

	public void clearOldSession() {
		Iterator<Entry<String, IoSession>> oldSessionentry = oldSession
				.entrySet().iterator();
		while (oldSessionentry.hasNext()) {
			IoSession io = oldSessionentry.next().getValue();

			long cuttiem = Long
					.parseLong(io.getAttribute("cuttime").toString());
			if (System.currentTimeMillis() - cuttiem > 20000) {
				io.close();
			}
		}
	}

	public void setPayService(IPayService payService) {
		this.payService = payService;
	}

	/**
	 * 玩家操作-mj（加入房间入口）
	 */
	public void playerOperation(PlayerTableOperationMsg msg, User pl) {
//		this.tableLogic_nndzz.playerOperation(msg, pl);
	}

	public void setUserFriendDAO(IUserFriendDAO userFriendDAO) {
		this.userFriendDAO = userFriendDAO;
	}

	public IVipGameRecordDAO getVipGameRecordDAO() {
		return vipGameRecordDAO;
	}

	public void setVipGameRecordDAO(IVipGameRecordDAO vipGameRecordDAO) {
		this.vipGameRecordDAO = vipGameRecordDAO;
	}

	public IVipRoomRecordDAO getVipRoomRecordDAO() {
		return vipRoomRecordDAO;
	}

	public void setVipRoomRecordDAO(IVipRoomRecordDAO vipRoomRecordDAO) {
		this.vipRoomRecordDAO = vipRoomRecordDAO;
	}

	public void getVipGameRecord(VipGameRecordMsg vgrm, IoSession session) {
		VipGameRecordAckMsg vgram = new VipGameRecordAckMsg();
		// vgram.gameRecords = vipGameRecordDAO.getVipGameRecordByRoomID(
		// vgrm.roomID, vgrm.date);
		GameContext.gameSocket.send(session, vgram);
	}

	public void getVipRoomRecord(VipRoomRecordMsg vrrm, IoSession session) {
		VipRoomRecordAckMsg vrram = new VipRoomRecordAckMsg();
		if (vrrm.clubCode != 0) {
			vrram.roomRecords = vipRoomRecordDAO.getQyqRoomRecord(null,
					vrrm.playerID, vrrm.gameID, vrrm.clubCode);
		} else {
			vrram.roomRecords = vipRoomRecordDAO.getMyVipRoomRecord(null,
					vrrm.playerID, vrrm.gameID);
		}
		GameContext.gameSocket.send(session, vrram);
	}

	// 获取俱乐部玩家战绩-lxw20180829
	public void getQyqRoomRecord(GetQyqWinnerListMsg vrrm, IoSession session) {
		GetQyqWinnerListMsgAck vrram = new GetQyqWinnerListMsgAck();

		List<VipRoomRecord> roomRecords = vipRoomRecordDAO.getAllVipRoomRecord(
				null, vrrm.clubID, vrrm.gameID);
		// 删除多余数据，每个房间保留一条数据
		String roomId = "";
		if (roomRecords == null) {
			roomRecords = new ArrayList<VipRoomRecord>();
		}
		for (int i = roomRecords.size() - 1; i >= 0; i--) {
			VipRoomRecord record = roomRecords.get(i);
			if (roomId.equals(record.getRoomID())) {
				roomRecords.remove(i);
			} else {
				roomId = record.getRoomID();
			}
		}
		vrram.roomRecords = roomRecords;
		vrram.opType = 0;
		GameContext.gameSocket.send(session, vrram);
	}

	//
	public void createVipRoomRecord(VipRoomRecord vrrm) {
		//
		vrrm.setHostName(filterEmojiUserName(vrrm.getHostName()));
		vrrm.setPlayer2Name(filterEmojiUserName(vrrm.getPlayer2Name()));
		vrrm.setPlayer3Name(filterEmojiUserName(vrrm.getPlayer3Name()));
		vrrm.setPlayer4Name(filterEmojiUserName(vrrm.getPlayer4Name()));
		vrrm.setEndWay(filterEmojiUserName(vrrm.getEndWay()));
		vipRoomRecordDAO.createVipRoomRecord(vrrm);
		//
	}

	// 更新战绩表的读取状态（大赢家用）-lxw20180828
	public void updateRecordReadState(List<String> recordIds, String gameID,
			IoSession session) {
		vipRoomRecordDAO.updateRecordReadState(recordIds, gameID);
		GetQyqWinnerListMsgAck vrram = new GetQyqWinnerListMsgAck();
		vrram.opType = 1;
		GameContext.gameSocket.send(session, vrram);
	}

	public void updateVipRoomRecord(VipRoomRecord vrrm) {
		vrrm.setHostName(filterEmojiUserName(vrrm.getHostName()));
		vrrm.setPlayer2Name(filterEmojiUserName(vrrm.getPlayer2Name()));
		vrrm.setPlayer3Name(filterEmojiUserName(vrrm.getPlayer3Name()));
		vrrm.setPlayer4Name(filterEmojiUserName(vrrm.getPlayer4Name()));
		vrrm.setPlayer5Name(filterEmojiUserName(vrrm.getPlayer5Name()));
		vrrm.setPlayer6Name(filterEmojiUserName(vrrm.getPlayer6Name()));
		vrrm.setEndWay(filterEmojiUserName(vrrm.getEndWay()));

		DBOperation omsg = new DBOperation();
		omsg.dao = DBConstant.VIP_ROOM_DAO;
		omsg.opertaion = DBConstant.VIP_ROOM_DAO_update_vip_room;
		omsg.vipRoomRecord = vrrm;

		dbThread.pushMsg(omsg);

		// vipRoomRecordDAO.updateRecord(vrrm);
	}

	//
	public void createVipGameRecord(VipGameRecord vgr) {

		vgr.setRecordID(UUIDGenerator.generatorUUID());

		DBOperation omsg = new DBOperation();
		omsg.dao = DBConstant.VIP_GAME_DAO;
		omsg.opertaion = DBConstant.VIP_GAME_DAO_create_vip_game;
		omsg.vipGameRecord = vgr;

		dbThread.pushMsg(omsg);

		// vipGameRecordDAO.createVipGameRecord(vgr);
	}

	//
	public List<UserFriend> GetPlayerFriendsByPlayerIndex(int playIndex) {
		return userFriendDAO.findPlayersByIndex(playIndex);
	}

	public List<UserFriend> GetPlayerFriendsByPlayerName(String playerName) {
		return userFriendDAO.findPlayersByName(playerName);
	}

	/**
	 * 是否存在此昵称用户
	 */
	public void validatePlayerName(String playerName, IoSession session) {
		// Player pl = userDAO.getPlayerByName(playerName);
		ValidateMsgAck ack = new ValidateMsgAck(
				MsgCmdConstant.GAME_VALIDATENAME_ACK);
		ack.result = 1;
		/*
		 * if (pl != null) { ack.result = 0;// 已经存在昵称 }
		 */
		GameContext.entranceServer.send(session, ack);
	}

	/**
	 * 修改密码*
	 */
	public void updatePlayerPassword(User pl, String oldpass, String newpass) {

		if (pl != null) {
			ValidateMsgAck ack = new ValidateMsgAck(
					MsgCmdConstant.GAME_USER_UPDATE_PASSWORD_ACK);
			if (!pl.getPassword().equals(oldpass)) {
				ack.result = 2;
				GameContext.gameSocket.send(pl.getSession(), ack);
				return;
			}

			DBOperation msg = new DBOperation();
			msg.dao = DBConstant.USER_DAO;
			msg.opertaion = DBConstant.USER_DAO_updatePlayerPassWorld;
			msg.playerID = pl.getPlayerID();
			msg.password = newpass;
			dbThread.pushMsg(msg);

			ack.optStr = newpass;
			ack.result = 1;
			ack.sex = pl.getSex();
			pl.setPassword(newpass);

			GameContext.gameSocket.send(pl.getSession(), ack);

		}
	}

	/**
	 * 是否可以添加为好友设置
	 */
	public void updatePlayerCanFriend(User pl, PlayerOpertaionMsg msg) {
		if (msg.canFriend > 1)
			return;

		if (pl != null) {
			ValidateMsgAck ack = new ValidateMsgAck(
					MsgCmdConstant.GAME_USER_UPDATE_CANFRIEND_ACK);

			DBOperation omsg = new DBOperation();
			omsg.dao = DBConstant.USER_DAO;
			omsg.opertaion = DBConstant.USER_DAO_updatePlayerCanFriend;
			omsg.playerID = pl.getPlayerID();
			omsg.canFriend = msg.canFriend;
			dbThread.pushMsg(omsg);

			ack.result = 1;
			ack.sex = pl.getSex();
			pl.setCanFriend(msg.canFriend);

			GameContext.gameSocket.send(pl.getSession(), ack);
		}
	}

	/**
	 * 更新玩家类别
	 */
	public void updatePlayerType(String playerID, int playerType) {
		DBOperation omsg = new DBOperation();
		omsg.dao = DBConstant.USER_DAO;
		omsg.opertaion = DBConstant.USER_DAO_updatePlayerType;
		omsg.playerID = playerID;
		omsg.playerType = playerType;
		dbThread.pushMsg(omsg);
	}

	/**
	 * 更新玩家位置信息
	 */
	public void updatePlayerCityName(String playerID, String cityName) {
		if (playerID == null || cityName == null) {
			return;
		}

		DBOperation omsg = new DBOperation();
		omsg.dao = DBConstant.USER_DAO;
		omsg.opertaion = DBConstant.USER_DAO_updatePlayerLocation;
		omsg.playerID = playerID;
		omsg.extStr = cityName;
		dbThread.pushMsg(omsg);
	}

	/**
	 * 更新玩家手机号码
	 */
	public void updatePlayerPhoneNumber(String playerID, String phoneNumber) {
		if (playerID == null || phoneNumber == null) {
			return;
		}

		// 更新玩家信息
		DBOperation updatePhone = new DBOperation();
		updatePhone.dao = DBConstant.USER_DAO;
		updatePhone.opertaion = DBConstant.USER_DAO_updatePlayerPhoneNumber;
		updatePhone.playerID = playerID;
		updatePhone.phoneNumber = phoneNumber;
		dbThread.pushMsg(updatePhone);
	}

	public void updatePlayerIdentifyCard(String playerID, String phoneNumber,
			String identifyCard, String weixinNum, String qqNum, String name,
			String birthday, String address) {
		if (playerID == null || phoneNumber == null) {
			return;
		}

		userDAO.updatePlayerIdentifyCard(playerID, phoneNumber, identifyCard,
				weixinNum, qqNum, name, birthday, address);
	}

	public void createNormalGameRecord(NormalGameRecord normalGameRecord) {
		//
		normalGameRecord.setRecordID(UUIDGenerator.generatorUUID());

		/*
		 * DBOperation omsg = new DBOperation(); omsg.dao =
		 * DBConstant.NORMAL_GAME_RECORD_DAO; omsg.opertaion =
		 * DBConstant.NORMAL_GAME_RECORD_DAO_create_normal_game_record;
		 * omsg.normalGameRecord = normalGameRecord;
		 * 
		 * dbThread.pushMsg(omsg);
		 */

		normalGameRecordDAO.createNormalGameRecord(normalGameRecord);
	}

	public List<NormalGameRecord> getNormalGameRecordByDate(String date,
			String player_index) {
		if (date.equals("")) {
			return null;
		}
		return normalGameRecordDAO
				.getNormalGameRecordByDate(date, player_index);
	}

	public INormalGameRecordDAO getNormalGameRecordDAO() {
		return normalGameRecordDAO;
	}

	public void setNormalGameRecordDAO(INormalGameRecordDAO normalGameRecordDAO) {
		this.normalGameRecordDAO = normalGameRecordDAO;
	}

	public void generateOrder(GameBuyItemMsg gbim, User player) {
		// 获取要购买的道具
		MallItem itemBase = mallItemDAO.getItemBaseByItemID(gbim.itemID);

		if ((null == itemBase)) {
			return;
		}

		// 生成订单号
		String outTradeNo = AliPayService.getOutTradeNo();

		int count = gbim.count;

		Date ct = DateService.getCurrentUtilDate();
		GameBuyItemAckMsg gbiam = new GameBuyItemAckMsg();
		gbiam.payType = gbim.payType;
		String orderInfo = "";

		if (!cfgService.get_PaySwitch()) {
			gbiam.otherstr = "支付开关已经关闭";
			GameContext.gameSocket.send(player.getSession(), gbiam);
			return;
		}

		MallHistory payHistory = new MallHistory();
		payHistory.setPlayerId(player.getPlayerID());
		payHistory.setAmount(itemBase.getPrice());
		payHistory.setOrderNo(outTradeNo);
		payHistory.setPayTime(new Date());
		payHistory.setFlagAccess(gbim.payType);
		payHistory.setState(0);
		// payHistory.setPayScript("");
		payHistory.setPayScript("{\"account\":\"" + player.getAccount() + "\","
				+ "\"description\":\"购买" + itemBase.getName() + ",花费"
				+ itemBase.getPrice() * count + "人民币\"}");
		payHistory.setBuyItemID(gbim.itemID);

		Map<String, String> map1 = new HashMap<String, String>();
		map1.put("gameID", gbim.gameID);
		map1.put("playerID", player.getPlayerID());
		if (GameContext.getPayMap() != null) {
			GameContext.setPayMapValue(outTradeNo, map1);
		}
		mallHistoryDAO.createPayHistory(payHistory, gbim.gameID);

		switch (gbim.payType) {
		// 支付宝支付订单
		case PayConstant.HISTORY_PLATFORM_TYPE_ALIPAY: {
			PayOperation msg = new PayOperation();
			msg.payPlatformType = gbim.payType;
			msg.gbiam = gbiam;
			msg.player = player;
			msg.outTradeNo = outTradeNo;
			msg.itemBase = itemBase;
			msg.itemCount = count;
			payThread.push(msg);
			break;
		}
			// 微信支付
		case PayConstant.HISTORY_PLATFORM_TYPE_WX_PAY: {
			PayOperation msg = new PayOperation();
			msg.payPlatformType = gbim.payType;
			msg.gbiam = gbiam;
			msg.player = player;
			msg.outTradeNo = outTradeNo;
			msg.itemBase = itemBase;
			msg.itemCount = count;
			payThread.push(msg);
			break;
		}

			// 一卡通支付
		case PayConstant.HISTORY_PLATFORM_TYPE_YI_KA_TONG:
			String cardNo = gbim.cardNo;
			String cardPwd = gbim.cardPwd;
			int money = itemBase.getPrice() * count;
			String response = sendYiKaTongHttpRequest(player, cardNo, cardPwd,
					money, outTradeNo);
			parseYiKaTongResponse(player, itemBase, count, payHistory, response);
			break;
		case PayConstant.HISTORY_PLATFORM_TYPE_IPA_PAY:
			gbiam.order = outTradeNo;
			gbiam.otherstr = GameConstant.IPA_PACKAGE_PREFIX
					+ itemBase.getBase_id();
			GameContext.gameSocket.send(player.getSession(), gbiam);
			break;
		default:
			break;
		}
	}

	// 第三方接口支付完成
	public void completePay(User pl, GameBuyItemCompleteMsg msg) {
		System.out.println(" shu  chu " + msg.payResult);
		Result result = new Result(msg.payResult);
		result.orderResultCallBack(pl, this);
	}

	/**
	 * iosIPA接口支付完成
	 */
	public void ipaCompletePay(User pl, GameIPABuyItemCompleteMsg msg) {
		PayOperation payMsg = new PayOperation();
		payMsg.payPlatformType = PayConstant.COMPLETE_TYPE_IPA_PAY;
		payMsg.completeIPAMsg = msg;
		payMsg.player = pl;
		ipaPayThread.push(payMsg);
	}

	public void completePayDeal(User pl, String orderno, boolean isok) {
		if (isok) {
			MallHistory history = mallHistoryDAO
					.getPayHistoryByOrderID(orderno);
			if (history.getState() != 1
					&& pl.getPlayerID().equals(history.getPlayerId())) {
				history.setState(1);
				mallHistoryDAO.updatePayHistory(history, "");
				this.buy_item_rmb(pl.getPlayerID(), history.getBuyItemID(), 1,
						GameConstant.OPT_BUY_ITEM, "");
			}
		}
	}

	/**
	 * 发送消息到一卡通支付接口
	 * 
	 * @param pl
	 *            玩家对象
	 * @param cardNo
	 *            一卡通卡号
	 * @param cardPwd
	 *            一卡通密码
	 * @param money
	 *            道具总金额
	 * @param outTradeNo
	 *            订单号
	 * @param platformType
	 *            一卡通平台类型
	 * @return 一卡通响应消息。
	 */
	private String sendYiKaTongHttpRequest(User pl, String cardNo,
			String cardPwd, int money, String outTradeNo) {

		// 一卡通商户ID
		SystemConfigPara scfPartnerID = cfgService
				.getPara(ConfigConstant.YI_KA_TONG_PARTNER_ID);
		String partnerID = scfPartnerID.getValueStr();
		// 一卡通平台地址
		SystemConfigPara scfPlatformAddress = null;
		String platformAddress = "";
		scfPlatformAddress = cfgService
				.getPara(ConfigConstant.YI_KA_TONG_PLATFORM);
		platformAddress = scfPlatformAddress.getValueStr();
		// 一卡通密钥
		SystemConfigPara scfKey = cfgService
				.getPara(ConfigConstant.YI_KA_TONG_KEY);
		String key = scfKey.getValueStr();

		// 加密
		StringBuffer unencryptedRequest = new StringBuffer();
		unencryptedRequest.append(partnerID).append(cardNo).append(cardPwd)
				.append(money).append(outTradeNo).append(1).append(key);
		String encrypt = SignUtil.MD5(unencryptedRequest.toString().getBytes());

		// 组合http消息
		StringBuffer request = new StringBuffer();

		request.append(platformAddress).append("?productid=").append(partnerID)
				.append("&cardno=").append(cardNo).append("&cardpassword=")
				.append(cardPwd).append("&money=").append(money)
				.append("&orderid=").append(outTradeNo).append("&username=")
				.append(pl.getAccount()).append("&codetype=1&sign=")
				.append(encrypt);

		String response = RequestHttpService.requestHttpGet(request.toString());

		return response;
	}

	private void parseYiKaTongResponse(User pl, MallItem itemBase, int count,
			MallHistory payHistory, String response) {

		PlayerGameOpertaionAckMsg pgoam = new PlayerGameOpertaionAckMsg();
		pgoam.result = ErrorCodeConstant.CMD_EXE_FAILED;
		pgoam.opertaionID = GameConstant.OPT_BUY_ITEM;
		pgoam.opValue = itemBase.getBase_id();
		pgoam.playerID = pl.getPlayerID();
		pgoam.playerIndex = pl.getPlayerIndex();
		pgoam.sex = pl.getSex();
		pgoam.gold = pl.getGold();
		pgoam.headImgUrl = pl.getHeadImgUrl();
		pgoam.location = pl.getLocation();
		if (response.equals("")) {
			GameContext.gameSocket.send(pl.getSession(), pgoam);
			return;
		}
		String temp[] = response.split(",");
		String code = temp[0];

		int iCode = Integer.valueOf(code);

		if (iCode > 0) {
			String encrypt = temp[1];
			SystemConfigPara scfKey = cfgService
					.getPara(ConfigConstant.YI_KA_TONG_KEY);
			String key = scfKey.getValueStr();
			StringBuffer responseBuf = new StringBuffer();
			String buf = responseBuf.append(code).append(key).toString();
			String encryptBuf = SignUtil.MD5(buf.toString().getBytes())
					.toLowerCase();

			// 验证消息是否匹配
			if (encryptBuf.equals(encrypt)) {
				if (payHistory.getState() == 0) {
					this.buy_item_rmb(pl.getPlayerID(), itemBase.getBase_id(),
							count, GameConstant.OPT_BUY_ITEM, "");
					payHistory.setState(1);
					// payHistory.setPayScript("{\"account\":\"" +
					// pl.getAccount() + "\"," + "\"description\":\"购买" +
					// itemBase.getName() + ",花费" + itemBase.getPrice() +
					// "人民币\"}" );
					mallHistoryDAO.updatePayHistory(payHistory, "");

				}
			} else {
				pgoam.result = ErrorCodeConstant.YIKATONG_MD5_ERROR;
				GameContext.gameSocket.send(pl.getSession(), pgoam);
			}

		} else {
			switch (iCode) {
			case ErrorCodeConstant.YIKATONG_CARDNO_ERROR:
				pgoam.result = ErrorCodeConstant.YIKATONG_CARDNO_ERROR;
				break;
			case ErrorCodeConstant.YIKATONG_CARDPWD_ERROR:
				pgoam.result = ErrorCodeConstant.YIKATONG_CARDPWD_ERROR;
				break;
			case ErrorCodeConstant.YIKATONG_CARD_DISABLE:
				pgoam.result = ErrorCodeConstant.YIKATONG_CARD_DISABLE;
				break;
			case ErrorCodeConstant.YIKATONG_CARD_MONEY_NOT_ENOUGH:
				pgoam.result = ErrorCodeConstant.YIKATONG_CARD_MONEY_NOT_ENOUGH;
				break;
			case ErrorCodeConstant.YIKATONG_CARD_NOT_SUPPORTED:
				pgoam.result = ErrorCodeConstant.YIKATONG_CARD_NOT_SUPPORTED;
				break;
			case ErrorCodeConstant.YIKATONG_OUTTRADENO_REPEAT:
				pgoam.result = ErrorCodeConstant.YIKATONG_OUTTRADENO_REPEAT;
				break;
			case ErrorCodeConstant.YIKATONG_PARA_ERROR:
				pgoam.result = ErrorCodeConstant.YIKATONG_PARA_ERROR;
				break;
			case ErrorCodeConstant.YIKATONG_PARTNER_ID_ERROR:
				pgoam.result = ErrorCodeConstant.YIKATONG_PARTNER_ID_ERROR;
				break;
			default:
				break;
			}
			GameContext.gameSocket.send(pl.getSession(), pgoam);
		}
	}

	/**
	 * 通知所有的好友更改金币信息
	 */
	public void refleshFrend(User pl) {

		FriendRefleshMsgAck msg = new FriendRefleshMsgAck();

		List<UserFriend> friends = userFriendDAO.getPlayerFriends(pl
				.getPlayerID());
		msg.friends = friends;

		if (msg.friends != null) {
			for (UserFriend friend : friends) {
				User frPlayer = playerMap.get(friend.playerID);
				if (frPlayer != null) {
					friend.isOnline = frPlayer.isOnline() ? 1 : 0;
				}
			}
			GameContext.gameSocket.send(pl.getSession(), msg);
		}
	}

	public IMallHistoryDAO getMallHistoryDAO() {
		return mallHistoryDAO;
	}

	public void setMallHistoryDAO(IMallHistoryDAO mallHistoryDAO) {
		this.mallHistoryDAO = mallHistoryDAO;
	}

	/**
	 * 消除卡桌玩家，清理玩家信息
	 */
	public void clearPlayerStaion(User pl) {
		if (null != pl) {
			tableLogic_nndzz.gameOverForce(pl);
		}
	}

	/**
	 * VIP房卡桌清理
	 * 
	 * @param vipTableID
	 */
	public void clearVipTableStation(String vipTableID, String gameId) {
		if (vipTableID != null && !"".equals(vipTableID.trim())) {
			tableLogic_nndzz.gameOverForceTable(vipTableID);
		}
	}

	public TableLogicProcess getTableLogicProcess() {
		return tableLogic_nndzz;
	}

	/**
	 * 获取游戏桌子信息
	 */
	public List<GameTable> getPlayingTable(int index) {
		return tableLogic_nndzz.getPlayingTables(index);
	}

	/**
	 * 强制结束一个桌子
	 */
	public void clearOneTable(String table_id) {
		tableLogic_nndzz.gameOverForceTable(table_id);
	}

	public List<ScrollMsg> getOnTimeScrollMsgs() {
		return onTimeScrollMsgs;
	}

	public void setOnTimeScrollMsgs(List<ScrollMsg> onTimeScrollMsgs) {
		this.onTimeScrollMsgs = onTimeScrollMsgs;
	}

	/**
	 * 重置密码
	 */
	public void resetPlayerPassword(User pl, String newPassword) {
		if (pl == null) {
			return;
		}

		String md5Password = MD5Service.encryptString(newPassword);

		DBOperation msg = new DBOperation();
		msg.dao = DBConstant.USER_DAO;
		msg.opertaion = DBConstant.USER_DAO_updatePlayerPassWorld;
		msg.playerID = pl.getPlayerID();
		msg.password = md5Password;
		dbThread.pushMsg(msg);

		pl.setPassword(md5Password);
	}

	/**
	 * 发送请求获取手机注册验证码
	 */
	public void sendMobileCodeRequest(MobileCodeMsg mcm, IoSession session) {
		// 获取消息参数
		String phoneNO = mcm.phoneNO;

		// 获取验证码时间间隔
		int limitedTime = 60;
		MobileCodeMsgAck mcma = new MobileCodeMsgAck();
		mcma.result = ErrorCodeConstant.CMD_EXE_FAILED;
		mcma.operation = mcm.operation;
		mcma.time = limitedTime;

		// 手机号格式正则表达式
		String phoneRegExp = "^(0|86|17951)?(13[0-9]|15[012356789]|17[0-9]|18[0-9]|14[57])[0-9]{8}$";
		Pattern phonePat = Pattern.compile(phoneRegExp);
		Matcher phoneMat = phonePat.matcher(phoneNO);
		// 验证手机号格式是否正确
		if (!phoneMat.matches()) {
			mcma.resultStr = "手机号码不正确";
			GameContext.gameSocket.send(session, mcma);
			return;
		}

		// 获取客户端IP
		String clientIP = "";
		String ip = "" + session.getRemoteAddress();
		ip = ip.substring(1);
		String[] ips = ip.split(":");
		if (ips != null && ips.length == 2) {
			clientIP = ips[0];
		}

		// 判断是否有人使用了此手机号
		int phoneCount = userDAO.getPhoneNumberCount(phoneNO);
		User pl = null;

		if (mcm.operation == GameConstant.GET_CODE_COMPLETE_PHONE_NUMBER) {
			// 绑定手机号码
			if (phoneCount > 5) {
				mcma.resultStr = "此手机号已超出绑定数量";
				GameContext.gameSocket.send(session, mcma);
				return;
			}
		} else if (mcm.operation == GameConstant.GET_CODE_FIND_PASS_WORD) {
			// 找回密码
			pl = playerAccountMap.get(mcm.account.toLowerCase());
			if (pl == null) {
				pl = userDAO.getPlayerByAccount(mcm.account);
				if (pl == null) {
					mcma.resultStr = "找不到此帐号";
					GameContext.gameSocket.send(session, mcma);
					return;
				}
			}

			if (pl.getPhoneNumber() == null
					|| !pl.getPhoneNumber().equals(mcm.phoneNO)) {
				mcma.resultStr = "帐号和手机号码不匹配";
				GameContext.gameSocket.send(session, mcma);
				return;
			}
		}

		if (limitedPhoneLists.containsKey(phoneNO)) {
			int phoneAndIpSameTimes = 5; // cfgService.getPara(GameConfigConstant.LIMIT_SAME_IP_AND_PHONE_TIME).getValueInt();
			MobileCodeLimitedPhone phone = limitedPhoneLists.get(phoneNO);
			if (phone.getPhoneAndIpSameTimes() > phoneAndIpSameTimes) {
				// 今天相同IP相同手机号，限制获取验证码
				mcma.resultStr = "同一手机号获取次数过多";
				GameContext.gameSocket.send(session, mcma);
				return;
			} else {
				int limtedPhoneTime = 5; // cfgService.getPara(GameConfigConstant.LIMIT_DIFF_IP_SAME_PHONE_TIME).getValueInt();
				if (phone.getLimtedPhoneTime() > limtedPhoneTime) {
					// 今天此手机号，限制获取验证码
					mcma.resultStr = "同一手机号获取次数过多";
					GameContext.gameSocket.send(session, mcma);
					return;
				} else {
					if ((new Date().getTime() - phone.getLastRegTime()
							.getTime()) < (limitedTime * 1000)) {
						mcma.resultStr = "获取验证码次数过于频繁";
						GameContext.gameSocket.send(session, mcma);
						return;
					} else {
						// 更新限制列表
						phone.setLastRegTime(new Date());
						phone.setLimtedPhoneTime(phone.getLimtedPhoneTime() + 1);
						if (phone.getIp().equals(clientIP)) {
							phone.setPhoneAndIpSameTimes(phone
									.getPhoneAndIpSameTimes() + 1);
						}
					}
				}
			}
		} else {
			// 将此手机号加入到限制列表
			MobileCodeLimitedPhone mcLimitedPhone = new MobileCodeLimitedPhone();
			mcLimitedPhone.setPhoneNO(phoneNO);
			mcLimitedPhone.setIp(clientIP);
			mcLimitedPhone.setPhoneAndIpSameTimes(1);
			mcLimitedPhone.setLimtedPhoneTime(1);
			mcLimitedPhone.setLastRegTime(new Date());
			limitedPhoneLists.put(phoneNO, mcLimitedPhone);
		}

		cfgService = (ISystemConfigService) SpringService
				.getBean("sysConfigService");
		// 短信提交平台地址
		String msgSubmitAddr = cfgService.getPara(
				ConfigConstant.MSG_VERTIFY_SUBMIT_ADDRESS).getValueStr();
		HttpClient client = new HttpClient();
		PostMethod method = new PostMethod(msgSubmitAddr);
		client.getParams().setContentCharset("UTF-8");
		method.setRequestHeader("ContentType",
				"application/x-www-form-urlencoded;charset=UTF-8");

		// 生成验证码
		int mobile_code = (int) ((Math.random() * 9 + 1) * 100000);
		logger.debug("### Mobile code:" + mobile_code);// System.out.println("### Mobile code:"
														// + mobile_code);

		String content = null;
		if (mcm.operation == GameConstant.GET_CODE_FIND_PASS_WORD) {
			content = new String("您的新密码是：" + mobile_code + "。请尽快登录游戏，修改密码。");
		} else if (mcm.operation == GameConstant.GET_CODE_COMPLETE_PHONE_NUMBER) {
			content = new String("您的验证码是：" + mobile_code + "。请不要把验证码泄露给其他人。");
		} else if (mcm.operation == GameConstant.GET_CODE_REG_VALIDATE) {
			content = new String("您的验证码是：" + mobile_code + "。请不要把验证码泄露给其他人。");
		}
		// 互亿短信验证帐号
		String msgAccount = cfgService.getPara(
				ConfigConstant.MSG_VERTIFY_ACCOUNT).getValueStr();
		// 互亿短信验证密码
		String msgPwd = cfgService.getPara(ConfigConstant.MSG_VERTIFY_PWD)
				.getValueStr();
		// 提交短信
		NameValuePair[] data = {
				new NameValuePair("account", msgAccount),
				new NameValuePair("password", msgPwd), // 密码可以使用明文密码或使用32位MD5加密
				// new
				// NameValuePair("password",util.StringUtil.MD5Encode("密码")),
				new NameValuePair("mobile", phoneNO),
				new NameValuePair("content", content), };
		method.setRequestBody(data);
		try {
			client.executeMethod(method);
			String SubmitResult = method.getResponseBodyAsString();
			// System.out.println(SubmitResult);
			Document doc = DocumentHelper.parseText(SubmitResult);
			Element root = doc.getRootElement();

			String code = root.elementText("code");
			String msg = root.elementText("msg");
			String smsid = root.elementText("smsid");

			// if (code.equals("2") || code.equals("4082")) {
			if (code.equals("2")) {
				// 将验证码存于session
				session.setAttribute("mobileCodeType", mcm.operation);
				session.setAttribute("mobileCode", mobile_code);
				session.setAttribute("phoneNumber", phoneNO);

				mcma.result = 1;
				mcma.resultStr = "获取验证码成功";

				if (mcm.operation == GameConstant.GET_CODE_FIND_PASS_WORD) {
					// 重置玩家密码
					String newPassword = "" + mobile_code;
					this.resetPlayerPassword(pl, newPassword);
					mcma.resultStr = "新密码已发送到您的手机，请及时查看";
				}

				GameContext.gameSocket.send(session, mcma);
			} else {
				mcma.resultStr = "获取验证码失败:" + code;
				GameContext.gameSocket.send(session, mcma);
				logger.error("### Msg sent failed...Error code:" + code + " - "
						+ msg + " - " + smsid);
			}
		} catch (Exception e) {
			e.printStackTrace();
			limitedPhoneLists.remove(phoneNO);

			session.setAttribute("mobileCodeType", mcm.operation);
			session.setAttribute("mobileCode", mobile_code);
			session.setAttribute("phoneNumber", phoneNO);

			mcma.resultStr = "获取验证码失败";
			GameContext.gameSocket.send(session, mcma);
		}

	}

	/**
	 * 清除限制注册手机号列表
	 */
	public void clearLimitedPhone() {

		limitedPhoneLists.clear();

	}

	/**
	 * 绑定手机号码
	 */
	public boolean completeMoblieNumber(User pl, RequestCompletePhoneNumber msg) {
		if (pl == null || msg == null) {
			return false;
		}
		boolean result = false;
		RequestCompletePhoneNumberAck msgAck = new RequestCompletePhoneNumberAck();

		msgAck.result = -1;
		msgAck.phoneNumber = msg.phoneNumber;

		IoSession session = pl.getSession();
		if (session == null)
			return result;

		String phone_number = (String) session.getAttribute("phoneNumber");
		Integer mobile_code = (Integer) session.getAttribute("mobileCode");
		Integer request_type = (Integer) session.getAttribute("mobileCodeType");

		if (mobile_code == null || phone_number == null || request_type == null) {
			msgAck.resultStr = "获取手机号或者验证码错误";
			result = false;
		} else if (request_type != GameConstant.GET_CODE_COMPLETE_PHONE_NUMBER) {
			msgAck.resultStr = "获取验证码类型不对";
		} else if (request_type == GameConstant.GET_CODE_REG_VALIDATE) {
			msgAck.result = 1;
			msgAck.resultStr = "验证成功！";
			GameContext.gameSocket.send(session, msgAck);
			return true;
		} else if (!phone_number.equals(msg.phoneNumber)) {
			msgAck.resultStr = "手机号码和获取验证码的手机号不匹配";
		} else if (mobile_code == Integer.parseInt(msg.comfirmCode)) {
			msgAck.result = 1;
			msgAck.resultStr = "绑定手机成功";
			pl.setPhoneNumber(msg.phoneNumber);

			this.updatePlayerPhoneNumber(pl.getPlayerID(), pl.getPhoneNumber());
			result = true;
		} else {
			msgAck.resultStr = "验证码错误";
			return false;
		}

		GameContext.gameSocket.send(session, msgAck);
		return result;

	}

	/**
	 * 更新系统公告
	 */
	public void updaeSystemNotice(SystemNotice notice) {
		if (notice == null)
			return;
		if (0 == notice.getNotice_type()) {
			this.setSysNotice(notice);
		} else if (5 == notice.getNotice_type()) {
			this.setQyqNotice(notice);
		}

		systemNoticeDAO.updateSystemNotice(notice);
	}

	public SystemNotice getSysNotice() {
		return sysNotice;
	}

	public void setSysNotice(SystemNotice sysNotice) {
		this.sysNotice = sysNotice;
	}

	// 更新固定跑马灯
	public void updateForeverNotice(SystemNotice foreverNotice) {
		this.setForeverNotice(foreverNotice);

		systemNoticeDAO.updateSystemNotice(foreverNotice);
	}

	public SystemNotice getForeverNotice() {
		return foreverNotice;
	}

	public void setForeverNotice(SystemNotice foreverNotice) {
		this.foreverNotice = foreverNotice;
	}

	public ISystemNoticeDAO getSystemNoticeDAO() {
		return systemNoticeDAO;
	}

	public void setSystemNoticeDAO(ISystemNoticeDAO systemNoticeDAO) {
		this.systemNoticeDAO = systemNoticeDAO;
	}

	public SystemNotice getQyqNotice() {
		return qyqNotice;
	}

	public void setQyqNotice(SystemNotice qyqNotice) {
		this.qyqNotice = qyqNotice;
	}

	/**
	 * 网页请求扣除玩家金币
	 */
	public boolean requestSubPlayerGoldByWeb(int playerIndex, int subGold) {
		User plTmp = this.getPlayerByPlayerIndex(playerIndex);

		if (plTmp == null) {
			logger.debug("网页消费找不到玩家：" + playerIndex);// System.out.println("网页消费找不到玩家："
														// + playerIndex);
			return false;
		}

		User pl = this.getPlayerByPlayerID(plTmp.getPlayerID());
		if (pl == null) {
			logger.debug("网页消费找不到玩家2：" + playerIndex);// System.out.println("网页消费找不到玩家2："
														// + playerIndex);
			return false;
		}

		if (pl.getGold() < subGold) {
			logger.debug("网页消费扣除玩家金币：" + subGold + ",实际拥有金币：" + pl.getGold());// logger.error("网页消费扣除玩家金币："
																				// +
																				// subGold
																				// +
																				// ",实际拥有金币："
																				// +
																				// pl.getGold());
			return false;
		}

		String remark = "网页消费扣除玩家金币：" + subGold;

		this.sub_player_gold(pl, subGold,
				LogConstant.OPERATION_TYPE_WEB_REQUEST_SUB_GOLD, remark);

		return true;
	}

	// 玩家申请解散房间
	public void userCloseVipRoom(PlayerAskCloseVipRoomMsg msg, User pl) {
		tableLogic_nndzz.player_close_vip_room(msg, pl);
	}

	public List<PlayerHuType> getPlayerHuTypesByPlayerID(String playerID) {
		return playerHuTypeDAO.getPlayerHuTypeListByPlayerID(playerID);
	}

	public void initAsyncThread() {
		this.initDataBaseThread();
		this.initPayThread();
	}

	public IPlayerHuTypeDAO getPlayerHuTypeDAO() {
		return playerHuTypeDAO;
	}

	public void setPlayerHuTypeDAO(IPlayerHuTypeDAO playerHuTypeDAO) {
		this.playerHuTypeDAO = playerHuTypeDAO;
	}

	/**
	 * 开局准备
	 * 
	 * @author  2017-2-8
	 * @param msg
	 * @param pl
	 */
	public void gameReady(GameReadyMsg msg, IoSession session) {
//		User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);
//		if (pl == null) {
//			logger.error("---------->开局准备，没找到玩家");
//			return;
//		}
//		logger.error("---------->开局准备gameReday，玩家ID【" + pl.getPlayerIndex()
//				+ "】, 玩家昵称【" + pl.getPlayerName() + "】, gameID=" + msg.game_id
//				+ ", roomID=" + msg.room_id + ", tableID=" + msg.table_id
//				+ ", playerID=" + msg.player_id + ", 是否准备【" + msg.opt_id + "】");
//		pl.setReadyFlag(msg.opt_id);
//		tableLogic_nndzz.gameReday(msg, pl);
	}
	
	/**
	 * 开局准备-nndzz
	 */
	public void gameReadyNNDZZ(com.chess.nndzz.msg.struct.other.GameReadyMsg msg,
			IoSession session) {
		User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);
		if (pl == null) {
			logger.error("------nndzz---->开局准备，没找到玩家");
			return;
		}
		logger.error("-----nndzz----->开局准备gameReday-->开始游戏，玩家ID【"
				+ pl.getPlayerIndex() + "】, 玩家昵称【" + pl.getPlayerName()
				+ "】, gameID=" + msg.game_id + ", roomID=" + msg.room_id
				+ ", tableID=" + msg.table_id + ", playerID=" + msg.player_id
				+ ", 是否准备【" + msg.opt_id + "】");
		pl.setReadyFlag(msg.opt_id);
		tableLogic_nndzz.gameStart(msg, pl);
	}

	/**
	 * 赠卡日志
	 * 
	 * @param msg
	 * @param pl
	 */
	public void createSendCardLog(User user, int cardId, int cardNum,
			int cardType) {
		SendCardLog SCLog = new SendCardLog();
		SCLog.setSendCardPlayerId(user.getPlayerID());
		SCLog.setSendCardNum(cardNum);
		SCLog.setSendCardType(cardType);
		SCLog.setSendCardDate(new Date());
		SCLog.setSendCardCardId(cardId);

		playerLogDAO.insertSendCard(SCLog);
	}

	public void endVipRoomRecord(VipRoomRecord vrrm) {

		DBOperation omsg = new DBOperation();
		omsg.dao = DBConstant.VIP_ROOM_DAO;
		omsg.opertaion = DBConstant.VIP_ROOM_DAO_end_vip_room;
		omsg.vipRoomRecord = vrrm;

		dbThread.pushMsg(omsg);
	}

	// 查询某种房卡的基础表暑假

	public MallItem getFangkaBaseItemddz(int quanNum, User pl) {
		int baseID = 3000;
		if (1 == pl.getDeviceFlag()) {
			baseID = 3200; // IOS的房卡ID从3201开始
		}

		// 改成找有的房卡
		int baseItemId = -1;
		// 循环玩家的所有道具
		List<UserBackpack> items = pl.getItems();
		for (int i = 0; i < items.size(); i++) {
			UserBackpack pi = items.get(i);

			// 判断是房卡
			if (pi.getItemBaseID() > baseID && pi.getItemBaseID() < 4000) {
				// 判断有
				if (pi.getItemNum() > 0) {
					baseItemId = pi.getItemBaseID();
					break;
				}
			}
		}
		if (baseItemId > 0) {
			for (int i = 0; i < baseItemList.size(); i++) {
				MallItem it = baseItemList.get(i);
				if (it.getBase_id() == baseItemId) {
					return it;
				}
			}
		}

		//
		return null;
	}

	private String filterEmojiUserName(String userName) {
		if (userName == null || userName.equals("")) {
			return "";
		}
		Pattern emoji = Pattern
				.compile(
						"[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\ud83e\udd00-\ud83e\udfff]|[\u2600-\u27ff]",
						Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
		Matcher emojiMatcher = emoji.matcher(userName);
		userName = emojiMatcher.replaceAll("");
		if (userName.equals("")) {
			userName = "微信用户";
		}
		if (userName.length() > 12) {
			userName = userName.substring(0, 11);
		}
		return userName;
	}

	public void setShopGoodsDAO(IShopGoodsDAO shopGoodsDAO) {
		this.shopGoodsDAO = shopGoodsDAO;
	}

	public IShopGoodsDAO getShopGoodsDAO() {
		return shopGoodsDAO;
	}

	public List<ShopGoods> ShopShowGoods() {

		try {
			shopGoods = shopGoodsDAO.getShowShopGoods();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return shopGoods;
	}

	public void UpdateShowGoods(ShopGoods sg) {
		// 更新缓存；
		shopGoodsDAO.UpdateShowGoods(sg);
		// if (shopGoods.size() <0 ) {
		// return ;
		// }
		//
		// for(int i= 0; i <shopGoods.size() ;i++){
		// ShopGoods good = shopGoods.get(i);
		// if (good.getGoodsId() == sg.getGoodsId()){
		// try {
		// good = shopGoodsDAO.unZipPicture(sg);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// shopGoods.set(i, sg);

		// good = sg;
		// }
		// }

	}

	public void UpdateShowGoodsExceptPicture(ShopGoods sg) {
		// 更新缓存；
		for (int i = 0; i < shopGoods.size(); i++) {
			ShopGoods good = shopGoods.get(i);
			if (good.getGoodsId() == sg.getGoodsId()) {
				shopGoods.get(i).setGoodsName(sg.getGoodsName());
				shopGoods.get(i).setGoodClass(sg.getGoodClass());
				shopGoods.get(i).setGoodsRemark(sg.getGoodsRemark());
				shopGoods.get(i).setExpenseScore(sg.getExpenseScore());
				shopGoods.get(i).setOpenState(sg.getOpenState());
			}
		}
		shopGoodsDAO.UpdateShowGoodsExceptPicture(sg);
	}

	public void insertShopGood(ShopGoods sg) {
		shopGoodsDAO.insertShopGood(sg);
	}

	public void updateGoodState(String goodId, int state) {
		// for(int i= 0; i <shopGoods.size() ;i++){
		// ShopGoods good = shopGoods.get(i);
		// if (good.getGoodsId() == goodId){
		// shopGoods.get(i).setOpenState(state);
		// }
		// }
		shopGoodsDAO.updateGoodState(goodId, state);
	}

	// 修改玩家积分以及信用积分
	public void addPlayerCredits(String playerID, int credits) {
		User user = userDAO.getPlayerByID(playerID);
		int creditsResult = user.getCredits();
		creditsResult = creditsResult + credits;
		int totalCredits = user.getTotalCredits();
		totalCredits = totalCredits + credits;
		user.setCredits(creditsResult);
		user.setTotalCredits(totalCredits);
		userDAO.addPlayerCredits(playerID, creditsResult, totalCredits);
		if (this.playerMap.get(playerID) != null) {
			User pl = this.playerMap.get(playerID);
			pl.setCredits(creditsResult);
			pl.setTotalCredits(totalCredits);
		}
	}

	// 修改玩家积分以及信用积分
	public Boolean costPlayerCredits(String playerID, int credits) {
		User user = userDAO.getPlayerByID(playerID);
		int creditsResult = user.getCredits();
		if (creditsResult >= credits) {

			creditsResult = creditsResult - credits;
			user.setCredits(creditsResult);
			userDAO.costPlayerCredits(playerID, creditsResult);

			if (this.playerMap.get(playerID) != null) {
				User pl = this.playerMap.get(playerID);
				pl.setCredits(creditsResult);
			}
			return true;
		}

		return false;
	}

	/****************** nndzz ************************/

	/**
	 * 创建房间nndzz
	 */
	public void createVipRoom(
			com.chess.nndzz.msg.struct.other.CreateVipRoomMsg msg, User pl) {

		if (msg.roomID == 0) {
			return;
		}

		// 根据规则，确定玩家人数
		List<Integer> tableRule = msg.tableRule;

		pl.setGameId(msg.gameID);

		// 验证福豆
		int kaKouNum = 0;
		GameConfig config = GameContext.tableLogic_nndzz.getGameConfig();
		if(msg.clubCode > 0){
			// 场次级别
			String level = msg.roomLevel;
			Map<String, Integer> mapKa = config.getPokerClubWanfa(msg.quanNum);
			logger.info("==========>俱乐部扣卡配置："+mapKa);
			kaKouNum = mapKa.get("cost_self_"+level);
			if (msg.isProxy == 2) {
				kaKouNum = mapKa.get("cost_aa_"+level);
			} else if (msg.isProxy == 1) {
				kaKouNum = mapKa.get("cost_proxy_"+level);
			}
		}else{
			Map<String, Integer> mapKa = config.getPokerVipWanfa(msg.quanNum);
			logger.info("==========>VIP房扣卡配置："+mapKa);
			kaKouNum = mapKa.get("cost_self");
			if (msg.isProxy == 2) {
				kaKouNum = mapKa.get("cost_aa");
			} else if (msg.isProxy == 1) {
				kaKouNum = mapKa.get("cost_proxy");
			}
		}
		
		// 代开优化 cc modify 2017-9-26
		int kakouNumProxy = kaKouNum;
		boolean isCardLock = false;
		if (null != pl.getPvrrs() && pl.getPvrrs().size() > 0) {
			for (ProxyVipRoomRecord pvrr : pl.getPvrrs()) {
				if (pvrr.getVipState() == GameConstant.PROXY_NO_START_GAME) {
					kakouNumProxy += pvrr.getProxyCardCount();
					isCardLock = true;
				}
			}
		}
		
		// 验证扣卡
		if(msg.clubCode > 0){
			TClub club = tClubDao.getClubByClubCode(msg.clubCode);
			if(club.getFangkaNum() < kaKouNum){
				com.chess.common.msg.struct.other.RequestStartGameMsgAck ack = new com.chess.common.msg.struct.other.RequestStartGameMsgAck();
				ack.gold = pl.getGold();
				ack.clubCode = msg.clubCode;
				// 代开优化 cc modify 2017-9-26
				if (isCardLock) {
					ack.result = ErrorCodeConstant.FANGKIA_LOCKED;
				} else {
					ack.result = ErrorCodeConstant.FANGKIA_NOT_FOUND;
				}
				GameContext.gameSocket.send(pl.getSession(), ack);
				return;
			}
		}else{
			if (commonPreuse_fanka(pl, kakouNumProxy, isCardLock) == false) {
				return;
			}
		}
		pl.setGameId(msg.gameID);

		logger.info("==>【nndzz】创建VIP房，玩家ID【" + pl.getPlayerIndex() + "】，玩家昵称【"
				+ pl.getPlayerName() + "】，俱乐部ID【"+msg.clubCode+"】，场次级别【"
				+msg.roomLevel+"】，圈数【"+msg.quanNum+"】，扣卡【"+kaKouNum
				+"】，玩家数量【"+msg.userNum+"】，扣卡方式【"+msg.isProxy+"】");
		tableLogic_nndzz.create_vip_table(pl, msg.psw, msg.quanNum, msg.roomID,
				3333, tableRule, msg.isRecord, msg.isProxy, msg.isSelectSite,
				msg.gameID, msg.userNum, kaKouNum, msg.clubCode, msg.roomLevel);
	}

	/**
	 * 加入房间=nndzz
	 */
	public void enterVipRoom(
			com.chess.nndzz.msg.struct.other.EnterVipRoomMsg msg, User pl) {
		boolean enterOld = enterVipRoom(msg.roomID, pl, msg.clubCode, msg.tableID);
		if (enterOld) {
			return;
		}
		
		// 验证俱乐部库存是否充足 
		if (msg.clubCode > 0) {
			TClub club = tClubDao.getClubByClubCode(msg.clubCode);
			if (club != null) {
				GameTable gt = tableLogic_nndzz.getGameTableByTableId(msg.tableID);
				int kaKouNum = gt.getCostKaNum();
				if (gt.getCostKaNum() > club.getFangkaNum()) {
					// 库存不足
					EnterVipRoomMsgAck ack = new EnterVipRoomMsgAck();
					ack.result = 2;
					ack.currKcNum = club.getFangkaNum();
					GameContext.gameSocket.send(pl.getSession(), ack);
					return;
				}
				
				logger.info("==>【nndzz】进入VIP房，玩家ID【" + pl.getPlayerIndex() + "】，玩家昵称【"
						+ pl.getPlayerName() + "】，房间ID【"+gt.getVipTableID()+"】，俱乐部ID【"+msg.clubCode+"】，场次级别【"+gt.getRoomLevel()
						+"】，圈数【"+gt.getRoomModel()+"】，扣卡【"+kaKouNum+"】，玩家数量【"+gt.getPlayerCount()+"】");
			}
		}
		
		// pl.setVipTableID(msg.tableID);
		pl.setTableID(msg.tableID);
		pl.setRoomID(msg.roomID);
		pl.setNeedCopyGameState(true);

		logger.info("【电子庄牛牛】====>enterVipRoom：tableID="+msg.tableID+", roomID="+msg.roomID+", clubCode="+msg.clubCode);
		
		tableLogic_nndzz.enterVipRoom(msg, pl);
	}

	/**
	 * 开始游戏=nndzz
	 */
	public void enter_room(
			com.chess.nndzz.msg.struct.other.RequestStartGameMsg msg,
			IoSession session) {
		User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);
		if (pl == null)
			return;

		if (msg.roomID == -1) {
			// 房间ID为-1表示断线重连消息
			this.reEnterRoom_define(pl);
			return;
		}

		if (!isSameReLoginIp(pl, session))
			return;

		// 检测是否需要救济金
		// check_save(pl);
		//
		tableLogic_nndzz.enter_room(pl, msg.roomID);

	}

	/**
	 * 重新进入房间-nndzz
	 * 
	 * @author  2017-11-30
	 * @param pl
	 * @return
	 */
	public boolean reEnterRoom_nndzz(User pl) {
		List<com.chess.nndzz.table.GameTable> oldTables = this.tableLogic_nndzz
				.getPlayingTables(pl.getPlayerIndex());
		for (int i = 0; i < oldTables.size(); i++) {
			com.chess.nndzz.table.GameTable gt = oldTables.get(i);
			List<User> gtPlayers = gt.getPlayers();
			for (int j = 0; j < gtPlayers.size(); j++) {
				if (gtPlayers.get(j).getPlayerID().equals(pl.getPlayerID())) {
//					// 创始人固定牌桌不能恢复
//					int clubCode = gt.getClubCode();
//					if(clubCode > 0){
//						TClub club = tClubDao.getClubByClubCode(clubCode);
//						if(!pl.getPlayerID().equals(club.getCreatePlayerId()) 
//								&& gt.getClubTableState() == 1
//								&& gtPlayers.size() == 1){
//							// 有房间；还需判断玩家是否为代开者，如果是代开者，那么房间内超过两人，则为普通房间，即不能再代开，不能再开房间
//							if (gt.getProxyCreator() == null
//									|| (gt.getProxyCreator() != null && gtPlayers
//											.size() > 1)
//									|| (gt.getProxyCreator() != null
//											&& gtPlayers.size() == 1 && !gtPlayers
//											.get(0).getPlayerID()
//											.equals(gt.getProxyCreator().getPlayerID()))) {
//
//								// 在其他桌子中不能进入，直接进入老桌子
//								pl.setGameId("nndzz");
//								this.tableLogic_nndzz.enter_room(pl, oldTables.get(0)
//										.getRoomID());
//								return true;
//							}
//						}
//					}
					
					// 有房间；还需判断玩家是否为代开者，如果是代开者，那么房间内超过两人，则为普通房间，即不能再代开，不能再开房间
					if (gt.isStartGame() || gt.getProxyCreator() == null
							|| (gt.getProxyCreator() != null && gtPlayers
									.size() > 1)
							|| (gt.getProxyCreator() != null
									&& gtPlayers.size() == 1 && !gtPlayers
									.get(0).getPlayerID()
									.equals(gt.getProxyCreator().getPlayerID()))) {
						// 在其他桌子中不能进入，直接进入老桌子
						pl.setGameId("nndzz");
						this.tableLogic_nndzz.enter_room(pl, oldTables.get(0)
								.getRoomID());
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * 玩家操作=nndzz
	 */
	public void playerOperation(
			com.chess.nndzz.msg.struct.other.PlayerTableOperationMsg msg, User pl) {
		this.tableLogic_nndzz.playerOperation(msg, pl);
	}

	/**
	 * 换座 nndzz
	 */
	public void changeSiteNNDZZ(
			com.chess.nndzz.msg.struct.other.GameSiteDownMsg msg,
			IoSession session) {
		User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);
		this.tableLogic_nndzz.change_site(msg, pl);
	}
	
	/**
	 * 互动表情
	 */
	public void playerSendNNDZZHuDongBiaoQing(User pl, HuDongBiaoQingMsgNNDZZ msg) {
		tableLogic_nndzz.playerSendNNQZHuDongBiaoQing(pl, msg);
	}

	/********************* 集成 **********************************/
	/**
	 * 多产品-重新进入房间
	 * 
	 * @author  2017-7-12
	 * @param pl
	 */
	public void reEnterRoom_define(User pl) {
		// nndzz
		boolean reEnter_nndzz = reEnterRoom_nndzz(pl);
		if (reEnter_nndzz) {
			return;
		}
	}

	/**
	 * 加入房间=集成
	 */
	public boolean enterVipRoom(int roomId, User pl, int clubCode, String tableId) {
		// nndzz
		List<com.chess.nndzz.table.GameTable> oldTables_nndzz = this.tableLogic_nndzz
				.getPlayingTables(pl.getPlayerIndex());
		if (oldTables_nndzz != null && oldTables_nndzz.size() >= 1) {
//			if(tableId != null && !"".equals(tableId)){
//				for(com.chess.nndzz.table.GameTable gt : oldTables_nndzz){
//					if(gt.getTableID().equals(tableId)){
//						
//						GameRoom rm = gt.game_room;
//						// tieguo
//						pl.setPlayerType(rm.roomType);
//						updatePlayerType(pl.getPlayerID(), rm.roomType);
//						//
//						pl.setTableID(gt.getTableID());
//						pl.setRoomID(rm.roomID);
//						
//						//
//						pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
//						// pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE);
//
//						// 游戏房间人数加1
//						rm.setPlayerNum(rm.getPlayerNum() + 1);
//						
//						GameContext.tableLogic_nndzz.enter_table_done(pl, gt, false);
//						
//						return true;
//					}
//				}
//			}else{
				int hasOldTable = 0;
				hasOldTable = isHasTable(pl, oldTables_nndzz);
				if (hasOldTable == 1) {
					this.tableLogic_nndzz.enter_room(pl, roomId);
					return true;
				}
//			}
		}
		return false;
	}

	public void welFare(User pl, WelFareMsg msg) {

		WelFare welFare = new WelFare();

		List<WelFare> firstGl = welFareDAO.getFirstGl();
		int dGl = 0;
		int xGl = 0;
		int otherGl = 0;
		for (WelFare wf : firstGl) {
			if (wf.part_no == GameConstant.WELFARE_PART_NO_DAER) {
				dGl = wf.getWin_percent_1();
			} else if (wf.part_no == GameConstant.WELFARE_PART_NO_XIAOER) {
				xGl = wf.getWin_percent_1();
			} else if (wf.part_no == GameConstant.WELFARE_PART_NO_OTHER) {
				otherGl = wf.getWin_percent_1();
			}
		}

		int part_no = 0;
		// 第一次取随机数为了 判断是大额还是小额等
		Random rd = new Random();
		int d = rd.nextInt(100) + 1;
		if (d >= (100 - dGl)) {
			part_no = GameConstant.WELFARE_PART_NO_DAER;
		} else if (d < (100 - dGl) && d >= xGl) {
			part_no = GameConstant.WELFARE_PART_NO_OTHER;
		} else if (d < xGl) {
			part_no = GameConstant.WELFARE_PART_NO_XIAOER;
		}

		d = rd.nextInt(100) + 1;
		List<WelFare> twoGl = welFareDAO.getTwoGl(part_no);
		int ces = 0;
		for (WelFare wf : twoGl) {
			if (d <= wf.getWin_percent_2() + ces) {
				welFare = wf;
				break;
			} else {
				ces += wf.getWin_percent_2();
			}
		}
		WelFareMsgAck ack = new WelFareMsgAck();
		ack.welFare_id = welFare.getWelfare_id();

		if (pl.getLuckyDrawsTimes() > 0) {
			pl.setLuckyDrawsTimes(pl.getLuckyDrawsTimes() - 1);
			userDAO.updatePlayerFreeCj(pl.getPlayerID(),
					pl.getLuckyDrawsTimes());
		}

		// 房卡
		UserBackpack item = pl.getPlayerItemByBaseID(3333);

		if (item == null) {
			item = new UserBackpack();
			item.setPlayerID(pl.getPlayerID());
			item.setItemBaseID(3333);
			item.setItemNum(welFare.getOp_money());
			item.setItemID(pl.getPlayerID() + "_" + 3333);
			userBackpackDAO.createPlayerItem(item, "");
			pl.getItems().add(item);
		} else {
			item.setItemNum(item.getItemNum() + welFare.getOp_money());
			userBackpackDAO.updatePlayerItemNum(pl.getPlayerID(), 3333,
					item.getItemNum(), "");
			for (int i = 0; i < pl.getItems().size(); i++) {
				if (pl.getItems().get(i).getItemID().equals(item.getItemID())) {
					pl.getItems().get(i).setItemNum(item.getItemNum());
				}
			}
		}

		// 存日志
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_ADD_PRO,
				LogConstant.OPERATION_SUBTYPE_WELFARE, welFare.getOp_money(),
				"通过转盘获得了房卡" + welFare.getOp_money() + "张", 3333, "lxmj");

		// 处理缓存 包括背包、道具
		load_player_items(pl);

		ack.op_money = welFare.getOp_money();
		ack.player = pl;

		GameContext.gameSocket.send(pl.getSession(), ack);

	}

	public IProxyVipRoomRecordDAO getProxyVipRoomRecordDAO() {
		return proxyVipRoomRecordDAO;
	}

	public void setProxyVipRoomRecordDAO(
			IProxyVipRoomRecordDAO proxyVipRoomRecordDAO) {
		this.proxyVipRoomRecordDAO = proxyVipRoomRecordDAO;
	}

	/**************************** 俱乐部 **************************************/

	public ITClubDao gettClubDao() {
		return tClubDao;
	}

	public void settClubDao(ITClubDao tClubDao) {
		this.tClubDao = tClubDao;
	}

	public IClubRankingListDAO getClubRankingListDAO() {
		return clubRankingListDAO;
	}

	public void setClubRankingListDAO(IClubRankingListDAO clubRankingListDAO) {
		this.clubRankingListDAO = clubRankingListDAO;
	}

	public IClubTemplate getClubTemplateDao() {
		return clubTemplateDao;
	}

	/**
	 * 玩家俱乐部权限修改（管理后台设置）  2018-08-28
	 */
	public boolean updateClubType(int playerIndex, int clubType) {
		if (playerIndex <= 0)
			return false;
		User pl = userDAO.getPlayerByPlayerIndex(playerIndex);
		if (pl == null)
			return false;
		pl.setClubType(clubType);
		userDAO.updateClubType(pl.getPlayerID(), clubType);
		if (playerMap.get(pl.getPlayerID()) != null) {
			playerMap.get(pl.getPlayerID()).setClubType(clubType);
			pl = playerMap.get(pl.getPlayerID());
		}
		if (playerWXMap.get(pl.getUnionID()) != null) {
			playerWXMap.get(pl.getUnionID()).setClubType(clubType);
			pl = playerWXMap.get(pl.getUnionID());
		}

		// 插日志 记录
		String detail = "平台修改，用户：" + pl.getPlayerName() + " ,玩家id："
				+ pl.getPlayerID() + "，俱乐部权限修改为： " + clubType;
		logger.error(detail);
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_UPDATE_PLAYER_CLUB_TYPE, 0, 1,
				detail, 0, "hlmj");

		UpdatePlayerProxyOpenRoomMsg upm = new UpdatePlayerProxyOpenRoomMsg();
		upm.init(pl.getProxyOpenRoom());
		upm.clubType = clubType;
		GameContext.gameSocket.send(pl.getSession(), upm);
		return true;
	}

	/**
	 * 玩家俱乐部状态修改（管理后台设置） 
	 */
	public boolean updateClubState(int clubCode, int clubState, int playerIndex) {
		if (clubCode <= 0 || playerIndex <= 0)
			return false;

		tClubDao.updateClubState(clubCode, clubState);

		// 插日志 记录
		String detail = "平台修改，俱乐部：" + clubCode + " ,状态修改为： " + clubState;

		User pl = userDAO.getPlayerByPlayerIndex(playerIndex);
		if (playerMap.get(pl.getPlayerID()) != null) {
			pl = playerMap.get(pl.getPlayerID());
		}
		if (playerWXMap.get(pl.getUnionID()) != null) {
			pl = playerWXMap.get(pl.getUnionID());
		}
		if (pl == null)
			return false;
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_UPDATE_CLUB_STATE, 0, 1, detail, 0,
				pl.getGameId(), clubCode);
		return true;
	}

	/**
	 * 新建俱乐部
	 */
	public void createClub(User pl, CreateClubMsg msg) {
		// TODO 判断身上有卡，并且够初始卡数
		CreateClubMsgAck ack = new CreateClubMsgAck();
		/* 验证名称合法性 */
		String word = msg.clubName.replaceAll(" ", "");
		IEntranceUserService entranceService = (IEntranceUserService) SpringService
				.getBean("entranceUserService");
		SensitiveWords words = entranceService.getSensitiveWords(word);
		if (words != null) {
			// 昵称不合法；
			logger.error("玩家：" + pl.getPlayerIndex() + "，创建俱乐部名称包含非法内容，创建失败");
			ack.errorResult = 1;
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}

		TClub club = new TClub();
		club.setId(UUIDGenerator.generatorUUID());
		club.setCreatePlayerId(msg.playerId);
		club.setClubName(msg.getClubName().trim());
		club.setClubState(1);
		club.setClubType(1);
		club.setRoomSwtich(msg.getRoomSwtich());
		club = tClubDao.createClub(club);
		club.setHeadUrl(pl.getHeadImgUrl());
		ack.club = club;

		// 创建6个空模板桌，并且添加到数据库
		List<ClubTemplate> gtTmplates = addClubTemplate(club.getClubCode());
		CreateVipRoomMsg createVipRoomMsg = new CreateVipRoomMsg();
		createVipRoomMsg.clubCode = club.getClubCode();
		createVipRoomMsg.roomID = 2006;
		createVipRoomMsg.psw = "";
		createVipRoomMsg.gameID = "nndzz";
		createVipRoomMsg.roomLevel = "1";
		/** 俱乐部固定房间，按房主扣卡存 */
		createVipRoomMsg.isProxy = 1;
		for (int i = 0; i < gtTmplates.size(); i++) {
			ClubTemplate template = gtTmplates.get(i);
			createVipRoomMsg.quanNum = template.getQuantity();
			createVipRoomMsg.tableRule = this.tableRuleConverter(template
					.getRules());
			this.createClubTemplateRoom(createVipRoomMsg,
					template.getTemplateInDex(), template.getQuantityType(), pl);
		}

		GameContext.gameSocket.send(pl.getSession(), ack);
	}

	/**
	 * 根据牌桌对象，创建一个模板牌桌对象
	 * 
	 * @param gt
	 */
	public void createClubTemplateTable(GameTable gt) {
		CreateVipRoomMsg createVipRoomMsg = new CreateVipRoomMsg();
		createVipRoomMsg.clubCode = gt.getClubCode();
		createVipRoomMsg.roomID = gt.getRoomID();
		createVipRoomMsg.psw = "";
		createVipRoomMsg.gameID = "nndzz";
		/** 俱乐部固定房间，按房主扣卡存 */
		createVipRoomMsg.isProxy = 1;
		createVipRoomMsg.quanNum = gt.getTotalJuNum();
		createVipRoomMsg.tableRule = gt.getTableRule();
		TClub club = tClubDao.getClubByClubCode(gt.getClubCode());
		User pl = userDAO.getPlayerByID(club.getCreatePlayerId());
		this.createClubTemplateRoom(createVipRoomMsg,
				gt.getClubTemplateIndex(), gt.getQuantityType(), pl);
	}

	/**
	 * @see ICacheUserService#sendMsgToClubPlayer
	 */
	public void sendMsgToClubPlayer(int clubCode, MsgBase msg) {
		List<String> clubMembers = clubMemberDAO
				.getClubMembersByClubCode(clubCode);
		for (String playerId : clubMembers) {
			User pl = this.getPlayerByPlayerIDFromCache(playerId);
			if (pl != null) {
				GameContext.gameSocket.send(pl.getSession(), msg);
			}
		}
	}
	
	/**
	 * @see ICacheUserService#sendMsgToClubPlayer
	 */
	public void sendMsgToClubPlayerExceptOne(int clubCode, MsgBase msg, User except) {
		List<String> clubMembers = clubMemberDAO
				.getClubMembersByClubCode(clubCode);
		for (String playerId : clubMembers) {
			User pl = this.getPlayerByPlayerIDFromCache(playerId);
			if (pl != null && !pl.getPlayerID().equals(except.getPlayerID())) {
				GameContext.gameSocket.send(pl.getSession(), msg);
			}
		}
	}

	/**
	 * /
	 * @description 创建俱乐部的模板房间
	 * @param msg
	 * @param pl
	 */
	public void createClubTemplateRoom(CreateVipRoomMsg msg,
			int clubTemplateIndex, int quantityType, User pl) {
		if (msg.roomID == 0) {
			return;
		}
		pl.setGameId(msg.gameID);
		logger.info("==>创建创建俱乐部的模板房间，玩家ID【" + pl.getPlayerIndex() + "】，玩家昵称【"
				+ pl.getPlayerName() + "】，所选规则【" + msg.tableRule + "】");

		if (this.tableLogic_nndzz.getRoom(msg.roomID).roomType == GameConstant.ROOM_TYPE_COUPLE) {
			pl.setTwoVipOnline(true);
		}
		tableLogic_nndzz.createClubTemplateTable(pl, msg.psw, msg.quanNum,
				msg.roomID, msg.tableRule, msg.gameID, 5, msg.isRecord,
				msg.isProxy, msg.clubCode, clubTemplateIndex, quantityType,msg.roomLevel);
	}

	/**
	 * 
	 * @description 创建牌桌模板，如果已经存在，则直接获取新会，如果不存在，则返回新创建的
	 * @param clubCode
	 *            俱乐部编号
	 */
	private List<ClubTemplate> addClubTemplate(int clubCode) {
		List<ClubTemplate> gtTmplates = clubTemplateDao.getAll(clubCode);
		if (gtTmplates == null || gtTmplates.size() == 0) {
			gtTmplates = new ArrayList<ClubTemplate>();
			Date date = new Date();
			for (int i = 0; i < 6; i++) {
				ClubTemplate ct = new ClubTemplate();
				ct.setTemplateInDex(i + 1);
				ct.setClubCode(clubCode);
				ct.setPlayerCount(5);
				ct.setQuantityType(0);
				ct.setQuantity(12);
				ct.setRules("");
				ct.setCreateTime(date);
				ct.setId(clubCode + "_" + (i + 1));
				ct.setRoomLevel("1");
				gtTmplates.add(ct);
				clubTemplateDao.insert(ct);
			}
		}
		return gtTmplates;
	}

	/**
	 * @see ICacheUserService#newGetSingleClubInfoMsgAck
	 */
	public GetSingleClubInfoMsgAck newGetSingleClubInfoMsgAck(int clubCode) {
		Map<String, List<GameTable>> gtmMap = this.tableLogic_nndzz
				.getClubGameTables(clubCode);
		GetSingleClubInfoMsgAck msgAck = new GetSingleClubInfoMsgAck();
		msgAck.isUpdate = 1;
		msgAck.clubCode = clubCode;
		msgAck.playerType = 0;
		TClub club = tClubDao.getClubByClubCode(clubCode);
		msgAck.fangkaNum = club.getFangkaNum();
		for (GameTable gt : gtmMap.get(GameConstant.STATIC_CLUB_TABLE)) {
			TableInfoBean infoBean = new TableInfoBean();
			infoBean.playerCount = gt.getPlayerCount();
			infoBean.templateIndex = gt.getClubTemplateIndex();
			infoBean.tableNo = gt.getVipTableID();
			infoBean.tableId = gt.getTableID();
			infoBean.type = gt.getClubTableState();
			infoBean.quantityType = gt.getQuantityType();
			infoBean.quantity = gt.getTotalJuNum();
			infoBean.quanNum = gt.getTotalJuNum();
			infoBean.tableRule = gt.getTableRule();
			infoBean.roomLevel = gt.getRoomLevel();
			setTablePlayerInfo(gt, infoBean);
			msgAck.templateTables.add(infoBean);
		}
		// 固定牌桌，按模板索引排序
		Collections.sort(msgAck.templateTables,
				new Comparator<TableInfoBean>() {
					public int compare(TableInfoBean o1, TableInfoBean o2) {
						return o1.templateIndex - o2.templateIndex;
					}
				});
		for (GameTable gt : gtmMap.get(GameConstant.USER_CLUB_TABLE)) {
			TableInfoBean infoBean = new TableInfoBean();
			infoBean.playerCount = gt.getPlayerCount();
			infoBean.templateIndex = gt.getClubTemplateIndex();
			infoBean.tableNo = gt.getVipTableID();
			infoBean.tableId = gt.getTableID();
			infoBean.type = gt.getClubTableState();
			infoBean.quantityType = gt.getQuantityType();
			infoBean.quantity = gt.getTotalJuNum();
			infoBean.quanNum = gt.getTotalJuNum();
			infoBean.tableRule = gt.getTableRule();
			infoBean.roomLevel = gt.getRoomLevel();
			setTablePlayerInfo(gt, infoBean);
			msgAck.privateTables.add(infoBean);
		}
		for (GameTable gt : gtmMap.get(GameConstant.STATIC_CLUB_PLAYING_TABLE)) {
			TableInfoBean infoBean = new TableInfoBean();
			infoBean.playerCount = gt.getPlayerCount();
			infoBean.templateIndex = gt.getClubTemplateIndex();
			infoBean.tableNo = gt.getVipTableID();
			infoBean.tableId = gt.getTableID();
			infoBean.type = gt.getClubTableState();
			infoBean.quantityType = gt.getQuantityType();
			infoBean.quantity = gt.getTotalJuNum();
			infoBean.quanNum = gt.getTotalJuNum();
			infoBean.tableRule = gt.getTableRule();
			infoBean.roomLevel = gt.getRoomLevel();
			setTablePlayerInfo(gt, infoBean);
			msgAck.playingTables.add(infoBean);
		}
		return msgAck;
	}

	/**
	 * @description 获取单个俱乐部的信息
	 * /
	 * @param pl
	 * @param infoMsg
	 */
	public void getSingleClubInfo(User pl, GetSingleClubInfoMsg infoMsg,
			boolean isSendAll, int isUpdate) {
		Map<String, List<GameTable>> gtmMap = this.tableLogic_nndzz
				.getClubGameTables(infoMsg.clubCode);
		if (pl == null) {
			return;
		}
		GetSingleClubInfoMsgAck msgAck = new GetSingleClubInfoMsgAck();
		msgAck.isUpdate = isUpdate;
		msgAck.clubCode = infoMsg.clubCode;
		msgAck.playerType = 0;
		TClub club = tClubDao.getClubByClubCode(infoMsg.clubCode,pl.getPlayerID());
		if (club == null) {
			msgAck.errorCode = 0;
			if (isSendAll) {
				this.sendMsgToClubPlayer(infoMsg.clubCode, msgAck);
			} else {
				GameContext.gameSocket.send(pl.getSession(), msgAck);
			}
			return;
		}
		msgAck.fangkaNum = club.getFangkaNum();
		User clubCreator = userDAO.getPlayerByID(club.getCreatePlayerId());
		msgAck.clubCreator = clubCreator.getPlayerName();
		msgAck.roomSwtich=club.getRoomSwtich();
		msgAck.goldNum=""+club.getGoldNum();
		msgAck.clubName=club.getClubName();
		
		if (club.getCreatePlayerId().equals(pl.getPlayerID())) {
			msgAck.playerType = 1;
		}else{
			msgAck.goldNum=""+club.getMemberGold();
		}
		
		List<ClubTemplate> gtTmplates = clubTemplateDao.getAll(infoMsg.clubCode);
		
		List<GameTable> staticTables = gtmMap.get(GameConstant.STATIC_CLUB_TABLE);
		if (staticTables == null || staticTables.size() < gtTmplates.size()) {
			// 不满就自动补充
			List<Integer> useTemplateIndexs = new ArrayList<Integer>(staticTables.size());
			for(GameTable gt : staticTables){
				useTemplateIndexs.add(gt.getClubTemplateIndex());
			}
			
			boolean isCreateNew = false;
			for (int i = 0; i < gtTmplates.size(); i++) {
				ClubTemplate template = gtTmplates.get(i);
				int index = template.getTemplateInDex();
				if(!useTemplateIndexs.contains(index)){
					isCreateNew = true;
					CreateVipRoomMsg createVipRoomMsg = new CreateVipRoomMsg();
					createVipRoomMsg.clubCode = club.getClubCode();
					if (template.getPlayerCount() == 2) {
						createVipRoomMsg.roomID = 2004;
					} else if (template.getPlayerCount() == 3) {
						createVipRoomMsg.roomID = 2007;
					} else {
						createVipRoomMsg.roomID = 2006;
					}
					createVipRoomMsg.psw = "";
					createVipRoomMsg.gameID = "nndzz";
					/** 俱乐部固定房间，按房主扣卡存 */
					createVipRoomMsg.isProxy = 1;
					createVipRoomMsg.quanNum = template.getQuantity();
					createVipRoomMsg.tableRule = this.tableRuleConverter(template
							.getRules());
					createVipRoomMsg.roomLevel=template.getRoomLevel();
					this.createClubTemplateRoom(createVipRoomMsg,
							template.getTemplateInDex(),
							template.getQuantityType(), clubCreator);
					logger.info("【电子庄牛牛】俱乐部【"+infoMsg.clubCode+"】补充固定牌桌，使用模板索引【"+index+"】，扣卡方式【"+createVipRoomMsg.isProxy
								+"】，局数【"+createVipRoomMsg.quanNum+"】，场次等级【"+createVipRoomMsg.roomLevel+"】");
				}
			}
			gtmMap = this.tableLogic_nndzz.getClubGameTables(infoMsg.clubCode);
			
			if(isCreateNew){
				logger.info("==========>【电子庄牛牛】俱乐部【"+infoMsg.clubCode+"】补充固定牌桌，通知其他玩家最新的牌桌情况");
				
	        	//TODO 发消息通知前台俱乐部的其他所有人，当前俱乐部各种牌桌状态
				sendMsgToClubPlayerExceptOne(club.getClubCode(), newGetSingleClubInfoMsgAck(club.getClubCode()), pl);
			}
			
		}
		for (GameTable gt : gtmMap.get(GameConstant.STATIC_CLUB_TABLE)) {
			TableInfoBean infoBean = new TableInfoBean();
			infoBean.playerCount = gt.getPlayerCount();
			infoBean.templateIndex = gt.getClubTemplateIndex();
			infoBean.tableNo = gt.getVipTableID();
			infoBean.tableId = gt.getTableID();
			infoBean.type = gt.getClubTableState();
			infoBean.quantityType = gt.getQuantityRawType();
			infoBean.quantity = gt.getTotalJuNum();
			infoBean.quanNum = gt.getTotalJuNum();
			infoBean.tableRule = gt.getTableRule();
			infoBean.roomLevel=gt.getRoomLevel();
			setTablePlayerInfo(gt, infoBean);
			msgAck.templateTables.add(infoBean);
		}
		// 固定牌桌，按模板索引排序
		Collections.sort(msgAck.templateTables,
				new Comparator<TableInfoBean>() {
					public int compare(TableInfoBean o1, TableInfoBean o2) {
						return o1.templateIndex - o2.templateIndex;
					}
				});

		for (GameTable gt : gtmMap.get(GameConstant.USER_CLUB_TABLE)) {
			TableInfoBean infoBean = new TableInfoBean();
			infoBean.playerCount = gt.getPlayerCount();
			infoBean.templateIndex = gt.getClubTemplateIndex();
			infoBean.tableNo = gt.getVipTableID();
			infoBean.tableId = gt.getTableID();
			infoBean.type = gt.getClubTableState();
			infoBean.quantityType = gt.getQuantityType();
			infoBean.quantity = gt.getTotalJuNum();
			infoBean.quanNum = gt.getTotalJuNum();
			infoBean.tableRule = gt.getTableRule();
			infoBean.roomLevel=gt.getRoomLevel();
			setTablePlayerInfo(gt, infoBean);
			msgAck.privateTables.add(infoBean);
		}
		for (GameTable gt : gtmMap.get(GameConstant.STATIC_CLUB_PLAYING_TABLE)) {
			TableInfoBean infoBean = new TableInfoBean();
			infoBean.playerCount = gt.getPlayerCount();
			infoBean.templateIndex = gt.getClubTemplateIndex();
			infoBean.tableNo = gt.getVipTableID();
			infoBean.tableId = gt.getTableID();
			infoBean.type = gt.getClubTableState();
			infoBean.quantityType = gt.getQuantityType();
			infoBean.quantity = gt.getTotalJuNum();
			infoBean.quanNum = gt.getTotalJuNum();
			infoBean.tableRule = gt.getTableRule();
			infoBean.roomLevel=gt.getRoomLevel();
			setTablePlayerInfo(gt, infoBean);
			msgAck.playingTables.add(infoBean);
		}
		if (isSendAll) {
			this.sendMsgToClubPlayer(infoMsg.clubCode, msgAck);
		} else {
			GameContext.gameSocket.send(pl.getSession(), msgAck);
		}
	}

	/**
	 * @description 设置牌桌玩家信息
	 * /
	 * @param gt
	 * @param infoBean
	 */
	private void setTablePlayerInfo(GameTable gt, TableInfoBean infoBean) {
		if (gt.getPlayers() != null && gt.getPlayers().size() > 0) {
			for (int i = 0; i < gt.getPlayers().size(); i++) {
				User player = gt.getPlayers().get(i);
				TableUserInfo userInfo = new TableUserInfo();
				userInfo.playerName = player.getPlayerName();
				userInfo.playerId = player.getPlayerID();
				userInfo.headImgUrl = player.getHeadImgUrl();
				userInfo.sex = player.getSex() == 1 ? 1 : 0;
				userInfo.tablePos = player.getTablePos();
				infoBean.players.add(userInfo);
			}
		}
	}

	/**
	 * @description 牌桌玩法规则转字符串
	 * /
	 * @param tableRule
	 * @return
	 */
	private List<Integer> tableRuleConverter(String tableRule) {
		List<Integer> list = new ArrayList<Integer>();
		if (tableRule == null || tableRule.trim().equals("")) {
			return list;
		}
		for (String rule : tableRule.split(";")) {
			if (NumberUtils.toInt(rule, -1) != -1) {
				list.add(NumberUtils.toInt(rule));
			}
		}
		return list;
	}

	/**
	 * @description 根据俱乐部编号和模板索引，获取对应的牌桌
	 * /
	 * @param clubNo
	 *            俱乐部编号
	 * @param clubTemplateIndex
	 *            模板索引
	 * @return
	 */
	private GameTable getGameTableByTemplateIndex(int clubNo,
			int clubTemplateIndex) {
		Map<String, List<GameTable>> gtmMap = this.tableLogic_nndzz
				.getClubGameTables(clubNo);
		List<GameTable> staticTables = gtmMap
				.get(GameConstant.STATIC_CLUB_TABLE);
		for (GameTable gt : staticTables) {
			if (gt.getClubTemplateIndex() == clubTemplateIndex) {
				return gt;
			}
		}
		return null;
	}

	public void createSystemNotice(SystemNotice notice) {
		systemNoticeDAO.createSystemNotice(notice);
	}
	public void deleteSystemNotice(String type){
		systemNoticeDAO.deleteSystemNotice(type);
	}
	
	public void deleteClubTemplate(int id) {
		clubTemplateDao.delate(id);
	}

	public List<ClubTemplate> getClubTemplateAll(int clubCode) {
		List<ClubTemplate> clubTemplates = clubTemplateDao.getAll(clubCode);
		return clubTemplates;
	}

	public void updateClubTemplate(User pl, FreeTableManagerMsg msg) {
		ClubTemplate ct = clubTemplateDao.getClubTemplate(msg.clubCode,
				msg.tempIndex);

		GameTable tableTmp = this.getGameTableByTemplateIndex(msg.clubCode,
				msg.tempIndex);
		if (tableTmp == null) {
			FreeTableManagerMsgAck ack = new FreeTableManagerMsgAck();
			ack.result = 2;
			GameContext.gameSocket.send(pl.getSession(), ack);
		} else if (tableTmp.getPlayers().size() >= 1) {
			FreeTableManagerMsgAck ack = new FreeTableManagerMsgAck();
			ack.result = 3;
			GameContext.gameSocket.send(pl.getSession(), ack);
		} else {
			ct.setClubCode(msg.clubCode);
			ct.setTemplateInDex(msg.tempIndex);
			ct.setPlayerCount(msg.playerCount);
			ct.setQuantityType(msg.quanTityType);
			ct.setQuantity(msg.quanTity);
			ct.setRoomLevel(msg.roomLevel);
			String str = "";
			for (int i = 0; i < msg.tableRule.size(); i++) {
				str = str + msg.tableRule.get(i) + ";";
			}
			ct.setRules(str);
			ct.setCreateTime(new Date());
			clubTemplateDao.update(ct);

			int roomType = 0;
			if (tableTmp.getPlayerCount() != msg.playerCount) {
				tableTmp.game_room.getPlayingTablesMap().remove(
						tableTmp.getTableID());
				int roomId = 0;
				if (msg.playerCount == 2) {
					roomId = ConfigConstant.ROOM_OF_VIP;
					roomType = GameConstant.ROOM_TYPE_COUPLE;
				} else if (msg.playerCount == 3) {
					roomId = ConfigConstant.ROOM_OF_ADVANCED2;
					roomType = GameConstant.ROOM_TYPE_THREE;
				} else {
					roomId = ConfigConstant.ROOM_OF_VIP2;
					roomType = GameConstant.ROOM_TYPE_VIP;
				}
				tableTmp.game_room = this.tableLogic_nndzz.getAllGameRoomMap().get(
						roomId);
				tableTmp.game_room.getPlayingTablesMap().put(
						tableTmp.getTableID(), tableTmp);
				tableTmp.setRoomType(roomType);
				tableTmp.setRoomID(roomId);
			}

			// 处理桌子信息更新
			tableTmp.setPlayerCount(msg.playerCount);
			tableTmp.setTotalJuNum(msg.quanTity);
			tableTmp.setQuantityType(msg.quanTityType);
			tableTmp.setRoomLevel(msg.roomLevel);
			tableTmp.resetFreePos();
			GetSingleClubInfoMsg gs = new GetSingleClubInfoMsg();
			gs.clubCode = msg.clubCode;
			this.getSingleClubInfo(pl, gs, true, 1);

			FreeTableManagerMsgAck ack = new FreeTableManagerMsgAck();
			ack.result = 1;
			GameContext.gameSocket.send(pl.getSession(), ack);
		}
	}

	/**
	 * 查询俱乐部
	 */
	public void queryClubs(User pl, GetClubsMsg msg) {
		GetClubsMsgAck ack = new GetClubsMsgAck();
		List<TClub> clubs = null;
		if(msg.type==0){
			clubs=tClubDao.getAllClubs(pl.getPlayerID());
		}else if(msg.type==1){
			clubs=tClubDao.getClubBySearch(msg.searchTxt);
		}
		logger.error("queryClubs : {}", clubs);
		ack.playerId = pl.getPlayerID();
		ack.clubs = clubs;
		GameContext.gameSocket.send(pl.getSession(), ack);
	}

	/**
	 * 加入俱乐部
	 */
	public void joinClub(User pl, JoinClubMsg msg) {
		JoinClubMsgAck ack = new JoinClubMsgAck();
		ack.joinType = msg.joinType;
		ack.clubCode = msg.clubCode;
		TClub club = tClubDao.getClubByClubCode(msg.clubCode);
		if (club == null) {
			ack.errorResult = 1;// 俱乐部不存在或已关闭
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}
		// 搜索俱乐部返回
		if (club.getCreatePlayerId().equals(pl.getPlayerID())) {
			ack.errorResult = 2;// 自己创建的不能加入
		}
		ClubMember clubMember = clubMemberDAO.queryMemberClub(msg.clubCode,
				pl.getPlayerID());
		if (clubMember != null) {
			ack.errorResult = 3;// 已经是该俱乐部成员
		}
		if (ack.errorResult == 0) {
			// 俱乐部成员数量
			int memberCount = clubMemberDAO.queryMemberCount(msg.clubCode);
			club.setMemberCount(memberCount);
			// if(msg.joinType == 2){
			// //加入俱乐部
			// clubMemberDAO.createClubMember(club.getCreatePlayerId(),
			// msg.memberId, String.valueOf(msg.clubCode));
			// }
			ack.club = club;
		}

		GameContext.gameSocket.send(pl.getSession(), ack);
	}

	/**
	 * 俱乐部充值
	 */
	public void rechargeClub(User pl, ClubRechargeMsg msg) {
		ClubRechargeMsgAck ack = new ClubRechargeMsgAck();
		ack.clubCode = msg.clubCode;

		List<UserBackpack> pis = pl.getItems();
		int cardCount = 0;
		if (pis == null || pis.size() == 0) {
			ack.result = 2;
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}
		UserBackpack pib = pis.get(0);
		cardCount = pib.getItemNum();

		int kakouNumProxy = 0;
		boolean isCardLock = false;
		if (null != pl.getPvrrs() && pl.getPvrrs().size() > 0) {
			for (ProxyVipRoomRecord pvrr : pl.getPvrrs()) {
				if (pvrr.getClubCode() > 0) {
					logger.info("---俱乐部转卡：俱乐部的代开房间不计数---");
					continue;
				}
				if (pvrr.getVipState() == GameConstant.PROXY_NO_START_GAME) {
					kakouNumProxy += pvrr.getProxyCardCount();
					isCardLock = true;
				}
			}
		}
		logger.info("---俱乐部转卡：扣代开锁定（" + kakouNumProxy + "）之前 卡数=" + cardCount
				+ "---");
		cardCount = cardCount - kakouNumProxy;
		logger.info("---俱乐部转卡：扣代开锁定（" + kakouNumProxy + "）之后 卡数=" + cardCount
				+ "---");

		if (cardCount <= 0 || msg.fangkaNum > cardCount) {
			ack.result = 2;
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}
		pib.setItemNum(pib.getItemNum() - msg.fangkaNum);
		// 更新扣卡
		userBackpackDAO.updatePlayerItemNum(pl.getPlayerID(),
				pib.getItemBaseID(), pib.getItemNum(), "");
		load_player_items(pl);
		// 更新库存
		TClub club = tClubDao.getClubByClubCode(msg.clubCode);
		club.setFangkaNum(club.getFangkaNum() + msg.fangkaNum);
		tClubDao.updateClubFangkaNum(msg.clubCode, club.getFangkaNum());

		String detail5 = "玩家【" + pl.getPlayerIndex() + "】给俱乐部【"
				+ club.getClubCode() + "】充值，俱乐部【" + msg.clubCode + "】"
				+ "， 充卡后背包卡数 = " + pib.getItemNum() + "，充卡数量=" + msg.fangkaNum;
		logger.debug(detail5);
		createPlayerLog(pl.getPlayerID(), pl.getPlayerIndex(),
				pl.getPlayerName(), pl.getGold(),
				LogConstant.OPERATION_TYPE_SUB_PRO,
				LogConstant.OPERATION_TYPE_SUB_PRO_TO_CLUB, msg.fangkaNum,
				detail5, pib.getItemBaseID(), "hlmj", club.getClubCode());

		ack.result = 1;
		ack.fangkaNum = club.getFangkaNum();
		ack.backpackNum = pib.getItemNum();
		GameContext.gameSocket.send(pl.getSession(), ack);

	}
	
	public void changeClubLevel(User pl, ClubChargeLevelMsg msg) {
		ClubChargeLevelMsgAck ack = new ClubChargeLevelMsgAck();
		ack.clubCode = msg.clubCode;
		tClubDao.updateClubLevel(msg.clubCode, msg.roomLevel);		
		
		String detail5 = "玩家【" + pl.getPlayerIndex() + "】修改俱乐部【"
		+ msg.getClubCode() + "】场次："+msg.roomLevel;
		logger.debug(detail5);	
		ack.result = 1;
		ack.roomLevel = msg.roomLevel;
		GameContext.gameSocket.send(pl.getSession(), ack);
		
	}

	/**
	 * 更新俱乐部排名
	 * 
	 * @author  2018-9-6 CacheUserService.java
	 * @param playerId
	 * @param clubCode
	 * @param stateStr
	 *            (1,2,3)
	 * @param count
	 *            累加数量
	 * @param columnName
	 *            void
	 */
	public void updateClubRankingList(String playerId, int clubCode,
			String stateStr, int count, String columnName) {
		if ("GAME_COUNT".equals(columnName)) {
			clubRankingListDAO.updateGameCount(playerId, clubCode, stateStr,
					count);
		} else if ("WINNER_COUNT".equals(columnName)) {
			clubRankingListDAO.updateWinnerCount(playerId, clubCode, stateStr,
					count);
		} else if ("GUNNER_COUNT".equals(columnName)) {
			clubRankingListDAO.updateGunnerCount(playerId, clubCode, stateStr,
					count);
		} else if("FANGKA_COUNT".equals(columnName)){
			clubRankingListDAO.updateFangkaCount(playerId, clubCode, stateStr,
					count);
		}
	}

	public void getClubRankingList(GetQyqRankListMsg msg, IoSession session) {
		GetQyqRankListMsgAck vrram = new GetQyqRankListMsgAck();

		List<ClubRankingList> rankListAll = clubRankingListDAO
				.getClubRankingList(msg.clubID, msg.opType);
		// 牌局总数排行
		List<ClubRankingList> rankTotal = new ArrayList<ClubRankingList>();
		rankTotal.addAll(rankListAll);
		// 大赢家排行
		List<ClubRankingList> rankWinner = new ArrayList<ClubRankingList>();
		Collections.sort(rankListAll, new Comparator<ClubRankingList>() {
			public int compare(ClubRankingList o1, ClubRankingList o2) {
				return o2.getWinnerCount() - o1.getWinnerCount();
			}
		});
		rankWinner.addAll(rankListAll);
		// 最佳炮手排行
		List<ClubRankingList> rankPao = new ArrayList<ClubRankingList>();
		Collections.sort(rankListAll, new Comparator<ClubRankingList>() {
			public int compare(ClubRankingList o1, ClubRankingList o2) {
				return o2.getGunnerCount() - o1.getGunnerCount();
			}
		});
		rankPao.addAll(rankListAll);
		
		vrram.rankTotal = rankTotal;
		vrram.rankWinner = rankWinner;
		vrram.rankPao = rankPao;
		GameContext.gameSocket.send(session, vrram);
	}

	/**
	 * @see ICacheUserService#getClubTableCount
	 */
	public int getClubTableCount(int clubNo) {
		Map<String, List<GameTable>> map = this.tableLogic_nndzz
				.getClubGameTables(clubNo);
		int cnt = 0;
		for (Map.Entry<String, List<GameTable>> entry : map.entrySet()) {
			if (GameConstant.STATIC_CLUB_TABLE.equals(entry.getKey())) {
				continue;
			}
			cnt += entry.getValue().size();
		}
		return cnt;
	}

	/**
	 * 俱乐部-开局扣卡
	 * 
	 * @param club
	 * @param costKaNum
	 *            void
	 */
	public void useClubFangka(int vipTableId, TClub club, User pl, int costKaNum,
			String gameId) {
		if (club == null)
			return;
		int finalKaNum = club.getFangkaNum() - costKaNum;
		tClubDao.updateClubFangkaNum(club.getClubCode(), finalKaNum);
		club.setFangkaNum(finalKaNum);

		String detail5 = "俱乐部【" + club.getClubCode() + "】贵宾房【" + vipTableId
				+ "】开局，扣库存，数量=" + costKaNum;
		logger.info(detail5+", 剩余="+finalKaNum);
		User creator = userDAO.getPlayerByID(club.getCreatePlayerId());
		createPlayerLog(creator.getPlayerID(), creator.getPlayerIndex(),
				creator.getPlayerName(), creator.getGold(),
				LogConstant.OPERATION_TYPE_SUB_PRO,
				LogConstant.OPERATION_TYPE_SUB_PRO_TO_CLUB_START_GAME, 1,
				detail5, 3333, gameId, club.getClubCode());
		
		// 更新扣卡排名
		updateClubRankingList(pl.getPlayerID(), club.getClubCode(), "1,2,3,4,10", costKaNum, "FANGKA_COUNT");
	}

	public void updateClubRankState(int state, boolean isToday) {
		clubRankingListDAO.updateClubRankState(state, isToday);

	}

	@Override
	public void updateClubPlayerGold(User pl, ClubChongZhiMsg msg) {
		ClubChongZhiMsgAck ack = new ClubChongZhiMsgAck();
		ack.clubCode = msg.clubCode;
		clubMemberDAO.updateClubMemberGold(msg.clubCode, msg.playerId, msg.goldNum);
        clubMemberDAO.updateClubMemberTotalPay(msg.clubCode, msg.playerId, msg.goldNum);
		
		String detail5 = "玩家【" + pl.getPlayerIndex() + "】给俱乐部玩家【"
		+ msg.playerId + "】充值："+msg.goldNum+"金豆";
		logger.debug(detail5);	
		ack.result = 1;
		ack.clubCode=msg.clubCode;
		ack.playerId=msg.playerId;
		ack.createrId=msg.createrId;
		ack.goldNum=msg.goldNum;
		GameContext.gameSocket.send(pl.getSession(), ack);
		
	}
	
	// 定时调度结算所有俱乐部茶水费
	public void settleClubServiceFee(){
		int enableSettleClubFee = GameContext.getConfigParamToInt("enable_settle_club_fee", 1);
		
		int goldUnit = GameContext.getConfigParamToInt("gold_unit", 1000);
		int fangkaUnit = GameContext.getConfigParamToInt("fangka_unit", 1);
		
		logger.info("开始结算所有俱乐部茶水费：开关="+enableSettleClubFee+", goldUnit="+goldUnit+", fangkaUnit="+fangkaUnit);
		
		BigDecimal bdGoldUnit = new BigDecimal(goldUnit);
		
		List<TClub> clubs = tClubDao.getAllClubs();
		logger.info(clubs.toString());
		for(TClub club : clubs){
			int clubCode = club.getClubCode();
			int fangkaNum = club.getFangkaNum();
			int gold = club.getGoldNum();
			
			logger.info("结算俱乐部："+clubCode+", gold="+gold+", fangka="+fangkaNum);
			try{
				BigDecimal bdGold = new BigDecimal(gold);
				int newFangkaNum = fangkaNum;
				int fangkaCount = 0;
				if(gold > 0){
					BigDecimal bdgn = bdGold.divide(bdGoldUnit, 2);
					bdgn.setScale(0, BigDecimal.ROUND_HALF_UP);
					
					fangkaCount = fangkaUnit * bdgn.intValue();
	
					newFangkaNum = fangkaNum - fangkaCount;
				}
				
				// 抹平盈亏
				tClubDao.addClubGold(clubCode, -gold);
				
				if(enableSettleClubFee == 0){
					logger.info("俱乐部结算开关没开，不扣卡：俱乐部【"+clubCode+"】茶水费，抹平盈亏【"+gold+"】");
					continue;
				}
				
				// 扣卡
				if(fangkaNum != newFangkaNum){
					tClubDao.updateClubFangkaNum(clubCode,  newFangkaNum);
				}
				logger.info("定时调度结算俱乐部【"+clubCode+"】茶水费，盈亏【"+gold
								+"】，兑换比例【"+goldUnit+":"+fangkaUnit+"】，扣卡前房卡数【"+fangkaNum
								+"】，扣卡数【"+fangkaCount+"】，扣卡后房卡数【"+newFangkaNum+"】");
			}catch(Exception e){
				e.printStackTrace();
				logger.error("俱乐部【"+clubCode+"】结算茶水费失败：", e);
			}
		}
	}
	
	public void addClubPlayerGold(User pl, int clubCode, int goldNum){
		clubMemberDAO.updateClubMemberGold(clubCode, pl.getPlayerID(), goldNum);
	}

	public void addClubGold(int clubCode, int goldNum){
		tClubDao.addClubGold(clubCode, goldNum);
	}

	@Override
	public List<SystemNotice> getSysNoticeList() {
		return systemNoticeDAO.getNoticeAll();
	}

	@Override
	public void updateNoticeCache(SystemNotice notice) {
		if(notice.getNotice_type()==0){
			sysNotice.setNotice_title(notice.getNotice_title());
			sysNotice.setNotice_content(notice.getNotice_content());
		}else if(notice.getNotice_type()==1){
			foreverNotice.setNotice_content(notice.getNotice_content());
		}else if(notice.getNotice_type()==5){
			qyqNotice.setNotice_title(notice.getNotice_title());
			qyqNotice.setNotice_content(notice.getNotice_content());
		}else if(notice.getNotice_type()==10){
			hallNotice.setNotice_content(notice.getNotice_content());
		}
		
	}
	
	//绑定推荐人
	public void setExtendPlayer(User pl, UserExtendMsg msg) {
		boolean isEmpty=msg.mgrIndex==null || msg.mgrIndex.trim().equals("");
		if(isEmpty || userDAO.getPlayerByPlayerIndex(Integer.parseInt(msg.mgrIndex))==null){
			UserExtendMsgAck msgAck=new UserExtendMsgAck();
			msgAck.result=0;
			GameContext.gameSocket.send(pl.getSession(), msgAck);
			return;
		}
		userDAO.updatePlayerMgrID(msg.playerID, msg.mgrID, msg.mgrIndex);
		
		pl.setMgrIndex(msg.mgrIndex);	
		UserExtendMsgAck msgAck=new UserExtendMsgAck();
		msgAck.result=1;
		GameContext.gameSocket.send(pl.getSession(), msgAck);
        int reward = GameContext.getConfigParamToInt("invite_reward", 10);
        createItemForPlayer(pl, 3333, "房卡奖励", LogConstant.OPERATION_TYPE_ADD_DIAMOND, reward, "");
		
	}
	
	// 搜索玩家
	public void searchUserByIndex(User pl, SearchUserByIndexMsg msg){
		SearchUserByIndexMsgAck ack = new SearchUserByIndexMsgAck();
		boolean isError = false;
		try{
			if(NumberUtils.isNumber(msg.searchKey)){
				int playerIndex = NumberUtils.toInt(msg.searchKey);
				User plx = this.userDAO.getPlayerByPlayerIndex(playerIndex);
				if(plx != null){
					ack.result = 1;
					ack.playerID = plx.getPlayerID();
					ack.playerIndex = plx.getPlayerIndex();
					ack.playerName = plx.getPlayerName();
					ack.headImgUrl = plx.getHeadImgUrl();
					logger.info("搜索玩家信息：给定key【"+msg.searchKey+"】，搜到的玩家ID【"+ack.playerID+"】，index【"
								+ack.playerIndex+"】，昵称【"+ack.playerName+"】，头像【"+ack.headImgUrl+"】");
					GameContext.gameSocket.send(pl.getSession(), ack);
					return;
				}
			}
		}catch(Exception e){
			isError = true;
		}
		
		logger.info("搜索玩家信息没搜到：给定key【"+msg.searchKey+"】");
		
		ack.result = 0;
		GameContext.gameSocket.send(pl.getSession(), ack);
	}
}
