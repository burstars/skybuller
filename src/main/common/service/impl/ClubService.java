package com.chess.common.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.jacorb.notification.filter.etcl.TCLCleanUp;
import org.jacorb.poa.util.StringPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.UUIDGenerator;
import com.chess.common.bean.ClubMember;
import com.chess.common.bean.ClubTemplate;
import com.chess.common.bean.TClub;
import com.chess.common.bean.TClubLevel;
import com.chess.common.bean.User;
import com.chess.common.dao.IClubMemberDAO;
import com.chess.common.dao.IClubTemplate;
import com.chess.common.dao.ITClubDao;
import com.chess.common.dao.ITClubLevelDao;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.struct.club.FreeTableManagerMsg;
import com.chess.common.msg.struct.club.FreeTableManagerMsgAck;
import com.chess.common.msg.struct.club.GetClubsMsg;
import com.chess.common.msg.struct.club.GetClubsMsgAck;
import com.chess.common.msg.struct.club.JoinClubMsg;
import com.chess.common.msg.struct.club.JoinClubMsgAck;
import com.chess.common.service.IClubService;

public class ClubService implements IClubService {

	private static Logger logger = LoggerFactory.getLogger(ClubService.class);

	private IClubTemplate clubTemplate;

	private ITClubDao tClubDao;

	private IClubMemberDAO clubMemberDAO;

	private ITClubLevelDao clubLevelDao;
	
	private List<TClubLevel> allClublLevels = null;

	public void deleteClubTemplate(int id) {
		this.clubTemplate.delate(id);
	}

	public List<ClubTemplate> getClubTemplateAll(int clubCode) {
		List<ClubTemplate> clubTemplates = this.clubTemplate.getAll(clubCode);
		return clubTemplates;
	}

	public void updateClubTemplate(User pl, FreeTableManagerMsg msg) {
		ClubTemplate ct = this.clubTemplate.getClubTemplate(msg.clubCode,
				msg.tempIndex);

		if (ct == null) {
			ct = new ClubTemplate();
			ct.setId(UUIDGenerator.generatorUUID());

			ct.setClubCode(msg.clubCode);
			ct.setTemplateInDex(msg.tempIndex);
			ct.setPlayerCount(msg.playerCount);
			ct.setQuantityType(msg.quanTityType);
			ct.setQuantity(msg.quanTity);
			String str = "";
			for (int i = 0; i < msg.tableRule.size(); i++) {
				str = str + msg.tableRule.get(i) + ";";
			}
			ct.setRules(str);
			ct.setCreateTime(new Date());
			this.clubTemplate.insert(ct);
			FreeTableManagerMsgAck ack = new FreeTableManagerMsgAck();
			ack.result = 1;
			GameContext.gameSocket.send(pl.getSession(), ack);
		} else {
			ct.setClubCode(msg.clubCode);
			ct.setTemplateInDex(msg.tempIndex);
			ct.setPlayerCount(msg.playerCount);
			ct.setQuantityType(msg.quanTityType);
			ct.setQuantity(msg.quanTity);
			String str = "";
			for (int i = 0; i < msg.tableRule.size(); i++) {
				str = str + msg.tableRule.get(i) + ";";
			}
			ct.setRules(str);
			ct.setCreateTime(new Date());
			this.clubTemplate.update(ct);
			FreeTableManagerMsgAck ack = new FreeTableManagerMsgAck();
			ack.result = 1;
			GameContext.gameSocket.send(pl.getSession(), ack);
		}

	}

	public void addClubTemplate(int clubCode) {
		Date date = new Date();
		for (int i = 0; i < 6; i++) {
			ClubTemplate ct = new ClubTemplate();
			ct.setTemplateInDex(i);
			ct.setClubCode(clubCode);
			ct.setPlayerCount(4);
			ct.setQuantityType(0);
			ct.setQuantity(1);
			ct.setRules("");
			ct.setCreateTime(date);
			ct.setId(clubCode + "_" + i);
			this.clubTemplate.insert(ct);
		}
	}

	/**
	 * 查询俱乐部
	 */
	public void queryClubs(User pl, GetClubsMsg msg) {
		GetClubsMsgAck ack = new GetClubsMsgAck();
		List<TClub> clubs = tClubDao.getAllClubs(pl.getPlayerID());
		ack.playerId = pl.getPlayerID();
		ack.clubs = clubs;
		GameContext.gameSocket.send(pl.getSession(), ack);
	}

	/**
	 * 加入俱乐部
	 */
	public void joinClub(User pl, JoinClubMsg msg) {
		JoinClubMsgAck ack = new JoinClubMsgAck();
		ack.joinType = msg.joinType;
		ack.clubCode = msg.clubCode;
		TClub club = tClubDao.getClubByClubCode(msg.clubCode);
		if (club == null) {
			ack.errorResult = 1;// 俱乐部不存在
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}
		// 搜索俱乐部返回
		if (club.getCreatePlayerId().equals(pl.getPlayerID())) {
			ack.errorResult = 2;// 自己创建的不能加入
		}
		ClubMember clubMember = clubMemberDAO.queryMemberClub(msg.clubCode,
				pl.getPlayerID());
		if (clubMember != null) {
			ack.errorResult = 3;// 已经是该俱乐部成员
		}
		if (ack.errorResult == 0) {
			// 俱乐部成员数量
			int memberCount = clubMemberDAO.queryMemberCount(msg.clubCode);
			club.setMemberCount(memberCount);
			// if(msg.joinType == 2){
			// //加入俱乐部
			// clubMemberDAO.createClubMember(club.getCreatePlayerId(),
			// msg.memberId, String.valueOf(msg.clubCode));
			// }
			club.setHeadUrl("http://wx.qlogo.cn/mmopen/vi_32/E5icvvKzlGRAvjt8micDX2PqnHZleLpdz2jsZRJejHOibIjuBXWJ3ibZnQreeDlGfjwr8k2iakkVETicg49fnOe482xQ/132");
			ack.club = club;
		}

		GameContext.gameSocket.send(pl.getSession(), ack);
	}
	
	public List<TClubLevel> getAllClubLevels(){
		if(allClublLevels != null){
			return allClublLevels;
		}
		allClublLevels = clubLevelDao.getAllClubLevels();
		return allClublLevels;
	}
	
	public List<Integer> getCoinItems(String level){
		List<Integer> items = new ArrayList<Integer>();
		List<TClubLevel> clubLevels = getAllClubLevels();
		for(TClubLevel clubLevel : clubLevels){
			if(String.valueOf(clubLevel.getId()).equals(level)){
				String goldLimit = clubLevel.getGoldLimit();
				if(goldLimit != null && !"".equals(goldLimit)){
					String[] sgl = goldLimit.split(",");
					for(String gl : sgl){
						if(NumberUtils.isNumber(gl)){
							items.add(NumberUtils.stringToInt(gl));
						}
					}
				}
				break;
			}
		}
		return items;
	}
	
	public TClubLevel getClubLevel(String level){
		List<TClubLevel> clubLevels = getAllClubLevels();
		for(TClubLevel clubLevel : clubLevels){
			if(String.valueOf(clubLevel.getId()).equals(level)){
				return clubLevel;
			}
		}
		return null;
	}
	
	public TClub getClub(int clubCode){
		return tClubDao.getClubByClubCode(clubCode);
	}
	
	public ClubMember getClubMember(int clubCode, String playerId){
		return clubMemberDAO.getClubMemberByPlayerId(clubCode, playerId);
	}

	public IClubTemplate getClubTemplate() {
		return clubTemplate;
	}

	public void setClubTemplate(IClubTemplate clubTemplate) {
		this.clubTemplate = clubTemplate;
	}

	public ITClubDao gettClubDao() {
		return tClubDao;
	}

	public void settClubDao(ITClubDao tClubDao) {
		this.tClubDao = tClubDao;
	}

	public IClubMemberDAO getClubMemberDAO() {
		return clubMemberDAO;
	}

	public void setClubMemberDAO(IClubMemberDAO clubMemberDAO) {
		this.clubMemberDAO = clubMemberDAO;
	}

	public ITClubLevelDao getClubLevelDao() {
		return clubLevelDao;
	}

	public void setClubLevelDao(ITClubLevelDao clubLevelDao) {
		this.clubLevelDao = clubLevelDao;
	}

}
