

package com.chess.common.service.impl;

import java.util.List;
import java.util.Map;

import com.chess.common.bean.MallItem;
import com.chess.common.dao.IMallItemDAO;
import com.chess.common.service.IMallItemService;

public class MallItemService implements IMallItemService {

	private IMallItemDAO mallItemDAO;
	
	public void deleteItemByID(int itemID) {
		mallItemDAO.deleteItemBaseByID(itemID);
	}
	
	public List<MallItem> refreshItemBase() {
		return mallItemDAO.getAll();
	}

	public IMallItemDAO getMallItemDAO() {
		return mallItemDAO;
	}

	public void setMallItemDAO(IMallItemDAO mallItemDAO) {
		this.mallItemDAO = mallItemDAO;
	}

	public MallItem getItemBaseById(int itemBaseID) {
		return mallItemDAO.getItemBaseByItemID(itemBaseID);
	}

	public Integer getSellCard(Map<String, String> map){
		return mallItemDAO.getSellCard(map);
	}

}
