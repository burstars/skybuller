package com.chess.common.service.impl;

import java.util.List;

import com.chess.common.bean.User;
import com.chess.common.bean.UserBackpack;
import com.chess.common.dao.IUserBackpackDAO;
import com.chess.common.dao.IUserDAO;
import com.chess.common.service.IUserService;

public class UserService implements IUserService {

	public IUserDAO userDAO;

	public IUserBackpackDAO userBackpackDAO;

	public void createPlayer(User pl){
		 userDAO.createPlayer(pl);
	}
	
	public List<User> getAllPlayer() {

		return userDAO.getAllPlayer();
	}

	public User getPlayerByID(String id) {

		return userDAO.getPlayerByID(id);
	}

	public IUserDAO getPlayerDAO() {
		return userDAO;
	}

	public void setPlayerDAO(IUserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public List<User> getAllPlayerByPage(String startRownum, String endRownum, String startTime, String endTime, int order, int queryPlayerType) {
		return userDAO.getAllPlayerByPage(startRownum, endRownum, startTime, endTime, order, queryPlayerType);
	}

	public int getTotalCount(String startTime, String endTime, int queryPlayerType) {

		return userDAO.getTotalCount(startTime, endTime, queryPlayerType);
	}

	public User getPlayerByAccount(String account) {
		return userDAO.getPlayerByAccount(account);
	}

	public List<UserBackpack> getPlayerItemByID(String id) {
		return userBackpackDAO.getPlayerItemListByPlayerID(id);
	}
	
	public void updatePlayerItemNum(String id,Integer itemBaseID,int itemNum) {
		userBackpackDAO.updatePlayerItemNum(id,itemBaseID,itemNum,"");
	}
	
	public void createPlayerItem(UserBackpack item) {
		userBackpackDAO.createPlayerItem(item,"");
	}

	public IUserBackpackDAO getUserBackpackDAO() {
		return userBackpackDAO;
	}

	public void setUserBackpackDAO(IUserBackpackDAO userBackpackDAO) {
		this.userBackpackDAO = userBackpackDAO;
	}

	public User getPlayerByPlayerIndex(int index) {
		// TODO Auto-generated method stub
		return userDAO.getPlayerByPlayerIndex(index);
	}
	
	public User getPlayerByPlayerName(String playerName)
	{
		return userDAO.getPlayerByName(playerName);
	}

	public int getCustomTotalCount(String account){
		return userDAO.getCustomTotalCount(account);
	}

	public List<User> getAllCustomPlayerByPage(String startRownum, String endRownum,String account)
	{
		return userDAO.getAllCustomPlayerByPage(startRownum, endRownum, account);
	}

	/**更新玩家金币*/
	public void updatePlayerGold(String playerID, int gold)
	{
		userDAO.updatePlayerGold(playerID, gold);
	}
	
	public List<User> getPlayersByPlayerIDByPage(String startRownum, String endRownum,String account, String startTime, String endTime, int order)
	{
		return userDAO.getPlayersByPlayerIDByPage(startRownum, endRownum, account, startTime, endTime, order);
	}
	
	public int getPlayersByPlayerIDCount(String account, String startTime, String endTime)
	{
		return userDAO.getPlayersByPlayerIDCount(account, startTime, endTime);
	}
	
	public List<User> getPlayersByPlayerAccountByPage(String startRownum, String endRownum,String account, String startTime, String endTime, int order)
	{
		return userDAO.getPlayersByPlayerAccountByPage(startRownum, endRownum, account, startTime, endTime, order);
	}
	
	public int getPlayersByPlayerAccountCount(String account, String startTime, String endTime)
	{
		return userDAO.getPlayersByPlayerAccountCount(account, startTime, endTime);
	}
	
	public List<User> getPlayersByPlayerNameByPage(String startRownum, String endRownum,String account, String startTime, String endTime, int order)
	{
		return userDAO.getPlayersByPlayerNameByPage(startRownum, endRownum, account, startTime, endTime, order);
	}
	
	public int getPlayersByPlayerNameCount(String account, String startTime, String endTime)
	{
		return userDAO.getPlayersByPlayerNameCount(account, startTime, endTime);
	}

}
