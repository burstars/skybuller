package com.chess.common.service.impl;

import com.chess.common.bean.ActiveCode;
import com.chess.common.dao.IActiveCodeDAO;
import com.chess.common.service.IActiveCodeService;
import com.chess.common.service.IUserService;

public class ActiveCodeService implements IActiveCodeService {

	public IActiveCodeDAO activeCodeDAO;

	public void createActiveCode(ActiveCode ac){
		activeCodeDAO.createActiveCode(ac);
	}

	public IActiveCodeDAO getActiveCodeDAO() {
		return activeCodeDAO;
	}

	public void setActiveCodeDAO(IActiveCodeDAO activeCodeDAO) {
		this.activeCodeDAO = activeCodeDAO;
	}
	
}
