package com.chess.common.service.impl;

import java.net.URLEncoder;
import java.util.Map;

import net.sf.json.JSONObject;

import com.chess.common.MD5Service;
import com.chess.common.service.IPayInterService;
import com.chess.common.service.IPayService;

public class PayInterService implements IPayInterService {
	
	private IPayService payService;
	//该接口已废除
	public String payByESurfing(Integer chargeResult,String txId,String chargeId,String payTime,
			String orderSn,String IMSI,String reservedInfo,String sig){
		JSONObject js = new JSONObject();
		js.put("resultCode", -1);
		js.put("resultDesc", "未知其他错误");
//		System.out.println("chargeResult="+chargeResult+"&txId="+txId+"&chargeId="+chargeId
//				+"&payTime="+payTime+"&orderSn="+orderSn+"&IMSI="+IMSI+"&reservedInfo="+reservedInfo+"&sig="+sig);
//		//验证签名
//		Map<String,String> parmMap = new HashMap<String,String>();
//		if(chargeResult==null){
//			return js.toString();
//		}
//		parmMap.put("chargeResult", chargeResult.toString());
//		parmMap.put("txId", txId);
//		parmMap.put("chargeId", chargeId);
//		parmMap.put("payTime", payTime);
//		parmMap.put("orderSn", orderSn);
//		parmMap.put("IMSI", IMSI);
//		//parmMap.put("reservedInfo", reservedInfo);
//		if(reservedInfo==null){
//			parmMap.put("reservedInfo", null);
//		}
//		
//		String back = MD5Service.encryptStringByOrder(parmMap);
//		try{
//			System.out.println(back);
//			back =URLEncoder.encode(back, "UTF-8");
//			System.out.println(back);
//		}catch(Exception ex) {
//			
//			return js.toString();
//		}
//		//E11FB59B4401378AE040007F0100120E
//		String sign = MD5Service.encryptStringLowerSHA(back+"E11FB59B4401378AE040007F0100120E");
//		
//		String testStr ="IMSI460030953157752chargeIdD51A8DBF012AE5C0E040007F01004FFAchargeResult0orderSn20130830154633346050payTime2013-08-30+15%3A46%3A34reservedInfonulltxId9695ba50b7294dd995057de6222c25a7";
//		String testSign = MD5Service.encryptStringLowerSHA(back+"cab1c89c19ebb56f");
//		
//		//用sha1 加密sign.equals(sig)
//		if(true){//验证通过
//			
//			if(chargeResult==0){
//				if(payService.payByESurfing(txId, chargeId, payTime, orderSn, reservedInfo)){//处理成功
//					js.put("resultCode", 0);
//			 		js.put("resultDesc", "充值成功");
//				}else {
//					js.put("resultCode", -2);
//			 		js.put("resultDesc", "充值处理失败");
//				}
//		 	}else{
//		 		js.put("resultCode", -3);
//		 		js.put("resultDesc", "回调计费结果错误，不处理");
//		 	}
//		}else{
//			
//		}
		return js.toString();
	}
	
	
	public String payByESurfing2(Map<String,String> params,String sig){
		JSONObject js = new JSONObject();
		js.put("resultCode", -1);
		js.put("resultDesc", "未知其他错误");
		String testStr = "";
		Map<String,String> testMap =params;
//		for(String ob:testMap.keySet()){
//			testStr+=ob+"="+testMap.get(ob)+"&";
//		}
//		//System.out.println(testStr);
		//验证签名
		String back = MD5Service.encryptStringByOrder(params);
		try{
			//System.out.println(back);
			back =URLEncoder.encode(back, "UTF-8");
			//System.out.println(back);
		}catch(Exception ex) {
			
			return js.toString();
		}
		String flagStr = "";
	
		String sign = MD5Service.encryptStringLowerSHA(back+"cab1c89c19ebb56f");
		//用sha1 加密
		if(sign.equals(sig)||"1".equals(flagStr)){//验证通过
			
			if(Integer.valueOf(params.get("chargeResult"))==0){
				if(payService.payByESurfing(params.get("txId"), params.get("chargeId"), params.get("payTime"), params.get("orderSn"), flagStr)){//处理成功
					js.put("resultCode", 0);
			 		js.put("resultDesc", "充值成功");
				}else {
					js.put("resultCode", -2);
			 		js.put("resultDesc", "充值处理失败");
				}
		 	}else{
		 		js.put("resultCode", -3);
		 		js.put("resultDesc", "回调计费结果错误，不处理");
		 	}
		}else{
			
		}
		return js.toString();
	}

	public IPayService getPayService() {
		return payService;
	}

	public void setPayService(IPayService payService) {
		this.payService = payService;
	}
	
	
	
	
	
	
}
