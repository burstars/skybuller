package com.chess.common.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.chess.common.bean.oss.OssMenu;
import com.chess.common.dao.IOssMenuDAO;
import com.chess.common.service.IOssMenuService;


public class OssMenuService implements IOssMenuService{

	private IOssMenuDAO ossMenuDAO;
	
	public Integer createOssMenu(OssMenu ossMenu) {
		return ossMenuDAO.createOssMenu(ossMenu);
	}

	public Integer deleteOssMenuByID(String ossMenuID) {
		return ossMenuDAO.deleteOssMenuByID(ossMenuID);
	}

	public OssMenu getOssMenuByID(String ossMenuID) {
		return ossMenuDAO.getOssMenuByID(ossMenuID);
	}

	public List<OssMenu> getOssMenuList() {
		return ossMenuDAO.getOssMenuList();
	}

	public Integer updateOssMenu(OssMenu ossMenu) {
		return ossMenuDAO.updateOssMenu(ossMenu);
	}
	
	/**-------工具--------------*/
	public List<OssMenu> createOssMenuTree(List<OssMenu> list){
		List ossMenuList = new ArrayList();
		HashMap hashMap = new HashMap();
		for(OssMenu m: list){
			if(m.getParentMenuID().equals("0")){//一级菜单
				ossMenuList.add(m);
				hashMap.put(m.getOssMenuID(), m);
			}
		}
		
		for(OssMenu m:list){
			Object obj = hashMap.get(m.getParentMenuID());
			if(obj != null){
				OssMenu ossMenu = (OssMenu)obj;
				ossMenu.getChildOssMenu().add(m);
			}
		}
		
		return ossMenuList;
	}



	public void setOssMenuDAO(IOssMenuDAO ossMenuDAO) {
		this.ossMenuDAO = ossMenuDAO;
	}
	

}
