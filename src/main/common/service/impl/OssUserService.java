package com.chess.common.service.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.chess.common.bean.oss.OssUser;
import com.chess.common.dao.IOssUserDAO;
import com.chess.common.service.IOssUserService;



public class OssUserService implements IOssUserService {
	
	private  Map<String,HttpSession> sessionMap = new HashMap<String,HttpSession>();
	private IOssUserDAO ossUserDAO;
	
	public void createOssUser(OssUser ossUser) {
		ossUserDAO.createOssUser(ossUser);
	}
	
	public void updateOssUserPassword(OssUser ossUser) {
		ossUserDAO.updateOssUserPassword(ossUser);
	}

	public void updateOssUserLastLoginInfo(OssUser ossUser) {
		ossUserDAO.updateOssUserLastLoginInfo(ossUser);
	}
	
	public OssUser getOssUserByID(String username) {
		return ossUserDAO.getOssUserByID(username);
	}
	
	public List<OssUser> getOssUserList(){
		return ossUserDAO.getOssUserList();
	}
	
	public Integer deleteOssUserByID(String username){
		return ossUserDAO.deleteOssUserByID(username);
	}
	public Integer updateOssUser(OssUser ossUser){
		return ossUserDAO.updateOssUser(ossUser);
	}

	public IOssUserDAO getOssUserDAO() {
		return ossUserDAO;
	}

	public void setOssUserDAO(IOssUserDAO ossUserDAO) {
		this.ossUserDAO = ossUserDAO;
	}

	public Map<String, HttpSession> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, HttpSession> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
