package com.chess.common.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.bean.LoginLog;
import com.chess.common.bean.LoginLogInfo;
import com.chess.common.bean.User;
import com.chess.common.constant.DBConstant;
import com.chess.common.dao.ILoginLogDAO;
import com.chess.common.framework.db.AsyncDatabaseThread;
import com.chess.common.framework.db.DBOperation;

public class LoginService {
	private ILoginLogDAO loginLogDAO;
	private static Logger logger = LoggerFactory.getLogger(LoginService.class);

	public void createPlayerLoginInRecord(User player) {
		// LoginLog loginLog = loginLogDAO.getPlayerLastLoginLogToday(playerID);
		// if(loginLog!=null&&loginLog.getQuitTime()==null){//异常登录记录，修正下
		// loginLogDAO.updatePlayerLoginOutTime(playerID);
		// }
		if (player.getPlayerID() == null) {
			return;
		}
		LoginLog loginLog = new LoginLog();
		loginLog.setPlayerID(player.getPlayerID());
		loginLog.setLoginTime(DateService.getCurrentUtilDate());
		loginLog.setRegisterTime(player.getRegisTime());
		loginLog.setIp(player.getProxyIp());
		// 设备玩家的名字和索引
		loginLog.setPlayName(player.getPlayerName());
		loginLog.setPlayIndex(player.getPlayerIndex());

		// 设置 ip 和 端口号
		loginLog.setClientIp(player.getClientIP());
		loginLog.setHttpClientPort(player.getHttpClientPort());

		// 设备客户端设备的相关信息
		loginLog.setDeviceBrand(player.getDeviceBrand());
		loginLog.setSystemVersion(player.getSystemVersion());
		loginLog.setPhone(player.getPhone());
		loginLog.setDeviceFlag(player.getDeviceFlag());
		//保存用户登录的地理位置,这里直接使用 ipAddress 存储位置信息
		loginLog.setIpAddress(player.getLocation());
		// 改成数据库处理线程处理
		DBOperation dbmsg = new DBOperation();
		dbmsg.dao = DBConstant.LOGIN_LOG_DAO;
		dbmsg.playerID = player.getPlayerID();
		dbmsg.opertaion = DBConstant.LOGIN_LOG_DAO_CREATE_LOG;
		dbmsg.loginLog = loginLog;

		AsyncDatabaseThread.pushDBMsg(dbmsg);
		// loginLogDAO.createLoginLog(loginLog);
	}

	public void playerLoginOut(User player) {
		if (player.getPlayerID() == null) {
			return;
		}
		Date curTime = DateService.getCurrentUtilDate();
		long time1 = curTime.getTime();
		LoginLog log = loginLogDAO.getPlayerLastLoginLogToday(player
				.getPlayerID());

		if (log != null) {
			// 改成数据库处理线程处理
			DBOperation dbmsg = new DBOperation();
			dbmsg.dao = DBConstant.LOGIN_LOG_DAO;
			dbmsg.opertaion = DBConstant.LOGIN_LOG_DAO_CREATE_UPDATE_LOGIN_OUT_TIME;
			log.setQuitTime(curTime);
			log.setPlayerID(player.getPlayerID());
			dbmsg.loginLog = log;
			AsyncDatabaseThread.pushDBMsg(dbmsg);
			// loginLogDAO.updatePlayerLoginOutTime(log.getLoginLogID(),
			// log.getTableNameSuffix(), curTime);
		} else {// 没有记录的话可能是昨天登录的玩家今天才下线,这个时候修复下
			log = new LoginLog();
			Date todayFristTime = DateService.getCurrentDayFirstUtilDate();
			log.setLoginTime(todayFristTime);
			log.setQuitTime(curTime);
			log.setPlayerID(player.getPlayerID());
			log.setRegisterTime(player.getRegisTime());
			log.setIp(player.getClientIP());
			// 改成数据库处理线程处理
			DBOperation dbmsg = new DBOperation();
			dbmsg.dao = DBConstant.LOGIN_LOG_DAO;
			dbmsg.playerID = player.getPlayerID();
			dbmsg.opertaion = DBConstant.LOGIN_LOG_DAO_CREATE_LOG;
			dbmsg.loginLog = log;
			AsyncDatabaseThread.pushDBMsg(dbmsg);
			// loginLogDAO.createLoginLog(log);
		}
		long time2 = DateService.getCurrentUtilDate().getTime();
		logger.debug("玩家推出记录玩家退出时间" + (time2 - time1));// System.out.println("玩家推出记录玩家退出时间"+(time2-time1));
	}

	/** 昨天登录的玩家一直没下线的情况，处理下 昨天没下线的玩家默认在昨天的23：59:59 统一下线 调度使用 */
	public void repairYesTodayNoLoginOut() {
		loginLogDAO.repairYesTodayNoLoginOut();
	}

	/** 按时间统计玩家的登录情况 */
	public List<LoginLogInfo> countPlayerLoginInfoByTime(Date time,
			int platformType) {
		return loginLogDAO.countPlayerLoginInfoByTime(time, platformType);
	}

	/** 统计在某个时间段注册的玩家在某天登录的去重人数 */
	public Integer countLoginNumByRegisterTime(Date time, Date regStartTime,
			Date regEndTime, int platformType) {
		return loginLogDAO.countLoginNumByRegisterTime(time, regStartTime,
				regEndTime, platformType);
	}

	public ILoginLogDAO getLoginLogDAO() {
		return loginLogDAO;
	}

	public void setLoginLogDAO(ILoginLogDAO loginLogDAO) {
		this.loginLogDAO = loginLogDAO;
	}
}
