package com.chess.common.service.impl;

import java.util.List;

import com.chess.common.bean.oss.OssMenu;
import com.chess.common.bean.oss.OssRoleMenu;
import com.chess.common.dao.IOssRoleMenuDAO;
import com.chess.common.service.IOssRoleMenuService;



public class OssRoleMenuService implements IOssRoleMenuService {

	private IOssRoleMenuDAO ossRoleMenuDAO;
	
	public void createOssRoleMenu(OssRoleMenu ossRoleMenu) {
		ossRoleMenuDAO.createOssRoleMenu(ossRoleMenu);

	}

	public Integer deleteOssRoleMenu(OssRoleMenu ossRoleMenu) {
		return ossRoleMenuDAO.deleteOssRoleMenu(ossRoleMenu);
	}

	public Integer deleteOssRoleMenuByRoleId(Integer roleId) {
		return ossRoleMenuDAO.deleteOssRoleMenuByRoleId(roleId);
	}

	public List<OssMenu> getOssMenuByRoleId(Integer roleId) {
		return ossRoleMenuDAO.getOssMenuByRoleId(roleId);
	}

	public List<OssRoleMenu> getOssRoleMenuList() {
		return ossRoleMenuDAO.getOssRoleMenuList();
	}

	public Integer updateOssRoleMenu(OssRoleMenu ossRoleMenu) {
		return ossRoleMenuDAO.updateOssRoleMenu(ossRoleMenu);
	}


	public void setOssRoleMenuDAO(IOssRoleMenuDAO ossRoleMenuDAO) {
		this.ossRoleMenuDAO = ossRoleMenuDAO;
	}

	public IOssRoleMenuDAO getOssRoleMenuDAO() {
		return ossRoleMenuDAO;
	}

}
