package com.chess.common.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.chess.common.DateService;
import com.chess.common.UUIDGenerator;
import com.chess.common.bean.MallHistory;
import com.chess.common.bean.MallItem;
import com.chess.common.bean.User;
import com.chess.common.bean.unipay.PayCallbackReq;
import com.chess.common.constant.GameConstant;
import com.chess.common.constant.PayConstant;
import com.chess.common.dao.IUserDAO;
import com.chess.common.dao.impl.MallHistoryDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.IPayService;

/**充值处理*/
public class PayService implements IPayService {
	
	private MallHistoryDAO mallHistoryDAO;
	private IUserDAO playerDAO;
	private ICacheUserService playerService;
	private DataSourceTransactionManager transactionManager;
	public Integer getOnlyPayMoneyByTime(String playerID,Integer flagAccess,Integer order, Date beginTime,
			Date endTime, Integer payState) {
		
		Integer num = mallHistoryDAO.getOnlyPayMoneyByTime(playerID,flagAccess,order,beginTime, endTime, payState);
		if(num==null){
			num = 0;
		}
		return num;
	}
	
	 public List<MallHistory> getPayHistoryListByTime(String playerID,Integer flagAccess,
				Integer order,Date beginTime, Date endTime, Integer beginNum,Integer onePageNum, Integer payState){
		 		List<MallHistory> payList = mallHistoryDAO.getPayHistoryListByTime(playerID, flagAccess, order, beginTime, endTime, beginNum, onePageNum, payState);
		 		/*
		 		if(payList!=null&&payList.size()>0)
		 		{
		 			for(PayHistory payHistory:payList)
		 			{
		 				payHistory.decodePayAccount();
		 			}
		 		}*/
		 	return payList;
		}
	 
	 public Integer getPayMoneySumByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, Date endTime){
			return mallHistoryDAO.getPayMoneySumByTime(playerID, flagAccess, order, beginTime, endTime);
		}

	 @SuppressWarnings("unchecked")
	public synchronized Integer addPlayerMoneyBase(User player, Integer money, String orderNo, Integer flagAccess, Date payTime,int buyItemID)
		{
			
			TransactionStatus status = null;

			try {
				
				//事务控制
				DefaultTransactionDefinition td = new DefaultTransactionDefinition();
				status = transactionManager.getTransaction(td);
				
				//是否存在订单号
				boolean orderNoFlag = this.isPayHistoryExistByOrderNo(orderNo);
				if(orderNoFlag){
					return -2;
				}
				
				//增加充值记录
				MallHistory payHistory = new MallHistory();
				payHistory.setPayHistoryId(UUIDGenerator.generatorUUID());
				payHistory.setOrderNo(orderNo);
				payHistory.setPlayerId(player.getPlayerID());
				payHistory.setAmount(money);
				payHistory.setFlagAccess(flagAccess);
				payHistory.setPayTime(payTime);
				payHistory.setState(1);
				payHistory.setBuyItemID(buyItemID);
				mallHistoryDAO.createPayHistory(payHistory,"");
				transactionManager.commit(status);
				return 0;
			} catch (Exception e) {
				transactionManager.rollback(status);
				e.printStackTrace();
				return -5;
			}
		}
	 
	 public boolean isPayHistoryExistByOrderNo(String orderNo) {
			
			return mallHistoryDAO.isPayHistoryExistByOrderNo(orderNo);
		}
	 public  MallHistory getPayHistoryByPayOrderID(String payOrderID){
		 return mallHistoryDAO.getPayHistoryByOrderID(payOrderID);
	 }
	
	 
	 
	 
	 
	 /**联通平台充值接口*/
	public boolean  payByUniPay( PayCallbackReq orderReq){
			boolean isSend = false;
			//检查下支付数量是否正确 
			//查询下记录
			MallHistory payHistory = this.getPayHistoryByPayOrderID(orderReq.getOrderid());
			if(payHistory!=null&&payHistory.getFlagAccess()==PayConstant.HISTORY_PLATFORM_TYPE_UNI_PAY){
				if(payHistory.getState()!=PayConstant.HISTORY_CUR_STATE_SUCCESS){//未实际发放
					if(payHistory.getAmount().toString().equals(orderReq.getPayfee())){ //检查下实际数值是否正确
						// isSend = payService.handlePay(orderReq.getOrderid());
					}else {//
						// TODO  有可能回调时数值与之前生成订单数值不同的话，额外处理
						//isSend = payService.repairPayHand(orderReq.getOrderid(), Integer.valueOf(orderReq.getPayfee()));
					}
				}else { //已发放 防止多次充值回调
					isSend = true;
				}
			 
			}
			return isSend;
	}
	
	/**联通平台验证订单号 接口*/
	public String  payByUniPayValidateOderID( String orderID){
		/*
		 PayHistory payHistory = this.getPayHistoryByPayOrderID(orderID);
		 ValidateOrderIDBack back = new ValidateOrderIDBack();
		 if(payHistory!=null&&(payHistory.getState()==PayConstant.HISTORY_CUR_STATE_NOTICE_REACH||payHistory.getState()==PayConstant.HISTORY_CUR_STATE_INIT_ODERID)){
			 String  script = payHistory.getPayScript();
			 if(script!=null){
				 JSONObject json = JSONObject.fromObject(script);
				 int goodsID = json.getInt("goodsID");
				 int num = json.getInt("num");
				 if(true){
					 back.setCheckOrderIdRsp("0");
					 String appversion = "0.0.2";
					 back.init(payHistory.getPlayerId(), "", "19216800115", "00012243", DateService.getCurrentDateAsString3(), "", appversion);
				 }
				
			 }
		
		 }
		return back.encodeXmlStr();*/
		return "";
	}
	 
	/**天翼空间平台充值接口
	 * @param txid 玩家索引
	 * @param  chargeId 计费代码 或道具编号
	 * */
	public boolean  payByESurfing(String txId,String chargeId,String payTime,
			String orderSn,String flagStr){
		boolean isOk = false;
		Date time = DateService.getCurrentUtilDate();
		try{
			 time = DateService.getDateByStrToTime(payTime);
		}catch(Exception e){
			return isOk;
		}
		//Player player = playerService.getPlayerByPlayerID(reservedInfo);
		User player = playerDAO.getPlayerByPlayerIndex(Integer.valueOf(txId));
		int item_base_id = this.getItemBaseIDByPayCode(PayConstant.HISTORY_PLATFORM_TYPE_E_SURFING, chargeId);
		int flag =PayConstant.HISTORY_PLATFORM_TYPE_E_SURFING;
//		//临时测试用
		if("1".equals(flagStr)){
			flag = PayConstant.HISTORY_PLATFORM_TYPE_TEST;
			item_base_id = Integer.valueOf(chargeId);
		}
//		if(item_base_id==0){
//			item_base_id = Integer.valueOf(chargeId);
//			orderSn = UUIDGenerator.generatorUUID();
//			flag = PayConstant.HISTORY_PLATFORM_TYPE_TEST;
//		}
		MallItem item = playerService.getMallItem(item_base_id);
		if(player==null||(item==null&&item_base_id!=101)){
			return isOk;
		}
		int money = 10;
		if(item!=null){
			 money = item.getPrice()*1;	
		}
		
		//查询下记录
		MallHistory payHistory = this.getPayHistoryByPayOrderID(orderSn);
		if(payHistory!=null&&payHistory.getFlagAccess()==PayConstant.HISTORY_PLATFORM_TYPE_E_SURFING){//已有记录的话，认为充值已成功
			isOk = true;
		}else {
			if(this.addPlayerMoneyBase(player, money, orderSn,flag , time,item_base_id)==0){
				playerService.buy_item_rmb(player.getPlayerID(), item_base_id, 1, GameConstant.OPT_BUY_ITEM,"");
				isOk = true;
			}
		}
		
		return isOk;
	}
	
	public boolean payCallBack(String out_trade_no,String total_fee,String discount) {
		boolean isSend = false;
		//检查下支付数量是否正确 
		//查询下记录
		MallHistory payHistory = this.getPayHistoryByPayOrderID(out_trade_no);
		if(payHistory!=null
				&&(payHistory.getFlagAccess().intValue()==PayConstant.HISTORY_PLATFORM_TYPE_WX_PAY
				||payHistory.getFlagAccess().intValue()==PayConstant.HISTORY_PLATFORM_TYPE_ALIPAY))
		{
			if(payHistory.getState()==PayConstant.HISTORY_CUR_STATE_NOTICE_REACH){//未实际发放
				if(payHistory.getState()==1)
					return false;
				String gameID = GameContext.getPayMapValue(out_trade_no).get("gameID");
				payHistory.setState(1);
				isSend = mallHistoryDAO.updatePayHistory(payHistory,gameID)>0;
				if(isSend)
				{
					playerService.buy_item_rmb(payHistory.getPlayerId(), payHistory.getBuyItemID(), 1, GameConstant.OPT_BUY_ITEM, gameID);
				}
				
			}else { //已发放 防止多次充值回调
				isSend = true;
			}
		}
		
		return isSend;
	}
	
	
	
	/**根据计费点及运营商平台类型获得道具id 暂时先在代码中写死*/
	private int getItemBaseIDByPayCode(int platfomType,String payCode){
		int item_base_id = 0;
		if(platfomType==PayConstant.HISTORY_PLATFORM_TYPE_E_SURFING){
			if(payCode.equals("F7851BFBF01A5D36E0430100007F2514")){
				item_base_id = 5001;
				return item_base_id;
			}
			if(payCode.equals("F7851BFBF01B5D36E0430100007F2514")){
				item_base_id = 5002;
				return item_base_id;
			}
			if(payCode.equals("F7851BFBF01C5D36E0430100007F2514")){
				item_base_id = 5003;
				return item_base_id;
			}
			if(payCode.equals("F7851BFBF01D5D36E0430100007F2514")){
				item_base_id = 5004;
				return item_base_id;
			}
			if(payCode.equals("F7851BFBF01E5D36E0430100007F2514")){
				item_base_id = 5005;
				return item_base_id;
			}
			//钻石
			if(payCode.equals("F7851BFBF01F5D36E0430100007F2514")){
				item_base_id = 3333;
				return item_base_id;
			}
			if(payCode.equals("F7851BFBF0205D36E0430100007F2514")){
				item_base_id = 3334;
				return item_base_id;
			}
			if(payCode.equals("F7851BFBF0215D36E0430100007F2514")){
				item_base_id = 3335;
				return item_base_id;
			}
			if(payCode.equals("F7851BFBF0225D36E0430100007F2514")){
				item_base_id = 3336;
				return item_base_id;
			}
			if(payCode.equals("F7851BFBF0235D36E0430100007F2514")){
				item_base_id = 3337;
				return item_base_id;
			}
			if(payCode.equals("F7854A686F194038E0430100007FED9D")){
				item_base_id = 101;
				return item_base_id;
			}
		}
		
		return item_base_id;
	}
	 
	public MallHistoryDAO getMallHistoryDAO() {
		return mallHistoryDAO;
	}

	public void setMallHistoryDAO(MallHistoryDAO mallHistoryDAO) {
		this.mallHistoryDAO = mallHistoryDAO;
	}

	public DataSourceTransactionManager getTransactionManager() {
		return transactionManager;
	}

	public void setTransactionManager(
			DataSourceTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	public ICacheUserService getPlayerService() {
		return playerService;
	}

	public void setPlayerService(ICacheUserService playerService) {
		this.playerService = playerService;
	}

	public IUserDAO getPlayerDAO() {
		return playerDAO;
	}

	public void setPlayerDAO(IUserDAO playerDAO) {
		this.playerDAO = playerDAO;
	}

	
}
