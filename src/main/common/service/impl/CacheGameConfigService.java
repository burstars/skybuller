package com.chess.common.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.bean.GameConfig;
import com.chess.common.dao.IGameConfigDAO;
import com.chess.common.service.ICacheGameConfigService;

/**
 * 配置麻将玩法信息缓存服务类
 * 
 * @author cuiweiqing 2016-11-02
 * 
 */
public class CacheGameConfigService implements ICacheGameConfigService {
	private Logger logger = LoggerFactory
			.getLogger(CacheGameConfigService.class);
	private IGameConfigDAO gameConfigDAO = null;

	// 游戏配置对象缓存
	private Map<String, GameConfig> gameConfigs = new ConcurrentHashMap<String, GameConfig>();

	public void initAllGameConfigs() {
		// 加载游戏配置基础对象
		this.initGameConfigs();
	}

	// 加载游戏配置基础对象
	private void initGameConfigs() {
		List<GameConfig> configList = gameConfigDAO.getAllGameConfigs();
		for (GameConfig gc : configList) {
			String gameID = gc.getGame_id();
			gameConfigs.put(gameID, gc);
		}
	}

	public GameConfig getGameConfig(String gameId) {
		return this.gameConfigs.get(gameId);
	}

	public IGameConfigDAO getGameConfigDAO() {
		return gameConfigDAO;
	}

	public void setGameConfigDAO(IGameConfigDAO gameConfigDAO) {
		this.gameConfigDAO = gameConfigDAO;
	}

}
