package com.chess.common.service.impl;
  
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.ISchedulerService;



public class SchedulerService implements ISchedulerService {

	private static Logger logger = LoggerFactory.getLogger(SchedulerService.class);
	
	//private ITaskService taskService ;
	private LoginService loginService;
	private StatisticUserService statisticUserService ;
	private ICacheUserService playerService;
	private UserService userService;

	public void handleDayScheduler() {
		logger.error("$$$$>>日定时调度任务执行方法内部：time="+(new Date().toString()));
		
		userService.getPlayerDAO().updateAllPlayerFreeCjAndParam01();
		//staskService.everyFlashPlayerTask();
		loginService.repairYesTodayNoLoginOut();
		Date yesDay = DateService.dateIncreaseByDay(DateService.getCurrentUtilDate(), -1);
		statisticUserService.dataStatisticByTimeAll(yesDay);
		playerService.daySchedClearToDayBestScore();
//		更新俱乐部日排行-lxw20180907
		playerService.updateClubRankState(2, true);
	}
	
	public void handleDay3Scheduler() {
		logger.error("$$$$>>日3定时调度任务执行方法内部：time="+(new Date().toString()));
		// 俱乐部结算茶水费
		playerService.settleClubServiceFee();
	}
	
	/**处理每五分钟调度*/
	public void handleFiveMinutesScheduler() {
	
	
	}
	

	
	
	public void handleHalfHourScheduler(){
		
	}
	
	public void handleHourInHalfScheduler() {
	
		
	}
	/**小时调度*/
	public void handleHourScheduler() {
		
	}
	
	/**
	 * 处理十分钟调度
	 */
	public void handleTenMinutesScheduler() 
	{
		
		
	}
	
	/**一分钟调度A*/
	public void handleOneMinutesScheduler()
	{
		
		
		
	}
	
	/**
	 * 处理一分钟调度B
	 */
	public void handleOneMinutesSchedulerB(){
		
	}
	/**
	 * 处理秒调度
	 * */
	public void  handleSecondsScheduler(){

	}
	
	
	public void handlePetTrainingScheduler(){
		
	}
	
	public void handleBattleGroupScheduler() {
		
	}


	/**
	 * 统一调度处理定时清理的缓存
	 */
	
	public void handleCleanSystemDateCache() {
		
		
	}
	
	/**系统停服调度*/
	public void handleSystemStopScheduler(){
		
	}
//	public ITaskService getTaskService() {
//		return taskService;
//	}
//	public void setTaskService(ITaskService taskService) {
//		this.taskService = taskService;
//	}
	public LoginService getLoginService() {
		return loginService;
	}
	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}
	public StatisticUserService getStatisticUserService() {
		return statisticUserService;
	}
	public void setStatisticUserService(StatisticUserService statisticUserService) {
		this.statisticUserService = statisticUserService;
	}
	public ICacheUserService getPlayerService() {
		return playerService;
	}
	public void setPlayerService(ICacheUserService playerService) {
		this.playerService = playerService;
	}
	public UserService getUserService() {
		return userService;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	public void handleMonthsScheduler() {
		playerService.updateClubRankState(4, false);
		
	}
	public void handleWeeksScheduler() {
		playerService.updateClubRankState(3, false);
		
	}
 
	
	
	
}
