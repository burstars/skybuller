package com.chess.common.service.impl;

import java.util.List;

import com.chess.common.bean.oss.OssUserRole;
import com.chess.common.dao.IOssUserRoleDAO;
import com.chess.common.service.IOssUserRoleService;


public class OssUserRoleService implements IOssUserRoleService{

	private IOssUserRoleDAO ossUserRoleDAO;
	
	public void createOssUserRole(OssUserRole ossUserRole) {
		ossUserRoleDAO.createOssUserRole(ossUserRole);
	}

	public Integer deleteOssUserRoleByRoleId(Integer ossUserRoleID) {
		return ossUserRoleDAO.deleteOssUserRoleByRoleId(ossUserRoleID);
	}

	public List getOssUserByRoleUserID(String username) {
		return ossUserRoleDAO.getOssUserByRoleUserID(username);
	}

	public List getOssUserRoleByRoleID(Integer ossUserRoleID) {
		return ossUserRoleDAO.getOssUserRoleByRoleID(ossUserRoleID);
	}

	public List<OssUserRole> getOssUserRoleList() {
		return ossUserRoleDAO.getOssUserRoleList();
	}

	public Integer updateOssUserRole(OssUserRole ossUserRole) {
		return ossUserRoleDAO.updateOssUserRole(ossUserRole);
	}
	
	/**删除用户的所有角色映射*/
	public Integer deleteOssUserRoleByUserID(String username){
		return ossUserRoleDAO.deleteOssUserRoleByUserID(username);
	}
	
	

	public IOssUserRoleDAO getOssUserRoleDAO() {
		return ossUserRoleDAO;
	}

	public void setOssUserRoleDAO(IOssUserRoleDAO ossUserRoleDAO) {
		this.ossUserRoleDAO = ossUserRoleDAO;
	}

}
