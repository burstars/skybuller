package com.chess.common.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.chess.common.SpringService;
import com.chess.common.bean.SystemConfigPara;
import com.chess.common.bean.alipay.util.Keys;
import com.chess.common.bean.wxpay.WxConstantUtil;
import com.chess.common.constant.ConfigConstant;
import com.chess.common.constant.GameConstant;
import com.chess.common.dao.ISystemParamDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.ISystemConfigService;


public class SystemConfigService implements ISystemConfigService {


    private ISystemParamDAO systemParamDAO;

    private Map<Integer, SystemConfigPara> allpara = new HashMap<Integer, SystemConfigPara>();
    private List<SystemConfigPara> allclientpara = new ArrayList<SystemConfigPara>();
    private int chu_delay_before_ting = 0;
    private int chu_delay_after_ting = 0;
    private int zhidui_support = 0;
    private int show_notice_oncreatevip = 0;
    private int show_notice_onlogin = 0;
    private int show_notice_onlogin_delay = 0;
    private int show_notice_onlogin_showtime = 0;
    private int show_notice_onlogin_fullscreen = 0;
    private int show_notice_onlogin_autoclose = 0;
    private int good_cards = 0;
    private int good_cards_max_count = 0;
    private int good_cards_count_room_owners = 0;
    private int couple_goodcards = 0;
    private int couple_goodcards_max_count = 0;
    private int couple_goodcards_count_room_owners = 0;
    private int couple_hongzhong_count = 4;
    private int bao_at_last_ofcards = -1;
    private int two_people_support = -1;

    public SystemConfigService() {

    }

    public void load_all_para() {
        List<SystemConfigPara> lt = systemParamDAO.getAllConfigPara();
        for (int i = 0; i < lt.size(); i++) {
            SystemConfigPara para = lt.get(i);
            allpara.put(para.getParaID(), para);

            if (para.getIsclient() == 1) {
                allclientpara.add(para);
            }
        }
        wxPara();
        //aliPara();
    }

    private void aliPara() {
        //ali 合作身份者id，以2088开头的16位纯数字
        if (allpara.containsKey(4010)) {
            Keys.DEFAULT_PARTNER = allpara.get(4010).getValueStr();
        }
        //ali 支付宝帐号
        if (allpara.containsKey(4011)) {
            Keys.DEFAULT_SELLER = allpara.get(4011).getValueStr();
        }
        //ali 回调url
        if (allpara.containsKey(4012)) {
            Keys.NOTICY_URL = allpara.get(4012).getValueStr();
        }
    }

    private void wxPara() {
        if (allpara.containsKey(4001)) {
            WxConstantUtil.APP_ID = allpara.get(4001).getValueStr();
        }
        if (allpara.containsKey(4002)) {
            WxConstantUtil.APP_SECRET = allpara.get(4002).getValueStr();
        }
        if (allpara.containsKey(4003)) {
            WxConstantUtil.APP_KEY = allpara.get(4003).getValueStr();
        }
        if (allpara.containsKey(4004)) {
            WxConstantUtil.PARTNER = allpara.get(4004).getValueStr();
        }
        if (allpara.containsKey(4005)) {
            WxConstantUtil.PARTNER_KEY = allpara.get(4005).getValueStr();
        }
        if (allpara.containsKey(4006)) {
            WxConstantUtil.NOTIFY_URL = allpara.get(4006).getValueStr();
        }

    }

    public void updatePara(SystemConfigPara para) {
        if (para != null) {
            SystemConfigPara old = allpara.get(para.getParaID());
            if (old != null) {
                switch (old.getParaID()) {
                    case ConfigConstant.CHU_DELAY_BEFORE_TING: {
                        chu_delay_before_ting = para.getValueInt();
                        break;
                    }
                    case ConfigConstant.CHU_DELAY_AFTER_TING: {
                        chu_delay_after_ting = para.getValueInt();
                        break;
                    }
                    case ConfigConstant.SUPPORT_ZHIDUI: {
                        zhidui_support = para.getValueInt();
                        break;
                    }
                    case ConfigConstant.SHOW_NOTICE_ONCREATEVIP: {
                        show_notice_oncreatevip = para.getValueInt();
                        break;
                    }
                    case ConfigConstant.SHOW_NOTICE_ONLOGIN: {
                        show_notice_onlogin = para.getValueInt();
                        show_notice_onlogin_fullscreen = para.getPro_1();
                        show_notice_onlogin_showtime = para.getPro_2();
                        show_notice_onlogin_delay = para.getPro_3();
                        show_notice_onlogin_autoclose = para.getPro_4();
                        break;
                    }
                    case ConfigConstant.GOOD_CARDS: {
                        good_cards = para.getValueInt();
                        good_cards_max_count = para.getPro_1();
                        good_cards_count_room_owners = para.getPro_2();
                        break;
                    }
                    case ConfigConstant.COUPLE_GOOD_CARDS: {
                        couple_goodcards = para.getValueInt();
                        couple_goodcards_max_count = para.getPro_1();
                        couple_goodcards_count_room_owners = para.getPro_2();
                        break;
                    }
                    case ConfigConstant.COUPLE_HONGZHONG_COUNT: {
                        couple_hongzhong_count = para.getValueInt();
                        break;
                    }
                    case ConfigConstant.TWO_PEOPLE_SUPPORT: {
                        two_people_support = para.getValueInt();
                        break;
                    }
                    case ConfigConstant.BAO_AT_LAST_OFCARDS: {
                        bao_at_last_ofcards = para.getValueInt();
                        break;
                    }

                }

                allpara.put(para.getParaID(), para);

                if (old.getIsclient() == 1) {
                    updateClientPara(para);
                }

                //
                systemParamDAO.updatePara(para);
                ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");
                playerService.updateRoom(para);
            }
        }
    }

    private void updateClientPara(SystemConfigPara para) {
        for (SystemConfigPara p : allclientpara) {
            if ((null != p) && (para.getParaID() == p.getParaID())) {
                allclientpara.remove(p);
                allclientpara.add(para);
                break;
            }
        }
    }

    public int get_robotLimitNum() {
        SystemConfigPara para = allpara.get(ConfigConstant.ROTBOT_LIMITNUM);
        if (para == null)
            return 800;
        return para.getValueInt();
    }

    public int get_robotWaitLimitTime() {
        SystemConfigPara para = allpara.get(ConfigConstant.ROTBOT_TIME_ENTER);
        if (para == null)
            return 8;
        return para.getValueInt();
    }

    public int get_max_save_time() {
        SystemConfigPara para = allpara.get(ConfigConstant.MAX_SAVE_TIME);
        if (para == null)
            return 3;
        return para.getValueInt();
    }

    public int get_max_save_gold_num() {
        SystemConfigPara para = allpara.get(ConfigConstant.HANDSEL_COIN_EACH_TIME);
        if (para == null)
            return 1000;
        return para.getValueInt();
    }


    public int get_max_online_num() {
        SystemConfigPara para = allpara.get(ConfigConstant.DEFAULT_ONLINE_COUNT);
        if (para == null)
            return 5000;
        return para.getValueInt();
    }

    public int get_max_reg_player_num() {
        SystemConfigPara para = allpara.get(ConfigConstant.SINGLE_SERVER_MAX_COUNT);
        if (para == null)
            return 300000;
        return para.getValueInt();
    }

    public int get_new_player_gold_num() {
        SystemConfigPara para = allpara.get(ConfigConstant.COIN_FOR_NEW_PLAYER);
        if (para == null)
            return 600;
        return para.getValueInt();
    }

    public int get_room_price(int idx) {
        return 200;
		/*SystemConfigPara para=allpara.get(GameConfigConstant.DIAMOND_TO_COIN + idx);
		if(para==null){
			return 200;
		}
		return para.getValueInt();*/
    }

    //绑定提示开关
    public int get_neet_bind_phone_num() {
        SystemConfigPara para = allpara.get(ConfigConstant.NEED_BIND_PHONE_NUMBER);
        if (para == null)
            return 0;
        return para.getPro_1();
    }

    //绑定开关
    public int get_neet_bind_phone_num_hint() {
        SystemConfigPara para = allpara.get(ConfigConstant.NEED_BIND_PHONE_NUMBER);
        if (para == null)
            return 0;
        return para.getValueInt();
    }

    //支付开关
    public boolean get_PaySwitch() {
        SystemConfigPara para = allpara.get(ConfigConstant.PAY_SWITCH);
        if (para == null)
            return false;

        return para.getValueInt() > 0;
    }

    //上听之前打牌延迟时间
    public int get_chu_delay_before_ting() {
        if (chu_delay_before_ting == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.CHU_DELAY_BEFORE_TING);
            if (para == null)
                return 300;
            return para.getValueInt();
        } else {
            return chu_delay_before_ting;
        }
    }

    //上听之后打牌延迟时间
    public int get_chu_delay_after_ting() {
        if (chu_delay_after_ting == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.CHU_DELAY_AFTER_TING);
            if (para == null)
                return 600;
            return para.getValueInt();
        } else {
            return chu_delay_after_ting;
        }
    }

    //初级场人气
    public int get_room_of_primary_player_num() {
        SystemConfigPara para = allpara.get(ConfigConstant.PRIMARY_PLAYER_NUM);
        if (para == null)
            return 31560;
        return para.getValueInt();
    }

    //中级场人气
    public int get_room_of_intermediate_player_num() {
        SystemConfigPara para = allpara.get(ConfigConstant.INTERMEDIATE_PLAYER_NUM);
        if (para == null)
            return 10950;
        return para.getValueInt();
    }

    //高级场人气
    public int get_room_of_advanced_player_num() {
        SystemConfigPara para = allpara.get(ConfigConstant.ADVANCED_PLAYER_NUM);
        if (para == null)
            return 5250;
        return para.getValueInt();
    }

    //支对支持
    public int get_zhidui_support() {
        if (zhidui_support == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.SUPPORT_ZHIDUI);
            if (para == null)
                return 0;
            zhidui_support = para.getValueInt();
            GameConstant.ZHI_DUI_SUPPORT = para.getValueInt() == 1;
            return para.getValueInt();
        } else {
            GameConstant.ZHI_DUI_SUPPORT = zhidui_support == 1;
            return zhidui_support;
        }
    }

    //创建房间显示的信息
    public int show_notice_oncreatevip() {
        if (show_notice_oncreatevip == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.SHOW_NOTICE_ONCREATEVIP);
            if (para == null)
                return 0;
            show_notice_oncreatevip = para.getValueInt();
            return para.getValueInt();
        } else {
            return show_notice_oncreatevip;
        }
    }

    //显示公告设置
    public int show_notice_onlogin() {
        if (show_notice_onlogin == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.SHOW_NOTICE_ONLOGIN);
            if (para == null)
                return 0;
            show_notice_onlogin = para.getValueInt();
            return para.getValueInt();
        } else {
            return show_notice_onlogin;
        }
    }

    public int get_show_notice_onlogin_fullscreen() {
        if (show_notice_onlogin_fullscreen == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.SHOW_NOTICE_ONLOGIN);
            if (para == null)
                return 0;
            show_notice_onlogin_fullscreen = para.getPro_1();
            return para.getPro_1();
        } else {
            return show_notice_onlogin_fullscreen;
        }
    }

    public int get_show_notice_onlogin_showtime() {
        if (show_notice_onlogin_showtime == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.SHOW_NOTICE_ONLOGIN);
            if (para == null)
                return 0;
            show_notice_onlogin_showtime = para.getPro_2();
            return para.getPro_2();
        } else {
            return show_notice_onlogin_showtime;
        }
    }

    public int get_show_notice_onlogin_delay() {
        if (show_notice_onlogin_delay == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.SHOW_NOTICE_ONLOGIN);
            if (para == null)
                return 0;
            show_notice_onlogin_delay = para.getPro_3();
            return para.getPro_3();
        } else {
            return show_notice_onlogin_delay;
        }
    }

    public int get_show_notice_onlogin_autoclose() {
        if (show_notice_onlogin_autoclose == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.SHOW_NOTICE_ONLOGIN);
            if (para == null)
                return 0;
            show_notice_onlogin_autoclose = para.getPro_4();
            return para.getPro_3();
        } else {
            return show_notice_onlogin_autoclose;
        }
    }

    //随机发好牌开关
    public int get_good_cards() {
        if (good_cards == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.GOOD_CARDS);
            if (para == null)
                return 0;
            good_cards = para.getValueInt();
            return para.getValueInt();
        } else {
            return good_cards;
        }
    }

    //一桌随机发好牌次数
    //每个人均等，开房人多一把
    public int get_good_cards_max_count() {
        if (good_cards_max_count == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.GOOD_CARDS);
            if (para == null)
                return 0;
            good_cards_max_count = para.getPro_1();
            return para.getPro_1();
        } else {
            return good_cards_max_count;
        }
    }

    public int get_good_cards_count_room_owners(){
        if (good_cards_count_room_owners == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.GOOD_CARDS);
            if (para == null)
                return 0;
            good_cards_count_room_owners = para.getPro_2();
            return para.getPro_2();
        } else {
            return good_cards_count_room_owners;
        }
    }
    //随机发好牌开关
    public int get_couple_goodcards() {
        if (couple_goodcards == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.COUPLE_GOOD_CARDS);
            if (para == null)
                return 0;
            couple_goodcards = para.getValueInt();
            return para.getValueInt();
        } else {
            return couple_goodcards;
        }
    }
    //一桌随机发好牌次数
    //每个人均等，开房人多一把
    public int get_couple_goodcards_max_count() {
        if (couple_goodcards_max_count == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.COUPLE_GOOD_CARDS);
            if (para == null)
                return 0;
            couple_goodcards_max_count = para.getPro_1();
            return para.getPro_1();
        } else {
            return couple_goodcards_max_count;
        }
    }

    public int get_couple_goodcards_count_room_owners() {
        if (couple_goodcards_count_room_owners == 0) {
            SystemConfigPara para = allpara.get(ConfigConstant.COUPLE_GOOD_CARDS);
            if (para == null)
                return 0;
            couple_goodcards_count_room_owners = para.getPro_2();
            return para.getPro_2();
        } else {
            return couple_goodcards_count_room_owners;
        }
    }

    //在牌尾取宝牌或者在牌头取宝牌
    public int get_bao_at_last_ofcards() {
        if (bao_at_last_ofcards == -1) {
            SystemConfigPara para = allpara.get(ConfigConstant.GOOD_CARDS);
            if (para == null)
                return 0;
            bao_at_last_ofcards = para.getValueInt();
            return para.getValueInt();
        } else {
            return bao_at_last_ofcards;
        }
    }

    //二人麻将支持开关
    public int get_two_people_support() {
        if (two_people_support == -1) {
            SystemConfigPara para = allpara.get(ConfigConstant.TWO_PEOPLE_SUPPORT);
            if (para == null)
                return 0;
            two_people_support = para.getValueInt();
            GameConstant.TWO_PERSON_SUPPORT = two_people_support == 1;
            return para.getValueInt();
        } else {
            GameConstant.TWO_PERSON_SUPPORT = two_people_support == 1;
            return two_people_support;

        }
    }

    //游戏开始倒计时时长
	/*public int get_game_start_count_down_seconds()
	{
		SystemConfigPara para=allpara.get(GameConfigConstant.COUNTDOWN);
		if(para==null){
			return 5;
		}
		return para.getValueInt();
	}*/
    //
    private SystemConfigPara getpara_fromdb(int paraID) {
        return systemParamDAO.getPara(paraID);
    }

    public SystemConfigPara getPara(int para_id) {
        SystemConfigPara para = allpara.get(para_id);
        if (para == null) {
            para = getpara_fromdb(para_id);
            allpara.put(para_id, para);
        }
        return para;
    }

    public List<SystemConfigPara> getAllConfigPara() {
        return this.systemParamDAO.getAllConfigPara();
    }

    //客户端变量
    public List<SystemConfigPara> getAllClientConfigPara() {
        return allclientpara;
    }

    public ISystemParamDAO getSystemParamDAO() {
        return systemParamDAO;
    }

    public void setSystemParamDAO(ISystemParamDAO systemParamDAO) {
        this.systemParamDAO = systemParamDAO;
    }
    /**
     * 获取被关闭产品
     * @author 
     * SystemConfigService.java
     * @return
     * String
     */
    public String getClosedProducts() {
        SystemConfigPara para = allpara.get(ConfigConstant.CLOSED_PRODUCTS);
        if (para == null)
            return "";
        if(1 == para.getValueInt())
        	return para.getValueStr()==null?"":para.getValueStr().trim();
        return "";
    }
    /**
     * 获取俱乐部上限
     * @author 
     * SystemConfigService.java
     * @return
     * String
     */
    public int getClubCountMax() {
        SystemConfigPara para = allpara.get(ConfigConstant.CLUB_COUNT);
        if (para == null)
            return 1;
        return para.getValueInt();
    }
}
