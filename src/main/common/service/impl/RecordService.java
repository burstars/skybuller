package com.chess.common.service.impl;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.chess.common.bean.RecordBean;
import com.chess.common.bean.SimpleRecord;
import com.chess.common.bean.User;
import com.chess.common.dao.IRecordDAO;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.struct.other.RecordMsg;
import com.chess.common.msg.struct.other.RecordMsgAck;
import com.chess.common.service.IRecordService;

public class RecordService implements IRecordService {

	public IRecordDAO recordDao;

	public void setRecordDAO(IRecordDAO recordDao) {
		this.recordDao = recordDao;
	}

	public void selectRecordById(RecordMsg rma, User pl) {
		List<SimpleRecord> roomIds = recordDao.getMyVipRoomRecord(null, pl.getPlayerID(), rma.gameId);
		
		RecordMsgAck msgAck = new RecordMsgAck();
		msgAck.zjlist = roomIds;

		// 将消息发送给客户端
		GameContext.gameSocket.send(pl.getSession(), msgAck);
	}
	
	/**
	 * 创建一条战绩记录
	 * @param rb
	 */
	public void createRocord(RecordBean rb){
		//rb.setHost_Name(filterEmojiUserName(rb.getHost_Name()));
		
		//rb.setEnd_Way(filterEmojiUserName(rb.getEnd_Way()));
		recordDao.createVipRoomRecord(rb); 
	}

	/**
	 * 更新战绩记录   结束时间    结束方式
	 * @param rb
	 */
	public void updateRecordEndWay(RecordBean rb){
		recordDao.updateRecordEndWay(rb);
	}
	
	
	public void updateRecordScore(RecordBean rb){
		recordDao.updateRecordScore(rb);
	}
}
