package com.chess.common.service;

import java.util.List;

import com.chess.common.bean.User;
import com.chess.common.bean.UserBackpack;
import com.chess.core.net.msg.MsgBase;

public interface IUserService {
	
	public void createPlayer(User pl);
	
	 public List<User> getAllPlayer();
	 
	 public User getPlayerByID (String id);
	 
	 public User getPlayerByAccount(String account);
	 
	 public User getPlayerByPlayerName(String playerName);
	 
	 public List<User> getAllPlayerByPage(String startRownum,String endRownum, String startTime, String endTime, int order, int queryPlayerType);
	 
	 public int getTotalCount(String startTime, String endTime, int queryPlayerType);
	 
	 public List<UserBackpack> getPlayerItemByID(String id);
	 
	 public void updatePlayerItemNum(String id,Integer itemBaseID,int itemNum) ;
	 
	 public void createPlayerItem(UserBackpack item);
	 
	 public User getPlayerByPlayerIndex(int index);

	public int getCustomTotalCount(String account);

	public List<User> getAllCustomPlayerByPage(String startRownum,
			String endRownum, String account);
	
	/**更新玩家金币*/
	public void updatePlayerGold(String playerID, int gold);
	
	public List<User> getPlayersByPlayerIDByPage(String startRownum, String endRownum,String account, String startTime, String endTime, int order);
	
	public int getPlayersByPlayerIDCount(String account, String startTime, String endTime);
	
	public List<User> getPlayersByPlayerAccountByPage(String startRownum, String endRownum,String account, String startTime, String endTime, int order);
	
	public int getPlayersByPlayerAccountCount(String account, String startTime, String endTime);
	
	public List<User> getPlayersByPlayerNameByPage(String startRownum, String endRownum,String account, String startTime, String endTime, int order);
	
	public int getPlayersByPlayerNameCount(String account, String startTime, String endTime);
}
