package com.chess.common.service;

import com.chess.common.bean.EntranceUser;
import com.chess.common.bean.SensitiveWords;

public interface IEntranceUserService {
	
 
	public void init();
	 public EntranceUser getUserByAccount (String account);
	 public SensitiveWords getSensitiveWords (String word);
	 
	 /**根据昵称查找用户*/
	 public EntranceUser getUserByNickName (String nickName);
	 public EntranceUser getUserByMachineCode (String machineCode);
	 public EntranceUser getUserByQQOpenID(String openid);
	 public EntranceUser getUserByWXOpenID(String openid);
	 public EntranceUser getUserByWXUnionID(String unionid);//cc modify 2017-12-5
	 public void addUser(EntranceUser user);
	 
}
