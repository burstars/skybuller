package com.chess.common.service;

import com.chess.common.bean.RecordBean;
import com.chess.common.bean.User;
import com.chess.common.msg.struct.other.RecordMsg;




public interface IRecordService {
	
	/**
	 * 根据用户id查询战绩
	 * @param rma
	 * @param pl
	 */
	public void selectRecordById(RecordMsg rma,User pl);
	/**
	 * 创建一条战绩记录
	 * @param rb
	 */
	public void createRocord(RecordBean rb);
	
	/**
	 * 更新战绩记录     比分    结束时间    结束方式
	 * @param rb
	 */
	public void updateRecordEndWay(RecordBean rb);
	/**
	 * 跟新玩家战绩分数
	 * @param rb
	 */
	public void updateRecordScore(RecordBean rb);
}
