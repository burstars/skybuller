package com.chess.common.service;

public interface ISchedulerService {

	/**
	 * 处理每天调度
	 */
	public void handleDayScheduler();
	
	/**
	 * 处理每天3点调度
	 */
	public void handleDay3Scheduler();
	
	/**
	 * 处理每五分钟调度
	 */
	public void handleFiveMinutesScheduler();
	
	/**
	 * 处理半小时调度
	 */
	public void handleHalfHourScheduler();
	
	/**
	 * 处理一个小时调度（在每个小时的30分钟的时候）
	 */
	public void handleHourInHalfScheduler();
	
	/**
	 * 处理一个小时调度（在每个小时的整点的时候）
	 */
	public void handleHourScheduler();
	
	/**
	 * 处理十分钟调度
	 */
	public void handleTenMinutesScheduler();
	
	/**
	 * 处理一分钟调度
	 */
	public void handleOneMinutesScheduler();
	
	/**
	 * 处理秒调度
	 * */
	public void  handleSecondsScheduler();
	
	
	
	/**系统停服调度*/
	public void handleSystemStopScheduler();
	
	/**
	 * 处理一分钟调度B
	 */
	public void handleOneMinutesSchedulerB();
	/**
	 * 处理周调度
	 * */
	public void  handleWeeksScheduler();
	/**
	 * 处理月调度
	 * */
	public void  handleMonthsScheduler();
}
