package com.chess.common.service;

import java.util.List;

import org.apache.mina.core.session.IoSession;

import com.chess.common.bean.ClubTemplate;
import com.chess.common.bean.MallItem;
import com.chess.common.bean.NormalGameRecord;
import com.chess.common.bean.PlayerHuType;
import com.chess.common.bean.ProxyVipRoomRecord;
import com.chess.common.bean.Reward;
import com.chess.common.bean.ShopGoods;
import com.chess.common.bean.SystemConfigPara;
import com.chess.common.bean.SystemNotice;
import com.chess.common.bean.User;
import com.chess.common.bean.UserBackpack;
import com.chess.common.bean.UserFriend;
import com.chess.common.bean.VipGameRecord;
import com.chess.common.bean.VipRoomRecord;
import com.chess.common.dao.IVipRoomRecordDAO;
import com.chess.common.msg.struct.club.ClubChargeLevelMsg;
import com.chess.common.msg.struct.club.ClubChongZhiMsg;
import com.chess.common.msg.struct.club.ClubRechargeMsg;
import com.chess.common.msg.struct.club.CreateClubMsg;
import com.chess.common.msg.struct.club.FreeTableManagerMsg;
import com.chess.common.msg.struct.club.GetClubsMsg;
import com.chess.common.msg.struct.club.GetQyqRankListMsg;
import com.chess.common.msg.struct.club.GetQyqWinnerListMsg;
import com.chess.common.msg.struct.club.GetSingleClubInfoMsg;
import com.chess.common.msg.struct.club.GetSingleClubInfoMsgAck;
import com.chess.common.msg.struct.club.JoinClubMsg;
import com.chess.common.msg.struct.entranc.EntranceUserMsgAck;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomAckMsg;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomMsg;
import com.chess.common.msg.struct.friend.UpdateFriendRemarkMsg;
import com.chess.common.msg.struct.login.Login2Msg;
import com.chess.common.msg.struct.login.LoginMsg;
import com.chess.common.msg.struct.login.ReconnectMsg;
import com.chess.common.msg.struct.login.RegisterPlayerMsg;
import com.chess.common.msg.struct.luckydraw.LuckyDrawMsg;
import com.chess.common.msg.struct.message.RequestCompletePhoneNumber;
import com.chess.common.msg.struct.other.AskRecoverGameMsg;
import com.chess.common.msg.struct.other.AskRecoverPlayerStateMsg;
import com.chess.common.msg.struct.other.CheckOfflineBackMsg;
import com.chess.common.msg.struct.other.CreateVipRoomMsg;
import com.chess.common.msg.struct.other.EnterVipRoomMsg;
import com.chess.common.msg.struct.other.GameReadyMsg;
import com.chess.common.msg.struct.other.GameUpdateMsg;
import com.chess.common.msg.struct.other.HuDongBiaoQingMsg;
import com.chess.common.msg.struct.other.PlayerAskCloseVipRoomMsg;
import com.chess.common.msg.struct.other.PlayerGameOpertaionMsg;
import com.chess.common.msg.struct.other.PlayerGameOverMsg;
import com.chess.common.msg.struct.other.PlayerOpertaionMsg;
import com.chess.common.msg.struct.other.PlayerTableOperationMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomCloseMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomRecordMsg;
import com.chess.common.msg.struct.other.RequestStartGameMsg;
import com.chess.common.msg.struct.other.SearchUserByIndexMsg;
import com.chess.common.msg.struct.other.TalkingInGameMsg;
import com.chess.common.msg.struct.other.UserExtendMsg;
import com.chess.common.msg.struct.other.VipGameRecordMsg;
import com.chess.common.msg.struct.other.VipRoomRecordMsg;
import com.chess.common.msg.struct.other.WelFareMsg;
import com.chess.common.msg.struct.pay.GameBuyItemCompleteMsg;
import com.chess.common.msg.struct.pay.GameBuyItemMsg;
import com.chess.common.msg.struct.pay.GameIPABuyItemCompleteMsg;
import com.chess.common.msg.struct.playeropt.ValidateMsgAck;
import com.chess.common.msg.struct.system.MobileCodeMsg;
import com.chess.common.msg.struct.system.ScrollMsg;
import com.chess.core.net.msg.MsgBase;
import com.chess.nndzz.msg.struct.other.HuDongBiaoQingMsgNNDZZ;
import com.chess.nndzz.table.GameTable;
import com.chess.nndzz.table.TableLogicProcess;

 
public interface ICacheUserService 
{
	//游戏循环
	public void run_one_loop();
 
	//修改游戏房间配置 
	public void updateRoom(SystemConfigPara cfg);
	
	//返回一个基础道具列表，不包括金币包
	public List<MallItem> getPropertyItemBaseList();
	//返回一个基础道具列表，包括全部
	public List<MallItem> getAllItemBaseTable();
	//
	public void refreshItemBaseTable();
	//
	public boolean use_fangka(User pl,int itemBaseID, int roomType,int num);
	public void min2Nodify(MsgBase msg,User pl);
	//
	public  void createPlayerAck(EntranceUserMsgAck msg);
	//
	public String modify_base_item(MallItem base_item);
	public String add_base_item(MallItem base_item);
	//
	public MallItem getMallItem(int item_base_id);
	//
	public void reconnect(ReconnectMsg msg,IoSession session);
	public void regNewPlayer(RegisterPlayerMsg msg,IoSession session);
	//
	public void load_base_tables();
	//
	//public void run_db_thread();
	//public void run_pay_thread();
	//
	public void initAsyncThread();
	public void udpate_player_item_num(String playerID,int itemBaseID,int num);
	 //
	public void createPlayerLog(String playerID,int playerIndex, String playerName,int playerGold,int operationType,int operationSubType, int moneyRelated,String detail,int moneyType,String gameId);
	
	public List<Reward> getRewardLists();

	public void setRewardLists(List<Reward> rewardLists);
	
	public void login( LoginMsg msg,IoSession session);
	
	public void login2(Login2Msg msg, IoSession session);
	//
	public void scroll_msg_send(IoSession session);
	//
 
	//
	public void sendSystemScrollMsg(String str,int loops,int should_clear, int playerType);
	
	/**添加定时跑马灯消息*/
	public void addOntimeSystemScrollMsg(String str, int loops, int should_clear, String sendTime,
			boolean clearOntimeList, int sendPlayerType, int ontimeType, int sendTimes);
	
	/**发送定时跑马灯消息*/
	public void sendOnTimeSystemScrollMsg();
	
	/**发送一小时频率定时跑马灯消息*/
	public void sendOneHourOnTimeSystemScrollMsg();
	//刷新玩家物品
	public void load_player_items(User pl);
	//下线
	public void playerOffline( String playerID);
	//半分钟调度
	public void schedule_10_minute();
	//开始游戏
	public void enter_room(RequestStartGameMsg msg,IoSession session);
	//请求断线恢复
	public void ask_recover(AskRecoverGameMsg msg, IoSession session);
	//断线重连请求恢复玩家状态
	public void ask_recover_state(AskRecoverPlayerStateMsg msg, IoSession session);
 
	public void sub_player_gold(User pl,int num,int logType,String remark);
	public void add_player_gold(User pl,int num,int logType,String remark);

	//检查重连
	public void check_reroom(CheckOfflineBackMsg msg, IoSession session);
	
	public void updatePlayerRecord(User pl);
	
	//是否在线
	public boolean containsPlayer(String playerID);
	
	/**更改用户资料*/
	public void updatePlayer(User pl,PlayerOpertaionMsg msg,ValidateMsgAck ack); 
	
	/**更新玩家帐号和密码*/
	public void updatePlayerAccountAndPassword(User pl, PlayerOpertaionMsg msg, ValidateMsgAck ack);
	
	/**是否可以添加为好友设置*/
	public void updatePlayerCanFriend(User pl,PlayerOpertaionMsg msg);
	
	/** 更新玩家类别 */
	public void updatePlayerType(String playerID, int playerType);
	
	/**更新玩家位置信息*/
	public void updatePlayerCityName(String playerID, String cityName);
	
	/**更新玩家手机号码*/
	public void updatePlayerPhoneNumber(String playerID, String phoneNumber);

	public void updatePlayerIdentifyCard(String playerID, String phoneNumber,
										 String identifyCard, String weixinNum, String qqNum, String name,
										 String birthday, String address);
	/**
	 * 添加好友
	 * @param User  操作玩家
	 * @param friendPlayID 添加的好友PlayerID
	 * */
	public void AddPlayerFriend(User pl,String friendPlayID);
	
	/**
	 * 删除好友
	 * @param User  操作玩家
	 * @param friendPlayID 添加的好友PlayerID
	 * */
	public void DelPlayerFriend(User pl,String friendPlayID);
	
	/**同意加为好友*/
	public void agreeBecomeFriendApply(User pl, String friendPlayID);
	
	/**拒绝好友申请*/
	public void rejectBecomeFriendApply(User pl, String friendPlayID);
	
	/**
	 * 查询好友（索引）
	 * */
	public List<UserFriend> GetPlayerFriendsByPlayerIndex(int playIndex);
	
	/**
	 * 查询好友（昵称）
	 * */
	public List<UserFriend> GetPlayerFriendsByPlayerName(String playerName);
	
	/**
	 * 申请加入俱乐部
	 * */
	public void applyJoinClub(User pl,String clubCode);
	/**
	 * 俱乐部 添加成员
	 * */
	public void AddClubMember(User pl, String memberId,String clubCode);
	/**
	 * 俱乐部 拒绝添加成员
	 * */
	public void IgnoreClubMember(User pl, String memberId,String clubCode);
	/**
	 * 俱乐部 删除成员
	 * */
	public void DelClubMember(User pl, String memberId,String clubCode);
	/**
	 * 俱乐部 退出俱乐部
	 * */
	public void QuitClubMember(User pl, String memberId,String clubCode);
	/**
	 * 俱乐部 删除所有成员
	 * */
	public void DelAllClubMember(String clubCode);
	/**
	 * 俱乐部 更新成员状态
	 * */
	public void UpdateClubMemberApplyState(User pl, String memberId,String clubCode,int applyState);
	/**
	 * 俱乐部 更新成员备注
	 * */
	public void UpdateClubMemberRemark(User pl, String memberId,String clubCode,String remark);
	/**
	 * 俱乐部 成员列表
	 * */
	public void GetClubMemberList(User pl,String clubCode,int applyState);
	
	public TableLogicProcess getTableLogicProcess();
	//给玩家创建一个道具
	public UserBackpack createItemForPlayer(User pl,int itemBaseID,String itemName,int logSubType,int num,String gameID);
	//使用道具
	public boolean useItem(User pl, int itemBaseID , int count) ;
	//客户端通知服务器，得分之类
	public void gameUpdate(GameUpdateMsg msg,IoSession session);
	
	//从缓存取出用户信息
	public User getPlayerByPlayerIDFromCache(String playerID);
	
	public User getPlayerByPlayerID(String playerID);
	
	///**游戏结束，***/
	public void playerGameOver(PlayerGameOverMsg msg,IoSession session);
	
	//玩家简单游戏行为
	public void playerGameOperation(PlayerGameOpertaionMsg msg,IoSession session);
 
	/**抽奖函数*/
	public void luckyDraw(IoSession session,LuckyDrawMsg msg);
 
	
	public void sendPrizeListToCS(IoSession session);
	
	
	/**更新玩家任务数据*/
	//public void updatePlayerExtByTaskID(String playerID,Integer taskID,Integer addNum);
 
	/**随机获取一个进阶场任务*/
	//public PlayerTask getRandomPlayerTask(String playerID);
	
	/**获取进阶场随机任务加成分数**/
	//public  Integer   checkPlayerTaskIsFinish(String playerID,PlayerTask playerTask);
	
	/**玩家每日的最高得分清零*/
	public void daySchedClearToDayBestScore();

	/**记录当前在线人数，5分钟一调度*/
	public void onlinePlayerNums();

	/**人民币购买 礼包或其他 接口*/
	public boolean buy_item_rmb(String playerID,int itemID,int itemNum,int opertaionID,String gameID);
	
	
	public void playerOperation(PlayerTableOperationMsg msg,User pl);
	
	public void createVipRoom(CreateVipRoomMsg msg,User pl);
	public void enterVipRoom(EnterVipRoomMsg msg,User pl);
	
	/**获取玩家指定VIP房间中所有游戏记录*/
	public void getVipGameRecord(VipGameRecordMsg vgrm, IoSession session);
	/**获取玩家所有VIP房间记录*/
	public void getVipRoomRecord(VipRoomRecordMsg vrrm, IoSession session);
	
	public IVipRoomRecordDAO getVipRoomRecordDAO();
	/**获取所有代开VIP房间记录 2017-9-26*/
	public void getProxyVipRoomRecord(ProxyVipRoomRecordMsg vrrm, IoSession session);
	public void createProxyVipRoomRecord(ProxyVipRoomRecord vrrm);
	public void updateProxyVipRoomRecord(ProxyVipRoomRecord vrrm);
	public void proxyCloseVipRoomRecord(ProxyVipRoomCloseMsg pvrcm, IoSession session);
	
	/**是否存在玩家你昵称的玩家*/
	//public void validatePlayerName(String playerName);
	
	/**修改密码*/
	public void updatePlayerPassword(User pl,String oldpass,String newpass);

	public void createVipRoomRecord(VipRoomRecord vrrm);
	public void updateVipRoomRecord(VipRoomRecord vrrm);
	public void createVipGameRecord(VipGameRecord vgr);
	
	public void createNormalGameRecord(NormalGameRecord normalGameRecord);
	public List<NormalGameRecord> getNormalGameRecordByDate(String date, String player_index);
	
	public void inviteFriendToVipRoom(InviteFriendEnterVipRoomMsg msg,User pl);
	//--> TODO : 玩家不同意邀请 add by  2016.9.13
	public void beInvitedAck(InviteFriendEnterVipRoomAckMsg msg, User pl);
	
	
	public void generateOrder(GameBuyItemMsg gbim,User player);
	
	/**第三方接口支付完成*/
	public void completePay(User pl,GameBuyItemCompleteMsg msg);
	
	/**iosIPA接口支付完成*/
	public void ipaCompletePay(User pl,GameIPABuyItemCompleteMsg ipacomplete);
	
	/**支付完成处理*/
	public void completePayDeal(User pl,String orderno,boolean isok);
	
	//清除大于2小时的缓存
	public void clearPlayerCatch();
	
	//关掉老连接
	public void clearOldSession();
	
	/**
	 * 通知所有的好友更改金币信息
	 * */
	public void refleshFrend(User pl);

	//当前区在线人数
	public int getCacheOnlineNum();
	
	//2人VIP游戏人数
	public int getCacheTwoOnline();
	//设备道具列表
	public List<MallItem> getItemBaseByPhone(User pl,List<MallItem> allBaseList);

	//创建道具库存
	public void createItemForAdmin(User pl, UserBackpack pi, int old_num, String adminName);

	//更改道具库
	public void updateItemForAdmin(User pl, UserBackpack pi, int old_num, String adminName);

	/**消除卡桌玩家，清理玩家信息*/
	public void clearPlayerStaion(User pl);
	
	/**
	 * VIP房卡桌清理
	 * @param vipTableID
	 */
	public void clearVipTableStation(String vipTableID , String gameId);
	
	/**游戏中发送聊天信息*/
	public void playerTalkingInGame(User pl, TalkingInGameMsg msg);
	
	/**互动表情消息*/
	public void playerSendHuDongBiaoQing(User pl, HuDongBiaoQingMsg msg);
	
	/**修改好友的备注名称*/
	public void updatePlayerRemark(User pl, UpdateFriendRemarkMsg msg);
	
	/**获取游戏桌子信息*/
	public List<GameTable> getPlayingTable(int index);
	
	/**强制结束一个桌子*/
	public void clearOneTable(String table_id);
	
	/** 发送请求获取手机注册验证码 */
	public void sendMobileCodeRequest(MobileCodeMsg mcm, IoSession session);

	/** 清除限制注册手机号列表 */
	public void clearLimitedPhone();
	
	/**绑定手机号码*/
	public boolean completeMoblieNumber(User pl, RequestCompletePhoneNumber msg);
	
	/**获取系统公告*/
	public SystemNotice getSysNotice();
	
	public List<SystemNotice> getSysNoticeList();
	
	/**更新系统公告*/
	public void updaeSystemNotice(SystemNotice notice);

	/**获取待发送跑马灯信息*/
	public List<ScrollMsg> getOnTimeScrollMsgs();
	
	/**网页请求扣除玩家金币*/
	public boolean requestSubPlayerGoldByWeb(int playerIndex, int subGold);
	
	//玩家申请解散房间
	public void userCloseVipRoom(PlayerAskCloseVipRoomMsg msg, User pl);


	public List<PlayerHuType> getPlayerHuTypesByPlayerID(String playerID);


	
	public IMin2Nodify getMin2Nodify();

	public void setMin2Nodify(IMin2Nodify min2Nodify) ;
	/**开局准备  20170209*/
	public void gameReady(GameReadyMsg msg, IoSession session);
	
	public void createSendCardLog(User pl, int i, int cardNum,
			int sendCardWechatShare);
	
	public void addItemForPlayer(String playerIndexStr, String itemNumStr,
			int itemType,String gameID);

	public UserBackpack getAllItemByPlayerId(String string, String playerID,
			int baseId);
	
	public void setProxyOpenRoomForPlayer(String playerIndexStr, String itemNumStr);
	
	public Boolean costPlayerCredits(String playerID, int credits);

	public void UpdateShowGoods(ShopGoods sg);

	public void insertShopGood(ShopGoods shopGoods);

	public void updateGoodState(String goodId, int state);

	public void UpdateShowGoodsExceptPicture(ShopGoods shopGoods);
	
	public void setForeverMsg(int isOpen, String msgContent, int msgId);
	
	public void updateNoticeCache(SystemNotice notice);

	public SystemNotice getForeverMsg();
	
	//------------------电子庄牛牛------------------
	public void createVipRoom(com.chess.nndzz.msg.struct.other.CreateVipRoomMsg msg,User pl);
	/**加入房间nndzz*/
	public void enterVipRoom(com.chess.nndzz.msg.struct.other.EnterVipRoomMsg msg,User pl);
	/**开始游戏nndzz*/
	public void enter_room(com.chess.nndzz.msg.struct.other.RequestStartGameMsg msg,IoSession session);	
	/**玩家操作nndzz*/
	public void playerOperation(com.chess.nndzz.msg.struct.other.PlayerTableOperationMsg msg,User pl);
	/**nndzz的准备方法**/
	public void gameReadyNNDZZ(
			com.chess.nndzz.msg.struct.other.GameReadyMsg gameRedayMsgGuandan,
			IoSession session);
	/*** 换座 nndzz  ***/
	public void changeSiteNNDZZ(com.chess.nndzz.msg.struct.other.GameSiteDownMsg gameSiteDownMsgGuandan,
			IoSession session);
	/**互动表情消息 -- nndzz*/
	public void playerSendNNDZZHuDongBiaoQing(User pl, HuDongBiaoQingMsgNNDZZ msg);
	
	/**大转盘*/
	public void welFare(User pl,WelFareMsg msg);
	
	/**修改玩家俱乐部权限  2018-08-28**/
	public boolean updateClubType(int playerIndex , int clubType);
	/**玩家俱乐部状态修改  2018-08-29**/
	public boolean updateClubState(int clubCode ,int clubState,int playerIndex);
	
//	获取俱乐部玩家战绩
	public void getQyqRoomRecord(GetQyqWinnerListMsg vrrm,IoSession session);
//	更新俱乐部已读状态
	public void updateRecordReadState(List<String> recordIds,String gameID, IoSession session);
	/**新建俱乐部*/
	public void createClub(User pl,CreateClubMsg msg);
	/**
	 * @description 获取单个俱乐部的信息
	 * /
	 * @param pl
	 * @param infoMsg 消息
	 * @param isSendAll 是否给所有人发消息
	 * @param isUpdate 是否更新
	 */
	public void getSingleClubInfo(User pl, GetSingleClubInfoMsg infoMsg,boolean isSendAll,int isUpdate);
	/**
	 * @description 根据牌桌对象，创建一个模板牌桌对象
	 * /
	 * @param gt
	 */
	public void createClubTemplateTable(GameTable gt);
	/**
	 * @description 给俱乐部的所有玩家发送消息
	 * /
	 * @param clubCode 俱乐部编码
	 * @param msg 要发送的消息
	 */
	public void sendMsgToClubPlayer(int clubCode,MsgBase msg);
	/**
	 * @description 根据俱乐部的编号，构造一个返回给前台的消息
	 * /
	 * @param clubCode 俱乐部编码
	 * @return
	 */
	public GetSingleClubInfoMsgAck newGetSingleClubInfoMsgAck(int clubCode);
	//	插入系统消息
	public void createSystemNotice(SystemNotice notice);
	
	public void deleteSystemNotice(String type);
	
	/**
	 * 更新俱乐部模板
	 */
	public void updateClubTemplate(User pl,FreeTableManagerMsg msg);
	
	/**
	 * 删除俱乐部模板
	 */
	public void deleteClubTemplate(int id);
	
	/**
	 * 获取俱乐部下所有模板
	 * @return
	 */
	public List<ClubTemplate> getClubTemplateAll(int clubCode);
	
	/**查询俱乐部 */
	public void queryClubs(User pl,GetClubsMsg msg);
	/**加入俱乐部 */
	public void joinClub(User pl,JoinClubMsg msg);
	/**俱乐部充值  */
	public void rechargeClub(User pl,ClubRechargeMsg msg);
	
	public void changeClubLevel(User pl,ClubChargeLevelMsg msg);
	
	public void updateClubPlayerGold(User pl,ClubChongZhiMsg msg);
//	查询俱乐部排行
	public void getClubRankingList (GetQyqRankListMsg msg,IoSession session);
	/**
	 * @description 获取俱乐部的牌桌数
	 * @param clubNo
	 * @return 固定牌桌个数+在玩牌桌个数+私密牌桌个数
	 */
	public int getClubTableCount(int clubNo);
//	定时调度，更新俱乐部排名状态（日、周、月）
//	isToday:日调度特殊处理
	public void updateClubRankState(int state,boolean isToday);
	
	void addClubPlayerGold(User pl, int clubCode, int goldNum);
	
	void addClubGold(int clubCode, int goldNum);
	
	// 定时调度结算所有俱乐部茶水费
	public void settleClubServiceFee();
	
	/**绑定推荐人*/
	public void setExtendPlayer(User pl,UserExtendMsg msg);
	
	public void searchUserByIndex(User pl, SearchUserByIndexMsg msg);
	
}
