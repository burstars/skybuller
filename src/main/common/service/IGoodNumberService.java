package com.chess.common.service;

import java.util.List;

import com.chess.common.bean.GoodNumber;


public interface IGoodNumberService {
	public void createGoodNumber(GoodNumber goodNumber);
	
	public void updateGoodNumber(GoodNumber goodNumber);
	
	public int getAllGoodNumberTotal();
	public List<GoodNumber> getAllGoodNumber(String start,String end);
	
	public int getGoodNumberByNumberTotal(String number);
	public int getGoodNumberTotal(String number);
	public GoodNumber getAnGoodNumberByNumber(String number);
	public List<GoodNumber> getGoodNumberByNumber(String number,String begin,String end);
	
	public GoodNumber getGoodNumberByPlayerId(String playerId);
	
	public void deleteGoodNumber(int id);
}
