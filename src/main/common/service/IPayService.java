package com.chess.common.service;

import java.util.Date;
import java.util.List;

import com.chess.common.bean.MallHistory;
import com.chess.common.bean.User;
import com.chess.common.bean.unipay.PayCallbackReq;

public interface IPayService {
	/**
	 * 获取指定时间玩家的充值记录数
	 * @param playerID
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public Integer getOnlyPayMoneyByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, Date endTime, Integer payState);
	
	
	/**
	 * 分页获取指定时间指定类型玩家的充值
	 * @param playerID
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public List<MallHistory> getPayHistoryListByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, Date endTime, Integer beginNum,Integer onePageNum,Integer payState);
	
	/**
	 * 获取指定时间指定类型的玩家充值或赠送的云币总数
	 * @param playerID
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public Integer getPayMoneySumByTime(String playerID,Integer flagAccess,Integer order,Date beginTime, Date endTime);
	
	/**
	 * 添加记录
	 * */
	public Integer addPlayerMoneyBase(User player, Integer money, String orderNo, Integer flagAccess, Date payTime,int buyItemID);
	
	/**联通平台充值接口*/
	public boolean  payByUniPay( PayCallbackReq orderReq);
	/**联通平台验证订单号 接口*/
	public String  payByUniPayValidateOderID( String orderID);
	
	/**天翼空间平台充值接口*/
	public boolean  payByESurfing(String txId,String chargeId,String payTime,
			String orderSn,String reservedInfo);
	
	/**充值成功处理接口*/
	public boolean payCallBack(String out_trade_no,String total_fee,String discount);
	
}
