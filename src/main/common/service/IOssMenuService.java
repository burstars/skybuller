package com.chess.common.service;

import java.util.List;

import com.chess.common.bean.oss.OssMenu;



public interface IOssMenuService {
	/**创建新菜单*/
	public Integer createOssMenu(OssMenu ossMenu);

	/**修改菜单*/
	public Integer updateOssMenu(OssMenu ossMenu);

	/**删除菜单*/
	public Integer deleteOssMenuByID(String ossMenuID);

	/**获取指定菜单*/
	public OssMenu getOssMenuByID(String ossMenuID);

	/**菜单列表*/
	public List<OssMenu> getOssMenuList();
	
	/**---------工具-------------------------*/
	
	/**构建菜单树*/
	public List<OssMenu> createOssMenuTree(List<OssMenu> list);
	
	
	
	
}
