package com.chess.common.service;

import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * oss接口接入
 * @Title: IPayInterService.java
 * @Description: 
 * @author  dengle
 * @date 2013-04-09
 * @version V1.0
 */
@Produces("application/json")
public interface IPayInterService {
	
	/**
	 * 电信充值回调接口
	 *  充值信息
	 * Sign  签名
	 * @return
	 */
	@POST
	@Path("/payByESurfing")
	public String payByESurfing(@QueryParam("chargeResult")Integer chargeResult,@QueryParam("txId")String txId,@QueryParam("chargeId")String chargeId,@QueryParam("payTime")String payTime,
			@QueryParam("orderSn")String orderSn,@QueryParam("IMSI")String IMSI,@QueryParam("reservedInfo")String reservedInfo,@QueryParam("sig")String sig);

}
