package com.chess.common.service;

import java.util.List;

import com.chess.common.bean.SystemConfigPara;
 

public interface ISystemConfigService {

	//public int get_1gem_n_gold();
	public int get_max_online_num();
	public int get_max_reg_player_num();
	
	public int get_new_player_gold_num();
	
	public int get_max_save_time();
	public int get_max_save_gold_num();

	public int get_neet_bind_phone_num();
	public int get_neet_bind_phone_num_hint();
	public int get_chu_delay_before_ting();
	public int get_chu_delay_after_ting();
	public int get_room_of_primary_player_num();
	public int get_room_of_intermediate_player_num();
	public int get_room_of_advanced_player_num();
	public int get_zhidui_support();
	public int show_notice_oncreatevip();

	public int show_notice_onlogin();
	public int get_show_notice_onlogin_fullscreen();
	public int get_show_notice_onlogin_showtime();
	public int get_show_notice_onlogin_delay();
	public int get_show_notice_onlogin_autoclose();

	public int get_good_cards();
	public int get_good_cards_max_count();
	public int get_good_cards_count_room_owners();

	public int get_couple_goodcards();
	public int get_couple_goodcards_max_count();
	public int get_couple_goodcards_count_room_owners();

	public int get_bao_at_last_ofcards();
	public int get_two_people_support();

	//游戏开始倒计时时长
	//public int get_game_start_count_down_seconds();
	
	public int get_room_price(int idx);
	
	public void load_all_para();
	public void updatePara(SystemConfigPara para);
	public SystemConfigPara getPara(int paraID);
	public List<SystemConfigPara> getAllConfigPara();
	public List<SystemConfigPara> getAllClientConfigPara();
 
	//支付开关
	public boolean get_PaySwitch();
	public int get_robotLimitNum();
	public int get_robotWaitLimitTime();
	//关闭产品
	public String getClosedProducts();
	/**俱乐部上限*/
	public int getClubCountMax();
}
