package com.chess.common.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.SpringService;
import com.chess.common.msg.processor.entrance.EntranceServerMsgProcessor;
import com.chess.common.msg.processor.entrance.EntranceServerThread;
import com.chess.common.msg.processor.entrance.EntranceSocket;
import com.chess.common.msg.processor.game.GameServerSocket;
import com.chess.common.msg.processor.game.GameServerThread;
import com.chess.common.msg.processor.update.UpdateServerMsgProcessor;
import com.chess.common.msg.processor.update.UpdateSocket;
import com.chess.core.ServerConnectScoket;
 

public class GameContext {
	
	
	private static Logger logger = LoggerFactory.getLogger(GameContext.class);
	
	/** 服务器名称 */
	public static String serverName = null;
	
	public static Integer gameSocketPort = 0;
	
	public static String serverID=null;
	public static String telecomIP=null;//电信ip
	public static String unicomIP=null;//联通ip
	//
	
	
	/**绿岸定制参数 指定本游戏区号标识(guid)*/
	public static String LvAnGameGuid = null;
	
	//入口服务器地址
	public static Integer entranceServerPort = 0;
	public static String entranceServerIP_telecom="";
	public static String entranceServerIP_unicom="";
	//0不是入口服务器（如果不是入口服务器，此服务器会将在线人数等信息发给上面的入口服务器地址），1是入口服务器
	public static Integer isEntranceServer=0;
	public static Integer isGameServer=0;
	public static Integer isUpdateServer=0;
	
	
	/** 充值WebServiceKey */
	public static String PayWebServiceKey = null;
	
	//运行环境的当前本地ip，双ip的怎么办？
	public static String localIp=null;
 
	/**游戏逻辑服*/
	public static GameServerSocket gameSocket=null;
	public static GameServerThread mainThread=null;
	
	public static com.chess.nndzz.table.TableLogicProcess tableLogic_nndzz=null;
	
	//游戏更新服务器
	public static Integer  patchServerSocketPort=0;
	public static String patchServerIP_telecom="";
	public static String patchServerIP_unicom="";
	public static UpdateServerMsgProcessor updateServerMsgProc=null;
	public static UpdateSocket updateServer=null;
	
	//游戏入口服务器
	public static  EntranceSocket entranceServer=null;
	public static  ServerConnectScoket entranceServerConnector=null;
	public static  EntranceServerMsgProcessor csMsgProcessor=null;
	public static  EntranceServerThread entranceLoop=null;
	
	// 参数缓存
	public static Map<String, Properties> propManager = new HashMap();
	
	/** session有效期 秒*/
	public static Integer SessionTimeOut=3600;
	 
 
	
	/**在线人数最大值,如果为负数表示不限制*/
	public static Integer onlineMaxNum;
  
 
	/** 玩家加密验证密钥 **/
	public static String verifyKey = "D&8S9DSDFSD%%$#@#$%$%%";
 
	
	
	
	//游戏币种选择 1单币种，2双币种
	public static Integer moneyTypeNum=1;
	
	/**游戏编号*/
	public static String gameId = "";
	
	/**网站分配的渠道*/
	public static String fromId = "";
	
 
	public static Map<String, String> cardsConfigMap;
	public static Map<String, String> payConfigMap;
	public static Map<String, String> configParamMap;
	
	
	/**几个服务器之间如果有链接，会发这个key，作为链接的合法性校验***/
	public static String checkKey="";
	
	public   String getCheckKey() {
		return GameContext.checkKey;
	}

	public   void setCheckKey(String checkKey) {
		GameContext.checkKey = checkKey;
	}
	
  
	//set
	public void setServerName(String serverName) {
		GameContext.serverName = serverName;
	}


	public void setSessionTimeOut(Integer sessionTimeOut) {
		SessionTimeOut = sessionTimeOut;
	}
	public void setPayWebServiceKey(String payWebServiceKey) {
		PayWebServiceKey = payWebServiceKey;
	}
	public void setMoneyTypeNum(String moneyTypeNum) {
		
		GameContext.moneyTypeNum = Integer.valueOf(moneyTypeNum);
	}
	public void setMoneyTypeNum(Integer moneyTypeNum) {
		GameContext.moneyTypeNum = moneyTypeNum;
	}
	

	public void setVerifyKey(String verifyKey) {
		GameContext.verifyKey = verifyKey;
	}
	public void setGameId(String gameId) {
		GameContext.gameId = gameId;
	}

	public void setFromId(String fromId) {
		GameContext.fromId = fromId;
	}

 
	public void setLvAnGameGuid(String lvAnGameGuid) {
		LvAnGameGuid = lvAnGameGuid;
	}

	public static Integer getMoneyTypeNum() {
		return moneyTypeNum;
	}

 

 
 

 

	public  void setIsEntranceServer(Integer isEntranceServer) {
		GameContext.isEntranceServer = isEntranceServer;
	}

	public  void setIsGameServer(Integer isGameServer) {
		GameContext.isGameServer = isGameServer;
	}

	public void setTelecomIP(String telecomIP) {
		GameContext.telecomIP = telecomIP;
	}

	public void setUnicomIP(String unicomIP) {
		GameContext.unicomIP = unicomIP;
	}

	public   void setServerID(String serverID) {
		GameContext.serverID = serverID;
	}

	public   void setGameSocketPort(Integer gameSocketPort) {
		GameContext.gameSocketPort = gameSocketPort;
	}

 
	public   void setEntranceServerPort(Integer entranceServerPort) {
		GameContext.entranceServerPort = entranceServerPort;
	}

	public  void setIsUpdateServer(Integer isUpdateServer) {
		GameContext.isUpdateServer = isUpdateServer;
	}

	public  void setPatchServerSocketPort(Integer patchServerSocketPort) {
		GameContext.patchServerSocketPort = patchServerSocketPort;
	}

	public   void setEntranceServerIP_telecom(String entranceServerIP_telecom) {
		GameContext.entranceServerIP_telecom = entranceServerIP_telecom;
	}

	public   void setEntranceServerIP_unicom(String entranceServerIP_unicom) {
		GameContext.entranceServerIP_unicom = entranceServerIP_unicom;
	}

	public   void setPatchServerIP_telecom(String patchServerIP_telecom) {
		GameContext.patchServerIP_telecom = patchServerIP_telecom;
	}

	public   void setPatchServerIP_unicom(String patchServerIP_unicom) {
		GameContext.patchServerIP_unicom = patchServerIP_unicom;
	}
	
	//支付缓存
	private static Map<String, Map<String, String>> payMap = new HashMap<String, Map<String,String>>();
	
	public static  Map<String, Map<String, String>> getPayMap() {
		return payMap;
	}

	public void setPayMap(Map<String, Map<String, String>> payMap) {
		this.payMap = payMap;
	}
	
	public static synchronized void setPayMapValue(String key,Map<String, String> value){
		getPayMap().put(key, value);
	}
	
	public static synchronized Map<String, String> getPayMapValue(String key){
		return getPayMap().get(key);
	}
	
	/**
	 * 加载配牌
	 * @return
	 */
	public static void loadCardsProperties(){
		Properties cardsProperties = loadProperties("cards", "WEB-INF/config/cards.properties");
		cardsConfigMap = propertiesToMap(cardsProperties);
	}
	/**
	 * 加载支付配置
	 */
	public static void loadPayConfigProperties(){
		Properties payProperties = loadProperties("payConfig", "WEB-INF/config/payConfig.properties");
		payConfigMap = propertiesToMap(payProperties);
	}
	
	/**
	 * 加载参数配置
	 * @return
	 */
	public static void loadConfigProperties(){
		Properties configProperties = loadProperties("config", "WEB-INF/config/config.properties");
		configParamMap = propertiesToMap(configProperties);
	}
	
	public static Map<String, String> getCardsConfigMap(){
		return cardsConfigMap;
	}
	
	public static boolean hasCardsConfig(String key){
		return hasConfigParam(cardsConfigMap, key);
	}
	
	public static String getCardsConfig(String key){
		return getConfigParam(cardsConfigMap, key);
	}
	
	public static Map<String, String> getPayConfigMap(){
		return payConfigMap;
	}
	
	public static boolean hasPayConfig(String key){
		return hasConfigParam(payConfigMap, key);
	}
	
	public static Object getPayConfig(String key){
		return getConfigParam(payConfigMap, key);
	}
	
	public static Map<String, String> getConfigsMap(){
		return configParamMap;
	}
	
	public static boolean hasConfigParam(String key){
		return hasConfigParam(configParamMap, key);
	}
	
	public static String getConfigParam(String key){
		return getConfigParam(configParamMap, key);
	}
	
	public static int getConfigParamToInt(String key, int dft){
		String obj = getConfigParam(key);
		if(NumberUtils.isNumber(obj)){
			return NumberUtils.toInt(obj);
		}
		return dft;
	}
	
	private static boolean hasConfigParam(Map<String, String> configMap, String key){
		if(configMap == null){
			return false;
		}
		return configMap.containsKey(key);
	}
	
	private static String getConfigParam(Map<String, String> configMap, String key){
		if(configMap == null){
			return null;
		}
		return configMap.get(key);
	}
	
	// properties转map
	private static Map<String, String> propertiesToMap(Properties properties){
		Map<String, String> map = new HashMap<String, String>(50);
		if(properties != null){
			for(Object key : properties.keySet()){
				String sKey = key.toString();
				map.put(sKey, (String) properties.get(key));
			}
		}
		return map;
	}
	
	/**
	 * 调试模式
	 * @return
	 */
	public static boolean isDebugMode(){
		return configParamMap != null && "0".equals(configParamMap.get("game_deploy_mode"));
	}
	
	/**
	 * 加载属性文件
	 * 
	 * @param key
	 * @param filePath
	 */
	public static Properties loadProperties(String key, String filePath) {
		Properties prop = null;
		InputStream ism = null;
		try {
//			ism = SpringService.getServletContext().getResourceAsStream(filePath);
			// 走spring会有缓存，这里先直接取文件；目前处理从路径中取，打成jar包可能会有问题，到时候再说
			
			ServletContext context = SpringService.getServletContext();
			String rPath = context.getRealPath("/");
			String realPath = rPath + filePath;
			File file = new File(realPath);
			ism = new FileInputStream(file);
			prop = new Properties();
			prop.load(ism);
			
			return prop;
				
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(key + ".properties 加载失败！");
		} finally {
			IOUtils.closeQuietly(ism);
		}
		return null;
	}
	
	public static String getConfigPropertiesValue(String key){
		return getPropertiesValue("config", key);
	}
	
	public static String getPropertiesValue(String fileName, String key) {
		Properties prop = propManager.get(fileName);
		if (prop == null) {
			return null;
		}
		return (String) prop.get(key);
	}
}