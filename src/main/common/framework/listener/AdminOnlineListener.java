package com.chess.common.framework.listener;

import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.chess.common.SpringService;
import com.chess.common.service.impl.OssUserService;
import com.chess.common.web.constant.AdminSystemConstant;
 

/**管理员在线监听*/
public class AdminOnlineListener implements HttpSessionListener,HttpSessionAttributeListener{

	
	private HttpSession session2 = null ; 
	
	
	public void sessionCreated(HttpSessionEvent arg0) {
		//this.session2 = arg0.getSession();
        //System.out.println("** 创建...") ;
        //System.out.println("SessionID:" + this.session2.getId()) ; 
        
        //查看，每次
        
		
	}

	public void sessionDestroyed(HttpSessionEvent arg0) {
		//System.out.println("**销毁...") ; 
		
	}

	public void attributeAdded(HttpSessionBindingEvent arg0) {
		HttpSession session = arg0.getSession();
		//System.out.println("**增加属性:" + arg0.getName() + "-->" + arg0.getValue()) ;
		if ((arg0.getName()).equals(AdminSystemConstant.ADMIN_SYSTEM_SESSION_KEY)) {
//			map.put(arg0.getSession().getId(),arg0.getValue() );
//			HttpSession session2 = arg0.getSession();
//			System.out.println("getCreationTime: "+session2.getCreationTime());
//			Date d = new Date(session2.getCreationTime());
//			System.out.println("d: " +DateService.parseDateToString(d));
//			System.out.println("getMaxInactiveInterval: "+session2.getMaxInactiveInterval());
			
			//管理员在线缓存
			OssUserService ossUserService = (OssUserService)SpringService.getBean("ossUserService");
			Map<String,HttpSession> map = ossUserService.getSessionMap();
			map.put(session.getId(), session);
			
			//增加玩家SESSION_KEY
		}
//		else if((arg0.getName()).equals(FlexSessionConstant.SESSION_KEY)) {//说明：这里只对有playerID的做处理，没有playerID当做临时不处理(正式验证登录通过之前不算)
//			PlayerSession playerSession = (PlayerSession)session.getAttribute(FlexSessionConstant.SESSION_KEY);
//			if(playerSession!=null && playerSession.getPlayerID()!=null && !"".equals(playerSession.getPlayerID().trim())){
//				ISystemLazyService systemLazyService = (ISystemLazyService)SpringService.getBean("systemLazyService");
//				systemLazyService.getSystemLazyCache().getPlayerOnlineSessionMap().put(playerSession.getPlayerID(), session);
//			}
//		}
		
	}

	public void attributeRemoved(HttpSessionBindingEvent arg0) {
		HttpSession session = arg0.getSession();
		//System.out.println("**删除属性:" + arg0.getName() + "-->" + arg0.getValue()) ;
		//在线管理员
		  if ((arg0.getName()).equals(AdminSystemConstant.ADMIN_SYSTEM_SESSION_KEY)) {
			//管理员在线缓存
				OssUserService ossUserService = (OssUserService)SpringService.getBean("ossUserService");
				Map<String,HttpSession> map = ossUserService.getSessionMap();
			  if(map!=null){
				  map.remove(session.getId());
			  }
		   //删除玩家SESSION_KEY
		  }
//		  else if((arg0.getName()).equals(FlexSessionConstant.SESSION_KEY)) {
//				PlayerSession playerSession = (PlayerSession)arg0.getValue();
//				if(playerSession!=null && playerSession.getPlayerID()!=null && !"".equals(playerSession.getPlayerID().trim())){
//					ISystemLazyService systemLazyService = (ISystemLazyService)SpringService.getBean("systemLazyService");
//					systemLazyService.getSystemLazyCache().getPlayerOnlineSessionMap().remove(playerSession.getPlayerID());
//					//System.out.println("删除---"+playerSession.getPlayerID());
//				}
//		  }
	}

	public void attributeReplaced(HttpSessionBindingEvent arg0) {
		//System.out.println("**更改属性:" + arg0.getName() + "-->" + arg0.getValue()) ; 
		
	}

}
