package com.chess.common.framework.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.ResourceBundleService;
import com.chess.common.SpringService;
import com.chess.common.framework.GameContext;
import com.chess.common.framework.scheduler.Day3Scheduler;
import com.chess.common.framework.scheduler.DayScheduler;
import com.chess.common.framework.scheduler.FiveMinutesScheduler;
import com.chess.common.framework.scheduler.HourScheduler;
import com.chess.common.framework.scheduler.MonthScheduler;
import com.chess.common.framework.scheduler.WeekScheduler;
import com.chess.common.framework.update.IUpdateService;
import com.chess.common.msg.processor.entrance.EntranceServerThread;
import com.chess.common.msg.processor.entrance.EntranceSocket;
import com.chess.common.msg.processor.game.GameServerSocket;
import com.chess.common.msg.processor.game.GameServerThread;
import com.chess.common.msg.processor.update.UpdateServerThread;
import com.chess.common.msg.processor.update.UpdateSocket;
import com.chess.common.service.ICacheGameConfigService;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.IEntranceUserService;
import com.chess.common.service.ISystemConfigService;
import com.chess.core.ServerConnectScoket;


public class SystemStartListener implements ServletContextListener {


    private GameServerSocket gameSocketServer = null;
    private GameServerThread mainThread = null;
    //
    private UpdateSocket updateServer = null;
    private UpdateServerThread updateMainLoop = null;
    //如果本服务器不是入口服务器，则有一个连接链接向入口服务器
    private ServerConnectScoket entranceServerConnector = null;

    private boolean debug = true;

    private static Logger logger = LoggerFactory.getLogger(SystemStartListener.class);

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();

        // 初始化SpringService.servletContext
        SpringService.setServletContext(servletContext);
        // 初始化系统参数
        this.initSysConfig(servletContext);
        
        // 加载配牌配置
        GameContext.loadCardsProperties();
        // 加载支付配置
        GameContext.loadPayConfigProperties();
        // 加载参数配置
        GameContext.loadConfigProperties();

        long d1 = System.currentTimeMillis();

        // 初始化缓存
        this.initCache();
        ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");

        playerService.initAsyncThread();
        
        //TODO 加载游戏玩法配置 
        ICacheGameConfigService gameConfigService = (ICacheGameConfigService) SpringService.getBean("gameConfigService");
        gameConfigService.initAllGameConfigs();
        
        long d2 = System.currentTimeMillis();

        logger.info(ResourceBundleService.getFormatString("txt_InitSystemListener_3_0", (d2 - d1)));

        if (GameContext.isGameServer == 1) {
            //创建主循环
            mainThread = new GameServerThread();
            GameContext.mainThread = mainThread;

            // 开启游戏Socket
            gameSocketServer = new GameServerSocket(GameContext.gameSocketPort, GameContext.serverName, mainThread);
            //引用
            GameContext.gameSocket = gameSocketServer;
        }


        //游戏补丁更新服务器，
        if (GameContext.isGameServer == 1) {
            updateMainLoop = new UpdateServerThread();
            updateServer = new UpdateSocket(GameContext.patchServerSocketPort, GameContext.serverName, updateMainLoop);
            GameContext.updateServer = updateServer;
        }

        //作为游戏入口服务器，
        EntranceServerThread entranceMainLoop = null;
        if (GameContext.isEntranceServer == 1) {
            entranceMainLoop = new EntranceServerThread();
            EntranceSocket entranceServer = new EntranceSocket(GameContext.entranceServerPort, GameContext.serverName, entranceMainLoop);
            GameContext.entranceServer = entranceServer;
            logger.info("本服务器作为入口服务器启动。");
        }

        //创建一个链接，链接向入口服务器，如果本服务器就是入口服务器，也创建个链接，方便其他代码统一操作
        String ip = GameContext.entranceServerIP_telecom + ":" + GameContext.entranceServerPort;
        entranceServerConnector = new ServerConnectScoket(ip, entranceMainLoop, GameContext.checkKey, GameContext.serverName);
        GameContext.entranceServerConnector = entranceServerConnector;

        //
        GameContext.entranceLoop = entranceMainLoop;
        //

        //开启调度
        this.startScheduler();

        //加载系统配置参数
        ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
        cfgService.load_all_para();

        //数据修复
        handleSystemStartDataRepair();
        //加载补丁到内存
        if (updateMainLoop != null) {
            updateMainLoop.load_all_patch();
        }
        //启动主循环
        if (mainThread != null) {
            mainThread.start();
            logger.info("游戏逻辑服务器消息线程启动完成。");
        }
        //
        if (updateMainLoop != null) {
            updateMainLoop.start();
            logger.info("游戏更新服务器消息线程启动完成。");
        }
        //
        if (entranceMainLoop != null) {
            //入口服务
            if (GameContext.entranceServer != null) {
                IEntranceUserService entranceUserService = (IEntranceUserService) SpringService.getBean("entranceUserService");
                entranceUserService.init();
            }
            //
            entranceMainLoop.start();
            logger.info("入口服务器消息线程启动完成。");

        }
        
        //
        playerService.load_base_tables();
        
        // 加载在线更新全部版本
		IUpdateService updateService = (IUpdateService) SpringService.getBean("updateService");
		updateService.loadAllVersions();
        
        logger.info("服务器启动完成");
    }

    /**
     * 开启调度
     */
    private void startScheduler() {
    	logger.error("$$$$>>启动定时调度任务！");
        HourScheduler.run();
        FiveMinutesScheduler.run();
        DayScheduler.run();
        Day3Scheduler.run();
        WeekScheduler.run();
        MonthScheduler.run();
    }

    private void initCache() {

    }

    //
    //仅供测试使用
    private void addPlayerResource() {
//		IVillageService villageService = (IVillageService)SpringService.getBean("villageService");
//		IPlayerService playerService = (IPlayerService)SpringService.getBean("playerService");
//		Map<String, PlayerCache> maps = playerService.getPlayerCacheMap();
//		for(String playerID:maps.keySet())
//		{
//			 
//			String villageID=villageService.getVillageIDByPlayerID(playerID);
//			villageService.addVillageResourcesCanOverstepByVillageID(villageID, 98999, 99999, 99999, 99999);
//		}
    }

    //仅供测试使用，创建全部玩家的小助手任务，进行测试
    private void testGameAssistant() {
//		IGameAssistantService gameAssistantService = (IGameAssistantService)SpringService.getBean("gameAssistantService");
//		Map<String, PlayerCache> maps=(Map<String, PlayerCache>)CacheService.getFromCache(CacheConstant.PLAYERID_PLAYERCACHE_MAP);
//		for(String playerID:maps.keySet())
//		{
//			System.out.println(playerID);
//			//PlayerCache playerCache=maps.get(playerID);
//			for(Integer i=1;i<15;i++)
//			{
//				String extData="{\"buildingID\":"+i+"}";
//				//gameAssistantService.addAutoExecTask(playerID, GameAssistantConstant.ASSISTANT_BUILDING_UPDRADE, 99, extData);
//			}
//		}
    }

    /**
     * 初始化系统参数
     *
     * @param servletContext
     */
    private void initSysConfig(ServletContext servletContext) {

    }

    /**
     * 处理系统启动时数据自动修复
     */
    private void handleSystemStartDataRepair() {


    }

    /**
     * 停止Tomcat时处理
     */
    public void contextDestroyed(ServletContextEvent servletContextEvent) {


    }


    public GameServerSocket getGameSocketServer() {
        return gameSocketServer;
    }

}
