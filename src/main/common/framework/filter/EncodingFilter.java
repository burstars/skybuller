package com.chess.common.framework.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * <p>Title: 解决jsp/servlet中文问题</p>
 * <p>Description: 中文问题,同时，在web.xml中加入如下配置：</p><br>
 *   <filter><br>
 *    <filter-name>Encoding</filter-name><br>
 *    <filter-class>gds.EncodingFilter</filter-class><br>
 *   <init-param><br>
 *        <param-name>encoding</param-name><br>
 *        <param-value>GBK</param-value><br>
 *    </init-param><br>
 *  </filter><br>
 *  <filter-mapping><br>
 *    <filter-name>Encoding</filter-name><br>
 *    <url-pattern>/*</url-pattern><br>
  *  </filter-mapping><br>
  * <p>Copyright: Copyright (c) 2002 writeonce</p>
  * <p>Company: </p>
  * @author writeonce
  * @version 1.0
  */
public class EncodingFilter implements Filter {
	protected String encoding = null;
	protected FilterConfig filterConfig = null;

	public void destroy() {
		this.encoding = null;
		this.filterConfig = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response,
		FilterChain chain) throws IOException, ServletException {
		// Select and set (if needed) the character encoding to be used
		String encoding = selectEncoding(request);

		if (encoding != null) {
			request.setCharacterEncoding(encoding);
			
		}
		else
		{
			request.setCharacterEncoding("GBK");
		}
		// Pass control on to the next filter
		chain.doFilter(request, response);
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
		this.encoding = filterConfig.getInitParameter("encoding");
	}

	protected String selectEncoding(ServletRequest request) {
		return (this.encoding);
	}
}
