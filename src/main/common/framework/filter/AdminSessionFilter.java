package com.chess.common.framework.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.chess.common.web.BaseAdminContext;
import com.chess.common.web.constant.AdminSystemConstant;

public class AdminSessionFilter implements Filter {
	protected FilterConfig filterConfig = null;

	public void destroy() {
		this.filterConfig = null;
	}

	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
		FilterChain chain) throws IOException, ServletException {
		
		/** 
		 * 如果处理HTTP请求，并且需要访问诸如getHeader或getCookies等在ServletRequest中 
		 * 无法得到的方法，就要把此request对象构造成HttpServletRequest 
		 */ 
		HttpServletRequest request = (HttpServletRequest)servletRequest;
		HttpServletResponse response = (HttpServletResponse)servletResponse; 
		
		//response.setHeader("P3P","CP=CAO PSA OUR");//iframe引起的内部cookie丢失
		request.getSession().setMaxInactiveInterval(3600);
		String currentURL = request.getRequestURI(); //取得根目录所对应的绝对路径
		//System.out.println(currentURL + "::::::::::::");
		//System.out.println("request.getContextPath()=" + request.getContextPath()+"=="+currentURL);  
//		if (currentURL != null && !"".equals(currentURL) && currentURL.indexOf("logout.jsp") < 0 && currentURL.indexOf("login.") < 0 && currentURL.indexOf("nopurview.jsp") < 0) {
//			HttpSession session = request.getSession(false);
//			if (session == null || session.getAttribute(AdminSystemConstant.ADMIN_SYSTEM_SESSION_KEY) == null) {
//				response.sendRedirect(request.getContextPath() + "/admin/logout.jsp"); 
//				return;
//			}else{//权限模块验证
//				/*
//				System.out.println("getCreationTime2: "+session.getCreationTime());
//				Date d = new Date(session.getCreationTime());
//				System.out.println("d2: " +DateService.parseDateToString(d));
//				System.out.println("getMaxInactiveInterval2"+session.getMaxInactiveInterval());
//				System.out.println("getLastAccessedTime: "+ session.getLastAccessedTime());
//				d = new Date(session.getLastAccessedTime());
//				System.out.println("最后操作时间：: " +DateService.parseDateToString(d));
//				*/
//				BaseAdminContext baseAdminContext = (BaseAdminContext)session.getAttribute(AdminSystemConstant.ADMIN_SYSTEM_SESSION_KEY);
////				if(!baseAdminContext.allow(currentURL)){
////					response.sendRedirect(request.getContextPath() + "/admin/nopurview.jsp"); 
////					return;
////				}
//				if(baseAdminContext==null){
//					response.sendRedirect(request.getContextPath() + "/admin/login.jsp"); 
//					return;
//				}
//			}
//		}
		
		
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
}
