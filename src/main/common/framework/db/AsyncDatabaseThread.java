package com.chess.common.framework.db;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.bean.User;
import com.chess.common.constant.DBConstant;
import com.chess.common.dao.IClubMemberDAO;
import com.chess.common.dao.ILoginLogDAO;
import com.chess.common.dao.INormalGameRecordDAO;
import com.chess.common.dao.IPlayerLogDAO;
import com.chess.common.dao.IPlayerPlayLogDAO;
import com.chess.common.dao.IProxyVipRoomRecordDAO;
import com.chess.common.dao.IUserBackpackDAO;
import com.chess.common.dao.IUserDAO;
import com.chess.common.dao.IUserFriendDAO;
import com.chess.common.dao.IVipGameRecordDAO;
import com.chess.common.dao.IVipRoomRecordDAO;


/***
 * 将游戏中一些数据更新插入删除之类的不是最紧急的操作，放到这条线程中来执行，这样，主游戏线程可以不用等待数据库反应
 **/
public class AsyncDatabaseThread extends Thread {
    private static Logger logger = LoggerFactory.getLogger(AsyncDatabaseThread.class);

    private static AsyncDatabaseThread myInstance = null;
    private Lock msgArraylock = new ReentrantLock();
    private List<DBOperation> msgReceived = new ArrayList<DBOperation>(2000);

    private IUserDAO userDAO = null;
    private IPlayerLogDAO playerLogDao = null;
    private IUserBackpackDAO userBackpackDAO = null;
    private ILoginLogDAO loginLogDAO = null;
    private IUserFriendDAO playerFriendDAO = null;
    private IVipGameRecordDAO vipGameRecordDAO = null;
    private IVipRoomRecordDAO vipRoomRecordDAO = null;
    private INormalGameRecordDAO normalGameRecordDAO = null;
    private IPlayerPlayLogDAO playerPlayLogDAO = null;
    private IProxyVipRoomRecordDAO proxyVipRoomRecordDAO = null;//代开优化 cc modify 2017-9-26
    private IClubMemberDAO clubMemberDAO= null;
    
    public IPlayerPlayLogDAO getPlayerPlayLogDAO() {
        return playerPlayLogDAO;
    }

    public void setPlayerPlayLogDAO(IPlayerPlayLogDAO playerPlayLogDAO) {
        this.playerPlayLogDAO = playerPlayLogDAO;
    }

    public AsyncDatabaseThread() {
        myInstance = this;
    }

    //
    public static void pushDBMsg(DBOperation msg) {
        myInstance.pushMsg(msg);
    }

    //
    public void pushMsg(DBOperation msg) {
        try {
            msgArraylock.lock();
            msgReceived.add(msg);
            //
        } catch (Exception e) {
            logger.error("addMsg failed", e);
        } finally {
            msgArraylock.unlock();
        }
    }

    //
    private DBOperation popMsg() {
        if (msgReceived.size() <= 0)
            return null;

        DBOperation msg = null;
        try {
            msgArraylock.lock();
            msg = msgReceived.remove(0);
            //

            //
        } catch (Exception e) {
            logger.error("popMsg failed", e);
        } finally {
            msgArraylock.unlock();
        }

        //
        return msg;
    }

    //
    private void execute_msg(DBOperation msg) {
        switch (msg.dao) {
            case DBConstant.USER_DAO:
                execute_player_dao_msg(msg);
                break;
            case DBConstant.USER_LOG_DAO:
                execute_player_log_dao_msg(msg);
                break;
            case DBConstant.USER_ITEM_DAO:
                execute_player_item_dao_msg(msg);
                break;
            case DBConstant.LOGIN_LOG_DAO:
                this.execute_login_log_dao_msg(msg);
                break;
            case DBConstant.USER_FRIEND_DAO:
                execute_playerfriend_dao_msg(msg);
                break;
            case DBConstant.VIP_ROOM_DAO:
                execute_vip_room_record_dao_msg(msg);
                break;
            case DBConstant.PROXY_VIP_ROOM_DAO:
                execute_proxy_vip_room_record_dao_msg(msg);
                break;
            case DBConstant.VIP_GAME_DAO:
                execute_vip_game_record_dao_msg(msg);
                break;
            case DBConstant.USER_PLAY_LOG_DAO:
                execute_player_play_log_msg(msg);
                break;
            case DBConstant.CLUB_MEMBER_DAO:
                execute_clubMember_dao_msg(msg);
                break;
            default:
                break;
        }
    }

    private void execute_player_play_log_msg(DBOperation msg) {
		switch (msg.opertaion) {
            case DBConstant.USER_PLAY_DAO_inc_create_vip_count: {
                playerPlayLogDAO.incCreateVipCountByPlayerID(msg.playerID);
                break;
            }
            case DBConstant.USER_PLAY_DAO_inc_normal_count: {
                playerPlayLogDAO.incNormalCountByPlayerID(msg.playerID);
                break;
            }
            case DBConstant.USER_PLAY_DAO_inc_vip_count: {
                playerPlayLogDAO.incVipCountByPlayerID(msg.playerID);
                break;
            }
            case DBConstant.USER_PLAY_DAO_update_vip_count: {
                playerPlayLogDAO.updateVipCountByPlayerID(msg.playerID, msg.vipCount);
            }
            case DBConstant.USER_PLAY_DAO_update_normal_count: {
                playerPlayLogDAO.updateNormalCountByPlayerID(msg.playerID, msg.normalCount);
            }
            case DBConstant.USER_PLAY_DAO_delete_player_log: {
                playerPlayLogDAO.deletePlayerPlayerLogByPlayerID(msg.playerID);
            }
        }
    }

    //
    private void execute_player_dao_msg(DBOperation msg) {
        switch (msg.opertaion) {
            //
            case DBConstant.USER_DAO_updateOfflineTime: {
                userDAO.updateOfflineTime(msg.playerID, msg.date);
            }
            break;
            //
            case DBConstant.USER_DAO_updatePlayerGold: {
                userDAO.updatePlayerGold(msg.playerID, msg.gold);
            }
            break;
            //
            case DBConstant.USER_DAO_updatePlayerRecord: {
                userDAO.updatePlayerRecord(msg.playerID, msg.wons, msg.loses, msg.escapes);
            }
            break;
            //
            case DBConstant.USER_DAO_updatePlayerHead: {
                userDAO.updatePlayerHead(msg.playerID, msg.headImg);
            }
            break;
            //
            case DBConstant.USER_DAO_updatePlayerLife: {
                userDAO.updatePlayerLife(msg.playerID, msg.life);
            }
            break;
            //
            case DBConstant.USER_DAO_updatePlayerLifeAndGold: {
                userDAO.updatePlayerLifeAndGold(msg.playerID, msg.gold, msg.life);
            }
            break;
            //
            case DBConstant.USER_DAO_updatePlayerLoginMsg: {
                userDAO.updatePlayerLoginMsg(msg.playerID, msg.gold, msg.date, msg.continueLanding, msg.luckyDrawsTimes, msg.diamond);
            }
            break;
            case DBConstant.USER_DAO_updatePlayerHistoryBestScore: {
                userDAO.updatePlayerHistoryBestScore(msg.playerID, msg.score);
            }
            break;

            case DBConstant.USER_DAO_updatePlayerTodayBestScore: {
                userDAO.updatePlayerTodayBestScore(msg.playerID, msg.score);
            }
            break;
            case DBConstant.USER_DAO_updatePlayerVipAndExp: {
                userDAO.updateVIP(msg.playerID, msg.vipExp, msg.vipLevel);
            }
            break;
            case DBConstant.USER_DAO_updatePlayerGem: {
                userDAO.updatePlayerGem(msg.playerID, msg.gemNum);
            }
            break;
            case DBConstant.USER_DAO_updatePlayer: {
                User pl = userDAO.getPlayerByID(msg.playerID);
                if (pl == null) return;
                pl.setPlayerName(msg.playerName);
                pl.setSex(msg.sex);
                pl.setHeadImg(msg.headImg);
                pl.setAccount(msg.account);
                pl.setPassword(msg.password);
                userDAO.updatePlayer(pl);
            }
            break;
            case DBConstant.USER_DAO_updatePlayerPassWorld: {
                userDAO.updatePlayerPassword(msg.playerID, msg.password);
            }
            break;
            case DBConstant.USER_DAO_updatePlayerCanFriend:
                userDAO.updatePlayerCanFriend(msg.playerID, msg.canFriend);
                break;
            case DBConstant.USER_DAO_updatePlayerType:
                userDAO.updatePlayerType(msg.playerID, msg.playerType);
                break;
            case DBConstant.USER_DAO_updatePlayerPhoneNumber:
                userDAO.updatePlayerPhoneNumber(msg.playerID, msg.phoneNumber);
                break;
            case DBConstant.USER_DAO_updatePlayerLocation:
                userDAO.updatePlayerLocation(msg.playerID, msg.extStr);
                break;
            case DBConstant.USER_DAO_update_share_num:
            	userDAO.updatePlayerShareNum(msg.playerID, msg.shareNum , msg.shareDate);
            	break;
            default:
                break;
        }
    }

    //
    private void execute_player_log_dao_msg(DBOperation msg) {
        switch (msg.opertaion) {
            //
            case DBConstant.USER_LOG_DAO_insert: {
                playerLogDao.insert(msg.log);
            }
            break;
            //
            default:
                break;
        }
    }

    //
    private void execute_player_item_dao_msg(DBOperation msg) {
        switch (msg.opertaion) {
            //
            case DBConstant.USER_ITEM_DAO_deleteByPlayerItemID: {
                userBackpackDAO.deleteByPlayerItemID(msg.playerItemID);
            }
            break;
            case DBConstant.USER_ITEM_DAO_updatePlayerItemNum: {
                userBackpackDAO.updatePlayerItemNum(msg.playerID, msg.itemBaseID, msg.itemNum,"");
            }
            break;
            //
            default:
                break;
        }
    }


    private void execute_login_log_dao_msg(DBOperation msg) {
        switch (msg.opertaion) {
            //
            case DBConstant.LOGIN_LOG_DAO_CREATE_LOG: {
                loginLogDAO.createLoginLog(msg.loginLog);
            }
            break;
            case DBConstant.LOGIN_LOG_DAO_CREATE_UPDATE_LOGIN_OUT_TIME: {
                loginLogDAO.updatePlayerLoginOutTime(msg.loginLog.getLoginLogID(), msg.loginLog.getTableNameSuffix(), msg.loginLog.getQuitTime());
            }
            break;
            //
            default:
                break;
        }
    }

    private void execute_playerfriend_dao_msg(DBOperation msg) {
        switch (msg.opertaion) {
            case DBConstant.USER_FRIEND_AddPLAYERFRIEND: {
                //添加好友
                playerFriendDAO.createPlayerFriend(msg.playerID, msg.extStr, msg.applyResult);
            }
            break;
            case DBConstant.USER_FRIEND_DELPLAYERFRIEND: {
                //删除好友
                playerFriendDAO.deletePlayerFriend(msg.playerID, msg.extStr);
            }
            break;
            case DBConstant.USER_DAO_updatePlayerRemark:
                //修改好友备注名称
                playerFriendDAO.updateFriendRemark(msg.playerID, msg.extStr, msg.remark);
                break;
            case DBConstant.USER_FRIEND_DAO_updateApplyResult:
                //修改好友验证标志
                playerFriendDAO.updateFriendApplyResult(msg.playerID, msg.extStr, msg.applyResult);
                break;
            default:
                break;
        }
    }
    
	private void execute_clubMember_dao_msg(DBOperation msg) {
		switch (msg.opertaion) {
		case DBConstant.CLUB_MEMBER_add_member:{
			// 请求添加成员
			clubMemberDAO.createClubMember(msg.playerID, msg.extStr,msg.playerName);
			break;
		}
		case DBConstant.CLUB_MEMBER_delete_member:{
			// 删除成员
			clubMemberDAO.deleteClubMember(msg.playerID, msg.extStr,msg.playerName);
			break;
		}
		case DBConstant.CLUB_MEMBER_update_member_state:{
			// 添加验证用过，更新状态
			clubMemberDAO.updateClubMemberState(msg.playerID, msg.extStr,msg.playerName,
					msg.applyResult);
		    break;
		}
		case DBConstant.CLUB_MEMBER_update_member_remark:{
			clubMemberDAO.updateClubMemberRemark(msg.playerID, msg.extStr,msg.playerName,
					msg.remark);
			break;
		}
		default:
			break;
		}
	}

    //操作普通场的游戏记录
    private void execute_normal_game_record_dao_msg(DBOperation msg) {
        if ((null == msg.normalGameRecord) || (null == normalGameRecordDAO)) {
            logger.error("execute_normal_game_record_dao_msg error");
            return;
        }

        switch (msg.opertaion) {
            case DBConstant.NORMAL_GAME_DAO_create_normal_game: {
                normalGameRecordDAO.createNormalGameRecord(msg.normalGameRecord);
            }
            break;
            default:
                break;
        }
    }

    //操作VIP场的房间记录
    private void execute_vip_room_record_dao_msg(DBOperation msg) {
        if ((null == msg.vipRoomRecord) || (null == vipRoomRecordDAO)) {
            logger.error("execute_vip_room_record_dao_msg error");
            return;
        }

        switch (msg.opertaion) {
            case DBConstant.VIP_ROOM_DAO_create_vip_room: {
                vipRoomRecordDAO.createVipRoomRecord(msg.vipRoomRecord);
            }
            break;
            case DBConstant.VIP_ROOM_DAO_update_vip_room: {
                vipRoomRecordDAO.updateRecord(msg.vipRoomRecord);
            }
            break;
            case DBConstant.VIP_ROOM_DAO_end_vip_room: {
                vipRoomRecordDAO.endRecord(msg.vipRoomRecord);
            }
            break;
            default:
                break;
        }
    }
    
    //操作VIP场的房间记录
    private void execute_proxy_vip_room_record_dao_msg(DBOperation msg) {
        if ((null == msg.proxyVipRoomRecord) || (null == proxyVipRoomRecordDAO)) {
            logger.error("execute_proxy_vip_room_record_dao_msg error");
            return;
        }

        switch (msg.opertaion) {
            case DBConstant.PROXY_VIP_ROOM_DAO_create_vip_room: {
            	proxyVipRoomRecordDAO.createProxyVipRoomRecord(msg.proxyVipRoomRecord);
            }
            break;
            case DBConstant.PROXY_VIP_ROOM_DAO_update_vip_room: {
            	proxyVipRoomRecordDAO.updateRecord(msg.proxyVipRoomRecord);
            }
            break;
            case DBConstant.PROXY_VIP_ROOM_DAO_end_vip_room: {
            	proxyVipRoomRecordDAO.endRecord(msg.proxyVipRoomRecord);
            }
            break;
            default:
                break;
        }
    }

    //操作VIP场的游戏记录
    private void execute_vip_game_record_dao_msg(DBOperation msg) {
        if ((null == msg.vipGameRecord) || (null == vipGameRecordDAO)) {
            logger.error("execute_vip_game_record_dao_msg error");
            return;
        }

        switch (msg.opertaion) {
            case DBConstant.VIP_GAME_DAO_create_vip_game: {
                vipGameRecordDAO.createVipGameRecord(msg.vipGameRecord);
            }
            break;
            default:
                break;
        }
    }


    //
    private void run_one_loop() {
        for (int i = 0; i < 300; i++) {
            DBOperation msg = popMsg();
            if (msg == null) {
                break;
            }
            execute_msg(msg);
        }
        /*
		DBOperation msg= popMsg();
		if(msg==null)
			return;
		execute_msg(msg);
		*/
        //long time2 = DateService.getCurrentUtilDate().getTime();
        //logger.info("dao operation: " + msg.opertaion + " 处理时间:" + (time2 - time1));
    }

    //线程函数，主循环
    @Override
    public void run() {
        while (true) {
            try {
                run_one_loop();
                Thread.sleep(10);
            } catch (Exception e) {
                logger.error("MaintainThread loop error", e);
            }
        }
    }

    public IUserDAO getUserDAO() {
        return userDAO;
    }

    public void setUserDAO(IUserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public IPlayerLogDAO getPlayerLogDao() {
        return playerLogDao;
    }

    public void setPlayerLogDao(IPlayerLogDAO playerLogDao) {
        this.playerLogDao = playerLogDao;
    }

    public IUserBackpackDAO getUserBackpackDAO() {
        return userBackpackDAO;
    }

    public void setUserBackpackDAO(IUserBackpackDAO userBackpackDAO) {
        this.userBackpackDAO = userBackpackDAO;
    }


    public ILoginLogDAO getLoginLogDAO() {
        return loginLogDAO;
    }

    public void setLoginLogDAO(ILoginLogDAO loginLogDAO) {
        this.loginLogDAO = loginLogDAO;
    }

    public IUserFriendDAO getPlayerFriendDAO() {
        return playerFriendDAO;
    }

    public void setPlayerFriendDAO(IUserFriendDAO playerFriendDAO) {
        this.playerFriendDAO = playerFriendDAO;
    }

    public IVipGameRecordDAO getVipGameRecordDAO() {
        return vipGameRecordDAO;
    }

    public void setVipGameRecordDAO(IVipGameRecordDAO vipGameRecordDAO) {
        this.vipGameRecordDAO = vipGameRecordDAO;
    }

    public IVipRoomRecordDAO getVipRoomRecordDAO() {
        return vipRoomRecordDAO;
    }


    public void setVipRoomRecordDAO(IVipRoomRecordDAO vipRoomRecordDAO) {
        this.vipRoomRecordDAO = vipRoomRecordDAO;
    }

    public INormalGameRecordDAO getNormalGameRecordDAO() {
        return normalGameRecordDAO;
    }

    public void setNormalGameRecordDAO(INormalGameRecordDAO normalGameRecordDAO) {
        this.normalGameRecordDAO = normalGameRecordDAO;
    }

	public IProxyVipRoomRecordDAO getProxyVipRoomRecordDAO() {
		return proxyVipRoomRecordDAO;
	}

	public void setProxyVipRoomRecordDAO(
			IProxyVipRoomRecordDAO proxyVipRoomRecordDAO) {
		this.proxyVipRoomRecordDAO = proxyVipRoomRecordDAO;
	}

	public IClubMemberDAO getClubMemberDAO() {
		return clubMemberDAO;
	}

	public void setClubMemberDAO(IClubMemberDAO clubMemberDAO) {
		this.clubMemberDAO = clubMemberDAO;
	}
	
}
