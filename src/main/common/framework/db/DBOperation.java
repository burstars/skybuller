package com.chess.common.framework.db;

import java.util.Date;

import com.chess.common.bean.LoginLog;
import com.chess.common.bean.NormalGameRecord;
import com.chess.common.bean.PlayerOperationLog;
import com.chess.common.bean.ProxyVipRoomRecord;
import com.chess.common.bean.User;
import com.chess.common.bean.VipGameRecord;
import com.chess.common.bean.VipRoomRecord;


public class DBOperation 
{
	public int dao=0;
	public int opertaion=0;
	
	public String playerID="";
	public String account = "";
	public String playerName="";
	public String password="";
	public Date date=null;
	
	public int gold=0;
	public int life=0;
	public int score=0;
	public int wons=0;
	public int loses=0;
	public int escapes=0;
	public int headImg=0;
	public int diamond=0;
	public int continueLanding=0;
	public int luckyDrawsTimes=0;
	
	/**扩展字符串*/
	public String extStr = "";
	
	//好友备注名字
	public String remark = "";
	
	public int sex = 0;
	
	public int gemNum = 0;
	
	public PlayerOperationLog log=null;
	
	public String playerItemID="";
	public int itemBaseID=0;
	public int itemNum=0;
	
	public int vipLevel=0;
	public int vipExp=0;
	
	public int pExtParamType = 10002;
	public int extAddNum = 0;
	
	public LoginLog loginLog = null;
	
	public int canFriend = 0;
	
	public int playerType = 0;
	
	/**申请好友的类型*/
	public int applyResult = 0;
	
	/**手机号码*/
	public String phoneNumber = "";

	public int vipCount = 0;
	public int normalCount = 0;
	/** game record*/
	public User pl_info = null;

	/**普通场游戏记录*/
	public NormalGameRecord normalGameRecord = null;
	
	/**VIP房间记录*/
	public VipRoomRecord vipRoomRecord = null;
	
	/**代理开房记录 2017-9-26*/
	public ProxyVipRoomRecord proxyVipRoomRecord = null;
	
	/**VIP游戏记录*/
	public VipGameRecord vipGameRecord = null;

	public int shareNum;
	public long shareDate;
	//
	public DBOperation()
	{
	}
	 
}
