package com.chess.common.framework.scheduler;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import com.chess.common.SpringService;
import com.chess.common.service.ISchedulerService;
 
 
/**每日的3点的调度**/
public class Day3Scheduler implements Job {
	
	private static ISchedulerService schedulerService = (ISchedulerService)SpringService.getBean("schedulerService");
	private static Scheduler sched = null;
	
	/** 锁对象 */
	private static Object lock = new Object();
	
	/** 任务是否正在运行的标志位 */
	private static boolean isRunning = false;

	private static Logger logger = LoggerFactory.getLogger(Day3Scheduler.class);
	
	public static void run() {
		try {
			logger.error("$$$$>>启动日定时调度任务，触发时间：每天凌晨3点");
			SchedulerFactory schedFact = new StdSchedulerFactory();
			sched = schedFact.getScheduler();
			
			JobDetail jobDetail = new JobDetail("Day3Job", null, Day3Scheduler.class);
			CronTrigger trigger = new CronTrigger("Day3JobTrigger", "Day3JobGroup");
			//trigger.setCronExpression("0 * * * * ?");
			trigger.setCronExpression("0 0 3 * * ?");
			//
			sched.scheduleJob(jobDetail, trigger);
			sched.start();
			logger.info("每日调度3启动成功");
		} catch (SchedulerException e) {
			logger.error("每日调度3启动失败", e);
		} catch (ParseException e) {
			logger.error("每日调度3启动失败", e);
		}
	}

	public void execute(JobExecutionContext job) throws JobExecutionException {
		
		synchronized (lock) {
			if (isRunning) {
				return;
			} else {
				isRunning = true;
			}
		}
		
		try {
			logger.error("$$$$>>执行每日3调度方法：time="+(new Date().toString()));
			schedulerService.handleDay3Scheduler();
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("每日调度3执行失败", e);
		} finally {
			isRunning = false;
		}
	}
	
	public static void shutdown() {
		try {
			sched.shutdown();
		} catch (SchedulerException e) {
			logger.error("每日调度3关闭失败", e);
		}
	}
}
