package com.chess.common.framework.scheduler;

import java.text.ParseException;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import com.chess.common.SpringService;
import com.chess.common.service.ICacheUserService;
 
 
 
/** 每个0分和30分钟的调度 20秒的延迟**/
public class HourScheduler implements Job {
	
 
	private static Scheduler sched = null;
	
	/** 锁对象 */
	private static Object lock = new Object();
	
	/** 任务是否正在运行的标志位 */
	private static boolean isRunning = false;

	private static Logger logger = LoggerFactory.getLogger(HourScheduler.class);
	
	public static void run() {
		try {
			SchedulerFactory schedFact = new StdSchedulerFactory();
			sched = schedFact.getScheduler();
			
			JobDetail jobDetail = new JobDetail("HourScheduler", null, HourScheduler.class);
			CronTrigger trigger = new CronTrigger("HourScheduler", "HourScheduler");
			
			//trigger.setCronExpression("2 0 * * * ?");//每个小时的0分2秒的时候调度
			trigger.setCronExpression("20 0/30 * * * ?");//每个0分和30分钟的调度
//			trigger.setCronExpression("* * * * * ?");//每秒1次调度
//			trigger.setCronExpression("0 * * * * ?");//每分钟0秒1次调度
			
			//
			sched.scheduleJob(jobDetail, trigger);
			sched.start();
			logger.info("每个0分和30分钟的调度调度启动成功");
		} catch (SchedulerException e) {
			logger.error("每个0分和30分钟的调度调度启动失败", e);
		} catch (ParseException e) {
			logger.error("每个0分和30分钟的调度调度启动失败", e);
		}
	}

	public void execute(JobExecutionContext job) throws JobExecutionException {
		
		synchronized (lock) {
			if (isRunning) {
				return;
			} else {
				isRunning = true;
			}
		}
		
		try {
			//这里加上要调度的任务
			//int ct=DateService.getCurrentMin();
			//logger.info("dbServer调度执行"+ct);
			
			ICacheUserService	playerService = (ICacheUserService) SpringService.getBean("playerService");
			//删除玩家缓存
			playerService.clearPlayerCatch();
			
			//统计在线人数
			playerService.onlinePlayerNums();
			
			//发送一小时频率定时跑马灯消息
			playerService.sendOneHourOnTimeSystemScrollMsg();
			//IAnimalTicketService	ticketService = (IAnimalTicketService) SpringService.getBean("animalTicketService");
			//ticketService.sendReward();//开奖发奖金
			
			//
			//IRankingListService	rankService = (IRankingListService) SpringService.getBean("rankingListService");
			//rankService.reloadAllRankList();//重新排名
			
			//schedulerService.handleDayScheduler();
		} catch (Exception e) {
			logger.error("每个0分和30分钟的调度调度执行失败", e);
		} finally {
			isRunning = false;
		}
	}
	
	public static void shutdown() {
		try {
			sched.shutdown();
		} catch (SchedulerException e) {
			logger.error("每个0分和30分钟的调度调度关闭失败", e);
		}
	}
}
