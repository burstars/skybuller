package com.chess.common.framework.scheduler;

import java.text.ParseException;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

import com.chess.common.SpringService;
import com.chess.common.service.ISchedulerService;
 
 
/**每日的0点的0分5秒的调度**/
public class DayScheduler implements Job {
	
	private static ISchedulerService schedulerService = (ISchedulerService)SpringService.getBean("schedulerService");
	private static Scheduler sched = null;
	
	/** 锁对象 */
	private static Object lock = new Object();
	
	/** 任务是否正在运行的标志位 */
	private static boolean isRunning = false;

	private static Logger logger = LoggerFactory.getLogger(DayScheduler.class);
	
	public static void run() {
		try {
			SchedulerFactory schedFact = new StdSchedulerFactory();
			sched = schedFact.getScheduler();
			
			JobDetail jobDetail = new JobDetail("DayJob", null, DayScheduler.class);
			CronTrigger trigger = new CronTrigger("DayJobTrigger", "DayJobGroup");
			//trigger.setCronExpression("0 * * * * ?");
			trigger.setCronExpression("5 0 0 * * ?");//每个小时的0分2秒的时候调度
			//trigger.setCronExpression("47 2,7,12,17,22,27,32,37,42,47,52,57 * * * ?");//每五分钟调度
			//
			sched.scheduleJob(jobDetail, trigger);
			sched.start();
			logger.info("每日调度启动成功");
		} catch (SchedulerException e) {
			logger.error("每日调度启动失败", e);
		} catch (ParseException e) {
			logger.error("每日调度启动失败", e);
		}
	}

	public void execute(JobExecutionContext job) throws JobExecutionException {
		
		synchronized (lock) {
			if (isRunning) {
				return;
			} else {
				isRunning = true;
			}
		}
		
		try {
			schedulerService.handleDayScheduler();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("每日调度执行失败", e);
		} finally {
			isRunning = false;
		}
	}
	
	public static void shutdown() {
		try {
			sched.shutdown();
		} catch (SchedulerException e) {
			logger.error("每日调度关闭失败", e);
		}
	}
}
