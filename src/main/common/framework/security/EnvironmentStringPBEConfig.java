package com.chess.common.framework.security;

public class EnvironmentStringPBEConfig extends org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig{
	 public EnvironmentStringPBEConfig()
	  {
	    setAlgorithm("PBEWithMD5AndDES");
	    setPassword(MasterKeyUtil.getMasterKey());
	  }
}
