package com.chess.common.framework.security;

import org.jasypt.util.text.BasicTextEncryptor;

public class MasterKeyUtil {
	
	  public static String getMasterKey()
	  {
	    return "TehTkQNkTGn+4h8BeezsNswkW";
	  }

	  public static String encKey(String src) {
	    BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
	    textEncryptor.setPassword(getMasterKey());
	    return textEncryptor.encrypt(src);
	  }

	  public static String decKey(String src) {
	    BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
	    textEncryptor.setPassword(getMasterKey());
	    return textEncryptor.decrypt(src);
	  }
	  
	  public static void main(String[] args) {
		  String s = "YrwRGpQn2NsHMB+JiVLTi7kRQwLeKF4nNUez8IH4zz9MD/WcdvRpPnHtuB1JTPUSRQhc37tk48qH3cF0ko/imMwXDHwEJ57ZyLotl5sGJ0oFnwwApq5wGQ==";
		  String ss = MasterKeyUtil.decKey(s);
//		  System.out.println(ss);
	}
}
