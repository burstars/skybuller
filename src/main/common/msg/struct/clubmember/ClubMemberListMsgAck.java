package com.chess.common.msg.struct.clubmember; 

import java.util.ArrayList;
import java.util.List;

import org.jacorb.idl.runtime.int_token;

import com.chess.common.bean.ClubMember;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/** 
 * @author wangjia
 * @version 创建时间：2018-8-30 
 * 类说明 :
 */
public class ClubMemberListMsgAck extends MsgBase {
	//0查询所有成员，1查询待处理成员
	public int type = 0;
	public List<ClubMember> members = new ArrayList<ClubMember>();
	
	public ClubMemberListMsgAck() {
		msgCMD = MsgCmdConstant.CLUB_MEMBER_LIST;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		type=ar.sInt(type);
		members = (List<ClubMember>) ar.sObjArray(members);
	}

}
 