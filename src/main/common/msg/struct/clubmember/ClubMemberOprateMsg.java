package com.chess.common.msg.struct.clubmember; 

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/** 
 * @author wangjia
 * @version 创建时间：2018-8-29 
 * 类说明 :俱乐部-成员管理相关
 */
public class ClubMemberOprateMsg  extends MsgBase{
	public int  opertaionID=0;
    public String playerId = "";    
    public String clubCode = "";
    public String memberId = "";    
    public String remark = "";  
    public int applyState = 0;
 
	public ClubMemberOprateMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.CLUB_MEMBER;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		opertaionID=ar.sInt(opertaionID);
		playerId=ar.sString(playerId);
		clubCode=ar.sString(clubCode);
		memberId=ar.sString(memberId);
		remark=ar.sString(remark);
		applyState=ar.sInt(applyState);
	}
}
 