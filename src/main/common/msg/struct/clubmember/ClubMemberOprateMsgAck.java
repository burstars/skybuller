package com.chess.common.msg.struct.clubmember; 

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/** 
 * @author wangjia
 * @version 创建时间：2018-8-30 
 * 类说明 :返回前台申请结果
 */
public class ClubMemberOprateMsgAck extends MsgBase{
	
	public int  opertaionID=0;
	//1 申请人存在 ，0 申请人不存在，2=俱乐部不存在,3=已加入
	public int result =0;
    public String playerId = "";    
    public int playerIndex = 0; 
    public String playerName = "";  
    public String clubCode = "";
    public String memberId = ""; 
    public int memberIndex = 0; 
    public String memberName = ""; 
	
	public ClubMemberOprateMsgAck(){
		 msgCMD=MsgCmdConstant.CLUB_MEMBER_OPRATE_RESULT;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		opertaionID = ar.sInt(opertaionID);
		result = ar.sInt(result);
		playerId=ar.sString(playerId);
		playerIndex = ar.sInt(playerIndex);
		playerName=ar.sString(playerName);
		clubCode=ar.sString(clubCode);
		memberId=ar.sString(memberId);
		memberIndex = ar.sInt(memberIndex);
		memberName=ar.sString(memberName);
	}
}
 