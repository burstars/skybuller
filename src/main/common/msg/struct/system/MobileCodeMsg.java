package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class MobileCodeMsg extends MsgBase {

	//手机号
	public String phoneNO;
	//帐号
	public String account;
	//操作标识符【1-注册帐号;2-找回密码】
	public int operation;

	public MobileCodeMsg() {
		msgCMD = MsgCmdConstant.MOBILE_CODE;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		phoneNO = ar.sString(phoneNO);
		account = ar.sString(account);
		operation = ar.sInt(operation);
	}
}
