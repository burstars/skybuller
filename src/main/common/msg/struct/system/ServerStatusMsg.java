package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**gs的状态，每隔一定时间，gs发送给dbgate，oss可以通过这个参数来分析gs的健康状况***/
public class ServerStatusMsg  extends MsgBase 
{
	
	public int loopCounter=0;
	public int currentMsgNum=0;
	public int msgProcessed=0;
	public int linkNum=0;
	public int msgReceived=0;
	public int msgSend=0;
	//
	public int dbgateMsgSendNum=0;
	public int dbgateMsgReceivedNum=0;
	//
	public int curentMsgCmd=0;
	//
	public String linkName="";
	
 
	//
	public ServerStatusMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.SERVER_STATUS_UPDATE;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		loopCounter=ar.sInt(loopCounter);
		currentMsgNum=ar.sInt(currentMsgNum);
		msgProcessed=ar.sInt(msgProcessed);
		linkNum=ar.sInt(linkNum);
		msgReceived=ar.sInt(msgReceived);
		msgSend=ar.sInt(msgSend);
		//
		dbgateMsgSendNum=ar.sInt(dbgateMsgSendNum);
		dbgateMsgReceivedNum=ar.sInt(dbgateMsgReceivedNum);
		
		curentMsgCmd=ar.sInt(curentMsgCmd);
		linkName=ar.sString(linkName);
 
	}
	//
	public String toStr()
	{
		String ss="name="+linkName+"\n";
		       ss+="loop="+loopCounter+"\n";
		       ss+="currentMsgNum="+currentMsgNum+"\n";
		       ss+="msgProcessed="+msgProcessed+"\n";
		       ss+="linkNum="+linkNum+"\n";
		       ss+="msgReceived="+msgReceived+"\n";
		       ss+="msgSend="+msgSend+"\n";
		       ss+="dbgateMsgSendNum="+dbgateMsgSendNum+"\n";
		       ss+="dbgateMsgReceivedNum="+dbgateMsgReceivedNum+"\n";
		       ss+="curentMsgCmd="+curentMsgCmd+"\n";
		       ss+="===========================\n";

		return ss;
	}
}