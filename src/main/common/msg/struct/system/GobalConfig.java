package com.chess.common.msg.struct.system;

import com.chess.common.bean.SystemConfigPara;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

//把全局参数修改到客户端
public class GobalConfig extends MsgBase 
{
	public SystemConfigPara para = new SystemConfigPara();
	
	//
	public GobalConfig()
	{ 
	  	 msgCMD=MsgCmdConstant.GLOBAL_CONFIG_CLIENT;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		para=(SystemConfigPara) ar.sObject(para);
	}

}