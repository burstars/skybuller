package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**心跳消息包***/
public class HeartBeatingMsg  extends MsgBase 
{
	
	//room123的房间人数
	public int r1Num=0;
	public int r2Num=0;
	public int r3Num=0;
	public int r4Num=0;
	
	public HeartBeatingMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.HEART_BEATING;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		r1Num=ar.sInt(r1Num);
		r2Num=ar.sInt(r2Num);
		r3Num=ar.sInt(r3Num);
		r4Num=ar.sInt(r4Num);
	}
}