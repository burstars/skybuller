package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**大厅断线重链接***/
public class LobbyReconnectMsg  extends MsgBase 
{
	public String playerID="";
	public String userAccount="";
	public String userPassword="";//md5
	
	public LobbyReconnectMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.LOBBY_CLIENT_LINK_RECONNECT;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		playerID=ar.sString(playerID);
		userAccount=ar.sString(userAccount);
		userPassword=ar.sString(userPassword);
		
	}
}