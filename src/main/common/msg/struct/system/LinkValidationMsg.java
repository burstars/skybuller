package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**链接后发送此消息来确认链接的合法性***/
public class LinkValidationMsg  extends MsgBase 
{
	public String checkKey="";
	public String linkName="";
	
	
	public LinkValidationMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.LINK_VALIDATION;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		checkKey=ar.sString(checkKey);
		linkName=ar.sString(linkName);
	}
}