package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class MobileCodeMsgAck  extends MsgBase  {

	/** 获取验证码操作结果 */
	public int result;
	/**错误描述*/
	public String resultStr;
	/** 注册帐号，允许获取手机验证码的时间间隔(s) */
	public int time;
	/**操作标识符【1-注册帐号;2-找回密码】*/
	public int operation;

	public MobileCodeMsgAck() {
		msgCMD = MsgCmdConstant.MOBILE_CODE_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		result = ar.sInt(result);
		resultStr = ar.sString(resultStr);
		time = ar.sInt(time);
		operation = ar.sInt(operation);
	}
}
