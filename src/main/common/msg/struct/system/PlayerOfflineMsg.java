package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**玩家从大厅离线***/
public class PlayerOfflineMsg  extends MsgBase 
{
	 //在大厅下线，通知dbgate；
 
	public String  playerID="";
 
	public PlayerOfflineMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.PLAYER_OFFLINE;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		playerID=ar.sString(playerID);
 
	}
}