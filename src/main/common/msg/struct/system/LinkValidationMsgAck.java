package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**链接后发送此消息来确认链接的合法性***/
public class LinkValidationMsgAck  extends MsgBase 
{
	public String  serverName="";
	public String linkName="";
	
	
	public LinkValidationMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.LINK_VALIDATION_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		serverName=ar.sString(serverName);
		linkName=ar.sString(linkName);
	}
}