package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;


/**
 * 心跳消息包
 ***/
public class HeartBeatingAckMsg extends MsgBase {
    public HeartBeatingAckMsg() {
        msgCMD = MsgCmdConstant.HEART_BEATING_ACK;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
    }
}