package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**某个服务器或者客户端玩家的网络断开消息包***/
public class LinkBrokenMsg  extends MsgBase 
{
	public LinkBrokenMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.LINK_BROKEN;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
	}
}