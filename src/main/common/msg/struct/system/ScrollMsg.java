package com.chess.common.msg.struct.system;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**gs的状态，每隔一定时间，gs发送给dbgate，oss可以通过这个参数来分析gs的健康状况***/
public class ScrollMsg  extends MsgBase 
{
	//消息内容
	public String msg="";
	//客户端循环次数
	public int loopNum=2;
	//客户端是否清除之前的消息
	public int removeAllPreviousMsg=0;//0不移除，1把之前的消息清理掉	
	
	/**以下字段，用于服务端控制*/
	//发送时间
	public long createDate=0L;
	public String sendTime="";	
	//服务端定时发送次数
	public int sendTimes = 0;
	//定时发送频率 0:一次性 1：5分钟一次，2：1小时一次
	public int ontimeType = 0;
	
	
	//发送到哪个玩家组
	public int sendPlayerType = 0;
	
	public ScrollMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_SEND_SCROLL_MES;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		msg=ar.sString(msg);
		loopNum=ar.sInt(loopNum);
		removeAllPreviousMsg=ar.sInt(removeAllPreviousMsg);
 
	}

}