package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 * 请求游戏状态消息
 * @author cuiweiqing 2017-1-5
 *
 */
public class AskRecoverGameMsg extends MsgBase {
	public String gameId="";
	public int roomId=0;
	public int vipTableId=0;
    
    public AskRecoverGameMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_ASK_GAME_RECOVER;
	}
    
    @Override
    public void serialize(ObjSerializer ar) {
    	super.serialize(ar);
    	
    	gameId = ar.sString(gameId);
    	roomId = ar.sInt(roomId);
    	vipTableId = ar.sInt(vipTableId);
    }
}
