/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class VipRoomRecordMsg extends MsgBase {
	/** 玩家ID */
	public String playerID;
	/** 游戏ID */
	public String gameID;
	
	/** 俱乐部id*/
	public int clubCode=0;

	public VipRoomRecordMsg() {
		msgCMD = MsgCmdConstant.GET_VIP_ROOM_RECORD;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		playerID = ar.sString(playerID);
		gameID = ar.sString(gameID);
		clubCode=ar.sInt(clubCode);
	}

}
