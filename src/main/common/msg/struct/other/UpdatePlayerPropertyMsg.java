package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** 服务器通知客户端更新属性***/
public class UpdatePlayerPropertyMsg  extends MsgBase 
{

	/** 玩家货币0 金币*/
	public int  gold=0;
 
	/** 钻石*/
	public int diamond=0;
	/** 积分*/
	public int score=0;
	/** 胜利记录*/
	public int wons=0;
	/** 失败记录*/
	public int loses=0;
	/** 逃跑记录*/
	public int escape=0;
	
	public int life=0; 
	//单机生命产生的cd
	public int serverCD=0;
	
	public UpdatePlayerPropertyMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_UPDATE_PLAYER_PROPERTY;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		gold=ar.sInt(gold);
		diamond=ar.sInt(diamond);
		score=ar.sInt(score);
		wons=ar.sInt(wons);
		loses=ar.sInt(loses);
		escape=ar.sInt(escape);
		life=ar.sInt(life);
		serverCD=ar.sInt(serverCD);
	}
}