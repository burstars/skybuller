/**
 * Description:返回玩家代开的所有房间记录
 */
package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.ProxyVipRoomRecord;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ProxyVipRoomRecordMsgAck  extends MsgBase{
	/** 玩家所有VIP房间记录列表 */
	public List<ProxyVipRoomRecord> proxyRoomRecord = new ArrayList<ProxyVipRoomRecord>();
	/**--记录类型，0：未开始房间，1：全部房间； */
	public int recordType=0 ;
    /**--总页数 */
	public int pageTotalNum = 0;
    /**--当前页码 */
	public int pageCurrentNum = 0;
	
	public ProxyVipRoomRecordMsgAck() {
		msgCMD = MsgCmdConstant.GET_PROXY_VIP_ROOM_RECORD_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		proxyRoomRecord = (List<ProxyVipRoomRecord>) ar.sObjArray(proxyRoomRecord);
		recordType=ar.sInt(recordType);
		pageTotalNum =ar.sInt(pageTotalNum);
		pageCurrentNum = ar.sInt(pageCurrentNum);
	}
}
