package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/** 客户端通知游戏服务器，玩家的某些行为，服务器返回 ***/
public class PlayerGameOpertaionAckMsg extends MsgBase {
	public String playerID = "";
	public String playerName = "";
	public String targetPlayerName = "";
	public int opertaionID = 0;
	public int opValue = 0;
	public int result = 0;

	public int playerIndex = 0;
	public int headImg = 0;
	public int gold = 0;
	public int tablePos = 0;

	public int sex = 0;

	public int canFriend = 0;

	public String ip = "";

	public int showReady = 0;
	public int readyFlag = 0;

	public String headImgUrl = "";
	public String location = "";

	public PlayerGameOpertaionAckMsg() {
		msgCMD = MsgCmdConstant.GAME_GAME_OPT_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);

		playerID = ar.sString(playerID);
		//
		playerName = ar.sString(playerName);
		targetPlayerName = ar.sString(targetPlayerName);
		//
		opertaionID = ar.sInt(opertaionID);
		opValue = ar.sInt(opValue);
		result = ar.sInt(result);

		playerIndex = ar.sInt(playerIndex);
		headImg = ar.sInt(headImg);
		sex = ar.sInt(sex);
		gold = ar.sInt(gold);
		tablePos = ar.sInt(tablePos);

		canFriend = ar.sInt(canFriend);
		ip = ar.sString(ip);
		showReady = ar.sInt(showReady);
		readyFlag = ar.sInt(readyFlag);

		headImgUrl = ar.sString(headImgUrl);
		location = ar.sString(location);
	}
}