package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.UserBackpack;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;


/**
 * 服务器通知客户端更新整个道具列表
 ***/
public class UpdatePlayerItemListMsg extends MsgBase {
    public int gold = 0;
    public List<Integer> items = new ArrayList<Integer>();
    public int credits = 0;
    public UpdatePlayerItemListMsg() {
        msgCMD = MsgCmdConstant.GAME_UPDATE_PLAYER_ITEM_LIST;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        //
        gold = ar.sInt(gold);
        //
        items = (List<Integer>) ar.sIntArray(items);
        credits = ar.sInt(credits);
    }

    public void init_list(List<UserBackpack> pil) {
        for (int i = 0; i < pil.size(); i++) {
            UserBackpack pi = pil.get(i);
            items.add(pi.getItemBaseID());
            items.add(pi.getItemNum());
        }
    }
}