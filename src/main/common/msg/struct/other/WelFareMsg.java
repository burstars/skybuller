package com.chess.common.msg.struct.other;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**道具兑换消息体 
 * 
 * 2018-01-18***/
public class WelFareMsg extends MsgBase 
{
	public String playerID = "";
	public WelFareMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.WELFARE_MSG;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		playerID=ar.sString(playerID);
	}
}




 
 
