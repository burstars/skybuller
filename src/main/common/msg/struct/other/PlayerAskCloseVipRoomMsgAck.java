package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import org.jacorb.idl.runtime.int_token;

import com.chess.common.bean.SimplePlayer;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class PlayerAskCloseVipRoomMsgAck extends MsgBase {
	//消息类型
	public int OPT_id=0;// 1是申请，2是选择，3是最终结果
	//申请者昵称
	public String askerName;
	//桌子id
	public int table_id = 0;
	//同意解散的玩家数量
	public int agree_num = 0;
	public int tablePos = 0;
	//是否操作太频繁了
	public int isoffen = 0;
	//是否已操作（玩家是否已操作，断线的时候发次参数）
	public int isOperate = 0;
	//操作结果(1:关闭窗口继续游戏，)
	public int result = 0;
	//操作等待时间
	public int wait_time = 0;
	//操作剩余时间
	public int lost_time = 0;
	public int roomType = 0;
	
	 public int player1_pos = 0;
	 public String player1_name="";
	 public int player1_result = 0;
	 
	 public int player2_pos = 0;
	 public String player2_name="";
	 public int player2_result = 0;
	 
	 public int player3_pos = 0;
	 public String player3_name="";
	 public int player3_result = 0;
	 public int player4_pos = 0;
	 public String player4_name="";
	 public int player4_result = 0;
	 
	 public int player5_pos = 0;
	 public String player5_name="";
	 public int player5_result = 0;
	 
	 public int player6_pos = 0;
	 public String player6_name="";
	 public int player6_result = 0;
	 
	 public String player1_playerId = "";
	 public String player2_playerId = "";
	 public String player3_playerId = "";
	 public String player4_playerId = "";
	 public String player5_playerId = "";
	 public String player6_playerId = "";
	public PlayerAskCloseVipRoomMsgAck() { 
	  	 msgCMD = MsgCmdConstant.GAME_CLOSE_ROOM_ACK;
	}
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		OPT_id = ar.sInt(OPT_id);
		askerName = ar.sString(askerName);
		table_id = ar.sInt(table_id);
		agree_num = ar.sInt(agree_num);
		tablePos = ar.sInt(tablePos);
		isoffen = ar.sInt(isoffen);
		isOperate = ar.sInt(isOperate);
		result = ar.sInt(result);
		wait_time = ar.sInt(wait_time);
		lost_time = ar.sInt(lost_time);
		roomType =  ar.sInt(roomType);
		
		player1_pos = ar.sInt(player1_pos);
		player1_name = ar.sString(player1_name);
		player1_result =  ar.sInt(player1_result);
		 
		player2_pos = ar.sInt(player2_pos);
		player2_name = ar.sString(player2_name);
		player2_result = ar.sInt(player2_result);
		 
		player3_pos = ar.sInt(player3_pos);
		player3_name = ar.sString(player3_name);
		player3_result = ar.sInt(player3_result);
		
		player4_pos = ar.sInt(player4_pos);
		player4_name = ar.sString(player4_name);
		player4_result = ar.sInt(player4_result);
		
		player5_pos = ar.sInt(player5_pos);
		player5_name = ar.sString(player5_name);
		player5_result = ar.sInt(player5_result);
		
		player6_pos = ar.sInt(player6_pos);
		player6_name = ar.sString(player6_name);
		player6_result = ar.sInt(player6_result);
		
		player1_playerId = ar.sString(player1_playerId);
		player2_playerId = ar.sString(player2_playerId);
		player3_playerId = ar.sString(player3_playerId);
		player4_playerId = ar.sString(player4_playerId);
		player5_playerId = ar.sString(player5_playerId);
		player6_playerId = ar.sString(player6_playerId);
	}
}
