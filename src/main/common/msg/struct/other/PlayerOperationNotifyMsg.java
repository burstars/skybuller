package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.constant.GameConstant;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/** 游戏服务器通知客户端，轮到玩家操作了 ***/
public class PlayerOperationNotifyMsg extends MsgBase {
	public int operation = 0;
	public int player_table_pos = 0;
	public int target_card = 0;
	
	public int chi_card_value = 0;// 吃、强制解散参数
	public int peng_card_value = 0;// 碰
	
	public int fan_type = 0;//番数类型

	// -->TODO: 各种杠牌信息， 新增的这些客户端也要对应增加 add by  2016.7.22
	public int ming_gang_card_value = 0;// 明杠
	public int an_gang_card_value = 0;
	public int one_egg_card_value = 0;
	public int nine_egg_card_value = 0;
	public int feng_gang_card_value = 0;
	public int happy_gang_card_value = 0;
	public int special_gang_card_value = 0;
	public int add_gang_card_value = 0;
	public int facai_gang_card_value = 0;
	public int chi_gang_card_value = 0;// 吃杠

	
	public int cardLeftNum = 0;// 剩余多少张牌
	// 吃听标识位
	public int chi_flag = 0;
	// 吃碰杠听胡谁的牌 add by  2016.9.6
	public int out_card_player_index = -1;

	// 打出哪些牌可以听
	public List<Byte> tingList = new ArrayList<Byte>();

	/**
	 * -->TODO 补杠信息缓存 cuiweiqing 2016-08-04
	 */
	public List<Byte> addNormalGangValues = new ArrayList<Byte>();
	public List<Byte> addSpecialGangValues = new ArrayList<Byte>();
	public List<Integer> normalAddGangIndexes = new ArrayList<Integer>();
	public List<Integer> specialAddGangIndexes = new ArrayList<Integer>();
	
	//预留Param01-int
	public int player0Parm01=-1;
	public int player1Parm01=-1;
	public int player2Parm01=-1;
	public int player3Parm01=-1;
	
	public List<Integer> player0Param01List = new ArrayList<Integer>();
	public List<Integer> player1Param01List = new ArrayList<Integer>();
	public List<Integer> player2Param01List = new ArrayList<Integer>();
	public List<Integer> player3Param01List = new ArrayList<Integer>();
	
	public List<Byte> player0Param02List = new ArrayList<Byte>();
	public List<Byte> player1Param02List = new ArrayList<Byte>();
	public List<Byte> player2Param02List = new ArrayList<Byte>();
	public List<Byte> player3Param02List = new ArrayList<Byte>();
	
	public List<Integer> one_gang_list = new ArrayList<Integer>();
	public List<Integer> nine_gang_list = new ArrayList<Integer>();
	public List<Integer> feng_gang_list = new ArrayList<Integer>();
	public String huanHuiDowns = "";
	
	/**扑扛的牌*/
//	public List<Byte> pu_gang_list = new ArrayList<Byte>();
	/**扑扛待选择的牌*/
	public List<Byte> pu_gang_sel_list = new ArrayList<Byte>();
	//
	public PlayerOperationNotifyMsg() {
		msgCMD = MsgCmdConstant.GAME_USER_OPERATION_NOTIFY;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		//
		operation = ar.sInt(operation);
		player_table_pos = ar.sInt(player_table_pos);
		
		chi_card_value = ar.sInt(chi_card_value);
		peng_card_value=ar.sInt(peng_card_value);
		//
		ming_gang_card_value = ar.sInt(ming_gang_card_value);
		an_gang_card_value = ar.sInt(an_gang_card_value);
		one_egg_card_value = ar.sInt(one_egg_card_value);
		nine_egg_card_value = ar.sInt(nine_egg_card_value);
		feng_gang_card_value = ar.sInt(feng_gang_card_value);
		happy_gang_card_value = ar.sInt(happy_gang_card_value);
		special_gang_card_value = ar.sInt(special_gang_card_value);
		add_gang_card_value = ar.sInt(add_gang_card_value);
		facai_gang_card_value = ar.sInt(facai_gang_card_value);
		chi_gang_card_value = ar.sInt(chi_gang_card_value);
		
		target_card = ar.sInt(target_card);
		cardLeftNum = ar.sInt(cardLeftNum);
		fan_type = ar.sInt(fan_type);

		chi_flag = ar.sInt(chi_flag);
		out_card_player_index = ar.sInt(out_card_player_index);
		//
		tingList = (List<Byte>) ar.sByteArray(tingList);
		
		addNormalGangValues = (List<Byte>) ar.sByteArray(addNormalGangValues);
		addSpecialGangValues = (List<Byte>) ar.sByteArray(addSpecialGangValues);
		normalAddGangIndexes = (List<Integer>) ar
				.sIntArray(normalAddGangIndexes);
		specialAddGangIndexes = (List<Integer>) ar
				.sIntArray(specialAddGangIndexes);
		
		player0Parm01 = ar.sInt(player0Parm01);
		player1Parm01 = ar.sInt(player1Parm01);
		player2Parm01 = ar.sInt(player2Parm01);
		player3Parm01 = ar.sInt(player3Parm01);
		
		player0Param01List = (List<Integer>) ar.sIntArray(player0Param01List);
		player1Param01List = (List<Integer>) ar.sIntArray(player1Param01List);
		player2Param01List = (List<Integer>) ar.sIntArray(player2Param01List);
		player3Param01List = (List<Integer>) ar.sIntArray(player3Param01List);
		
		player0Param02List = (List<Byte>) ar.sByteArray(player0Param02List);
		player1Param02List = (List<Byte>) ar.sByteArray(player1Param02List);
		player2Param02List = (List<Byte>) ar.sByteArray(player2Param02List);
		player3Param02List = (List<Byte>) ar.sByteArray(player3Param02List);
		one_gang_list = (List<Integer>) ar.sIntArray(one_gang_list);
		nine_gang_list = (List<Integer>) ar.sIntArray(nine_gang_list);
		feng_gang_list = (List<Integer>) ar.sIntArray(feng_gang_list);
		huanHuiDowns = ar.sString(huanHuiDowns);
		
//		pu_gang_list = (List<Byte>) ar.sByteArray(pu_gang_list);
		pu_gang_sel_list = (List<Byte>) ar.sByteArray(pu_gang_sel_list);
	}
	
	public int getOperation() {
		return operation;
	}

	public void setOperation(int operation) {
		this.operation = operation;
	}

	public int getPlayer_table_pos() {
		return player_table_pos;
	}

	public void setPlayer_table_pos(int player_table_pos) {
		this.player_table_pos = player_table_pos;
	}

	public int getTarget_card() {
		return target_card;
	}

	public void setTarget_card(int target_card) {
		this.target_card = target_card;
	}

	public int getChi_card_value() {
		return chi_card_value;
	}

	public void setChi_card_value(int chi_card_value) {
		this.chi_card_value = chi_card_value;
	}

	public int getPeng_card_value() {
		return peng_card_value;
	}

	public void setPeng_card_value(int peng_card_value) {
		this.peng_card_value = peng_card_value;
	}

	public int getFan_type() {
		return fan_type;
	}

	public void setFan_type(int fan_type) {
		this.fan_type = fan_type;
	}

	public int getMing_gang_card_value() {
		return ming_gang_card_value;
	}

	public void setMing_gang_card_value(int ming_gang_card_value) {
		this.ming_gang_card_value = ming_gang_card_value;
	}

	public int getAn_gang_card_value() {
		return an_gang_card_value;
	}

	public void setAn_gang_card_value(int an_gang_card_value) {
		this.an_gang_card_value = an_gang_card_value;
	}

	public int getOne_egg_card_value() {
		return one_egg_card_value;
	}

	public void setOne_egg_card_value(int one_egg_card_value) {
		this.one_egg_card_value = one_egg_card_value;
	}

	public int getNine_egg_card_value() {
		return nine_egg_card_value;
	}

	public void setNine_egg_card_value(int nine_egg_card_value) {
		this.nine_egg_card_value = nine_egg_card_value;
	}

	public int getFeng_gang_card_value() {
		return feng_gang_card_value;
	}

	public void setFeng_gang_card_value(int feng_gang_card_value) {
		this.feng_gang_card_value = feng_gang_card_value;
	}

	public int getHappy_gang_card_value() {
		return happy_gang_card_value;
	}

	public void setHappy_gang_card_value(int happy_gang_card_value) {
		this.happy_gang_card_value = happy_gang_card_value;
	}

	public int getSpecial_gang_card_value() {
		return special_gang_card_value;
	}

	public void setSpecial_gang_card_value(int special_gang_card_value) {
		this.special_gang_card_value = special_gang_card_value;
	}

	public int getAdd_gang_card_value() {
		return add_gang_card_value;
	}

	public void setAdd_gang_card_value(int add_gang_card_value) {
		this.add_gang_card_value = add_gang_card_value;
	}

	public int getFacai_gang_card_value() {
		return facai_gang_card_value;
	}

	public void setFacai_gang_card_value(int facai_gang_card_value) {
		this.facai_gang_card_value = facai_gang_card_value;
	}

	public int getChi_gang_card_value() {
		return chi_gang_card_value;
	}

	public void setChi_gang_card_value(int chi_gang_card_value) {
		this.chi_gang_card_value = chi_gang_card_value;
	}

	public int getCardLeftNum() {
		return cardLeftNum;
	}

	public void setCardLeftNum(int cardLeftNum) {
		this.cardLeftNum = cardLeftNum;
	}

	public int getChi_flag() {
		return chi_flag;
	}

	public void setChi_flag(int chi_flag) {
		this.chi_flag = chi_flag;
	}

	public int getOut_card_player_index() {
		return out_card_player_index;
	}

	public void setOut_card_player_index(int out_card_player_index) {
		this.out_card_player_index = out_card_player_index;
	}

	public List<Byte> getTingList() {
		return tingList;
	}

	public void setTingList(List<Byte> tingList) {
		this.tingList = tingList;
	}

	public List<Byte> getAddNormalGangValues() {
		return addNormalGangValues;
	}

	public void setAddNormalGangValues(List<Byte> addNormalGangValues) {
		this.addNormalGangValues = addNormalGangValues;
	}

	public List<Byte> getAddSpecialGangValues() {
		return addSpecialGangValues;
	}

	public void setAddSpecialGangValues(List<Byte> addSpecialGangValues) {
		this.addSpecialGangValues = addSpecialGangValues;
	}

	public List<Integer> getNormalAddGangIndexes() {
		return normalAddGangIndexes;
	}

	public void setNormalAddGangIndexes(List<Integer> normalAddGangIndexes) {
		this.normalAddGangIndexes = normalAddGangIndexes;
	}

	public List<Integer> getSpecialAddGangIndexes() {
		return specialAddGangIndexes;
	}

	public void setSpecialAddGangIndexes(List<Integer> specialAddGangIndexes) {
		this.specialAddGangIndexes = specialAddGangIndexes;
	}

	public int getPlayer0Parm01() {
		return player0Parm01;
	}

	public void setPlayer0Parm01(int player0Parm01) {
		this.player0Parm01 = player0Parm01;
	}

	public int getPlayer1Parm01() {
		return player1Parm01;
	}

	public void setPlayer1Parm01(int player1Parm01) {
		this.player1Parm01 = player1Parm01;
	}

	public int getPlayer2Parm01() {
		return player2Parm01;
	}

	public void setPlayer2Parm01(int player2Parm01) {
		this.player2Parm01 = player2Parm01;
	}

	public int getPlayer3Parm01() {
		return player3Parm01;
	}

	public void setPlayer3Parm01(int player3Parm01) {
		this.player3Parm01 = player3Parm01;
	}

	public List<Integer> getPlayer0Param01List() {
		return player0Param01List;
	}

	public void setPlayer0Param01List(List<Integer> player0Param01List) {
		this.player0Param01List = player0Param01List;
	}

	public List<Integer> getPlayer1Param01List() {
		return player1Param01List;
	}

	public void setPlayer1Param01List(List<Integer> player1Param01List) {
		this.player1Param01List = player1Param01List;
	}

	public List<Integer> getPlayer2Param01List() {
		return player2Param01List;
	}

	public void setPlayer2Param01List(List<Integer> player2Param01List) {
		this.player2Param01List = player2Param01List;
	}

	public List<Integer> getPlayer3Param01List() {
		return player3Param01List;
	}

	public void setPlayer3Param01List(List<Integer> player3Param01List) {
		this.player3Param01List = player3Param01List;
	}

	public List<Byte> getPlayer0Param02List() {
		return player0Param02List;
	}

	public void setPlayer0Param02List(List<Byte> player0Param02List) {
		this.player0Param02List = player0Param02List;
	}

	public List<Byte> getPlayer1Param02List() {
		return player1Param02List;
	}

	public void setPlayer1Param02List(List<Byte> player1Param02List) {
		this.player1Param02List = player1Param02List;
	}

	public List<Byte> getPlayer2Param02List() {
		return player2Param02List;
	}

	public void setPlayer2Param02List(List<Byte> player2Param02List) {
		this.player2Param02List = player2Param02List;
	}

	public List<Byte> getPlayer3Param02List() {
		return player3Param02List;
	}

	public void setPlayer3Param02List(List<Byte> player3Param02List) {
		this.player3Param02List = player3Param02List;
	}

	public List<Integer> getOne_gang_list() {
		return one_gang_list;
	}

	public void setOne_gang_list(List<Integer> one_gang_list) {
		this.one_gang_list = one_gang_list;
	}

	public List<Integer> getNine_gang_list() {
		return nine_gang_list;
	}

	public void setNine_gang_list(List<Integer> nine_gang_list) {
		this.nine_gang_list = nine_gang_list;
	}

	public List<Integer> getFeng_gang_list() {
		return feng_gang_list;
	}

	public void setFeng_gang_list(List<Integer> feng_gang_list) {
		this.feng_gang_list = feng_gang_list;
	}

	public String getHuanHuiDowns() {
		return huanHuiDowns;
	}

	public void setHuanHuiDowns(String huanHuiDowns) {
		this.huanHuiDowns = huanHuiDowns;
	}

	public List<Byte> getPu_gang_sel_list() {
		return pu_gang_sel_list;
	}

	public void setPu_gang_sel_list(List<Byte> pu_gang_sel_list) {
		this.pu_gang_sel_list = pu_gang_sel_list;
	}
	
	
}