package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class HuDongBiaoQingMsg extends MsgBase {
	
	// 发表情玩家座位号
	public int playerPos_send = 0;
	// 接受表情玩家座位号
	public int playerPos_recv = 0;
	// 表情名字
	public String biaoqing = "";
	
	public HuDongBiaoQingMsg()
	{
		this.msgCMD = MsgCmdConstant.HU_DONG_BIAO_QING;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		
		playerPos_send = ar.sInt(playerPos_send);
		playerPos_recv = ar.sInt(playerPos_recv);
		biaoqing = ar.sString(biaoqing);
	}
}
