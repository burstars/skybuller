/**
 * Description:响应-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.VipRoomRecord;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class VipRoomRecordAckMsg extends MsgBase {
	/** 玩家所有VIP房间记录列表 */
	public List<VipRoomRecord> roomRecords = new ArrayList<VipRoomRecord>();

	public VipRoomRecordAckMsg() {
		msgCMD = MsgCmdConstant.GET_VIP_ROOM_RECORD_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		roomRecords = (List<VipRoomRecord>) ar.sObjArray(roomRecords);
	}

}
