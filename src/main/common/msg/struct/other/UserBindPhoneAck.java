package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class UserBindPhoneAck extends MsgBase {


    public UserBindPhoneAck()
    {
        msgCMD= MsgCmdConstant.REQUEST_BIND_PHONE_ACK;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
    }
}
