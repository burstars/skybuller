package com.chess.common.msg.struct.other;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**玩家绑定推荐人***/
public class UserExtendMsg extends MsgBase 
{
	public String playerID = "";//玩家id
	public String mgrID = "";//推荐人id
	public String mgrIndex = "";//推荐人index
	
	public UserExtendMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.USER_EXTEND_GOLD_MSG;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		playerID=ar.sString(playerID);
		mgrID=ar.sString(mgrID);
		mgrIndex=ar.sString(mgrIndex);
	}
}




 
 
