package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.bean.SimplePlayer;
import com.chess.common.bean.User;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.nndzz.table.GameTable;

/** 游戏结束 ***/
public class PlayerGameOverMsgAck extends MsgBase{
	private static Logger logger = LoggerFactory
			.getLogger(PlayerGameOverMsgAck.class);

	public int roomID = 0;
	public List<SimplePlayer> players = new ArrayList<SimplePlayer>();

	public int dealerPos = 0;// 庄家位置
	// 赢家手牌
	public List<Byte> cards = new ArrayList<Byte>();
	// 胡的牌
	public int huCard = 0;
	public int stage = 0;// 一般这个是0，如果是1表示这个是vip锅结束
	public int isVipTable; // 是否VIP桌子
	public int readyTime = 15; // 准备时间

	// 宝牌
	public int baoCard = 0;
	public int huiCard = 0;// $$PJ$$
	public int moHuiCard = 0;// $$PJ$$

	// 胡牌位置
	public int huPos = 0;

	// 玩家手牌
	public List<Byte> player0HandCards = new ArrayList<Byte>();
	public List<Byte> player1HandCards = new ArrayList<Byte>();
	public List<Byte> player2HandCards = new ArrayList<Byte>();
	public List<Byte> player3HandCards = new ArrayList<Byte>();

	// 吃碰杠的牌
	public List<Integer> player0DownCards = new ArrayList<Integer>();
	public List<Integer> player1DownCards = new ArrayList<Integer>();
	public List<Integer> player2DownCards = new ArrayList<Integer>();
	public List<Integer> player3DownCards = new ArrayList<Integer>();

	// 补杠的牌
	public List<Integer> player0addGangs = new ArrayList<Integer>();
	public List<Integer> player1addGangs = new ArrayList<Integer>();
	public List<Integer> player2addGangs = new ArrayList<Integer>();
	public List<Integer> player3addGangs = new ArrayList<Integer>();

	// 暗杠的牌
	public List<Integer> player0anGangs = new ArrayList<Integer>();
	public List<Integer> player1anGangs = new ArrayList<Integer>();
	public List<Integer> player2anGangs = new ArrayList<Integer>();
	public List<Integer> player3anGangs = new ArrayList<Integer>();

	// 特殊杠的牌 传到前台，处理特殊杠高地位调转 cuiweiqing 2016-08-15
	public List<Integer> player0SpecialGangs = new ArrayList<Integer>();
	public List<Integer> player1SpecialGangs = new ArrayList<Integer>();
	public List<Integer> player2SpecialGangs = new ArrayList<Integer>();
	public List<Integer> player3SpecialGangs = new ArrayList<Integer>();

	// 支对扣牌
	public int player0ZhiDuiCloseCard = 0;
	public int player1ZhiDuiCloseCard = 0;
	public int player2ZhiDuiCloseCard = 0;
	public int player3ZhiDuiCloseCard = 0;
	
	//点杠的牌#@HLH@#
	public List<Integer> player0DianGangs = new ArrayList<Integer>();
	public List<Integer> player1DianGangs = new ArrayList<Integer>();
	public List<Integer> player2DianGangs = new ArrayList<Integer>();
	public List<Integer> player3DianGangs = new ArrayList<Integer>();
	
	
	//下蛋的牌
	public List<Integer> player0Xiadans = new ArrayList<Integer>();
	public List<Integer> player1Xiadans = new ArrayList<Integer>();
	public List<Integer> player2Xiadans = new ArrayList<Integer>();
	public List<Integer> player3Xiadans = new ArrayList<Integer>();

	// 小鸡下蛋
	// 位置
	public int eggPosition = 0;
	// 数量
	public int eggCount = 0;
	// 是否是牌局结束
	public int bOver = 0;

	// 胡牌后抓来兑奖的牌
	public List<Byte> duiJiangCards = new ArrayList<Byte>();

	public PlayerGameOverMsgAck() {
		msgCMD = MsgCmdConstant.GAME_GAME_OVER_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		//
		roomID = ar.sInt(roomID);
		//
		players = (List<SimplePlayer>) ar.sObjArray(players);
		dealerPos = ar.sInt(dealerPos);
		// cards=(List<Byte>)ar.sByteArray(cards);
		huCard = ar.sInt(huCard);
		System.out
				.println("------***********---------PlayerGameOverMsgAck serialize:胡牌值为"
						+ huCard);
		stage = ar.sInt(stage);
		isVipTable = ar.sInt(isVipTable);
		readyTime = ar.sInt(readyTime);

		baoCard = ar.sInt(baoCard);
		huiCard = ar.sInt(huiCard);// $$PJ$$
		moHuiCard = ar.sInt(moHuiCard);// $$PJ$$

		huPos = ar.sInt(huPos);

		player0HandCards = (List<Byte>) ar.sByteArray(player0HandCards);
		player1HandCards = (List<Byte>) ar.sByteArray(player1HandCards);
		player2HandCards = (List<Byte>) ar.sByteArray(player2HandCards);
		player3HandCards = (List<Byte>) ar.sByteArray(player3HandCards);

		player0DownCards = (List<Integer>) ar.sIntArray(player0DownCards);
		player1DownCards = (List<Integer>) ar.sIntArray(player1DownCards);
		player2DownCards = (List<Integer>) ar.sIntArray(player2DownCards);
		player3DownCards = (List<Integer>) ar.sIntArray(player3DownCards);

		player0addGangs = (List<Integer>) ar.sIntArray(player0addGangs);
		player1addGangs = (List<Integer>) ar.sIntArray(player1addGangs);
		player2addGangs = (List<Integer>) ar.sIntArray(player2addGangs);
		player3addGangs = (List<Integer>) ar.sIntArray(player3addGangs);

		player0anGangs = (List<Integer>) ar.sIntArray(player0anGangs);
		player1anGangs = (List<Integer>) ar.sIntArray(player1anGangs);
		player2anGangs = (List<Integer>) ar.sIntArray(player2anGangs);
		player3anGangs = (List<Integer>) ar.sIntArray(player3anGangs);

		player0SpecialGangs = (List<Integer>) ar.sIntArray(player0SpecialGangs);
		player1SpecialGangs = (List<Integer>) ar.sIntArray(player1SpecialGangs);
		player2SpecialGangs = (List<Integer>) ar.sIntArray(player2SpecialGangs);
		player3SpecialGangs = (List<Integer>) ar.sIntArray(player3SpecialGangs);

		player0ZhiDuiCloseCard = ar.sInt(player0ZhiDuiCloseCard);
		player1ZhiDuiCloseCard = ar.sInt(player1ZhiDuiCloseCard);
		player2ZhiDuiCloseCard = ar.sInt(player2ZhiDuiCloseCard);
		player3ZhiDuiCloseCard = ar.sInt(player3ZhiDuiCloseCard);
		
		//#@HLH@#
		player0DianGangs = (List<Integer>)ar.sIntArray(player0DianGangs);
		player1DianGangs = (List<Integer>)ar.sIntArray(player1DianGangs);
		player2DianGangs = (List<Integer>)ar.sIntArray(player2DianGangs);
		player3DianGangs = (List<Integer>)ar.sIntArray(player3DianGangs);

		eggPosition = ar.sInt(eggPosition);
		eggCount = ar.sInt(eggCount);
		duiJiangCards = (List<Byte>) ar.sByteArray(duiJiangCards);
		bOver = ar.sInt(bOver);
		
		player0Xiadans = (List<Integer>)ar.sIntArray(player0Xiadans);
		player1Xiadans = (List<Integer>)ar.sIntArray(player1Xiadans);
		player2Xiadans = (List<Integer>)ar.sIntArray(player2Xiadans);
		player3Xiadans = (List<Integer>)ar.sIntArray(player3Xiadans);
	}


	//
	public void init_players_2(List<User> lps, GameTable gt, int init_took_gold) {
		if (lps == null)
			return;
		//
		for (int i = 0; i < lps.size(); i++) {
			User pl = lps.get(i);
			SimplePlayer sp = new SimplePlayer();
			sp.playerID = pl.getPlayerID();
			sp.palyerIndex = pl.getPlayerIndex();
			sp.playerName = pl.getPlayerName();
			sp.headImg = pl.getHeadImg();
			sp.sex = pl.getSex();

			sp.gold = pl.getVipTableGold() - init_took_gold;

			sp.fan = 0;
			sp.tablePos = pl.getTablePos();
			sp.gameResult = 0;
			sp.canFriend = pl.getCanFriend();
			sp.desc = "";
			sp.ip = pl.getClientIP();
			sp.location = pl.getLocation();
			players.add(sp);
		}
	}
	
	
}