/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.other;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class RecordMsg extends MsgBase {

	public String playerId;
	public String gameId;
	
	public RecordMsg() {
		msgCMD = MsgCmdConstant.GAME_RECORD_DEFINE;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		playerId=ar.sString(playerId);
		gameId=ar.sString(gameId);
	}

}
