package com.chess.common.msg.struct.other;

import com.chess.common.bean.SimplePlayer;
import com.chess.common.bean.User;
import com.chess.common.constant.GameConstant;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class UpdateGameTableGoldAck extends MsgBase{
    public int tablePos=0;
    public List<SimplePlayer> players=new ArrayList<SimplePlayer>();

    public UpdateGameTableGoldAck()
    {
        msgCMD= MsgCmdConstant.UPDATE_GAMETABLE_GOLD;
    }

    @Override
    public void serialize(ObjSerializer ar)
    {
        super.serialize(ar);
        tablePos=ar.sInt(tablePos);
        players=(List<SimplePlayer>)ar.sObjArray(players);
    }
    //
    public void init_players(List<User> lps, boolean isVipTable)
    {
        if(lps==null)
            return;
        for(int i=0;i<lps.size();i++)
        {
            User pl=lps.get(i);
            //�ڵȴ�δȷ���Ƿ������һ��
            if(pl.getGameState()== GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE)
            {
                continue;
            }
            //
            SimplePlayer sp=new SimplePlayer();
            sp.playerID=pl.getPlayerID();
            sp.palyerIndex=pl.getPlayerIndex();
            sp.playerName=pl.getPlayerName();
            sp.headImg=pl.getHeadImg();
            //
            if(isVipTable)
                sp.gold=pl.getVipTableGold();
            else
                sp.gold=pl.getGold();
            //
            sp.tablePos=pl.getTablePos();
            sp.canFriend = pl.getCanFriend();
            sp.sex = pl.getSex();
            sp.inTable = pl.getOnTable() ? 1 : 0;
            sp.ip = pl.getClientIP();
            sp.location = pl.getLocation();
            players.add(sp);
        }
    }
}