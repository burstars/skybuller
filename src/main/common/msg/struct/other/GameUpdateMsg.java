package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**客户端通知游戏服务器，玩家得分进展***/
public class GameUpdateMsg  extends MsgBase 
{
	public int score=0;
	public String playerName="";
	// 进阶场随机任务需统计的数据类型 0 是不用提交
	public int taskDataType = 0;
	//该局游戏中进阶场随机任务统计的数据
	public int taskTouchNum = 0;
	//游戏中最大combo数
	public int gameMaxCombo = 0;
	//游戏中达到最大combo数的次数
	public int maxComboNum = 0;
	public GameUpdateMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_GAME_UPDATE;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		score=ar.sInt(score);
		playerName=ar.sString(playerName);
		taskDataType = ar.sInt(taskDataType);
		taskTouchNum  = ar.sInt(taskTouchNum);
		gameMaxCombo = ar.sInt(gameMaxCombo);
		maxComboNum = ar.sInt(maxComboNum);
	}
}