package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

import org.apache.xpath.operations.Bool;

/**
 */
public class ShowNoticeOnLoginAck extends MsgBase {

    public int show=0;
    public int full_screen = 0;
    public int show_time = 0;
    public int delay = 0;
    public int auto_close = 0;

    public ShowNoticeOnLoginAck() {
        msgCMD = MsgCmdConstant.SHOW_NOTICE_ONLOGIN;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        show=ar.sInt(show);
        full_screen=ar.sInt(full_screen);
        show_time=ar.sInt(show_time);
        delay=ar.sInt(delay);
        auto_close=ar.sInt(auto_close);
    }
}
