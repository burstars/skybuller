package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**玩家绑定推荐人
 * lxw
 * 2018-01-23***/
public class UserExtendMsgAck extends MsgBase 
{
	public String playerID = "";//玩家id
	public int result = 0;//是否成功0：失败，1：成功
	
		
	public UserExtendMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.USER_EXTEND_GOLD_MSG_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		playerID=ar.sString(playerID);
		result = ar.sInt(result);			
	}
}




 
 
