package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.UserBackpack;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** 服务器通知客户端更新整个道具列表***/
public class RequestPlayerHuTypeMsg  extends MsgBase 
{
	public RequestPlayerHuTypeMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_USER_HU_TYPE;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
	}
}