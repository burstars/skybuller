package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 准备：牌局开始前，等待所有玩家都准备，房主才能开局
 * @author  2017-2-8
 */
public class GameReadyMsg extends MsgBase 
{
	/**操作类型:0=为准备；1=准备*/
	public int opt_id = 0;
	/**游戏id*/
	public String game_id = "";
	/**房间id*/
	public int room_id = 0;
	/**桌子id*/
	public int table_id = 0;
	/**玩家id*/
	public String player_id = "";
	
	public GameReadyMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_READY;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		opt_id = ar.sInt(opt_id);
		game_id = ar.sString(game_id);
		room_id = ar.sInt(room_id);
		table_id = ar.sInt(table_id);		
		player_id = ar.sString(player_id);
	}
}