package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class PlayerBindPhoneNumber extends MsgBase {

    public String phone_number = "";
    public String comfirm_code = "";
    public String identify_card = "";
    public String wx_num = "";
    public String qq_num = "";
    public String name = "";
    public String birthday = "";
    public String player_address = "";


    public PlayerBindPhoneNumber()
    {
        msgCMD= MsgCmdConstant.REQUEST_BIND_PHONE;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        phone_number = ar.sString(phone_number);
        comfirm_code = ar.sString(comfirm_code);
        identify_card = ar.sString(identify_card);
        wx_num = ar.sString(wx_num);
        qq_num = ar.sString(qq_num);
        name = ar.sString(name);
        birthday = ar.sString(birthday);
        player_address = ar.sString(player_address);
    }
}
