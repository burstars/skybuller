package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**客户端通知游戏服务器，玩家的某些行为***/
public class PlayerGameOpertaionMsg  extends MsgBase 
{
	public String gameId="";
	public int opertaionID=0;
	public int opValue=0;
	public PlayerGameOpertaionMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_GAME_OPERTAION;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		gameId = ar.sString(gameId);
		opertaionID=ar.sInt(opertaionID);
		opValue=ar.sInt(opValue);
	}
}