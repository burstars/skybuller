package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

import java.util.List;

/**
 */
public class ShowNoticeOnCreateVipAck extends MsgBase{
    public int show=0;

    public ShowNoticeOnCreateVipAck() {
        msgCMD = MsgCmdConstant.SHOW_NOTICE_ONCREATEVIP;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        show=ar.sInt(show);
    }
}
