/**
 * Description:响应-玩家指定VIP房间中所有游戏记录
 */
package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.VipGameRecord;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class VipGameRecordAckMsg extends MsgBase {
	/** 玩家指定VIP房间中所有游戏记录列表 */
	public List<VipGameRecord> gameRecords = new ArrayList<VipGameRecord>();

	public VipGameRecordAckMsg() {
		msgCMD = MsgCmdConstant.GET_VIP_GAME_RECORD_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		this.gameRecords = (List<VipGameRecord>) ar.sObjArray(gameRecords);
	}

}
