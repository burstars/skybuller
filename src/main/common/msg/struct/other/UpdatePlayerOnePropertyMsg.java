package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** 服务器通知客户端更新一个属性***/
public class UpdatePlayerOnePropertyMsg  extends MsgBase 
{
	/** 玩家货币0 金币*/
	public static final int  PLAYER_PROPERTY_gold=0;
	/** 钻石*/
	public static final int PLAYER_PROPERTY_diamond=1;
	/** 积分*/
	public static final int PLAYER_PROPERTY_score=2;
	/** 胜利记录*/
	public static final int PLAYER_PROPERTY_wons=3;
	/** 失败记录*/
	public static final int PLAYER_PROPERTY_loses=4;
	/** 逃跑记录*/
	public static final int PLAYER_PROPERTY_escape=5;
	public static final int PLAYER_PROPERTY_life=6; 
	//单机生命产生的cd
	public static final int PLAYER_PROPERTY_serverCD=7;
	
 

	public int  which_property=0;

	public int value=0;
	
	//room123的房间人数
	public int r1Num=0;
	public int r2Num=0;
	public int r3Num=0;
	public int r4Num=0;
	
	public UpdatePlayerOnePropertyMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_UPDATE_PLAYER_ONE_PROPERTY;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		which_property=ar.sInt(which_property);
		value=ar.sInt(value);
		r1Num=ar.sInt(r1Num);
		r2Num=ar.sInt(r2Num);
		r3Num=ar.sInt(r3Num);
		r4Num=ar.sInt(r4Num);
	}
}