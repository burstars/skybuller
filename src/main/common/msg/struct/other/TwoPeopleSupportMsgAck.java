package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class TwoPeopleSupportMsgAck extends MsgBase {
    public int two_people_support = 0;

    public TwoPeopleSupportMsgAck() {
        msgCMD = MsgCmdConstant.TWO_PEOPLE_SUPPORT;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        two_people_support = ar.sInt(two_people_support);
    }
}
