package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**游戏结束***/
public class PlayerGameOverMsg  extends MsgBase 
{

	public int score=0;
	
	public PlayerGameOverMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_GAME_OVER;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		score=ar.sInt(score);
	}
}