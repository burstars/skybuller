package com.chess.common.msg.struct.other;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class SearchUserByIndexMsg extends MsgBase 
{
	public String searchKey = "";//玩家id
	
	public SearchUserByIndexMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.SEARCH_USER_INDEX;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		searchKey=ar.sString(searchKey);
	}
}




 
 
