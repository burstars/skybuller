package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class TalkingInGameMsg extends MsgBase {
	
	//说话玩家的座位号
	public int playerPos = 0;
	//性别
	public int playerSex = 0;
	//说话类型  0：系统自带快捷语音 1：系统表情 3:自定义文字
	public int msgType = 0;
	//快捷语音编号、表情编号
	public int msgNo = 0;
	//自定义文字内容
	public String msgText = "";
	
	public String gameID="";
	
	public TalkingInGameMsg()
	{
		this.msgCMD = MsgCmdConstant.TALKING_IN_GAME;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		
		playerPos = ar.sInt(playerPos);
		playerSex = ar.sInt(playerSex);
		msgType = ar.sInt(msgType);
		msgNo = ar.sInt(msgNo);
		msgText = ar.sString(msgText);
		gameID = ar.sString(gameID);
	}
}
