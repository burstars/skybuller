package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.PlayerHuType;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** 服务器通知客户端更新整个道具列表***/
public class RequestPlayerHuTypeMsgAck  extends MsgBase 
{
	public List<PlayerHuType> playerHuTypes=new ArrayList<PlayerHuType>();
	
	public RequestPlayerHuTypeMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_USER_HU_TYPE_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		playerHuTypes=(List<PlayerHuType> )ar.sObjArray(playerHuTypes);
		
	}
}