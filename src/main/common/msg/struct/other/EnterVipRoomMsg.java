/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class EnterVipRoomMsg extends MsgBase {

	public String tableID="";
	public String psw="";
	public int roomID=0;
	/**俱乐部编号   2018-08-22*/
	public int clubCode = 0;
	//
	public EnterVipRoomMsg() {
		msgCMD = MsgCmdConstant.GAME_ENTER_VIP_ROOM;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		tableID=ar.sString(tableID);
		psw = ar.sString(psw);
		roomID=ar.sInt(roomID);
		clubCode = ar.sInt(clubCode);
	}

}
