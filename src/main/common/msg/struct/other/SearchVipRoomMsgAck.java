/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.User;
import com.chess.common.bean.UserFriend;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class SearchVipRoomMsgAck extends MsgBase {

	public int vipTableID=0;
	public int numPlayer=0;
	public String psw="";
	public String tableID="";
	public String createName="";
	public int dizhu=0;
	public int minGold=0;
	public List<Integer> tableRule;//STiV modify
	public int roomType = 0;
	public List<User> players = new ArrayList<User>();
	public int openRoomType = 0;	//开房类型：0为房主付卡；1为代开；2为AA
	
	public int clubCode = 0;
	public int result = 0;

	public SearchVipRoomMsgAck() {
		msgCMD = MsgCmdConstant.GAME_SEARCH_VIP_ROOM_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		vipTableID=ar.sInt(vipTableID);
		numPlayer=ar.sInt(numPlayer);
		psw = ar.sString(psw);
		tableID=ar.sString(tableID);
		createName=ar.sString(createName);
		dizhu=ar.sInt(dizhu);
		minGold=ar.sInt(minGold);
		tableRule=(List<Integer>)ar.sIntArray(tableRule);
		roomType = ar.sInt(roomType);
		players = (List<User>)ar.sObjArray(players);
		openRoomType = ar.sInt(openRoomType);
		clubCode = ar.sInt(clubCode);
		result = ar.sInt(result);
		
	}

}
