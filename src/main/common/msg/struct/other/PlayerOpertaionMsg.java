package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**玩家发送一个操作给服务器，带一个字符串***/
public class PlayerOpertaionMsg  extends MsgBase 
{
	public int opertaionID=0;
	
	//账户
	public String account = "";
	
	//昵称
	public String playerName ="";
	
	//头像索引
	public int  headIndex =0;
	
	//性别
	public int  sex = 0;
	
	//旧密码
	public String oldPassWord ="";
	
	//新密码
	public String newPassWord ="";
	
	//是否可以加为好友（0,可以，1不可以）
	public int canFriend =0;
	
	//扩展字符
	public String opStr="";
	
	//
	public PlayerOpertaionMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_UPDATE_PLAYER_PROPERTY;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		opertaionID=ar.sInt(opertaionID);
		account = ar.sString(account);
		playerName = ar.sString(playerName);
		headIndex = ar.sInt(headIndex);
		sex = ar.sInt(sex);
		oldPassWord = ar.sString(oldPassWord);
		newPassWord = ar.sString(newPassWord);
		canFriend =ar.sInt(canFriend);
		opStr=ar.sString(opStr);
	}
}