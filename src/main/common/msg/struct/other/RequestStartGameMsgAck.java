package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.SimplePlayer;
import com.chess.common.bean.User;
import com.chess.common.constant.GameConstant;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 * 开始游戏
 ***/
public class RequestStartGameMsgAck extends MsgBase{
    public int result = 0;
    public int gold = 0;
    public int roomID = -1;//
    public int roomType = -1;// kn add
    public int tablePos = 0;
    public int reEnter = 0;
    //如果是vip桌子，这个不是0
    public int vipTableID = 0;
    //如果是vip桌子，房主名字
    public String creatorName = "";
    //VIP桌子密码
    public String tablePassword = "";
    public int quanNum = 0;
    public List<SimplePlayer> players = new ArrayList<SimplePlayer>();
    public List<Integer> tableRule;//STiV modify
    //
    public String creatorID = "";
    public int isRecord = 0 ;//0:牌桌不添加语音功能，1：牌桌添加语音聊天功能
    public int openRoomType = 0;//0:正常扣卡；1：代开扣卡；2：AA扣卡
    
    public int clubCode = 0;//俱乐部id
    
    public RequestStartGameMsgAck() {
        msgCMD = MsgCmdConstant.GAME_START_GAME_REQUEST_ACK;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        //
        result = ar.sInt(result);
        gold = ar.sInt(gold);
        roomID = ar.sInt(roomID);
        if(GameConstant.TWO_PERSON_SUPPORT) {
            roomType = ar.sInt(roomType);
        }
        tablePos = ar.sInt(tablePos);
        reEnter = ar.sInt(reEnter);
        //
        vipTableID = ar.sInt(vipTableID);
        creatorName = ar.sString(creatorName);
        tablePassword = ar.sString(tablePassword);
        
        quanNum = ar.sInt(quanNum);
        
        players = (List<SimplePlayer>) ar.sObjArray(players);
        tableRule = (List<Integer>) ar.sIntArray(tableRule);//STiV modify
        creatorID = ar.sString(creatorID);

        isRecord = ar.sInt(isRecord);
        openRoomType = ar.sInt(openRoomType);
        clubCode = ar.sInt(clubCode);
    }

    //
    public void init_players(List<User> lps, boolean isVipTable) {
        if (lps == null)
            return;
        for (int i = 0; i < lps.size(); i++) {
            User pl = lps.get(i);
            //在等待，未确定是否进行下一局
            if (pl.getGameState() == GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE) {
                continue;
            }
            //
            SimplePlayer sp = new SimplePlayer();
            sp.playerID = pl.getPlayerID();
            sp.palyerIndex = pl.getPlayerIndex();
            sp.playerName = pl.getPlayerName();
            sp.headImg = pl.getHeadImg();
            //
            if (isVipTable)
                sp.gold = pl.getVipTableGold();
            else
                sp.gold = pl.getGold();
            //
            sp.tablePos = pl.getTablePos();
            sp.canFriend = pl.getCanFriend();
            sp.sex = pl.getSex();
            sp.inTable = pl.getOnTable() && pl.isOnline() ? 1 : 0;
            sp.ip = pl.getClientIP();
            sp.readyFlag = pl.getReadyFlag();//开局准备  20170209
            sp.headImgUrl = pl.getHeadImgUrl();
            sp.location = pl.getLocation();
            players.add(sp);
        }
    }

    public void init_players_time_hend(List<User> lps) {
        if (lps == null)
            return;
        for (int i = 0; i < lps.size(); i++) {
            User pl = lps.get(i);
            //在等待，未确定是否进行下一局
            //
            SimplePlayer sp = new SimplePlayer();
            sp.playerID = pl.getPlayerID();
            sp.palyerIndex = pl.getPlayerIndex();
            sp.playerName = pl.getPlayerName();
            sp.headImg = pl.getHeadImg();
            sp.gold = pl.getGold();
            sp.tablePos = pl.getTablePos();
            sp.canFriend = pl.getCanFriend();
            sp.sex = pl.getSex();
            sp.inTable = 1;
            sp.readyFlag = pl.getReadyFlag();//开局准备  20170209
            players.add(sp);
        }
    }
    
}



