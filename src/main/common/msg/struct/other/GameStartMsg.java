package com.chess.common.msg.struct.other;

import java.util.List;

import com.chess.common.bean.User;
import com.chess.common.constant.GameConstant;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**牌局开始***/
public class GameStartMsg  extends MsgBase
{
	public String gameId="";
	public int roomId=0;
	public int myTablePos=0;
	public int reEnter = 0;
	//自己手里的牌
	public List<Byte> myCards=null;
	//0号位打出来的牌
	public List<Byte> player0Cards=null;
	//1出来的牌
	public List<Byte> player1Cards=null;
	//2打出来的牌
	public List<Byte> player2Cards=null;
	//3打出来的牌
	public List<Byte> player3Cards=null;
	
	//吃碰杠的牌
	public List<Integer> player0CardsDown=null;
	public List<Integer> player1CardsDown=null;
	public List<Integer> player2CardsDown=null;
	public List<Integer> player3CardsDown=null;
	
	public int chuCardPlayerIndex=0;//出牌玩家的座位
	public int chuCard=0;//当前操作玩家打出的牌，断线重链时此字段有值
	public int dealerPos=0;//庄家位置
	public int quanNum=0;//圈数
	public int curTotalNum=0;//当前房卡总局数
	public int quanTotal=0;//总圈数
 
	public int baoCard=0;//如果听牌，可以看到宝牌
	public int huiCard=0;//实际的会牌
	public int moHuiCard=0;//摸到的会牌
	public int tingPlayers=0;//0就是未听牌，1听牌
	
	public int player0Gold=0;//0玩家的金币数量，如果是vip场，就是桌子上的金币
	public int player1Gold=0;//1玩家的金币数量
	public int player2Gold=0;//2玩家的金币数量
	public int player3Gold=0;//3玩家的金币数量

	public int player0ZhiduiCard=0;
	public int player1ZhiduiCard=0;
	public int player2ZhiduiCard=0;
	public int player3ZhiduiCard=0;
	
	public int serviceGold=0;//本局服务费
	
	public int OffLinePlayers=0;//玩家是否在线 1：不在线，0在线
	
	public int playerOperationTime = 12; //玩家操作时间（出牌时间）
	
	public int isDealerAgain = 0;	//1:连庄，0：不是连庄
	
	//--> TODO 新增杠牌信息--用于断线回来后显示 add by  2016.9.2
	public List<Integer>player0MingGangCards = null;
	public List<Integer>player1MingGangCards = null;
	public List<Integer>player2MingGangCards = null;
	public List<Integer>player3MingGangCards = null;
	public List<Integer>player0AnGangCards = null;
	public List<Integer>player1AnGangCards = null;
	public List<Integer>player2AnGangCards = null;
	public List<Integer>player3AnGangCards = null;
	public List<Integer>player0SpecialGangCards = null;
	public List<Integer>player1SpecialGangCards = null;
	public List<Integer>player2SpecialGangCards = null;
	public List<Integer>player3SpecialGangCards = null;
	public List<Integer>player0AddGangCards = null; 
	public List<Integer>player1AddGangCards = null; 
	public List<Integer>player2AddGangCards = null; 
	public List<Integer>player3AddGangCards = null; 
	public List<Integer>player0ChiGangCards = null;
	public List<Integer>player1ChiGangCards = null;
	public List<Integer>player2ChiGangCards = null;
	public List<Integer>player3ChiGangCards = null;
	/**潇洒（只能自摸胡牌）*/
	public int player0XiaoSaCard=0;
	public int player1XiaoSaCard=0;
	public int player2XiaoSaCard=0;
	public int player3XiaoSaCard=0;
	
	/**取消停牌和胡牌次数*/
	public int player0Cancel=0;
	public int player1Cancel=0;
	public int player2Cancel=0;
	public int player3Cancel=0;
	//四个玩家的胡牌状态 ： 0代表没胡牌，1代表胡牌了
	public int player0HuFlag = 0;
	public int player1HuFlag = 0;
	public int player2HuFlag = 0;
	public int player3HuFlag = 0;
	//预留Param01-int
	public int player0Parm01=-1;
	public int player1Parm01=-1;
	public int player2Parm01=-1;
	public int player3Parm01=-1;
	//预留Param02-byte
	public byte player0Parm02=0;
	public byte player1Parm02=0;
	public byte player2Parm02=0;
	public byte player3Parm02=0;
	//预留Param01List-List<Integer>
	public List<Integer> player0Param01List = null; 
	public List<Integer> player1Param01List = null; 
	public List<Integer> player2Param01List = null; 
	public List<Integer> player3Param01List = null; 
	//预留Param02List-List<Byte>
	public List<Byte> player0Param02List = null; 
	public List<Byte> player1Param02List = null; 
	public List<Byte> player2Param02List = null; 
	public List<Byte> player3Param02List = null; 
	// 小鸡下蛋状态
	public List<Integer> player0Eggs = null;
	public List<Integer> player1Eggs = null;
	public List<Integer> player2Eggs = null;
	public List<Integer> player3Eggs = null;
	/**扑扛=1（只能自摸胡牌）*/
	public int player0PuGang = 0;
	public int player1PuGang = 0;
	public int player2PuGang = 0;
	public int player3PuGang = 0;
	// 亮牌状态
	public List<Byte> player0CacheLiangCards = null;
	public List<Byte> player1CacheLiangCards = null;
	public List<Byte> player2CacheLiangCards = null;
	public List<Byte> player3CacheLiangCards = null;
	/**打混状态*/
	public int player0DaHui = 0;
	public int player1DaHui = 0;
	public int player2DaHui = 0;
	public int player3DaHui = 0;
    public int currentTablePos = -1;
	//
	public GameStartMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_START_GAME;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		gameId=ar.sString(gameId);
		roomId=ar.sInt(roomId);
		myTablePos=ar.sInt(myTablePos);
		reEnter=ar.sInt(reEnter);
		myCards=(List<Byte>)ar.sByteArray(myCards);
		//
		player0Cards=(List<Byte>)ar.sByteArray(player0Cards);
		player1Cards=(List<Byte>)ar.sByteArray(player1Cards);
		player2Cards=(List<Byte>)ar.sByteArray(player2Cards);
		player3Cards=(List<Byte>)ar.sByteArray(player3Cards);
		//
		player0CardsDown=(List<Integer>)ar.sIntArray(player0CardsDown);
		player1CardsDown=(List<Integer>)ar.sIntArray(player1CardsDown);
		player2CardsDown=(List<Integer>)ar.sIntArray(player2CardsDown);
		player3CardsDown=(List<Integer>)ar.sIntArray(player3CardsDown);
		
		chuCardPlayerIndex=ar.sInt(chuCardPlayerIndex);
		chuCard=ar.sInt(chuCard);
		dealerPos=ar.sInt(dealerPos);
		quanNum=ar.sInt(quanNum);
		curTotalNum=ar.sInt(curTotalNum);
		quanTotal=ar.sInt(quanTotal);
		
		baoCard=ar.sInt(baoCard);
		huiCard=ar.sInt(huiCard);
		moHuiCard=ar.sInt(moHuiCard);
		tingPlayers=ar.sInt(tingPlayers);
		//
		player0Gold=ar.sInt(player0Gold);
		player1Gold=ar.sInt(player1Gold);
		player2Gold=ar.sInt(player2Gold);
		player3Gold=ar.sInt(player3Gold);
		
		if(GameConstant.ZHI_DUI_SUPPORT) {
			player0ZhiduiCard = ar.sInt(player0ZhiduiCard);
			player1ZhiduiCard = ar.sInt(player1ZhiduiCard);
			player2ZhiduiCard = ar.sInt(player2ZhiduiCard);
			player3ZhiduiCard = ar.sInt(player3ZhiduiCard);
		}
		
		serviceGold=ar.sInt(serviceGold);		
		OffLinePlayers=ar.sInt(OffLinePlayers);
		playerOperationTime = ar.sInt(playerOperationTime);
		isDealerAgain = ar.sInt(isDealerAgain);
		
		player0MingGangCards=(List<Integer>)ar.sIntArray(player0MingGangCards);
		player1MingGangCards=(List<Integer>)ar.sIntArray(player1MingGangCards);
		player2MingGangCards=(List<Integer>)ar.sIntArray(player2MingGangCards);
		player3MingGangCards=(List<Integer>)ar.sIntArray(player3MingGangCards);

		player0AnGangCards=(List<Integer>)ar.sIntArray(player0AnGangCards);
		player1AnGangCards=(List<Integer>)ar.sIntArray(player1AnGangCards);
		player2AnGangCards=(List<Integer>)ar.sIntArray(player2AnGangCards);
		player3AnGangCards=(List<Integer>)ar.sIntArray(player3AnGangCards);

		player0SpecialGangCards=(List<Integer>)ar.sIntArray(player0SpecialGangCards);
		player1SpecialGangCards=(List<Integer>)ar.sIntArray(player1SpecialGangCards);
		player2SpecialGangCards=(List<Integer>)ar.sIntArray(player2SpecialGangCards);
		player3SpecialGangCards=(List<Integer>)ar.sIntArray(player3SpecialGangCards);

		player0AddGangCards=(List<Integer>)ar.sIntArray(player0AddGangCards);
		player1AddGangCards=(List<Integer>)ar.sIntArray(player1AddGangCards);
		player2AddGangCards=(List<Integer>)ar.sIntArray(player2AddGangCards);
		player3AddGangCards=(List<Integer>)ar.sIntArray(player3AddGangCards);
		
		player0ChiGangCards=(List<Integer>)ar.sIntArray(player0ChiGangCards);
		player1ChiGangCards=(List<Integer>)ar.sIntArray(player1ChiGangCards);
		player2ChiGangCards=(List<Integer>)ar.sIntArray(player2ChiGangCards);
		player3ChiGangCards=(List<Integer>)ar.sIntArray(player3ChiGangCards);
		//潇洒
		player0XiaoSaCard=ar.sInt(player0XiaoSaCard);
		player1XiaoSaCard=ar.sInt(player1XiaoSaCard);
		player2XiaoSaCard=ar.sInt(player2XiaoSaCard);
		player3XiaoSaCard=ar.sInt(player3XiaoSaCard);
		
		//取消停牌和胡牌次数
		player0Cancel=ar.sInt(player0Cancel);
		player1Cancel=ar.sInt(player1Cancel);
		player2Cancel=ar.sInt(player2Cancel);
		player3Cancel=ar.sInt(player3Cancel);
		//胡牌标志
		player0HuFlag = ar.sInt(player0HuFlag);
		player1HuFlag = ar.sInt(player1HuFlag);
		player2HuFlag = ar.sInt(player2HuFlag);
		player3HuFlag = ar.sInt(player3HuFlag);
		
		//param01
		player0Parm01=ar.sInt(player0Parm01);
		player1Parm01=ar.sInt(player1Parm01);
		player2Parm01=ar.sInt(player2Parm01);
		player3Parm01=ar.sInt(player3Parm01);
		//param02
		player0Parm02=ar.sByte(player0Parm02);
		player1Parm02=ar.sByte(player1Parm02);
		player2Parm02=ar.sByte(player2Parm02);
		player3Parm02=ar.sByte(player3Parm02);
		//param01List
		player0Param01List=(List<Integer>)ar.sIntArray(player0Param01List);
		player1Param01List=(List<Integer>)ar.sIntArray(player1Param01List);
		player2Param01List=(List<Integer>)ar.sIntArray(player2Param01List);
		player3Param01List=(List<Integer>)ar.sIntArray(player3Param01List);
		//param01List
		player0Param02List=(List<Byte>)ar.sByteArray(player0Param02List);
		player1Param02List=(List<Byte>)ar.sByteArray(player1Param02List);
		player2Param02List=(List<Byte>)ar.sByteArray(player2Param02List);
		player3Param02List=(List<Byte>)ar.sByteArray(player3Param02List);
		
		player0Eggs = (List<Integer>)ar.sIntArray(player0Eggs);
		player1Eggs = (List<Integer>)ar.sIntArray(player1Eggs);
		player2Eggs = (List<Integer>)ar.sIntArray(player2Eggs);
		player3Eggs = (List<Integer>)ar.sIntArray(player3Eggs);
		//pugang
		player0PuGang = ar.sInt(player0PuGang);
		player1PuGang = ar.sInt(player1PuGang);
		player2PuGang = ar.sInt(player2PuGang);
		player3PuGang = ar.sInt(player3PuGang);
		
		player0CacheLiangCards = (List<Byte>)ar.sByteArray(player0CacheLiangCards);
		player1CacheLiangCards = (List<Byte>)ar.sByteArray(player1CacheLiangCards);
		player2CacheLiangCards = (List<Byte>)ar.sByteArray(player2CacheLiangCards);
		player3CacheLiangCards = (List<Byte>)ar.sByteArray(player3CacheLiangCards);
		
		player0DaHui = ar.sInt(player0DaHui);
		player1DaHui = ar.sInt(player1DaHui);
		player2DaHui = ar.sInt(player2DaHui);
		player3DaHui = ar.sInt(player3DaHui);
		currentTablePos = ar.sInt(currentTablePos);
	}
	
	public void setup_gold(List<User> plist,boolean isVipTable)
	{
		for(int i=0;i<plist.size();i++)
		
		{
			User pl=plist.get(i);
			int ipt=pl.getTablePos();
			if(isVipTable)
			{
				if(ipt==0) this.player0Gold=pl.getVipTableGold();
				else if(ipt==1) this.player1Gold=pl.getVipTableGold();
				else if(ipt==2) this.player2Gold=pl.getVipTableGold();
				else if(ipt==3) this.player3Gold=pl.getVipTableGold();
			}else
			{
				if(ipt==0) this.player0Gold=pl.getGold();
				else if(ipt==1) this.player1Gold=pl.getGold();
				else if(ipt==2) this.player2Gold=pl.getGold();
				else if(ipt==3) this.player3Gold=pl.getGold();
			}
		}
	}
	
	
}