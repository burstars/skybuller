package com.chess.common.msg.struct.other;

import java.util.List;
import java.util.ArrayList;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
 
 
/**玩家发送一个操作给服务器，带一个字符串***/
public class PlayerInfoUpdatedMsgAck  extends MsgBase 
{
	private int wons = 0;
	private int loses = 0;
	
	private int normalLevel = 0;
	private int normalExp = 0;
	private int normalGameCount = 0;
	private int maxFanNum = 0;
	private int maxHuType = 0;
	private int maxHuCard = 0;
	private byte[] maxDownCards;
	private byte[] maxHandCards;

	// pair : (1byte:huTypeId+3byte:huTypeCount);
	private byte[] huTypeCounts;
	
	//
	public PlayerInfoUpdatedMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_USER_UPDATE_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		wons = ar.sInt(wons);
		loses = ar.sInt(loses);
		
		normalLevel = ar.sInt(normalLevel);
		normalExp = ar.sInt(normalExp);
		normalGameCount = ar.sInt(normalGameCount);
		maxFanNum = ar.sInt(maxFanNum);
		maxHuType = ar.sInt(maxHuType);
		maxHuCard = ar.sInt(maxHuCard);
		
		List<Byte> array = convertArrayToList(maxDownCards);
		array = (List<Byte>)ar.sByteArray(array);
		
		array = convertArrayToList(maxHandCards);
		array = (List<Byte>)ar.sByteArray(array);
		
		array = convertArrayToList(huTypeCounts);
		array = (List<Byte>)ar.sByteArray(array);
	}
	
	private List<Byte> convertArrayToList(byte[] array) {
		List<Byte> list = new ArrayList();
		for (int i=0; i<array.length; i++) {
			list.add(array[i]);
		}
		return list;
	}

	public int getWons() {
		return wons;
	}

	public void setWons(int wons) {
		this.wons = wons;
	}

	public int getLoses() {
		return loses;
	}

	public void setLoses(int loses) {
		this.loses = loses;
	}

	public int getNormalLevel() {
		return normalLevel;
	}

	public void setNormalLevel(int normalLevel) {
		this.normalLevel = normalLevel;
	}

	public int getNormalExp() {
		return normalExp;
	}

	public void setNormalExp(int normalExp) {
		this.normalExp = normalExp;
	}

	public int getNormalGameCount() {
		return normalGameCount;
	}

	public void setNormalGameCount(int normalGameCount) {
		this.normalGameCount = normalGameCount;
	}

	public int getMaxFanNum() {
		return maxFanNum;
	}

	public void setMaxFanNum(int maxFanNum) {
		this.maxFanNum = maxFanNum;
	}

	public int getMaxHuType() {
		return maxHuType;
	}

	public void setMaxHuType(int maxHuType) {
		this.maxHuType = maxHuType;
	}

	public int getMaxHuCard() {
		return maxHuCard;
	}

	public void setMaxHuCard(int maxHuCard) {
		this.maxHuCard = maxHuCard;
	}

	public byte[] getMaxDownCards() {
		return maxDownCards;
	}

	public void setMaxDownCards(byte[] maxDownCards) {
		this.maxDownCards = maxDownCards;
	}

	public byte[] getMaxHandCards() {
		return maxHandCards;
	}

	public void setMaxHandCards(byte[] maxHandCards) {
		this.maxHandCards = maxHandCards;
	}

	public byte[] getHuTypeCounts() {
		return huTypeCounts;
	}

	public void setHuTypeCounts(byte[] huTypeCounts) {
		this.huTypeCounts = huTypeCounts;
	}
}