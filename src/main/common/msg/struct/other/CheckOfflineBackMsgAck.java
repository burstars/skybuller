package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class CheckOfflineBackMsgAck extends MsgBase {
	public int roomID=0;
	public int roomType=0;
	public int proxyTip = 0 ;//1:代开房间；2：代开上限;3:在普通房间，不能代开
	/**加入房间类型：1-返回房间功能；0-默认进入老房子 cc modify 2017-10-17*/
	public int enterType = 0;
	/**0=创建房间；1=加入房间；2=俱乐部  2018-08-29*/
	public int clickType = 0;
	
	public CheckOfflineBackMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CHECK_OFFLINE_BACK_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		roomID=ar.sInt(roomID);
		roomType=ar.sInt(roomType);
		proxyTip = ar.sInt(proxyTip);
		enterType = ar.sInt(enterType);
		clickType = ar.sInt(clickType);
	}
}
