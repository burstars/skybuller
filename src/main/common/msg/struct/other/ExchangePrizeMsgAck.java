package com.chess.common.msg.struct.other;

import com.chess.common.bean.GameActiveCode;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ExchangePrizeMsgAck extends MsgBase {
	
	//0没有找到相应的兑换码；1.兑换码已使用；2.兑换码不在有效期内；3兑换成功；
	public int result =0;
	public int coin = 0;
	public int card =0 ;
	public ExchangePrizeMsgAck(){
		 msgCMD=MsgCmdConstant.EXCHANGE_PIRZE_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		result = ar.sInt(result);
		coin = ar.sInt(coin);
		card = ar.sInt(card);
		
	}
}
