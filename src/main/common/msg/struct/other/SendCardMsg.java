package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.constant.GameConstant;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class SendCardMsg extends MsgBase {
	public int player_table_pos = 0;
	public List<Byte> handCards = new ArrayList<Byte>();
	
	public SendCardMsg() {
		msgCMD = MsgCmdConstant.GAME_SEND_CARDS;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		player_table_pos = ar.sInt(player_table_pos);
		handCards=(List<Byte>)ar.sByteArray(handCards);
	}
	
}
