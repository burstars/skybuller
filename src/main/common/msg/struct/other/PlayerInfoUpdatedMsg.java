package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
 
 
/**玩家发送一个操作给服务器，带一个字符串***/
public class PlayerInfoUpdatedMsg  extends MsgBase 
{
	//
	public PlayerInfoUpdatedMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_USER_UPDATE;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
	}
}