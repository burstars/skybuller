package com.chess.common.msg.struct.other;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**客户端通知游戏服务器，玩家的某些行为***/
public class ExchangePrizeMsg extends MsgBase 
{
	public String changeNum="";
	public String playerID = "";
	public ExchangePrizeMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.EXCHANGE_PIRZE;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		changeNum=ar.sString(changeNum);
		playerID=ar.sString(playerID);
	}
}




 
 
