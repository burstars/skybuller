/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.other;
import java.util.ArrayList;
import java.util.List;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class CreateVipRoomMsg extends MsgBase {

	public int quanNum;//几圈的
	public String psw="";
	public int roomID=0;
	public List<Integer> tableRule = new ArrayList<Integer>();//STiV modify
	public String gameID = "";
	public int isRecord = 0 ;
	public int isProxy = 0;//0:正常扣卡；1：代开扣卡；2：AA扣卡
	/**俱乐部编号   2018-08-22*/
	public int clubCode = 0;
	
	public String roomLevel="";
	
	public CreateVipRoomMsg() {
		msgCMD = MsgCmdConstant.GAME_VIP_CREATE_ROOM;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		quanNum=ar.sInt(quanNum);
		psw = ar.sString(psw);
		roomID=ar.sInt(roomID);
		tableRule=(List<Integer>)ar.sIntArray(tableRule);//STiV modify
		gameID = ar.sString(gameID);

		isRecord = ar.sInt(isRecord);
		isProxy = ar.sInt(isProxy);
		
		clubCode = ar.sInt(clubCode);
	}

}
