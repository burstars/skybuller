package com.chess.common.msg.struct.other;
import com.chess.common.bean.User;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**道具兑换消息体 
 * 
 * 2018-01-18***/
public class WelFareMsgAck extends MsgBase 
{
	public String playerID = "";
	public int welFare_id = 0;
	public String welFare_name = "";
	public int welFare_type =0;
	public int op_money = 0;
	public int montey_type = 0;
	public User player = null;
	
	public WelFareMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.WLEFARE_MSG_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		playerID=ar.sString(playerID);
		welFare_id = ar.sInt(welFare_id);
		welFare_name = ar.sString(welFare_name);
		welFare_type = ar.sInt(welFare_type);
		op_money = ar.sInt(op_money);
		montey_type = ar.sInt(montey_type);
		player = (User) ar.sObject(player);
	}
}




 
 
