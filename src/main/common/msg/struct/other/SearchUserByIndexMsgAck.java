package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class SearchUserByIndexMsgAck extends MsgBase 
{
	public int result = 0;//是否成功0：失败，1：成功
	public String optStr = "";
	public String playerID = "";//玩家id
	public int playerIndex = 0;
	public String playerName = "";
	public String headImgUrl = "";
	
		
	public SearchUserByIndexMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.SEARCH_USER_INDEX_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		result = ar.sInt(result);		
		optStr =ar.sString(optStr);
		playerID=ar.sString(playerID);
		playerIndex = ar.sInt(playerIndex);
		playerName=ar.sString(playerName);
		headImgUrl=ar.sString(headImgUrl);
	}
}




 
 
