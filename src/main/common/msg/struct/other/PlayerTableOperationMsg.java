package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.constant.GameConstant;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**游戏服务器通知客户端，轮到玩家操作了***/
public class PlayerTableOperationMsg  extends MsgBase 
{
	public String gameId="";//游戏ID
	public int  operation=0;
	public int  player_table_pos=0;
	public int  card_value=0;
	public int  opValue=0;
	public int  cardLeftNum=0;//剩余多少张牌
	// 吃碰杠听胡谁的牌 add by  2016.9.6
	public int	 out_card_player_index = -1;
	//-->TODO: 各种杠牌信息，  新增的这些客户端也要对应增加 		add by cuiweiqing 2016.08.02
	public int  ming_gang_card_value=0;
	public int  an_gang_card_value = 0;
	public int  one_egg_card_value = 0;
	public int  nine_egg_card_value = 0;
	public int  feng_gang_card_value = 0;
	public int  happy_gang_card_value = 0;
	public int  special_gang_card_value = 0;
	public int  add_gang_card_value = 0;
	public int  chi_gang_card_value=0;
	public String gameID = "";
	
	//用于刷新玩家牌数据，防止客户端、服务端数据不一致	
	public List<Byte> handCards=new ArrayList<Byte>();			//出牌玩家手牌	
	public List<Byte> beforeCards=new ArrayList<Byte>();		//出牌玩家已经打出的牌
	public List<Integer> downCards=new ArrayList<Integer>();	//出牌玩家吃、碰牌
	
	// 听牌列表
	public List<Byte> tingList = new ArrayList<Byte>();
	// 取消次数
	public int  cancel_count = 0;
	//预留Param01-int
	public int player0Parm01=-1;
	public int player1Parm01=-1;
	public int player2Parm01=-1;
	public int player3Parm01=-1;
	
	// $$PJ$$
	public int player0MaiDuanMenColor = -1;
	public int player1MaiDuanMenColor = -1;
	public int player2MaiDuanMenColor = -1;
	public int player3MaiDuanMenColor = -1;
	// $$PJ$$
	
	//wangkui start
	/**扑扛待选择的牌*/
	public List<Byte> pu_gang_sel_list = new ArrayList<Byte>();
	//wangkui end
	
	public int clubCode = 0;
	public PlayerTableOperationMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_USER_TABLE_OPERATION;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		gameId=ar.sString(gameId);
		operation=ar.sInt(operation);
		player_table_pos=ar.sInt(player_table_pos);
		card_value=ar.sInt(card_value);
		//
		opValue=ar.sInt(opValue);
		cardLeftNum=ar.sInt(cardLeftNum);
		out_card_player_index = ar.sInt(out_card_player_index);
		
		ming_gang_card_value=ar.sInt(ming_gang_card_value);
		an_gang_card_value = ar.sInt(an_gang_card_value);
		one_egg_card_value = ar.sInt(one_egg_card_value);
		nine_egg_card_value = ar.sInt(nine_egg_card_value);
		feng_gang_card_value = ar.sInt(feng_gang_card_value);
		happy_gang_card_value = ar.sInt(happy_gang_card_value);
		special_gang_card_value = ar.sInt(special_gang_card_value);
		add_gang_card_value = ar.sInt(add_gang_card_value);
		chi_gang_card_value=ar.sInt(chi_gang_card_value);
		
		
		handCards=(List<Byte> )ar.sByteArray(handCards);		
		beforeCards=(List<Byte> )ar.sByteArray(beforeCards);
		downCards=(List<Integer> )ar.sIntArray(downCards);
		
		tingList = (List<Byte>) ar.sByteArray(tingList);
		cancel_count = ar.sInt(cancel_count);
		player0Parm01 = ar.sInt(player0Parm01);
		player1Parm01 = ar.sInt(player1Parm01);
		player2Parm01 = ar.sInt(player2Parm01);
		player3Parm01 = ar.sInt(player3Parm01);
		
		//$$PJ$$ 买断门
		player0MaiDuanMenColor = ar.sInt(player0MaiDuanMenColor);
		player1MaiDuanMenColor = ar.sInt(player1MaiDuanMenColor);
		player2MaiDuanMenColor = ar.sInt(player2MaiDuanMenColor);
		player3MaiDuanMenColor = ar.sInt(player3MaiDuanMenColor);
		//$$PJ$$
		
		pu_gang_sel_list = (List<Byte>) ar.sByteArray(pu_gang_sel_list);
		
		clubCode = ar.sInt(clubCode);
	}
	
	
}