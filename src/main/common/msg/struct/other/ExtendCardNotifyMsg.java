package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

// 续卡提醒消息
public class ExtendCardNotifyMsg extends MsgBase {
	public int tablePos = 0; // 玩家位置
	public int waitTime = 0; // 等待时间
	public int roomtType = 0; // 房间类型
	public int curQuanNum = 0; // 当前房卡圈数

	public String creator_name = ""; // 房主昵称
	public int creator_pos = 0; // 房主位置
	public String player1_name = ""; // 玩家1昵称
	public int player1_pos = 0; // 玩家1位置
	public String player2_name = ""; // 玩家2昵称
	public int player2_pos = 0; // 玩家2位置
	public String player3_name = ""; // 玩家3昵称
	public int player3_pos = 0; // 玩家3位置
	
	

	public ExtendCardNotifyMsg() {
		msgCMD = MsgCmdConstant.EXTEND_CARD_NOTIFY;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);

		tablePos = ar.sInt(tablePos);
		waitTime = ar.sInt(waitTime);
		roomtType = ar.sInt(roomtType);
		curQuanNum = ar.sInt(curQuanNum);

		creator_name = ar.sString(creator_name);
		creator_pos = ar.sInt(creator_pos);
		player1_name = ar.sString(player1_name);
		player1_pos = ar.sInt(player1_pos);
		player2_name = ar.sString(player2_name);
		player2_pos = ar.sInt(player2_pos);
		player3_name = ar.sString(player3_name);
		player3_pos = ar.sInt(player3_pos);
	}
}
