package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

// 续卡操作结果消息
public class ExtendCardResultMsg extends MsgBase {
	public int tablePos = 0;		//操作位置
	public int waitTime = 0;		//等待时间
	public int lastTime = 0;		//剩余时间
	public int roomType = 0;		//房间类型
	public int curQuanNum = 0;		//当前房卡圈数
	public int extendQuanNum = 0;	//选择续卡圈数
	public int extendResult = 0;	//最终续卡结果
	
	

	public String creator_name = ""; // 房主昵称
	public int creator_pos = 0; // 房主位置
	public int creator_result = 0;	// 操作结果	//0、等待选择，1、同意，2、拒绝
	public String player1_name = "";		//玩家1昵称
	public int player1_pos = 0;		//玩家1位置
	public int player1_result = 0;	//玩家1操作结果
	public String player2_name = "";		//玩家2昵称
	public int player2_pos = 0;		//玩家2位置
	public int player2_result = 0;	//玩家2操作结果
	public String player3_name = "";		//玩家3昵称
	public int player3_pos = 0;		//玩家3位置
	public int player3_result = 0;	//玩家3操作结果
	
	public ExtendCardResultMsg() {
		msgCMD = MsgCmdConstant.EXTEND_CARD_RESULT;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		tablePos = ar.sInt(tablePos);
		waitTime = ar.sInt(waitTime);
		lastTime = ar.sInt(lastTime);
		roomType = ar.sInt(roomType);
		curQuanNum = ar.sInt(curQuanNum);
		extendQuanNum = ar.sInt(extendQuanNum);
		extendResult = ar.sInt(extendResult);
		
		creator_name = ar.sString(creator_name);
		creator_pos = ar.sInt(creator_pos);
		creator_result = ar.sInt(creator_result);
		player1_name = ar.sString(player1_name);
		player1_pos = ar.sInt(player1_pos);
		player1_result = ar.sInt(player1_result);
		player2_name = ar.sString(player2_name);
		player2_pos = ar.sInt(player2_pos);
		player2_result = ar.sInt(player2_result);
		player3_name = ar.sString(player3_name);
		player3_pos = ar.sInt(player3_pos);
		player3_result = ar.sInt(player3_result);
		
	}
}
