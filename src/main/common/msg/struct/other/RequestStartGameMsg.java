package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** 开始游戏***/
public class RequestStartGameMsg  extends MsgBase 
{
	public int roomID=0;
	public String gameId="";
	
	public RequestStartGameMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_START_GAME_REQUEST;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		roomID=ar.sInt(roomID);
		gameId=ar.sString(gameId);
	}
}