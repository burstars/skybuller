/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import org.jacorb.idl.runtime.int_token;

import com.chess.common.bean.SimplePlayer;
import com.chess.common.bean.User;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class VipRoomCloseMsg extends MsgBase {

	public List<SimplePlayer> players = new ArrayList<SimplePlayer>();

	/** 最佳炮手 */
	public int paoPos = -1;

	/** 大赢家 */
	public int winPos = -1;

	/** 开房 */
	public int openPos = -1;
	
	/**代开强制解散，是为1，否为0**/
	public int proxyForceClose = 0;

	public VipRoomCloseMsg() {
		msgCMD = MsgCmdConstant.GAME_VIP_ROOM_CLOSE;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);

		paoPos = ar.sInt(paoPos);
		winPos = ar.sInt(winPos);
		openPos = ar.sInt(openPos);
		proxyForceClose = ar.sInt(proxyForceClose);
		players = (List<SimplePlayer>) ar.sObjArray(players);
	}

	//
//	public void init_players(List<User> lps, GameTable gt, int took) {
//		if (lps == null)
//			return;
//
//		// 最佳炮手
//		int maxPao = 0;
//		int maxWin = 0;
//
//		for (int i = 0; i < lps.size(); i++) {
//			User pl = lps.get(i);
//			AbsUserGameHandler handler = pl.getGameHandler();
//
//			//
//			SimplePlayer sp = new SimplePlayer();
//			sp.playerID = pl.getPlayerID();
//			sp.palyerIndex = pl.getPlayerIndex();
//			sp.playerName = pl.getPlayerName();
//			sp.headImg = pl.getHeadImg();
//			sp.gold = pl.getVipTableGold() - took;
//			sp.canFriend = pl.getCanFriend();
//
//			sp.zhuangCount = handler.getZhuangCount();
//			sp.winCount = handler.getWinCount();
//			sp.dianpaoCount = handler.getDianpaoCount();
//			sp.mobaoCount = handler.getMobaoCount();
//			sp.baozhongbaoCount = handler.getBaozhongbaoCount();
//			sp.headImgUrl = pl.getHeadImgUrl();
//			sp.location = pl.getLocation();
//			
//			sp.mingdanCount = handler.getMingdanCount();
//			sp.andanCount = handler.getAndanCount();
//			
//			players.add(sp);
//
//			if ((maxPao < sp.dianpaoCount) && (sp.dianpaoCount > 0)) {
//				maxPao = sp.dianpaoCount;
//				this.paoPos = i;
//			}
//
//			if ((maxWin < sp.gold) && (sp.gold > 0)) {
//				maxWin = sp.gold;
//				this.winPos = i;
//			}
//
//			if (gt.getCreator() == pl) {
//				this.openPos = i;
//			}
//		}
//
//		// 相同次数，输分多的是最佳炮手
//		List<Integer> maxPaoIndexs = new ArrayList<Integer>();
//		for (int i = 0; i < lps.size(); i++) {
//			User pl = lps.get(i);
//			AbsUserGameHandler handler = pl.getGameHandler();
//			int dianpaoCount = handler.getDianpaoCount();
//			if (dianpaoCount > 0 && dianpaoCount >= maxPao) {
//				maxPaoIndexs.add(i);
//			}
//		}
//		if (maxPaoIndexs.size() > 1) {
//			int minGold = -1;
//			for (int idx : maxPaoIndexs) {
//				User pl = lps.get(idx);
//				// 剩下的金币，谁的少谁是最佳炮手
//				int gold = pl.getVipTableGold();
//				if (minGold == -1 || gold < minGold) {
//					minGold = gold;
//					this.paoPos = idx;
//				}
//			}
//		}
//	}

}
