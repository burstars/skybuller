package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 * 断线重连请求恢复玩家状态
 * @author cuiweiqing 2017-1-5
 *
 */
public class AskRecoverPlayerStateMsg extends MsgBase {
	public String gameId="";
	public int roomId=0;
    
    public AskRecoverPlayerStateMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_ASK_RECOVER_PLAYER_STATE;
	}
    
    @Override
    public void serialize(ObjSerializer ar) {
    	super.serialize(ar);
    	
    	gameId = ar.sString(gameId);
    	roomId = ar.sInt(roomId);
    }
}
