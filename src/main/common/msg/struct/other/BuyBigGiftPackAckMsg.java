package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** 购买大礼包返回***/
public class BuyBigGiftPackAckMsg  extends MsgBase 
{
	public int total_gold=0;
	public int got_gold=0;
	public int result=0;
	public int reason=0;
	public int item_id=0;
	public int item_num=0;
	
	public BuyBigGiftPackAckMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_BUY_BIG_GIFT_PACK_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		total_gold=ar.sInt(total_gold);
		got_gold=ar.sInt(got_gold);
		result=ar.sInt(result);
		reason=ar.sInt(reason);
		//
		//item_id=ar.sInt(item_id);
		//item_num=ar.sInt(item_num);
		
	}
}