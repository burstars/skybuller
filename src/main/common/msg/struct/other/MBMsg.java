package com.chess.common.msg.struct.other;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class MBMsg extends MsgBase {
	public int getT_d() {
		return t_d;
	}

	public void setT_d(int t_d) {
		this.t_d = t_d;
	}

	public int getG_d() {
		return g_d;
	}

	public void setG_d(int g_d) {
		this.g_d = g_d;
	}



	public int getOpt() {
		return opt;
	}

	public void setOpt(int opt) {
		this.opt = opt;
	}
	
	private int t_d;
	private int d_d;
	private int r_d;
	public int getD_d() {
		return d_d;
	}

	public void setD_d(int d_d) {
		this.d_d = d_d;
	}

	public int getR_d() {
		return r_d;
	}

	public void setR_d(int r_d) {
		this.r_d = r_d;
	}

	private int opt;
	private int g_d;
	private int p_d;

	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		opt = ar.sInt(opt);
		
		t_d = ar.sInt(t_d);
		r_d = ar.sInt(r_d);
		g_d = ar.sInt(g_d);
		p_d = ar.sInt(p_d);
	}

	public int getP_d() {
		return p_d;
	}

	public void setP_d(int p_d) {
		this.p_d = p_d;
	}
}
