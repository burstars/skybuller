package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class PlayerAskCloseVipRoomMsg extends MsgBase {
	//消息类型
	public int OPT_id=0;
	//桌子id
	public int table_id = 0;
	//玩家id
	public int player_id = 0;
	//是否同意解散房间
	public int agree = 0;
	
	public String gameID  ="";
	
	public PlayerAskCloseVipRoomMsg() {
	  	 msgCMD=MsgCmdConstant.GAME_CLOSE_ROOM;
	}
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		OPT_id = ar.sInt(OPT_id);
		table_id = ar.sInt(table_id);		
		player_id = ar.sInt(player_id);
		agree = ar.sInt(agree);
		gameID = ar.sString(gameID);
	}
}
