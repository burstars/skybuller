package com.chess.common.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.SimpleRecord;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;


/**
 * 开始游戏
 ***/
public class RecordMsgAck extends MsgBase {	
	/* 战绩消息 */
	public List<SimpleRecord> zjlist = new ArrayList<SimpleRecord>();

	public RecordMsgAck() {
		msgCMD = MsgCmdConstant.GAME_RECORD_ACK_DEFINE;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		zjlist=(List<SimpleRecord>) ar.sObjArray(zjlist);
	}
}
