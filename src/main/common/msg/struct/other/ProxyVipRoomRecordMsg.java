/**
 * Description:获取玩家代开的所有房间记录
 */
package com.chess.common.msg.struct.other;


import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ProxyVipRoomRecordMsg extends MsgBase {
	/** 玩家所有VIP房间记录列表 */
	public String playerID = "";
	public String gameID = "";
	/** 记录类型，0：未开始房间，1：全部房间； */
	public int recordType= 0  ;
    /** --请求页码*/
	public int pageCurrentNum = 0;
	public ProxyVipRoomRecordMsg() {
		msgCMD = MsgCmdConstant.GET_PROXY_VIP_ROOM_RECORD;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		playerID = ar.sString(playerID);
		gameID = ar.sString(gameID);
		recordType= ar.sInt(recordType);
		pageCurrentNum = ar.sInt(pageCurrentNum);
	}
}
