/**
 * Description:请求-玩家指定VIP房间中所有游戏记录
 */
package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class VipGameRecordMsg extends MsgBase {
	/** VIP房间ID */
	public String roomID;
	/** 查询日期 */
	public String date;

	public VipGameRecordMsg() {
		msgCMD = MsgCmdConstant.GET_VIP_GAME_RECORD;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		roomID = ar.sString(roomID);
		date = ar.sString(date);
	}

}
