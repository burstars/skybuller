package com.chess.common.msg.struct.other; 

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/** 
 * @author wangjia
 * @version 创建时间：2017-6-14 
 * 类说明 :服务器通知客户端更新用户代开房间权限
 */
public class UpdatePlayerProxyOpenRoomMsg extends MsgBase{
	public int proxyOpenRoom = 0;
	public int clubType = 1;
    
    public UpdatePlayerProxyOpenRoomMsg() {
        msgCMD = MsgCmdConstant.GAME_UPDATE_PLAYER_PROXY_OPEN_ROOM;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        proxyOpenRoom=ar.sInt(proxyOpenRoom);
        clubType = ar.sInt(clubType);
    }
    
    public void init(int proxyOpenRoom) {
    	this.proxyOpenRoom = proxyOpenRoom;
	}

	public int getProxyOpenRoom() {
		return proxyOpenRoom;
	}

	public void setProxyOpenRoom(int proxyOpenRoom) {
		this.proxyOpenRoom = proxyOpenRoom;
	}

	public int getClubType() {
		return clubType;
	}

	public void setClubType(int clubType) {
		this.clubType = clubType;
	}
    
}
 