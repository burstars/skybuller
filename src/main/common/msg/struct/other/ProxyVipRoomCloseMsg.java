/**
 * Description:代理强制关闭房间
 */
package com.chess.common.msg.struct.other;


import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ProxyVipRoomCloseMsg extends MsgBase {
	/** 玩家所有VIP房间记录列表 */
	public String playerID = "";
	public String roomID = "";
	public String gameID = "";

	public ProxyVipRoomCloseMsg() {
		msgCMD = MsgCmdConstant.GAME_PROXY_VIP_ROOM_CLOSE;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		playerID = ar.sString(playerID);
		roomID = ar.sString(roomID);
		gameID = ar.sString(gameID);
	}
}
