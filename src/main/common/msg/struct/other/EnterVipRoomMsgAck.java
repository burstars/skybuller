package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 加入vip房间消息返回
 * @author  2018-9-6
 */
public class EnterVipRoomMsgAck extends MsgBase {

	public String tableID="";
	public int result = 0;
	public int clubCode = 0;
	public int currKcNum = 0;
	//
	public EnterVipRoomMsgAck() {
		msgCMD = MsgCmdConstant.GAME_ENTER_VIP_ROOM_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		
		result=ar.sInt(result);
		tableID=ar.sString(tableID);
		clubCode = ar.sInt(clubCode);
		currKcNum = ar.sInt(currKcNum);
	}

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

}
