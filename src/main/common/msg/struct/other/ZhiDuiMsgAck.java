package com.chess.common.msg.struct.other;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class ZhiDuiMsgAck extends MsgBase {

	public int zhidui_support = 0;

	public ZhiDuiMsgAck() {
		msgCMD = MsgCmdConstant.ZHIDUI_SUPPORT;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		zhidui_support = ar.sInt(zhidui_support);
	}

}
