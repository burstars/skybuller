package com.chess.common.msg.struct.update;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class GetPatchVersionAckMsg extends MsgBase 
{
	//版本号
	public int patchVersion=0;
	//是否强制更新
	public int isStrongUpdate = 0;
	//安装url
	public String androidUrl = "";
	
	//ios url
	public String iosUrl = "";
	
	public GetPatchVersionAckMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GET_PATCH_VESION_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		patchVersion = ar.sInt(patchVersion);
		isStrongUpdate = ar.sInt(isStrongUpdate);
		androidUrl = ar.sString(androidUrl);
		iosUrl = ar.sString(iosUrl);
	}
}
