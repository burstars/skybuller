package com.chess.common.msg.struct.update;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** ***/
public class GetPatchFileListMsg  extends MsgBase 
{
	public int clientVersion;
	public GetPatchFileListMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GET_PATCH_FILE_LIST;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		clientVersion=ar.sInt(clientVersion);
	}
}