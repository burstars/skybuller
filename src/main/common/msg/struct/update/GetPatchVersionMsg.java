package com.chess.common.msg.struct.update;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class GetPatchVersionMsg extends MsgBase 
{	
	public int platformType = 0;		//IOS:1 安卓：2
	
	public GetPatchVersionMsg()
	{ 
	  	 msgCMD = MsgCmdConstant.GET_PATCH_VESION;	  	 
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		platformType = ar.sInt(platformType);
	}
}