package com.chess.common.msg.struct.update;

import java.util.List;

import com.chess.common.bean.GamePatchFile;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** ***/
public class GetPatchFileListAckMsg  extends MsgBase 
{
	
	public String entranceTelecomIP="";
	public String entranceUnicomIP="";
	public int entrancePort=0;
	
	//
	public int patchVersion=0;
	public List<GamePatchFile> files=null;
	
	public GetPatchFileListAckMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GET_PATCH_FILE_LIST_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		entranceTelecomIP=ar.sString(entranceTelecomIP);
		entranceUnicomIP=ar.sString(entranceUnicomIP);
		entrancePort=ar.sInt(entrancePort);
		
		patchVersion=ar.sInt(patchVersion);
		//
		files=(List<GamePatchFile>)ar.sObjArray(files);
	}
}