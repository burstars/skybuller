package com.chess.common.msg.struct.update;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** ***/
public class GetPatchFileMsg  extends MsgBase 
{
	public int patchVersion=0;
	public int fileIndex=0;
	public int pageIndex=0;
	
	public GetPatchFileMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GET_PATCH_FILE;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		patchVersion=ar.sInt(patchVersion);
		fileIndex=ar.sInt(fileIndex);
		pageIndex=ar.sInt(pageIndex);
	}
}