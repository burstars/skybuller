package com.chess.common.msg.struct.update;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** ***/
public class GetPatchFileAckMsg  extends MsgBase 
{
	public int fileIndex;
	public int pageIndex;
	
	//
	public int data_offset=0;
	public int data_size=0;
	public byte[] bytes;
	//
	
	public GetPatchFileAckMsg()
	{ 
		neverCompressedMe=true;//不要压缩
	  	 msgCMD=MsgCmdConstant.GET_PATCH_FILE_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		fileIndex=ar.sInt(fileIndex);
		pageIndex=ar.sInt(pageIndex);
		//写入部分数据
		bytes=ar.sBytesWithOffsetAndSize(bytes, data_offset, data_size);
		
	}
}