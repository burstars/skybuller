package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class CSFindVipTableMsg extends MsgBase {
    public String server_id = "";
    public int table_id = 0;
    public long player_session_id = 0;

    public CSFindVipTableMsg()
    {
        msgCMD= MsgCmdConstant.ENTRANCE_SERVER_FIND_VIP_TABLE;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        server_id = ar.sString(server_id);
        table_id = ar.sInt(table_id);

    }
}
