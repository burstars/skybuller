package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class RequestClientCompletePhoneNumAck extends MsgBase {
    public int sessionID=0;
    public RequestClientCompletePhoneNumAck() {
        msgCMD = MsgCmdConstant.REQUEST_CLIENT_COMPLETE_PHONE_NUMBER_ACK;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        sessionID = ar.sInt(sessionID);
    }
}
