package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class CSFindVipTableMsgAck extends MsgBase {
    public String server_id = "";
    public String serverName="";
    public String telecomIP="";
    public String unicomIP="";
    public int gamePort=0;
    public int player_session_id = 0;

    public CSFindVipTableMsgAck()
    {
        msgCMD= MsgCmdConstant.ENTRANCE_SERVER_FIND_VIP_TABLE_ACK;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        server_id = ar.sString(server_id);
        player_session_id = ar.sInt(player_session_id);
    }
}
