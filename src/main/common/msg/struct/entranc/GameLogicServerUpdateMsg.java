package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**游戏逻辑服向入口服务器提交信息***/
public class GameLogicServerUpdateMsg  extends MsgBase 
{
	public String serverID="";
	//在线人数
	public int linkNum=0;
	//几个房间的人数
	public int r0Nnum=0;
	public int r1Nnum=0;
	public int r2Nnum=0;
	public int r3Nnum=0;
	public int r4Nnum=0;
	//
 
	//
	public GameLogicServerUpdateMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.ENTRANCE_SERVER_GS_UPDATE;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		serverID=ar.sString(serverID);
		linkNum=ar.sInt(linkNum);
		r0Nnum=ar.sInt(r0Nnum);
		r1Nnum=ar.sInt(r1Nnum);
		r2Nnum=ar.sInt(r2Nnum);
		r3Nnum=ar.sInt(r3Nnum);
		r4Nnum=ar.sInt(r4Nnum);
	}
}