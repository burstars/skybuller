package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 客户端向入口服务器请求逻辑服地址ack
 ***/
public class GetGameServerInfoMsgAck extends MsgBase {
    public String serverID = "";
    public String telecomIP = "";
    public String unicomIP = "";
    //
    public int gamePort = 0;
    public int patchPort = 0;

    //
    public GetGameServerInfoMsgAck() {
        msgCMD = MsgCmdConstant.ENTRANCE_SERVER_GET_GAME_SERVER_INFO_ACK;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        //
        serverID = ar.sString(serverID);
        telecomIP = ar.sString(telecomIP);
        unicomIP = ar.sString(unicomIP);
        gamePort = ar.sInt(gamePort);
        patchPort = ar.sInt(patchPort);

    }
}