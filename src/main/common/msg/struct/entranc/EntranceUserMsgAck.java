package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/** 游戏服向入口服请求创建新用户 ***/
public class EntranceUserMsgAck extends MsgBase {
	public String account = "";
	public String machine_code = "";
	public String qqOpenID = "";
	public String wxOpenID = "";
	public String playerName = "";
	public String playerID = "";
	public String password = "";
	public int sessionID = 0;
	public int result = 0;
	public Integer playerIndex = 1000;
	public int deviceFlag = 0;

	public String unionId = "";
	public String param01 = "";//  20161203
	/** 客户端手机设备相关信息 */
	public String ip = "";
	public String deviceBrand = "";// 手机厂商
	public String systemVersion = "";// 系统版本
	public String phone = ""; // 客户端手机号码

	public String headImgUrl = "";
	public String location = "";
	public int sex = 0;
	public EntranceUserMsgAck() {
		msgCMD = MsgCmdConstant.ENTRANCE_SERVER_CREATE_NEW_PLAYER_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		account = ar.sString(account);
		machine_code = ar.sString(machine_code);
		qqOpenID = ar.sString(qqOpenID);
		wxOpenID = ar.sString(wxOpenID);
		playerName = ar.sString(playerName);
		playerID = ar.sString(playerID);
		password = ar.sString(password);
		sessionID = ar.sInt(sessionID);
		result = ar.sInt(result);
		playerIndex = ar.sInt(playerIndex);
		deviceFlag = ar.sInt(deviceFlag);
		unionId = ar.sString(unionId);
		param01 = ar.sString(param01);
		ip = ar.sString(ip);
		deviceBrand = ar.sString(deviceBrand);
		systemVersion = ar.sString(systemVersion);
		phone = ar.sString(phone);
		headImgUrl = ar.sString(headImgUrl);
		location = ar.sString(location);
		sex = ar.sInt(sex);
	}
}