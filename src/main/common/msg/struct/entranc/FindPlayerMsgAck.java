package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**查找用户返回***/
public class FindPlayerMsgAck  extends MsgBase 
{

	public int result=0;
 

	//
	public FindPlayerMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.ENTRANCE_SERVER_FIND_PLAYER_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		result=ar.sInt(result);
	}
}