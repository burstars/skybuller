package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class CSCreateVipTableMsg extends MsgBase {
    public String server_id = "";
    public int player_session_id = 0;

    public CSCreateVipTableMsg()
    {
        msgCMD= MsgCmdConstant.ENTRANCE_SERVER_CREATE_VIP_TABLE;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        server_id = ar.sString(server_id);
        player_session_id = ar.sInt(player_session_id);
    }
}
