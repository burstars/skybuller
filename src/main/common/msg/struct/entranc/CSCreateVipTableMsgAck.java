package com.chess.common.msg.struct.entranc;

import com.chess.common.constant.GameConstant;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 */
public class CSCreateVipTableMsgAck extends MsgBase {
    public String server_id = "";
    public int table_id = 0;
    public int player_session_id = 0;
    public int result = GameConstant.ENTRANCE_SERVER_OPERATION_RESULT_FAILED;

    public CSCreateVipTableMsgAck() {
        msgCMD = MsgCmdConstant.ENTRANCE_SERVER_CREATE_VIP_TABLE_ACK;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        server_id = ar.sString(server_id);
        table_id = ar.sInt(table_id);
        player_session_id = ar.sInt(player_session_id);
        result = ar.sInt(result);
    }
}
