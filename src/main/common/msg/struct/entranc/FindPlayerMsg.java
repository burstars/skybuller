package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**游戏服向入口查找用户是否存在***/
public class FindPlayerMsg  extends MsgBase 
{
	public String account="";
 
 
	//
	public FindPlayerMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.ENTRANCE_SERVER_FIND_PLAYER;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		account=ar.sString(account);
 
	}
}