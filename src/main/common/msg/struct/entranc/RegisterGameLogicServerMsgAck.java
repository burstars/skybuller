package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**游戏逻辑服向入口服务器进行注册***/
public class RegisterGameLogicServerMsgAck  extends MsgBase 
{
	public String serverID;

	//
	public RegisterGameLogicServerMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.ENTRANCE_SERVER_REG_GS_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		serverID=ar.sString(serverID);
	}
}