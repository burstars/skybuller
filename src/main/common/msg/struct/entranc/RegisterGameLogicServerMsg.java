package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**游戏逻辑服向入口服务器进行注册***/
public class RegisterGameLogicServerMsg  extends MsgBase 
{
	public String serverID;
	public String serverName;
	//
	public String telecomIP="";
	public String unicomIP="";
	public int gamePort=0;
	public int patchPort=0;
	public int is_game_server = 0;
	//
 
	//
	public RegisterGameLogicServerMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.ENTRANCE_SERVER_REG_GS;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		serverID=ar.sString(serverID);
		serverName=ar.sString(serverName);
		telecomIP=ar.sString(telecomIP);
		unicomIP=ar.sString(unicomIP);
		gamePort=ar.sInt(gamePort);
		patchPort=ar.sInt(patchPort);
		is_game_server = ar.sInt(is_game_server);
	}
}