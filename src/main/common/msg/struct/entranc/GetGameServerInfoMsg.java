package com.chess.common.msg.struct.entranc;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**客户端向入口服务器请求逻辑服地址***/
public class GetGameServerInfoMsg  extends MsgBase 
{
	public String account="";
	public String machine_code="";
	public String qqOpenID="";
	public String wxOpenID ="";
	public int type=0;
	public String wxUnionID ="";//cc modify 2017-12-5

	//
	public GetGameServerInfoMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.ENTRANCE_SERVER_GET_GAME_SERVER_INFO;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		account=ar.sString(account);
		machine_code=ar.sString(machine_code); 
		qqOpenID = ar.sString(qqOpenID);
		wxOpenID = ar.sString(wxOpenID);
		type=ar.sInt(type);
		wxUnionID = ar.sString(wxUnionID);
	}
}