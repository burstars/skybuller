package com.chess.common.msg.struct.luckydraw;


import java.util.List;

import com.chess.common.bean.Prize;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/**抽奖之后返回给客户端的消息，是中奖信息 ***/
public class LuckyDrawMsgAck  extends MsgBase 
{
	public int gold=0;
	public List<Prize> prizeList=null;
	
	
	public LuckyDrawMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_LUCKYDRAW_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		gold=ar.sInt(gold);
		prizeList=(List<Prize>)ar.sObjArray(prizeList);
	}
}