package com.chess.common.msg.struct.luckydraw;


import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** ***/
public class LuckyDrawMsg  extends MsgBase 
{
	public String playerID="";
	/**1为抽一次       2为10连抽*/
	public int kind =0;
	
	
	public LuckyDrawMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_LUCKYDRAW;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		playerID=ar.sString(playerID);
		kind=ar.sInt(kind);
	}
}