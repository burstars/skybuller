package com.chess.common.msg.struct.luckydraw;

import java.util.List;

import com.chess.common.bean.Prize;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;



public class GetPrizeListMsgAck extends MsgBase {
	/**礼物列表*/
	public List<Prize> list=null;
	public GetPrizeListMsgAck(){


		msgCMD=MsgCmdConstant.GAME_GET_PRIZE_LIST_ACK;
	}
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		list=(List<Prize>) ar.sObjArray(list);
		
	}







}
