package com.chess.common.msg.struct.rankingList;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class GetRankListMsg extends MsgBase {

	/**排行榜种类，1是蹦富榜     2是蹦神榜   3每日排行榜    4 钻石排行*/
	public int kind=0;
	
	public GetRankListMsg(){
		
		
		 msgCMD=MsgCmdConstant.GAME_GET_RANKING_LIST;
	}
	
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		kind=ar.sInt(kind);
	}

	
	
	
}
