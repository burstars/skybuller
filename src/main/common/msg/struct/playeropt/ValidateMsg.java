package com.chess.common.msg.struct.playeropt;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ValidateMsg extends MsgBase{
	public String optStr="";
	
	//MSG_GAME_VALIDATENAME_ACK 昵称
	public ValidateMsg(int ack)
	{
		 msgCMD=ack;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);   
		optStr = ar.sString(optStr);
	}
}
