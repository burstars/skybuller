package com.chess.common.msg.struct.playeropt;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ChangeSexMsgAck extends MsgBase{
	public int result=0;//result 1 为修改完成
	public ChangeSexMsgAck()
	{
		 msgCMD=MsgCmdConstant.GAME_CHANGESEX_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);   
		result=ar.sInt(result);
	}
}
