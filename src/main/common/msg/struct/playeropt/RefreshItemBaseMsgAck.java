
package com.chess.common.msg.struct.playeropt;

import java.util.List;

import com.chess.common.bean.MallItem;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class RefreshItemBaseMsgAck extends MsgBase {

	public List<MallItem> baseItemList=null;
	
	public RefreshItemBaseMsgAck(){
		msgCMD=MsgCmdConstant.GAME_REFRESH_ITEM_BASE_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		baseItemList=(List<MallItem>)ar.sObjArray(baseItemList);
	}
	
	
}
