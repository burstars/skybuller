package com.chess.common.msg.struct.playeropt;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class UpdatePlayerMsg extends MsgBase{

	public String plname="";//换名字
	public UpdatePlayerMsg()
	{
		 msgCMD=MsgCmdConstant.GAME_CHANGENAME_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);   
		plname=ar.sString(plname);
	}
}

