package com.chess.common.msg.struct.playeropt;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ValidateMsgAck extends MsgBase{
	public int result=0;//result 1 为修改完成
	
	public String optStr="";
	
	public int sex;		//性别
	
	public int tablePos=-1;
	
	//MSG_GAME_VALIDATENAME_ACK 昵称
	public ValidateMsgAck(int ack)
	{
		 msgCMD=ack;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);   
		result=ar.sInt(result);
		optStr=ar.sString(optStr);
		sex = ar.sInt(sex);
		tablePos = ar.sInt(tablePos);
	}
}