/**
 */
package com.chess.common.msg.struct.playeropt;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class RefreshItemBaseMsg extends MsgBase {

	public String account;
	
	public RefreshItemBaseMsg(){
		msgCMD=MsgCmdConstant.GAME_REFRESH_ITEM_BASE;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		account=ar.sString(account);
	}
}
