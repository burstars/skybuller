package com.chess.common.msg.struct.friend;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class FriendOprateMsgACK extends MsgBase{
	public int  opertaionID=0;
	public int result=0;
	public String optStr="";
	public FriendOprateMsgACK()
	{ 
	  	 msgCMD=MsgCmdConstant.FRIEND_PROPERTY_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		opertaionID = ar.sInt(opertaionID);
		result = ar.sInt(result);
		optStr = ar.sString(optStr);
	}
}