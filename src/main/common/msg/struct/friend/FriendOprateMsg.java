package com.chess.common.msg.struct.friend;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class FriendOprateMsg extends MsgBase{
	public int  opertaionID=0;
	public String opStr="";
 
	public FriendOprateMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.FRIEND_PROPERTY;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		opertaionID=ar.sInt(opertaionID);
		opStr=ar.sString(opStr);
	}
}
