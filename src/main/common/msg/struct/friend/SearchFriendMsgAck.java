package com.chess.common.msg.struct.friend;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.UserFriend;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class SearchFriendMsgAck  extends MsgBase {
	//0无记录；1有记录；
	public int result =0;
	public List<UserFriend> friends= new ArrayList<UserFriend>();
	public SearchFriendMsgAck(){
		 msgCMD=MsgCmdConstant.GAME_GET_PLAYER_FINDFRIEND_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		result = ar.sInt(result);
		friends = (List<UserFriend>)ar.sObjArray(friends);
	}
}
