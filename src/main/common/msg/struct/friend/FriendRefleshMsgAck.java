package com.chess.common.msg.struct.friend;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.UserFriend;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class FriendRefleshMsgAck extends MsgBase{
	
	public List<UserFriend> friends = new ArrayList<UserFriend>();
	
	public FriendRefleshMsgAck()
	{
		msgCMD = MsgCmdConstant.FRIEND_REFLESHNFO_ACK;
	}
	
	 @Override
	 public void serialize(ObjSerializer ar)
	 {
		 super.serialize(ar);
		 friends = (List<UserFriend> )ar.sObjArray(friends);
	 }
}
