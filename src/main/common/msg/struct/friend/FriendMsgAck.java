package com.chess.common.msg.struct.friend;

import com.chess.common.bean.UserFriend;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class FriendMsgAck  extends MsgBase {
	public UserFriend friend= new UserFriend();
	public FriendMsgAck(){
		 msgCMD=MsgCmdConstant.GAME_GET_FRIEND__ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		friend=(UserFriend)ar.sObject(friend);
	}
}

