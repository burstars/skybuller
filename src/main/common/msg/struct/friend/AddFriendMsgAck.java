package com.chess.common.msg.struct.friend;

import com.chess.common.bean.UserFriend;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class AddFriendMsgAck extends MsgBase {
	//0无记录；1有记录；
	public int result =0;
	public UserFriend friend= new UserFriend();
	public AddFriendMsgAck(){
		 msgCMD=MsgCmdConstant.GAME_ADD_FRIEND__ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		result = ar.sInt(result);
		friend=(UserFriend)ar.sObject(friend);
	}
}
