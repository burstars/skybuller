package com.chess.common.msg.struct.friend;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class UpdateFriendRemarkMsgAck extends MsgBase {

	public int result = 0;
	public String resultDesc = "";
	
	public String friendID = "";	
	public String remark = "";
	
	public UpdateFriendRemarkMsgAck()
	{
		msgCMD=MsgCmdConstant.UPDATE_FRIEND_REMARK_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		result = ar.sInt(result);
		resultDesc = ar.sString(resultDesc);
		
		friendID = ar.sString(friendID);
		remark = ar.sString(remark);
	}
}
