package com.chess.common.msg.struct.friend;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class InviteFriendEnterVipRoomMsg extends MsgBase
{
	 
	public int roomID=0;							//房间Id
	public int vipTableID=0;						//桌子ID
	public String playerID="";		//被邀请玩家ID
	
	
	public InviteFriendEnterVipRoomMsg(){
		 msgCMD=MsgCmdConstant.GAME_INVITE_FRIEND_ENTER_VIP_ROOM;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		roomID = ar.sInt(roomID);
		vipTableID = ar.sInt(vipTableID);
		playerID=ar.sString(playerID);
	}
}
