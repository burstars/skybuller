package com.chess.common.msg.struct.friend;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class BeInvitedEnterVipRoomMsg extends MsgBase
{
	 
	public int roomID=0;							//房间Id
	public int vipTableID=0;
	public String tableID="";		//桌子ID
	public String playerID="";		//邀请者ID
	public String password="";		//密码
	public int openRoomType = 0;	//开房类型：0为房主付卡；1为代开；2为AA
	
	public BeInvitedEnterVipRoomMsg(){
		 msgCMD=MsgCmdConstant.GAME_BE_INVITED_ENTER_VIP_ROOM;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		roomID = ar.sInt(roomID);
		vipTableID = ar.sInt(vipTableID);
		tableID=ar.sString(tableID);
		playerID=ar.sString(playerID);
		password=ar.sString(password);
		openRoomType = ar.sInt(openRoomType);
	}
}
