package com.chess.common.msg.struct.friend;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class UpdateFriendRemarkMsg extends MsgBase {

	/**玩家ID*/
	public String playerID = "";
	/**好友ID*/
	public String friendID = "";
	/**备注名称*/
	public String remark = "";
	
	public UpdateFriendRemarkMsg()
	{
		msgCMD=MsgCmdConstant.UPDATE_FRIEND_REMARK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		playerID = ar.sString(playerID);
		friendID = ar.sString(friendID);
		remark = ar.sString(remark);
	}
}
