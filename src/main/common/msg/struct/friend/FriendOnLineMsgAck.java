package com.chess.common.msg.struct.friend;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class FriendOnLineMsgAck extends MsgBase{
	public String friendID="";
	public int isOnline = 0;
	public FriendOnLineMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.FRIEND_ONLINE_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		friendID = ar.sString(friendID);
		isOnline = ar.sInt(isOnline);
	}
}
