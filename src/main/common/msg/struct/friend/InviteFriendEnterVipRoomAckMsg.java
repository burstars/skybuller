package com.chess.common.msg.struct.friend;

import org.jacorb.idl.runtime.int_token;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class InviteFriendEnterVipRoomAckMsg extends MsgBase
{
	public int result = 0;								// 结果：0-成功，1-对方不同意，2-对方金币不足，
	public int roomID=0;							//房间Id
	public int vipTableID=0;						//桌子ID
	public String playerID="";						//被邀请玩家ID
	
	public InviteFriendEnterVipRoomAckMsg(){
		 msgCMD=MsgCmdConstant.GAME_INVITE_FRIEND_ENTER_VIP_ROOM_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		result = ar.sInt(result);
		roomID = ar.sInt(roomID);
		vipTableID = ar.sInt(vipTableID);
		playerID=ar.sString(playerID);
	}
}
