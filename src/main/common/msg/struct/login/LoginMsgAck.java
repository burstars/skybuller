package com.chess.common.msg.struct.login;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.MallItem;
import com.chess.common.bean.ShopGoods;
import com.chess.common.bean.SystemConfigPara;
import com.chess.common.bean.User;
import com.chess.common.bean.UserFriend;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.nndzz.table.GameRoom;

 
 
/** ***/
public class LoginMsgAck  extends MsgBase 
{
	//
	public User player=null;
	//
	public List<MallItem> baseItemList=null;
	public int result=0;
	/**登陆的奖励*/
	public int reward=0;
	public int lifeCD=0;
	//首次登录
	public int firstLogin=0;
	//修改变量名dddd-->bigGift
	public String bigGift = "";
	/**持续登录的天数*/
	public int continueLanding=0;
	/**5天的奖励*/
	public List<Integer> goldList=new ArrayList<Integer>();
	
	/**好友列表*/
	public List<UserFriend> friends = new ArrayList<UserFriend>();
	
	/**随机送的道具*/
	public Integer baseItemID=0;
	/**随机送的道具数量*/
	public Integer baseItemCount=0;
    /**各种钻石送的道具和数量*/
	public List<Integer> itemAndNumsList=new ArrayList<Integer>();
	
	public List<GameRoom> rooms=new ArrayList<GameRoom>();
	
	//客户端配置
	public  List<SystemConfigPara> clientParma = new ArrayList<SystemConfigPara>();
	
	/**是否有公告*/
	public int noticeIsExit = 0;
	/**公告标题*/
	public String noticeTitle = "";
	/**公告内容*/
	public String noticeContent = "";

	// 显示宝箱和转盘
	public int isShowBAP = 0;  //  0; hide, 1; show
	// 显示消息
	public int isShowMsg = 0;  //  0; hide, 1; show
	// 是否启用积分商城
	public int isStartCredits = 0;	// 1 启用，0 不启用
	/**积分商城列表*/
	public List<ShopGoods> shopGoods = new ArrayList<ShopGoods>();
	
	/***固定跑马灯的内容和是否开启*/
	public String foreverContent = "";
	public int foreverOpen = 0;//针对固定跑马灯的是否开启参数0= 未开启，1= 开启
	/**被关闭的产品*/
	public String closedProducts = "";
	/**俱乐部上限  2018-08-28*/
	public int clubCountMax = 1;

	/**俱乐部公告标题*/
	public String qyqNoticeTitle = "";
	/**俱乐部公告内容*/
	public String qyqNoticeContent = "";
	
	/**大厅公告标题*/
	public String hallNoticeTitle = "";
	/**大厅公告内容*/
	public String hallNoticeContent = "";
	
	public LoginMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_LOGIN_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		player=(User)ar.sObject(player);
		baseItemList=(List<MallItem>)ar.sObjArray(baseItemList);
		result=ar.sInt(result);
		reward=ar.sInt(reward);
		lifeCD=ar.sInt(lifeCD);
		firstLogin=ar.sInt(firstLogin);
		bigGift=ar.sString(bigGift);
		continueLanding=ar.sInt(continueLanding);
		goldList=(List<Integer>)ar.sIntArray(goldList);
		friends = (List<UserFriend>)ar.sObjArray(friends);
		baseItemID=ar.sInt(baseItemID);
		baseItemCount=ar.sInt(baseItemCount);
		itemAndNumsList=(List<Integer>)ar.sIntArray(itemAndNumsList);
		
		rooms =(List<GameRoom>)ar.sObjArray(rooms);
		clientParma = (List<SystemConfigPara>)ar.sObjArray(clientParma);
		
		noticeIsExit = ar.sInt(noticeIsExit);
		noticeTitle = ar.sString(noticeTitle);
		noticeContent = ar.sString(noticeContent);
		isShowBAP = ar.sInt(isShowBAP);
		isShowMsg = ar.sInt(isShowMsg);
		isStartCredits = ar.sInt(isStartCredits);
		shopGoods = (List<ShopGoods>)ar.sObjArray(shopGoods);
		foreverContent = ar.sString(foreverContent);
		foreverOpen = ar.sInt(foreverOpen);
		
		closedProducts = ar.sString(closedProducts);
		clubCountMax = ar.sInt(clubCountMax);
		
		qyqNoticeTitle = ar.sString(qyqNoticeTitle);
		qyqNoticeContent = ar.sString(qyqNoticeContent);
		
		hallNoticeTitle = ar.sString(hallNoticeTitle);
		hallNoticeContent = ar.sString(hallNoticeContent);
	}
}