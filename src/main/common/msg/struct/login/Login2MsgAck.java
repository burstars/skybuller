package com.chess.common.msg.struct.login;

import com.chess.common.bean.User;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class Login2MsgAck extends MsgBase {

	//
	public User player = null;
	public int result = 0;
	public int operate = 0;
	public int vipTableId = 0;

	public Login2MsgAck() {
		msgCMD = MsgCmdConstant.GAME_LOGIN2_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		player=(User)ar.sObject(player);
		result=ar.sInt(result);
		operate=ar.sInt(operate);
		vipTableId=ar.sInt(vipTableId);
	}
}
