package com.chess.common.msg.struct.login;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

 
 
/** 注册新用户***/
public class RegisterPlayerMsg  extends MsgBase 
{
	public String account="";
	public String password="";
	public String nickname="";
	public int deviceFlag = 0;
	
	public RegisterPlayerMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_REGISTER_PLAYER;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		account=ar.sString(account);
		password=ar.sString(password);
		nickname=ar.sString(nickname);
		deviceFlag = ar.sInt(deviceFlag);
	}
}