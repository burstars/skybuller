package com.chess.common.msg.struct.login;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.ObjSerializer;


public class Login2Msg extends LoginMsg {
	public String checkKey="";
	public String linkName="";
	
	public int operate = 0;
	public int vipTableId = 0;
	
	public Login2Msg() {
		msgCMD=MsgCmdConstant.GAME_LOGIN2;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		checkKey = ar.sString(checkKey);
		linkName = ar.sString(linkName);
		
		operate=ar.sInt(operate);
		vipTableId=ar.sInt(vipTableId);
	}
	
}
