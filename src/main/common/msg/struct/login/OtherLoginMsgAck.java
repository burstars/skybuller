package com.chess.common.msg.struct.login;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class OtherLoginMsgAck  extends MsgBase {
	//登录人的IP
	public String otherip="";
	
	public OtherLoginMsgAck()
	{
		msgCMD=MsgCmdConstant.GAME_OTHERLOGIN_ACK;	//帐号在其他地方登录
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		otherip = ar.sString(otherip);
	}
}
