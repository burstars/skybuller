package com.chess.common.msg.struct.login;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;


/**
 * 玩家断线重链接
 ***/
public class ReconnectMsg extends MsgBase {
    public String account = "";
    public String password = "";
    //匿名登录，使用手机机器编码
    public String machineCode = "";

    /**
     * qq微信标识
     */
    public String qqOpenID = "";
    public String wxOpenID = "";
    public String wxUnionID = "";//cc modify 2017-12-5

    public ReconnectMsg() {
        msgCMD = MsgCmdConstant.GAME_RECONNECT_IN;
    }

    @Override
    public void serialize(ObjSerializer ar) {
        super.serialize(ar);
        //
        account = ar.sString(account);
        password = ar.sString(password);
        machineCode = ar.sString(machineCode);
        qqOpenID = ar.sString(qqOpenID);
        wxOpenID = ar.sString(wxOpenID);
        wxUnionID = ar.sString(wxUnionID);
    }
}