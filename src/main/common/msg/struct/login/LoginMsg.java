package com.chess.common.msg.struct.login;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/** ***/
public class LoginMsg extends MsgBase {
	public String account = "";
	public String password = "";
	// 匿名登录，使用手机机器编码
	public String machineCode = "";
	public String userName = "";

	/** qq标识 */
	public String qqOpenID = "";
	/** 微信标识 */
	public String wxOpenID = "";
	/** 登录设备标识位 */
	public int deviceFlag = 0;

	public String unionId = "";
	/** 客户端手机设备相关信息  */
	public String ip = "";
	public String deviceBrand = "";// 手机厂商
	public String systemVersion = "";// 系统版本
	public String phone = ""; // 客户端手机号码

	public String headImgUrl = "";
	public String location; // 位置信息：纬度,经度
	public int sex = 0;
	public LoginMsg() {
		msgCMD = MsgCmdConstant.GAME_LOGIN;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		//
		account = ar.sString(account);
		password = ar.sString(password);
		machineCode = ar.sString(machineCode);
		userName = ar.sString(userName);
		qqOpenID = ar.sString(qqOpenID);
		wxOpenID = ar.sString(wxOpenID);
		deviceFlag = ar.sInt(deviceFlag);
		unionId = ar.sString(unionId);
		
		ip = ar.sString(ip);
		deviceBrand = ar.sString(deviceBrand);
		systemVersion = ar.sString(systemVersion);
		phone = ar.sString(phone);
		headImgUrl = ar.sString(headImgUrl);
		location = ar.sString(location);
		sex = ar.sInt(sex);
	}
}