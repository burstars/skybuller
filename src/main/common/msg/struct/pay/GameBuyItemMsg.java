/**
 * Description:请求消息-购买道具，获取要购买的道具ID
 */
package com.chess.common.msg.struct.pay;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class GameBuyItemMsg extends MsgBase {

	/** 道具ID */
	public int itemID;
	/** 购买数量 */
	public int count;
	/** 支付类型：【支付宝-5】/【微信支付-6】/【联通支付-3】/【一卡通-7】）/ 【ios内支付-8】*/
	public int payType;
	/** 一卡通卡号 */
	public String cardNo;
	/** 一卡通密码 */
	public String cardPwd;
	
	public String gameID;

	public GameBuyItemMsg() {
		msgCMD = MsgCmdConstant.GAME_BUY_ITEM;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		itemID = ar.sInt(itemID);
		count = ar.sInt(count);
		payType = ar.sInt(payType);
		cardNo = ar.sString(cardNo);
		cardPwd = ar.sString(cardPwd);
		gameID = ar.sString(gameID);
	}

}
