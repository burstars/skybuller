/**
 * Description:相应消息-生成加密订单
 */
package com.chess.common.msg.struct.pay;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class GameBuyItemAckMsg extends MsgBase {

	/** 支付类型：【支付宝-5】/【微信支付-6】/【联通支付-3】） */
	public int payType;
	
	public String order;
	
	/**预支付签名*/
	public String sign="";
	
	//额外备用字符串
	public String otherstr="";
	
	public GameBuyItemAckMsg(){
		msgCMD = MsgCmdConstant.GAME_BUY_ITEM_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		payType=ar.sInt(payType);
		order = ar.sString(order);
		sign = ar.sString(sign);
		otherstr=ar.sString(otherstr);
	}
}
