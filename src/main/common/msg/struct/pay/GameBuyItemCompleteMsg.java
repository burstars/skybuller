package com.chess.common.msg.struct.pay;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 * 支付完成购买信息
 * */
public class GameBuyItemCompleteMsg extends MsgBase {
	/** 订单ID */
	public String payResult;
	public GameBuyItemCompleteMsg() {
		msgCMD = MsgCmdConstant.GAME_PAY_ITEM_COMPLETE;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		payResult = ar.sString(payResult);
	}
}
