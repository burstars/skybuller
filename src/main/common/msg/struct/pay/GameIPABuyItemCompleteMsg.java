package com.chess.common.msg.struct.pay;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 * 支付完成购买信息
 * */
public class GameIPABuyItemCompleteMsg extends MsgBase {
	/** 订单ID */
	public String orderNo="";
	public String payResult;
	public GameIPABuyItemCompleteMsg() {
		msgCMD = MsgCmdConstant.GAME_PAY_ITEM_IPA_COMPLETE;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		orderNo = ar.sString(orderNo);
		payResult = ar.sString(payResult);
	}
}
