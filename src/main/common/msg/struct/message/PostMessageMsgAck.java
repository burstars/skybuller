package com.chess.common.msg.struct.message;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class PostMessageMsgAck extends  MsgBase 
{
	//载体类型：0文字，1声音
	public int infoType =0;
	
	//信息类型
	public int opertaionID=0;
	
	//时长
	public int iTimeLenght =0;
	
	//发送用户ID
	public String playerID="";
	
	//发送人账号
	public String account="";
	//发送人昵称
	public String nickname="";
	//玩家索引
	public int playerindex=0;
	
	//发送信息
	public byte[] infoSound = new byte[0];
	public String infoText ="";
	
	public PostMessageMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.POST_USER_INFO_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		infoType = ar.sInt(infoType);
		opertaionID = ar.sInt(opertaionID);
		iTimeLenght =ar.sInt(iTimeLenght);
		playerID = ar.sString(playerID);
		account=ar.sString(account);
		nickname=ar.sString(nickname);
		playerindex=ar.sInt(playerindex);
		infoSound = ar.sBytes(infoSound);
		infoText = ar.sString(infoText);
	}
}
