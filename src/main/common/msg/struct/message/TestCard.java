package com.chess.common.msg.struct.message;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class TestCard  extends MsgBase{
	//public byte orginCard = 0;
	//public byte newCard = 0;
	public String changeStr = "";
	
	public TestCard()
	{
		 msgCMD = MsgCmdConstant.TEST_CARD;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//orginCard = ar.sByte(orginCard);
		//newCard = ar.sByte(newCard);
		
		changeStr = ar.sString(changeStr);
	}
}
