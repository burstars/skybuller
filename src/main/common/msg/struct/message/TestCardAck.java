package com.chess.common.msg.struct.message;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class TestCardAck extends MsgBase{
	
	public List<Byte> cardsInHand=new ArrayList<Byte>();
	
	public TestCardAck()
	{
		 msgCMD = MsgCmdConstant.TEST_CARD_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);

		cardsInHand = (List<Byte>)ar.sByteArray(cardsInHand);
	}
}
