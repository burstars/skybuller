package com.chess.common.msg.struct.message;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class PostMessageMsg extends MsgBase{
	
	//载体类型：0文字，1声音
	public int infoType =0;
	
	//消息类型
	public int  opertaionID=0;
	
	//时长
	public int iTimeLenght = 0;
	
	//接收ID,0为广播
	public String receiveID = "";
	
	//发送信息
	public byte[] infoSound=new byte[0];
	public String infoText="";
	
	public String gameID = "";
	
	public PostMessageMsg()
	{
		 msgCMD=MsgCmdConstant.POST_USER_INFO;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		infoType = ar.sInt(infoType);
		opertaionID = ar.sInt(opertaionID);
		iTimeLenght = ar.sInt(iTimeLenght);
		receiveID = ar.sString(receiveID);
		infoSound = ar.sBytes(infoSound);
		infoText = ar.sString(infoText);
		gameID = ar.sString(gameID);
	}
}
