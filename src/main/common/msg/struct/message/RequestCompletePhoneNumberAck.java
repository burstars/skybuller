package com.chess.common.msg.struct.message;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class RequestCompletePhoneNumberAck extends MsgBase {

	public int result = 0;
	public String resultStr = "";
	public String phoneNumber = "";

	
	public RequestCompletePhoneNumberAck()
	{
		msgCMD = MsgCmdConstant.REQUEST_COMPLETE_PHONE_NUMBER_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		result = ar.sInt(result);		
		resultStr = ar.sString(resultStr);
		phoneNumber = ar.sString(phoneNumber);
	}
}
