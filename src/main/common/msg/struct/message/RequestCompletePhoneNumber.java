package com.chess.common.msg.struct.message;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**请求绑定手机*/
public class RequestCompletePhoneNumber extends MsgBase {

	public String phoneNumber = "";
	
	public String comfirmCode = "";
	
	public RequestCompletePhoneNumber()
	{
		msgCMD = MsgCmdConstant.REQUEST_COMPLETE_PHONE_NUMBER;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		phoneNumber = ar.sString(phoneNumber);
		comfirmCode = ar.sString(comfirmCode);
	}
}
