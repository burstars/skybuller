package com.chess.common.msg.struct.club;

import com.chess.common.bean.club.TableUserInfo;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * @description 玩家离开或坐下到俱乐部牌桌时，发送给前台的消息
 * /
 *
 */
public class PlayerDownOrUpMsgAck extends MsgBase {
	/**俱乐部编号*/
	public int clubCode;
	/**坐下 1，站起来 0*/
	public int type = -1;
	/**牌桌编号*/
	public int tableNo;
	/**牌桌人数*/
	public int playerCount = 0;
	public TableUserInfo userInfo;
	
	public PlayerDownOrUpMsgAck() {
		msgCMD=MsgCmdConstant.GAME_CLUB_DOWN_OR_UP_TABLE;
	}
	
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		this.clubCode = ar.sInt(clubCode);
		this.type = ar.sInt(type);
		this.tableNo = ar.sInt(tableNo);
		this.playerCount = ar.sInt(playerCount);
		this.userInfo = (TableUserInfo) ar.sObject(userInfo);
	}
}
