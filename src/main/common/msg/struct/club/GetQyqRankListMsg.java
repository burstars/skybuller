/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class GetQyqRankListMsg extends MsgBase {
	/** 俱乐部ID */
	public int clubID;
	/** 游戏ID */
	public String gameID;
	/** 排名类型 1：今日，2：昨天，3：周，4：月，10：全部*/
	public int opType=0;
	/** 删除读取状态的房间id字符串，用;隔开*/
	public String playerID="";

	public GetQyqRankListMsg() {
		msgCMD = MsgCmdConstant.GAME_CLUB_GET_RANK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		clubID = ar.sInt(clubID);
		gameID = ar.sString(gameID);
		opType=ar.sInt(opType);
		playerID = ar.sString(playerID);
	}

}
