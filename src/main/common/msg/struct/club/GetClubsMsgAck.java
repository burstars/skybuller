package com.chess.common.msg.struct.club;

import java.util.List;

import com.chess.common.bean.TClub;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 获取俱乐部
 * @author  2018-8-29
 */
public class GetClubsMsgAck extends MsgBase{
	
	public String playerId = "";
	
	public List<TClub> clubs = null;
 
	public GetClubsMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_GET_CLUBS_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		playerId = ar.sString(playerId);
		clubs = (List<TClub>) ar.sObjArray(clubs);
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public List<TClub> getClubs() {
		return clubs;
	}

	public void setClubs(List<TClub> clubs) {
		this.clubs = clubs;
	}
	
}
