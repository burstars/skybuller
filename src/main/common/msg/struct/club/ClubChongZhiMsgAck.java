package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ClubChongZhiMsgAck extends MsgBase{
	public int result = 0;
	public int clubCode = 0;
	public int goldNum=0;
	public int goldTotal=0;
    public String playerId = "";
    public String createrId="";
 
	public ClubChongZhiMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_CHONGZHI_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		result = ar.sInt(result);
		clubCode = ar.sInt(clubCode);
		goldNum = ar.sInt(goldNum);
		goldTotal = ar.sInt(goldTotal);
		playerId = ar.sString(playerId);
		createrId = ar.sString(createrId);
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public int getGoldNum() {
		return goldNum;
	}

	public void setGoldNum(int goldNum) {
		this.goldNum = goldNum;
	}

	public int getGoldTotal() {
		return goldTotal;
	}

	public void setGoldTotal(int goldTotal) {
		this.goldTotal = goldTotal;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}

}
