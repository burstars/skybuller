package com.chess.common.msg.struct.club;

import com.chess.common.bean.TClub;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 加入俱乐部-消息返回
 * @author  2018-8-30
 */
public class JoinClubMsgAck extends MsgBase{
	/**错误结果：
	 * 1=俱乐部不存在或已关闭;2=自己创建的不能加入;3=已经是该俱乐部成员;
	 * 4=俱乐部已关闭（给其他情况打开俱乐部查询用）*/
	public int errorResult = 0;
	public int clubCode = 0;
	public int joinType = 1;
	
	public TClub club = null;
 
	public JoinClubMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_JOIN_CLUB_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		errorResult = ar.sInt(errorResult);
		clubCode = ar.sInt(clubCode);
		joinType = ar.sInt(joinType);
		
		club = (TClub) ar.sObject(club);
	}

	public TClub getClub() {
		return club;
	}

	public void setClub(TClub club) {
		this.club = club;
	}

	public int getJoinType() {
		return joinType;
	}

	public void setJoinType(int joinType) {
		this.joinType = joinType;
	}

	public int getErrorResult() {
		return errorResult;
	}

	public void setErrorResult(int errorResult) {
		this.errorResult = errorResult;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}
	
}
