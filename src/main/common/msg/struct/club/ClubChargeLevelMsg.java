package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ClubChargeLevelMsg extends MsgBase{
	public int clubCode = 0;
	public String roomLevel = "";
 
	public ClubChargeLevelMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_CHARGE_LEVEL;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		clubCode = ar.sInt(clubCode);
		roomLevel = ar.sString(roomLevel);
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public String getRoomLevel() {
		return roomLevel;
	}

	public void setRoomLevel(String roomLevel) {
		this.roomLevel = roomLevel;
	}


}
