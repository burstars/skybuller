/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class GetQyqWinnerListMsg extends MsgBase {
	/** 俱乐部ID */
	public String clubID;
	/** 游戏ID */
	public String gameID;
	/** 操作类型 0：查询 ；1：删除*/
	public int opType=0;
	/** 删除读取状态的房间id字符串，用;隔开*/
	public String roomIds="";

	public GetQyqWinnerListMsg() {
		msgCMD = MsgCmdConstant.GAME_CLUB_GET_WINNER;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		clubID = ar.sString(clubID);
		gameID = ar.sString(gameID);
		opType=ar.sInt(opType);
		roomIds = ar.sString(roomIds);
	}

}
