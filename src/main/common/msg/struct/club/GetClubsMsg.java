package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 获取俱乐部
 * @author  2018-8-29
 */
public class GetClubsMsg extends MsgBase{
	public String playerId = "";
	public int type = 0;
	public String searchTxt = "";
 
	public GetClubsMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_GET_CLUBS;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		playerId = ar.sString(playerId);
		type = ar.sInt(type);
		searchTxt = ar.sString(searchTxt);
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	
}
