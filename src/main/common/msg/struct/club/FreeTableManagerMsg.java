/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.club;
import java.util.ArrayList;
import java.util.List;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class FreeTableManagerMsg extends MsgBase {

	public int clubCode;
	
	public int tempIndex=0;
	public List<Integer> tableRule = new ArrayList<Integer>();//STiV modify
	
	public int playerCount = 0 ;
	public int quanTityType = 0;
	public int quanTity = 0;
	public String roomLevel="";

	public FreeTableManagerMsg() {
		msgCMD = MsgCmdConstant.GAME_CLUB_VIP_KZGL;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		clubCode=ar.sInt(clubCode);
		tempIndex = ar.sInt(tempIndex);
		playerCount=ar.sInt(playerCount);
		quanTityType = ar.sInt(quanTityType);
		quanTity = ar.sInt(quanTity);
		tableRule=(List<Integer>)ar.sIntArray(tableRule);//STiV modify
		roomLevel=ar.sString(roomLevel);
	}

}
