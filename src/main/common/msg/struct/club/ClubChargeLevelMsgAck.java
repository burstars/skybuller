package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ClubChargeLevelMsgAck extends MsgBase{
	public int result = 0;
	public int clubCode = 0;
	public String roomLevel = "";
 
	public ClubChargeLevelMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_CHARGE_LEVEL_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		result = ar.sInt(result);
		clubCode = ar.sInt(clubCode);
		roomLevel = ar.sString(roomLevel);
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public String getRoomLevel() {
		return roomLevel;
	}

	public void setRoomLevel(String roomLevel) {
		this.roomLevel = roomLevel;
	}

	

}
