package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class ClubChongZhiMsg extends MsgBase{
	public int clubCode = 0;
	public int goldNum=0;
    public String playerId = "";
    public String createrId="";
 
	public ClubChongZhiMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_CHONGZHI;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		clubCode = ar.sInt(clubCode);
		goldNum = ar.sInt(goldNum);
		playerId = ar.sString(playerId);
		createrId = ar.sString(createrId);
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public int getGoldNum() {
		return goldNum;
	}

	public void setGoldNum(int goldNum) {
		this.goldNum = goldNum;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getCreaterId() {
		return createrId;
	}

	public void setCreaterId(String createrId) {
		this.createrId = createrId;
	}
}
