/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.club;
import java.util.ArrayList;
import java.util.List;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class FreeTableManagerMsgAck extends MsgBase {

	public int result = 0;
	

	public FreeTableManagerMsgAck() {
		msgCMD = MsgCmdConstant.GAME_CLUB_VIP_KZGL_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		result=ar.sInt(result);
		
	}

}
