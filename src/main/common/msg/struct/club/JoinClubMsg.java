package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 加入俱乐部
 * @author  2018-8-30
 */
public class JoinClubMsg extends MsgBase{
	public String memberId = "";
	public int clubCode = 0;
	/**加入操作类型：1=搜索；2=确认加入*/
	public int joinType = 1;
 
	public JoinClubMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_JOIN_CLUB;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		memberId = ar.sString(memberId);
		clubCode = ar.sInt(clubCode);
		joinType = ar.sInt(joinType);
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public int getJoinType() {
		return joinType;
	}

	public void setJoinType(int joinType) {
		this.joinType = joinType;
	}
	
}
