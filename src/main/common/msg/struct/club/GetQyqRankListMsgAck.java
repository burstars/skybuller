/**
 * Description:响应-获取玩家所有VIP房间记录
 */
package com.chess.common.msg.struct.club;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.ClubRankingList;
import com.chess.common.bean.VipRoomRecord;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class GetQyqRankListMsgAck extends MsgBase {
	/** 参与局数排行 */
	public List<ClubRankingList> rankTotal = new ArrayList<ClubRankingList>();
	/** 大赢家局数排行 */
	public List<ClubRankingList> rankWinner = new ArrayList<ClubRankingList>();
	/** 最佳炮手局数排行 */
	public List<ClubRankingList> rankPao = new ArrayList<ClubRankingList>();

	public GetQyqRankListMsgAck() {
		msgCMD = MsgCmdConstant.GAME_CLUB_GET_RANK_ACK;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		rankTotal = (List<ClubRankingList>) ar.sObjArray(rankTotal);
		rankWinner = (List<ClubRankingList>) ar.sObjArray(rankWinner);
		rankPao = (List<ClubRankingList>) ar.sObjArray(rankPao);
	}

}
