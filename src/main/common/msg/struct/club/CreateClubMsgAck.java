package com.chess.common.msg.struct.club;

import com.chess.common.bean.TClub;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 新建俱乐部-返回
 * @author  2018-8-30
 */
public class CreateClubMsgAck extends MsgBase{
	
	public TClub club = null;
	/** 0=成功；1=名称不合法*/
	public int errorResult = 0;
 
	public CreateClubMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_CREATE_CLUB_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		errorResult = ar.sInt(errorResult);
		club = (TClub) ar.sObject(club);
	}

	public TClub getClub() {
		return club;
	}

	public void setClub(TClub club) {
		this.club = club;
	}

	public int getErrorResult() {
		return errorResult;
	}

	public void setErrorResult(int errorResult) {
		this.errorResult = errorResult;
	}
	
}
