package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 俱乐部充值
 * @author  2018-9-6
 */
public class ClubRechargeMsgAck extends MsgBase{
	public int result = 0;
	public int clubCode = 0;
	public int fangkaNum = 0;
	public int backpackNum = 0;
 
	public ClubRechargeMsgAck()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_RECHARGE_ACK;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		result = ar.sInt(result);
		clubCode = ar.sInt(clubCode);
		fangkaNum = ar.sInt(fangkaNum);
		backpackNum = ar.sInt(backpackNum);
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public int getFangkaNum() {
		return fangkaNum;
	}

	public void setFangkaNum(int fangkaNum) {
		this.fangkaNum = fangkaNum;
	}

	public int getBackpackNum() {
		return backpackNum;
	}

	public void setBackpackNum(int backpackNum) {
		this.backpackNum = backpackNum;
	}

}
