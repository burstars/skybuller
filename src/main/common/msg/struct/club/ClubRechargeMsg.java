package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 俱乐部充值
 * @author  2018-9-6
 */
public class ClubRechargeMsg extends MsgBase{
	public int clubCode = 0;
	public int fangkaNum = 0;
 
	public ClubRechargeMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_RECHARGE;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		clubCode = ar.sInt(clubCode);
		fangkaNum = ar.sInt(fangkaNum);
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public int getFangkaNum() {
		return fangkaNum;
	}

	public void setFangkaNum(int fangkaNum) {
		this.fangkaNum = fangkaNum;
	}

}
