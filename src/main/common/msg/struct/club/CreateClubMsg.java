package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
/**
 * 新建俱乐部
 * @author  2018-8-30
 */
public class CreateClubMsg extends MsgBase{
	public String playerId = "";
	public String clubName = "";
	public int fangkaNum = 0;
	public String roomSwtich="";
 
	public CreateClubMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_CLUB_CREATE_CLUB;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		playerId = ar.sString(playerId);
		clubName = ar.sString(clubName);
		fangkaNum = ar.sInt(fangkaNum);
		roomSwtich = ar.sString(roomSwtich);
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getClubName() {
		return clubName;
	}

	public void setClubName(String clubName) {
		this.clubName = clubName;
	}

	public int getFangkaNum() {
		return fangkaNum;
	}

	public void setFangkaNum(int fangkaNum) {
		this.fangkaNum = fangkaNum;
	}

	public String getRoomSwtich() {
		return roomSwtich;
	}

	public void setRoomSwtich(String roomSwtich) {
		this.roomSwtich = roomSwtich;
	}
	
}
