package com.chess.common.msg.struct.club;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 * 
 * /
 * @description 获取单个俱乐部的信息
 * 
 */
public class GetSingleClubInfoMsg extends MsgBase {
	/** 俱乐部编码 */
	public int clubCode;

	public GetSingleClubInfoMsg() {
		msgCMD = MsgCmdConstant.GAME_CLUB_GET_SINGLE_CLUB_INFO;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		this.clubCode = ar.sInt(clubCode);
	}
}
