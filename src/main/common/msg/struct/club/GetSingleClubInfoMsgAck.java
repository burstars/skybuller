package com.chess.common.msg.struct.club;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.club.TableInfoBean;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/**
 * 
 * /
 * @description 获取单个俱乐部的信息返回前台
 * 
 */
public class GetSingleClubInfoMsgAck extends MsgBase {
	/**1成功，0失败*/
	public int errorCode = 1;
	/**是否更新, 0 init ,1 update*/
	public int isUpdate = 0;
	/** 俱乐部编码 */
	public int clubCode;
	/** 玩家类型，0 - 普通玩家； 1 - 俱乐部圈主 */
	public int playerType;
	public int fangkaNum;
	public String clubCreator;
	public String roomSwtich;
	public String goldNum;
	public String clubName;
	/** 俱乐部的固定牌桌 */
	public List<TableInfoBean> templateTables = new ArrayList<TableInfoBean>();
	/** 俱乐部正在玩的牌桌 */
	public List<TableInfoBean> playingTables = new ArrayList<TableInfoBean>();
	/** 俱乐部的自建牌桌 */
	public List<TableInfoBean> privateTables = new ArrayList<TableInfoBean>();

	public GetSingleClubInfoMsgAck() {
		msgCMD = MsgCmdConstant.GAME_CLUB_GET_SINGLE_CLUB_INFO_ACK;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		this.errorCode = ar.sInt(errorCode);
		this.isUpdate = ar.sInt(isUpdate);
		this.clubCode = ar.sInt(clubCode);
		this.playerType = ar.sInt(playerType);
		this.fangkaNum = ar.sInt(fangkaNum);
		this.clubCreator = ar.sString(clubCreator);
		this.roomSwtich = ar.sString(roomSwtich);
		this.goldNum = ar.sString(goldNum);
		this.clubName = ar.sString(clubName);
		this.templateTables = (List<TableInfoBean>) ar.sObjArray(templateTables);
		this.playingTables = (List<TableInfoBean>) ar.sObjArray(playingTables);
		this.privateTables = (List<TableInfoBean>) ar.sObjArray(privateTables);
	}
}
