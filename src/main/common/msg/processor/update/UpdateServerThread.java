package com.chess.common.msg.processor.update;

import java.util.Date;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.framework.GameContext;
import com.chess.core.net.msg.BaseMsgThread;
 


/***
 * 游戏更新文件服务器
 * **/
public class UpdateServerThread extends BaseMsgThread implements IUpdateServerMainLoop
{
	private static Logger logger = LoggerFactory.getLogger(UpdateServerThread.class);
	 
 
	private UpdateServerMsgProcessor msgProcessor=null;
	
	

 
	private Date old_loop_time=new Date();

	
	public UpdateServerThread()
	{
		this.setName("GameUpdateServerMainLoop");
		
		msgProcessor=new UpdateServerMsgProcessor();
		GameContext.updateServerMsgProc=msgProcessor;
		//
		this.msgProc=msgProcessor;
		//
		serverStatus.linkName=GameContext.serverName;
		
		old_loop_time=DateService.getCurrentUtilDate();
 
	}
	public void load_all_patch()
	{
		msgProcessor.load_all_patch();
	}
 
	//线程维护
	@Override
	public void mt()
	{
	}
	
	@Override
	public void loop()
	{
		Date tt=DateService.getCurrentUtilDate();
		long dt=tt.getTime()-old_loop_time.getTime();
		
		//
		if(dt>60000)//超过1分钟
		{
			old_loop_time=tt;
 
		}
		//
		if(GameContext.updateServer!=null)
		{
			GameContext.updateServer.breakIdleSession();
		}
	}
	
 
	
	
}
