package com.chess.common.msg.processor.update;

import com.chess.core.net.mina2.BaseSocketServer;
import com.chess.core.net.msg.IMsgRev;

 
public class UpdateSocket extends BaseSocketServer
{
	public UpdateSocket(int port,String server_name,IMsgRev in_msgProc)
	{
		super(port,server_name,in_msgProc); 
	}
}
