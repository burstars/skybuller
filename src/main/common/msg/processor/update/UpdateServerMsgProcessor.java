package com.chess.common.msg.processor.update;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Deflater;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.bean.GamePatch;
import com.chess.common.bean.GamePatchFile;
import com.chess.common.bean.SystemConfigPara;
import com.chess.common.bean.User;
import com.chess.common.constant.ConfigConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.common.msg.struct.system.LinkBrokenMsg;
import com.chess.common.msg.struct.update.GetPatchFileAckMsg;
import com.chess.common.msg.struct.update.GetPatchFileListAckMsg;
import com.chess.common.msg.struct.update.GetPatchFileListMsg;
import com.chess.common.msg.struct.update.GetPatchFileMsg;
import com.chess.common.msg.struct.update.GetPatchVersionAckMsg;
import com.chess.common.msg.struct.update.GetPatchVersionMsg;
import com.chess.common.service.ISystemConfigService;
import com.chess.core.constant.NetConstant;
import com.chess.core.net.code.NetMsgEncoder;
import com.chess.core.net.msg.BaseMsgProcessor;
import com.chess.core.net.msg.MsgBase;




public class UpdateServerMsgProcessor  extends BaseMsgProcessor
{
	private static Logger logger = LoggerFactory.getLogger(UpdateServerMsgProcessor.class);

 
	private List<GamePatch> patchList = new ArrayList<GamePatch>();
	
	private final int PAGE_SIZE=16384;
	//
 
	//
	public UpdateServerMsgProcessor()
	{
		this.setCheckKey(GameContext.checkKey);
	}

	//
	public void load_all_patch()
	{
		//
		String path=Thread.currentThread().getContextClassLoader().getResource("") + "patch";
		path=path.substring(6);
		//
		read_patch_root_directory(path);
	}
	//
	private GamePatch find_patch(int patch_ver)
	{
		for(int i=0;i<patchList.size();i++)
		{
			GamePatch gp=patchList.get(i);
			if(gp.patchVersion==patch_ver)
				return gp;
		}
		//
		return null;
	}
	//
	private GamePatch get_patch_big_than(int patch_ver)
	{
		for(int i=0;i<patchList.size();i++)
		{
			GamePatch gp=patchList.get(i);
			if(gp.patchVersion>patch_ver)
				return gp;
		}
		//
		return null;
	}
	
	//
	private void add_patch(GamePatch patch)
	{
		if(patch==null)
			return;
		//
		boolean added=false;
		//
		for(int i=0;i<patchList.size();i++)
		{
			GamePatch gp=patchList.get(i);
			if(patch.patchVersion<gp.patchVersion)
			{
				patchList.add(i, patch);
				added=true;
				break;
			}
		}
		//
		if(added==false)
		{
			patchList.add(patch);
		}

	}
	
	//网页调用添加新的补丁目录
	public void add_new_patch(String folder,boolean reload_all)
	{
		//
		if(reload_all)
		{
			clear_all_patch();
			load_all_patch();
		}else if(folder!=null && folder.length()>1)
		{
			String path=Thread.currentThread().getContextClassLoader().getResource("") + "patch/"+folder;
			path=path.substring(6);
			//
			int ver=Integer.parseInt(folder);
			GamePatch gp=find_patch(ver);
			if(gp!=null)
			{
				clear_patch_list(gp.files);
			}
			else
			{
				gp=new GamePatch();
				gp.patchVersion=ver;
				add_patch(gp);
			}
       	 	//
			readPatchDirectory(path,"",gp.files);
		}
	}
	//
	private void clear_all_patch()
	{
		for(int i=0;i<patchList.size();i++)
		{
			GamePatch gp=patchList.get(i);
			clear_patch_list(gp.files);
		}
		//
		patchList.clear();
	}
	//
	private void clear_patch_list(List<GamePatchFile> lt)
	{
		if(lt==null)
			return;
		for(int i=0;i<lt.size();i++)
		{
			GamePatchFile gpf=lt.get(i);
			gpf.buf=null;//buf如何释放？？
	
		}
		//
		lt.clear();
	}
	//
	 /**
     * 读取某个文件夹下的所有文件
     */
    private boolean read_patch_root_directory(String filepath)
    {
	    try {
	
            File file = new File(filepath);
            //
            if (file.isDirectory()) 
            {
                 String[] filelist = file.list();
                 //
                 for (int i = 0; i < filelist.length; i++) 
                 {
                	 String fname=filelist[i];
                	 if(fname.equals(".svn"))
                		 continue;
                	 //
                	 int ver=Integer.parseInt(fname);
                	 //
                	 GamePatch gp=new GamePatch();
                	 gp.patchVersion=ver;
                	 add_patch(gp);
                	 //
                	 String file_full_path=filepath + "/" + fname;
                     File readfile = new File(file_full_path);
                     //
                     if (readfile.isDirectory()) 
                     {
                    	 readPatchDirectory(filepath + "/" + filelist[i],"",gp.files);
                     } 
                     //patch目录下不处理文件，只处理目录
                 }
            } 
	
	    } 
	    catch (Exception e) 
	    {
            logger.debug("readfile()   Exception:" + e.getMessage());//System.out.println("readfile()   Exception:" + e.getMessage());
	    }
	    return true;
    }
    
    private boolean readPatchDirectory(String filepath,String short_path,List<GamePatchFile>lt)
    {
	    try {
	
	    	//System.out.println("relative path=" + short_path);
            File file = new File(filepath);
            //
            if (file.isDirectory()) 
            {
                 String[] filelist = file.list();
                 for (int i = 0; i < filelist.length; i++) 
                 {
                	 String fname=filelist[i];
                	 if(fname.equals(".svn"))
                		 continue;
                	 //
                	 String file_full_path=filepath + "/" + fname;
                     File readfile = new File(file_full_path);
                     //
                     String short_file_name="";
                     if(short_path!=null && short_path.length()>1)
                    	 short_file_name=short_path+"/"+readfile.getName();
                     else
                    	 short_file_name=readfile.getName();
                     //
                     if (readfile.isDirectory()) 
                     {
                    	 readPatchDirectory(filepath + "/" + filelist[i],short_file_name,lt);
                     } 
                     else
                     {
                    	 //System.out.println("path=" + readfile.getPath());
                        // System.out.println("absolutepath="+ readfile.getAbsolutePath());
                        // System.out.println("name=" + readfile.getName());
                         
                    	 if(readfile.getName().equals("Thumbs.db"))
                    		 continue;
                         GamePatchFile patch=load_patch_file(readfile.getAbsolutePath(),short_file_name);
                         if(patch!=null)
                        	 lt.add(patch);
                     }
                 }
            } 
            else
            {
            	// System.out.println("文件");
                 //System.out.println("path=" + file.getPath());
                 //System.out.println("absolutepath=" + file.getAbsolutePath());
                // System.out.println("name=" + file.getName());
                 //
            	if(file.getName().equals("Thumbs.db")==false)
            	{
            		 String short_file_name="";
                     if(short_path!=null && short_path.length()>1)
                    	 short_file_name=short_path+"/"+file.getName();
                     else
                    	 short_file_name=file.getName();
                     
            	
            		GamePatchFile patch=load_patch_file(file.getAbsolutePath(),short_file_name);
            		if(patch!=null)
            			lt.add(patch);
            	}

            }
	
	    } 
	    catch (Exception e) 
	    {
            logger.debug("readfile()   Exception:" + e.getMessage());System.out.println("readfile()   Exception:" + e.getMessage());
	    }
	    return true;
    }
    
 
	//
	private GamePatchFile load_patch_file(String full_path,String game_relative_path)
	{
		if(full_path==null || full_path.length()<3)
			return null;
		//
		try{

			GamePatchFile patch=new GamePatchFile();
			//
			patch.name=game_relative_path;
			//
			 logger.debug("file=" + game_relative_path+",path="+full_path);//System.out.println("file=" + game_relative_path+",path="+full_path);
			//
			FileInputStream fs=new FileInputStream (full_path);
			IoBuffer buf=IoBuffer.allocate(1024*256);
			buf.order(ByteOrder.LITTLE_ENDIAN);
			//
			buf.setAutoExpand(true);
			//
			int b=fs.read();
			while(b!=-1)
			{
				buf.put((byte)b);
				b=fs.read();
			}
			fs.close();
			fs=null;
			//
			
			//
			int buf_size=buf.position();
			//
			patch.file_size=buf_size;
			//
			byte[] src_bytes=new byte[buf_size];
			buf.limit(buf_size);
			buf.capacity(buf_size);
			buf.position(0);
			buf.get(src_bytes, 0, buf_size);
			//压缩一下
			Deflater compressor=new Deflater();
			compressor.setInput(src_bytes);
			//
			compressor.finish(); 
			 //
			//buf复位
			buf.position(0);
			//压缩
			byte[] dest=buf.array();
			int dest_len=dest.length;
			
			int count = compressor.deflate(dest, 0, dest_len); 
			//
			patch.file_compressed_size=count;
			patch.pageNum=patch.file_compressed_size/PAGE_SIZE;
			//看看有没有零头
			if(patch.file_compressed_size>patch.pageNum*PAGE_SIZE)
			{
				patch.pageNum++;
			}
			//结束
			compressor.end();
			buf.limit(count);
			//
			buf.flip();
			//
			patch.buf=buf;
			//
			return patch;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		//
		return null;
	}
	
	
     @Override
	public void send(IoSession session,MsgBase msg)
    {
	 
    	if(session==null || msg==null)
    	{
    		return;
    	}
    	//
    	IoBuffer buf=NetMsgEncoder.encode(msg);

        if(buf!=null){
        	session.write(buf);
        }
    }
	  
	//
	private void sendPatchList(int ver,IoSession session)
	{
		GetPatchFileListAckMsg axk=new GetPatchFileListAckMsg();
		axk.entranceTelecomIP=GameContext.entranceServerIP_telecom;
		axk.entranceUnicomIP=GameContext.entranceServerIP_unicom;
		axk.entrancePort=GameContext.entranceServerPort;
		//

		GamePatch gp=get_patch_big_than(ver);
		if(gp!=null)
		{
			axk.patchVersion=gp.patchVersion;
			axk.files=gp.files;
		}
	
		//
		send(session,axk);
	}
	
	private void sendPatchFile(GetPatchFileMsg msg,IoSession session)
	{
	
		GamePatch gp=find_patch(msg.patchVersion);
		if(gp==null)
			return;
		//
		List<GamePatchFile> files=gp.files;
		if(files==null|| files.size()==0 || msg.fileIndex>=files.size())
			return;
		//
		//
		GamePatchFile patch=files.get(msg.fileIndex);


		GetPatchFileAckMsg axk=new GetPatchFileAckMsg();
		axk.fileIndex=msg.fileIndex;
		axk.pageIndex=msg.pageIndex;
		//
		axk.bytes=patch.buf.array();
		axk.data_offset=axk.pageIndex*PAGE_SIZE;
		//处理下可能是最后一个page
		if(axk.pageIndex==patch.pageNum-1)
		{
			axk.data_size=patch.file_compressed_size-(patch.pageNum-1)*PAGE_SIZE;
			
		}else
		{
			axk.data_size=PAGE_SIZE;
		}
			
		send(session,axk);
	}
	
	//
	@Override
	protected void udpateMsgProcess( MsgBase msg,IoSession session)
	{
		User pl=(User)session.getAttribute(NetConstant.PLAYER_SESSION_KEY);

		//System.out.println("lobbyProcess");
		int cmd= msg.msgCMD;
		//设置下属性，如果一个链接，链接到补丁服务器，最后一个消息后1分钟，就被强行断开
		Long dt=DateService.getCurrentUtilDate().getTime();
		session.setAttribute(NetConstant.PATCH_SERVER_LAST_MSG_TIME, dt);
	 

		switch(msg.msgCMD)
		{
		case MsgCmdConstant.GET_PATCH_FILE_LIST:
			GetPatchFileListMsg gpflm=(GetPatchFileListMsg)msg;
			sendPatchList(gpflm.clientVersion,session);
			break;
		case MsgCmdConstant.GET_PATCH_FILE:
			GetPatchFileMsg gpfm=(GetPatchFileMsg)msg;
			sendPatchFile(gpfm,session);
			break;
		case MsgCmdConstant.GET_PATCH_VESION:
			GetPatchVersionMsg ver = (GetPatchVersionMsg)msg;
			sendVesionAck(ver, session);
			break;
		default:
			logger.error("unkown msg，cmd="+msg.msgCMD+"----"+Integer.toHexString(msg.msgCMD));
			break;
		}

	}
	 
	@Override
	protected void player_link_validation_passed(IoSession session)
	{
		 

	}

	//玩家断线
	@Override
	protected void playerLinkBroken(LinkBrokenMsg msg,IoSession session)
	{

	}

 
	private void sendVesionAck(GetPatchVersionMsg msg, IoSession session){
		ISystemConfigService cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		GetPatchVersionAckMsg vesionMsgAck = new GetPatchVersionAckMsg();

		SystemConfigPara para = null;
		if(msg.platformType == ConfigConstant.DEVICE_TYPE_IOS)
		{
			para = cfgService.getPara(ConfigConstant.UPATCHE_VERSION_IOS);
		}
		else
		{
			para = cfgService.getPara(ConfigConstant.UPATCHE_VERSION_ANDROID);
		}
		
		if(para!=null){
			String[] usrl = para.getValueStr().split(";");
			if(usrl.length==2){
				vesionMsgAck.androidUrl = usrl[0];
				vesionMsgAck.iosUrl = usrl[1];
			}
			else if(usrl.length==1){
				vesionMsgAck.androidUrl = usrl[0];
				vesionMsgAck.iosUrl = usrl[0];
			}
			else
			{
				return;
			}
			
			vesionMsgAck.patchVersion = para.getValueInt();
			vesionMsgAck.isStrongUpdate = para.getPro_1();
		}

		send(session, vesionMsgAck);
	}


}
