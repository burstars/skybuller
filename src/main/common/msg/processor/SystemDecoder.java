package com.chess.common.msg.processor;

import java.nio.ByteOrder;

import org.apache.mina.core.buffer.IoBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.common.msg.struct.club.ClubChargeLevelMsg;
import com.chess.common.msg.struct.club.ClubChargeLevelMsgAck;
import com.chess.common.msg.struct.club.ClubChongZhiMsg;
import com.chess.common.msg.struct.club.ClubChongZhiMsgAck;
import com.chess.common.msg.struct.club.ClubRechargeMsg;
import com.chess.common.msg.struct.club.ClubRechargeMsgAck;
import com.chess.common.msg.struct.club.CreateClubMsg;
import com.chess.common.msg.struct.club.CreateClubMsgAck;
import com.chess.common.msg.struct.club.FreeTableManagerMsg;
import com.chess.common.msg.struct.club.FreeTableManagerMsgAck;
import com.chess.common.msg.struct.club.GetClubsMsg;
import com.chess.common.msg.struct.club.GetClubsMsgAck;
import com.chess.common.msg.struct.club.GetQyqRankListMsg;
import com.chess.common.msg.struct.club.GetQyqRankListMsgAck;
import com.chess.common.msg.struct.club.GetQyqWinnerListMsg;
import com.chess.common.msg.struct.club.GetQyqWinnerListMsgAck;
import com.chess.common.msg.struct.club.GetSingleClubInfoMsg;
import com.chess.common.msg.struct.club.GetSingleClubInfoMsgAck;
import com.chess.common.msg.struct.club.JoinClubMsg;
import com.chess.common.msg.struct.club.JoinClubMsgAck;
import com.chess.common.msg.struct.clubmember.ClubMemberOprateMsg;
import com.chess.common.msg.struct.entranc.EntranceUserMsg;
import com.chess.common.msg.struct.entranc.EntranceUserMsgAck;
import com.chess.common.msg.struct.entranc.FindPlayerMsg;
import com.chess.common.msg.struct.entranc.FindPlayerMsgAck;
import com.chess.common.msg.struct.entranc.GameLogicServerUpdateMsg;
import com.chess.common.msg.struct.entranc.GetGameServerInfoMsg;
import com.chess.common.msg.struct.entranc.GetGameServerInfoMsgAck;
import com.chess.common.msg.struct.entranc.RegisterGameLogicServerMsg;
import com.chess.common.msg.struct.entranc.RegisterGameLogicServerMsgAck;
import com.chess.common.msg.struct.entranc.RequestClientCompletePhoneNumAck;
import com.chess.common.msg.struct.friend.FriendOprateMsg;
import com.chess.common.msg.struct.friend.FriendOprateMsgACK;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomAckMsg;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomMsg;
import com.chess.common.msg.struct.friend.UpdateFriendRemarkMsg;
import com.chess.common.msg.struct.login.Login2Msg;
import com.chess.common.msg.struct.login.LoginMsg;
import com.chess.common.msg.struct.login.LoginMsgAck;
import com.chess.common.msg.struct.login.ReconnectMsg;
import com.chess.common.msg.struct.login.RegisterPlayerMsg;
import com.chess.common.msg.struct.luckydraw.GetPrizeListMsg;
import com.chess.common.msg.struct.luckydraw.LuckyDrawMsg;
import com.chess.common.msg.struct.message.PostMessageMsg;
import com.chess.common.msg.struct.message.PostMessageMsgAck;
import com.chess.common.msg.struct.message.RequestCompletePhoneNumber;
import com.chess.common.msg.struct.other.AskRecoverGameMsg;
import com.chess.common.msg.struct.other.AskRecoverPlayerStateMsg;
import com.chess.common.msg.struct.other.CheckOfflineBackMsg;
import com.chess.common.msg.struct.other.CreateVipRoomMsg;
import com.chess.common.msg.struct.other.EnterVipRoomMsg;
import com.chess.common.msg.struct.other.EnterVipRoomMsgAck;
import com.chess.common.msg.struct.other.ExchangePrizeMsg;
import com.chess.common.msg.struct.other.ExchangePrizeMsgAck;
import com.chess.common.msg.struct.other.GameReadyMsg;
import com.chess.common.msg.struct.other.GameUpdateMsg;
import com.chess.common.msg.struct.other.HuDongBiaoQingMsg;
import com.chess.common.msg.struct.other.MBMsg;
import com.chess.common.msg.struct.other.PlayerAskCloseVipRoomMsg;
import com.chess.common.msg.struct.other.PlayerAskCloseVipRoomMsgAck;
import com.chess.common.msg.struct.other.PlayerBindPhoneNumber;
import com.chess.common.msg.struct.other.PlayerGameOpertaionMsg;
import com.chess.common.msg.struct.other.PlayerGameOverMsg;
import com.chess.common.msg.struct.other.PlayerGameOverMsgAck;
import com.chess.common.msg.struct.other.PlayerOperationNotifyMsg;
import com.chess.common.msg.struct.other.PlayerOpertaionMsg;
import com.chess.common.msg.struct.other.PlayerTableOperationMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomCloseMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomRecordMsg;
import com.chess.common.msg.struct.other.RecordMsg;
import com.chess.common.msg.struct.other.RequestPlayerHuTypeMsg;
import com.chess.common.msg.struct.other.RequestStartGameMsg;
import com.chess.common.msg.struct.other.SearchUserByIndexMsg;
import com.chess.common.msg.struct.other.SearchUserByIndexMsgAck;
import com.chess.common.msg.struct.other.TalkingInGameMsg;
import com.chess.common.msg.struct.other.UpdatePlayerPropertyMsg;
import com.chess.common.msg.struct.other.UserBindPhoneAck;
import com.chess.common.msg.struct.other.UserExtendMsg;
import com.chess.common.msg.struct.other.UserExtendMsgAck;
import com.chess.common.msg.struct.other.VipGameRecordAckMsg;
import com.chess.common.msg.struct.other.VipGameRecordMsg;
import com.chess.common.msg.struct.other.VipRoomCloseMsg;
import com.chess.common.msg.struct.other.VipRoomRecordAckMsg;
import com.chess.common.msg.struct.other.VipRoomRecordMsg;
import com.chess.common.msg.struct.other.WelFareMsg;
import com.chess.common.msg.struct.other.WelFareMsgAck;
import com.chess.common.msg.struct.pay.GameBuyItemAckMsg;
import com.chess.common.msg.struct.pay.GameBuyItemCompleteMsg;
import com.chess.common.msg.struct.pay.GameBuyItemMsg;
import com.chess.common.msg.struct.pay.GameIPABuyItemCompleteMsg;
import com.chess.common.msg.struct.playeropt.RefreshItemBaseMsg;
import com.chess.common.msg.struct.playeropt.ValidateMsg;
import com.chess.common.msg.struct.rankingList.GetRankListMsg;
import com.chess.common.msg.struct.system.HeartBeatingAckMsg;
import com.chess.common.msg.struct.system.HeartBeatingMsg;
import com.chess.common.msg.struct.system.LinkBrokenMsg;
import com.chess.common.msg.struct.system.LinkValidationMsg;
import com.chess.common.msg.struct.system.LinkValidationMsgAck;
import com.chess.common.msg.struct.system.LobbyReconnectMsg;
import com.chess.common.msg.struct.system.MobileCodeMsg;
import com.chess.common.msg.struct.system.PlayerOfflineMsg;
import com.chess.common.msg.struct.system.ServerStatusMsg;
import com.chess.common.msg.struct.update.GetPatchFileListMsg;
import com.chess.common.msg.struct.update.GetPatchFileMsg;
import com.chess.common.msg.struct.update.GetPatchVersionMsg;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

/*******************************************************************************
 * 把底层的二进制iobuffer数据解析成NetMsgBase继承类的实例对象
 ******************************************************************************/
public class SystemDecoder {
    private static Logger logger = LoggerFactory.getLogger(SystemDecoder.class);
    
    public static MsgBase decode(IoBuffer buf) {
        if (buf == null)
            return null;
        //
        ByteOrder order = buf.order();
        //
        int size = buf.getInt();
        if (size < 4)// 至少有1个int cmd的大小
        {
            logger.error("msg size error, size=" + size);
            return null;
        }
        int flag = buf.getInt();
        int old_pos = buf.position();
        MsgBase msg = null;
        int cmd = buf.getInt();

        //
        buf.position(old_pos);// 恢复位置
        //

        //
        if (cmd > MsgCmdConstant.GROUP_SYSTEM_MAINTAIN_START
                && cmd < MsgCmdConstant.GROUP_SYSTEM_MAINTAIN_END) {
            msg = systemMsgDecode(buf, cmd);

        } else if (cmd > MsgCmdConstant.GROUP_PATCH_SERVER_START
                && cmd < MsgCmdConstant.GROUP_PATCH_SERVER_END) {
            msg = patchServerDecode(buf, cmd);
        } else if (cmd > MsgCmdConstant.GROUP_GAME_START
                && cmd < MsgCmdConstant.GROUP_GAME_END) {
            msg = gameDecode(buf, cmd);
        } else if (cmd > MsgCmdConstant.ENTRANCE_SERVER_START
                && cmd < MsgCmdConstant.ENTRANCE_SERVER_END) {
            msg = entranceServerMsgDecode(buf, cmd);
        } else {
            logger.error("unkown message cmd:" + cmd);
        }
        if (msg == null) {
            logger.error("msg decoder failed,unkown message cmd:0x"
                    + Integer.toHexString(cmd));
        }

        return msg;

    }

    //
    private static MsgBase patchServerDecode(IoBuffer buf, int cmd) {
        ObjSerializer objSerializer = new ObjSerializer(true, buf);
        MsgBase msg = null;
        //
        switch (cmd) {

            case MsgCmdConstant.GET_PATCH_FILE_LIST:
                msg = new GetPatchFileListMsg();
                break;
            case MsgCmdConstant.GET_PATCH_FILE:
                msg = new GetPatchFileMsg();
                break;
            case MsgCmdConstant.GET_PATCH_VESION:
                msg = new GetPatchVersionMsg();
                break;
            default:
                break;
        }
        if (msg != null)
            msg.serialize(objSerializer);
        else {
            logger.error("patchServerDecode unkown message:cmd=0x"
                    + Integer.toHexString(cmd));
        }
        //
        return msg;
    }

    //
    private static MsgBase gameDecode(IoBuffer buf, int cmd) {
        ObjSerializer objSerializer = new ObjSerializer(true, buf);
        MsgBase msg = null;
        //
        switch (cmd) {

            case MsgCmdConstant.GAME_LOGIN:
                msg = new LoginMsg();
                break;
            case MsgCmdConstant.GAME_REGISTER_PLAYER:
                msg = new RegisterPlayerMsg();
                break;
            case MsgCmdConstant.GAME_LOGIN_ACK:
                msg = new LoginMsgAck();
                break;
            case MsgCmdConstant.GAME_UPDATE_PLAYER_PROPERTY: {
                msg = new UpdatePlayerPropertyMsg();
                break;
            }
            case MsgCmdConstant.GAME_START_GAME_REQUEST:{
                msg = new RequestStartGameMsg();
                break;
            }
            case MsgCmdConstant.GAME_ASK_GAME_RECOVER:{
            	msg = new AskRecoverGameMsg();
            	break;
            }
            case MsgCmdConstant.GAME_ASK_RECOVER_PLAYER_STATE:{
            	msg = new AskRecoverPlayerStateMsg();
            	break;
            }
            case MsgCmdConstant.GAME_CHECK_OFFLINE_BACK:{
            	msg = new CheckOfflineBackMsg();
            	break;
            }
            case MsgCmdConstant.GAME_GAME_UPDATE:{
                msg = new GameUpdateMsg();
                break;
            }
            case MsgCmdConstant.GAME_GAME_OPERTAION:{
                msg = new PlayerGameOpertaionMsg();
                break;
            }
            case MsgCmdConstant.GAME_GAME_OVER:{
                msg = new PlayerGameOverMsg();
                break;
            }
            case MsgCmdConstant.GAME_GAME_OVER_ACK:{
                msg = new PlayerGameOverMsgAck();
                break;
            }
            case MsgCmdConstant.GAME_GET_PRIZE_LIST:{
                msg = new GetPrizeListMsg();
                break;
            }
            case MsgCmdConstant.GAME_LUCKYDRAW:{
                msg = new LuckyDrawMsg();
                break;
            }
            case MsgCmdConstant.GAME_GET_RANKING_LIST:{
                msg = new GetRankListMsg();
                break;
            }

            case MsgCmdConstant.GAME_SEND_PLAYER_OPERATIOIN_STRING:{
                msg = new PlayerOpertaionMsg();
                break;
            }
         
            case MsgCmdConstant.GAME_RECONNECT_IN:{
                msg = new ReconnectMsg();
                break;
            }
            case MsgCmdConstant.GAME_USER_OPERATION_NOTIFY:{
                msg = new PlayerOperationNotifyMsg();
                break;
            }
            case MsgCmdConstant.GAME_USER_TABLE_OPERATION:{
                msg = new PlayerTableOperationMsg();
                break;
            }
            case MsgCmdConstant.GAME_USER_MIN:{
                msg = new MBMsg();
                break;
            }
            
            case MsgCmdConstant.GET_VIP_ROOM_RECORD:{
                msg = new VipRoomRecordMsg();
                break;
            }
            case MsgCmdConstant.GET_VIP_ROOM_RECORD_ACK:{
                msg = new VipRoomRecordAckMsg();
                break;
            }
            case MsgCmdConstant.GET_PROXY_VIP_ROOM_RECORD:{
            	msg = new ProxyVipRoomRecordMsg();
                break;
            }
            case MsgCmdConstant.GAME_PROXY_VIP_ROOM_CLOSE:{
            	msg = new ProxyVipRoomCloseMsg();
                break;
            }
            case MsgCmdConstant.GET_VIP_GAME_RECORD:
                msg = new VipGameRecordMsg();
                break;
            case MsgCmdConstant.GET_VIP_GAME_RECORD_ACK:
                msg = new VipGameRecordAckMsg();
                break;
            case MsgCmdConstant.FRIEND_PROPERTY:
                msg = new FriendOprateMsg();
                break;
            case MsgCmdConstant.FRIEND_PROPERTY_ACK:
                msg = new FriendOprateMsgACK();
                break;
            case MsgCmdConstant.CLUB_MEMBER:
                msg = new ClubMemberOprateMsg();
                break;
            case MsgCmdConstant.POST_USER_INFO:
                msg = new PostMessageMsg();
                break;
            case MsgCmdConstant.POST_USER_INFO_ACK:
                msg = new PostMessageMsgAck();
                break;
            case MsgCmdConstant.GAME_READY:
            	//开局前准备  20170208
            	logger.info("--->SystemDecoder:cmd = " + MsgCmdConstant.GAME_READY + " <---");
                msg = new GameReadyMsg();
                break;
                
            case MsgCmdConstant.GAME_VIP_CREATE_ROOM:
                msg = new CreateVipRoomMsg();
                break;
            case MsgCmdConstant.GAME_ENTER_VIP_ROOM:
                msg = new EnterVipRoomMsg();
                break;
            case MsgCmdConstant.GAME_VIP_ROOM_CLOSE:
                msg = new VipRoomCloseMsg();
                break;
            case MsgCmdConstant.GAME_INVITE_FRIEND_ENTER_VIP_ROOM:
                msg = new InviteFriendEnterVipRoomMsg();
                break;
            case MsgCmdConstant.GAME_INVITE_FRIEND_ENTER_VIP_ROOM_ACK:
            	msg = new InviteFriendEnterVipRoomAckMsg();
            	break;
            case MsgCmdConstant.GAME_BUY_ITEM:
                msg = new GameBuyItemMsg();
                break;
            case MsgCmdConstant.GAME_BUY_ITEM_ACK:
                msg = new GameBuyItemAckMsg();
                break;
            case MsgCmdConstant.GAME_REFRESH_ITEM_BASE:
                msg = new RefreshItemBaseMsg();
                break;
            case MsgCmdConstant.GAME_PAY_ITEM_COMPLETE:
                msg = new GameBuyItemCompleteMsg();
                break;
            case MsgCmdConstant.GAME_PAY_ITEM_IPA_COMPLETE:
                msg = new GameIPABuyItemCompleteMsg();
                break;
            case MsgCmdConstant.GAME_USER_HU_TYPE:
                msg = new RequestPlayerHuTypeMsg();
                break;
            
            case MsgCmdConstant.TEST_CARD:
                //msg = new TestCard();
                break;
            case MsgCmdConstant.TALKING_IN_GAME:
                msg = new TalkingInGameMsg();
                break;
            case MsgCmdConstant.HU_DONG_BIAO_QING:
            	msg = new HuDongBiaoQingMsg();
            	break;
            case MsgCmdConstant.UPDATE_FRIEND_REMARK:
                msg = new UpdateFriendRemarkMsg();
                break;
            case MsgCmdConstant.MOBILE_CODE:
                msg = new MobileCodeMsg();
                break;
            case MsgCmdConstant.REQUEST_COMPLETE_PHONE_NUMBER:
                msg = new RequestCompletePhoneNumber();
                break;
            case MsgCmdConstant.GAME_CLOSE_ROOM:
                msg = new PlayerAskCloseVipRoomMsg();
                break;
            case MsgCmdConstant.GAME_CLOSE_ROOM_ACK:
                msg = new PlayerAskCloseVipRoomMsgAck();
                break;

            case MsgCmdConstant.REQUEST_CLIENT_COMPLETE_PHONE_NUMBER_ACK:
                msg = new RequestClientCompletePhoneNumAck();
                break;
            //要求客户端完善资料
            case MsgCmdConstant.REQUEST_BIND_PHONE_ACK:
                msg = new UserBindPhoneAck();
                break;
            case MsgCmdConstant.REQUEST_BIND_PHONE:
                msg = new PlayerBindPhoneNumber();
                break;
            case MsgCmdConstant.EXCHANGE_PIRZE:
            	msg = new ExchangePrizeMsg();
            	break;
            case MsgCmdConstant.EXCHANGE_PIRZE_ACK:
            	msg = new ExchangePrizeMsgAck();
            	break;
            case MsgCmdConstant.GAME_RECORD_DEFINE:{
                msg = new RecordMsg();
                break;
            }
              //电子庄牛牛 
            case MsgCmdConstant.GAME_VIP_CREATE_ROOM_NNDZZ:
            	msg = new com.chess.nndzz.msg.struct.other.CreateVipRoomMsg();
            	break;
            case MsgCmdConstant.GAME_ENTER_VIP_ROOM_NNDZZ:
                msg = new com.chess.nndzz.msg.struct.other.EnterVipRoomMsg();
                break;
            case MsgCmdConstant.GAME_START_GAME_REQUEST_NNDZZ:{
                msg = new com.chess.nndzz.msg.struct.other.RequestStartGameMsg();
                break;
            }
            case MsgCmdConstant.GAME_USER_TABLE_OPERATION_NNDZZ:{
                msg = new com.chess.nndzz.msg.struct.other.PlayerTableOperationMsg();
                break;
            }
            case MsgCmdConstant.GAME_READY_NNDZZ:{
                msg = new com.chess.nndzz.msg.struct.other.GameReadyMsg();
                break;
            }
            case MsgCmdConstant.GAME_GAME_CHANGE_SITE_NNDZZ:
            	msg = new com.chess.nndzz.msg.struct.other.GameSiteDownMsg();
                break;
                
            	// 2次登陆
            case MsgCmdConstant.GAME_LOGIN2:
            	msg = new Login2Msg();
            	break;
            	
            	//绑定推荐人 
            case MsgCmdConstant.USER_EXTEND_GOLD_MSG:
            	msg = new UserExtendMsg();
            	break;
            case MsgCmdConstant.USER_EXTEND_GOLD_MSG_ACK:
            	msg = new UserExtendMsgAck();
            	break;
            	
            case MsgCmdConstant.SEARCH_USER_INDEX:
                msg = new SearchUserByIndexMsg();
                break;
            case MsgCmdConstant.SEARCH_USER_INDEX_ACK:
                msg = new SearchUserByIndexMsgAck();
                break;
            	
            	 //大转盘
            case MsgCmdConstant.WELFARE_MSG:
            	msg = new WelFareMsg();
            	break;
            case MsgCmdConstant.WLEFARE_MSG_ACK:
            	msg = new WelFareMsgAck();
            	break;
            	//查询俱乐部
            case MsgCmdConstant.GAME_CLUB_GET_CLUBS:
            	msg = new GetClubsMsg();
            	break;
            case MsgCmdConstant.GAME_CLUB_GET_CLUBS_ACK:
            	msg = new GetClubsMsgAck();
            	break;
            case MsgCmdConstant.GAME_CLUB_VIP_KZGL:
            	msg = new FreeTableManagerMsg();
            	break;
            case MsgCmdConstant.GAME_CLUB_VIP_KZGL_ACK:
            	msg = new FreeTableManagerMsgAck();
            	break;

            case MsgCmdConstant.GAME_CLUB_GET_WINNER:{
                msg = new GetQyqWinnerListMsg();
                break;
            }
            case MsgCmdConstant.GAME_CLUB_GET_WINNER_ACK:{
                msg = new GetQyqRankListMsgAck();
                break;
            }
            case MsgCmdConstant.GAME_CLUB_GET_RANK:{
                msg = new GetQyqRankListMsg();
                break;
            }
            case MsgCmdConstant.GAME_CLUB_GET_RANK_ACK:{
                msg = new GetQyqWinnerListMsgAck();
                break;
            }
            //创建俱乐部
            case MsgCmdConstant.GAME_CLUB_CREATE_CLUB:
            	msg = new CreateClubMsg();
            	break;
            case MsgCmdConstant.GAME_CLUB_CREATE_CLUB_ACK:
            	msg = new CreateClubMsgAck();
            	break;
            	//加入俱乐部
            case MsgCmdConstant.GAME_CLUB_JOIN_CLUB:
            	msg = new JoinClubMsg();
            	break;
            case MsgCmdConstant.GAME_CLUB_JOIN_CLUB_ACK:
            	msg = new JoinClubMsgAck();
            	break;
            case MsgCmdConstant.GAME_CLUB_GET_SINGLE_CLUB_INFO: //获取单个俱乐部的信息
            	msg = new GetSingleClubInfoMsg();
            	break;
            case MsgCmdConstant.GAME_CLUB_GET_SINGLE_CLUB_INFO_ACK: //获取单个俱乐部的信息返回
            	msg = new GetSingleClubInfoMsgAck();
            	break;
            	//俱乐部充值
            case MsgCmdConstant.GAME_CLUB_RECHARGE:
            	msg = new ClubRechargeMsg();
            	break;
            case MsgCmdConstant.GAME_CLUB_RECHARGE_ACK:
            	msg = new ClubRechargeMsgAck();
            	break;
            case MsgCmdConstant.GAME_CLUB_CHARGE_LEVEL:
            	msg = new ClubChargeLevelMsg();
            	break;
            case MsgCmdConstant.GAME_CLUB_CHARGE_LEVEL_ACK:
            	msg = new ClubChargeLevelMsgAck();
            	break;
            case MsgCmdConstant.GAME_CLUB_CHONGZHI:
            	msg = new ClubChongZhiMsg();
            	break;
            case MsgCmdConstant.GAME_CLUB_CHONGZHI_ACK:
            	msg = new ClubChongZhiMsgAck();
            	break;
            case MsgCmdConstant.GAME_ENTER_VIP_ROOM_ACK:
                msg = new EnterVipRoomMsgAck();
                break;
            default:
                break;
        }
        if (msg != null)
            msg.serialize(objSerializer);
        else {
            logger.error("gameDecode unkown message:cmd=0x"
                    + Integer.toHexString(cmd));
        }
        //
        return msg;
    }

    //
    public static MsgBase systemMsgDecode(IoBuffer buf, int cmd) {
        ObjSerializer objSerializer = new ObjSerializer(true, buf);
        MsgBase msg = null;
        //
        switch (cmd) {
            case MsgCmdConstant.HEART_BEATING:
                msg = new HeartBeatingMsg();
                break;
            case MsgCmdConstant.HEART_BEATING_ACK:
                msg = new HeartBeatingAckMsg();
                break;
            case MsgCmdConstant.LINK_VALIDATION:
                msg = new LinkValidationMsg();
                break;
            case MsgCmdConstant.LINK_VALIDATION_ACK:
                msg = new LinkValidationMsgAck();
                break;
            case MsgCmdConstant.PLAYER_OFFLINE:
                msg = new PlayerOfflineMsg();
                break;

            case MsgCmdConstant.LINK_BROKEN:
                msg = new LinkBrokenMsg();
                break;

            case MsgCmdConstant.SERVER_STATUS_UPDATE:
                msg = new ServerStatusMsg();
                break;

            case MsgCmdConstant.LOBBY_CLIENT_LINK_RECONNECT:
                msg = new LobbyReconnectMsg();
                break;
            default:
                break;
        }
        if (msg != null)
            msg.serialize(objSerializer);
        else {
            logger.error("systemMsgDecode unkown message:cmd=0x"
                    + Integer.toHexString(cmd));
        }
        //
        return msg;
    }

    //
    private static MsgBase entranceServerMsgDecode(IoBuffer buf, int cmd) {
        ObjSerializer objSerializer = new ObjSerializer(true, buf);
        MsgBase msg = null;
        //
        switch (cmd) {
            case MsgCmdConstant.ENTRANCE_SERVER_GS_UPDATE:
                msg = new GameLogicServerUpdateMsg();
                break;
            case MsgCmdConstant.ENTRANCE_SERVER_REG_GS:
                msg = new RegisterGameLogicServerMsg();
                break;
            case MsgCmdConstant.ENTRANCE_SERVER_REG_GS_ACK:
                msg = new RegisterGameLogicServerMsgAck();
                break;
            case MsgCmdConstant.ENTRANCE_SERVER_GET_GAME_SERVER_INFO:
                msg = new GetGameServerInfoMsg();
                break;
            case MsgCmdConstant.ENTRANCE_SERVER_GET_GAME_SERVER_INFO_ACK:
                msg = new GetGameServerInfoMsgAck();
                break;
            case MsgCmdConstant.ENTRANCE_SERVER_CREATE_NEW_PLAYER: {
                msg = new EntranceUserMsg();
            }
            break;
            case MsgCmdConstant.ENTRANCE_SERVER_CREATE_NEW_PLAYER_ACK: {
                msg = new EntranceUserMsgAck();
            }
            break;
            case MsgCmdConstant.ENTRANCE_SERVER_FIND_PLAYER:
                msg = new FindPlayerMsg();
                break;
            case MsgCmdConstant.ENTRANCE_SERVER_FIND_PLAYER_ACK:
                msg = new FindPlayerMsgAck();
                break;
            case MsgCmdConstant.ENTRANCE_SERVER_FIND_NICKNAME:
                msg = new ValidateMsg(
                        MsgCmdConstant.ENTRANCE_SERVER_FIND_NICKNAME);
                break;
            case MsgCmdConstant.ENTRANCE_SERVER_FIND_NICKNAME_ACK:
                msg = new ValidateMsg(MsgCmdConstant.ENTRANCE_SERVER_FIND_NICKNAME_ACK);
                break;

            default:
                break;
        }
        if (msg != null)
            msg.serialize(objSerializer);
        else {
            logger.error("entranceServerMsgDecode unkown message:cmd=0x"
                    + Integer.toHexString(cmd));
        }
        //
        return msg;
    }
}
