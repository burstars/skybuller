package com.chess.common.msg.processor.entrance;

import com.chess.core.net.mina2.BaseSocketServer;
import com.chess.core.net.msg.IMsgRev;

 
public class EntranceSocket extends BaseSocketServer
{
	public EntranceSocket(int port,String server_name,IMsgRev in_msgProc)
	{
		super(port,server_name,in_msgProc);
		
		//heart_beating_supported=false;
	}
}
