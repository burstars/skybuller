package com.chess.common.msg.processor.entrance;

import java.util.Date;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;

import com.chess.common.DateService;
import com.chess.common.framework.GameContext;
import com.chess.core.net.msg.BaseMsgThread;
 


/***
 * 游戏更新文件服务器
 * **/
public class EntranceServerThread extends BaseMsgThread 
{
	private static Logger logger = LoggerFactory.getLogger(EntranceServerThread.class);
	 
 
	private EntranceServerMsgProcessor msgProcessor=null;
	
	

 
	private Date old_loop_time=new Date();
	private Date old_loop_time20=new Date();
	
	
	public EntranceServerThread()
	{
		this.setName("EntranceServerMainLoop");
		
		msgProcessor=new EntranceServerMsgProcessor();
		//
		this.msgProc=msgProcessor;
		GameContext.csMsgProcessor=msgProcessor;
		//
		old_loop_time=DateService.getCurrentUtilDate();
 
	}
 
 
	//线程维护
	@Override
	public void mt()
	{
	}
	
	//检查网络连接有效性，如果没有连接，则尝试重连
	private void linkValidation()
	{
		if(GameContext.entranceServerConnector!=null)
		{
			GameContext.entranceServerConnector.linkCheck();
		}
	}
	

	
	
	@Override
	public void loop()
	{
		Date tt=DateService.getCurrentUtilDate();
		long dt=tt.getTime()-old_loop_time.getTime();
		
		//
		linkValidation();
		//
		if(dt>300000)//超过5分钟
		{
			old_loop_time=tt;	
			if(msgProcessor!=null){
				msgProcessor.sendToEntrance();
			}	
		}//
		long dt20=tt.getTime()-old_loop_time20.getTime();
		if(dt20>1200000)//超过20分钟
		{
			old_loop_time20=tt;	
			if(msgProcessor!=null){
				msgProcessor.remove_old_data_from_cache();
			}
		}
		
 
		
		if(GameContext.entranceServer!=null)
		{
			GameContext.entranceServer.heartBeatingCheck(true);
		}
	}
	
	
}
