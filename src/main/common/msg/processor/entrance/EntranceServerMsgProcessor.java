package com.chess.common.msg.processor.entrance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.UUIDGenerator;
import com.chess.common.bean.EntranceUser;
import com.chess.common.bean.GameLogicServerInfo;
import com.chess.common.bean.SensitiveWords;
import com.chess.common.constant.ErrorCodeConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.common.msg.struct.entranc.EntranceUserMsg;
import com.chess.common.msg.struct.entranc.EntranceUserMsgAck;
import com.chess.common.msg.struct.entranc.FindPlayerMsg;
import com.chess.common.msg.struct.entranc.FindPlayerMsgAck;
import com.chess.common.msg.struct.entranc.GameLogicServerUpdateMsg;
import com.chess.common.msg.struct.entranc.GetGameServerInfoMsg;
import com.chess.common.msg.struct.entranc.GetGameServerInfoMsgAck;
import com.chess.common.msg.struct.entranc.RegisterGameLogicServerMsg;
import com.chess.common.msg.struct.entranc.RegisterGameLogicServerMsgAck;
import com.chess.common.msg.struct.login.ReconnectMsg;
import com.chess.common.msg.struct.playeropt.ValidateMsg;
import com.chess.common.msg.struct.playeropt.ValidateMsgAck;
import com.chess.common.msg.struct.system.LinkBrokenMsg;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.IEntranceUserService;
import com.chess.common.service.ISystemConfigService;
import com.chess.core.constant.NetConstant;
import com.chess.core.net.code.NetMsgEncoder;
import com.chess.core.net.msg.BaseMsgProcessor;
import com.chess.core.net.msg.MsgBase;

public class EntranceServerMsgProcessor extends BaseMsgProcessor {
	private static Logger logger = LoggerFactory
			.getLogger(EntranceServerMsgProcessor.class);

	// 游戏服map,key为serverid
	private Map<String, GameLogicServerInfo> gsMap = new HashMap<String, GameLogicServerInfo>();
	private Map<Integer, String> table_server_map = new HashMap<Integer, String>();
	// 登录过的玩家，保留20分钟
	private Map<String, EntranceUser> userAccountMap = new HashMap<String, EntranceUser>();
	private Map<String, EntranceUser> userMachineCodeMap = new HashMap<String, EntranceUser>();
	private Map<String, EntranceUser> userQQMap = new HashMap<String, EntranceUser>();
	private Map<String, EntranceUser> userWXMap = new HashMap<String, EntranceUser>();

	private IEntranceUserService entranceService = (IEntranceUserService) SpringService
			.getBean("entranceUserService");
	private ICacheUserService playerService = (ICacheUserService) SpringService
			.getBean("playerService");

	private ISystemConfigService cfgService = null;

	private Random table_id_random = new Random();

	//
	public EntranceServerMsgProcessor() {
		this.setCheckKey(GameContext.checkKey);
	}

	//

	@Override
	public void send(IoSession session, MsgBase msg) {

		if (session == null || msg == null) {
			return;
		}
		//
		IoBuffer buf = NetMsgEncoder.encode(msg);

		if (buf != null) {
			session.write(buf);
		}
	}

	private GameLogicServerInfo get_gs_for_new_player() {
		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		//
		int max_reg = cfgService.get_max_reg_player_num();
		GameLogicServerInfo found = null;
		for (String gsid : gsMap.keySet()) {
			GameLogicServerInfo gs = gsMap.get(gsid);
			if (gs.onlinePlayerNum < max_reg) {
				found = gs;
				break;
			}
		}
		//
		return found;
	}

	//
	private void create_player_ack(EntranceUserMsgAck msg) {
		playerService.createPlayerAck(msg);
	}

	private EntranceUser findPlayerByAccount(String acc) {
		EntranceUser cu = userAccountMap.get(acc);
		if (cu != null)
			return cu;
		cu = entranceService.getUserByAccount(acc);
		return cu;
	}

	private SensitiveWords findSensitiveWords(String word) {

		SensitiveWords words = entranceService.getSensitiveWords(word);
		return words;
	}

	private EntranceUser findplayerByPlayerName(String nickName) {
		EntranceUser cu = entranceService.getUserByNickName(nickName);
		return cu;
	}

	private void create_player(EntranceUserMsg msg, IoSession session) {
		EntranceUser usr = null;

		EntranceUserMsgAck axk = new EntranceUserMsgAck();
		axk.password = msg.password;
		axk.playerID = msg.playerID;
		axk.machine_code = msg.machine_code;
		axk.qqOpenID = msg.qqOpenID;
		axk.wxOpenID = msg.wxOpenID;
		axk.playerName = msg.playerName;
		axk.sessionID = msg.sessionID;
		axk.account = msg.account;
		axk.playerIndex = msg.playerIndex;
		axk.deviceFlag = msg.deviceFlag;
		axk.unionId = msg.wxUnionId;
		axk.param01 = msg.param01;// 20161203
		axk.ip = msg.ip;
		axk.deviceBrand = msg.deviceBrand;
		axk.systemVersion = msg.systemVersion;
		axk.phone = msg.phone;
		axk.result = ErrorCodeConstant.CMD_EXE_OK;
		axk.location = msg.location;
		axk.sex = msg.sex;
		//
		if (msg.account != null && msg.account.length() > 1) {
			usr = entranceService.getUserByAccount(msg.account);
			if (usr != null) {
				axk.result = ErrorCodeConstant.ACCOUNT_ALREADY_EXIST;
			}
		} else if (msg.machine_code != null && msg.machine_code.length() > 1) {
			usr = entranceService.getUserByMachineCode(msg.machine_code);
			if (usr != null) {
				axk.result = ErrorCodeConstant.MACHINE_CODE_ALREADY_EXIST;
			}
		} else if (msg.qqOpenID != null && msg.qqOpenID.length() > 1) {
			usr = entranceService.getUserByQQOpenID(msg.qqOpenID);
			if (usr != null) {
				axk.result = ErrorCodeConstant.MACHINE_CODE_ALREADY_EXIST;
			}
		} 
//		else if (msg.wxOpenID != null && msg.wxOpenID.length() > 1) {
//			usr = entranceService.getUserByWXOpenID(msg.wxOpenID);
//			if (usr != null) {
//				axk.result = ErrorCodeConstant.MACHINE_CODE_ALREADY_EXIST;
//			}
//		}
		//cc modify 2017-12-5
		else if (msg.wxUnionId != null && msg.wxUnionId.length() > 1) {
			usr = entranceService.getUserByWXUnionID(msg.wxUnionId);
			if (usr != null) {
				axk.result = ErrorCodeConstant.MACHINE_CODE_ALREADY_EXIST;
			}
		}

		//
		if (usr == null) {
			usr = new EntranceUser();
			usr.setAccount(msg.account);
			usr.setMachineCode(msg.machine_code);
			usr.setQqOpenID(msg.qqOpenID);
			usr.setWxOpenID(msg.wxOpenID);
			usr.setServerID(msg.serverID);
			usr.setPlayerID(msg.playerID);
			usr.setPlayerName(msg.playerName);
			usr.setUserID(UUIDGenerator.generatorUUID());
			usr.setWxUnionID(msg.wxUnionId);//cc modify 2017-12-5
			entranceService.addUser(usr);
			//
			usr.setLoginDate(DateService.getCurrentUtilDate().getTime());
			//
			if (msg.account != null && msg.account.length() > 1) {
				userAccountMap.put(msg.account, usr);
			} else if (msg.machine_code != null
					&& msg.machine_code.length() > 1) {
				userMachineCodeMap.put(msg.machine_code, usr);
			} else if (msg.qqOpenID != null && msg.qqOpenID.length() > 1) {
				userQQMap.put(msg.qqOpenID, usr);
			} 
//			else if (msg.wxOpenID != null && msg.wxOpenID.length() > 1) {
//				userWXMap.put(msg.wxOpenID, usr);
//			}
			else if (msg.wxUnionId != null && msg.wxUnionId.length() > 1) {
				userWXMap.put(msg.wxUnionId, usr);//cc modify 2017-12-5
			}
		}

		// session.setAttribute("CreateUserMsgAck", axk);
		// RequestClientCompletePhoneNumAck ack = new
		// RequestClientCompletePhoneNumAck();
		// ack.sessionID = msg.sessionID;
		// this.sendMsg(session, ack);
		this.send(session, axk);
	}

	//
	public void add_server(RegisterGameLogicServerMsg rgm, IoSession session) {
		GameLogicServerInfo info = gsMap.get(rgm.serverID);
		if (info == null)// 有可能断线重连了
		{
			info = new GameLogicServerInfo();
			info.telecomIP = rgm.telecomIP;
			info.unicomIP = rgm.unicomIP;
			info.gamePort = rgm.gamePort;
			info.patchPort = rgm.patchPort;
			info.serverName = rgm.serverName;
			info.serverID = rgm.serverID;
			//

			gsMap.put(info.serverID, info);
		}
		// 有可能断线重连了，gs重启又进来了
		info.session = session;
		//
		logger.debug("add new server,ID=" + info.serverID);// System.out.println("add new server,ID="
															// + info.serverID);
		//
		RegisterGameLogicServerMsgAck axk = new RegisterGameLogicServerMsgAck();
		axk.serverID = info.serverID;
		this.send(session, axk);
	}

	//
	public void server_update(GameLogicServerUpdateMsg msg) {
		GameLogicServerInfo info = gsMap.get(msg.serverID);
		if (info == null)
			return;
		//
		info.onlinePlayerNum = msg.linkNum;
		info.room0PlayerNum = msg.r0Nnum;
		info.room1PlayerNum = msg.r1Nnum;
		info.room2PlayerNum = msg.r2Nnum;
		info.room3PlayerNum = msg.r3Nnum;

		// System.out.println("server_update serverID="+msg.serverID);
	}

	// 把老数据移除掉
	public void remove_old_data_from_cache() {
		List<EntranceUser> removeme = new ArrayList<EntranceUser>();
		long ct = DateService.getCurrentUtilDate().getTime();
		for (String kkey : userMachineCodeMap.keySet()) {
			EntranceUser usr = userMachineCodeMap.get(kkey);
			if (ct - usr.getLoginDate() > 1200000)
				removeme.add(usr);
		}
		//
		for (int i = 0; i < removeme.size(); i++) {
			EntranceUser usr = removeme.get(i);
			userMachineCodeMap.remove(usr.getMachineCode());
		}
		//
		removeme.clear();
		//
		for (String kkey : userAccountMap.keySet()) {
			EntranceUser usr = userAccountMap.get(kkey);
			if (ct - usr.getLoginDate() > 1200000)
				removeme.add(usr);
		}
		//
		for (int i = 0; i < removeme.size(); i++) {
			EntranceUser usr = removeme.get(i);
			userAccountMap.remove(usr.getAccount());
		}
		//
		removeme.clear();
	}

	//
	// 向入口服务器报告在线人数等信息
	public void sendToEntrance() {
		if (GameContext.entranceServerConnector != null) {
			//
			GameLogicServerUpdateMsg msg = new GameLogicServerUpdateMsg();
			msg.linkNum = GameContext.gameSocket.getManagedSessionCount();
			msg.serverID = GameContext.serverID;
			//
			GameContext.entranceServerConnector.send(msg);
		}
	}

	//
	private void get_gs_info(GetGameServerInfoMsg msg, IoSession session) {
		GameLogicServerInfo info = null;
		//
		EntranceUser user = null;
		if (msg.account != null && msg.account.length() > 1) {
			user = userAccountMap.get(msg.account);
			//
			if (user == null) {
				user = entranceService.getUserByAccount(msg.account);
				if (user != null) {
					userAccountMap.put(user.getAccount(), user);
				}
			}
		} else if (msg.machine_code != null && msg.machine_code.length() > 1) {
			user = userMachineCodeMap.get(msg.machine_code);
			//
			if (user == null) {
				user = entranceService.getUserByMachineCode(msg.machine_code);
				if (user != null) {
					userMachineCodeMap.put(user.getMachineCode(), user);
				}
			}
		} else if (msg.qqOpenID != null && msg.qqOpenID.length() > 1) {
			user = userQQMap.get(msg.qqOpenID);
			//
			if (user == null) {
				user = entranceService.getUserByQQOpenID(msg.qqOpenID);
				if (user != null) {
					userQQMap.put(user.getQqOpenID(), user);
				}
			}
		} 
//		else if (msg.wxOpenID != null && msg.wxOpenID.length() > 1) {
//			user = userWXMap.get(msg.wxOpenID);
//			//
//			if (user == null) {
//				user = entranceService.getUserByWXOpenID(msg.wxOpenID);
//				if (user != null) {
//					userWXMap.put(user.getWxOpenID(), user);
//				}
//			}
//		}
		//cc modify 2017-12-5
		else if (msg.wxUnionID != null && msg.wxUnionID.length() > 1) {
			user = userWXMap.get(msg.wxUnionID);
			//
			if (user == null) {
				user = entranceService.getUserByWXUnionID(msg.wxUnionID);
				if (user != null) {
					userWXMap.put(user.getWxUnionID(), user);
				}
			}
		}

		// 老玩家
		if (user != null) {
			info = gsMap.get(user.getServerID());
		} else {
			// 新用户
			info = get_gs_for_new_player();
		}
		//
		GetGameServerInfoMsgAck axk = new GetGameServerInfoMsgAck();
		if (info != null) {
			axk.serverID = info.serverID;
			axk.gamePort = info.gamePort;
			axk.patchPort = info.patchPort;
			axk.telecomIP = info.telecomIP;
			axk.unicomIP = info.unicomIP;
		}
		this.send(session, axk);
	}

	//
	@Override
	protected void entranceServerMsgProcess(MsgBase msg, IoSession session) {

		// 校验不通过的一律不处理消息
		Integer cr = (Integer) session
				.getAttribute(NetConstant.LINK_CHECK_RESULT);
		if (cr == null || cr == 1)
			return;

		// 设置下属性，如果一个链接，链接到补丁服务器，最后一个消息后1分钟，就被强行断开
		Long dt = DateService.getCurrentUtilDate().getTime();
		session.setAttribute(NetConstant.ENTRANCE_SERVER_LAST_MSG_TIME, dt);
		//
		msgProcess(msg, session);
	}

	@Override
	protected void gameMsgProcess(MsgBase msg, IoSession session) {
		switch (msg.msgCMD) {

		// 断线重链接
		case MsgCmdConstant.GAME_RECONNECT_IN:
			ReconnectMsg rmsg = (ReconnectMsg) msg;
			Log.error("entrance server--发现断线重链接,account" + rmsg.account
					+ ",code=" + rmsg.machineCode);
			break;
		default:
			break;
		}
	}

	//
	private void msgProcess(MsgBase msg, IoSession session) {
		//
		int cmd = msg.msgCMD;

		switch (cmd) {
		case MsgCmdConstant.ENTRANCE_SERVER_GS_UPDATE: {
			GameLogicServerUpdateMsg gsu = (GameLogicServerUpdateMsg) msg;
			server_update(gsu);
		}
			break;
		//
		case MsgCmdConstant.ENTRANCE_SERVER_REG_GS: {
			RegisterGameLogicServerMsg rgm = (RegisterGameLogicServerMsg) msg;
			add_server(rgm, session);
		}
			break;
		case MsgCmdConstant.ENTRANCE_SERVER_REG_GS_ACK: {
			RegisterGameLogicServerMsgAck rgmaxkg = (RegisterGameLogicServerMsgAck) msg;
			logger.debug("myServerID=" + rgmaxkg.serverID);// System.out.println("myServerID="
															// +
															// rgmaxkg.serverID);
		}
			break;
		//
		case MsgCmdConstant.ENTRANCE_SERVER_GET_GAME_SERVER_INFO: {
			GetGameServerInfoMsg ggs = (GetGameServerInfoMsg) msg;
			get_gs_info(ggs, session);
		}
			break;
		case MsgCmdConstant.ENTRANCE_SERVER_CREATE_NEW_PLAYER: {
			create_player((EntranceUserMsg) msg, session);
		}
			break;
		case MsgCmdConstant.ENTRANCE_SERVER_CREATE_NEW_PLAYER_ACK: {
			create_player_ack((EntranceUserMsgAck) msg);
		}
			break;
		case MsgCmdConstant.ENTRANCE_SERVER_FIND_PLAYER: {
			FindPlayerMsg fpm = (FindPlayerMsg) msg;
			EntranceUser usr = findPlayerByAccount(fpm.account);

			FindPlayerMsgAck axxk = new FindPlayerMsgAck();
			axxk.result = 0;
			if (usr != null)
				axxk.result = 1;

			this.send(session, axxk);
		}
			break;
		case MsgCmdConstant.ENTRANCE_SERVER_FIND_NICKNAME:
			ValidateMsg validateMsg = (ValidateMsg) msg;

			EntranceUser usr = findplayerByPlayerName(validateMsg.optStr);

			SensitiveWords words = findSensitiveWords(validateMsg.optStr);

			ValidateMsgAck validateMsgAck = new ValidateMsgAck(
					MsgCmdConstant.ENTRANCE_SERVER_FIND_NICKNAME_ACK);

			if (usr != null)
				validateMsgAck.result = 1;// 昵称重复
			if (words != null)
				validateMsgAck.result = 2;// 涉及敏感字

			this.send(session, validateMsgAck);
			break;
		/*
		 * //分布式服务器 //入口服务器接收到创建vip桌子请求 case
		 * MsgCmdConstant.ENTRANCE_SERVER_CREATE_VIP_TABLE: {
		 * CSCreateVipTableMsg cs_msg = (CSCreateVipTableMsg) msg; //生成id int id
		 * = generate_vip_table_id(); int result =
		 * GameConstant.ENTRANCE_SERVER_OPERATION_RESULT_FAILED; if (id != 0) {
		 * table_server_map.put(id, cs_msg.server_id); result =
		 * GameConstant.ENTRANCE_SERVER_OPERATION_RESULT_SUCCESS; } //返回给游戏服务器
		 * CSCreateVipTableMsgAck cs_ack = new CSCreateVipTableMsgAck();
		 * cs_ack.server_id = cs_msg.server_id; cs_ack.player_session_id =
		 * cs_msg.player_session_id; cs_ack.table_id = id; cs_ack.result =
		 * result; this.sendMsg(session, cs_ack); break; }
		 * 
		 * //入口服务器接收到销毁vip桌子请求 case
		 * MsgCmdConstant.ENTRANCE_SERVER_DESTROY_VIP_TABLE: { //与create使用同一结构
		 * CSOperationTableMsg cs_msg = (CSOperationTableMsg) msg; if
		 * (table_server_map.get(cs_msg.table_id) != null) {
		 * table_server_map.remove(cs_msg.table_id); } break; }
		 * 
		 * //入口服务器接收到查找vip桌子请求 case
		 * MsgCmdConstant.ENTRANCE_SERVER_FIND_VIP_TABLE: { CSFindVipTableMsg
		 * cs_msg = (CSFindVipTableMsg) msg; //在其它逻辑服找到桌子 //返回一个结构让客户端重新连接这个逻辑服
		 * String server_id = table_server_map.get(cs_msg.table_id); if
		 * (server_id != null && cs_msg.server_id != server_id) {
		 * GameLogicServerInfo server_info = gsMap.get(server_id);
		 * 
		 * CSFindVipTableMsgAck cs_msg_ack = new CSFindVipTableMsgAck();
		 * cs_msg_ack.server_id = server_id;
		 * 
		 * } }
		 */

		// add end

		default:
			logger.error("unkown msg，cmd=" + msg.msgCMD + "----"
					+ Integer.toHexString(msg.msgCMD));
			break;
		}

	}

	// 向其入口服务器发起的连接，返回校验通过
	@Override
	protected void link_validation_passed(IoSession session,
			String server_name, String linkName) {

		// 需要注册
		if (GameContext.entranceServerConnector != null) {
			//
			session.setAttribute(NetConstant.LINK_CHECK_RESULT,
					NetConstant.LINK_CHECK_OK);

			RegisterGameLogicServerMsg rgm = new RegisterGameLogicServerMsg();
			rgm.telecomIP = GameContext.telecomIP;
			rgm.unicomIP = GameContext.unicomIP;
			rgm.gamePort = GameContext.gameSocketPort;
			rgm.patchPort = GameContext.patchServerSocketPort;
			rgm.serverName = GameContext.serverName;
			rgm.serverID = GameContext.serverID;
			//
			GameContext.entranceServerConnector.send(rgm);
		}

	}

	// 玩家断线
	@Override
	protected void playerLinkBroken(LinkBrokenMsg msg, IoSession session) {

	}

	public IEntranceUserService getEntranceServer() {
		return entranceService;
	}

	public void setEntranceServer(IEntranceUserService entranceService) {
		this.entranceService = entranceService;
	}

}
