package com.chess.common.msg.processor.game;

import java.util.Date;

import org.slf4j.Logger;import org.slf4j.LoggerFactory;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ICacheUserService;
import com.chess.core.net.msg.BaseMsgThread;


/***
 * 游戏主循环，游戏里面所有需要定时处理或者，每帧处理的时间，都从这里发起
 **/
public class GameServerThread extends BaseMsgThread {
    private static Logger logger = LoggerFactory.getLogger(GameServerThread.class);


    private ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");


    private Date old_loop_time = new Date();
    private Date old_loop_time_10mins = new Date();

    public GameServerThread() {
        this.setName("DBServerMainLoop");

        this.msgProc = new GameServerMsgProcessor();
        serverStatus.linkName = GameContext.serverName;

        old_loop_time = DateService.getCurrentUtilDate();
        old_loop_time_10mins = DateService.getCurrentUtilDate();
    }


    //线程维护
    @Override
    public void mt() {
    }

    @Override
    public void loop() {
        Date tt = DateService.getCurrentUtilDate();
        long dt = tt.getTime() - old_loop_time.getTime();

        //
        if (dt > 60000)//超过1分钟
        {
            old_loop_time = tt;

        }
        //10mins调度
        if (tt.getTime() - old_loop_time_10mins.getTime() > 240000)//10分钟
        {
            playerService.schedule_10_minute();
            old_loop_time_10mins = tt;
        }
        //
        playerService.run_one_loop();
        //
        if (GameContext.gameSocket != null) {
            GameContext.gameSocket.heartBeatingCheck(false);
        }
    }

    //
    public int get10minLeft() {
        Date tt = DateService.getCurrentUtilDate();
        long dt = 240000 - (tt.getTime() - old_loop_time_10mins.getTime());
        if (dt < 0)
            dt = 0;
        //
        return (int) dt;
    }



}
