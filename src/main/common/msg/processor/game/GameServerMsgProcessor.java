package com.chess.common.msg.processor.game;

import java.net.SocketAddress;
import java.util.List;

import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.bean.User;
import com.chess.common.constant.MessageConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.common.msg.struct.club.ClubChargeLevelMsg;
import com.chess.common.msg.struct.club.ClubChongZhiMsg;
import com.chess.common.msg.struct.club.ClubRechargeMsg;
import com.chess.common.msg.struct.club.CreateClubMsg;
import com.chess.common.msg.struct.club.FreeTableManagerMsg;
import com.chess.common.msg.struct.club.GetClubsMsg;
import com.chess.common.msg.struct.club.GetQyqRankListMsg;
import com.chess.common.msg.struct.club.GetSingleClubInfoMsg;
import com.chess.common.msg.struct.club.GetSingleClubInfoMsgAck;
import com.chess.common.msg.struct.club.JoinClubMsg;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomAckMsg;
import com.chess.common.msg.struct.login.Login2Msg;
import com.chess.common.msg.struct.login.LoginMsg;
import com.chess.common.msg.struct.login.ReconnectMsg;
import com.chess.common.msg.struct.luckydraw.GetPrizeListMsg;
import com.chess.common.msg.struct.luckydraw.LuckyDrawMsg;
import com.chess.common.msg.struct.message.PostMessageMsg;
import com.chess.common.msg.struct.message.PostMessageMsgAck;
import com.chess.common.msg.struct.message.TestCard;
import com.chess.common.msg.struct.message.TestCardAck;
import com.chess.common.msg.struct.other.AskRecoverGameMsg;
import com.chess.common.msg.struct.other.AskRecoverPlayerStateMsg;
import com.chess.common.msg.struct.other.CheckOfflineBackMsg;
import com.chess.common.msg.struct.other.CreateVipRoomMsg;
import com.chess.common.msg.struct.other.EnterVipRoomMsg;
import com.chess.common.msg.struct.other.GameReadyMsg;
import com.chess.common.msg.struct.other.GameUpdateMsg;
import com.chess.common.msg.struct.other.PlayerGameOpertaionMsg;
import com.chess.common.msg.struct.other.PlayerGameOverMsg;
import com.chess.common.msg.struct.other.PlayerTableOperationMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomCloseMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomRecordMsg;
import com.chess.common.msg.struct.other.RecordMsg;
import com.chess.common.msg.struct.other.RequestPlayerHuTypeMsgAck;
import com.chess.common.msg.struct.other.RequestStartGameMsg;
import com.chess.common.msg.struct.other.SearchUserByIndexMsg;
import com.chess.common.msg.struct.other.WelFareMsg;
import com.chess.common.msg.struct.pay.GameBuyItemCompleteMsg;
import com.chess.common.msg.struct.pay.GameBuyItemMsg;
import com.chess.common.msg.struct.pay.GameIPABuyItemCompleteMsg;
import com.chess.common.msg.struct.playeropt.RefreshItemBaseMsgAck;
import com.chess.common.msg.struct.system.LinkBrokenMsg;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.IClubService;
import com.chess.common.service.IExchangePrizeService;
import com.chess.common.service.IMallItemService;
import com.chess.common.service.IRecordService;
import com.chess.common.service.impl.LoginService;
import com.chess.core.constant.NetConstant;
import com.chess.core.net.msg.BaseMsgProcessor;
import com.chess.core.net.msg.MsgBase;
import com.chess.nndzz.table.GameRoom;
import com.chess.nndzz.table.GameTable;

public class GameServerMsgProcessor extends BaseMsgProcessor {
    private static Logger logger = LoggerFactory.getLogger(GameServerMsgProcessor.class);

    private ICacheUserService playerService = (ICacheUserService) SpringService.getBean("playerService");

    private IClubService clubService = (IClubService) SpringService.getBean("clubService");

    //private ITaskService taskService = (ITaskService) SpringService.getBean("taskService");
   
    private LoginService loginService = (LoginService) SpringService.getBean("loginService");
    private IMallItemService mallItemService = (IMallItemService) SpringService.getBean("mallItemService");
    private IExchangePrizeService exchangPrizeService = (IExchangePrizeService) SpringService.getBean("exchangePrizeService");
    private IRecordService recordService = (IRecordService) SpringService.getBean("recordService");
    private GameServerAssistMsgProcessor dbserver_helper = null;
    private GameServerAssistMsgProcessor dbserver_helper_bind_phone = null;
    private GameServerAssistMsgProcessor dbserver_reg_friend = null;
    private GameServerAssistMsgProcessor dbserver_vip_game_record = null;
    private GameServerAssistMsgProcessor dbserver_exchange_prize = null;
    private GameServerAssistMsgProcessor dbserver_club_member = null;

    //private boolean use_helper_thread = true;

    public GameServerMsgProcessor() {
        this.setCheckKey(GameContext.checkKey);
        //if(use_helper_thread) {
        dbserver_helper = new GameServerAssistMsgProcessor(playerService, mallItemService,exchangPrizeService);
        dbserver_helper.start();
        dbserver_helper_bind_phone = new GameServerAssistMsgProcessor(playerService, mallItemService,exchangPrizeService);
        dbserver_helper_bind_phone.start();
        dbserver_reg_friend = new GameServerAssistMsgProcessor(playerService, mallItemService,exchangPrizeService);
        dbserver_reg_friend.start();
        dbserver_club_member = new GameServerAssistMsgProcessor(playerService, mallItemService,exchangPrizeService);
        dbserver_club_member.start();
        dbserver_vip_game_record = new GameServerAssistMsgProcessor(playerService, mallItemService,exchangPrizeService);
        dbserver_vip_game_record.start();
        dbserver_exchange_prize = new GameServerAssistMsgProcessor(playerService, mallItemService,exchangPrizeService);
        dbserver_exchange_prize.start();
        //}

    }

    private GameServerMessage construct_dbmsg(MsgBase msg, IoSession session, User pl) {
        //if(dbserver_helper == null) {


        //}
        GameServerMessage dbmsg = new GameServerMessage();
        dbmsg.msg = msg;
        dbmsg.session = session;
        dbmsg.pl = pl;
        return dbmsg;
    }

    @Override
    protected void gameMsgProcess(MsgBase msg, IoSession session) {
        User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);
        // System.out.println("lo5bbyProcess");
        // int cmd = msg.msgCMD;
        // animalTicketService.test();
        // System.out.print(cmd);
        
        long ctt = DateService.getCurrentUtilDate().getTime();
        long delta = 0;
        switch (msg.msgCMD) {
            case MsgCmdConstant.GAME_LOGIN:
                LoginMsg login = (LoginMsg) msg;
                playerService.login(login, session);
                break;
            // 现在客户端直接重连接是发登录消息，这个废了
            case MsgCmdConstant.GAME_RECONNECT_IN:
                ReconnectMsg rmsg = (ReconnectMsg) msg;
                logger.debug("断线重链");//System.out.println("断线重链");
                playerService.reconnect(rmsg, session);
                //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            // 注册新用户
            case MsgCmdConstant.GAME_REGISTER_PLAYER:
                //RegisterPlayerMsg rpm = (RegisterPlayerMsg) msg;
                //playerService.regNewPlayer(rpm, session);
                dbserver_reg_friend.push_msg(construct_dbmsg(msg, session, pl));
                break;
            
            case MsgCmdConstant.GAME_READY:
            	logger.info("--->GameServerMsgProcess开局准备<---");
            	GameReadyMsg gameRedayMsg = (GameReadyMsg)msg;
            	playerService.gameReady(gameRedayMsg, session);
            	break;
            case MsgCmdConstant.GAME_START_GAME_REQUEST:
                RequestStartGameMsg rsmg = (RequestStartGameMsg) msg;
                playerService.enter_room(rsmg, session);
                //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            case MsgCmdConstant.GAME_ASK_GAME_RECOVER:{
            	AskRecoverGameMsg armsg = (AskRecoverGameMsg) msg;
            	playerService.ask_recover(armsg, session);
            	break;
            }
            case MsgCmdConstant.GAME_ASK_RECOVER_PLAYER_STATE:{
            	AskRecoverPlayerStateMsg arMsg = (AskRecoverPlayerStateMsg) msg;
            	playerService.ask_recover_state(arMsg, session);
            	break;
            }
            case MsgCmdConstant.GAME_CHECK_OFFLINE_BACK:
        	{
            	CheckOfflineBackMsg cmsg = (CheckOfflineBackMsg) msg;
            	playerService.check_reroom(cmsg, session);
            	break;
            }
            case MsgCmdConstant.GAME_GAME_UPDATE:
            {
                GameUpdateMsg gum = (GameUpdateMsg) msg;
                playerService.gameUpdate(gum, session);
                break;
            }
            case MsgCmdConstant.GAME_GAME_OPERTAION:
            {
                PlayerGameOpertaionMsg pgom = (PlayerGameOpertaionMsg) msg;
                playerService.playerGameOperation(pgom, session);
                //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_GAME_OVER:
            {
                PlayerGameOverMsg pgovm = (PlayerGameOverMsg) msg;
                playerService.playerGameOver(pgovm, session);
                //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_GET_PRIZE_LIST:
            {
                GetPrizeListMsg gplm = (GetPrizeListMsg) msg;
                playerService.sendPrizeListToCS(session);
                break;
            }
            case MsgCmdConstant.GAME_LUCKYDRAW:
            {
                LuckyDrawMsg ldm = (LuckyDrawMsg) msg;
                playerService.luckyDraw(session, ldm);
                break;
            }
            case MsgCmdConstant.GAME_SEND_PLAYER_OPERATIOIN_STRING:
            {
                //PlayerOpertaionMsg opom = (PlayerOpertaionMsg) msg;
                //player_OPT_str(pl, opom);
                dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_USER_MIN: 
            {
            	this.playerService.min2Nodify(msg,pl);
                break;
            }
            case MsgCmdConstant.GAME_USER_TABLE_OPERATION:
            {
                // 玩家操作
                PlayerTableOperationMsg opnm = (PlayerTableOperationMsg) msg;
                this.playerService.playerOperation(opnm, pl);
                break;
            }
            case MsgCmdConstant.FRIEND_PROPERTY:
            {
                //FriendOprateMsg opom = (FriendOprateMsg) msg;
                //friend_OPT_str(pl, opom);

                dbserver_reg_friend.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.CLUB_MEMBER:
            {
                dbserver_club_member.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.POST_USER_INFO:
            {
                PostMessageMsg postMsg = (PostMessageMsg) msg;
                // 处理消息
                DealPostMessage(pl, postMsg);
                break;
            }
            case MsgCmdConstant.GET_VIP_ROOM_RECORD:
            {
                //VipRoomRecordMsg vrrm = (VipRoomRecordMsg) msg;
                //this.playerService.getVipRoomRecord(vrrm, session);
                dbserver_vip_game_record.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GET_PROXY_VIP_ROOM_RECORD:
            {//代开优化 cc modify 2017-9-26
                ProxyVipRoomRecordMsg pvrrm = (ProxyVipRoomRecordMsg) msg;
                this.playerService.getProxyVipRoomRecord(pvrrm, session);
                break;
            }
            case MsgCmdConstant.GET_VIP_GAME_RECORD:
            {
                //VipGameRecordMsg vgrm = (VipGameRecordMsg) msg;
                //this.playerService.getVipGameRecord(vgrm, session);
                dbserver_vip_game_record.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_PROXY_VIP_ROOM_CLOSE:
            	//强制关闭代理开的房间
            	ProxyVipRoomCloseMsg proxyClose = (ProxyVipRoomCloseMsg) msg;
            	//TODO 强制关闭代理开的房间
            	this.playerService.proxyCloseVipRoomRecord(proxyClose, session);
            	break;
            case MsgCmdConstant.GAME_VIP_CREATE_ROOM:// 创建vip房间
            {
                CreateVipRoomMsg cmsg = (CreateVipRoomMsg) msg;
                this.playerService.createVipRoom(cmsg, pl);
                //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_ENTER_VIP_ROOM:// 进入vip房间
            {
                EnterVipRoomMsg emsg = (EnterVipRoomMsg) msg;
                this.playerService.enterVipRoom(emsg, pl);
                //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_INVITE_FRIEND_ENTER_VIP_ROOM:
            {
                //InviteFriendEnterVipRoomMsg ivf = (InviteFriendEnterVipRoomMsg) msg;
                //this.playerService.inviteFriendToVipRoom(ivf, pl);
                dbserver_reg_friend.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_RECORD_DEFINE:
            {
            	RecordMsg rma = (RecordMsg) msg;
                this.recordService.selectRecordById(rma,pl);
                break;
            }
            case MsgCmdConstant.GAME_INVITE_FRIEND_ENTER_VIP_ROOM_ACK:
            {
            	InviteFriendEnterVipRoomAckMsg inviteAckMsg = (InviteFriendEnterVipRoomAckMsg) msg;
            	this.playerService.beInvitedAck(inviteAckMsg, pl);
            	//dbserver_reg_friend.push_msg(construct_dbmsg(msg, session, pl));
            	break;
            }
            case MsgCmdConstant.GAME_BUY_ITEM:
            {
                GameBuyItemMsg gbim = (GameBuyItemMsg) msg;
                this.playerService.generateOrder(gbim, pl);
                //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_REFRESH_ITEM_BASE:
            {
                RefreshItemBaseMsgAck ribma = new RefreshItemBaseMsgAck();
                ribma.baseItemList = this.playerService.getItemBaseByPhone(pl, mallItemService.refreshItemBase());
                GameContext.gameSocket.send(pl.getSession(), ribma);
                //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_PAY_ITEM_COMPLETE:
            {
                
                GameBuyItemCompleteMsg paycomplete = (GameBuyItemCompleteMsg)msg;
                this.playerService.completePay(pl,paycomplete);
                
                break;
            }
            case MsgCmdConstant.GAME_PAY_ITEM_IPA_COMPLETE://IOS专用
            {
                GameIPABuyItemCompleteMsg ipacomplete = (GameIPABuyItemCompleteMsg) msg;
                this.playerService.ipaCompletePay(pl, ipacomplete);
                break;
            }
            case MsgCmdConstant.TEST_CARD:
            {
                //测试牌型，发布到时候要注释掉
                //TestCard testmsg = (TestCard)msg;
                //test(pl,testmsg);
                break;
            }
            case MsgCmdConstant.TALKING_IN_GAME:
            {
                //TalkingInGameMsg talkMsg = (TalkingInGameMsg)msg;
                //playerService.playerTalkingInGame(pl, talkMsg);
                dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.HU_DONG_BIAO_QING:
            {
            	dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.DDZ_HU_DONG_BIAO_QING:
            {
            	dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.BANCHENG_HU_DONG_BIAO_QING:
            {
            	dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.XUANZHAN_HU_DONG_BIAO_QING:
            	dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
            	break;
            case MsgCmdConstant.CHJ_HU_DONG_BIAO_QING:
            {
            	dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.UPDATE_FRIEND_REMARK:
            {
                //UpdateFriendRemarkMsg markMsg = (UpdateFriendRemarkMsg)msg;
                //playerService.updatePlayerRemark(pl, markMsg);
                dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.MOBILE_CODE:
            {
                //短信验证�?
                //MobileCodeMsg mcm = (MobileCodeMsg)msg;
                //playerService.sendMobileCodeRequest(mcm, session);
                dbserver_helper_bind_phone.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.REQUEST_COMPLETE_PHONE_NUMBER:
            {
                //绑定手机号码
                //RequestCompletePhoneNumber completeMsg = (RequestCompletePhoneNumber) msg;
                //playerService.completeMoblieNumber(pl, completeMsg);
                dbserver_helper_bind_phone.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            //玩家申请解散房间
            case MsgCmdConstant.GAME_CLOSE_ROOM:
            {
                //PlayerAskCloseVipRoomMsg closeVipRoomMsg = (PlayerAskCloseVipRoomMsg)msg;
                //playerService.player_close_vip_room(closeVipRoomMsg, pl);
                dbserver_reg_friend.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            //完善资料
            case MsgCmdConstant.REQUEST_BIND_PHONE:
            {
                //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                dbserver_helper_bind_phone.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_USER_HU_TYPE:
            {
                RequestPlayerHuTypeMsgAck msgAck = new RequestPlayerHuTypeMsgAck();
                msgAck.playerHuTypes = playerService.getPlayerHuTypesByPlayerID(pl.getPlayerID());

                GameContext.gameSocket.send(pl.getSession(), msgAck);
                break;
            }
            case MsgCmdConstant.EXCHANGE_PIRZE:
            {
            	dbserver_exchange_prize.push_msg(construct_dbmsg(msg, session, pl));
            	break;
            }
            case MsgCmdConstant.GAME_LOGIN2:
            {
            	Login2Msg login2 = (Login2Msg) msg;
                playerService.login2(login2, session);
            	break;
            }
            	//电子庄牛牛
            case MsgCmdConstant.GAME_VIP_CREATE_ROOM_NNDZZ:
            {
            	com.chess.nndzz.msg.struct.other.CreateVipRoomMsg cmsg = (com.chess.nndzz.msg.struct.other.CreateVipRoomMsg) msg;
            	 this.playerService.createVipRoom(cmsg, pl);
            	 break;
            }
            case MsgCmdConstant.GAME_ENTER_VIP_ROOM_NNDZZ:// 进入vip房间
            {
                com.chess.nndzz.msg.struct.other.EnterVipRoomMsg emsg = (com.chess.nndzz.msg.struct.other.EnterVipRoomMsg) msg;
                this.playerService.enterVipRoom(emsg, pl);
                break;
            }
            case MsgCmdConstant.GAME_USER_TABLE_OPERATION_NNDZZ:
            {
                // 玩家操作
                com.chess.nndzz.msg.struct.other.PlayerTableOperationMsg opnm = (com.chess.nndzz.msg.struct.other.PlayerTableOperationMsg) msg;
                this.playerService.playerOperation(opnm, pl);
                break;
            }
            case MsgCmdConstant.GAME_READY_NNDZZ:{
            	com.chess.nndzz.msg.struct.other.GameReadyMsg gameRedayMsgnndzz = (com.chess.nndzz.msg.struct.other.GameReadyMsg)msg;
            	playerService.gameReadyNNDZZ(gameRedayMsgnndzz, session);
            	break;
            }
            	
            case MsgCmdConstant.WELFARE_MSG:{
            	WelFareMsg  welFareMsg = (WelFareMsg) msg;
            	playerService.welFare(pl, welFareMsg);
            	break;
            }
                /*俱乐部*/
            case MsgCmdConstant.GAME_CLUB_VIP_KZGL:{
            	FreeTableManagerMsg freeTableManagerMsg = (FreeTableManagerMsg) msg;
//            	clubService.updateClubTemplate(pl,freeTableManagerMsg);
            	playerService.updateClubTemplate(pl, freeTableManagerMsg);
            	break;
            }
            case MsgCmdConstant.GAME_CLUB_GET_CLUBS:// 查询俱乐部
            {
                GetClubsMsg getClubsMsg = (GetClubsMsg) msg;
//                this.clubService.queryClubs(pl,getClubsMsg);
                playerService.queryClubs(pl, getClubsMsg);
                break;
            }
            case MsgCmdConstant.GAME_CLUB_GET_WINNER:
            {
                dbserver_vip_game_record.push_msg(construct_dbmsg(msg, session, pl));
                break;
            }
            case MsgCmdConstant.GAME_CLUB_CREATE_CLUB:// 新建俱乐部
            {
                CreateClubMsg createClubMsg = (CreateClubMsg) msg;
                this.playerService.createClub(pl,createClubMsg);
                break;
            }
            case MsgCmdConstant.GAME_CLUB_JOIN_CLUB:// 加入俱乐部
            {
                JoinClubMsg joinClubMsg = (JoinClubMsg) msg;
//                this.clubService.joinClub(pl,joinClubMsg);
                playerService.joinClub(pl, joinClubMsg);
                break;
            }
            case MsgCmdConstant.GAME_CLUB_GET_SINGLE_CLUB_INFO: //获取单个俱乐部的信息
            {	
            	GetSingleClubInfoMsg  infoMsg = (GetSingleClubInfoMsg) msg;
            	this.playerService.getSingleClubInfo(pl,infoMsg,false,0);
            	break;
            }
            case MsgCmdConstant.GAME_CLUB_RECHARGE:// 俱乐部充值
            {
                ClubRechargeMsg clubRechargeMsg = (ClubRechargeMsg) msg;
                this.playerService.rechargeClub(pl,clubRechargeMsg);
                break;
            }
            case MsgCmdConstant.GAME_CLUB_CHARGE_LEVEL:// 俱乐部充值
            {
            	ClubChargeLevelMsg clubRechargeMsg = (ClubChargeLevelMsg) msg;
            	this.playerService.changeClubLevel(pl,clubRechargeMsg);
            	break;
            }
            case MsgCmdConstant.GAME_CLUB_CHONGZHI:// 俱乐部充值
            {
            	ClubChongZhiMsg clubRechargeMsg = (ClubChongZhiMsg) msg;
            	this.playerService.updateClubPlayerGold(pl,clubRechargeMsg);
            	break;
            }
            case MsgCmdConstant.GAME_CLUB_GET_RANK:
                GetQyqRankListMsg qyqRankMsg = (GetQyqRankListMsg) msg;
                this.playerService.getClubRankingList(qyqRankMsg, session);                       
                break;
              //绑定推荐人
            case MsgCmdConstant.USER_EXTEND_GOLD_MSG:{
            	dbserver_exchange_prize.push_msg(construct_dbmsg(msg, session, pl));
            	break;
            }
            case MsgCmdConstant.SEARCH_USER_INDEX:
                dbserver_reg_friend.push_msg(construct_dbmsg(msg, session, pl));
                break;
            default:
                logger.error("unkown msg，cmd=" + msg.msgCMD + "----" + Integer.toHexString(msg.msgCMD));
                break;
        }

        delta = ctt - DateService.getCurrentUtilDate().getTime();
        if (delta > 3000) {
            logger.error("gameMsgProcess error cmd=" + msg.msgCMD + " process time:" + delta);
        }

    }
	/*
	private void player_OPT_str(Player pl, PlayerOpertaionMsg msg) {
		if (pl == null || msg == null)
			return;

		switch (msg.opertaionID) {
		case GameConstant.OPT_EXCHANGE_GIFT_CODE:
			// 兑换礼品卡
			//ChangGift(pl, msg);
			break;
		case GameConstant.OPT_CHANGEPLAYERNAME:
			// 修改用户昵称
			ValidateMsgAck ackNick = new ValidateMsgAck(MsgCmdConstant.GAME_PLAYER_UPDATE_NICKNAME_ACK);
			msg.headIndex = pl.getHeadImg();
			msg.sex = pl.getSex();
			ChangePlayer(pl, msg, ackNick);
			break;
		case GameConstant.OPT_CHANGEPLAYERACCOUNT:
			ValidateMsgAck ackAccount = new ValidateMsgAck(MsgCmdConstant.GAME_PLAYER_UPDATE_ACCOUNT_ACK);
			playerService.updatePlayer(pl, msg, ackAccount);
			break;
		case GameConstant.OPT_CHANGEPLAYERHEADIMGSEX:
			// 修改头像和性别
			ValidateMsgAck ackHSex = new ValidateMsgAck(MsgCmdConstant.GAME_PLAYER_UPDATE_LOGO_ACK);
			msg.playerName = pl.getPlayerName();
			ChangePlayer(pl, msg, ackHSex);
			break;
		case GameConstant.OPT_UPDATE_PASSWORD:
			UpdatePlayerPassWord(pl, msg);
			break;
		case GameConstant.OPT_CHANGEPLAYERCANFRIEND_FLAG:
			playerService.updatePlayerCanFriend(pl, msg);
			break;
		case GameConstant.OPERATION_COMPLETE_ACCOUNT_AND_PASSWORD:
			//补全帐号和密码
			ValidateMsgAck ackAccountPasswrod = new ValidateMsgAck(MsgCmdConstant.GAME_PLAYER_UPDATE_ACCOUNT_ACK);
			playerService.updatePlayerAccountAndPassword(pl, msg, ackAccountPasswrod);
			break;
		case GameConstant.OPERATION_UPLOAD_CITY_NAME:
			//客户端上传位置信息
			playerService.updatePlayerCityName(pl.getPlayerID(), msg.opStr);
			break;
		default:
			break;
		}
	}
	*/
	/*
	private void friend_OPT_str(Player pl, FriendOprateMsg msg) {
		if (pl == null || msg == null)
			return;

		switch (msg.opertaionID) {
		case GameConstant.OPT_ADDFRIEND:
			// 添加好友
			playerService.AddPlayerFriend(pl, msg.opStr);
			break;
		case GameConstant.OPT_DELFRIEND:
			// 删除好友
			playerService.DelPlayerFriend(pl, msg.opStr);
			break;
		case GameConstant.OPT_SEARCHFRIENDBYINDEX:
			// 通过索引查询好友
			SeachFriend(0, pl, msg.opStr);
			break;
		case GameConstant.OPT_SEARCHFRIENDBYNAME:
			// 通过名称查询好友
			SeachFriend(1, pl, msg.opStr);
			break;
		case GameConstant.OPT_REFLESHFRIENDBYNAME:
			playerService.refleshFrend(pl);
			break;
		case GameConstant.OPERATON_AGREE_FRIEND_APPLY_RESULT:
			playerService.agreeBecomeFriendApply(pl, msg.opStr);
			break;
		case GameConstant.OPERATON_REJECT_FRIEND_APPLY_RESULT:
			playerService.rejectBecomeFriendApply(pl, msg.opStr);
			break;
		default:
			break;
		}
	}
	*/

   
    /**
     * 修改用户信息
     */
	/*
	private void ChangePlayer(Player pl, PlayerOpertaionMsg msg, ValidateMsgAck ack) {

		// 昵称，性别，头像
		if (msg.playerName.length() > 50)
			return;

		// 修改数据库
		playerService.updatePlayer(pl, msg, ack);
	}
	*/
    /**
     * 修改用户信息
     */
	/*
	private void UpdatePlayerPassWord(Player pl, PlayerOpertaionMsg msg) {

		if (msg.oldPassWord.length() > 50 || msg.newPassWord.length() > 50)
			return;

		// 修改数据库
		playerService.updatePlayerPassword(pl, msg.oldPassWord, msg.newPassWord);
	}
	*/


    /**
     * 搜索好友
     *
     * @param type
     * 0 索引查询,1 名称查询
     */
	/*
	private void SeachFriend(int type, Player player, String opStr) {
		if (opStr.length() > 50)
			return;

		List<FriendPlayer> findFriends;
		if (type == 0) {

			// if(!StringUtils.isInteger(opStr))
			// return;

			int index = Integer.parseInt(opStr);

			findFriends = playerService.GetPlayerFriendsByPlayerIndex(index);
		} else {
			findFriends = playerService.GetPlayerFriendsByPlayerName(opStr);
		}

		SearchFriendMsgAck ack = new SearchFriendMsgAck();
		if (findFriends != null) {
			ack.friends = findFriends;
			for(FriendPlayer friend:findFriends)
			if (playerService.containsPlayer(friend.getPlayerID())) {
				Player fl = playerService.getPlayerByPlayerIDFromCache(friend.getPlayerID());
				if(fl!=null&&fl.isOnline())
				{
					friend.isOnline = 1;
				}
			}
			ack.result = 1;
		} else {
			ack.result = 0;
		}

		SystemConfig.gameSocketServer.sendMsg(player.getSession(), ack);
	}
	*/

    /**
     * 处理发送消息
     */
    private void DealPostMessage(User pl, PostMessageMsg msg) {
        if (pl == null || msg == null)
            return;

        switch (msg.opertaionID) {
            case MessageConstant.MSG_POST_FRIEND:
                User player = playerService.getPlayerByPlayerIDFromCache(msg.receiveID);
                if (player != null) {
                    if (player.getSession() != null) {
                        PostMessageMsgAck pAck = new PostMessageMsgAck();
                        pAck.playerID = pl.getPlayerID();
                        pAck.infoType = msg.infoType;
                        pAck.opertaionID = MessageConstant.MSG_POST_FRIEND_ACK;
                        pAck.account = pl.getAccount();
                        pAck.iTimeLenght = msg.iTimeLenght;

                        pAck.infoSound = msg.infoSound;
                        //pAck.infoSound=msg.infoSound.clone();
                        //System.out.println("length="+pAck.infoSound.length);

                        pAck.infoText = msg.infoText;
                        pAck.nickname = pl.getPlayerName();
                        pAck.playerindex = pl.getPlayerIndex();
                        GameContext.gameSocket.send(player.getSession(), pAck);
                    }
                }
                break;
	            case MessageConstant.MSG_POST_FRIEND_ACK:
	            	 
	                if (pl.getSession() != null) {
	                	
	                	PostMessageMsgAck pAck = new PostMessageMsgAck();
	                    pAck.playerID = pl.getPlayerID();
	                    pAck.infoType = msg.infoType;
	                    pAck.opertaionID = MessageConstant.MSG_POST_FRIEND_ACK;
	                    pAck.account = pl.getAccount();
	                    pAck.iTimeLenght = msg.iTimeLenght;
	
	                    pAck.infoSound = msg.infoSound;
	//	                  logger.info("##############length="+msg.infoSound.length);
		                  logger.info("##############infoText="+msg.infoText);
	
	                    pAck.infoText = msg.infoText;
	                    pAck.nickname = pl.getPlayerName();
	                    pAck.playerindex = pl.getPlayerIndex();
	                	
                		com.chess.nndzz.table.GameRoom rm = GameContext.tableLogic_nndzz.getRoom(pl.getRoomID());
                		com.chess.nndzz.table.GameTable gt = rm.getTable(pl.getTableID(), false);
	                    gt.sendMsgToTableExceptMe(pAck, pl.getPlayerID());
	                }
	       	break;
        }
    }

    // 玩家链接校验成功
    @Override
    protected void player_link_validation_passed(IoSession session) {
        SocketAddress add = session.getRemoteAddress();
        // logger.info("player_link_validation_passed");
        this.playerService.scroll_msg_send(session);
    }

    // 玩家断线
    @Override
    protected void playerLinkBroken(LinkBrokenMsg msg, IoSession session) {
        User pl = (User) session.getAttribute(NetConstant.PLAYER_SESSION_KEY);
        if (pl != null && pl.getSession() != null && pl.getSession().equals(session)) {
            logger.info("玩家：" + pl.getPlayerIndex() + "，昵称："+pl.getPlayerName()+"，断线playerLinkBroken");
            // 设置不在牌桌上，避免其他玩家看着状态不对 cuiweiqing 2017-01-22
            pl.setOnTable(false);
            pl.setOnline(false);
            pl.setTwoVipOnline(false);
            pl.setOfflineTime(DateService.getCurrentUtilDate());
            playerService.playerOffline(pl.getPlayerID());
            loginService.playerLoginOut(pl);
        }
    }

    private void test(User pl, TestCard testCard) {


        if (pl != null && testCard != null && testCard.changeStr != null) {

            String[] CardSplits = testCard.changeStr.split("\\,");
            if (CardSplits.length < 2)
                return;

            List<Byte> cards = pl.getCardsInHand();
            if (cards == null) {
                return;
            }

            //拆分新手牌
            String[] newHandCards = CardSplits[1].split("\\.");

            if (CardSplits[0].equals("next")) {
                if (CardSplits.length == 2 /*&& pl.getAccount().equals("zheng20")*/) {
                    //下次摸这只牌
                    pl.setNextCard(Integer.valueOf(CardSplits[1], 16));
                }
            } else if (CardSplits[0].equals("*")) {
                if (newHandCards.length != cards.size()) {
                    //手牌数量不对
                    logger.info("新手牌的数量不对");
                    return;
                }

                //替换所有手牌
                pl.getCardsInHand().clear();

                for (int i = 0; i < newHandCards.length; i++) {
                    byte card = Byte.valueOf(newHandCards[i], 16);
                    if (card > 0 && card < 0x38) {
                        pl.addCardInHand(card);
                    }
                }

                //更新玩家手牌
                TestCardAck ack = new TestCardAck();

                ack.cardsInHand = pl.getCardsInHand();
                GameContext.gameSocket.send(pl.getSession(), ack);
            } else {
                String[] oldCards = CardSplits[0].split("\\.");

                if (oldCards.length <= 0 || oldCards.length > cards.size()) {
                    logger.info("要替换的手牌数量不对");
                    return;
                }


                for (int i = 0; i < oldCards.length; i++) {
                    byte card = Byte.valueOf(oldCards[i], 16);
                    byte newCard = Byte.valueOf(newHandCards[i], 16);

                    if (card <= 0 || card >= 0x38 || newCard <= 0 || newCard >= 0x38) {
                        continue;
                    }

                    for (int j = 0; j < cards.size(); j++) {
                        if (cards.get(j).byteValue() == card) {
                            //cards.remove(j);
                            pl.removeCardInHand(card);
                            pl.addCardInHand(newCard);
                        }
                    }
                }

                //更新玩家手牌
                TestCardAck ack = new TestCardAck();

                ack.cardsInHand = pl.getCardsInHand();
                GameContext.gameSocket.send(pl.getSession(), ack);
            }

        }
    }



}
