package com.chess.common.msg.processor.game;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.SpringService;
import com.chess.common.bean.SensitiveWords;
import com.chess.common.bean.User;
import com.chess.common.bean.UserFriend;
import com.chess.common.constant.GameConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.common.msg.struct.club.GetQyqRankListMsg;
import com.chess.common.msg.struct.club.GetQyqWinnerListMsg;
import com.chess.common.msg.struct.clubmember.ClubMemberOprateMsg;
import com.chess.common.msg.struct.entranc.EntranceUserMsgAck;
import com.chess.common.msg.struct.friend.FriendOprateMsg;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomAckMsg;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomMsg;
import com.chess.common.msg.struct.friend.SearchFriendMsgAck;
import com.chess.common.msg.struct.friend.UpdateFriendRemarkMsg;
import com.chess.common.msg.struct.login.LoginMsg;
import com.chess.common.msg.struct.login.ReconnectMsg;
import com.chess.common.msg.struct.login.RegisterPlayerMsg;
import com.chess.common.msg.struct.message.RequestCompletePhoneNumber;
import com.chess.common.msg.struct.other.AskRecoverGameMsg;
import com.chess.common.msg.struct.other.AskRecoverPlayerStateMsg;
import com.chess.common.msg.struct.other.CheckOfflineBackMsg;
import com.chess.common.msg.struct.other.EnterVipRoomMsg;
import com.chess.common.msg.struct.other.ExchangePrizeMsg;
import com.chess.common.msg.struct.other.GameReadyMsg;
import com.chess.common.msg.struct.other.HuDongBiaoQingMsg;
import com.chess.common.msg.struct.other.PlayerAskCloseVipRoomMsg;
import com.chess.common.msg.struct.other.PlayerGameOpertaionMsg;
import com.chess.common.msg.struct.other.PlayerGameOverMsg;
import com.chess.common.msg.struct.other.PlayerOpertaionMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomRecordMsg;
import com.chess.common.msg.struct.other.RequestStartGameMsg;
import com.chess.common.msg.struct.other.SearchUserByIndexMsg;
import com.chess.common.msg.struct.other.SearchUserByIndexMsgAck;
import com.chess.common.msg.struct.other.TalkingInGameMsg;
import com.chess.common.msg.struct.other.UserExtendMsg;
import com.chess.common.msg.struct.other.VipGameRecordMsg;
import com.chess.common.msg.struct.other.VipRoomRecordMsg;
import com.chess.common.msg.struct.other.WelFareMsg;
import com.chess.common.msg.struct.pay.GameBuyItemMsg;
import com.chess.common.msg.struct.playeropt.RefreshItemBaseMsgAck;
import com.chess.common.msg.struct.playeropt.ValidateMsgAck;
import com.chess.common.msg.struct.system.MobileCodeMsg;
import com.chess.common.service.ICacheUserService;
import com.chess.common.service.IEntranceUserService;
import com.chess.common.service.IExchangePrizeService;
import com.chess.common.service.IMallItemService;
import com.chess.nndzz.msg.struct.other.HuDongBiaoQingMsgNNDZZ;

public class GameServerAssistMsgProcessor extends Thread {
    private static Logger logger = LoggerFactory.getLogger(GameServerAssistMsgProcessor.class);

    //private Lock msg_lock = new ReentrantLock();
    //private List<DBServerMessage> msg_queue = new ArrayList<DBServerMessage>();
    private ConcurrentLinkedQueue<GameServerMessage> msg_queue = new ConcurrentLinkedQueue<GameServerMessage>();
    
    private IEntranceUserService entranceService = (IEntranceUserService) SpringService
	.getBean("entranceUserService");
    private boolean keep_running = true;

    private ICacheUserService player_service = null;
    private IMallItemService mallItemService = null;
    private IExchangePrizeService exchange_service = null;

    private int operation_id = 0;

    public GameServerAssistMsgProcessor(ICacheUserService playerService, IMallItemService mallItemService ,IExchangePrizeService exchangeService) {
        player_service = playerService;
        this.mallItemService = mallItemService;
        exchange_service  = exchangeService;
    }

    public void push_msg(GameServerMessage dbmsg) {
        msg_queue.add(dbmsg);
        /*
        try {
            msg_lock.lock();
            msg_queue.add(dbmsg);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            msg_lock.unlock();
        }
        */
    }

    private GameServerMessage pop_msg() {
        return msg_queue.poll();
        /*
        if (msg_queue.size() <= 0)
            return null;

        DBServerMessage dbmsg = null;
        try {
            msg_lock.lock();
            dbmsg = msg_queue.remove(0);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            msg_lock.unlock();
        }
        return dbmsg;
        */
    }

    //生成请求号


    /**
     * 修改用户信息
     */
    private void ChangePlayer(User pl, PlayerOpertaionMsg msg, ValidateMsgAck ack) {

        // 昵称，性别，头�?
        if (msg.playerName.length() > 50)
            return;
        String word = msg.playerName; 
        String wordTrim = word.replaceAll(" ", "");
        SensitiveWords words = findSensitiveWords(wordTrim);
        if (words != null)
        {
        	 //昵称不合法；
			  logger.error("玩家：" + pl.getPlayerIndex()
						+ "昵称包含非法内容，修改失败");

				ack.result = 5;
				ack.optStr = msg.playerName;
				GameContext.gameSocket.send(pl.getSession(), ack);
        }else{
        	// 修改数据�?
        	player_service.updatePlayer(pl, msg, ack);
        	
        }
        	
    }
    /**
     * 修改用户信息
     */
    private void UpdatePlayerPassWord(User pl, PlayerOpertaionMsg msg) {

        if (msg.oldPassWord.length() > 50 || msg.newPassWord.length() > 50)
            return;

        // 修改数据�?
        player_service.updatePlayerPassword(pl, msg.oldPassWord, msg.newPassWord);
    }

    private void player_OPT_str(User pl, PlayerOpertaionMsg msg) {
        if (pl == null || msg == null)
            return;

        switch (msg.opertaionID) {
            case GameConstant.OPT_CHANGEPLAYERNAME:
                // 修改用户昵称
                ValidateMsgAck ackNick = new ValidateMsgAck(MsgCmdConstant.GAME_USER_UPDATE_NICKNAME_ACK);
                msg.headIndex = pl.getHeadImg();
                msg.sex = pl.getSex();
                ChangePlayer(pl, msg, ackNick);
                break;
            case GameConstant.OPT_CHANGEPLAYERACCOUNT:
                ValidateMsgAck ackAccount = new ValidateMsgAck(MsgCmdConstant.GAME_USER_UPDATE_ACCOUNT_ACK);
                player_service.updatePlayer(pl, msg, ackAccount);
                break;
            case GameConstant.OPT_CHANGEPLAYERHEADIMGSEX:
                // 修改头像和性别
                ValidateMsgAck ackHSex = new ValidateMsgAck(MsgCmdConstant.GAME_USER_UPDATE_LOGO_ACK);
                msg.playerName = pl.getPlayerName();
                ChangePlayer(pl, msg, ackHSex);
                break;
            case GameConstant.OPT_UPDATE_PASSWORD:
                UpdatePlayerPassWord(pl, msg);
                break;
            case GameConstant.OPT_CHANGEPLAYERCANFRIEND_FLAG:
                player_service.updatePlayerCanFriend(pl, msg);
                break;
            case GameConstant.OPERATION_COMPLETE_ACCOUNT_AND_PASSWORD:
                //补全帐号和密�?
                ValidateMsgAck ackAccountPasswrod = new ValidateMsgAck(MsgCmdConstant.GAME_USER_UPDATE_ACCOUNT_ACK);
                player_service.updatePlayerAccountAndPassword(pl, msg, ackAccountPasswrod);
                break;
            case GameConstant.OPERATION_UPLOAD_CITY_NAME:
                //客户端上传位置信�?
                player_service.updatePlayerCityName(pl.getPlayerID(), msg.opStr);
                break;

            default:
                break;
        }
    }


    private void SeachFriend(int type, User player, String opStr) {
        if (opStr.length() > 50)
            return;

        List<UserFriend> findFriends;
        if (type == 0) {

            // if(!StringUtils.isInteger(opStr))
            // return;

            int index = Integer.parseInt(opStr);

            findFriends = player_service.GetPlayerFriendsByPlayerIndex(index);
        } else {
            findFriends = player_service.GetPlayerFriendsByPlayerName(opStr);
        }

        SearchFriendMsgAck ack = new SearchFriendMsgAck();
        if (findFriends != null) {
            ack.friends = findFriends;
            for(UserFriend friend:findFriends)
                if (player_service.containsPlayer(friend.getPlayerID())) {
                    User fl = player_service.getPlayerByPlayerIDFromCache(friend.getPlayerID());
                    if(fl!=null&&fl.isOnline())
                    {
                        friend.isOnline = 1;
                    }
                }
            ack.result = 1;
        } else {
            ack.result = 0;
        }

        GameContext.gameSocket.send(player.getSession(), ack);
    }

    private void friend_OPT_str(User pl, FriendOprateMsg msg) {
        if (pl == null || msg == null)
            return;

        switch (msg.opertaionID) {
            case GameConstant.OPT_ADDFRIEND:
                // 添加好友
                player_service.AddPlayerFriend(pl, msg.opStr);
                break;
            case GameConstant.OPT_DELFRIEND:
                // 删除好友
                player_service.DelPlayerFriend(pl, msg.opStr);
                break;
            case GameConstant.OPT_SEARCHFRIENDBYINDEX:
                // 通过索引查询好友
                SeachFriend(0, pl, msg.opStr);
                break;
            case GameConstant.OPT_SEARCHFRIENDBYNAME:
                // 通过名称查询好友
                SeachFriend(1, pl, msg.opStr);
                break;
            case GameConstant.OPT_REFLESHFRIENDBYNAME:
                player_service.refleshFrend(pl);
                break;
            case GameConstant.OPERATON_AGREE_FRIEND_APPLY_RESULT:
                player_service.agreeBecomeFriendApply(pl, msg.opStr);
                break;
            case GameConstant.OPERATON_REJECT_FRIEND_APPLY_RESULT:
                player_service.rejectBecomeFriendApply(pl, msg.opStr);
                break;
            default:
                break;
        }
    }
    
    private void clubMember_OPT_str(User pl, ClubMemberOprateMsg msg) {
        if (pl == null || msg == null)
            return;
        switch (msg.opertaionID) {
            case GameConstant.CLUB_MEMGER_OPT_APPLY:{
                 // 申请加入俱乐部
                 player_service.applyJoinClub(pl, msg.clubCode);
                 break;
            }
            case GameConstant.CLUB_MEMGER_OPT_ADD:{
                // 添加好友
                player_service.AddClubMember(pl, msg.memberId, msg.clubCode);
                break;
            }
            case GameConstant.CLUB_MEMBER_OPT_IGNORE:{
                // 拒绝添加
                player_service.IgnoreClubMember(pl, msg.memberId, msg.clubCode);
                break;
            }
            case GameConstant.CLUB_MEMGER_OPT_DELETE:{
                // 删除成员
                player_service.DelClubMember(pl, msg.memberId, msg.clubCode);
                break;
            }
            case GameConstant.CLUB_MEMGER_OPT_UPDATE_STATE:{
                // 同意添加
                player_service.UpdateClubMemberApplyState(pl, msg.memberId, msg.clubCode,msg.applyState);
                break;
            }
            case GameConstant.CLUB_MEMGER_OPT_UPDATE_REMARK:{
                // 更新备注
                player_service.UpdateClubMemberRemark(pl, msg.memberId, msg.clubCode,msg.remark);
                break;
            }
            case GameConstant.CLUB_MEMGER_OPT_GET_LIST:{
                // 成员列表
                player_service.GetClubMemberList(pl, msg.clubCode,msg.applyState);
                break;
            }
            case GameConstant.CLUB_MEMBER_OPT_QUIT:{
                // 退出俱乐部
                player_service.QuitClubMember(pl, msg.memberId, msg.clubCode);
                break;
            }
            default:
                break;
        }
    }


    private void msg_processor() {
        for (int i = 0; i < 500; i++) {
            try {
                GameServerMessage dbmsg = pop_msg();
                if(dbmsg == null) {
                    break;
                }
                //if (dbmsg != null) {
                    switch (dbmsg.msg.msgCMD) {
                        case MsgCmdConstant.GAME_LOGIN:
                            LoginMsg login = (LoginMsg) dbmsg.msg;
                            player_service.login(login, dbmsg.session);
                            return;
                        case MsgCmdConstant.GAME_RECONNECT_IN:
                            ReconnectMsg rmsg = (ReconnectMsg) dbmsg.msg;
                            logger.debug("断线重链");//System.out.println("断线重链");
                            player_service.reconnect(rmsg, dbmsg.session);
                            break;
                        // 注册新用�?
                        case MsgCmdConstant.GAME_REGISTER_PLAYER:
                            RegisterPlayerMsg rpm = (RegisterPlayerMsg) dbmsg.msg;
                            player_service.regNewPlayer(rpm, dbmsg.session);
                            break;
                            
                        case MsgCmdConstant.GAME_START_GAME_REQUEST:
                            RequestStartGameMsg rsmg = (RequestStartGameMsg) dbmsg.msg;
                            player_service.enter_room(rsmg, dbmsg.session);
                            break;
                            //NNQZ
                        case MsgCmdConstant.GAME_ASK_GAME_RECOVER:{
                        	AskRecoverGameMsg armsg = (AskRecoverGameMsg) dbmsg.msg;
                        	player_service.ask_recover(armsg, dbmsg.session);
                        	break;
                        }
                        case MsgCmdConstant.GAME_ASK_RECOVER_PLAYER_STATE:{
                        	AskRecoverPlayerStateMsg arMsg = (AskRecoverPlayerStateMsg) dbmsg.msg;
                        	player_service.ask_recover_state(arMsg, dbmsg.session);
                        	break;
                        }
                        case MsgCmdConstant.GAME_CHECK_OFFLINE_BACK:
                        	CheckOfflineBackMsg cmsg = (CheckOfflineBackMsg) dbmsg.msg;
                        	player_service.check_reroom(cmsg, dbmsg.session);
                        	break;
                        case MsgCmdConstant.GET_VIP_ROOM_RECORD:
                            VipRoomRecordMsg vrrm = (VipRoomRecordMsg) dbmsg.msg;
                            this.player_service.getVipRoomRecord(vrrm, dbmsg.session);
                            break;
	                    case MsgCmdConstant.GET_PROXY_VIP_ROOM_RECORD:
	                      	//代开优化 cc modify 2017-9-26
	                        ProxyVipRoomRecordMsg pvrrm = (ProxyVipRoomRecordMsg) dbmsg.msg;
	                        this.player_service.getProxyVipRoomRecord(pvrrm, dbmsg.session);
	                        break;
                        case MsgCmdConstant.GET_VIP_GAME_RECORD:
                            VipGameRecordMsg vgrm = (VipGameRecordMsg) dbmsg.msg;
                            this.player_service.getVipGameRecord(vgrm, dbmsg.session);
                            break;
                        case MsgCmdConstant.GAME_ENTER_VIP_ROOM:// 进入vip房间
                            EnterVipRoomMsg emsg = (EnterVipRoomMsg) dbmsg.msg;
                            this.player_service.enterVipRoom(emsg, dbmsg.pl);
                            break;
                        case MsgCmdConstant.GAME_INVITE_FRIEND_ENTER_VIP_ROOM:
                            InviteFriendEnterVipRoomMsg ivf = (InviteFriendEnterVipRoomMsg) dbmsg.msg;
                            this.player_service.inviteFriendToVipRoom(ivf, dbmsg.pl);
                            break;
                        case MsgCmdConstant.GAME_INVITE_FRIEND_ENTER_VIP_ROOM_ACK:
                        	InviteFriendEnterVipRoomAckMsg msg = (InviteFriendEnterVipRoomAckMsg) dbmsg.msg;
                        	this.player_service.beInvitedAck(msg, dbmsg.pl);
                        	break;
                        case MsgCmdConstant.REQUEST_COMPLETE_PHONE_NUMBER:
                            //绑定手机号码
                            RequestCompletePhoneNumber completeMsg = (RequestCompletePhoneNumber)dbmsg.msg;
                            player_service.completeMoblieNumber(dbmsg.pl, completeMsg);
                            break;
                        //玩家申请解散房间
                        case MsgCmdConstant.GAME_CLOSE_ROOM:
                            PlayerAskCloseVipRoomMsg closeVipRoomMsg = (PlayerAskCloseVipRoomMsg)dbmsg.msg;
                            player_service.userCloseVipRoom(closeVipRoomMsg, dbmsg.pl);
                            break;
                        //开局准备
                        case MsgCmdConstant.GAME_READY:
                            GameReadyMsg gameRedayMsg = (GameReadyMsg)dbmsg.msg;
                            player_service.gameReady(gameRedayMsg, dbmsg.session);
                            break;
                          //nndzz换座消息
                        case MsgCmdConstant.GAME_GAME_CHANGE_SITE_NNDZZ:
                        	com.chess.nndzz.msg.struct.other.GameSiteDownMsg gameSiteDownMsgNNDZZ = (com.chess.nndzz.msg.struct.other.GameSiteDownMsg)dbmsg.msg;
                            player_service.changeSiteNNDZZ(gameSiteDownMsgNNDZZ, dbmsg.session);
                            break;//nndzz
                            
                        case MsgCmdConstant.TALKING_IN_GAME:
                            TalkingInGameMsg talkMsg = (TalkingInGameMsg)dbmsg.msg;
                            player_service.playerTalkingInGame(dbmsg.pl, talkMsg);
                            break;
                        case MsgCmdConstant.HU_DONG_BIAO_QING:
                        	HuDongBiaoQingMsg hdbqMsg = (HuDongBiaoQingMsg)dbmsg.msg;
                        	player_service.playerSendHuDongBiaoQing(dbmsg.pl, hdbqMsg);
                        	break;
                        case MsgCmdConstant.HU_DONG_BIAO_QING_NNDZZ:{
                        	HuDongBiaoQingMsgNNDZZ nndzzhdbqMsg = (HuDongBiaoQingMsgNNDZZ)dbmsg.msg;
                        	player_service.playerSendNNDZZHuDongBiaoQing(dbmsg.pl, nndzzhdbqMsg);
                        	break;
                        }
                        case MsgCmdConstant.GAME_BUY_ITEM:
                            GameBuyItemMsg gbim = (GameBuyItemMsg) dbmsg.msg;
                            this.player_service.generateOrder(gbim, dbmsg.pl);
                            break;
                        case MsgCmdConstant.GAME_REFRESH_ITEM_BASE:
                            RefreshItemBaseMsgAck ribma = new RefreshItemBaseMsgAck();
                            ribma.baseItemList = this.player_service.getItemBaseByPhone(dbmsg.pl, mallItemService.refreshItemBase());
                            GameContext.gameSocket.send(dbmsg.pl.getSession(), ribma);
                            break;
                        case MsgCmdConstant.UPDATE_FRIEND_REMARK:
                            UpdateFriendRemarkMsg markMsg = (UpdateFriendRemarkMsg)dbmsg.msg;
                            player_service.updatePlayerRemark(dbmsg.pl, markMsg);
                            break;
                        case MsgCmdConstant.MOBILE_CODE:
                            //短信验证�?
                            MobileCodeMsg mcm = (MobileCodeMsg)dbmsg.msg;
                            player_service.sendMobileCodeRequest(mcm, dbmsg.session);
                            break;
                        case MsgCmdConstant.GAME_GAME_OPERTAION:
                            PlayerGameOpertaionMsg pgom = (PlayerGameOpertaionMsg) dbmsg.msg;
                            player_service.playerGameOperation(pgom, dbmsg.session);
                            break;
                        case MsgCmdConstant.GAME_GAME_OVER:
                            PlayerGameOverMsg pgovm = (PlayerGameOverMsg) dbmsg.msg;
                            player_service.playerGameOver(pgovm, dbmsg.session);
                            break;
                        case MsgCmdConstant.GAME_SEND_PLAYER_OPERATIOIN_STRING: {
                            PlayerOpertaionMsg opom = (PlayerOpertaionMsg) dbmsg.msg;
                            player_OPT_str(dbmsg.pl, opom);
                            break;
                        }
                        case MsgCmdConstant.FRIEND_PROPERTY: {
                            FriendOprateMsg opom = (FriendOprateMsg) dbmsg.msg;
                            friend_OPT_str(dbmsg.pl, opom);
                            break;
                        }
                        case MsgCmdConstant.CLUB_MEMBER: {
                            ClubMemberOprateMsg opom = (ClubMemberOprateMsg) dbmsg.msg;
                            clubMember_OPT_str(dbmsg.pl, opom);
                            break;
                        }
                        case MsgCmdConstant.ENTRANCE_SERVER_CREATE_NEW_PLAYER_ACK:
                            EntranceUserMsgAck ack = (EntranceUserMsgAck)dbmsg.msg;
                            player_service.createPlayerAck(ack);
                            break;
                        //完善资料
                        case MsgCmdConstant.REQUEST_BIND_PHONE:
//                            PlayerBindPhoneNumber msg = (PlayerBindPhoneNumber)dbmsg.msg;
//                            player_service.bind_phone_number(msg, dbmsg.pl);
                            //dbserver_helper.push_msg(construct_dbmsg(msg, session, pl));
                            break;
                          //绑定推荐人 
                        case MsgCmdConstant.USER_EXTEND_GOLD_MSG:{
                        	UserExtendMsg userExtendMsg = (UserExtendMsg) dbmsg.msg;
                        	player_service.setExtendPlayer(dbmsg.pl,userExtendMsg);
                        	break;
                        }
                        case MsgCmdConstant.SEARCH_USER_INDEX:
                            SearchUserByIndexMsg searchMsg = (SearchUserByIndexMsg) dbmsg.msg;
                            player_service.searchUserByIndex(dbmsg.pl, searchMsg);
                            break;
                          //兑换奖励
                        case MsgCmdConstant.EXCHANGE_PIRZE:
                        	ExchangePrizeMsg exchange = (ExchangePrizeMsg)dbmsg.msg;
                        	exchange_service.useExchangeNum(dbmsg.pl,exchange);
                            break;
                        	//大转盘
                        case MsgCmdConstant.WELFARE_MSG:
                        	WelFareMsg wmgFareMsg = (WelFareMsg) dbmsg.msg;
                        	player_service.welFare(dbmsg.pl, wmgFareMsg);
                        	break;
                        case MsgCmdConstant.GAME_CLUB_GET_RANK:
                            GetQyqRankListMsg qyqRankMsg = (GetQyqRankListMsg) dbmsg.msg;
                            this.player_service.getClubRankingList(qyqRankMsg, dbmsg.session);                       
                            break;
                        case MsgCmdConstant.GAME_CLUB_GET_WINNER:
                            GetQyqWinnerListMsg qyqWinnerMsg = (GetQyqWinnerListMsg) dbmsg.msg;
                            if(qyqWinnerMsg.opType==1){//删除
                            	String recordIds=qyqWinnerMsg.roomIds;
                            	List<String> rIds=new ArrayList<String>();
                            	String[] rIdArr=recordIds.split(";");
                            	for(int k=0;k<rIdArr.length;k++){
                            		rIds.add(rIdArr[k]);
                            	}
                            	this.player_service.updateRecordReadState(rIds, qyqWinnerMsg.gameID,dbmsg.session);
                            }else {//查询
                            	this.player_service.getQyqRoomRecord(qyqWinnerMsg, dbmsg.session);
							}
                            
                            break;

                        //add end
                    }
                    
                //}
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                continue;
            }

        }
    }
    
    private SensitiveWords findSensitiveWords(String word) {
		
    	SensitiveWords words = entranceService.getSensitiveWords(word);
		return words;
	}

    //线程函数，主循环
    @Override
    public void run() {
        while (keep_running) {
            try {

                msg_processor();
                Thread.sleep(2);
            } catch (Exception e) {
                logger.error(this.getName() + " table manager loop error", e);
                logger.error(e.getMessage(), e);
                continue;
            }
        }
    }

}
