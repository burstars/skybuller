package com.chess.nndzz.msg.command;


public class MsgCmdConstant {

	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// 系统维护类消息
	/** 心跳消息发送** */

	public static final int HEART_BEATING = 0xa11001;
	public static final int HEART_BEATING_ACK = 0xa11002;

	/** *链接之后发送确认链接消息** */
	public static final int LINK_VALIDATION = 0xa11003;
	public static final int LINK_VALIDATION_ACK = 0xa11004;
	/** 玩家断开，这个消息可以由大厅发给dbgate，或者gs发给dbgate* */
	public static final int PLAYER_OFFLINE = 0xa11005;
	/** 服务器状态健康状态更新* */
	public static final int SERVER_STATUS_UPDATE = 0xa11006;
	/** 底层通知应用曾，网络断开* */
	public static final int LINK_BROKEN = 0xa11007;
	/** 客户端重链接到大厅* */
	public static final int LOBBY_CLIENT_LINK_RECONNECT = 0xa11008;

	public static final int GROUP_SYSTEM_MAINTAIN_START = 0xa11000;
	public static final int GROUP_SYSTEM_MAINTAIN_END = 0xb21000;
	// ////////////////////////////////////////////////////////////////

	/** 游戏补丁文件服务器** */
	public static final int GROUP_PATCH_SERVER_START = 0xb21000;

	/** 客户端请求补丁文件列表* */
	public static final int GET_PATCH_FILE_LIST = 0xb21001;
	public static final int GET_PATCH_FILE_LIST_ACK = 0xb21002;
	/** 客户端请求补丁文件* */
	public static final int GET_PATCH_FILE = 0xb21003;
	public static final int GET_PATCH_FILE_ACK = 0xb21004;

	/** 获取版本更新信息 */
	public static final int GET_PATCH_VESION = 0xb21006;
	public static final int GET_PATCH_VESION_ACK = 0xb21007;

	// ////////////////////////////////////////////////////////////
	public static final int GROUP_PATCH_SERVER_END = 0xc31000;
	// 游戏消息
	/** 心跳消息发送** */
	public static final int GROUP_GAME_START = 0xc31000;

	public static final int GAME_LOGIN = 0xc31001;

	public static final int GAME_UPDATE_PLAYER_PROPERTY = 0xc31002;

	//public static final int GAME_START_GAME_REQUEST = 0xc61003;
	//public static final int GAME_START_GAME_REQUEST_ACK = 0xc61004;

	/** 客户端通知服务端抽奖消息 */
	public static final int GAME_LUCKYDRAW = 0xc31005;
	/** 服务端返回客户端的抽奖消息 */
	public static final int GAME_LUCKYDRAW_ACK = 0xc31006;

	// 客户端通知游戏服务器，玩家得分进展
	public static final int GAME_GAME_UPDATE = 0xc31007;

	/** 客户端通知游戏服务器，玩家的某些行为** */
	public static final int GAME_GAME_OPERTAION = 0xc31008;
	/** 客户端通知游戏服务器，玩家的某些行为** */
	public static final int GAME_GAME_OPT_ACK = 0xc31009;

	/** 游戏结束* */
	public static final int GAME_GAME_OVER = 0xc3100c;
	//public static final int GAME_GAME_OVER_ACK = 0xc3100d;
	public static final int GAME_GAME_OVER_ACK = 0xc6100d;
	/** 客户端通知服务端获取礼物列表消息 */
	public static final int GAME_GET_PRIZE_LIST = 0xc31011;
	/** 服务端返回客户端返回礼物列表消息 */
	public static final int GAME_GET_PRIZE_LIST_ACK = 0xc31012;

	/** 客户端通知服务端返回排行榜的消息 */
	public static final int GAME_GET_RANKING_LIST = 0xc31013;
	/** 服务端返回给客户端的排行榜消息 */
	public static final int GAME_GET_RANKING_LIST_ACK = 0xc31014;

	/** 服务端通知客户端滚动条消息 */
	public static final int GAME_SEND_SCROLL_MES = 0xc31015;

	/** 服务端返回给客户端，通知客户端更新一个属性 */
	public static final int GAME_UPDATE_PLAYER_ONE_PROPERTY = 0xc31016;

	/** 服务器通知客户端更新整个道具列表** */
	public static final int GAME_UPDATE_PLAYER_ITEM_LIST = 0xc31017;

	/** 购买大礼包返回** */
	public static final int GAME_BUY_BIG_GIFT_PACK_ACK = 0xc31018;

	/** 客户端通知服务端返回动物彩票个人数据的消息 */
	public static final int GAME_GET_ANIMALINFOR = 0xc31019;
	/** 服务端返回给客户端的动物彩票个人数据消息 */
	public static final int GAME_GET_ANIMALINFOR_ACK = 0xc3101a;
	/** 客户端通知服务端客户购买了彩票并要求返回动物彩票个人数据的消息 */
	public static final int GAME_BUY_TICKET = 0xc3101b;
	/** 客户端通知服务端客户领取了奖金并要求返回动物彩票个人数据的消息 */
	public static final int GAME_GET_ANIMALAWARD = 0xc3101c;

	/** 客户端请求获得玩家任务列表 */
	public static final int CLIENT_GET_PLAYER_TASK_MSG = 0xc3101d;
	/** 客户端请求获得玩家任务列表 返回 */
	public static final int CLIENT_GET_PLAYER_TASK_MSG_ACK = 0xc3101f;
	/** *玩家发送一个操作给服务器，带一个字符串** */
	public static final int GAME_SEND_PLAYER_OPERATIOIN_STRING = 0xc31020;

	// 玩家断线重链接
	public static final int GAME_RECONNECT_IN = 0xc31021;

	// 换名字返回
	public static final int GAME_CHANGENAME_ACK = 0xc31022;

	public static final int GAME_LOGIN_ACK = 0xc31023;

	/** 查询玩家好友 */
	public static final int GAME_GET_PLAYER_FINDFRIEND_ACK = 0xc31024;

	// 换性别返回
	public static final int GAME_CHANGESEX_ACK = 0xc31025;

	// 注册时获取手机注册验证码
	public static final int MOBILE_CODE = 0xc31053;
	public static final int MOBILE_CODE_ACK = 0xc31054;

	// 注册新用户
	public static final int GAME_REGISTER_PLAYER = 0xc31055;

	// 牌局开始
	public static final int GAME_START_GAME = 0xc61060;

	// 提醒玩家进行操作
	public static final int GAME_USER_OPERATION_NOTIFY = 0xc31061;
	
	// 提醒玩家进行操作
	public static final int GAME_USER_OPERATION_NOTIFY_DDZ = 0xc61061;

	// 客户端发给服务器的玩家在牌桌上的操作行为
	//public static final int GAME_USER_TABLE_OPERATION = 0xc61062;

	// 客户端请求获取玩家所有VIP房间记录
	public static final int GET_VIP_ROOM_RECORD = 0xc31063;
	public static final int GET_VIP_ROOM_RECORD_ACK = 0xc31064;

	// 客户端请求获取玩家指定VIP房间记录中的所有游戏记录
	public static final int GET_VIP_GAME_RECORD = 0xc31065;
	public static final int GET_VIP_GAME_RECORD_ACK = 0xc31066;

	// 信息模块
	public static final int POST_USER_INFO = 0xc31067;
	public static final int POST_USER_INFO_ACK = 0xc31068;

	// 验证消息
	public static final int GAME_VALIDATENAME_ACK = 0xc31070;

	// 购买道具生成支付宝订单号
	public static final int GAME_BUY_ITEM = 0xc31071;
	public static final int GAME_BUY_ITEM_ACK = 0xc31072;

	// 玩家请求兑换奖品
	public static final int EXCHANGE_PIRZE = 0xc31087;
	public static final int EXCHANGE_PIRZE_ACK = 0xc31086;

	// 好友相关操作
	public static final int FRIEND_PROPERTY = 0xc31080;
	public static final int FRIEND_PROPERTY_ACK = 0xc31081;

	// 添加好友返回被添加人信息
	public static final int GAME_GET_FRIEND__ACK = 0xc31082;
	// 添加好友返回添加人信息
	public static final int GAME_ADD_FRIEND__ACK = 0xc31083;

	// 通知好友在线
	public static final int FRIEND_ONLINE_ACK = 0xc31084;

	// 通知好友更改信息
	public static final int FRIEND_REFLESHNFO_ACK = 0xc31085;
	
	// 通知玩家续卡
	//public static final int EXTEND_CARD_NOTIFY = 0xc31088;
	public static final int EXTEND_CARD_NOTIFY = 0xc61088;
	// 通知玩家续卡操作结果
	//public static final int EXTEND_CARD_RESULT = 0xc31089;
	public static final int EXTEND_CARD_RESULT = 0xc61089;

	// 修改用户信息返回（result 1,成功；2，昵称已存在；）
	public static final int GAME_USER_UPDATE_NICKNAME_ACK = 0xc31103;
	// 修改用户信息返回（result 1,成功；2，旧密码不正确；）
	public static final int GAME_USER_UPDATE_PASSWORD_ACK = 0xc31104;
	// 修改头像返回
	public static final int GAME_USER_UPDATE_LOGO_ACK = 0xc31105;

	// 修改是否可以加为好友设置返回
	public static final int GAME_USER_UPDATE_CANFRIEND_ACK = 0xc31106;

	// 修改帐号返回（0为失败，1为成功，2为存在,3帐号不在3-12字符）
	public static final int GAME_USER_UPDATE_ACCOUNT_ACK = 0xc31107;

	// 客户端请求刷新商城道具
	public static final int GAME_REFRESH_ITEM_BASE = 0xc31108;
	public static final int GAME_REFRESH_ITEM_BASE_ACK = 0xc31109;
	// 支付完成刷新道具列表
	public static final int GAME_PAY_ITEM_COMPLETE = 0xc31110;

	// 内支付完成
	public static final int GAME_PAY_ITEM_IPA_COMPLETE = 0xc31111;

	public static final int GAME_USER_HU_TYPE = 0xc31130;
	public static final int GAME_USER_HU_TYPE_ACK = 0xc31131;

	public static final int GAME_USER_UPDATE = 0xc31150;
	public static final int GAME_USER_UPDATE_ACK = 0xc31151;

	// vip房间结束
	public static final int GAME_VIP_ROOM_CLOSE = 0xc31200;

	// 邀请好友进入VIP房间
	public static final int GAME_INVITE_FRIEND_ENTER_VIP_ROOM = 0xc31203;
	// 被邀请进入VIP房间
	public static final int GAME_BE_INVITED_ENTER_VIP_ROOM = 0xc31204;

	// 帐号在其他地方登录
	public static final int GAME_OTHERLOGIN_ACK = 0xc31205;

	// 测试牌型
	public static final int TEST_CARD = 0xc31210;
	public static final int TEST_CARD_ACK = 0xc31211;

	// 修改好友备注名称
	public static final int UPDATE_FRIEND_REMARK = 0xc31220;
	public static final int UPDATE_FRIEND_REMARK_ACK = 0xc31221;

	/** 获取短信验证码 */
	public static final int REQUEST_CLIENT_COMPLETE_PHONE_NUMBER_ACK = 0xc31222;

	public static final int REQUEST_BIND_PHONE = 0xc31326;
	public static final int REQUEST_BIND_PHONE_ACK = 0xc31327;

	/** 请求绑定手机号码 */
	public static final int REQUEST_COMPLETE_PHONE_NUMBER = 0xc31223;
	public static final int REQUEST_COMPLETE_PHONE_NUMBER_ACK = 0xc31224;

	public static final int UPDATE_GAMETABLE_GOLD = 0xc310225;

	public static final int ZHIDUI_SUPPORT = 0xc310226;

	public static final int SHOW_NOTICE_ONCREATEVIP = 0xc310227;
	public static final int SHOW_NOTICE_ONLOGIN = 0xc310228;

	public static final int TWO_PEOPLE_SUPPORT = 0xc310229;

	// 游戏中聊天
	public static final int TALKING_IN_GAME = 0xc31300;

	public static final int GROUP_GAME_END = 0xd41000;

	// 修改全局参数到客户端
	public static final int GLOBAL_CONFIG_CLIENT = 0xd41001;

	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////
	// 入口服务器相关消息
	public static final int ENTRANCE_SERVER_START = 0xd41000;

	public static final int ENTRANCE_SERVER_REG_GS = 0xd41001;
	public static final int ENTRANCE_SERVER_REG_GS_ACK = 0xd41002;
	public static final int ENTRANCE_SERVER_GS_UPDATE = 0xd41003;

	// 客户端向入口服务器请求逻辑服地址
	public static final int ENTRANCE_SERVER_GET_GAME_SERVER_INFO = 0xd41100;
	public static final int ENTRANCE_SERVER_GET_GAME_SERVER_INFO_ACK = 0xd41101;

	// 游戏服向入口服请求创建新用户
	public static final int ENTRANCE_SERVER_CREATE_NEW_PLAYER = 0xd41200;
	public static final int ENTRANCE_SERVER_CREATE_NEW_PLAYER_ACK = 0xd41201;
	//
	// 游戏服向入口服请求查找用户
	public static final int ENTRANCE_SERVER_FIND_PLAYER = 0xd41202;
	public static final int ENTRANCE_SERVER_FIND_PLAYER_ACK = 0xd41203;

	// 游戏入口服请求查找昵称
	public static final int ENTRANCE_SERVER_FIND_NICKNAME = 0xd41204;
	public static final int ENTRANCE_SERVER_FIND_NICKNAME_ACK = 0xd41205;

	// 游戏服通知入口服创建了新vip桌子
	public static final int ENTRANCE_SERVER_CREATE_VIP_TABLE = 0xd41206;
	public static final int ENTRANCE_SERVER_CREATE_VIP_TABLE_ACK = 0xd41207;

	// 游戏服通知入口服销毁vip桌子
	public static final int ENTRANCE_SERVER_DESTROY_VIP_TABLE = 0xd41208;
	public static final int ENTRANCE_SERVER_DESTROY_VIP_TABLE_ACK = 0xd41209;

	// 游戏服向入口服请求查找vip桌子
	public static final int ENTRANCE_SERVER_FIND_VIP_TABLE = 0xd4120A;
	public static final int ENTRANCE_SERVER_FIND_VIP_TABLE_ACK = 0xd4120B;

	public static final int ENTRANCE_SERVER_END = 0xd50000;

	// 玩家申请解散房间
	public static final int GAME_CLOSE_ROOM = 0xc31225;
	public static final int GAME_CLOSE_ROOM_ACK = 0xc31226;
	
	// 断线重连检测
	public static final int GAME_CHECK_OFFLINE_BACK = 0xc33333;
	public static final int GAME_CHECK_OFFLINE_BACK_ACK = 0xc333333;
	
	/** 服务器通知客户端更新代开房间权限** */
	public static final int GAME_UPDATE_PLAYER_PROXY_OPEN_ROOM = 0xc31902;
	
	// //////////////////////////////////////////////////////////////////////////////////
	
	/*******************poker 诈金花******************************/
	// 创建vip房间
	public static final int GAME_SEARCH_VIP_ROOM_ACK = 0xcd1101;
	public static final int GAME_ENTER_VIP_ROOM = 0xcd1102;
	/**准备开始游戏c-s 0xc31077*/
	public static final int GAME_READY = 0xc31077;
	public static final int GAME_START_GAME_REQUEST = 0xcd1003;
	public static final int GAME_START_GAME_REQUEST_ACK = 0xcd1004;
	
}