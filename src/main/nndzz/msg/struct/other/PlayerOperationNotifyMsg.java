package com.chess.nndzz.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.msg.command.MsgCmdConstant;

/** 游戏服务器通知客户端，轮到玩家操作了 ***/
public class PlayerOperationNotifyMsg extends MsgBase {
	/** 操作类型：GameConstant.YABAO_OPT_... */
	public int operationType = 0;
	/** 操作玩家的座位号，取值（0~5...） */
	public int playerTablePos = 0;
	public int gold = -999999999;
	public int opValue = 0;
	public int operationTime = 0;
	public List<Integer> winPlayersPos = new ArrayList<Integer>();
	
	public List<Integer> coinItems = new ArrayList<Integer>();
	public List<Integer> allowCoinItems = new ArrayList<Integer>();
	
	public PlayerOperationNotifyMsg() {
		msgCMD = MsgCmdConstant.GAME_USER_OPERATION_NOTIFY_NNDZZ;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);

		operationType = ar.sInt(operationType);
		playerTablePos = ar.sInt(playerTablePos);
		gold = ar.sInt(gold);
		opValue = ar.sInt(opValue);
		operationTime  =ar.sInt(operationTime);
		winPlayersPos = (List<Integer>) ar.sIntArray(winPlayersPos);
		coinItems = (List<Integer>) ar.sIntArray(coinItems);
		allowCoinItems = (List<Integer>) ar.sIntArray(allowCoinItems);
	}
}