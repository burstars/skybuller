/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.nndzz.msg.struct.other;
import java.util.ArrayList;
import java.util.List;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class CreateVipRoomMsg extends MsgBase {

	public int quanNum;//几圈的
	public String psw="";
	public int roomID=0;
	public List<Integer> tableRule = new ArrayList<Integer>();//STiV modify
	public String gameID = "";
	public int isRecord = 0 ;
	public int isProxy = 0;//0:正常扣卡；1：代开扣卡；2：AA扣卡
	public int isSelectSite = 0;//选座模式  0:随机，1:选座
	/**参战人数*/
	public int userNum = 2;
	
	public String roomLevel = "1";
	public int clubCode = 0;
	
	public CreateVipRoomMsg() {
		msgCMD = MsgCmdConstant.GAME_VIP_CREATE_ROOM_NNDZZ;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		quanNum=ar.sInt(quanNum);
		psw = ar.sString(psw);
		roomID=ar.sInt(roomID);
		tableRule=(List<Integer>)ar.sIntArray(tableRule);//STiV modify
		gameID = ar.sString(gameID);

		isRecord = ar.sInt(isRecord);
		isProxy = ar.sInt(isProxy);
		isSelectSite = ar.sInt(isSelectSite);
		userNum = ar.sInt(userNum);
		
		roomLevel = ar.sString(roomLevel);
		clubCode = ar.sInt(clubCode);
	}

}
