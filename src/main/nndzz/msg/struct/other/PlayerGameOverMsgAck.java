package com.chess.nndzz.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.bean.User;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.nndzz.bean.SimplePlayer;
import com.chess.nndzz.table.GameTable;

 
 
/** 游戏结束***/
public class PlayerGameOverMsgAck  extends MsgBase 
{
	private static Logger logger = LoggerFactory.getLogger(PlayerGameOverMsgAck.class);

	public int roomID=0;
	public List<SimplePlayer> players=new ArrayList<SimplePlayer>();
	
	public int isVipTable;	//是否VIP桌子
	public int clubCode = 0;
	public int readyTime = 15;	//准备时间
	public int playerNum = 0 ;
	public int gameCount = 0 ;
	public int dealerPos = 0;
	
	//玩家手牌
	public List<Byte> player0HandCards=new ArrayList<Byte>();
	public List<Byte> player1HandCards=new ArrayList<Byte>();
	public List<Byte> player2HandCards=new ArrayList<Byte>();
	public List<Byte> player3HandCards=new ArrayList<Byte>();
	public List<Byte> player4HandCards=new ArrayList<Byte>();
	public List<Byte> player5HandCards=new ArrayList<Byte>();
	
	/**vip是否结束 cc 2017-08-07*/
	public int isOver = 0;
	
	public PlayerGameOverMsgAck()
	{
	  	 msgCMD=MsgCmdConstant.GAME_GAME_OVER_ACK_NNDZZ;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		roomID=ar.sInt(roomID);
		//
		players=(List<SimplePlayer>)ar.sObjArray(players);
		
		isVipTable=ar.sInt(isVipTable);
		clubCode = ar.sInt(clubCode);
		readyTime = ar.sInt(readyTime);
		playerNum = ar.sInt(playerNum);
		gameCount = ar.sInt(gameCount);
		dealerPos = ar.sInt(dealerPos);
		
		player0HandCards = (List<Byte>)ar.sByteArray(player0HandCards);
		player1HandCards = (List<Byte>)ar.sByteArray(player1HandCards);
		player2HandCards = (List<Byte>)ar.sByteArray(player2HandCards);
		player3HandCards = (List<Byte>)ar.sByteArray(player3HandCards);
		player4HandCards = (List<Byte>)ar.sByteArray(player4HandCards);
		player5HandCards = (List<Byte>)ar.sByteArray(player5HandCards);
		
		isOver = ar.sInt(isOver);
	}
	
	//
	public void init_players(List<User> lps,GameTable gt)
	{
		if(lps==null)
			return;
		//
		for(User pl : lps)
		{
			int plPos = pl.getTablePos();
			SimplePlayer sp=new SimplePlayer();
			sp.playerID =pl.getPlayerID();
			sp.palyerIndex=pl.getPlayerIndex();
			sp.playerName=pl.getPlayerName();
			sp.headImg=pl.getHeadImg();
			sp.sex = pl.getSex();
			
			//
			sp.gold=pl.getWinLoseGoldNum();
			sp.winLosePoint=pl.getWinLoseTotalCoinNum();
			sp.onceWinLosePoint = pl.getWinLoseCurrCoinNum();
			
			logger.debug("玩家【"+sp.playerName+"】牌局得分信息： 得分->"+sp.gold);
			//
			sp.tablePos=plPos;
			//
			sp.gameResult=pl.getFanType();
			sp.canFriend = pl.getCanFriend();
			sp.specialCoinNum = pl.getSpecialCoinNum();
			//
			sp.desc="";//pl.getPlayerName()+","+pl.getWinLoseGoldNum()+","+pl.getFanDesc();
			sp.ip = pl.getClientIP();
			sp.location = pl.getLocation();
			sp.winNum = pl.getUserGameHanderForNNDZZ().getWinNum();
			
			sp.pingBeiScore = pl.getUserGameHanderForNNDZZ().getPingBeiScore();
			sp.fanBeiScore = pl.getUserGameHanderForNNDZZ().getFanBeiScore();
			
			sp.maxCardShape = pl.getUserGameHanderForNNDZZ().getCardShape();
			sp.maxCardShapeLevel = pl.getUserGameHanderForNNDZZ().getMaxCardShapeLevel();
			logger.debug(sp.desc);//System.out.println(sp.desc);
			
			//TODO 玩家是否赢
			sp.isWin = pl.getWinState();
			
			players.add(sp);
			
			List<Byte> ch = pl.getCardsInHand();
			
			if(plPos ==0)
			{
				player0HandCards = ch;
			}
			else if(plPos == 1)
			{
				player1HandCards = ch;
			}
			else if(plPos == 2)
			{
				player2HandCards = ch;
			}
			else if(plPos == 3)
			{
				player3HandCards = ch;
			}
			else if(plPos == 4)
			{
				player4HandCards = ch;
			}
			else if(plPos == 5)
			{
				player5HandCards = ch;
			}
		}
	}
	
	//
	public void init_players_2(List<User> lps,GameTable gt,int init_took_gold)
	{
		if(lps==null)
			return;
		//
		for(int i=0;i<lps.size();i++)
		{
			User pl=lps.get(i);
			//
			SimplePlayer sp=new SimplePlayer();
			sp.playerID =pl.getPlayerID();
			sp.palyerIndex=pl.getPlayerIndex();
			sp.playerName=pl.getPlayerName();
			sp.headImg=pl.getHeadImg();
			sp.sex = pl.getSex();
			
			//
			sp.gold=pl.getVipTableGold()-init_took_gold;
			
			//
			sp.tablePos=pl.getTablePos();
			//
			sp.gameResult=0;
			sp.canFriend = pl.getCanFriend();
			//
			sp.desc="";
			sp.ip=pl.getClientIP();
			sp.location = pl.getLocation();
			 
			players.add(sp);
		}
	}
}