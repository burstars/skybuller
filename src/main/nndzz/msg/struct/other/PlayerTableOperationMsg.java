package com.chess.nndzz.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.msg.command.MsgCmdConstant;

/** 游戏服务器通知客户端,玩家操作了 ***/
public class PlayerTableOperationMsg extends MsgBase {
	/** 操作类型：GameConstant.NNDZZ_OPT_xx */
	public int operationType = 0;
	/** 操作玩家的座位号，取值（0~5） */
	public int playerTablePos = -1;
	/** 房间id,原来是 card_value */
	public int vipTableID = 0;
	/** 操作值，存倍数、下注筹码等 */
	public int opValue = 0;
	/** 我的手牌 */
	public List<Byte> myCards = new ArrayList<Byte>();
	
	public int clubCode = 0;
	
	public List<Integer> coinItems = new ArrayList<Integer>();
	public List<Integer> allowCoinItems = new ArrayList<Integer>();
	
	public PlayerTableOperationMsg() {
		msgCMD = MsgCmdConstant.GAME_USER_TABLE_OPERATION_NNDZZ;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		operationType = ar.sInt(operationType);
		playerTablePos = ar.sInt(playerTablePos);
		vipTableID = ar.sInt(vipTableID);
		opValue = ar.sInt(opValue);
		myCards = (List<Byte>) ar.sByteArray(myCards);
		clubCode = ar.sInt(clubCode);
		coinItems = (List<Integer>) ar.sIntArray(coinItems);
		allowCoinItems = (List<Integer>) ar.sIntArray(allowCoinItems);
	}
}