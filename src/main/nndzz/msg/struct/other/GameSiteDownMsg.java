package com.chess.nndzz.msg.struct.other;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.msg.command.MsgCmdConstant;
/**
 * 换坐消息：牌局开始前，玩家可以选择其他空位置坐下
 */
public class GameSiteDownMsg extends MsgBase {
	/**操作类型:0=为操作失败；1=操作成功*/
	public int opt_id = 0;
	/**游戏id*/
	public String game_id = "";
	/**房间id*/
	public int room_id = 0;
	/**桌子id*/
	public int table_id = 0;
	/**玩家id*/
	public String player_id = "";
	/**玩家原坐位号**/
	public  int  player_site_old = 0;
	/**玩家新坐位号**/
	public  int  player_site_new = 0;
	
	
	public GameSiteDownMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_GAME_CHANGE_SITE_NNDZZ;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		opt_id = ar.sInt(opt_id);
		game_id = ar.sString(game_id);
		room_id = ar.sInt(room_id);
		table_id = ar.sInt(table_id);		
		player_id = ar.sString(player_id);
		player_site_old = ar.sInt(player_site_old);		
		player_site_new = ar.sInt(player_site_new);		
	}
}
