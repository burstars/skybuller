package com.chess.nndzz.msg.struct.other;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.msg.command.MsgCmdConstant;

 
/**客户端通知游戏服务器，玩家的某些行为***/
public class PlayerGameOpertaionMsg  extends MsgBase 
{
	public int opertaionID=0;
	public int opValue=0;
	public PlayerGameOpertaionMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_GAME_OPERTAION;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		opertaionID=ar.sInt(opertaionID);
		opValue=ar.sInt(opValue);
	}
}