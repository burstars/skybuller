/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.nndzz.msg.struct.other;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.msg.command.MsgCmdConstant;

public class EnterVipRoomMsg extends MsgBase {

	public String tableID="";
	public String psw="";
	public int roomID=0;
	public int roomModel = 0;	//6:过6；8：过8；10：过10；14：过A
	/**俱乐部编号  */
	public int clubCode = 0;
	//
	public EnterVipRoomMsg() {
		msgCMD = MsgCmdConstant.GAME_ENTER_VIP_ROOM_NNDZZ;
	}

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		tableID=ar.sString(tableID);
		psw = ar.sString(psw);
		roomID=ar.sInt(roomID);
		roomModel=ar.sInt(roomModel);
		clubCode = ar.sInt(clubCode);
	}

}
