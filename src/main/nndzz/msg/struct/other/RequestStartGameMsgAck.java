package com.chess.nndzz.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.bean.User;
import com.chess.common.constant.GameConstant;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.nndzz.bean.SimplePlayer;

/**
 * 开始游戏
 ***/
public class RequestStartGameMsgAck extends MsgBase {
	/** 操作结果 */
	public int result = 0;
	/** 金币 */
	public int gold = 0;
	/** 房间ID */
	public int roomID = -1;
	/** 房间类型 */
	public int roomType = -1;
	/** 底注 */
	public int coinBaseNum = 0;
	/** 当前局数 */
	public int juCount = 0;
	/** 总局数 */
	public int totalJuNum = 0;
	/** 玩家位置 */
	public int tablePos = 0;
	/** 如果是vip桌子，这个不是0 */
	public int vipTableID = 0;
	/** 如果是vip桌子，房主ID */
	public String creatorID = "";
	// 如果是vip桌子，房主名字
	public String creatorName = "";
	// VIP桌子密码
	public String tablePassword = "";
	public List<SimplePlayer> players = new ArrayList<SimplePlayer>();
	public List<Integer> tableRules;// STiV modify
	/** 0:牌桌不添加语音功能，1：牌桌添加语音聊天功能 */
	public int isRecord = 0;
	/** 0:正常扣卡；1：代开扣卡；2：AA扣卡 */
	public int openRoomType = 0;
	/** 选座模式 0为随机，1为选座 */
	public int isSelectSite = 0;
	/** 代理开房玩家的名字 */
	public String proxyName = "";
	/* 玩家人数 */
	public int playerNum = 0;
	
	public int gameStart = 0;
	public int clubCode = 0;

	public RequestStartGameMsgAck() {
		msgCMD = MsgCmdConstant.GAME_START_GAME_REQUEST_ACK_NNDZZ;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		result = ar.sInt(result);
		gold = ar.sInt(gold);
		roomID = ar.sInt(roomID);
		coinBaseNum = ar.sInt(coinBaseNum);
		juCount = ar.sInt(juCount);
		totalJuNum = ar.sInt(totalJuNum);
		roomType = ar.sInt(roomType);
		
		tablePos = ar.sInt(tablePos);
		vipTableID = ar.sInt(vipTableID);
		creatorID = ar.sString(creatorID);
		creatorName = ar.sString(creatorName);
		tablePassword = ar.sString(tablePassword);
		tableRules = (List<Integer>) ar.sIntArray(tableRules);// STiV modify
		players = (List<SimplePlayer>) ar.sObjArray(players);
		isRecord = ar.sInt(isRecord);
		openRoomType = ar.sInt(openRoomType);
		isSelectSite = ar.sInt(isSelectSite);
		proxyName = ar.sString(proxyName);
		playerNum = ar.sInt(playerNum);
		gameStart = ar.sInt(gameStart);
		clubCode = ar.sInt(clubCode);
	}

	//
	public void init_players(List<User> lps, boolean isVipTable) {
		if (lps == null)
			return;
		
		for (int i = 0; i < lps.size(); i++) {
			User pl = lps.get(i);
			// 在等待，未确定是否进行下一局
			if (pl.getGameState() == GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE) {
				continue;
			}
			//
			SimplePlayer sp = new SimplePlayer();
			sp.playerID = pl.getPlayerID();
			sp.palyerIndex = pl.getPlayerIndex();
			sp.playerName = pl.getPlayerName();
			sp.headImg = pl.getHeadImg();
			//
			if (isVipTable)
				sp.gold = pl.getVipTableGold();
			else
				sp.gold = pl.getGold();
			//
			sp.tablePos = pl.getTablePos();
			sp.canFriend = pl.getCanFriend();
			sp.sex = pl.getSex();
			sp.inTable = pl.getOnTable() ? 1 : 0;
			sp.ip = pl.getClientIP();
			sp.readyFlag = pl.getReadyFlag();// 开局准备 
			sp.location = pl.getLocation();
			sp.headImgUrl = pl.getHeadImgUrl();
			players.add(sp);
		}
	}

	public void init_players_time_hend(List<User> lps) {
		if (lps == null)
			return;
		for (int i = 0; i < lps.size(); i++) {
			User pl = lps.get(i);
			// 在等待，未确定是否进行下一局
			//
			SimplePlayer sp = new SimplePlayer();
			sp.playerID = pl.getPlayerID();
			sp.palyerIndex = pl.getPlayerIndex();
			sp.playerName = pl.getPlayerName();
			sp.headImg = pl.getHeadImg();
			sp.gold = pl.getGold();
			sp.tablePos = pl.getTablePos();
			sp.canFriend = pl.getCanFriend();
			sp.sex = pl.getSex();
			sp.inTable = 1;
			sp.readyFlag = pl.getReadyFlag();// 开局准备 
			sp.location = pl.getLocation();
			players.add(sp);
		}
	}
}
