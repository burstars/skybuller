package com.chess.nndzz.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;

public class SendCardMsg extends MsgBase{
	public int dealerPos = 0;
	public int chooseSendNum1 = 0;
	public int chooseSendNum2 = 0;
	
	public List<Integer> sendCardPoses = new ArrayList<Integer>();
	
	/** 玩家手牌 */
	public List<Byte> player0Cards = new ArrayList<Byte>();
	public List<Byte> player1Cards = new ArrayList<Byte>();
	public List<Byte> player2Cards = new ArrayList<Byte>();
	public List<Byte> player3Cards = new ArrayList<Byte>();
	public List<Byte> player4Cards = new ArrayList<Byte>();
	public List<Byte> player5Cards = new ArrayList<Byte>();
	
	public SendCardMsg() {
		msgCMD = MsgCmdConstant.GAME_SEND_CARDS_NNDZZ;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		dealerPos = ar.sInt(dealerPos);
		chooseSendNum1 = ar.sInt(chooseSendNum1);
		chooseSendNum2 = ar.sInt(chooseSendNum2);
		
		sendCardPoses = (List<Integer>) ar.sIntArray(sendCardPoses);
		
		player0Cards = (List<Byte>) ar.sByteArray(player0Cards);
		player1Cards = (List<Byte>) ar.sByteArray(player1Cards);
		player2Cards = (List<Byte>) ar.sByteArray(player2Cards);
		player3Cards = (List<Byte>) ar.sByteArray(player3Cards);
		player4Cards = (List<Byte>) ar.sByteArray(player4Cards);
		player5Cards = (List<Byte>) ar.sByteArray(player5Cards);
	}
	
	public void setPlayersCard(int tablePos,List<Byte> cards){
		if(tablePos == 0)
			player0Cards = cards;
		if(tablePos == 1)
			player1Cards = cards;
		if(tablePos == 2)
			player2Cards = cards;
		if(tablePos == 3)
			player3Cards = cards;
		if(tablePos == 4)
			player4Cards = cards;
		if(tablePos == 5)
			player5Cards = cards;
	}
}
