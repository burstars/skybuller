package com.chess.nndzz.msg.struct.other;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.msg.command.MsgCmdConstant;

 
 
/**客户端通知游戏服务器，玩家的某些行为，服务器返回***/
public class PlayerGameOpertaionAckMsg  extends MsgBase 
{
	public String playerID ="";
	public String playerName="";
	public String targetPlayerName="";
	public int opertaionID=0;
	public int opValue=0;
	public int result=0;
	
	public int playerIndex=0;
	public int headImg=0;
	public int gold=0;
	public int tablePos=0;
	
	public int sex =0;
	
	public int canFriend =0;
	
	public String ip = "";
	public int readyFlag = 0;
	public String location = "";
	public String headImgUrl = "";
	public int winNum = -1;
	//
	//
	public PlayerGameOpertaionAckMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_GAME_OPT_ACK_NNDZZ;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		
		playerID=ar.sString(playerID);
		//
		playerName=ar.sString(playerName);
		targetPlayerName=ar.sString(targetPlayerName);
		//
		opertaionID=ar.sInt(opertaionID);
		opValue=ar.sInt(opValue);
		result=ar.sInt(result);
		
		playerIndex = ar.sInt(playerIndex);
		headImg=ar.sInt(headImg);
		sex = ar.sInt(sex);
		gold=ar.sInt(gold);
		tablePos=ar.sInt(tablePos);
		
		canFriend = ar.sInt(canFriend);
		ip = ar.sString(ip);
		readyFlag = ar.sInt(readyFlag);
		location = ar.sString(location);
		headImgUrl = ar.sString(headImgUrl);
		winNum = ar.sInt(winNum);
	}
}