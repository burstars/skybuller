package com.chess.nndzz.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.bean.User;
import com.chess.common.constant.GameConstant;
import com.chess.common.msg.command.MsgCmdConstant;

/** 牌局开始 ***/
public class GameStartMsg extends MsgBase {
	/** 我的位置 */
	public int myTablePos = 0;
	/** 我的手牌 */
	public List<Byte> myCards = new ArrayList<Byte>();
	/** 庄家位置 */
	public int dealerPos = -1;
	/** 局数-当前局数累加 */
	public int juCount = 1;
	/** 总局数 */
	public int totalJuNum = 1;
	
	/** 玩家操作时间（出牌时间） */
	public int playerOperationTime = 20;
	/** 是否断线重连，0不是，1是*/
	public int recover = 0;
	/** 本局服务费 */
	public int serviceGold=0;
	/** 最大玩牌人数 */
	public int playerCountMax = 6;
	/** 当局玩牌人数 */
	public int playerCount = 2;
	
	/** 玩家的金币数量，按玩家座位号相对应（第0个位置，存位置0的玩家金币） */
	public List<Integer> playersGoldList = new ArrayList<Integer>();
	/** 所有玩家是否在线 ，0在线，1不在线（第0个位置，存位置0的玩家的在线状态） */
	public List<Integer> playersOffLineList = new ArrayList<Integer>();
	
	/** 平倍押注 */
	public List<Integer> pingBeiCoinNums = new ArrayList<Integer>();
	/** 翻倍押注 */
	public List<Integer> fanBeiCoinNums = new ArrayList<Integer>();
	/** 亮牛 */
	public List<Integer> liangNiuNums = new ArrayList<Integer>();
	
	/** 玩家手牌 */
	public List<Byte> player0Cards = new ArrayList<Byte>();
	public List<Byte> player1Cards = new ArrayList<Byte>();
	public List<Byte> player2Cards = new ArrayList<Byte>();
	public List<Byte> player3Cards = new ArrayList<Byte>();
	public List<Byte> player4Cards = new ArrayList<Byte>();
	public List<Byte> player5Cards = new ArrayList<Byte>();
	
	public List<Integer> coinItems = new ArrayList<Integer>();
	public List<Integer> allowCoinItems = new ArrayList<Integer>();
	
	public GameStartMsg() {
		msgCMD = MsgCmdConstant.GAME_START_GAME_NNDZZ;
		
		for (int i = 0; i < 6; i++) {
			playersGoldList.add(1000);
			playersOffLineList.add(0);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar) {
		super.serialize(ar);
		myTablePos = ar.sInt(myTablePos);
		myCards = (List<Byte>) ar.sByteArray(myCards);
		dealerPos = ar.sInt(dealerPos);
		juCount = ar.sInt(juCount);
		totalJuNum = ar.sInt(totalJuNum);
		playerOperationTime = ar.sInt(playerOperationTime);
		recover = ar.sInt(recover);
		serviceGold = ar.sInt(serviceGold);
		
		playerCountMax = ar.sInt(playerCountMax);
		playerCount = ar.sInt(playerCount);
		
		playersGoldList = (List<Integer>) ar.sIntArray(playersGoldList);
		playersOffLineList = (List<Integer>) ar.sIntArray(playersOffLineList);
		
		pingBeiCoinNums = (List<Integer>) ar.sIntArray(pingBeiCoinNums);
		fanBeiCoinNums = (List<Integer>) ar.sIntArray(fanBeiCoinNums);
		liangNiuNums = (List<Integer>) ar.sIntArray(liangNiuNums);
		
		player0Cards = (List<Byte>) ar.sByteArray(player0Cards);
		player1Cards = (List<Byte>) ar.sByteArray(player1Cards);
		player2Cards = (List<Byte>) ar.sByteArray(player2Cards);
		player3Cards = (List<Byte>) ar.sByteArray(player3Cards);
		player4Cards = (List<Byte>) ar.sByteArray(player4Cards);
		player5Cards = (List<Byte>) ar.sByteArray(player5Cards);
		
		coinItems = (List<Integer>) ar.sIntArray(coinItems);
		allowCoinItems = (List<Integer>) ar.sIntArray(allowCoinItems);
	}

	public void setGold(List<User> plist, int countMax, boolean isVipTable) {
		if(playersGoldList.size() == 0){
			for (int i = 0; i < countMax; i++) {
				playersGoldList.add(1000);
			}
		}
		for (int i = 0; i < plist.size(); i++){
			User pl = plist.get(i);
			playersGoldList.set(pl.getTablePos(),pl.getVipTableGold());
		}
	}
	
	public void setOffLine(int tablePos, int countMax, int offLine){
		if(playersOffLineList.size() == 0){
			for (int i = 0; i < countMax; i++) {
				playersOffLineList.add(0);
			}
		}
		playersOffLineList.set(tablePos, offLine);
	}
	
	public void setPlayersPingBeiCoinNum(int tablePos, int countMax, int coinNum){
		if(pingBeiCoinNums.size() == 0){
			for (int i = 0; i < countMax; i++) {
				pingBeiCoinNums.add(-1);
			}
		}
		pingBeiCoinNums.set(tablePos, coinNum);
	}
	
	public void setPlayersFanBeiCoinNum(int tablePos, int countMax,int coinNum){
		if(fanBeiCoinNums.size() == 0){
			for (int i = 0; i < countMax; i++) {
				fanBeiCoinNums.add(-1);
			}
		}
		fanBeiCoinNums.set(tablePos, coinNum);
	}
	
	public void setPlayersLiangNiuNum(int tablePos, int countMax, boolean isLiangNiu){
		if(liangNiuNums.size() == 0){
			for (int i = 0; i < countMax; i++) {
				liangNiuNums.add(0);
			}
		}
		liangNiuNums.set(tablePos, isLiangNiu ? 1 : 0);
	}
	
	public void setPlayersCard(int tablePos,List<Byte> cards){
		if(tablePos == 0)
			player0Cards = cards;
		if(tablePos == 1)
			player1Cards = cards;
		if(tablePos == 2)
			player2Cards = cards;
		if(tablePos == 3)
			player3Cards = cards;
		if(tablePos == 4)
			player4Cards = cards;
		if(tablePos == 5)
			player5Cards = cards;
	}
	
	public void setMyCards(User pl) {
		myCards = pl.getCardsInHand();
	}
	
}