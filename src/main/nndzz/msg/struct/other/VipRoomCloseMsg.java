/**
 * Description:请求消息-获取玩家所有VIP房间记录
 */
package com.chess.nndzz.msg.struct.other;

import java.util.ArrayList;
import java.util.List;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.bean.User;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.nndzz.bean.SimplePlayer;
import com.chess.nndzz.table.GameTable;

public class VipRoomCloseMsg extends MsgBase {

	public List<SimplePlayer> players=new ArrayList<SimplePlayer>();
	
	/**最佳炮手*/
	public int paoPos = -1;
	
	/**大赢家*/
	public int winPos = -1;
	
	// 房主ID
	public int creatorPos = 0;
	/**代开强制解散，是为1，否为0**/
	public int proxyForceClose = 0;

	public VipRoomCloseMsg() 
	{
		msgCMD = MsgCmdConstant.GAME_VIP_ROOM_CLOSE;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		
		paoPos = ar.sInt(paoPos);
		winPos = ar.sInt(winPos);
		creatorPos = ar.sInt(creatorPos);
		proxyForceClose = ar.sInt(proxyForceClose);
		players=(List<SimplePlayer>)ar.sObjArray(players);
	}
	
	//
	public void init_players(List<User> lps,GameTable gt,int took)
	{
		if(lps==null)
			return;
		
		int maxWin = 0;
		
		for(int i=0;i<lps.size();i++)
		{
			User pl=lps.get(i);
			
			//
			SimplePlayer sp=new SimplePlayer();
			sp.playerID=pl.getPlayerID();
			sp.palyerIndex=pl.getPlayerIndex();
			sp.playerName=pl.getPlayerName();
			sp.headImg=pl.getHeadImg();
			sp.gold=pl.getVipTableGold();
			sp.canFriend = pl.getCanFriend();
			
			sp.winLosePoint = pl.getVipTableGold()-took;
			
			sp.tablePos = pl.getTablePos();
			sp.location = pl.getLocation();
			sp.maxCardShape = pl.getUserGameHanderForNNDZZ().getMaxCardShape();
			sp.maxCardShapeLevel = pl.getUserGameHanderForNNDZZ().getMaxCardShapeLevel();
			sp.maxCoinNum = pl.getUserGameHanderForNNDZZ().getMaxCoinNum();
			sp.winNum = pl.getUserGameHanderForNNDZZ().getWinNum();
			sp.loseNum = pl.getUserGameHanderForNNDZZ().getLoseNum();
			sp.winLoseTotalCoinNum = pl.getWinLoseTotalCoinNum();
			 
			players.add(sp);
			
			
			if( (maxWin < sp.gold)  && (sp.gold > 0) )
			{
				maxWin = sp.gold;
				this.winPos = i;
			}
		}
	}

}
