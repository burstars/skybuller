package com.chess.nndzz.msg.struct.other;

import com.chess.core.net.msg.MsgBase;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.msg.command.MsgCmdConstant;

 
/** 开始游戏***/
public class RequestStartGameMsg  extends MsgBase 
{

	public int roomID=0;
	
	//0 查房间
	//1 进入房间
	public int type=0;
	
	public String tableID="";
	
	public RequestStartGameMsg()
	{ 
	  	 msgCMD=MsgCmdConstant.GAME_START_GAME_REQUEST_NNDZZ;
	}
	
	@Override
	public void serialize(ObjSerializer ar)
	{
		super.serialize(ar);
		//
		roomID=ar.sInt(roomID);
		type=ar.sInt(type);
		if (type==1){
			tableID=ar.sString(tableID);
		}
		
	}
}