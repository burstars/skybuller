package com.chess.nndzz.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.chess.common.constant.GameConstant;

public class CardUtil {
	
	/**
	 * 平倍倍数
	 * 
	 * @return
	 */
	public static int getPingBeiMultiple(List<Byte> cards){
		return 1;
	}
	
	/**
	 * 翻倍倍数
	 * 
	 * @return
	 */
	public static int getFanBeiMultiple(List<Byte> cards) {
		int cardShape = CardUtil.getCardShape(cards);
		int multiple = 1;
		switch (cardShape) {
		case GameConstant.NNDZZ_CARD_TYPE_NO_NIU: {
			multiple = 1;
			break;
		}
		case GameConstant.NNDZZ_CARD_TYPE_HAS_NIU: {
			int nnNum = CardUtil.getNNTypeNum(cards);
			if (nnNum <= 6) {
				multiple = 1;
			} else if (nnNum == 7) {
				multiple = 2;
			} else if (nnNum == 8) {
				multiple = 3;
			} else if (nnNum == 9) {
				multiple = 4;
			}
			break;
		}
		case GameConstant.NNDZZ_CARD_TYPE_NIU_NIU: {
			multiple = 5;
			break;
		}
		case GameConstant.NNDZZ_CARD_TYPE_FIVE_GONG_NIU:{
			multiple = 5;
			break;
		}
		case GameConstant.NNDZZ_CARD_TYPE_BOMB_NIU: {
			multiple = 6;
			break;
		}
		}
		return multiple;
	}
	
	/**
	 * 比较两手牌的大小
	 * 
	 * @param iCardList
	 * @param otherCardList
	 * @return 我的牌大，返回 true，否则返回 false
	 */
	public static boolean compareMyCardsWithOtherCards(List<Byte> iCardList,List<Byte> otherCardList) {
		int myCardShape = getCardShape(iCardList);
		int otherCardShape = getCardShape(otherCardList);
		// 我的牌型大
		if (myCardShape > otherCardShape) {
			return true;
		}
		// 我的牌型小
		if (otherCardShape > myCardShape) {
			return false;
		}
		// 处理相同牌型
		if (myCardShape == GameConstant.NNDZZ_CARD_TYPE_BOMB_NIU) { // 我的牌和别人的牌都是炸弹牛
			return getBombNNCardTypeNum(iCardList) > getBombNNCardTypeNum(otherCardList);
		}
		// 都有牛
		if (myCardShape == GameConstant.NNDZZ_CARD_TYPE_HAS_NIU) {
			int myNum = getNNTypeNum(iCardList);
			int oNum = getNNTypeNum(otherCardList);
			if (myNum > oNum) {
				return true;
			}
			if(myNum < oNum) 
			{
				return false;
			}
		}
		// 我手里的最大牌
		byte myMaxCard = getMaxCardNumCard(iCardList);
		// 别人手里的最大牌
		byte otherMaxCard = getMaxCardNumCard(otherCardList);
		return getCardNum(myMaxCard) > getCardNum(otherMaxCard)
				|| (getCardNum(myMaxCard) == getCardNum(otherMaxCard) && getCardType(myMaxCard) > getCardType(otherMaxCard));
	}

	/**
	 * 根据给定的牌，获取牌型
	 * 
	 * @param iCardList
	 * @return
	 */
	public static int getCardShape(List<Byte> iCardList) {
		if (iCardList == null || iCardList.size() != 5) {
			return GameConstant.NNDZZ_CARD_TYPE_ERROR;
		}
		if (getBombNNCardTypeNum(iCardList) != -1) { // 炸弹牛
			return GameConstant.NNDZZ_CARD_TYPE_BOMB_NIU;
		}
		if (isFiveGongNiu(iCardList)) { // 五公牛
			return GameConstant.NNDZZ_CARD_TYPE_FIVE_GONG_NIU;
		}
		if (isNN(iCardList)) { // 牛牛
			return GameConstant.NNDZZ_CARD_TYPE_NIU_NIU;
		}
		if (hasNiu(iCardList)) { // 有牛
			return GameConstant.NNDZZ_CARD_TYPE_HAS_NIU;
		}
		return GameConstant.NNDZZ_CARD_TYPE_NO_NIU; // 没牛
	}

	/**
	 * 判断是否为5公牛
	 * 
	 * @param cards
	 *            要检查的牌
	 * @return 是5花牛，返回  true，否则返回 false
	 */
	public static boolean isFiveGongNiu(List<Byte> cards) {
		for (Byte card : cards) {
			if (getCardNum(card) <= 10) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 判断给定牌中是否有牛
	 * 
	 * @param cards
	 *            检测的5张牌
	 * @return 有牛返回 true,没牛返回 false
	 */
	public static boolean hasNiu(List<Byte> cards) {
		for (int i = 0; i <= 2; i++) {
			int cardNumi = getNNCardNum(cards.get(i));
			for (int j = i + 1; j <= 3; j++) {
				int cardNumj = getNNCardNum(cards.get(j));
				for (int k = j + 1; k < cards.size(); k++) {
					int cardNumk = getNNCardNum(cards.get(k));
					if ((cardNumi + cardNumj + cardNumk) % 10 == 0) {
						return true;
					}
				}
			}
		}
		return false;
	}
	/**
	 * 获取牛牛的点数
	 * @param cards
	 * @return 返回牛牛的点数，如果牛10返回0
	 */
	public static int getNNTypeNum(List<Byte> cards) {
		int totalNum = 0;
		for (int i = 0; i < cards.size(); i++) {
			totalNum += getNNCardNum(cards.get(i));
		}
		return totalNum % 10;
	}
	
	/**
	 * 判断是否是牛牛
	 * 
	 * @param cards
	 * @return 是牛牛，返回 true，否则返回 false
	 */
	public static boolean isNN(List<Byte> cards) {
		return hasNiu(cards) && getNNTypeNum(cards) == 0;
	}

	/**
	 * 获取炸弹牛的，炸弹点数
	 * 
	 * @param cards
	 * @return 如果有炸弹，则返回炸弹对应的牌点，否则返回 -1
	 */
	public static int getBombNNCardTypeNum(List<Byte> cards) {
		Map<Integer, Integer> cardNumMap = toCardNumCountMap(cards);
		for (Integer key : cardNumMap.keySet()) {
			if (cardNumMap.get(key).intValue() == 4) {
				return key;
			}
		}
		return -1;
	}

	/**
	 * 转换成牌点数量集合逻辑抽出
	 * 
	 * @param cardList
	 * @return
	 */
	public static Map<Integer, Integer> toCardNumCountMap(List<Byte> cardList) {
		Map<Integer, Integer> temp = new TreeMap<Integer, Integer>();
		for (int i = 0; i < cardList.size(); i++) {
			int cardNum = getCardNum(cardList.get(i));
			Integer count = temp.get(cardNum);
			if (count == null) {
				count = 0;
			}
			count++;
			temp.put(cardNum, count);
		}
		return temp;
	}

	/**
	 * 获取一组牌中，牌点最大的一张牌
	 * 
	 * @param cards
	 * @return
	 */
	public static byte getMaxCardNumCard(List<Byte> cards) {
		
		byte maxCard = cards.get(0);
		int cardNum = getCardNum(maxCard);
		for (int i = 1; i < cards.size(); i++) {
			int cn = getCardNum(cards.get(i));
			if (cardNum < cn || ( cardNum == cn && getCardType(maxCard) < getCardType(cards.get(i)))) {
				maxCard = cards.get(i);
				cardNum = getCardNum(maxCard);
			}
		}
		return maxCard;
	}
	/**
	 * 比较两张单牌的大小
	 * @param c1	第一张牌的编码
	 * @param c2	第二张牌的编码 
	 * @return
	 */
	public static boolean compareOne(byte c1,byte c2) {
		int cn1 = getCardNum(c1);
		int cn2 = getCardNum(c2);
		if(cn1 > cn2) {
			return true;
		}
		if(cn1 == cn2) {
			return getCardType(c1) > getCardType(c2);
		}
		return false;
	}
	/**
	 * 获取花色 计算花色公式：牌型 & 0x70
	 * 
	 * @param card
	 *            指定的牌编码
	 * @return 返回牌对应的花色编码
	 */
	public static int getCardType(byte card) {
		return card & GameConstant.CARDTYPE_MASK;
	}

	/**
	 * 获取牌点 计算牌点公式：牌型 - (牌型 & 0x70)
	 * 
	 * @param card
	 *            指定的牌编码
	 * @return 返回对应的牌点
	 */
	public static int getCardNum(byte card) {
		if (card == GameConstant.SPECIAL_CARD_XIAOWANG) {
			return GameConstant.CARD_NUM_XIAOWANG;
		}
		if (card == GameConstant.SPECIAL_CARD_DAWANG) {
			return GameConstant.CARD_NUM_DAWANG;
		}
		return (int) (card % 16);
	}

	/**
	 * 获取牛牛中计算的牌点
	 * 
	 * @param card
	 *            牌编码
	 * @return 如果牌点大于10，则返回10，否则返回对应的牌点
	 */
	public static int getNNCardNum(byte card) {
		int cardNum = getCardNum(card);
		return cardNum < 10 ? cardNum : 10;
	}

	/**
	 * 获取手中的指定牌的张数
	 * 
	 * @param cardlist
	 * @param card
	 * @return
	 */
	public static int getCardInHand(List<Byte> cardlist, byte card) {
		int num = 0;
		for (int i = 0; i < cardlist.size(); i++) {
			if (getCardNum(cardlist.get(i)) == getCardNum(card)) {
				num++;
			}
		}
		return num;
	}

	public static String formatRealNum(int cardNum) {
		if (cardNum >= 2 && cardNum <= 10) {
			return String.valueOf(cardNum);
		}
		switch (cardNum) {
		case 1:
			return "A";
		case 11:
			return "J";
		case 12:
			return "Q";
		case 13:
			return "K";
		case 16:
			return "小王";
		case 17:
			return "大王";
		}
		return "";
	}

	/**
	 * 花色
	 * 
	 * @param cardNum
	 * @return
	 */
	public static String formatRealColor(byte card) {
		int color = getCardType(card);
		switch (color) {
		case 0:
			return "方块";
		case 16:
			return "梅花";
		case 32:
			return "红桃";
		case 48:
			return "黑桃";
		default:
			return "";
		}
	}

	public static List<Integer> formatToCardNums(List<Byte> iCardList) {
		List<Integer> iReuslt = new ArrayList<Integer>(iCardList.size());
		for (byte card : iCardList) {
			int num = getCardNum(card);
			iReuslt.add(num);
		}
		return iReuslt;
	}

	public static String formatRealCardNums(List<Byte> iCardList) {
		StringBuilder sb = new StringBuilder(256);
		int i = 0;
		for (byte card : iCardList) {
			int num = getCardNum(card);
			String sNum = formatRealNum(num);
			String color = formatRealColor(card);
			if (!color.equals("")) {
				sb.append(color);
			}
			sb.append(sNum);
			if (i < iCardList.size() - 1) {
				sb.append(", ");
			}

			i++;
		}
		return sb.toString();
	}
}
