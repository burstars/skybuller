package com.chess.nndzz.util;

import java.util.List;

import com.chess.common.bean.User;
import com.chess.common.constant.GameConstant;
import com.chess.nndzz.table.GameRoom;
import com.chess.nndzz.table.GameTable;

public class CommonUtil {
	private static CommonUtil utils;

	private CommonUtil() {
	}

	public synchronized static CommonUtil getInstance() {
		if (utils == null) {
			utils = new CommonUtil();
		}
		return utils;
	}
	
	/**
	 * 根据玩家座位，获取对应玩家对象
	 * 
	 * @return 玩家对象，没有找到，则返回 null
	 */
	public User getUserByTablePos(GameTable gt, GameRoom gr, int tablePos) {
		if (gt.getPlayers() != null) {
			for (User user : gt.getPlayers()) {
				if (user.getTablePos() == tablePos) {
					return user;
				}
			}
		}
		return null;
	}

	/**
	 * 玩家 tablePos 到 玩家所在玩家集合索引的转换
	 * 
	 * @param gt
	 * @param gr
	 * @param tablePos
	 * @return
	 */
	public int tablePost2PlalyerIndex(GameTable gt, GameRoom gr, int tablePos) {
		List<User> players = gt.getPlayers();
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).getTablePos() == tablePos) {
				return i;
			}
		}
		return -1;
	}

	// 获取对家位置
	public int getOptPosition(int pos) {
		int optPos = pos + 2;
		if (optPos >= 4) {
			optPos = optPos - 4;
		}
		return optPos;
	}

	// 获取对家玩家
	public User getOptUser(GameTable gt, GameRoom gr, User plx) {
		List<User> users = gt.getPlayers();
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getTablePos() == getOptPosition(plx.getTablePos())) {
				return users.get(i);
			}
		}
		return null;
	}

	// 获取上家位置
	public int getUpPosition(GameTable gt, int pos) {
		int optPos = -1;
		switch (pos) {
		case 3:
			optPos = 2;
			break;
		case 2:
			optPos = 1;
			break;
		case 1:
			optPos = 0;
			break;
		case 0:
			optPos = 3;
			break;
		default:
			break;
		}

		return optPos;
	}

	/**
	 * 获取上家
	 * 
	 * @param 
	 * @return
	 */
	public User getUpUser(GameTable gt, GameRoom gr, User plx) {
		List<User> users = gt.getPlayers();
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getTablePos() == getUpPosition(gt,
					plx.getTablePos())) {
				return users.get(i);
			}
		}
		return null;
	}

	// 获取下家位置
	public int getDownPosition(GameTable gt, int pos) {
		int optPos = -1;
		switch (pos) {
		case 3:
			optPos = 0;
			break;
		case 2:
			optPos = 3;
			break;
		case 1:
			optPos = 2;
			break;
		case 0:
			optPos = 1;
			break;
		default:
			break;
		}

		return optPos;
	}

	/**
	 * 获取下家玩家
	 * 
	 * @param 
	 * @return
	 */
	public User getDownUser(GameTable gt, GameRoom gr, User plx) {
		for (User user : gt.getPlayers()) {
			if (user.getTablePos() == this.getDownPosition(gt,
					plx.getTablePos())) {
				return user;
			}
		}
		return null;
	}
}
