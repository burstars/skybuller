package com.chess.nndzz.table;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.service.impl.CacheUserService;

/**
 */
public class GameProcessThreadPool {
    private List<GameProcessThread> tmgr_list = new ArrayList<GameProcessThread>();
    private int tmgr_capacity = 0;

    public GameProcessThreadPool(CacheUserService ps, int pool_capacity, int table_capacity) {
        tmgr_capacity = pool_capacity;
        for (int i = 0; i < tmgr_capacity; i++) {
            GameProcessThread tmgr = new GameProcessThread(ps, table_capacity);
            tmgr.init();
            tmgr.start();
            tmgr_list.add(tmgr);
        }
    }

    private GameProcessThread getTmgr() {

        for (GameProcessThread tmgr : tmgr_list) {
            if(tmgr.getTableCapacity() > tmgr.getTableCount()) {
                return tmgr;
            }
        }
        return null;
    }

    public GameProcessThread get_tmgr(String table_id) {
        for (GameProcessThread tmgr : tmgr_list) {
            if(tmgr.getTableCount() > 0) {
                GameTable gt = tmgr.getTable(table_id);
                if(gt != null) {
                    return tmgr;
                }
            }
        }
        return null;
    }

    public GameProcessThread addTable(GameTable gt) {
        GameProcessThread tmgr = get_tmgr(gt.getTableID());
        if(tmgr == null) {
            tmgr = getTmgr();
        }
        if(tmgr != null) {
            tmgr.addTable(gt);
            /*
            System.out.println("-------add_table_to_tmgr-------");
            System.out.println("tmgr id:" + tmgr.getId());
            System.out.println("table id:" + gt.getTableID());
            System.out.println("-------------------------------");
            */
            return tmgr;
        }
        return null;
    }

}
