package com.chess.nndzz.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.operations.Gt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.UUIDGenerator;
import com.chess.core.net.msg.MsgBase;
import com.chess.common.bean.GameConfig;
import com.chess.common.bean.MahjongCheckResult;
import com.chess.common.bean.TClubLevel;
import com.chess.common.bean.User;
import com.chess.common.constant.ConfigConstant;
import com.chess.common.constant.GameConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.struct.other.PlayerAskCloseVipRoomMsgAck;
import com.chess.common.service.IClubService;
import com.chess.common.service.ISystemConfigService;
import com.chess.common.service.impl.CacheUserService;
import com.chess.common.util.ControlCardsUtil;
import com.chess.nndzz.bean.GameMultiple;
import com.chess.nndzz.bean.UserGameHander;
import com.chess.nndzz.msg.struct.other.GameStartMsg;
import com.chess.nndzz.msg.struct.other.PlayerOperationNotifyMsg;
import com.chess.nndzz.msg.struct.other.SendCardMsg;
import com.chess.nndzz.util.CardUtil;

public class GameTable {

	private static Logger logger = LoggerFactory.getLogger(GameTable.class);
	
	private CacheUserService playerService = null;
	private ISystemConfigService cfgService = null;
	
	public static boolean disable_test = false;
	public static byte[] test_cards = null;

	public static int test_cards_dealer = 0;
	
	public GameProcessThread gameProcessThread = null;

	public GameRoom game_room = null;
	
	private boolean isStartGame = false; // 牌桌是否已经开始游戏了，开始之后不能再进玩家了

	private static final int TABLE_RULE_DEFAULT[] = new int[] {
	// Test
	};

	private String tableID = "";
	/**vip房间id，方便查找*/
	private int vipTableID = 0;
	/**产品ID*/
	private String gameId = "";
	/**玩家人数*/
	private int playerCount = 2;
	/**roomID*/
	private int roomID = 0;
	/**房间类型：2人、3人、4人*/
	private int roomType = 0;
	/**庄家位置索引*/
	private int dealerPos = -1;
	/**vip房间的密码md5形式*/
	private String vipPswMd5 = "";
	/**创建者playerID*/
	private String creatorPlayerID = "";
	/**创建者playerName*/
	private String creatorPlayerName = "";
	/**游戏规则:局数or圈数，通用*/
	private int roomModel = 12;
	/** 局数-当前局数累加 */
	public int juCount = 0;
	/** 总局数 */
	public int totalJuNum = 0;
	/**桌子规则*/
	private List<Integer> tableRuleOption = new ArrayList<Integer>();
	/**桌子规则test*/
	private List<Integer> tableRule = new ArrayList<Integer>();
	
	/**玩家集合*/
	private List<User> players = new ArrayList<User>();
	/**桌子上空闲的位置初始化为0，1，2...*/
	private List<Integer> freePos = new ArrayList<Integer>();
	
	/**这个房间的房卡基础id=3333*/
	private int itemBaseID = 0;
	/**vip桌子创建的时间*/
	private long vipCreateTime = 0;
	
	/**桌子状态*/
	private int state = GameConstant.TABLE_STATE_INVALID;
	private int backup_state = GameConstant.TABLE_STATE_INVALID;
	
	/**等待时间：等待开始startTime记录*/
	private long waitingStartTime = 0L; 
	/**等待时间：解散房间等待开始startTime记录*/
	private long applyCloseStartTime = 0L;
	private int playSubstate = 0;// 玩牌的时候的一些子状态，比如等待客户播动画，或者无操作
	/**是否已经发送vip断线等待消息*/
	private boolean sendClientWaitingBreakPlayerNotify = false;

	/**vip房间开始游戏日志记录时间*/
	private Date gameStartTime = null;
	private Date gameEndTime = null;
	/**人数到齐的开始时间*/
	private long readyTime = 0L;
	/**本局开始时间*/
	private long handStartTime = 0L;
	/**本局结束时间*/
	private long handEndTime = 0L;
	/**底注大小*/
	private int dizhu = 0;
	/**基础筹码*/
	private int coinBaseNum = 1;
	//
	private boolean singlePlayerTable = false;

	/**桌子上的尚未被摸走牌*/
	private List<Byte> cards = new ArrayList<Byte>();
	/**倍数对象*/
	private GameMultiple gameMultiple = new GameMultiple();
	/**暂停开始时间*/
	private long pausedTime = 0L;
	
	private int currentWinPlayerIndex = -1;
	/**当前操作的玩家位置*/
	private int currentOpertaionPlayerIndex;

	/** 等待玩家进行某个操作*/
	private MahjongCheckResult waitingPlayerOperate = null;
	/**当前打出来的牌；*/
	private List<Byte> currentCards = new ArrayList<Byte>();
	/** 当前这牌哪个玩家打出来的*/
	private int cardOpPlayerIndex = -1;

	/**vip房解散参数*/
	private String tableEndWay = "";
	
	/**是否支付vip房卡*/
	private boolean isPayVipKa = true;
	/**最新等待其他玩家进入时间*/
	private long newWailtOtherPlayerTime = 0;

	/**循环错误超过一定次数后就把玩家踢出*/
	private int tickError = 0;

	/**同意结束VIP房间的人数（超过两人就结束房间）*/
	private int closeVipRoomNum = 0;

	/**不同意结束VIP房间的人数*/
	private int disAgreeCloseVipRoomNum = 0;

	private Map<String, Long> prevApplyCloseVipRoom = new HashMap<String, Long>();
	/**vip房间能否关闭标志 */
	public boolean canCloseVipRoom = true;
	
	public boolean good_cards_sent = false;

	private int vip_game_over_waiting_start_game_time = 0;

	private long PrevApplyCloseVipRoomTime = 0;
	
	/**是否选座*/
	private int isSelectSite = 0;
	/**牌桌内出牌通知次数*/
	private int chuNotifyCount = 0;
	/**是否单局结算了*/
	private boolean isSingleRoundOver = true;
	
	/**代理开房的创建人*/
	private User proxyCreator = null;
	/**开房模式:0=一人支付；1=代开；2=AA cc modify 2017-8-15*/
	private int proxyState = 0;
	/**代开优化功能：强制解散代开房间*/
	private boolean isProxyForceClose = false;
	/**代开优化：是否已开始游戏*/
	private boolean isStart = false;
	/**当前进行到第几轮跟牌，第一轮不能执行比牌操作*/
	private int currentRound = 0;
	
	private int isRecordTable = 0;
	/** 最后一个被比牌玩家 */
	private int lastComparedUserTablePos = -1;
	/** 最后的比牌cp */
	private boolean isLastCPForBiPai = false;
	
	/**配牌工具类（20170927）*/
	private ControlCardsUtil controlCardsUtil=null;
	
	private Random random = new Random();
	
	private int chooseSendNum1 = 0;
	private int chooseSendNum2 = 0;
	private List<Integer> sendCardPoses = null;
	private List<Byte> dealerCards = null;
	private int dealerWinLoseTotal = 0;
	
	/**俱乐部编号  2018-08-22*/
	protected int clubCode = 0;
	/**俱乐部牌桌类型（固定的6张桌）：1=公共空桌可用；2=自定义桌子（私）; 3 固定牌桌在玩状态*/
	protected int clubTableState = 0;
	/**对应的是那一个俱乐部的模板索引*/
	private int clubTemplateIndex = 0;
	private int quantityType = -1;
	private String roomLevel="1";
	private Map<Integer, Boolean> kouKaMap = new HashMap<Integer, Boolean>();
	
	public GameTable(int _roomType) {
		tableID = UUIDGenerator.generatorUUID();
		dizhu = 1;
		gameMultiple.basePoint = dizhu;
		GameContext.gameId = "nndzz";

		this.roomType = _roomType;
		if (cfgService == null) {
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		}

		if (playerService == null) {
			playerService = (CacheUserService) SpringService
					.getBean("playerService");
		}

		if (vip_game_over_waiting_start_game_time == 0) {
			vip_game_over_waiting_start_game_time = cfgService.getPara(
					ConfigConstant.VIP_GAME_OVER_WAITING_START_GAME_TIME)
					.getValueInt();
		}
	}

	public boolean isVipTable() {
//		if (this.roomType == GameConstant.ROOM_TYPE_VIP
//				|| this.roomType == GameConstant.ROOM_TYPE_COUPLE)
//			return true;
//		return false;
		return true;
	}

	/**
	 * 一个桌子用完释放掉，每次建新桌
	 * @author  2017-11-15
	 * GameTable.java
	 * void
	 */
	public void feeTable() {
		logger.info("free table=" + this.tableID);// System.out.println("free table="
													// + this.tableID);
		this.force_clear_player();
		this.clear_all();
		this.resetFreePos();
	}

	/**
	 * 洗牌
	 * @author  2017-11-15
	 * GameTable.java
	 * void
	 */
	public void washCards() {
		//
		cards.clear();

		// 1 	 2 		3 	4 	  5    6 	7 	8 	9 	10 	  11   12 	13
		// 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09 0x0A 0x0B 0x0C 0x0D
		// 方块A-13
		// 0x11	0x12 0x13 0x14 0x15 0x16 0x17 0x18 0x19 0x1A 0x1B 0x1C 0x1D
		// 梅花A-13
		// 0x21	0x22 0x23 0x24 0x25 0x26 0x27 0x28 0x29 0x2A 0x2B 0x2C 0x2D
		// 红桃A-13
		// 0x31	0x32 0x33 0x34 0x35 0x36 0x37 0x38 0x39 0x3A 0x3B 0x3C 0x3D
		// 黑桃A-13
		// 0x4E 0x4F 小王、大王
		for (int i = 0; i < 4; i++) {
			for (int j = 1; j < 14; j++) {
				int ib = (i << GameConstant.DDZ_COLOR_SHIFTS) + j;
				byte b = (byte) (ib & 0xff);
				cards.add(b);
			}
		}
		logger.info("【牛牛】基础牌型：【" + cards.size() + "】张，牌型：" + cards);
		//
		if (disable_test || test_cards == null)
			Collections.shuffle(cards);
		else {
			for (int i = 0; i < test_cards.length; i++) {
				if (test_cards[i] == 0)
					break;
				setTestCard(i, test_cards[i]);
			}
		}
	}

	boolean setTestCard(int pos, byte card) {
		int remainCount = cards.size() - pos;
		while (cards.get(pos) != card) {
			remainCount--;
			if (remainCount < 0) {
				traceLog("Invalid test_cards. i=" + pos + ", card=" + card);
				return false;
			}
			Byte b = cards.remove(pos);
			cards.add(b);
		}
		return true;
	}

	/**
	 * 庄家移动到下一个位置
	 * @author  2017-11-15
	 * GameTable.java
	 * void
	 */
//	public void moveDealer() {
//		// 随机
//		// dealerPos = new Random().nextInt(playerCount);
//		dealerPos++;
//		//
//		if (dealerPos >= this.players.size()) {
//			dealerPos = 0;
//		}
//	}

	/**
	 * vip是否结束：判断头游玩家是否过级
	 * 
	 * @author  2017-8-7
	 * @return
	 */
	public boolean isVipTableTimeOver() {
		if (!isVipTable()) {
			return false;
		}
		
		return (juCount+1) > totalJuNum;
	}

	//
	public void addWaitingPlayerOp(MahjongCheckResult checkResult) {
		waitingPlayerOperate = checkResult;
	}

	/**
	 * 查询当前桌子在等谁操作
	 * @author  2017-11-15
	 * GameTable.java
	 * @return
	 * MahjongCheckResult
	 */
	public MahjongCheckResult getCurrentWaitingOperationPlayer() {
		return waitingPlayerOperate;
	}

	/**
	 * 移除当前桌子在等谁操作
	 * @author  2017-11-15
	 * GameTable.java
	 * void
	 */
	public void clearCurrentWaitingOperationPlayer() {
		waitingPlayerOperate = null;
	}

	/**
	 * 获取当前操作的玩家
	 * @return
	 */
	public User getCurrentOperationPlayer() {
		for(User user : players) {
			if(user.getTablePos() == currentOpertaionPlayerIndex) {
				return user;
			}
		}
		return null;
	}
	/**
	 * 获取庄家下一个玩家的索引
	 * @return
	 */
	public int getDealerUserNextIndex() {
		int index = -1;
		for(int i = 0 ; i < players.size(); i ++) {
			User pl = players.get(i); 
			if(pl.getTablePos() == dealerPos) {
				index =  i;
				break;
			}
		}
		index = index + 1;
		if(index == players.size()) {
			index = 0;
		}
		return index;
	}
	/**
	 * 获取当前操作玩家的索引
	 * @param pl
	 * @return
	 */
	public int getCurrentPlayerIndex() {
		for(int i = 0 ; i < players.size(); i ++) {
			User pl = players.get(i); 
			if(pl.getTablePos() == currentOpertaionPlayerIndex) {
				return i;
			}
		}
		return -1;
	}

	private void shuffle_send_cards(List<Integer> peiPos, List<User> stakeCoinPlayers) {
		Map<Integer, List<Byte>> srcMap = new java.util.TreeMap<Integer, List<Byte>>();
		int plNum = stakeCoinPlayers.size();// 玩家数量
		int cNum = 5; 
		List<Integer> randomPos = new ArrayList<Integer>();
		for (int i = 0; i < plNum; i++) {
			randomPos.add(i);
		}
		for (int i = 0; i < plNum; i++) {
			List<Byte> src = srcMap.get(i);
			if (src == null) {
				src = new ArrayList<Byte>();
			}
			// 每人发 cNum 张牌
			for (int z = 0; z < cNum; z++) {
				src.add(cards.remove(0));
			}
			srcMap.put(randomPos.get(i), src);
		}
		// 发手牌到玩家手里
		for (int i = 0; i < stakeCoinPlayers.size(); i++) {
			if (peiPos != null && peiPos.contains(i)) {
				continue;
			}
			User pl = stakeCoinPlayers.get(i);
			//清空之前的发牌信息
			pl.clear_cards_nndzz();
			List<Byte> cl = pl.getCardsInHand();
			cl.clear();
			List<Byte> src = srcMap.get(randomPos.get(i));
			pl.setCardsInHand(src);
			logger.info("【牛牛】发牌，随机发牌==>房间ID【" + getVipTableID() + "】，玩家ID【"+pl.getPlayerIndex()
					+"】，玩家昵称【"+pl.getPlayerName()+"】，发牌："+pl.getCardsInHand());
		}
	}
	
	/**
	 * 发送游戏开始消息
	 * @param service_gold
	 * @param operationTime
	 */
	public void sendGameStartMsg(int service_gold,int operationTime){
		// 当前轮到庄家操作
		this.currentOpertaionPlayerIndex = dealerPos;
		
		// 发送到前台
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			// 把牌发给玩家
			sendGameStartMsg(pl, service_gold, operationTime);
		}
	}
	
	
	public void sendGameStartMsg(User pl, int service_gold,int operationTime) {
		logger.info("【牛牛】发开局消息==>房间ID【" + getVipTableID() + "】，玩家ID【"
				+ pl.getPlayerIndex() + "】，玩家位置【" + pl.getTablePos()
				+ "】，玩家昵称【" + pl.getPlayerName() + "】手牌【"
				+ CardUtil.formatRealCardNums(pl.getCardsInHand()) + "】");

		// 把牌发给玩家
		GameStartMsg msg = new GameStartMsg();
		msg.dealerPos = getRealDealerPos();
		msg.myCards = pl.getCardsInHand();
		msg.myTablePos = pl.getTablePos();
		msg.juCount = juCount;
		msg.totalJuNum = this.totalJuNum;
		msg.setGold(players, this.playerCount, this.isVipTable());
		msg.serviceGold = service_gold;
		msg.playerOperationTime = operationTime;
		this.isSingleRoundOver = false;
		
		msg.juCount = getJuCount();
		msg.totalJuNum = getTotalJuNum();
		
		msg.playerCountMax = getMaxPlayer();
		msg.playerCount = getPlayerNum();
		
		for (int i = 0; i < getPlayers().size(); i++) {
			User pxx = getPlayers().get(i);
			
			if((pxx != null) && (!pxx.getOnTable())){
				msg.setOffLine(pxx.getTablePos(), playerCount, 1);
			}else{
				msg.setOffLine(pxx.getTablePos(), playerCount, 0);
			}
			if (pxx != null) {
				int pos = pxx.getTablePos();
				UserGameHander hander = pxx.getUserGameHanderForNNDZZ();
				msg.setPlayersPingBeiCoinNum(pos, playerCount, hander.getPingBeiCoin());
				msg.setPlayersFanBeiCoinNum(pos, playerCount, hander.getFanBeiCoin());
				msg.setPlayersLiangNiuNum(pos, playerCount, hander.isLiangNiu());
				
				int plPos = pxx.getTablePos();
				List<Byte> ch = pxx.getCardsInHand();
				if(plPos ==0)
				{
					msg.player0Cards = ch;
				}
				else if(plPos == 1)
				{
					msg.player1Cards = ch;
				}
				else if(plPos == 2)
				{
					msg.player2Cards = ch;
				}
				else if(plPos == 3)
				{
					msg.player3Cards = ch;
				}
				else if(plPos == 4)
				{
					msg.player4Cards = ch;
				}
				else if(plPos == 5)
				{
					msg.player5Cards = ch;
				}
			}
		}
		msg.setGold(players, playerCount, true);
		// 庄家的牌
		msg.setPlayersCard(5, dealerCards);
		msg.coinItems = this.getCoinItemsByLevel();
		msg.allowCoinItems = this.getPlayerAllowCoinItems(msg.coinItems, pl);
		
		GameContext.gameSocket.send(pl.getSession(), msg);
	}
	
	private void chooseFirstSendCard(){
		// 随机抽张牌
		chooseSendNum1 = random.nextInt(5) + 1;
		chooseSendNum2 = random.nextInt(5) + 1;
		
		List<User> pls = getStakeCoinPlayers();
		Set<Integer> set = new TreeSet<Integer>();
		for(User pl : pls){
			set.add(pl.getTablePos());
		}
		if(clubCode == 0){
			set.add(dealerPos);
		}
		sendCardPoses = new ArrayList<Integer>(set);
		logger.info("【牛牛】发牌抽取发牌位置==>房间ID【" + getVipTableID() + "】，抽取骰子点数："+chooseSendNum1+", "+chooseSendNum2
					+", 庄家位置【"+dealerPos+"】，发牌位置集合【"+sendCardPoses+"】");
	}
	
	/**
	 * 发牌
	 * GameTable.java
	 * void
	 */
	public void sendCards() {
		// 抽发牌顺序
		this.chooseFirstSendCard();

		List<User> pls = getStakeCoinPlayers();
		if(clubCode == 0){
			pls.add(getPlayerByTablePos(dealerPos));
		}
		logger.info("【牛牛】发牌==>房间ID【" + getVipTableID() + "】，发牌玩家:"+pls);
		
		long ctt = System.currentTimeMillis();
		this.setWaitingStartTime(ctt);
		this.setPlaySubstate(GameConstant.TABLE_SUB_STATE_SEND_CARD);
		
		if(controlCardsUtil==null){
			controlCardsUtil=new ControlCardsUtil();	
		}
		Map<Integer,List<Byte>> cardsMap=controlCardsUtil.getControlCards(pls);
		boolean isPeiCard = cardsMap!=null; // 配牌开关
		// 给配牌的玩家发牌
		if (isPeiCard) {
			// 配牌在这里配		
			int plNum=pls.size();//玩家数量
			int cNum=5;
			List<Integer> randomPos = new ArrayList<Integer>();
			for(int i = 0 ; i < plNum ; i ++) {
				randomPos.add(pls.get(i).getTablePos());
			}
			//移除配牌
			for (int i = 0; i < pls.size(); i++) {
				List<Byte> peiCards=cardsMap.get(pls.get(i).getTablePos());
				if (peiCards!=null&&peiCards.size()>0) {
					for(byte scard:peiCards){
						int idx = 0;
						for (int k = 0; k < cards.size(); k++) {
							Byte b = cards.get(k);
							if (b == scard) {
								idx = k;
								cards.remove(idx);
								break;
							}
						}						
					}
				}
			}
			
			Map<Integer, List<Byte>> srcMap = new java.util.TreeMap<Integer, List<Byte>>();				
			for (int i = 0; i < pls.size(); i++) {
				List<Byte> peiCards=cardsMap.get(pls.get(i).getTablePos());
				if (peiCards!=null&&peiCards.size()>0) {
					User pl = pls.get(i);
					List<Byte> cl = pl.getCardsInHand();
					int size=peiCards.size();
					//补齐剩余牌
					for (int j = 0; j < cNum-size; j++) {
						byte c = cards.remove(0);
						peiCards.add(c);
					}
					cl.clear();
					pl.setCardsInHand(peiCards);
					logger.info("【牛牛】发牌，配牌，配牌玩家==>房间ID【" + getVipTableID() + "】，玩家ID【"+pl.getPlayerIndex()
								+"】，玩家昵称【"+pl.getPlayerName()+"】，发牌："+pl.getCardsInHand());
					continue;
				}	
				List<Byte> src =new ArrayList<Byte>();
				User pl = pls.get(i);
				pl.clear_cards_nndzz();
				for (int j = 0; j < cNum; j++) {
					byte c = cards.remove(0);
					src.add(c);
				}
				srcMap.put(i, src);
				pl.setCardsInHand(src);
				logger.info("【牛牛】发牌，配牌，非配牌玩家==>房间ID【" + getVipTableID() + "】，玩家ID【"+pl.getPlayerIndex()
						+"】，玩家昵称【"+pl.getPlayerName()+"】，发牌："+pl.getCardsInHand());
			}

		} else {
			shuffle_send_cards(null, pls);
		}
		
		// 发送到前台
		SendCardMsg sMsg = new SendCardMsg();
		sMsg.dealerPos = dealerPos;
		sMsg.chooseSendNum1 = chooseSendNum1;
		sMsg.chooseSendNum2 = chooseSendNum2;
		sMsg.sendCardPoses = sendCardPoses;
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			sMsg.setPlayersCard(pl.getTablePos(), pl.getCardsInHand());
			logger.info("给玩家【"+pl.getPlayerIndex()+", "+pl.getPlayerName()+", "+pl.getTablePos()+"】发牌："+pl.getCardsInHand());
		}
		
		// 给庄家发牌
		if(clubCode > 0){
			dealerCards =new ArrayList<Byte>(5);
			for(int i=0; i<5; i++){
				dealerCards.add(cards.remove(0));
			}
			// 庄家的牌
			sMsg.setPlayersCard(5, dealerCards);
		}
		
		this.sendMsgToTable(sMsg);

		for (int rule : TABLE_RULE_DEFAULT) {
			if (!this.tableRule.contains(rule))
				this.tableRule.add(rule);
		}
		traceLog("Rule: " + tableRule);
	}
	
	/**
	 * 获取押注筹码
	 * @return
	 */
	public List<Integer> getCoinItemsByLevel(){
		List<Integer> coinItems = null;
		if(clubCode > 0){
			IClubService clubService = (IClubService) SpringService.getBean("clubService");
			coinItems = clubService.getCoinItems(roomLevel);
		}else{
			String str = GameContext.getConfigParam("vip_coins");
			String[] strs = StringUtils.split(str, ",");
			coinItems = new ArrayList<Integer>(strs.length);
			for(String s : strs){
				Integer coin = Integer.parseInt(s);
				coinItems.add(coin);
			}
		}
		return coinItems;
	}
	
	/**
	 * 获取玩家可以押注的筹码
	 * @param coinItems
	 * @param pl
	 * @return
	 */
	public List<Integer> getPlayerAllowCoinItems(List<Integer> coinItems, User pl){
		// 加载可以下注的筹码
		UserGameHander hander = pl.getUserGameHanderForNNDZZ();
		int gold = pl.getVipTableGold();
		int pingBeiCoin = hander.getPingBeiCoin();
		int fanBeiCoin = hander.getFanBeiCoin();
		
		int maxCoin = 0;
		if(clubCode > 0){
			IClubService clubService = (IClubService) SpringService.getBean("clubService");
			TClubLevel clubLevel = clubService.getClubLevel(roomLevel);
			maxCoin = clubLevel.getMaxGold();
		}else{
			maxCoin = GameContext.getConfigParamToInt("vip_max_coin", 3000);
		}
		
		List<Integer> onesCoinItems = new ArrayList<Integer>(coinItems.size());
		for(Integer coin : coinItems){
			
			if(clubCode > 0){
				if(gold >= coin){
					int last = gold;
					if(last > maxCoin){
						last = maxCoin;
					}
					if(pingBeiCoin != -1){
						last = last - pingBeiCoin;
					}
					if(fanBeiCoin != -1){
						last = last - fanBeiCoin;
					}
					if(last >= coin){
						onesCoinItems.add(coin);
					}
				}
			}else{
				int last = maxCoin;
				if(pingBeiCoin != -1){
					last = last - pingBeiCoin;
				}
				if(fanBeiCoin != -1){
					last = last - fanBeiCoin;
				}
				if(last >= coin){
					onesCoinItems.add(coin);
				}
			}
		}
		logger.info("【牛牛】加载玩家可以押注筹码，房间号【"+vipTableID+"】，玩家昵称【"+pl.getPlayerName()
					+"】，玩家ID【"+pl.getPlayerIndex()+"】，筹码组【"+coinItems+"】，玩家当前金币【"+gold
					+"】，玩家平倍押注【"+pingBeiCoin+"】，玩家翻倍押注【"+fanBeiCoin+"】，可以押注组【"+onesCoinItems+"】");
		return onesCoinItems;
	}

	// 查找房主
	public User getCreator() {
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.getPlayerID().equals(this.creatorPlayerID) == true)
				return pl;
		}
		return null;
	}

	// 把玩家根据位置重新坐下
	public void re_position() {
		List<User> temp = new ArrayList<User>();
		//
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			temp.add(pl);
		}
		//
		players.clear();
		//
		for (int i = 0; i < temp.size(); i++) {
			User pl = temp.get(i);

			boolean added = false;
			for (int j = 0; j < players.size(); j++) {
				User plx = players.get(j);
				if (pl.getTablePos() < plx.getTablePos()) {
					added = true;
					players.add(j, pl);
					break;
				}
			}
			//
			if (added == false)
				players.add(pl);
		}
	}

	public void printfPosition() {
		Set<Integer> test = new HashSet<Integer>();
		if (this.state == GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE) {
			for (int i = 0; i < players.size(); i++) {
				User pl = players.get(i);
				if (test.contains(pl.getTablePos())) {
					// System.out.println("loop player index= ----------------state "+
					// this.state);
					logger.info("loop player index= ----------------state "
							+ this.state);
					test.add(pl.getTablePos());
				}
			}
		}
	}

	//移动到下一个玩家操作
//	public void move_to_next_player() {
//		//将当前操作玩家的 tablePos 转换为用户的 index
//		int currentUserIndex = CommonUtil.getInstance().tablePost2PlalyerIndex(this, this.game_room, currentOpertaionPlayerIndex);
//		//移动到下一个位置
//		currentUserIndex ++;
//		//最后一个用户的下一个玩家是第一个用户
//		if(currentUserIndex >= this.players.size()) {
//			currentUserIndex = 0;
//		}
//		//设置当前操作玩家
//		this.currentOpertaionPlayerIndex = players.get(currentUserIndex).getTablePos();
//	}
	//重置位置
	public void resetFreePos() {
		freePos.clear();
		for (int i = 0; i < playerCount; i++)
			freePos.add(i);
		// 随机打乱座位
		Collections.shuffle(freePos);
	}

	//
	public void addFreePos(int pos) {
		if (pos < 0 || pos > playerCount - 1)
			return;
		freePos.add(pos);
	}

	//
	public void enterTable(User pl) {
		if (this.isFull()) {
			return;
		}
		
		if (this.freePos.size() <= 0)
			return;

		//
		pl.setVipTableID("");
		pl.setRoomID(roomID);
		pl.setTableID(this.getTableID());

		pl.create_game_hander();

		int pos = this.freePos.remove(0);
		pl.setTablePos(pos);

		//
		pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
		//
		players.add(pl);
	}

	//
	public void clear_all() {
		// cc modify 清除玩家vipTableId
		for (int i = 0; i < players.size(); i++) {			
			User pl = players.get(i);
			if (this.proxyCreator!=null && pl.getTableID() != this.tableID){
				// 2017.9.19 玩家不在此房间里，则不清除
			}else{
				players.get(i).clearVipRoomID();
				players.get(i).setTableID("") ;
			}
		}
		players.clear();
		// 清空倍数
		gameMultiple = new GameMultiple();

		tableRule.clear();// STiV modify
		tableRuleOption.clear();

		state = GameConstant.TABLE_STATE_INVALID;
		//
		juCount = 0;
		sendClientWaitingBreakPlayerNotify = false;
		vipPswMd5 = "";
		// 创建者playerID
		creatorPlayerID = "";
		creatorPlayerName = "";
		// 代理creator
		proxyCreator = null;
		// vip房间id，方便查找
		vipTableID = 0;
		currentWinPlayerIndex = -1;
		dealerPos = 0;
		newWailtOtherPlayerTime = 0;
		prevApplyCloseVipRoom.clear();
		clearGameState();
		proxyState = 0;
		chuNotifyCount = 0;
		isStart = false;
		isStartGame = false;
		
		chooseSendNum1 = 0;
		chooseSendNum2 = 0;
		dealerCards = null;
		sendCardPoses = null;
		
		/**俱乐部编号*/
		clubCode = 0;
		/**俱乐部牌桌类型*/
		clubTableState = 0;
		/**对应的是那一个俱乐部的模板索引*/
		clubTemplateIndex = 0;
		quantityType = -1;
		roomLevel="1";
		dealerWinLoseTotal = 0;
		kouKaMap = new HashMap<Integer, Boolean>();
	}

	/**
	 * 清除牌局数据，进行下一局，玩家不清掉
	 * @author  2017-11-18
	 * GameTable.java
	 * void
	 */
	public void clearGameState() {

		playSubstate = 0;
		waitingStartTime = 0;
		//
		currentOpertaionPlayerIndex = dealerPos;
		sendClientWaitingBreakPlayerNotify = false;
		//
		singlePlayerTable = false;
		//
		cards.clear();
		pausedTime = 0L;
		waitingPlayerOperate = null;
		currentCards.clear();
		cardOpPlayerIndex = -1;

		//
		long ct = DateService.getCurrentUtilDate().getTime();
		newWailtOtherPlayerTime = ct;
		
		for (int i = players.size() - 1; i >= 0; i--) {
			User pl = players.get(i);
			pl.playerContinue_nndzz();
		}

		// 清空倍数
		gameMultiple = new GameMultiple();
		gameMultiple.basePoint = this.dizhu;
		// firstPos=-1;
		chuNotifyCount = 0;
		//
		currentRound = 0;
		//
		lastComparedUserTablePos = 0;
		isLastCPForBiPai = false;
		
		chooseSendNum1 = 0;
		chooseSendNum2 = 0;
		sendCardPoses = null;
		dealerCards = null;
	}

	public int getMaxPlayer() {
		return this.playerCount;
	}

	//
	public boolean isFull() {
		if (players.size() == getMaxPlayer())
			return true;

		return false;
	}
	/**
	 * 是否允许开始游戏
	 * @author  2017-11-18
	 * GameTable.java
	 * @return
	 * boolean
	 */
	public boolean isAgreeStart(){
		if(players.size() >= 2){
			return true;
		}
		return false;
	}

	// 未准备的玩家移掉
	public void remove_not_ready_player() {
		for (int i = players.size() - 1; i >= 0; i--) {
			User pl = players.get(i);
			// 如果玩家不是在准备状态，统统删掉
			if (pl.getGameState() != GameConstant.USER_GAME_STATE_IN_TABLE_READY) {
				addFreePos(pl.getTablePos());
				pl.clear_game_state();
				players.remove(i);
			}
		}
	}

	// 未准备的玩家移掉
	public void set_not_ready_player_ready() {
		this.set_all_player_state(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
	}

	// 查询桌子非机器人玩家的分数
	public int get_real_player_score() {
		int sc = 0;
		for (int i = players.size() - 1; i >= 0; i--) {
			User pl = players.get(i);
			if (pl.isRobot())
				continue;
			sc = pl.getGameScore();
			break;

		}
		//
		return sc;
	}

	// 强行清理掉所有玩家
	public void force_clear_player() {

		for (int i = players.size() - 1; i >= 0; i--) {
			User pl = players.get(i);
			if (pl.getTableID() != this.tableID){
				// 2017.9.19 玩家不在此房间里，则不清除
			}else{
				pl.clear_game_state();
				pl.setTableID("");
			}
		}
		//
		players.clear();
	}

	//

	//
	public int getReadyPlayerNum() {
		int num = 0;
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.getGameState() == GameConstant.USER_GAME_STATE_IN_TABLE_READY)
				num++;
		}
		//
		return num;
	}

	public void setAllRobotReady() {

		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.isRobot())
				pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
		}
	}

	//
	public int getRobotNum() {
		int num = 0;

		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.isRobot())
				num++;
		}
		//
		return num;
	}

	// 是否所有玩家都已经离开
	public boolean isAllPlayerLeft() {
		int num = 0;

		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.getGameState() == GameConstant.USER_GAME_STATE_IN_TABLE_PAUSED)
				num++;
		}
		//
		if (players.size() == num)
			return true;

		return false;
	}

	// 断线重连，都回来了没
	public boolean isAllPlayerBack() {
		int num = 0;

		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.getGameState() == GameConstant.USER_GAME_STATE_IN_TABLE_PLAYING)
				num++;
		}
		//
		if (players.size() == num)
			return true;

		return false;
	}

	//
	public boolean hasRobotPlayer() {
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.isRobot())
				return true;
		}
		//
		return false;
	}

	public boolean hasOfflinePlayer() {
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (!pl.isOnTable())
				return true;
		}
		//
		return false;
	}

	// 如果之前是暂停，现在如果都到齐了，继续打
	public boolean pauseContinue() {
		if (state == GameConstant.TABLE_STATE_PAUSED) {
			boolean all_backup = true;
			for (int i = 0; i < players.size(); i++) {
				User pl = players.get(i);
				if (pl.isRobot()) {
					if (cards.size() > 0)
						pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_PLAYING);
					else
						pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
				}
				if (pl.getGameState() == GameConstant.USER_GAME_STATE_IN_TABLE_PAUSED) {
					all_backup = false;
					break;
				}
			}
			//
			if (all_backup) {
				state = this.backup_state;

				this.setSendClientWaitingBreakPlayerNotify(false);

				return true;
			} else {
			}
		}

		return false;
	}

	//
	public void set_all_player_state(int ss) {
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			pl.setGameState(ss);
		}

	}

	//
	public User getPlayerAtIndex(int idx) {
		if ( idx < 0)
			return null;
		//
		for (int i = 0; i < players.size(); i++) {
			if (i == idx)
				return players.get(i);
		}
		return null;
	}
	
	public User getPlayerByTablePos(int pos){
		if ( pos < 0)
			return null;
		//
		for (int i = 0; i < players.size(); i++) {
			User user = players.get(i);
			if (user.getTablePos() == pos)
				return user;
		}
		return null;
	}

	//
	public boolean removePlayer(String playerID) {
		boolean found = false;
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.getPlayerID().equals(playerID)) {
				players.remove(i);
				found = true;
				break;
			}
		}
		//
		return found;
	}

	// 如果全是机器人的桌子，马上移除全部
	public void remove_robots_table() {
		//
		if (isAllRobot()) {
			players.clear();
		}
	}

	//
	public int getPlayerNum() {
		return players.size();
	}

	//
	public int findPlayerIndex(User ply) {
		int idx = -1;
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.getPlayerID().equals(ply.getPlayerID())) {
				idx = pl.getTablePos();

				if (idx >= 0
						&& (ply.getTablePos() != idx || ply
								.isNeedCopyGameState())) {
					logger.error("【牛牛】玩家:" + ply.getPlayerIndex()
							+ "断线重连，游戏数据异常，old_tablePos=" + idx + ",tablePos="
							+ ply.getTablePos() + ",old_vipTableGold="
							+ pl.getVipTableGold() + ", vipTableGold="
							+ ply.getVipTableGold());

					// 复制玩家的游戏数据
					ply.copy_game_state(pl);
					ply.setOnTable(true);
					ply.setOnline(true);
					ply.setNeedCopyGameState(false);

					// 替换桌子保存的玩家数据
					players.set(i, ply);
				}

				break;
			}
		}

		//
		return idx;
	}

	//
	public boolean isAllRobot() {
		boolean all_rb = true;
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.isRobot() == false) {
				all_rb = false;
				break;
			}
		}
		//
		return all_rb;
	}

	public boolean isOtherPlayerAllRobot(User plx) {
		boolean all_rb = true;
		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.getPlayerID().equals(plx.getPlayerID()))
				continue;
			//
			if (pl.isRobot() == false) {
				all_rb = false;
				break;
			}
		}
		//
		return all_rb;
	}

	// 去桌子上的所有玩家发消息
	public void sendMsgToTable(MsgBase msg) {
		if (msg == null)
			return;
		//

		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			GameContext.gameSocket.send(pl.getSession(), msg);
		}
	}

	// 去桌子上的所有玩家发消息,除了我
	public void sendMsgToTableExceptMe(MsgBase msg, String myPlayerID) {
		if (msg == null || myPlayerID == null)
			return;
		//

		for (int i = 0; i < players.size(); i++) {
			User pl = players.get(i);
			if (pl.getPlayerID().equals(myPlayerID)) {
				// System.out.print("send desk player name
				// :"+pl.getPlayerName()+"\n");
				continue;
			}
			// System.out.print("rececise desk player name
			// :"+pl.getPlayerName()+"\n");
			GameContext.gameSocket.send(pl.getSession(), msg);
		}
	}

	// 是否可以离开，不增加逃跑记录
	public boolean couldLeft(User pl) {
		// 创建者离开还要保留房间一段时间
		if (this.isVipTable()
				&& (this.getCreatorPlayerID() == pl.getPlayerID())) {
			return false;
		}

		if (pl.getReadyFlag() == 1) {
			return false;
		}

		// 如果除了这个要离开的玩家，其他人都是机器人，那可以离开
		if (this.isOtherPlayerAllRobot(pl)) {
			return true;
		}

		// 如果是vip房，一旦开始游戏，就不能离开
		if (this.isVipTable()
				&& this.state != GameConstant.TABLE_STATE_WAITING_PLAYER) {
			// 不是房主且未开局，房主离开了也可以离开
			if (this.state == GameConstant.TABLE_STATE_PAUSED
					&& backup_state == GameConstant.TABLE_STATE_WAITING_PLAYER) {
				return true;
			}
			return false;
		}

		//
		if (state == GameConstant.TABLE_STATE_INVALID
				|| state == GameConstant.TABLE_STATE_WAITING_PLAYER
				|| state == GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE) {
			return true;
		}
		//

		if (state == GameConstant.TABLE_STATE_PLAYING
				|| state == GameConstant.TABLE_STATE_SHOW_GAME_OVER_SCREEN
				|| state == GameConstant.TABLE_STATE_PAUSED)
			return false;

		//
		return false;
	}

	//
	public boolean couldAddPlayer() {
		// kn change
		if (players.size() >= playerCount)
			return false;
		//
		if (state == GameConstant.TABLE_STATE_INVALID)
			return true;
		if (state == GameConstant.TABLE_STATE_WAITING_PLAYER)
			return true;
		//

		if (state == GameConstant.TABLE_STATE_PLAYING)
			return false;
		if (state == GameConstant.TABLE_STATE_SHOW_GAME_OVER_SCREEN)
			return false;

		//
		return false;
	}

	//
	public boolean shouldRecycleTable() {
		if (this.isVipTable()) {
			// VIP房间就是没人，没开始也要保留一段是时间，game_table_tick中会结束房间的
			return false;
		}

		if (players.size() <= 0)
			return true;
		if (isAllRobot()) {
			return true;
		}
		//
		return false;
	}

	/**
	 * 是不是房主
	 * 
	 * @param pl
	 * @return
	 */
	public boolean isCreator(User pl) {
		User creator = getCreator();
		if (creator != null) {
			return creator.getPlayerIndex() == pl.getPlayerIndex();
		}
		return false;
	}

	public int getNextIndex(int idx) {
		idx++;
		if (idx >= getPlayerNum()) {
			idx = 0;
		}

		return idx;
	}

	public int getNextPlayerPos(int pos){
		if(players.size() == 1){
			return pos;
		}
		pos++;
		if (pos >= playerCount) {
			pos = 0;
		}
		User px = getPlayerByTablePos(pos);
		if(px == null){
			return getNextPlayerPos(pos);
		}
		return pos;
	}
	
	public void resetIsAgreeExtendRoom() {

		for (int i = 0; i < players.size(); i++) {
			User user = players.get(i);
			user.setChooseExtendCard(false);
			user.setAgreeExtendCard(false);
		}
	}

//	// 重新给玩家发续卡提示
//	public void re_extend_card_remind(User pl) {
//
//		ExtendCardResultMsg mxg = new ExtendCardResultMsg();
//		mxg.roomType = this.getRoomType();
//		mxg.waitTime = vip_game_over_waiting_start_game_time;
//		if (extendCardStartTime == 0) {
//			extendCardStartTime = System.currentTimeMillis();
//		}
//		// mxg.lastTime = vip_game_over_waiting_start_game_time -
//		// (int)(System.currentTimeMillis() - extendCardStartTime)/1000;
//		mxg.lastTime = Integer.parseInt(String
//				.valueOf(vip_game_over_waiting_start_game_time
//						- (System.currentTimeMillis() - extendCardStartTime)
//						/ 1000));
//		mxg.curQuanNum = this.getCurQuanTotal();
//		mxg.extendQuanNum = extendQuanNum;
//
//		User creator = getCreator();
//		int creatorPos = creator.getTablePos();
//		mxg.creator_pos = creatorPos;
//		mxg.creator_name = creator.getPlayerName();
//		mxg.creator_result = creator.isChooseExtendCard() ? (creator
//				.isAgreeExtendCard() ? 1 : 2) : 0;// 0、等待选择，1、同意，2、拒绝
//
//		int p1 = getNextIndex(++creatorPos);
//		User pl1 = getPlayerAtIndex(p1);
//		mxg.player1_pos = p1;
//		mxg.player1_name = pl1.getPlayerName();
//		mxg.player1_result = pl1.isChooseExtendCard() ? (pl1
//				.isAgreeExtendCard() ? 1 : 2) : 0;
//
//		if (roomType != GameConstant.ROOM_TYPE_COUPLE) {
//			int p2 = getNextIndex(++p1);
//			User pl2 = getPlayerAtIndex(p2);
//			mxg.player2_pos = p2;
//			mxg.player2_name = pl2.getPlayerName();
//			mxg.player2_result = pl2.isChooseExtendCard() ? (pl2
//					.isAgreeExtendCard() ? 1 : 2) : 0;
//		}
//
//		// 有没有不同意的，有就直接解散
//		boolean hasUnChoose = false;
//		boolean hasUnAgree = false;
//		List<User> players = getPlayers();
//		for (User plx : players) {
//			if (!plx.isChooseExtendCard()) {
//				hasUnChoose = true;
//			}
//			if (!plx.isAgreeExtendCard()) {
//				hasUnAgree = true;
//			}
//		}
//		// 给桌上玩家发结果，0、没选完，1、有不同意的，解散房间，2、续卡
//		mxg.extendResult = hasUnAgree ? 1 : (hasUnChoose ? 0 : 2);
//		// for(User plx : players){
//		mxg.tablePos = pl.getTablePos();
//
//		GameContext.gameSocket.send(pl.getSession(), mxg);
//		// }
//	}

	/***
	 * 换座  2017.8.2
	 * 
	 * ****/
	public boolean change_site(int oldSite, int newSite, User pl) {
		// 桌子已满不能换坐
		if (this.isFull()) {
			return false;
		}

		if (this.freePos.size() <= 0)
			return false;

		// 开始换坐
		for (int i = 0; i < this.freePos.size(); i++) {
			if (this.freePos.get(i) == newSite) {

				this.freePos.remove(i);
				addFreePos(oldSite);
				pl.setTablePos(newSite);

				return true;
			}
		}
		return false;
	}

	public boolean isHavePlayer(int index) {
		if (this.players != null) {
			for (User pl : this.players) {
				if (pl.getPlayerIndex() == index) {
					return true;
				}
			}
		}
		return false;
	}

	// STiV modify
	public void setTableRuleOption(List<Integer> tableRule) {
		debugLog("=== setTableRuleOption :" + tableRule);

		this.tableRule.clear();
		if (tableRule != null) {
			this.tableRuleOption = tableRule;
			for (int rule : tableRule) {
				// if (!this._TableRule.contains(rule))
				this.tableRule.add(rule);
			}
		} else {
			this.tableRuleOption = new ArrayList<Integer>();
		}
	}

	// 获取是否同意解散房间的状态  2017.8.7
	public PlayerAskCloseVipRoomMsgAck playerIsAgreeCloseRoom(
			PlayerAskCloseVipRoomMsgAck ackAgree) {
		//四人房间、三人
		int  temp = 1;
		for (int i = 0; i < players.size(); i++) {
			User user = players.get(i);
			if (!user.isApplyCloseRoom()){
				if (temp == 1 ) {
					ackAgree.player2_pos = user.getTablePos();
					ackAgree.player2_name = user.getPlayerName();
					ackAgree.player2_result = user.isAgreeCloseRoom();
					ackAgree.player2_playerId = user.getPlayerID();
				}else if (temp == 2 ) {
					ackAgree.player3_pos = user.getTablePos();
					ackAgree.player3_name = user.getPlayerName();
					ackAgree.player3_result = user.isAgreeCloseRoom();
					ackAgree.player3_playerId = user.getPlayerID();
				}else if (temp == 3 ) {
					ackAgree.player4_pos = user.getTablePos();
					ackAgree.player4_name = user.getPlayerName();
					ackAgree.player4_result = user.isAgreeCloseRoom();
					ackAgree.player4_playerId = user.getPlayerID();
				}
				else if (temp == 4 ) {
					ackAgree.player5_pos = user.getTablePos();
					ackAgree.player5_name = user.getPlayerName();
					ackAgree.player5_result = user.isAgreeCloseRoom();
					ackAgree.player5_playerId = user.getPlayerID();
				}
				else if (temp == 5 ) {
					ackAgree.player6_pos = user.getTablePos();
					ackAgree.player6_name = user.getPlayerName();
					ackAgree.player6_result = user.isAgreeCloseRoom();
					ackAgree.player6_playerId = user.getPlayerID();
				}
				
				temp =temp + 1 ;
			}
			if (user.isApplyCloseRoom()){
				ackAgree.askerName = user.getPlayerName();
				ackAgree.player1_pos = user.getTablePos();
				ackAgree.player1_name = user.getPlayerName();
				ackAgree.player1_result = user.isAgreeCloseRoom();
				ackAgree.player1_playerId = user.getPlayerID();
				
			}
		}

		return ackAgree;
	}

	/***
	 * 检测房间是否可以解散的方法抽出；返回值0：等待结果，1：结束解散，2：继续游戏 * 2017.8.7, 修改为动态获取人数
	 ***/
	public int checkIsClose(GameTable gt, User user) {
		int maxPlayer = this.getPlayers().size();
		int halfPlayer = (int) Math.ceil(maxPlayer*1.0 / 2);
		// 三人vip房间
		if(maxPlayer == 1){
			logger.error(maxPlayer + "人房间，【牛牛】自己申请解散，解散VIP房间:" + gt.getVipTableID());
			return 1;
		}else if ((maxPlayer ==2 && gt.getCloseVipRoomNum() >= maxPlayer - 1) ||(maxPlayer > 2 && gt.getCloseVipRoomNum() + 1 >= halfPlayer)) {
			logger.error(maxPlayer + "人房间，【牛牛】同意人数达到" + halfPlayer
					+ "人或以上，解散VIP房间:" + gt.getVipTableID());
			return 1;
		} else if (gt.getDisAgreeCloseVipRoomNum() >= halfPlayer) {
			logger.error(maxPlayer + "人房间，【牛牛】拒绝人数达到" + halfPlayer
					+ "人或以上，继续VIP房间:" + gt.getVipTableID());
			return 2;
		} else {
			// 拒绝人数和同意人数均未过半，等待·······
			return 0;
		}
	}

	public void resetIsAgreeCloseRoom() {
		for (int i = 0; i < players.size(); i++) {
			User user = players.get(i);
			user.setApplyCloseRoom(false);

			user.setAgreeCloseRoom(0);
		}
	}

	public int getSendCardTime(int baseTime, int singleTime){
		int count = this.getStakeCoinPlayers().size();
		return baseTime + (count+1) * singleTime;
	}
	
	// ===================================================================

	public String getTableID() {
		return tableID;
	}

	public void setTableID(String tableID) {
		this.tableID = tableID;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public List<User> getPlayers() {
		return players;
	}

	public void setPlayers(List<User> players) {
		this.players = players;
	}

	//

	public int getRoomID() {
		return roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	public boolean isSinglePlayerTable() {
		return singlePlayerTable;
	}

	public void setSinglePlayerTable(boolean singlePlayerTable) {
		this.singlePlayerTable = singlePlayerTable;
	}

	public long getPausedTime() {
		return pausedTime;
	}

	public void setPausedTime(long pausedTime) {
		this.pausedTime = pausedTime;
	}

	public int getDealerPos() {
		return dealerPos;
	}
	
	public int getRealDealerPos(){
		User dealer = getPlayerByTablePos(dealerPos);
		if(dealer == null){
			return -1;
		}
		if(dealer.getUserGameHanderForNNDZZ().isChooseZhuang()){
			return dealerPos;
		}
		return -1;
	}

	public void setDealerPos(int dealerPos) {
		this.dealerPos = dealerPos;
	}

	public int getCurrentOpertaionPlayerIndex() {
		return currentOpertaionPlayerIndex;
	}

	public void setCurrentOpertaionPlayerIndex(int currentOpertaionPlayerIndex) {
		this.currentOpertaionPlayerIndex = currentOpertaionPlayerIndex;
	}

	public long getHandStartTime() {
		return handStartTime;
	}

	public void setHandStartTime(long handStartTime) {
		this.handStartTime = handStartTime;
	}

	public long getHandEndTime() {
		return handEndTime;
	}

	public void setHandEndTime(long handEndTime) {
		this.handEndTime = handEndTime;
	}

	public long getReadyTime() {
		return readyTime;
	}

	public void setReadyTime(long readyTime) {
		this.readyTime = readyTime;
	}

	public List<Byte> getCurrentCards() {
		return currentCards;
	}

	public void setCurrentCards(List<Byte> currentCards) {
		this.currentCards = currentCards;
	}

	public MahjongCheckResult getWaitingPlayerOperate() {
		return waitingPlayerOperate;
	}

	public void setWaitingPlayerOperate(MahjongCheckResult waitingPlayerOperate) {
		this.waitingPlayerOperate = waitingPlayerOperate;
	}

	public int getPlaySubstate() {
		return playSubstate;
	}

	public void setPlaySubstate(int playSubstate) {
		this.playSubstate = playSubstate;
	}

	public long getWaitingStartTime() {
		return waitingStartTime;
	}

	public void setWaitingStartTime(long waitingStartTime) {
		this.waitingStartTime = waitingStartTime;
	}

	public String getVipPswMd5() {
		return vipPswMd5;
	}

	public void setVipPswMd5(String vipPswMd5) {
		this.vipPswMd5 = vipPswMd5;
	}

	public String getCreatorPlayerID() {
		return creatorPlayerID;
	}

	public void setCreatorPlayerID(String creatorPlayerID) {
		this.creatorPlayerID = creatorPlayerID;
	}

	public int getVipTableID() {
		return vipTableID;
	}

	public void setVipTableID(int vipTableID) {
		this.vipTableID = vipTableID;
	}

	public String getCreatorPlayerName() {
		return creatorPlayerName;
	}

	public void setCreatorPlayerName(String creatorPlayerName) {
		this.creatorPlayerName = creatorPlayerName;
	}

	public int getDizhu() {
		return dizhu;
	}

	public void setDizhu(int dizhu) {
		this.dizhu = dizhu;
		gameMultiple.basePoint = dizhu;
	}

//	public int getHandNum() {
//		return handNum;
//	}
//
//	public void setHandNum(int handNum) {
//		this.handNum = handNum;
//	}

	@SuppressWarnings("unused")
	private void add_to(List<Byte> blist, byte b) {
		boolean inserted = false;
		for (int j = 0; j < blist.size(); j++) {
			byte bb = blist.get(j);
			if (b < bb) {
				blist.add(j, b);
				inserted = true;
				break;
			}
		}
		//
		if (inserted == false) {
			blist.add(b);
		}
	}

	public int getCurrentWinPlayerIndex() {
		return currentWinPlayerIndex;
	}

	public void setCurrentWinPlayerIndex(int currentWinPlayerIndex) {
		this.currentWinPlayerIndex = currentWinPlayerIndex;
	}

	public long getNewWailtOtherPlayerTime() {
		return newWailtOtherPlayerTime;
	}

	public void setNewWailtOtherPlayerTime(long newWailtOtherPlayerTime) {
		this.newWailtOtherPlayerTime = newWailtOtherPlayerTime;
	}

	public Date getGameStartTime() {
		return gameStartTime;
	}

	public void setGameStartTime(Date gameStartTime) {
		this.gameStartTime = gameStartTime;
	}

	public Date getGameEndTime() {
		return gameEndTime;
	}

	public void setGameEndTime(Date gameEndTime) {
		this.gameEndTime = gameEndTime;
	}

	public int getRoomType() {
		return roomType;
	}

	public void setRoomType(int roomType) {
		this.roomType = roomType;
	}

	public int getItemBaseID() {
		return itemBaseID;
	}

	public void setItemBaseID(int itemBaseID) {
		this.itemBaseID = itemBaseID;
	}

	public int getBackup_state() {
		return backup_state;
	}

	public void setBackup_state(int backup_state) {
		this.backup_state = backup_state;
	}

	public boolean isSendClientWaitingBreakPlayerNotify() {
		return sendClientWaitingBreakPlayerNotify;
	}

	public void setSendClientWaitingBreakPlayerNotify(
			boolean sendClientWaitingBreakPlayerNotify) {
		this.sendClientWaitingBreakPlayerNotify = sendClientWaitingBreakPlayerNotify;
	}

	public long getVipCreateTime() {
		return vipCreateTime;
	}

	public void setVipCreateTime(long vipCreateTime) {
		this.vipCreateTime = vipCreateTime;
	}

	public boolean isPayVipKa() {
		return isPayVipKa;
	}

	public void setPayVipKa(boolean isPayVipKa) {
		this.isPayVipKa = isPayVipKa;
	}

	public int getTickError() {
		return tickError;
	}

	public void addTickError() {
		this.tickError++;
	}

	public void reSetTickError() {
		this.tickError = 0;
	}

	public int getCloseVipRoomNum() {
		return closeVipRoomNum;
	}

	public void setCloseVipRoomNum(int closeVipRoomNum) {
		this.closeVipRoomNum = closeVipRoomNum;
	}
	public int getDisAgreeCloseVipRoomNum() {
		return disAgreeCloseVipRoomNum;
	}
	public void setDisAgreeCloseVipRoomNum(int disAgreeCloseVipRoomNum) {
		this.disAgreeCloseVipRoomNum = disAgreeCloseVipRoomNum;
	}
	public List<Integer> getTableRule() {
		return this.tableRule;
	}
	public List<Integer> getTableRuleOption() {
		return this.tableRuleOption;
	}

	public Map<String, Long> getPrevApplyCloseVipRoom() {
		return prevApplyCloseVipRoom;
	}

	public void setPrevApplyCloseVipRoom(Map<String, Long> prevApplyCloseVipRoom) {
		this.prevApplyCloseVipRoom = prevApplyCloseVipRoom;
	}

	public void playerPrevApplyCloseVipRoom(String playerID, long time) {
		prevApplyCloseVipRoom.put(playerID, time);
	}

	public void clearPrevApplyCloseVipRoom() {
		prevApplyCloseVipRoom.clear();
	}

	public Long getPlayerPrevApplyCloseVipRoomTime(String playerID) {
		return prevApplyCloseVipRoom.get(playerID);
	}

	void debugLog(String log) {
		logger.info(log);
	}

	void traceLog(String log) {
		if (this.game_room.roomType == GameConstant.ROOM_TYPE_COUPLE)
			debugLog("***" + GameConstant.LOG_TAG + "_s3:" + tableID + " "
					+ log);
		else
			debugLog("***" + GameConstant.LOG_TAG + ":" + tableID + " " + log);
	}

	public int getPlayerCount() {
		return players.size();
	}

	public List<User> getStakeCoinPlayers(){
		List<User> pls = new ArrayList<User>(players.size());
		for(User pl : players){
			UserGameHander hander = pl.getUserGameHanderForNNDZZ();
			if(hander.isStakeCoin()){
				pls.add(pl);
			}
		}
		return pls;
	}
	
	public boolean isAllPlayersPingBei(){
		boolean isAll = true;
		for(User pl : players){
			UserGameHander hander = pl.getUserGameHanderForNNDZZ();
			if(hander.getPingBeiCoin() == -1){
				isAll = false;
			}
		}
		return isAll;
	}
	
	public boolean isAllPlayersFanBei(){
		boolean isAll = true;
		for(User pl : players){
			UserGameHander hander = pl.getUserGameHanderForNNDZZ();
			if(hander.getFanBeiCoin() == -1){
				isAll = false;
			}
		}
		return isAll;
	}
	
	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}
	
	public int getCardOpPlayerIndex() {
		return cardOpPlayerIndex;
	}

	public void setCardOpPlayerIndex(int cardOpPlayerIndex) {
		this.cardOpPlayerIndex = cardOpPlayerIndex;
	}

	public GameMultiple getGameMultiple() {
		return gameMultiple;
	}

	public void setGameMultiple(GameMultiple gameMultiple) {
		this.gameMultiple = gameMultiple;
	}

	public void addBombCount() {
		gameMultiple.addBombCount();
	}

	public void setPlayerCallDouble(User pl, int doubleResult) {
		gameMultiple.doubleMultiples.set(pl.getTablePos(), doubleResult);
	}

	public List<Byte> getCards() {
		return cards;
	}

	public String getTableEndWay() {
		return tableEndWay;
	}

	public void setTableEndWay(String tableEndWay) {
		this.tableEndWay = tableEndWay;
	}

	public void setApplyCloseStartTime(long applyCloseStartTime) {
		this.applyCloseStartTime = applyCloseStartTime;
	}

	public long getApplyCloseStartTime() {
		return applyCloseStartTime;
	}

	public void setPrevApplyCloseVipRoomTime(long prevApplyCloseVipRoomTime) {
		PrevApplyCloseVipRoomTime = prevApplyCloseVipRoomTime;
	}

	public long getPrevApplyCloseVipRoomTime() {
		return PrevApplyCloseVipRoomTime;
	}


	public int getIsRecordTable() {
		return isRecordTable;
	}

	public void setIsRecordTable(int p) {
		isRecordTable = p;
	}

	public User getProxyCreator() {
		return proxyCreator;
	}

	public void setProxyCreator(User proxyCreator) {
		this.proxyCreator = proxyCreator;
	}

	public int getRoomModel() {
		return roomModel;
	}

	public void setRoomModel(int roomModel) {
		this.roomModel = roomModel;
	}
	
	public int getCostKaNum(){
		GameConfig gameConfig = GameContext.tableLogic_nndzz.getGameConfig();
		
		Map<String, Integer> mapKa = gameConfig.getPokerClubWanfa(roomModel);
		logger.info("==========>扣卡配置："+mapKa);
		int kaKouNum = mapKa.get("cost_self_"+this.roomLevel);
		if (proxyState == 2) {
			kaKouNum = mapKa.get("cost_aa_"+this.roomLevel);
		} else if (proxyState == 1) {
			kaKouNum = mapKa.get("cost_proxy_"+this.roomLevel);
		}
		return kaKouNum * (players.size()+1);
	}

	public int getIsSelectSite() {
		return isSelectSite;
	}

	public void setIsSelectSite(int isSelectSite) {
		this.isSelectSite = isSelectSite;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public int getProxyState() {
		return proxyState;
	}

	public void setProxyState(int proxyState) {
		this.proxyState = proxyState;
	}

	public void setChuNotifyCount(int chuNotifyCount) {
		this.chuNotifyCount = chuNotifyCount;
	}

	public int getChuNotifyCount() {
		return chuNotifyCount;
	}
	public boolean isSingleRoundOver() {
		return isSingleRoundOver;
	}

	public void setSingleRoundOver(boolean isSingleRoundOver) {
		this.isSingleRoundOver = isSingleRoundOver;
	}

	public boolean isProxyForceClose() {
		return isProxyForceClose;
	}

	public void setProxyForceClose(boolean isProxyForceClose) {
		this.isProxyForceClose = isProxyForceClose;
	}

	public boolean isStart() {
		return isStart;
	}

	public void setStart(boolean isStart) {
		this.isStart = isStart;
	}

	public int getJuCount() {
		return juCount;
	}

	public void setJuCount(int juCount) {
		this.juCount = juCount;
	}

	public int getTotalJuNum() {
		return totalJuNum;
	}

	public void setTotalJuNum(int totalJuNum) {
		this.totalJuNum = totalJuNum;
	}

	public int getCoinBaseNum() {
		return coinBaseNum;
	}

	public void setCoinNum(int coinBaseNum) {
		this.coinBaseNum = coinBaseNum;
	}
	public int getCurrentRound() {
		return currentRound;
	}
	public void setCurrentRound(int currentRound) {
		this.currentRound = currentRound;
	}

	public int getLastComparedUserTablePos() {
		return lastComparedUserTablePos;
	}

	public void setLastComparedUserTablePos(int lastComparedUserTablePos) {
		this.lastComparedUserTablePos = lastComparedUserTablePos;
	}

	public boolean isLastCPForBiPai() {
		return isLastCPForBiPai;
	}

	public void setLastCPForBiPai(boolean isLastCPForBiPai) {
		this.isLastCPForBiPai = isLastCPForBiPai;
	}

	public boolean isStartGame() {
		return isStartGame;
	}

	public void setStartGame(boolean isStartGame) {
		this.isStartGame = isStartGame;
	}

	public int getChooseSendNum1() {
		return chooseSendNum1;
	}

	public void setChooseSendNum1(int chooseSendNum1) {
		this.chooseSendNum1 = chooseSendNum1;
	}

	public int getChooseSendNum2() {
		return chooseSendNum2;
	}

	public void setChooseSendNum2(int chooseSendNum2) {
		this.chooseSendNum2 = chooseSendNum2;
	}

	public List<Integer> getSendCardPoses() {
		return sendCardPoses;
	}

	public void setSendCardPoses(List<Integer> sendCardPoses) {
		this.sendCardPoses = sendCardPoses;
	}

	public List<Byte> getDealerCards() {
		return dealerCards;
	}

	public void setDealerCards(List<Byte> dealerCards) {
		this.dealerCards = dealerCards;
	}

	public int getClubCode() {
		return clubCode;
	}

	public void setClubCode(int clubCode) {
		this.clubCode = clubCode;
	}

	public int getClubTableState() {
		return clubTableState;
	}

	public void setClubTableState(int clubTableState) {
		this.clubTableState = clubTableState;
	}
	public void setQuantityType(int quantityType) {
		this.quantityType = quantityType;
	}
	public int getQuantityRawType(){
		return this.quantityType;
	}
//	获取房间类型，局或者圈
	public int getQuantityType(){
//		try{
//			Map<String, Integer> mapWanfa = getGameController().getConfigGame().getVipInfo(this.getRoomType());
//			int qType = mapWanfa.get("unit");
//			return qType;
//		}catch(Exception e){
//			logger.error(e.getMessage(), e);
//		}
		return quantityType;
	}
	public int getClubTemplateIndex() {
		return clubTemplateIndex;
	}
	public void setClubTemplateIndex(int clubTemplateIndex) {
		this.clubTemplateIndex = clubTemplateIndex;
	}
	
	public String getRoomLevel() {
		return roomLevel;
	}

	public void setRoomLevel(String roomLevel) {
		this.roomLevel = roomLevel;
	}

	public int getDealerWinLoseTotal() {
		return dealerWinLoseTotal;
	}

	public void setDealerWinLoseTotal(int dealerWinLoseTotal) {
		this.dealerWinLoseTotal = dealerWinLoseTotal;
	}

	public Map<Integer, Boolean> getKouKaMap() {
		return kouKaMap;
	}

	public void setKouKaMap(Map<Integer, Boolean> kouKaMap) {
		this.kouKaMap = kouKaMap;
	}
	
}
