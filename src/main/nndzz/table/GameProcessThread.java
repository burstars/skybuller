package com.chess.nndzz.table;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.UUIDGenerator;
import com.chess.common.bean.GameConfig;
import com.chess.common.bean.MahjongCheckResult;
import com.chess.common.bean.NormalGameRecord;
import com.chess.common.bean.ProxyVipRoomRecord;
import com.chess.common.bean.RecordBean;
import com.chess.common.bean.TClub;
import com.chess.common.bean.TClubLevel;
import com.chess.common.bean.User;
import com.chess.common.bean.VipRoomRecord;
import com.chess.common.constant.ConfigConstant;
import com.chess.common.constant.ErrorCodeConstant;
import com.chess.common.constant.GameConstant;
import com.chess.common.constant.LogConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.command.MsgCmdConstant;
import com.chess.common.msg.struct.other.EnterVipRoomMsgAck;
import com.chess.common.msg.struct.other.PlayerOpertaionMsg;
import com.chess.common.msg.struct.other.ProxyVipRoomRecordMsg;
import com.chess.common.msg.struct.playeropt.ValidateMsgAck;
import com.chess.common.service.ICacheGameConfigService;
import com.chess.common.service.IClubService;
import com.chess.common.service.IRecordService;
import com.chess.common.service.ISystemConfigService;
import com.chess.common.service.impl.CacheUserService;
import com.chess.nndzz.bean.UserGameHander;
import com.chess.nndzz.msg.struct.other.PlayerGameOpertaionAckMsg;
import com.chess.nndzz.msg.struct.other.PlayerGameOverMsgAck;
import com.chess.nndzz.msg.struct.other.PlayerOperationNotifyMsg;
import com.chess.nndzz.msg.struct.other.PlayerTableOperationMsg;
import com.chess.nndzz.msg.struct.other.RequestStartGameMsgAck;
import com.chess.nndzz.msg.struct.other.VipRoomCloseMsg;
import com.chess.nndzz.util.CardUtil;

/**
 * 游戏处理线程
 */
public class GameProcessThread extends Thread {
	private static Logger logger = LoggerFactory.getLogger(TableLogicProcess.class);
	private Map<String, GameTable> playing_table_map = new ConcurrentHashMap<String, GameTable>();

	private ConcurrentLinkedQueue<TableMessage> msg_queue = new ConcurrentLinkedQueue<TableMessage>();

	private CacheUserService playerService = null;
	private ISystemConfigService cfgService = null;
	private IRecordService recordService = null;

	// 本条线程可以处理游戏桌子的数量
	private int table_capacity = 0;

	public boolean keep_running = true;

	private int vip_room_create_wait_start = 0;
	private int vip_paused_exist_time = 0;
	private int vip_game_over_waiting_start_game_time = 0;
	private int vip_game_over_enter_next_turn = 0;
	private int operation_over_time = 0;

	public GameProcessThread(CacheUserService ps, int capacity) {
		this.playerService = ps;
		// 承载桌子数量
		table_capacity = capacity;

	}

	public void init() {
		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		if (cfgService == null) {
			return;
		}
		if (recordService == null)
			recordService = (IRecordService) SpringService
					.getBean("recordService");
		if (recordService == null) {
			return;
		}
		if (vip_room_create_wait_start == 0) {
			vip_room_create_wait_start = cfgService.getPara(
					ConfigConstant.VIP_ROOM_CREATE_WAIT_START).getValueInt();
		}
		if (vip_paused_exist_time == 0) {
			vip_paused_exist_time = cfgService.getPara(
					ConfigConstant.VIP_PAUSED_EXIST_TIME).getValueInt();
		}
		if (vip_game_over_waiting_start_game_time == 0) {
			vip_game_over_waiting_start_game_time = cfgService.getPara(
					ConfigConstant.VIP_GAME_OVER_WAITING_START_GAME_TIME)
					.getValueInt();
		}
		if (vip_game_over_enter_next_turn == 0) {
			vip_game_over_enter_next_turn = cfgService.getPara(
					ConfigConstant.VIP_GAME_OVER_ENTER_NEXT_TURN).getValueInt();
		}
		if (operation_over_time == 0) {
			operation_over_time = cfgService.getPara(
					ConfigConstant.OPERATION_OVER_TIME).getValueInt();
		}

	}

	public void pushMsg(TableMessage tm) {
		msg_queue.offer(tm);
	}

	public int getTableCount() {
		return playing_table_map.size();
	}

	public int getTableCapacity() {
		return table_capacity;
	}

	public GameTable getTable(String table_id) {
		return playing_table_map.get(table_id);
	}

	public int addTable(GameTable gt) {
		if (playing_table_map.get(gt.getTableID()) == null) {
			playing_table_map.put(gt.getTableID(), gt);
		}
		return playing_table_map.size();
	}

	public int delTable(GameTable gt) {
		playing_table_map.remove(gt.getTableID());
		return playing_table_map.size();
	}
	/**
	 * vip房间开始游戏创建log记录
	 * GameProcessThread.java
	 * @param gt
	 * void
	 */
	private void vipTableLogCreate(GameTable gt) {
		Date ct = DateService.getCurrentUtilDate();
		List<User> plist = gt.getPlayers();
		boolean isClub=gt.getClubCode() != 0;//判断是否是俱乐部内牌桌
		
		// 打第一把，创建下非房主的房间记录，开一次房，3个人，3条记录
		if (gt.isVipTable() && gt.getJuCount() == 1) {
			gt.setGameStartTime(ct);
			
			if(gt.getClubCode() > 0){
	        	gt.setClubTableState(3);//俱乐部的私密桌子
	        	//TODO 发消息通知前台俱乐部的所有人，当前俱乐部各种牌桌状态
				playerService.sendMsgToClubPlayer(gt.getClubCode(), playerService.newGetSingleClubInfoMsgAck(gt.getClubCode()));
	        }
			
			//
			for (int i = 0; i < plist.size(); i++) {
				User pl = plist.get(i);
				{
					// 创建一条vip房间记录
					VipRoomRecord vrr = new VipRoomRecord();
					vrr.setStartTime(ct);
					vrr.setRoomID(gt.getTableID());
					vrr.setRoomIndex(gt.getVipTableID());
					vrr.setPlayer1ID(pl.getPlayerID());
					vrr.setHostName(gt.getCreatorPlayerName());
					vrr.setTableNamePrefix(pl.getGameId());
					
					if(isClub){//判断是否是俱乐部内牌桌
						setTableProperties(vrr,gt);
					}
					//
					int num = 0;
					for (int j = 0; j < plist.size(); j++) {
						User plj = plist.get(j);
						if (plj.getPlayerID().equals(pl.getPlayerID()) == false) {
							if (num == 0) {
								vrr.setPlayer2Name(plj.getPlayerName());
								vrr.setScore2(plj.getWinLoseTotal());
								
								if(isClub){//判断是否是俱乐部内牌桌
									vrr.setPlayer2ID(plj.getPlayerID());
									vrr.setPlayer2Index(plj.getPlayerIndex());
								}
								
							} else if (num == 1) {
								vrr.setPlayer3Name(plj.getPlayerName());
								vrr.setScore3(plj.getWinLoseTotal());
								
								if(isClub){//判断是否是俱乐部内牌桌
									vrr.setPlayer3ID(plj.getPlayerID());
									vrr.setPlayer3Index(plj.getPlayerIndex());
								}
								
							} else if (num == 2) {
								vrr.setPlayer4Name(plj.getPlayerName());
								vrr.setScore4(plj.getWinLoseTotal());
								
								if(isClub){//判断是否是俱乐部内牌桌
									vrr.setPlayer4ID(plj.getPlayerID());
									vrr.setPlayer4Index(plj.getPlayerIndex());
								}
								
							} else if (num == 3) {
								vrr.setPlayer5Name(plj.getPlayerName());
								vrr.setScore5(plj.getWinLoseTotal());
								
								if(isClub){//判断是否是俱乐部内牌桌
									vrr.setPlayer5ID(plj.getPlayerID());
									vrr.setPlayer5Index(plj.getPlayerIndex());
								}
							}else if (num == 4) {
								vrr.setPlayer6Name(plj.getPlayerName());
								vrr.setScore6(plj.getWinLoseTotal());
								
								if(isClub){//判断是否是俱乐部内牌桌
									vrr.setPlayer6ID(plj.getPlayerID());
									vrr.setPlayer6Index(plj.getPlayerIndex());
								}
							}
							//
							num++;
						}else{
							vrr.setPlayer1Name(pl.getPlayerName());
							vrr.setPlayer1ID(pl.getPlayerID());
							vrr.setPlayer1Index(pl.getPlayerIndex());
							vrr.setScore1(pl.getWinLoseTotal());
						}
					}
					
					if(isClub){//判断是否是俱乐部内牌桌
						// 庄
						vrr.setPlayer6Name(gt.getCreatorPlayerName());
						vrr.setScore6(0);
						vrr.setPlayer6ID(gt.getCreatorPlayerID());
					}
					
					//
					vrr.setRecordID(UUIDGenerator.generatorUUID());
					//
					this.playerService.createVipRoomRecord(vrr);
					//
					pl.setVrr(vrr);
				}
			}
		}
	}
	
	//	设置牌桌基本属性（玩家数量，耗卡数，耗卡类型，局数，所选规则）
	protected void setTableProperties(VipRoomRecord record,GameTable gameTable){
		record.setClubCode(gameTable.getClubCode());
		record.setPlayerCount(gameTable.getPlayerCount());
		record.setPayCount(gameTable.getCostKaNum());
		record.setQuantity(gameTable.getTotalJuNum());
		record.setQuantityType(gameTable.getQuantityType());
		record.setRules("");
	}
	
	/**
	 * 每局扣服务费
	 * @author  2017-11-15
	 * GameProcessThread.java
	 * @param gt
	 * @param gr
	 * void
	 */
	private void serviceFee(GameTable gt, GameRoom gr) {
		if (gr == null || gr.serviceFee <= 0)
			return;
		//
		List<User> plist = gt.getPlayers();
		for (int i = 0; i < plist.size(); i++) {
			User pl = plist.get(i);
			if (pl.getGold() > gr.serviceFee) {
				String remark = "服务费=" + gr.serviceFee;
				this.playerService.sub_player_gold(pl, gr.serviceFee,
						LogConstant.OPERATION_TYPE_SUB_GOLD_GAME_SERVICE_FEE,
						remark);
			}
		}
	}

	/**
	 * 检测是否玩家输光，第一局玩家扣金币
	 * @author  2017-11-15
	 * GameProcessThread.java
	 * @param gt
	 * @param gr
	 * @return
	 * boolean
	 */
	private boolean vipTableCheck(GameTable gt, GameRoom gr) {
		List<User> plist = gt.getPlayers();
		// vip房间第一把牌，在这个时候把玩家的携带进来的金币扣一下，如果金币不足，游戏开始失败
		if (gt.isVipTable() && gt.getJuCount() == 0) {
			if (gr == null) {
				gt.setState(GameConstant.TABLE_STATE_INVALID);
				return false;
			}
			// 先测试金币够不够
			for (int i = 0; i < plist.size(); i++) {
				User pl = plist.get(i);
				if (pl.getGold() < gr.fixedGold) {
					gt.setState(GameConstant.TABLE_STATE_INVALID);
					return false;
				}
			}
			// 再扣金币
			for (int i = 0; i < plist.size(); i++) {
				User pl = plist.get(i);
				this.playerService.sub_player_gold(pl, gr.fixedGold,
						LogConstant.OPERATION_TYPE_SUB_GOLD_TAKE_TO_GAME_TABLE,
						"vip金币带上桌=" + gr.fixedGold);
				// 金币带到桌子上
//				pl.setVipTableGold(gr.fixedGold);
				// 输赢数据清零
				pl.setWinLoseTotal(0);
				pl.setWinLoseGoldNum(0);
			}
		}
		return true;
	}

	private void gameStart(GameTable gt, GameRoom gr) {
		if(gt.isVipTableTimeOver()){//处理牌局结束后，都断线，然后都上线，可以自动开牌问题
			not_extend_vip_table(gt, gr, false);
			return;
		}
		List<User> plist = gt.getPlayers();
		for (int i = 0; i < plist.size(); i++) {
			User pl = plist.get(i);
			pl.setAutoOperation(0);
			if (null != pl) {
				pl.setOnTable(true);
			}
		}

		// vip桌子校验
		if (gt.isVipTable()) {
			if (vipTableCheck(gt, gr) == false)
				return;
		}

		Date ct = DateService.getCurrentUtilDate();
		long ctt = ct.getTime();
		// 把玩家按座位排序
		gt.re_position();
		// 先洗牌
		gt.washCards();

		// 收台费
		serviceFee(gt, gr);

		// 局数增加
		gt.setJuCount(gt.getJuCount() + 1);
		
		if(gt.getClubCode() > 0){
			
		}else{
			int pos = 0;
			User px = null;
			if(gt.getJuCount() <= 1){
				// 第一把房主坐庄
				px = gt.getCreator();
				pos = px.getTablePos();
			}else{
				pos = gt.getNextPlayerPos(gt.getDealerPos());
				px = gt.getPlayerByTablePos(pos);
			}
			
			gt.setCardOpPlayerIndex(pos);
			gt.setCurrentOpertaionPlayerIndex(pos);
			gt.setDealerPos(pos);
			logger.info("【电子庄牛牛】，房间ID【"+gt.getVipTableID()+"】，游戏开局选庄【"+pos
						+"】，庄家昵称【"+px.getPlayerName()+"】，庄家ID【"+px.getPlayerIndex()+"】");
		}

		//
		logger.info("【电子庄牛牛】游戏正式开始");
		// 游戏开始
		gt.setState(GameConstant.TABLE_WAITING_CLIENT_SHOW_INIT_CARDS);
		gt.setStartGame(true);
		gt.setReadyTime(ctt);
		gt.setHandStartTime(ctt);
		gt.setHandEndTime(0);
		// 玩家全体进入游戏状态
		gt.set_all_player_state(GameConstant.USER_GAME_STATE_IN_TABLE_PLAYING);
		// 台费
		int service_gold = 0;
		service_gold = gr.serviceFee;

		// 玩家操作时间
		if (operation_over_time == 0) {
			operation_over_time = cfgService.getPara(
					ConfigConstant.OPERATION_OVER_TIME).getValueInt();
		}
		int operationTime = operation_over_time;
		if (operationTime < 5) {
			operationTime = 5;
		}
		// 发开局消息
		gt.sendGameStartMsg(service_gold, operationTime);

		vipTableLogCreate(gt);

		//俱乐部排名数据处理 
		if(gt.getClubCode() > 0){
			List<User> players = gt.getPlayers();
			for (int i = 0; i < players.size(); i++) {
				User pl = players.get(i);
				if (pl != null) {
					playerService.updateClubRankingList(pl.getPlayerID(), gt.getClubCode(), "1,2,3,4,10", 1, "GAME_COUNT");
				}
			}
		}else{
			// 扣卡
			GameConfig config = GameContext.tableLogic_nndzz.getGameConfig();
			Map<String, Integer> mapKa = config.getPokerVipWanfa(gt.getTotalJuNum());
			logger.info("==========>扣卡配置："+mapKa);
			int isProxy = gt.getProxyState();
			int kaKouNum = mapKa.get("cost_self");
			if (isProxy == 2) {
				kaKouNum = mapKa.get("cost_aa");
			} else if (isProxy == 1) {
				kaKouNum = mapKa.get("cost_proxy");
			}
			if (gt.isVipTable()) {
				if (!gt.isPayVipKa()) {
					//代理开房扣卡
					if(gt.getProxyCreator() != null){
						logger.info("==>【牛牛】代理开房开局扣卡，房间ID【"+gt.getVipTableID()
								+"】，代理ID【"+gt.getProxyCreator().getPlayerIndex()
								+"】，代理昵称【"+gt.getProxyCreator().getPlayerName()
								+"】，本次房卡局数【"+gt.getTotalJuNum()
								+"】，扣卡张数【"+kaKouNum
								+"】，当前房间总局数【"+gt.getTotalJuNum()
								+"】");
						playerService.use_fangka(gt.getProxyCreator(), gt.getItemBaseID(),gt.getRoomType(),kaKouNum);
					}else if (gt.getProxyState() == 2){
						for (int i = 0; i < plist.size(); i++) {
							User pl = plist.get(i);
							logger.info("==>【牛牛】AA开局扣卡，房间ID【"+gt.getVipTableID()
									+"】，房主ID【"+pl.getPlayerID()
									+"】，房主昵称【"+pl.getPlayerName()
									+"】，本次房卡局数【"+gt.getTotalJuNum()
									+"】，扣卡张数【"+kaKouNum
									+"】，当前房间总局数【"+gt.getTotalJuNum()
									+"】");
							playerService.use_fangka(pl, gt.getItemBaseID(),gt.getRoomType(),kaKouNum);
						}
					}else{
						logger.info("==>【牛牛】开局扣卡，房间ID【"+gt.getVipTableID()
								+"】，【一人支付】，本次房卡局数【"+gt.getTotalJuNum()
								+"】，扣卡张数【"+kaKouNum
								+"】，当前房间总局数【"+gt.getTotalJuNum()
								+"】");
						playerService.use_fangka(gt.getCreator(), gt.getItemBaseID(),gt.getRoomType(),kaKouNum);
					}
					
					gt.setPayVipKa(true);
					gt.setStart(true);//开始游戏
					for (User pl : gt.getPlayers()) {
						if (pl != null) {
							pl.game_total_count++;
						}
					}
					if (gt.canCloseVipRoom) {
						gt.canCloseVipRoom = false;
					}
					//更新代理开房记录//代开优化 cc modify 2017-9-26
					if(gt.getProxyCreator() != null){
						List<ProxyVipRoomRecord> pvrrs = gt.getProxyCreator().getPvrrs();
						if (pvrrs != null && pvrrs.size()>0) {
							for(ProxyVipRoomRecord pvrr:pvrrs){
								if(pvrr.getRoomID().equals(gt.getTableID())){
									for (int i = 0; i < gt.getPlayers().size(); i++) {
										User pl = gt.getPlayers().get(i);
										if (i == 0) {
											pvrr.setPlayer1Name(pl.getPlayerName());
											pvrr.setPlayer1Index(pl.getPlayerIndex());
										} else if (i == 1) {
											pvrr.setPlayer2Name(pl.getPlayerName());
											pvrr.setPlayer2Index(pl.getPlayerIndex());
										} else if (i == 2) {
											pvrr.setPlayer3Name(pl.getPlayerName());
											pvrr.setPlayer3Index(pl.getPlayerIndex());
										}else{
											pvrr.setPlayer4Name(pl.getPlayerName());
											pvrr.setPlayer4Index(pl.getPlayerIndex());
										}
									}
									pvrr.setStartTime(ct);
									pvrr.setHostID(gt.getCreatorPlayerID());
									pvrr.setHostName(gt.getCreatorPlayerName());
									pvrr.setVipState(GameConstant.PROXY_START_GAME);
		//							pvrr.setProxyCardCount(quanNum);
									playerService.updateProxyVipRoomRecord(pvrr);	
									
									//给代理发刷新列表消息
									ProxyVipRoomRecordMsg prvm = new ProxyVipRoomRecordMsg();
									prvm.playerID = gt.getProxyCreator().getPlayerID();
									prvm.gameID = gt.getGameId();
									playerService.getProxyVipRoomRecord(prvm, gt.getProxyCreator().getSession());
								}
							}	
						}
					}
				}
			}
		}
	}

	/**
	 * vip结束
	 * @author  2017-11-15
	 * GameProcessThread.java
	 * @param gameTable
	 * @param gameRoom
	 * void
	 */
	private void vip_table_end(GameTable gameTable, GameRoom gameRoom) {
		List<User> plist = gameTable.getPlayers();
//		RecordBean recordbean = null;
//		for (int i = 0; i < plist.size(); i++) {
//			User pl = plist.get(i);
//			recordbean = pl.getRecordBean();
//		}
//        if(recordbean != null){
//        	recordbean.setRoom_Id(gameTable.getTableID());
//        	recordbean.setEnd_Way(gameTable.getTableEndWay());
//        	recordbean.setEnd_Time(new Date());
//        	recordService.updateRecordEndWay(recordbean);
//        }
		
		for (int i = 0; i < plist.size(); i++) {
			User pl = plist.get(i);
			VipRoomRecord vrr = pl.getVrr();
			if(vrr != null){
	            vrr.setEndWay(gameTable.getTableEndWay());
	            vrr.setEndTime(new Date());
	            playerService.endVipRoomRecord(vrr);
	       }
		}
        
        User winnerPl = null;
        /*处理俱乐部排名数据：大赢家*/
		if(gameTable.getClubCode() > 0){
			int maxPoint = 0;
			for(User pl : plist){
				int point = pl.getWinLoseTotalCoinNum();
				if(point > maxPoint){
					maxPoint = point;
					winnerPl = pl;
				}
			}
			if(winnerPl != null && gameTable.getClubCode() > 0){
				playerService.updateClubRankingList(winnerPl.getPlayerID(), gameTable.getClubCode(), "", 1, "WINNER_COUNT");
			}
		}

		//代理开房记录日志//代开优化 cc modify 2017-9-26
		if(gameTable.getProxyCreator() != null){			
			List<ProxyVipRoomRecord> pvrrs = gameTable.getProxyCreator().getPvrrs();
			if(pvrrs != null){
				for(int i=pvrrs.size()-1;i>=0;i--){
					ProxyVipRoomRecord pvrr = pvrrs.get(i);
					if(pvrr.getRoomID().equals(gameTable.getTableID())){
						boolean isUpdateProxyList = false;//是否需要前台刷新代开记录列表
						if(gameTable.isStart()){//已开始游戏
							if(gameTable.getTableEndWay().equals("超时解散")){
								pvrr.setVipState(GameConstant.PROXY_OUTTIME_END_GAME_START);
							}else{
								if (gameTable.getCloseVipRoomNum()>= 1 ){
									pvrr.setVipState(GameConstant.PROXY_PLAYER_END_GAME_START);
								}else{
									pvrr.setVipState(GameConstant.PROXY_END_GAME);
								}
							}
							pvrr.setEndTime(new Date());
							pvrr.setEndWay(gameTable.getTableEndWay());
						}else{//未开始游戏
							if(gameTable.isProxyForceClose()){	//如果是代理强制结束
								gameTable.setTableEndWay("未开始游戏，代理解散房间");
								pvrr.setVipState(GameConstant.PROXY_FORCE_END_GAME);
							}else{
								if(pvrr.getHostID()==null || pvrr.getHostID().equals("")){
									gameTable.setTableEndWay("玩家未进入，代理解散房间");
								}else{
									gameTable.setTableEndWay("房主解散房间");
									isUpdateProxyList = true;//游戏未开始，玩家解散房间，代理刷新代开记录列表
								}
								pvrr.setVipState(GameConstant.PROXY_PLAYER_END_GAME);
							}
							pvrr.setEndWay(gameTable.getTableEndWay());
							
						}
						pvrrs.remove(i);
						this.playerService.endProxyVipRoomRecord(pvrr);
						
						if(isUpdateProxyList){
							//给代理发刷新列表消息
							ProxyVipRoomRecordMsg prvm = new ProxyVipRoomRecordMsg();
							prvm.playerID = gameTable.getProxyCreator().getPlayerID();
							prvm.gameID = gameTable.getGameId();
							playerService.getProxyVipRoomRecord(prvm, gameTable.getProxyCreator().getSession());
						}
						pvrr.setEndTime(new Date());
					}
				}
			}
			gameTable.getProxyCreator().setPvrrs(pvrrs);
		}
		//
		VipRoomCloseMsg closeMsg = new VipRoomCloseMsg();
		closeMsg.init_players(gameTable.getPlayers(), gameTable,gameRoom.fixedGold);
		closeMsg.proxyForceClose = 0;//给牌桌内玩家发
		if(gameTable.isProxyForceClose() && gameTable.getPlayers().contains(gameTable.getProxyCreator())){
			gameTable.sendMsgToTableExceptMe(closeMsg , gameTable.getProxyCreator().getPlayerID());
		}else{
			gameTable.sendMsgToTable(closeMsg);
		}
		
		int clubCode = gameTable.getClubCode();
		
		//
		for (int i = 0; i < plist.size(); i++) {
			User pl = plist.get(i);
			if (pl.getTableID() != gameTable.getTableID()){
			// 2017.9.19 玩家不在此房间里，则不清除
			}else{
				// 清理掉玩家的桌子vip金币
				pl.setVipTableGold(0);
				// 输赢金币清零
				pl.setWinLoseTotal(0);
				pl.setWinLoseGoldNum(0);
				pl.setWinLoseCurrCoinNum(0);
				// 清除VIP桌子统计信息
				pl.clearVipTableInfo();
				// 清除游戏信息
				pl.clear_game_state();
				pl.setVipTableID("");
				logger.info("vip房间结束，gpt.vip_table_end，清空玩家【"+pl.getVipTableID()+":"
						+pl.getPlayerIndex()+"-"+pl.getPlayerName()+"】vipTableId");
			}
		}
		gameTable.clear_all();
		gameTable.setState(GameConstant.TABLE_STATE_INVALID);
//		 del_table(gameTable);
		
		if(clubCode > 0){
        	//TODO 发消息通知前台俱乐部的所有人，当前俱乐部各种牌桌状态
			playerService.sendMsgToClubPlayer(clubCode, playerService.newGetSingleClubInfoMsgAck(clubCode));
        }
	}

	private void close_vip_table(GameRoom gr, User pl) {
		if (gr == null)
			return;

		GameTable gt = gr.getTable(pl.getTableID(), pl.isPlayingSingleTable());
		if (gt == null) {
			return;
		}

		logger.error("【电子庄牛牛】玩家" + pl.getPlayerIndex() + ",同意解散VIP房间:"
				+ gt.getVipTableID());

		// 有两个人同意结束，则结束房间
		gt.setCloseVipRoomNum(gt.getCloseVipRoomNum() + 1);
		// kn add
		if (gr.roomType != GameConstant.ROOM_TYPE_COUPLE) {
			if (gt.getCloseVipRoomNum() >= 1) {
				logger.error("【电子庄牛牛】同意人数达到1人，解散VIP房间:" + gt.getVipTableID());
				// 结束VIP房间
				List<User> players = gt.getPlayers();
		    	String endWay = "";
				for (int i = 0; i < players.size(); i++) {
					User user = players.get(i);
					if(user.isApplyCloseRoom()){
						endWay = "玩家"+user.getPlayerName()+"申请解散";
						break;
					}
				}
				gt.setTableEndWay(endWay);
				vip_table_end(gt, gr);
			}
		} else {
			if (gt.getCloseVipRoomNum() >= 1) {
				logger.error("【电子庄牛牛】同意人数达到2人，解散VIP房间:" + gt.getVipTableID());
				// 结束VIP房间
				List<User> players = gt.getPlayers();
		    	String endWay = "";
				for (int i = 0; i < players.size(); i++) {
					User user = players.get(i);
					if(user.isApplyCloseRoom()){
						endWay = "玩家"+user.getPlayerName()+"申请解散";
						break;
					}
				}
				gt.setTableEndWay(endWay);
				vip_table_end(gt, gr);

				pl.setPlayerType(0);
				playerService.updatePlayerType(pl.getPlayerID(), 0);

				pl.setTwoVipOnline(false);
			}
		}
	}

	/**
	 * VIP房间累计游戏信息
	 */
	public void vipTableGameInfoCount(GameTable gt, User pl) {// STiV modify
																// param
		// 赢牌次数+1
		pl.setWinCount(pl.getWinCount() + 1);

		// 地主次数
		for (User px : gt.getPlayers()) {
			if (px.getTablePos() == gt.getDealerPos()) {
				px.setZhuangCount(px.getZhuangCount() + 1);
			}
		}
	}
	/**
	 * 牌局结算
	 * GameProcessThread.java
	 * @param gt
	 * @param pl
	 * @param plx
	 * void
	 */
	private void gameSettle(GameTable gt) {
		logger.info("【电子庄牛牛】--->单局结算，房间ID【 "+gt.getVipTableID()+"】");
		int dealerPos = gt.getDealerPos();
		List<Byte> dealerCards = null;
		int dealerScoreSum = 0;
		int dealerGold = 0;
		double serviceFee = 0.05;
		User dealer = null;
		TClub club = null;
		boolean isClub = gt.getClubCode() > 0;
		if(isClub){
			// 俱乐部
			club = playerService.getClubByClubCode(gt.getClubCode());
			dealerGold = club.getGoldNum();
			dealerCards = gt.getDealerCards();
		}else{
			// VIP房
			dealer = gt.getPlayerByTablePos(dealerPos);
			dealerGold = dealer.getVipTableGold();
			dealerCards = dealer.getCardsInHand();
		}
		
		int dealerPingBeiMultiple = CardUtil.getPingBeiMultiple(dealerCards);
		int dealerFanBeiMultiple = CardUtil.getFanBeiMultiple(dealerCards);
		for(User pl : gt.getStakeCoinPlayers()){
			List<Byte> cards = pl.getCardsInHand();
			UserGameHander hander = pl.getUserGameHanderForNNDZZ();
			int cardShape = hander.getCardShape();
			//设置最大牌型
			if(cardShape >= hander.getMaxCardShape()){
				hander.setMaxCardShape(cardShape);
				if(cardShape == GameConstant.NNDZZ_CARD_TYPE_HAS_NIU){
					if(CardUtil.getNNTypeNum(cards) > hander.getMaxCardShapeLevel()){
						hander.setMaxCardShapeLevel(CardUtil.getNNTypeNum(cards));
					}
				}
			}
			
			int pingBeiMultiple = CardUtil.getPingBeiMultiple(cards);
			int fanBeiMultiple = CardUtil.getFanBeiMultiple(cards);
			
			int pingBeiCoin = hander.getPingBeiCoin();
			int fanBeiCoin = hander.getFanBeiCoin();
			
			int pingBeiScore = 0;
			int fanBeiScore = 0;
			int totalScore = 0;
			
			int dealerPingBeiScore = 0;
			int dealerFanBeiScore = 0;
			int dealerTotalScore = 0;
			if(hander.isWin()){
				// 闲家赢
				if(pingBeiCoin != -1){
					pingBeiScore = pingBeiCoin * pingBeiMultiple;
					dealerPingBeiScore = pingBeiScore * -1;
				}
				if(fanBeiCoin != -1){
					fanBeiScore = fanBeiCoin * fanBeiMultiple;
					dealerFanBeiScore = fanBeiScore * -1;
				}
				
				hander.setPingBeiScore(pingBeiScore);
				hander.setFanBeiScore(fanBeiScore);
				totalScore = pingBeiScore + fanBeiScore;
				int dealerTotal = dealerPingBeiScore + dealerFanBeiScore;
				
				// 牛七以上收服务费
				if(fanBeiMultiple >= 2){
					Long dfee = Math.round(fanBeiScore * serviceFee);
					int fee = dfee.intValue();
					
					totalScore = totalScore - fee;
					dealerTotalScore = dealerTotal + fee;
					logger.info("【电子庄牛牛】--->单局结算，房间ID【 "+gt.getVipTableID()+"】，玩家昵称【"+pl.getPlayerName()
							+"】，玩家ID【"+pl.getPlayerID()+"】，翻倍倍数【"+fanBeiMultiple+"】，翻倍得分【"+fanBeiScore
							+"】，收取服务费比例【"+serviceFee+"】，服务费【"+fee+"】");
				}else{
					dealerTotalScore = dealerTotal; 
				}
				
				pl.setWinLoseCurrCoinNum(totalScore);
				//设置当局最高分
				if(pl.getWinLoseCurrCoinNum() > pl.getUserGameHanderForNNDZZ().getMaxCoinNum()){
					pl.getUserGameHanderForNNDZZ().setMaxCoinNum(pl.getWinLoseCurrCoinNum());
				}
				
				hander.setWinNum(hander.getWinNum() + 1);
			}else{
				// 庄家赢
				if(pingBeiCoin != -1){
					dealerPingBeiScore = pingBeiCoin * dealerPingBeiMultiple;
					pingBeiScore = dealerPingBeiScore * -1;
				}
				if(fanBeiCoin != -1){
					dealerFanBeiScore = fanBeiCoin * dealerFanBeiMultiple;
					fanBeiScore = dealerFanBeiScore * -1;
				}
				
				hander.setPingBeiScore(pingBeiScore);
				hander.setFanBeiScore(fanBeiScore);
				totalScore = pingBeiScore + fanBeiScore;
				dealerTotalScore = dealerPingBeiScore + dealerFanBeiScore;
				
				pl.setWinLoseCurrCoinNum(totalScore);
				
				hander.setLoseNum(hander.getLoseNum() + 1);
			}
			
			dealerScoreSum += dealerTotalScore;
			//总分
			pl.setWinLoseTotalCoinNum(pl.getWinLoseTotalCoinNum() + pl.getWinLoseCurrCoinNum());
			//设置金币
			pl.setVipTableGold(pl.getVipTableGold() + pl.getWinLoseCurrCoinNum());
			
			if(isClub){
				// 更新玩家俱乐部金币
				playerService.addClubPlayerGold(pl, gt.getClubCode(), pl.getWinLoseCurrCoinNum());
			}
			
			pl.setScore(pl.getWinLoseTotalCoinNum());
			int cardLevel = 0;
			if(cardShape == GameConstant.NNDZZ_CARD_TYPE_HAS_NIU) {
				cardLevel = CardUtil.getNNTypeNum(pl.getCardsInHand());
			}
			logger.info("【电子庄牛牛】算分：玩家【"+pl.getPlayerName()+":"+pl.getPlayerIndex()+"】平倍押注【"+pingBeiCoin
					+"】，平倍倍数【"+pingBeiMultiple+"】，平倍得分【"+pingBeiScore+"】，翻倍押注【"+fanBeiCoin+"】，翻倍倍数【"+fanBeiMultiple
					+"】，翻倍得分【"+fanBeiScore+"】，本局得分【"+pl.getWinLoseCurrCoinNum()+"】," +
					"当前金币【"+pl.getVipTableGold()+"】，当前牌型【"+cardShape+"】"+ (cardLevel == 0 ? "" : "：牛" + cardLevel )+"，最大牌型【"+hander.getMaxCardShape()+":"+
					hander.getMaxCardShapeLevel()+"】");
		}
		
		int finalGold = dealerGold+dealerScoreSum;
		
		if(isClub){
			// 更新俱乐部金币
			club.setGoldNum(finalGold);
			playerService.addClubGold(gt.getClubCode(), dealerScoreSum);
			logger.info("【电子庄牛牛】算分：俱乐部更新前金币【"+dealerGold+"】，本局总输赢【"+dealerScoreSum+"】，最终金币【"+finalGold+"】");
		}else{
			// 更新庄家金币
			dealer.setVipTableGold(finalGold);
			dealer.setWinLoseCurrCoinNum(dealerScoreSum);
			//总分
			dealer.setWinLoseTotalCoinNum(dealer.getWinLoseTotalCoinNum() + dealer.getWinLoseCurrCoinNum());
			
			UserGameHander hander = dealer.getUserGameHanderForNNDZZ();
			int cardShape = hander.getCardShape();
			int cardLevel = 0;
			if(cardShape == GameConstant.NNDZZ_CARD_TYPE_HAS_NIU) {
				cardLevel = CardUtil.getNNTypeNum(dealer.getCardsInHand());
			}
			//设置最大牌型
			if(cardShape >= hander.getMaxCardShape()){
				hander.setMaxCardShape(cardShape);
				if(cardShape == GameConstant.NNDZZ_CARD_TYPE_HAS_NIU){
					List<Byte> cards = dealer.getCardsInHand();
					if(CardUtil.getNNTypeNum(cards) > hander.getMaxCardShapeLevel()){
						hander.setMaxCardShapeLevel(CardUtil.getNNTypeNum(cards));
					}
				}
			}
			
			logger.info("【电子庄牛牛】庄家算分：玩家【"+dealer.getPlayerName()+":"+dealer.getPlayerIndex()+"】，计算前金币【"+dealerGold+"】，本局得分【"+dealerScoreSum+"】," +
					"最终金币【"+finalGold+"】，当前牌型【"+cardShape+"】"+ (cardLevel == 0 ? "" : "：牛" + cardLevel )+"，最大牌型【"+hander.getMaxCardShape()+":"+
					hander.getMaxCardShapeLevel()+"】");
		}
        gt.setDealerWinLoseTotal(gt.getDealerWinLoseTotal() + dealerScoreSum);
		gt.setState(GameConstant.TABLE_STATE_SHOW_GAME_OVER_SCREEN);
		long ctt = DateService.getCurrentUtilDate().getTime();
		gt.setHandEndTime(ctt);
	}
	
	/**
	 * 玩家执行选庄操作
	 * @param gt
	 * @param msg
	 * @param pl
	 * @return
	 */
	protected int player_op_zhuang(GameTable gt, PlayerTableOperationMsg msg, User pl) {
		UserGameHander gameHander = pl.getUserGameHanderForNNDZZ();
		boolean chooseZhuang =gameHander.isChooseZhuang();
		if(chooseZhuang) {
			logger.error("ERROR----->【电子庄牛牛】【选庄】房间ID【"+gt.getVipTableID()+"】【"+pl.getTablePos()
					+"】号位置，ID为【"+pl.getPlayerIndex()+"】，玩家昵称【"+pl.getPlayerName()
					+"】执行了选庄操作，玩家已经选过，不能重复选");
			return 0;
		}
		
		int dealerPos = gt.getDealerPos();
		if(dealerPos != pl.getTablePos()){
			logger.error("ERROR----->【电子庄牛牛】【选庄】房间ID【"+gt.getVipTableID()+"】【"+pl.getTablePos()
					+"】号位置，ID为【"+pl.getPlayerIndex()+"】，玩家昵称【"+pl.getPlayerName()
					+"】当前不是在等待此玩家");
			return 0;
		}
		
		logger.info("----->【电子庄牛牛】【选庄】房间ID【"+gt.getVipTableID()+"】【"+pl.getTablePos()
				+"】号位置，ID为【"+pl.getPlayerIndex()+"】，玩家昵称【"+pl.getPlayerName()
				+"】执行了选庄操作，操作结果【"+msg.opValue+"】");

		gameHander.setChooseZhuang(true);
		
		msg.operationType = GameConstant.NNDZZ_OPT_ZHUANG;
		msg.playerTablePos = pl.getTablePos();
		gt.sendMsgToTable(msg);
		
		if(msg.opValue == 1){
			// 当庄，提醒下注
			player_xiazhu_notify(gt, gt.game_room);
		}else{
			// 下庄，下家继续当庄
			int nextPos = gt.getNextPlayerPos(dealerPos);
			
			User px = gt.getPlayerByTablePos(nextPos);
			if(px.getUserGameHanderForNNDZZ().isChooseZhuang()){
				// 已经轮完一圈了，暂时按清空重新轮圈处理
				for(User pxx : gt.getPlayers()){
					pxx.getUserGameHanderForNNDZZ().setChooseZhuang(false);
				}
			}
			
			gt.setDealerPos(nextPos);
			player_zhuang_notify(gt, gt.game_room);
		}
		
		return 0;
	}
	
	/**
	 * 玩家执行平倍下注操作
	 * @param gt
	 * @param msg
	 * @param pl
	 * @return
	 */
	protected int player_op_pingbei(GameTable gt, PlayerTableOperationMsg msg, User pl) {
		List<Integer> coinItems = gt.getCoinItemsByLevel();
		List<Integer> onesCoinItems = gt.getPlayerAllowCoinItems(coinItems, pl);
		int value = msg.opValue;

        UserGameHander gameHander = pl.getUserGameHanderForNNDZZ();
        int playerPingBeiCoin = gameHander.getPingBeiCoin();
        int playerFanBeiCoin = gameHander.getFanBeiCoin();
        int gold = pl.getVipTableGold();
        int availableGold = gold;
        if (playerFanBeiCoin == -1) { playerFanBeiCoin = 0; }            
        if (playerPingBeiCoin == -1) { playerPingBeiCoin = 0; }

        boolean isClub=gt.getClubCode() != 0;//判断是否是俱乐部内牌桌
        if (isClub) {
            availableGold -= 5 * playerFanBeiCoin + playerPingBeiCoin;
        } else {
            availableGold = GameContext.getConfigParamToInt("vip_max_coin", 3000);
            availableGold -= playerFanBeiCoin + playerPingBeiCoin;
        }

			boolean isOver = false;
			if(onesCoinItems.size() == 0){
				isOver = true;
            } else if (value > availableGold) {
                isOver = true;
            } else {
				boolean allow = false;
				for(Integer allowCoin : onesCoinItems){
					if(allowCoin >= value){
						allow = true;
						break;
					}
				}
				isOver = !allow;
			}
			
			if(isOver){
				logger.error("----->【电子庄牛牛】【平倍超限】俱乐部ID【"+gt.getClubCode()+"】房间ID【"+gt.getVipTableID()+"】【"+pl.getTablePos()
						+"】号位置，ID为【"+pl.getPlayerIndex()+"】，玩家昵称【"+pl.getPlayerName()
						+"】执行了平倍押注操作但分数已超限，押注分数【"+value+"】，玩家金币【"+pl.getVipTableGold()
						+"】，玩家平倍押注分数【"+gameHander.getPingBeiCoin()
						+"】，玩家翻倍押注分数【"+gameHander.getFanBeiCoin()+"】");
				
				// 发送信息更新玩家前端押注额度
				msg.operationType = GameConstant.NNDZZ_OPT_PINGBEI;
				msg.playerTablePos = pl.getTablePos();
				msg.coinItems = coinItems;
				msg.allowCoinItems = onesCoinItems;
				msg.opValue = playerPingBeiCoin;
				GameContext.gameSocket.send(pl.getSession(), msg);
				return 0;
			}
			gameHander.setPingBeiCoin(playerPingBeiCoin+value);		
		
		logger.info("----->【电子庄牛牛】【平倍】房间ID【"+gt.getVipTableID()+"】【"+pl.getTablePos()
				+"】号位置，ID为【"+pl.getPlayerIndex()+"】，玩家昵称【"+pl.getPlayerName()
				+"】执行了平倍押注操作，押注分数【"+msg.opValue+"】，最新押注分数【"+gameHander.getPingBeiCoin()+"】");
		
		msg.operationType = GameConstant.NNDZZ_OPT_PINGBEI;
		msg.playerTablePos = pl.getTablePos();
        playerPingBeiCoin = gameHander.getPingBeiCoin();
        if (playerPingBeiCoin == -1) { playerPingBeiCoin = 0; }
        msg.opValue = playerPingBeiCoin;
		gt.sendMsgToTableExceptMe(msg, pl.getPlayerID());
		
		// 更新下注筹码
		onesCoinItems = gt.getPlayerAllowCoinItems(coinItems, pl);
		msg.coinItems = coinItems;
		msg.allowCoinItems = onesCoinItems;
		GameContext.gameSocket.send(pl.getSession(), msg);
		
		//记录状态
		MahjongCheckResult result = gt.getCurrentWaitingOperationPlayer();
		Map<Integer, List<Integer>> allowCoinItems = result.allowStakeItems;
		allowCoinItems.put(pl.getTablePos(), onesCoinItems);
		result.allowStakeItems = allowCoinItems;
		gt.setWaitingPlayerOperate(result);		
		
		return 0;
	}
	
	/**
	 * 玩家执行翻倍操作
	 * @param gt
	 * @param msg
	 * @param pl
	 * @return
	 */
	protected int player_op_fanbei(GameTable gt, PlayerTableOperationMsg msg, User pl) {
		List<Integer> coinItems = gt.getCoinItemsByLevel();
		List<Integer> onesCoinItems = gt.getPlayerAllowCoinItems(coinItems, pl);
		int value = msg.opValue;

        UserGameHander gameHander = pl.getUserGameHanderForNNDZZ();
        int playerFanBeiCoin = gameHander.getFanBeiCoin();
        int playerPingBeiCoin = gameHander.getPingBeiCoin();
        int gold = pl.getVipTableGold();
        int availableGold = gold;
        if (playerFanBeiCoin == -1) { playerFanBeiCoin = 0; }            
        if (playerPingBeiCoin == -1) { playerPingBeiCoin = 0; }

        boolean isClub=gt.getClubCode() != 0;//判断是否是俱乐部内牌桌
        if (isClub) {
            availableGold -= 5 * playerFanBeiCoin + playerPingBeiCoin;
        } else {
            availableGold = GameContext.getConfigParamToInt("vip_max_coin", 3000);
            availableGold -= playerFanBeiCoin + playerPingBeiCoin;
        }

            boolean isOver = false;
            if(onesCoinItems.size() == 0){
                isOver = true;
            } else if ( 5 * value > availableGold && isClub) {
                isOver = true;
            } else if (value > availableGold && !isClub) {
                isOver = true;
            } else {
				boolean allow = false;
				for(Integer allowCoin : onesCoinItems){
					if(allowCoin >= value){
						allow = true;
						break;
					}
				}
				isOver = !allow;
			}
			
			if(isOver){
				logger.error("----->【电子庄牛牛】【翻倍超限】俱乐部ID【"+gt.getClubCode()+"】房间ID【"+gt.getVipTableID()+"】【"+pl.getTablePos()
						+"】号位置，ID为【"+pl.getPlayerIndex()+"】，玩家昵称【"+pl.getPlayerName()
						+"】执行了翻倍押注操作但分数已超限，押注分数【"+value+"】，玩家金币【"+pl.getVipTableGold()
						+"】，玩家平倍押注分数【"+gameHander.getPingBeiCoin()
						+"】，玩家翻倍押注分数【"+gameHander.getFanBeiCoin()+"】");
				
				// 发送信息更新玩家前端押注额度
				msg.operationType = GameConstant.NNDZZ_OPT_FANBEI;
				msg.playerTablePos = pl.getTablePos();
				msg.coinItems = coinItems;
				msg.allowCoinItems = onesCoinItems;
                msg.opValue = playerFanBeiCoin;
				GameContext.gameSocket.send(pl.getSession(), msg);
				return 0;
			}
			gameHander.setFanBeiCoin(playerFanBeiCoin+msg.opValue);
		
		logger.info("----->【电子庄牛牛】【翻倍】房间ID【"+gt.getVipTableID()+"】【"+pl.getTablePos()
				+"】号位置，ID为【"+pl.getPlayerIndex()+"】，玩家昵称【"+pl.getPlayerName()
				+"】执行了翻倍押注操作，押注分数【"+msg.opValue+"】，最新押注分数【"+gameHander.getFanBeiCoin()+"】");
		
		msg.operationType = GameConstant.NNDZZ_OPT_FANBEI;
		msg.playerTablePos = pl.getTablePos();        
        playerFanBeiCoin = gameHander.getFanBeiCoin();
        if (playerFanBeiCoin == -1) { playerFanBeiCoin = 0; }
		msg.opValue = playerFanBeiCoin;
		gt.sendMsgToTableExceptMe(msg, pl.getPlayerID());
		
		// 更新下注筹码
		onesCoinItems = gt.getPlayerAllowCoinItems(coinItems, pl);
		msg.coinItems = coinItems;
		msg.allowCoinItems = onesCoinItems;
		GameContext.gameSocket.send(pl.getSession(), msg);
		
		//记录状态
		MahjongCheckResult result = gt.getCurrentWaitingOperationPlayer();
		Map<Integer, List<Integer>> allowCoinItems = result.allowStakeItems;
		allowCoinItems.put(pl.getTablePos(), onesCoinItems);
		result.allowStakeItems = allowCoinItems;
		gt.setWaitingPlayerOperate(result);		
		
		return 0;
	}
	
	/**
	 * 玩家亮牛操作
	 * @param gt
	 * @param msg
	 * @param pl
	 * @return
	 */
	protected int player_op_liangniu(GameTable gt, PlayerTableOperationMsg msg, User pl) {
		UserGameHander gameHander = pl.getUserGameHanderForNNDZZ();
		//该玩家没有亮牛
		if(!gameHander.isLiangNiu()) {
			gameHander.setLiangNiu(true);
			logger.info("----->【电子庄牛牛】【亮牛】房间ID【"+gt.getVipTableID()+"】【"+pl.getTablePos()
					+"】号位置，ID为【"+pl.getPlayerIndex()
					+"】，玩家昵称【"+pl.getPlayerName()+"】执行了亮牛操作");
		}else{
			logger.info("----->【电子庄牛牛】【亮牛】【ERROR】房间ID【"+gt.getVipTableID()+"】【"+pl.getTablePos()
					+"】号位置，ID为【"+pl.getPlayerIndex()
					+"】，玩家昵称【"+pl.getPlayerName()+"】已经执行过亮牛操作，不能重复亮牛");
			return -1;
		}
		msg.operationType = GameConstant.NNDZZ_OPT_LIANGNIU;
		msg.playerTablePos = pl.getTablePos();
		msg.myCards = pl.getCardsInHand();
		msg.opValue = gameHander.getCardShape();
		
		gt.sendMsgToTable(msg);
		
		// 跟庄家比牌，判断输赢
		User dealer = null;
		List<Byte> dealerCards = null;
		boolean isDealer = false;
		if(gt.getClubCode() > 0){
			dealerCards = gt.getDealerCards();
		}else{
			dealer = gt.getPlayerByTablePos(gt.getDealerPos());
			dealerCards = dealer.getCardsInHand();
			isDealer = gt.getDealerPos() == pl.getTablePos();
		}
		boolean isDealerWin = CardUtil.compareMyCardsWithOtherCards(dealerCards, pl.getCardsInHand());
		gameHander.setWin(!isDealerWin);

		PlayerOperationNotifyMsg nMsg = new PlayerOperationNotifyMsg();
		nMsg.operationType = GameConstant.NNDZZ_OPT_WIN;
		nMsg.playerTablePos = pl.getTablePos();
		nMsg.opValue = isDealer ? -1 : (isDealerWin ? 0 : 1);
		nMsg.gold = pl.getVipTableGold();
		GameContext.gameSocket.send(pl.getSession(), nMsg);
		
		boolean isAllPlayersLiangNiu = true;
		List<User> pls = gt.getStakeCoinPlayers();
		if(gt.getClubCode() == 0){
			pls.add(gt.getPlayerByTablePos(gt.getDealerPos()));
		}
		for(User plx : pls){
			if(!plx.getUserGameHanderForNNDZZ().isLiangNiu()){
				isAllPlayersLiangNiu = false;
			}
		}
		
		if(isAllPlayersLiangNiu){
			// 结算
			gameSettle(gt);
		}
		
		return 0;
	}
	
	/**
	 * 发送提示消息给桌上的玩家，让大家知道当前谁在操作
	 * @param msg 
	 * 
	 * @param gt
	 * @param operationType 
	 * @param pos
	 */
	protected void notifyOtherPlayersInTable(PlayerOperationNotifyMsg msg, GameTable gt) {
		msg.operationType = GameConstant.DDZ_OPT_TIP;
		gt.sendMsgToTable(msg);
	}
	/**
	 * 玩家断线后，重新提醒玩家操作
	 * @param gt
	 * @param gr
	 */
	private void re_notify_current_operation_player(GameTable gt, GameRoom gr, User pl) {
		MahjongCheckResult result = gt.getWaitingPlayerOperate();
		int operatorType = result.opertaion;
		PlayerOperationNotifyMsg msg = new PlayerOperationNotifyMsg();
		msg.operationType = operatorType;
		
		int substate = gt.getPlaySubstate();

		long startTime = gt.getWaitingStartTime();
		long now = System.currentTimeMillis();
		int useTime = (int) ((now - startTime) / 1000);
		
		if (substate == GameConstant.TABLE_SUB_STATE_XIAZHU) { //平倍押注
			// time1代表进桌等待时间，time2代表押注时间，time3代表基础发牌时间，time4代表单人发牌时间，每多一个玩家多算一份时间，time5代表亮牛等待时间，time5代表亮牛后等待时间
			GameConfig gameConfig = GameContext.tableLogic_nndzz.getGameConfig();
			int operationTime = gameConfig.getOpt_time2();
			int leastTime = operationTime - useTime;
			leastTime = leastTime < 0 ? 0 : leastTime;
			
			int value = pl.getUserGameHanderForNNDZZ().getPingBeiCoin();
			msg.playerTablePos = pl.getTablePos();
			msg.operationTime = leastTime;
			msg.opValue = value;
			
			msg.coinItems = result.stakeItems;
			msg.allowCoinItems = result.allowStakeItems.get(pl.getTablePos());
			msg.gold = pl.getVipTableGold();
			GameContext.gameSocket.send(pl.getSession(), msg);
			
			logger.info("【电子庄牛牛】---->断线重回，重新提醒玩家平倍押注，房间ID【"+gt.getVipTableID()
					+"】，玩家ID【"+pl.getPlayerIndex()
					+"】，玩家位置【"+pl.getTablePos()
					+"】，玩家昵称【"+pl.getPlayerName()+"】,平倍押注【"+value+"】");
			
		}else if (substate == GameConstant.TABLE_SUB_STATE_SEND_CARD){// 发牌
			
		}else if (substate == GameConstant.TABLE_SUB_STATE_LIANGNIU){// 亮牛
			UserGameHander hander = pl.getUserGameHanderForNNDZZ();
			if(hander.isStakeCoin()){
				// time1代表进桌等待时间，time2代表押注时间，time3代表基础发牌时间，time4代表单人发牌时间，每多一个玩家多算一份时间，time5代表亮牛等待时间，time5代表亮牛后等待时间
				GameConfig gameConfig = GameContext.tableLogic_nndzz.getGameConfig();
				int operationTime = gameConfig.getOpt_time5();
				int leastTime = operationTime - useTime;
				leastTime = leastTime < 0 ? 0 : leastTime;
				
				boolean value = hander.isLiangNiu();
				msg.playerTablePos = pl.getTablePos();
				msg.operationTime = leastTime;
				msg.opValue = value ? 1 : 0;
				msg.gold = pl.getVipTableGold();
				GameContext.gameSocket.send(pl.getSession(), msg);
				
				logger.info("【电子庄牛牛】---->断线重回，重新提醒玩家亮牛，房间ID【"+gt.getVipTableID()
						+"】，玩家ID【"+pl.getPlayerIndex()
						+"】，玩家位置【"+pl.getTablePos()
						+"】，玩家昵称【"+pl.getPlayerName()+"】,是否亮牛【"+value+"】");
			}else{
				logger.info("【电子庄牛牛】---->断线重回，没押注不重新提醒亮牛，房间ID【"+gt.getVipTableID()
						+"】，玩家ID【"+pl.getPlayerIndex()
						+"】，玩家位置【"+pl.getTablePos()
						+"】，玩家昵称【"+pl.getPlayerName()+"】");
			}
		}
	}

	private void sendErrorMsg(User pl){
		if(pl.isRobot()){
			logger.error("====>玩家是机器人，不发异常信息了");
			return;
		}
		PlayerOperationNotifyMsg notifyMsg = new PlayerOperationNotifyMsg();
		notifyMsg.operationType = GameConstant.DDZ_OPT_ERROR_TIP;
		notifyMsg.playerTablePos = pl.getTablePos();
		GameContext.gameSocket.send(pl.getSession(), notifyMsg);
	}

	/**
	 * 操作业务派发
	 * @author  2017-11-15
	 * GameProcessThread.java
	 * @param msg
	 * @param pl
	 * @param gr
	 * void
	 */
	public void playerOperation(PlayerTableOperationMsg msg, User pl,
			GameRoom gr) {
		if (msg == null || pl == null)
			return;
		
		if (gr == null)
			return;

		GameTable gt = gr.getTable(pl.getTableID(), pl.isPlayingSingleTable());
		if (gt == null) {
			return;
		}
		
		// 玩家主动续卡
		if (msg.operationType == GameConstant.DDZ_OPT_EXTEND_CARD_REMIND) {
			if (msg.opValue == 0){
				not_extend_vip_table(gt,gr,false);
			}
			return;
		}

		// 有玩家同意关闭房间
		if (msg.operationType == GameConstant.DDZ_OPT_WAITING_OR_CLOSE_VIP) {
			close_vip_table(gr, pl);
			return;
		}

		// 异常处理
		boolean isError = false;
		//TODO 各种操作
		if(msg.operationType == GameConstant.NNDZZ_OPT_ZHUANG) {
			//执行选庄操作
			player_op_zhuang(gt, msg, pl);
		} else if (msg.operationType == GameConstant.NNDZZ_OPT_PINGBEI) {
			//执行平倍下注操作
			player_op_pingbei(gt, msg, pl);
		} else if (msg.operationType == GameConstant.NNDZZ_OPT_FANBEI) {
			//执行翻倍操作
			player_op_fanbei(gt, msg, pl);
		} else if(msg.operationType == GameConstant.NNDZZ_OPT_LIANGNIU){
			//执行亮牛操作
			player_op_liangniu(gt, msg, pl);
		}
		if(isError){
			logger.error(">>>>>>>操作异常处理：【"+pl.getTablePos()+"】号位玩家【"+pl.getPlayerName()+
					"】，后端手牌张数【"+pl.getCardNumInHand()+
					"】，后端手牌【"+pl.getCardsInHand()+"】");
			// 发送错误消息到前端
			sendErrorMsg(pl);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
//			GameContext.gameSocket.closeSession(pl.getSession());
		}
	}
	
	// vip不续卡处理
	private void not_extend_vip_table(GameTable gt, GameRoom gr,boolean isExtend){
		// 把结果通知前台
		PlayerOperationNotifyMsg msxg = new PlayerOperationNotifyMsg();
		msxg.operationType = GameConstant.DDZ_OPT_EXTEND_CARD_FAILED;
		
		List<User> players = gt.getPlayers();
		for(User pl : players){
			msxg.playerTablePos = pl.getTablePos();
			GameContext.gameSocket.send(pl.getSession(), msxg);
		}
		
		// 解散牌桌
		String tableEndWay = "牌局结束，正常解散";
		gt.setTableEndWay(tableEndWay);
		vip_table_end(gt,gr);
	}
	
	/**
	 * 玩牌中
	 * @author  2017-11-15
	 * GameProcessThread.java
	 * @param gt
	 * @param ctt
	 * @param gr
	 * void
	 */
	private void playing_table_tick(GameTable gt, long ctt, GameRoom gr) {
		if(gt.getState() != GameConstant.TABLE_SUB_STATE_IDLE 
				&& gt.getState() != GameConstant.TABLE_STATE_PLAYING) {
			return;
		}
		int substate = gt.getPlaySubstate();
		
		GameConfig gameConfig = GameContext.tableLogic_nndzz.getGameConfig();
		
		/**
		 * time1代表进桌等待时间，time2代表押注时间，time3代表基础发牌时间，time4代表单人发牌时间，每多一个玩家多算一份时间，time5代表亮牛等待时间，time5代表亮牛后等待时间
		 * 
		 * 考虑网络延迟原因，后台计算时间会在此基础上+2秒反应时间
		 */
		long now = System.currentTimeMillis();
		long opStartTime = gt.getWaitingStartTime();
		long timeInterval = now - opStartTime;
		
		if(substate == GameConstant.TABLE_SUB_STATE_ZHUANG){
			int operationTime = gameConfig.getOpt_time7();
			int time = (operationTime+2) * 1000;
			
			// 选庄时间到了自动当庄，开始下注
			if(timeInterval >= time){ 
				User pl = gt.getPlayerByTablePos(gt.getDealerPos());
				PlayerTableOperationMsg msg = new PlayerTableOperationMsg();
				msg.operationType = GameConstant.NNDZZ_OPT_ZHUANG;
				msg.playerTablePos = pl.getTablePos();
				msg.opValue = 1;
				gt.sendMsgToTable(msg);
				
				logger.info("========选庄到时间自动提醒下注：operationTime="+operationTime+", time="+time+", timeInterval="+timeInterval);
				player_xiazhu_notify(gt, gr);
			}
		}else if(substate == GameConstant.TABLE_SUB_STATE_XIAZHU){
			int operationTime = gameConfig.getOpt_time2();
			int time = (operationTime+2) * 1000;
			// 押注到时间自动发牌
			if(timeInterval >= time){ 
				logger.info("========下注到时间自动发牌：operationTime="+operationTime+", time="+time+", timeInterval="+timeInterval);
				sendCardToTable(gt, gr);
			}
		}else if(substate == GameConstant.TABLE_SUB_STATE_SEND_CARD){
			int time3 = gameConfig.getOpt_time3();
			int time4 = gameConfig.getOpt_time4();
			int sendCardTime = gt.getSendCardTime(time3, time4);
			int time = sendCardTime * 1000;
			// 发牌到时间自动提示亮牛
			if(timeInterval >= time){
				logger.info("========发牌到时间自动提醒亮牛：operationTime="+sendCardTime+", time="+time+", timeInterval="+timeInterval);
				player_liangniu_notify(gt, gr);
			}
		}else if(substate == GameConstant.TABLE_SUB_STATE_LIANGNIU){
			int operationTime = gameConfig.getOpt_time5();
			int time = (operationTime+2) * 1000;
			// 亮牛到时间自动亮牛
			if(timeInterval >= time){
				logger.info("========亮牛到时间自动亮牛：operationTime="+operationTime+", time="+time+", timeInterval="+timeInterval);
				PlayerTableOperationMsg msg = new PlayerTableOperationMsg();
				msg.operationType = GameConstant.NNDZZ_OPT_LIANGNIU;
				List<User> pls = gt.getStakeCoinPlayers();
				if(gt.getClubCode() == 0){
					pls.add(gt.getPlayerByTablePos(gt.getDealerPos()));
				}
				for(User pl : pls){
					if(!pl.getUserGameHanderForNNDZZ().isLiangNiu()){
						player_op_liangniu(gt, msg, pl);
					}
				}
			}
		}else{
			User currentPl = gt.getCurrentOperationPlayer();
			//
			if (currentPl == null) {
				gt.addTickError();
				// 错误大于300次自动清除桌子
				if (gt.getTickError() > 3000) {
					// 清除桌子
					// 先测试金币够不够
					for (int i = 0; i < gt.getPlayers().size(); i++) {
						User pl = gt.getPlayers().get(i);
						// player_left_table(pl);
						logger.error("current operation player not found contail player:"
								+ pl.getPlayerIndex());
					}

					logger.error("current operation player not found, tableID="
							+ gt.getTableID() + ",State=" + gt.getState()
							+ ",BackUpState=" + gt.getBackup_state()
							+ ",GameStartTime=" + gt.getGameStartTime()
							+ ",CurrentOperateIndex="
							+ gt.getCurrentOpertaionPlayerIndex());

					gt.clearGameState();
					gt.setState(GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE);
					gt.set_all_player_state(GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE);
					gt.setHandEndTime(System.currentTimeMillis());

					// 设为非法状态，系统会自动清理
					gt.setState(GameConstant.TABLE_STATE_INVALID);
					gt.reSetTickError();
				}
				return;
			}

			if (operation_over_time == 0) {
				operation_over_time = cfgService.getPara(
						ConfigConstant.OPERATION_OVER_TIME).getValueInt();
			}
		}
	}

	private void ivite_machines(GameTable gt, GameRoom gr) {

		// vip房间不要机器人
		if (gt.isVipTable())
			return;

		if (gt.getPlayerNum() == 0)
			return;

		if (gt.getMaxPlayer() == gt.getPlayerNum())
			return;

		ISystemConfigService conf = (ISystemConfigService) SpringService
				.getBean("sysConfigService");
		if (conf == null)
			return;
		int robotNum = conf.get_robotLimitNum();
		if (robotNum == 0) {
			return;
		}

		long ct = DateService.getCurrentUtilDate().getTime();

		for (User play : gt.getPlayers()) {
			// 如果有整个桌子等待玩家超过2秒并且有玩家等待超过5秒，立即邀请机器人
			if (play.getInRoomWaitOtherTime() == 0)
				continue;

			if (play.isRobot())
				continue;

			int xtime = 1;
			//int xtime = new Random().nextInt(3);//等待1 - 3秒
			
			// if (ct - play.getInRoomWaitOtherTime() > cfgService
			// .get_robotWaitLimitTime() * 1000) {
			if (ct - play.getInRoomWaitOtherTime() >= xtime * 1000) {
//				if (machineService != null) {
//					machineService.create_machine(gr, gt);
//					if(gt.getPlayerNum() >= gt.getMaxPlayer()){
//						machineService.reset_machinetime(gt);
//					}
//				}
				break;
			}
		}
	}

	/**
	 * 玩家离开
	 * @author  2017-11-15
	 * GameProcessThread.java
	 * @param pl
	 * @param gr
	 * void
	 */
	public void player_left_table(User pl, GameRoom gr) {
		if (pl == null)
			return;

		if (gr == null)
			return;
		//
		GameTable gt = gr.getTable(pl.getTableID(), pl.isPlayingSingleTable());
		if (gt == null) {
			return;
		}

		// 玩家不在桌子上
		pl.setOnTable(false);
		//
		if (gt.couldLeft(pl)) {
			//
			boolean found = gt.removePlayer(pl.getPlayerID());

			//
			if (found) {
				gr.setPlayerNum(gr.getPlayerNum() - 1);
				gt.addFreePos(pl.getTablePos());
			}

			// 如果vip离开桌子,全部清理
			if (gt.isVipTable()
					&& pl.getPlayerID().equals(gt.getCreatorPlayerID())) {
				PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();
				axk.opertaionID = GameConstant.OPT_ROOM_DISMISS;
				gt.sendMsgToTableExceptMe(axk, pl.getPlayerID());
				//
				gt.clear_all();
			}
			//
			if (gt.getPlayerNum() > 0) {
				// 如果还有玩家，通知他们有玩家离开
				if (found) {
					PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();
					axk.opertaionID = GameConstant.OPT_PLAYER_LEFT_TABLE;
					axk.playerName = pl.getPlayerName();
					axk.gold = pl.getGold();
					axk.headImg = pl.getHeadImg();
					axk.sex = pl.getSex();
					axk.playerID = pl.getPlayerID();
					axk.playerIndex = pl.getPlayerIndex();
					axk.canFriend = pl.getCanFriend();
					axk.tablePos = pl.getTablePos();
					axk.ip = pl.getClientIP();
					axk.location = pl.getLocation();
					axk.headImgUrl = pl.getHeadImgUrl();
					gt.sendMsgToTableExceptMe(axk, pl.getPlayerID());
				}
			}

			//
			if (gt.shouldRecycleTable()) {
				// death_machine(gt);
				// 空桌回收
				gt.setState(GameConstant.TABLE_STATE_INVALID);
			}

			pl.clear_game_state();
		} else {
			// 玩家正在游戏，保持数据，等待一定时间，等他回来
			if (gt.isVipTable()) {
				// 游戏暂停
				if ((gt.getState() != GameConstant.TABLE_STATE_PAUSED)
						&& gt.getState() != GameConstant.TABLE_STATE_WAITING_VIP_EXTEND_CARD//等待续卡
				){
						//&& (gt.getState() != GameConstant.TABLE_STATE_WAITING_PLAYER)) {
					// 保存当前状态
					gt.setBackup_state(gt.getState());

					Date ct = DateService.getCurrentUtilDate();
					gt.setPausedTime(ct.getTime());
					
					gt.setState(GameConstant.TABLE_STATE_PAUSED);
				}
			} else {
				// 普通房间设置成托管
				pl.setAutoOperation(1);
			}
			// 玩家状态，暂停
			pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_PAUSED);
			//
			// 玩家离开
			PlayerOperationNotifyMsg msg = new PlayerOperationNotifyMsg();
			msg.operationType = GameConstant.DDZ_OPT_OFFLINE;
			msg.playerTablePos = pl.getTablePos();
			gt.sendMsgToTableExceptMe(msg, pl.getPlayerID());
		}

		// debug,如果玩家都离开了，把桌子回收；
		if (gt.isAllPlayerLeft()) {
			if (gt.isVipTable()) {
				// 不马上结束，保留一段时间
				// vip_table_end(gr, gt);
			} else {
				gt.clear_all();
				gt.setState(GameConstant.TABLE_STATE_INVALID);
			}
		}
	}

	/**
	 * 检测玩家昵称是否含有Emoji字符，有就进行清理
	 */
	private void checkPlayerNickName(GameTable gt) {
		if (null == gt) {
			return;
		}

		List<User> plist = gt.getPlayers();
		if (null == plist) {
			return;
		}

		// 过滤表情字符
		Pattern emoji = Pattern
				.compile(
						"[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\ud83e\udd00-\ud83e\udfff]|[\u2600-\u27ff]",
						Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
		for (User pl : plist) {
			
			Matcher emojiMatcher = emoji.matcher(pl.getPlayerName());
			
			if (emojiMatcher.find()) {
				logger.error("【电子庄牛牛】玩家：" + pl.getPlayerIndex() + "昵称："
						+ pl.getPlayerName() + "包含Emoji字符，系统自动修改为默认昵称【微信用户】");
				String userName = pl.getPlayerName();
				if (userName.equals("")) {
					userName = "微信用户";
				}
				if (userName.length() > 12) {
					userName = userName.substring(0, 11);
				}
				
				PlayerOpertaionMsg msg = new PlayerOpertaionMsg();
//				msg.playerName = "微信用户";
				msg.playerName = userName;

				ValidateMsgAck ack = new ValidateMsgAck(
						MsgCmdConstant.GAME_USER_UPDATE_NICKNAME_ACK);
				// 修改数据库
				playerService.updatePlayer(pl, msg, ack);

			}
		}
	}

	private void game_over_log(GameTable gt) {
		// 检查玩家昵称是否包含非法字符
		checkPlayerNickName(gt);
		
		boolean isClub=gt.getClubCode() != 0;//判断是否是俱乐部内牌桌

		// 写日志
		List<User> plist = gt.getPlayers();
		if (gt.isVipTable()) {
			//
			for (int i = 0; i < plist.size(); i++) {
				User pl = plist.get(i);
				VipRoomRecord vrr = pl.getVrr();
				if (vrr != null) {
					int num = 0;
					for (int j = 0; j < plist.size(); j++) {
						User plj = plist.get(j);
						String pljID = plj.getPlayerID();
						if (plj.getPlayerID().equals(pl.getPlayerID()) == false) {
							if(isClub){
								if(pljID.equals(vrr.getPlayer2ID())){
									vrr.setPlayer2Name(plj.getPlayerName());
									vrr.setScore2(plj.getWinLoseTotal());
									vrr.setPlayer2Index(plj.getPlayerIndex());
								}else if(pljID.equals(vrr.getPlayer3ID())){
									vrr.setPlayer3Name(plj.getPlayerName());
									vrr.setScore3(plj.getWinLoseTotal());
									vrr.setPlayer3Index(plj.getPlayerIndex());
								}else if(pljID.equals(vrr.getPlayer4ID())){
									vrr.setPlayer4Name(plj.getPlayerName());
									vrr.setScore4(plj.getWinLoseTotal());
									vrr.setPlayer4Index(plj.getPlayerIndex());
								}else if(pljID.equals(vrr.getPlayer5ID())){
									vrr.setPlayer5Name(plj.getPlayerName());
									vrr.setScore5(plj.getWinLoseTotal());
									vrr.setPlayer5Index(plj.getPlayerIndex());
								}
							}else{
								if (num == 0) {
									vrr.setPlayer2Name(plj.getPlayerName());
									vrr.setScore2(plj.getWinLoseTotal());
								} else if (num == 1) {
									vrr.setPlayer3Name(plj.getPlayerName());
									vrr.setScore3(plj.getWinLoseTotal());
								} else if (num == 2) {
									vrr.setPlayer4Name(plj.getPlayerName());
									vrr.setScore4(plj.getWinLoseTotal());
								} else if (num == 3) {
									vrr.setPlayer5Name(plj.getPlayerName());
									vrr.setScore5(plj.getWinLoseTotal());
								} else if (num == 4) {
									vrr.setPlayer6Name(plj.getPlayerName());
									vrr.setScore6(plj.getWinLoseTotal());
								}
							}
							//
							num++;
						} else {
							vrr.setPlayer1Name(pl.getPlayerName());
							vrr.setPlayer1ID(pl.getPlayerID());
							vrr.setPlayer1Index(pl.getPlayerIndex());
							vrr.setScore1(pl.getWinLoseTotal());
						}
					}
					
					if(isClub){//判断是否是俱乐部内牌桌
						// 庄
						vrr.setPlayer6Name(gt.getCreatorPlayerName());
						vrr.setScore6(gt.getDealerWinLoseTotal());
						vrr.setPlayer6ID(gt.getCreatorPlayerID());
					}

					playerService.updateVipRoomRecord(vrr);
				}
				
			}

			//更新代理开房记录//代开优化 cc modify 2017-9-26
			if(gt.getProxyCreator() != null && gt.getClubCode() == 0){
				List<ProxyVipRoomRecord> pvrrs = gt.getProxyCreator().getPvrrs();
				if (pvrrs != null && pvrrs.size()>0) {
					for(ProxyVipRoomRecord pvrr:pvrrs){
						if(pvrr.getRoomID().equals(gt.getTableID())){
							pvrr.setPlayer1Name("");
							pvrr.setPlayer1Index(-1);
							pvrr.setScore1(0);
							pvrr.setPlayer2Name("");
							pvrr.setPlayer2Index(-1);
							pvrr.setScore2(0);
							pvrr.setPlayer3Name("");
							pvrr.setPlayer3Index(-1);
							pvrr.setScore3(0);
							pvrr.setPlayer4Name("");
							pvrr.setPlayer4Index(-1);
							pvrr.setScore4(0);
							for (int i = 0; i < plist.size(); i++) {
								User pl = plist.get(i);
								if (i == 0) {
									pvrr.setPlayer1Name(pl.getPlayerName());
									pvrr.setPlayer1Index(pl.getPlayerIndex());
									pvrr.setScore1(pl.getWinLoseTotal());
								} else if (i == 1) {
									pvrr.setPlayer2Name(pl.getPlayerName());
									pvrr.setPlayer2Index(pl.getPlayerIndex());
									pvrr.setScore2(pl.getWinLoseTotal());
								} else if (i == 2) {
									pvrr.setPlayer3Name(pl.getPlayerName());
									pvrr.setPlayer3Index(pl.getPlayerIndex());
									pvrr.setScore3(pl.getWinLoseTotal());
								}else{
									pvrr.setPlayer4Name(pl.getPlayerName());
									pvrr.setPlayer4Index(pl.getPlayerIndex());
									pvrr.setScore4(pl.getWinLoseTotal());
								}
							}
							logger.info(gt.getGameId()+"代开VIP房更新游戏记录，房间ID【" + gt.getVipTableID()
									+ "】，代理ID【" + gt.getProxyCreator().getPlayerIndex() + "】，代理昵称【"
									+ gt.getProxyCreator().getPlayerName() + "】，牌桌玩家得分情况------------>玩家1【"
									+ pvrr.getPlayer1Name() + "】，得分【" + pvrr.getScore1()
									+ "】；玩家2【"
									+ pvrr.getPlayer2Name() + "】，得分【" + pvrr.getScore2()
									+ "】；玩家3【" + pvrr.getPlayer3Name() + "】，得分【"
									+ pvrr.getScore3() + "】");
							playerService.updateProxyVipRoomRecord(pvrr);	
						}
					}
				}
			}
		} else {
			User px0 = null;
			User px1 = null;
			User px2 = null;
			User px3 = null;
			if (plist.size() > 0)
				px0 = plist.get(0);
			if (plist.size() > 1)
				px1 = plist.get(1);
			if (plist.size() > 2)
				px2 = plist.get(2);
			if (plist.size() > 3)
				px3 = plist.get(3);

			NormalGameRecord ngr = new NormalGameRecord();
			ngr.setRoomID(gt.getTableID());
			ngr.setRoomType(gt.getRoomID());
			//
			if (px0 != null) {
				ngr.setPlayer1Index(px0.getPlayerIndex());
				ngr.setGameType1(px0.getFanType());
				ngr.setScore1(px0.getWinNum());
				ngr.setGameMultiple1(px0.getFanNum());
			}

			//
			if (px1 != null) {
				ngr.setPlayer2Index(px1.getPlayerIndex());
				ngr.setGameType2(px1.getFanType());
				ngr.setScore2(px1.getWinNum());
				ngr.setGameMultiple2(px1.getFanNum());
			}

			//
			if (px2 != null) {
				ngr.setPlayer3Index(px2.getPlayerIndex());
				ngr.setGameType3(px2.getFanType());
				ngr.setScore3(px2.getWinNum());
				ngr.setGameMultiple3(px2.getFanNum());
			}

			//
			if (px3 != null) {
				ngr.setPlayer4Index(px3.getPlayerIndex());
				ngr.setGameType4(px3.getFanType());
				// ngr.setScore4(px3.getWinLoseGoldNum());
				ngr.setScore4(px3.getWinNum());
				ngr.setGameMultiple4(px3.getFanNum());
			}

			ngr.setHostIndex(px0.getPlayerIndex());
			Date tt = new Date(gt.getHandStartTime());
			ngr.setStartTime(tt);
			ngr.setEndTime(DateService.getCurrentUtilDate());
			//
			this.playerService.createNormalGameRecord(ngr);
		}
	}

	/**
	 * gameover
	 * @author  2017-11-15
	 * GameProcessThread.java
	 * @param gt
	 * @param gr
	 * void
	 */
	private void game_over(GameTable gt, GameRoom gr) {
		gt.setSingleRoundOver(true);
		// 发送游戏结束界面
		PlayerGameOverMsgAck gov = new PlayerGameOverMsgAck();
		gov.roomID = gt.getRoomID();
		gov.gameCount = gt.getJuCount();
		gov.dealerPos = gt.getDealerPos();
		gov.playerNum = gt.getPlayerNum();
		
		List<User> pls = gt.getStakeCoinPlayers();
		if(gt.getClubCode() == 0){
			pls.add(gt.getPlayerByTablePos(gt.getDealerPos()));
		}
		
		gov.init_players(pls, gt);
		
		if (gt.isVipTable()) {
			gov.isVipTable = 1;
			if (vip_game_over_enter_next_turn == 0) {
				vip_game_over_enter_next_turn = cfgService.getPara(
						ConfigConstant.VIP_GAME_OVER_ENTER_NEXT_TURN)
						.getValueInt();
			}
			gov.readyTime = vip_game_over_enter_next_turn;
		} else {
			gov.isVipTable = 0;
			gov.readyTime = 10;
		}
		gov.clubCode = gt.getClubCode();

		// 2人vip
		for (int i = 0; i < gt.getPlayers().size(); i++) {
			User pl = gt.getPlayers().get(i);
			pl.setTwoVipOnline(false);
			pl.setPlayerType(0);
			playerService.updatePlayerType(pl.getPlayerID(), 0);
		}

		// vip输赢累加
		if (gt.isVipTable()) {
			for (int i = 0; i < pls.size(); i++) {
				User pl = pls.get(i);
				// 带正负号
				pl.setWinLoseTotal(pl.getWinLoseTotal() + pl.getWinLoseCurrCoinNum());
			}
		}
		// 日志处理
		game_over_log(gt);
		
		// VIP最后一把，圈数用光
		if (gt.isVipTableTimeOver()) {
			gov.isOver = 1;
		} else {
			// 不是最后一把，单局结算
			gov.isOver = 0;
		}
		gt.sendMsgToTable(gov);
		gt.clearGameState();
		gt.set_all_player_state(GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE);
		// 然后，游戏进入等待玩家继续界面
		gt.setState(GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE);
		
		// 机器人处理
		if (!gt.isVipTable()) {
			for (int i = 0; i < pls.size(); i++) {
				User pl = pls.get(i);

				if (!pl.getOnTable()) {
					player_left_table(pl, gr);
				} else if (pl.getGold() < gr.minGold) {
					player_left_table(pl, gr);
				} else if (pl.isRobot()) {
					pl.dcRobotgamenum();
					if (pl.getRobotgamenum() <= 0) {
						player_left_table(pl, gr);
					}
				}
			}
		}

		gt.setAllRobotReady();
		gt.setHandEndTime(System.currentTimeMillis());
		
		// 通知俱乐部状态
		if(gt.getClubCode() > 0){
			playerService.sendMsgToClubPlayer(gt.getClubCode(), playerService.newGetSingleClubInfoMsgAck(gt.getClubCode()));
		}
	}

	public void proxyTableOutTime(GameTable gt){		
		List<ProxyVipRoomRecord> pvrrs = gt.getProxyCreator().getPvrrs();
		if(pvrrs != null){
			for(int i=pvrrs.size()-1;i>=0;i--){
				ProxyVipRoomRecord pvrr = pvrrs.get(i);
				if(pvrr.getRoomID().equals(gt.getTableID())){
					boolean isUpdateProxyList = false;//是否需要前台刷新代开记录列表
					String tableEndWay = "房间未开局，超时解散";
					isUpdateProxyList = true;//游戏未开始，玩家解散房间，代理刷新代开记录列表
					pvrr.setVipState(GameConstant.PROXY_OUTTIME_END_GAME);
					pvrr.setEndWay(tableEndWay);
					pvrrs.remove(i);
					List<User> players = gt.getPlayers();
					if(players.size()>0){
						pvrr.setPlayer1Name("");
						pvrr.setPlayer1Index(-1);
						pvrr.setPlayer2Name("");
						pvrr.setPlayer2Index(-1);
						pvrr.setPlayer3Name("");
						pvrr.setPlayer3Index(-1);
						pvrr.setPlayer4Name("");
						pvrr.setPlayer4Index(-1);
						for(int j=0;j<players.size();j++){
							if(j==0){
								pvrr.setPlayer1Name(players.get(j).getPlayerName());
								pvrr.setPlayer1Index(players.get(j).getPlayerIndex());
							}else if(j==1){
								pvrr.setPlayer2Name(players.get(j).getPlayerName());
								pvrr.setPlayer2Index(players.get(j).getPlayerIndex());
							}else if(j==2){
								pvrr.setPlayer3Name(players.get(j).getPlayerName());
								pvrr.setPlayer3Index(players.get(j).getPlayerIndex());
							}else{
								pvrr.setPlayer4Name(players.get(j).getPlayerName());
								pvrr.setPlayer4Index(players.get(j).getPlayerIndex());
							}
						}
					}
					this.playerService.updateProxyVipRoomRecord(pvrr);
					this.playerService.endProxyVipRoomRecord(pvrr);
					if(isUpdateProxyList){
						//给代理发刷新列表消息
						ProxyVipRoomRecordMsg prvm = new ProxyVipRoomRecordMsg();
						prvm.playerID = gt.getProxyCreator().getPlayerID();
						prvm.gameID = gt.getGameId();
						playerService.getProxyVipRoomRecord(prvm, gt.getProxyCreator().getSession());
					}
				}
			}
		}
		gt.getProxyCreator().setPvrrs(pvrrs);		
	}
	//
	private void game_table_tick(GameTable gt, GameRoom gr) {
		Date ct = DateService.getCurrentUtilDate();
		long ctt = ct.getTime();
		int state = gt.getState();

		// 机器人桌，或者空桌回收
		if (gt.shouldRecycleTable()) {
			gt.setState(GameConstant.TABLE_STATE_INVALID);
			return;
		}

		if (state == GameConstant.TABLE_STATE_WAITING_PLAYER) {
			// 桌子等待超过1秒
			if (gt.getNewWailtOtherPlayerTime() != 0
					&& ctt - gt.getNewWailtOtherPlayerTime() > 1000) {
				ivite_machines(gt, gr);
			}
			//
			long delta = ctt - gt.getVipCreateTime();
			if (vip_room_create_wait_start == 0) {
				vip_room_create_wait_start = cfgService.getPara(
						ConfigConstant.VIP_ROOM_CREATE_WAIT_START)
						.getValueInt();
			}
	        
			if (gt.isVipTable() && delta > vip_room_create_wait_start * 1000)// vip房间如果超过一定时间没有开始游戏，房间结束
			{
				if(gt.getClubCode() > 0 && gt.getClubTableState() == 1){
					// 俱乐部固定牌桌不结束
					
				}else{
					// 通知玩家离开房间
					PlayerOperationNotifyMsg msg = new PlayerOperationNotifyMsg();
					msg.operationType = GameConstant.DDZ_OPT_NO_START_CLOSE_VIP;
					for(User pl:gt.getPlayers()){
						msg.playerTablePos = pl.getTablePos();
						if(gt.getTableID() == pl.getTableID()){
							GameContext.gameSocket.send(pl.getSession(), msg);
						}
						//增加超时玩家房卡输出
						if(pl.getItems() != null && pl.getItems().size() > 0){
							logger.info(gt.getGameId()+"vip房间如果超过一定时间没有开始游戏，房间结束：房间ID【" + gt.getVipTableID()
									+ "】当前玩家【"+pl.getPlayerIndex()+"】【"+pl.getPlayerName()+"】房卡  = "+pl.getItems().get(0).getItemNum()+" ");
						}
						
					}

					if(gt.getProxyCreator() != null){	
						this.proxyTableOutTime(gt);
					}
				
					gt.clear_all();
					gt.setState(GameConstant.TABLE_STATE_INVALID);
					logger.info("#####"+gt.getGameId()+"vip房间如果超过一定时间没有开始游戏，房间结束：房间ID【" + gt.getVipTableID()
							+ "】，创建时间【" + gt.getVipCreateTime() + "】，当前时间【" + ctt
							+ "】，等待时间【" + delta + "】，规定时间【"
							+ vip_room_create_wait_start + "】");
				}
			}
		} else if (state == GameConstant.TABLE_STATE_READY_GO) {
			long delta = ctt - gt.getReadyTime();

			if (delta > 1000) {//发牌动画时间
				gameStart(gt, gr);
			}
		} else if (state == GameConstant.TABLE_WAITING_CLIENT_SHOW_INIT_CARDS) {
			long delta = ctt - gt.getReadyTime();
			if (delta > 1000) {
				// 桌子进入开打状态
				gt.setState(GameConstant.TABLE_STATE_PLAYING);
				
				if(gt.getClubCode() > 0){
					player_xiazhu_notify(gt, gr);
				}else{
					player_zhuang_notify(gt, gr);
				}
			}
		} else if (state == GameConstant.TABLE_STATE_PLAYING) {
			playing_table_tick(gt, ctt, gr);
		} else if (state == GameConstant.TABLE_STATE_SHOW_GAME_OVER_SCREEN) {
			long delta = ctt - gt.getHandEndTime();
			
			// time1代表进桌等待时间，time2代表押注时间，time3代表基础发牌时间，time4代表单人发牌时间，每多一个玩家多算一份时间，time5代表亮牛等待时间，time5代表亮牛后等待时间
			GameConfig gameConfig = GameContext.tableLogic_nndzz.getGameConfig();
			int operationTime = gameConfig.getOpt_time6();
			int time = operationTime * 1000;
			// 游戏结束后3秒，给客户端播放排名
			if (delta >= time) {
				logger.info("【电子庄牛牛】游戏结束后3秒，给客户端播放排名");
				game_over(gt, gr);
			}
		} else if (state == GameConstant.TABLE_STATE_PAUSED) {
			// TODO 代理开房记录日志//代开优化 cc modify 2017-9-26
			if(gt.getProxyCreator() != null && gt.getPlayers().size() == 0){
				long delta = ctt - gt.getVipCreateTime();
				if (vip_room_create_wait_start == 0) {
					vip_room_create_wait_start = cfgService.getPara(
							ConfigConstant.VIP_ROOM_CREATE_WAIT_START)
							.getValueInt();
				}
				if (gt.isVipTable() && delta > vip_room_create_wait_start * 1000){
					this.proxyTableOutTime(gt);
					
					// this.sendMsgToTable(msg);
	
					logger.info("#####vip代开房间如果超过一定时间没有开始游戏，房间结束：房间ID【" + gt.getVipTableID()
							+ "】，代开者【"+gt.getProxyCreator().getPlayerName()+"】，创建时间【" + gt.getVipCreateTime() + "】，当前时间【" + ctt
							+ "】，等待时间【" + delta + "】，规定时间【"
							+ vip_room_create_wait_start + "】");
	
					gt.clear_all();
				}
			}else{				
				long delta = ctt - gt.getPausedTime();
				// Vip场有玩家中途离场，房间存活时间
				if (vip_paused_exist_time == 0) {
					vip_paused_exist_time = cfgService.getPara(
							ConfigConstant.VIP_PAUSED_EXIST_TIME).getValueInt();
				}
				// 超时，关闭房间
				if (delta > vip_paused_exist_time * 1000) {
					// GameRoom gr = this.getRoom(gt.getRoomID());
	
					logger.info("【电子庄牛牛】有玩家中途离场房间存活超时，VIP房间:" + gt.getVipTableID()
							+ "结束 delta:" + delta + " vip_paused_exist_time:"
							+ vip_paused_exist_time + " state:" + gt.getState());
	
					gt.setTableEndWay("超时解散");
					vip_table_end(gt, gr);
					gt.clear_all();
					gt.setState(GameConstant.TABLE_STATE_INVALID);
				}
				
				if( gt.isVipTableTimeOver() && gt.isSingleRoundOver() ) {
					if (delta >= vip_game_over_enter_next_turn * 1000&& gt.isVipTable())// vip牌局结束15秒，自动结束（20171122）
					{
						not_extend_vip_table(gt, gr, false);
					}
					return;
				}
				
				if( gt.getJuCount() > 0 && gt.isSingleRoundOver() && gt.pauseContinue()) {
					if (delta >= vip_game_over_enter_next_turn * 1000
							&& gt.isVipTable())// vip牌局结束15秒，自动进下一把
					{
		
						if (vip_table_check_next_hand(gt, gr)) {
							logger.info("VIP房间ID【" + gt.getVipTableID() + "】结算等待时间超过"
									+ delta + "自动开始下一局");
							vip_next_hend(gt, ctt);
						}
					}
				}		
	
	//			
				// 看看桌子上玩家是否都已经到齐
				if (gt.pauseContinue()) {
					if (gt.getState() == GameConstant.TABLE_STATE_PLAYING) {
						// 断线回来，如果当前桌子子状态为0，说明没有等待状态，直接提示当前玩家行动
	//					if (gt.getPlaySubstate() == 0)//
	//					{
							// 通知玩家行动
//							re_notify_current_operation_player(gt, gr);
	//					} else {
	//						// 否则就把等待时间设置为当前，系统等待一定时间后，提示玩家进行应该进行的动作，比如有玩家吃碰等
	//						Date dt = DateService.getCurrentUtilDate();
	//						gt.setWaitingStartTime(dt.getTime());
	//					}
					}else{
						
						// 人满不开局，需要确认准备状态
	//					// 如等待玩家中，满了就开始，补漏处理
	//			        if (gt.isFull() && gt.getState() == GameConstant.TABLE_STATE_WAITING_PLAYER) {
	//			        	logger.info("------->牌局应该开始，但由于断线没开始，手动设置开始，房间ID【"+gt.getVipTableID()+"】");
	//			            gt.setState(GameConstant.TABLE_STATE_READY_GO);
	//			        }
					}
				}
			}
		}
		else if (state == GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE) {
			if (gt.getPlayerNum() < gt.getMaxPlayer()
					&& gt.getNewWailtOtherPlayerTime() != 0
					&& ctt - gt.getNewWailtOtherPlayerTime() > 2000) {
				ivite_machines(gt, gr);
			}

			//
			long delta = ctt - gt.getHandEndTime();
			
			GameConfig gameConfig = GameContext.tableLogic_nndzz.getGameConfig();
			int operationTime = gameConfig.getOpt_time1();
			int time = operationTime * 1000;
			
			// 人满随时开始，如果大家都点击了继续
			if (gt.getReadyPlayerNum() == gt.getPlayerCount()) {
				// 把游戏数据先清理掉，保留玩家
				gt.clearGameState();
				// 人满开始下一局
				gt.setReadyTime(ctt);
				gt.setState(GameConstant.TABLE_STATE_READY_GO);
			} else if (delta >= time
					&& gt.isVipTable())// vip牌局结束15秒，自动进下一把
			{
				logger.info("【电子庄牛牛】VIP房间结算等待时间超过" + delta + "自动开始下一局");

				if (gt.isVipTableTimeOver()) {
					// GameRoom gr = getRoom(gt.getRoomID());

					logger.info("【电子庄牛牛】vip圈数用光，VIP房间:" + gt.getVipTableID() + "结束");

					gt.setTableEndWay("牌局结束，正常解散");
					vip_table_end(gt, gr);
				} else {
					if (vip_table_check_next_hand(gt, gr)) {
						vip_next_hend(gt, ctt);
					}
				}
			}
			// 游戏结束后60秒，等60秒继续
			else if (delta >= 30 * 1000) {
				if (!gt.isVipTable()) {
					logger.info("【电子庄牛牛】房间结算等待时间超过" + delta + "房间结束");

					// 把未准备好的玩家删掉
					// gt.set_not_ready_player_ready();
					gt.remove_not_ready_player();
					if (gt.getPlayerNum() == gt.getMaxPlayer()) {
						next_hend(gt, ctt);
					} else if (gt.getReadyPlayerNum() > 0) {
						// 把游戏数据先清理掉，保留玩家
						gt.clearGameState();
						// 超过满开始下一局
						gt.setState(GameConstant.TABLE_STATE_WAITING_PLAYER);

					} else {
						// 如果没有人玩，状态非法，后面的逻辑收掉这个桌子
						gt.clear_all();
					}
				}
			}
		}
	}
	
	/**
	 * 玩家当庄提醒
	 * @param gt
	 * @param gr
	 */
	private void player_zhuang_notify(GameTable gt, GameRoom gr) {
		PlayerOperationNotifyMsg msg = new PlayerOperationNotifyMsg();
		msg.operationType = GameConstant.NNDZZ_OPT_ZHUANG;
		
		// time1代表进桌等待时间，time2代表押注时间，time3代表基础发牌时间，time4代表单人发牌时间，每多一个玩家多算一份时间，time5代表亮牛等待时间，time6代表亮牛后等待时间，time7代表贵宾房选择当庄时间
		GameConfig gameConfig = GameContext.tableLogic_nndzz.getGameConfig();
		int operationTime = gameConfig.getOpt_time7();
		
		int dealerPos = gt.getDealerPos();
		User dealer = gt.getPlayerByTablePos(dealerPos);
		msg.playerTablePos = dealerPos;
		msg.operationTime = operationTime;
		for(User pl : gt.getPlayers()){
			msg.gold = pl.getVipTableGold();
			GameContext.gameSocket.send(pl.getSession(), msg);
		}
		
		logger.info("【电子庄牛牛】，房间【"+gt.getVipTableID()+"】提醒玩家当庄，玩家昵称【"+dealer.getPlayerName()
						+"】，玩家ID【"+dealer.getPlayerIndex()+"】，tablePos【"+dealer.getTablePos()+"】");
		
		//记录状态
		MahjongCheckResult result = new MahjongCheckResult();
		result.opertaion = msg.operationType;
		result.operationTime = operationTime;
		result.playerTableIndex = dealerPos;
		gt.setWaitingPlayerOperate(result);		
		
		long ctt = System.currentTimeMillis();
		gt.setWaitingStartTime(ctt);
		gt.setPlaySubstate(GameConstant.TABLE_SUB_STATE_ZHUANG);
	}

	/**
	 * 玩家下注提醒
	 * @param gt
	 * @param gr
	 */
	private void player_xiazhu_notify(GameTable gt, GameRoom gr) {
		PlayerOperationNotifyMsg msg = new PlayerOperationNotifyMsg();
		msg.operationType = GameConstant.NNDZZ_OPT_PINGBEI;
		
		// time1代表进桌等待时间，time2代表押注时间，time3代表基础发牌时间，time4代表单人发牌时间，每多一个玩家多算一份时间，time5代表亮牛等待时间，time5代表亮牛后等待时间
		GameConfig gameConfig = GameContext.tableLogic_nndzz.getGameConfig();
		int operationTime = gameConfig.getOpt_time2();
		
		// 加载下注筹码
		List<Integer> coinItems = gt.getCoinItemsByLevel();
		logger.info("【电子庄牛牛】--->【通知所有玩家可以下注了】，操作时间："+operationTime+"秒，当前桌次筹码组："+coinItems);
		
		msg.operationTime = operationTime;
		msg.coinItems = coinItems;
		
		Map<Integer, List<Integer>> allowStakeItems = new HashMap<Integer, List<Integer>>();
		for(User pl : gt.getPlayers()){
			msg.playerTablePos = pl.getTablePos();
			
			if(gt.getClubCode() > 0){
				// 俱乐部
				// 加载可以下注的筹码
				msg.opValue = 0;
				List<Integer> onesCoinItems = gt.getPlayerAllowCoinItems(coinItems, pl);
				allowStakeItems.put(pl.getTablePos(), onesCoinItems);
				msg.allowCoinItems = onesCoinItems;
			}else{
				// VIP房
				if(pl.getTablePos() == gt.getDealerPos()){
					// 庄家
					msg.opValue = 1;
				}else{
					// 闲家
					msg.opValue = 0;
					List<Integer> onesCoinItems = gt.getPlayerAllowCoinItems(coinItems, pl);
					allowStakeItems.put(pl.getTablePos(), onesCoinItems);
					msg.allowCoinItems = onesCoinItems;
					
				}
			}
			msg.gold = pl.getVipTableGold();
			GameContext.gameSocket.send(pl.getSession(), msg);
		}
		
		//记录状态
		MahjongCheckResult result = new MahjongCheckResult();
		result.opertaion = msg.operationType;
		result.stakeItems = coinItems;
		result.allowStakeItems = allowStakeItems;
		result.operationTime = operationTime;
		gt.setWaitingPlayerOperate(result);		
		
		long ctt = System.currentTimeMillis();
		gt.setWaitingStartTime(ctt);
		gt.setPlaySubstate(GameConstant.TABLE_SUB_STATE_XIAZHU);
	}
	
	/**
	 * 发牌
	 * @param gt
	 * @param gr
	 */
	private void sendCardToTable(GameTable gt, GameRoom gr){
		List<User> pls = gt.getStakeCoinPlayers();
		if(pls.size() == 0){
			// TODO 都不压游戏结束
			game_over(gt, gr);
			return;
		}
		
		// 俱乐部扣卡
		if(gt.getClubCode() > 0){
			GameConfig config = GameContext.tableLogic_nndzz.getGameConfig();
			Map<String, Integer> mapKa = config.getPokerClubWanfa(gt.getRoomModel());
			logger.info("==========>扣卡配置："+mapKa);
			int kaKouNum = mapKa.get("cost_self_"+gt.getRoomLevel());
			if (gt.getProxyState() == 2){
		        kaKouNum = mapKa.get("cost_aa_"+gt.getRoomLevel());
			}else if(gt.getProxyState() == 1){
				kaKouNum = mapKa.get("cost_proxy_"+gt.getRoomLevel());
			}
			if (gt.isVipTable()) {
				int clubCode = gt.getClubCode();
				if(clubCode > 0){
					TClub club = playerService.getClubByClubCode(clubCode);
					Map<Integer, Boolean> kouKaMap = gt.getKouKaMap();
					
					for(User pl : pls){
						int playerIndex = pl.getPlayerIndex();
						if(!kouKaMap.containsKey(playerIndex)){
							logger.info("俱乐部【"+clubCode+"】牌桌扣卡，房间ID【"+gt.getVipTableID()+"】，扣卡玩家ID【"+pl.getPlayerIndex()
									+"】，扣卡玩家昵称【"+pl.getPlayerName()+"】，扣卡张数【"+kaKouNum+"】");
							playerService.useClubFangka(gt.getVipTableID(), club, pl, kaKouNum, gt.getGameId());
							kouKaMap.put(playerIndex, true);
							gt.setKouKaMap(kouKaMap);
						}
					}
				}
			}
		}
		
		// 有人押注，发牌
		gt.sendCards();
	}
	
	
	/**
	 * 亮牛提醒，直接显示输赢
	 * @param gt
	 * @param gr
	 */
	private void player_liangniu_notify(GameTable gt, GameRoom gr) {
		// time1代表进桌等待时间，time2代表押注时间，time3代表基础发牌时间，time4代表单人发牌时间，每多一个玩家多算一份时间，time5代表亮牛等待时间，time5代表亮牛后等待时间
		GameConfig gameConfig = GameContext.tableLogic_nndzz.getGameConfig();
		int operationTime = gameConfig.getOpt_time5();
		
		logger.info("【电子庄牛牛】--->【通知所有玩家可以亮牛了】，操作时间："+operationTime+"秒");
		
		PlayerOperationNotifyMsg msg = new PlayerOperationNotifyMsg();
		msg.operationType = GameConstant.NNDZZ_OPT_LIANGNIU;
		msg.operationTime = operationTime;
		
		//记录状态
		MahjongCheckResult result = new MahjongCheckResult();
		result.opertaion = msg.operationType;
		result.operationTime = operationTime;
		gt.setWaitingPlayerOperate(result);		
		//给所有玩家发消息

		List<User> pls = gt.getStakeCoinPlayers();
		if(gt.getClubCode() == 0){
			pls.add(gt.getPlayerByTablePos(gt.getDealerPos()));
		}
		for(User pl : pls){
			msg.playerTablePos = pl.getTablePos();
			msg.gold = pl.getVipTableGold();
			GameContext.gameSocket.send(pl.getSession(), msg);
		}
		
		long ctt = System.currentTimeMillis();
		gt.setWaitingStartTime(ctt);
		gt.setPlaySubstate(GameConstant.TABLE_SUB_STATE_LIANGNIU);
	}
	
	public void next_hend(GameTable gt, long ctt) {
		List<User> plist = gt.getPlayers();

		//
		for (int i = 0; i < plist.size(); i++) {
			User pl = plist.get(i);
			if (pl.getGameState() == GameConstant.USER_GAME_STATE_IN_TABLE_READY)
				continue;
			//
			pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
			//
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();

			ack.result = ErrorCodeConstant.CMD_EXE_OK;
			//
			if (gt.isVipTable()) {
				ack.init_players(gt.getPlayers(), true);
				ack.gold = pl.getVipTableGold();
				ack.tableRules = gt.getTableRuleOption();
				ack.roomType = gt.game_room.roomType; // kn add
			} else {
				ack.init_players_time_hend(gt.getPlayers());
				ack.gold = pl.getGold();
				ack.roomType = gt.game_room.roomType; // kn add
			}
			//
			ack.tablePos = pl.getTablePos();
			ack.roomID = gt.getRoomID();

			ack.coinBaseNum = gt.getCoinBaseNum();//gt.getDizhu();
			ack.juCount = gt.getJuCount();
			ack.totalJuNum = gt.getTotalJuNum();
			ack.playerNum = gt.getPlayerCount();
			ack.vipTableID = gt.getVipTableID();
			ack.creatorID = gt.getCreatorPlayerID();
			ack.creatorName = gt.getCreatorPlayerName();
			if(gt.getProxyCreator() != null){
				ack.proxyName = gt.getProxyCreator().getPlayerName();
			}
			ack.tablePassword = gt.getVipPswMd5();
			ack.isSelectSite = gt.getIsSelectSite() ;
			ack.gameStart = gt.isStartGame() ? 1 : 0;
			ack.clubCode = gt.getClubCode();
			// 进入桌子，返回消息给玩家
			GameContext.gameSocket.send(pl.getSession(), ack);

			// 给桌子上的其他玩家发个进入新玩家的消息
			PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();
			axk.opertaionID = GameConstant.OPT_TABLE_ADD_NEW_PLAYER;
			axk.playerName = pl.getPlayerName();
			axk.gold = pl.getVipTableGold();
			axk.headImg = pl.getHeadImg();
			axk.sex = pl.getSex();
			axk.tablePos = pl.getTablePos();
			axk.playerID = pl.getPlayerID();
			axk.playerIndex = pl.getPlayerIndex();
			axk.canFriend = pl.getCanFriend();
			axk.ip = pl.getClientIP();
			axk.location = pl.getLocation();
			axk.headImgUrl = pl.getHeadImgUrl();
			gt.sendMsgToTableExceptMe(axk, pl.getPlayerID());

		}

		// 把游戏数据先清理掉，保留玩家
		gt.clearGameState();
		// 人满开始下一局
		gt.setReadyTime(ctt);
		gt.set_all_player_state(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
		gt.setState(GameConstant.TABLE_STATE_READY_GO);
	}

	private void vip_next_hend(GameTable gt, long ctt) {
		next_hend(gt, ctt);
	}

	/**
	 * 检测是否玩家输光，第一局玩家扣金币
	 * @author  2017-11-15
	 * GameProcessThread.java
	 * @param gt
	 * @param gr
	 * @return
	 * boolean
	 */
	private boolean vip_table_check_next_hand(GameTable gt, GameRoom gr) {
		if (gt.isVipTable() == false)
			return false;
		List<User> plist = gt.getPlayers();

		if (gt.getJuCount() == 0)
			return true;

		// GameRoom gr = getRoom(gt.getRoomID());
		if (gr == null) {
			gt.setState(GameConstant.TABLE_STATE_INVALID);
			return false;
		}
		// 先测试金币够不够
		for (int i = 0; i < plist.size(); i++) {
			User pl = plist.get(i);
			if (pl.getVipTableGold() < gr.minGold)// vip 桌子当玩家金币小于minGold就解散
			{
				gt.setState(GameConstant.TABLE_STATE_INVALID);

				logger.info("【电子庄牛牛】玩家金币不足，VIP房间:" + gt.getVipTableID() + "结束");

				gt.setTableEndWay("牌局结束，正常结束");
				vip_table_end(gt, gr);
				return false;
			}
		}
		//
		if (gt.isVipTableTimeOver())
			return false;

		return true;
	}

	private void game_room_tick() {
		List<GameTable> removed = new ArrayList<GameTable>();

		for (String tID : playing_table_map.keySet()) {
			try {
				GameTable t = playing_table_map.get(tID);
				game_table_tick(t, t.game_room);
				// 如果桌子状态非法，回收掉
				if (t.getState() == GameConstant.TABLE_STATE_INVALID) {
					removed.add(t);
				}
				// 等待牌桌默认解散 xuyingquan 2017-3-28
				else if (t.getPlaySubstate() == GameConstant.TABLE_SUB_STATE_WAIT_APPLY_CLOSE_ROOM){
					//等待选择同意解散房间	
					int waitTime = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
					Date dt = DateService.getCurrentUtilDate();
					if (dt.getTime() - t.getApplyCloseStartTime() > waitTime* 1000)// 等待玩家同意解散倒计时结束
					{
						//处理默认同意逻辑
						GameContext.tableLogic_nndzz.playerCloseRoomOverTime(t);
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				continue;
			}
		}

		//
		for (int i = 0; i < removed.size(); i++) {
			GameTable t = removed.get(i);
			t.game_room.getPlayingTablesMap().remove(t.getTableID());
			delTable(t);
			t.game_room.beforeRecycle(t);
			t.feeTable();
		}
	}

	private void msg_processor() {
		for (int i = 0; i < 500; i++) {
			// while(true) {
			try {
				TableMessage tmsg = msg_queue.poll();
				if (tmsg == null) {
					break;
				}

				switch (tmsg.operation_id) {
				case GameConstant.OPERATION_PLAYER_LEFT_TABLE:
					player_left_table(tmsg.pl, tmsg.gr);
					return;
				case GameConstant.OPERATION_RE_NOTIFY_CURRENT_OPERATION_PLAYER:
					re_notify_current_operation_player(tmsg.gt, tmsg.gr, tmsg.pl);
					return;
				case GameConstant.OPERATION_VIP_TABLE_END:
					vip_table_end(tmsg.gt, tmsg.gr);
					return;
				}
				playerOperation(tmsg.msg, tmsg.pl, tmsg.gr);

			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}

		}
	}

	// 线程函数，主循环
	@Override
	public void run() {
		while (keep_running) {
			try {

				if (playing_table_map.size() <= 0) {
					Thread.sleep(10);
					continue;
				}

				game_room_tick();
				msg_processor();
				Thread.sleep(10);
			} catch (Exception e) {
				logger.error(this.getName() + " table manager loop error", e);
				logger.error(e.getMessage(), e);
				continue;
			}
		}
	}

	void debugLog(String log) {
		logger.info(log);
	}

	void traceLog(GameTable gt, String log) {
		if (gt.game_room.roomType == GameConstant.ROOM_TYPE_COUPLE)
			debugLog("***" + GameConstant.LOG_TAG + "_s3:" + gt.getTableID()
					+ " " + log);
		else
			debugLog("***" + GameConstant.LOG_TAG + ":" + gt.getTableID() + " "
					+ log);
	}
}
