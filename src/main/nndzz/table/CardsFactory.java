package com.chess.nndzz.table;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.nndzz.util.CardUtil;
import com.chess.common.constant.GameConstant;

// 牌型工厂 author by Qhs
public class CardsFactory {
	private static Logger logger = LoggerFactory.getLogger(CardsFactory.class);
	
	/** 3星牌大牌类型 */
	public static final int TYPE_B3_ROCKET_2 = 31; // 火箭+(2-4个)2 //2炸，2道 或 1炸，3-5道
	public static final int TYPE_B3_ROCKET_BOMB = 32; // 火箭+（0-1个）2+炸弹 //2炸，2-3道
														// 或 1炸，3-4道
	public static final int TYPE_B3_42_BOMB = 33; // 4个2+炸弹 //2炸，2道 或 1炸，5道
	public static final int TYPE_B3_DW_BOMB = 34; // 大王+（1-4个）2+炸弹 //2炸，3道 或
													// 1炸，2-5道
	public static final int TYPE_B3_XW_BOMB = 35; // 小王+（2-4个）2+炸弹 //2炸，2道 或
													// 1炸，3-5道
	public static final int TYPE_B3_2_BOMB2 = 36; // （0-2个）2+炸弹*2 //2炸，2-4道
	public static final int TYPE_B3_DW_BOMB2 = 37; // 大王+炸弹*2 //2炸，3道
	public static final int TYPE_B3_XW_BOMB2 = 38; // 小王+2+炸弹*2 //2炸，3道

	/** 2星牌大牌类型 */
	public static final int TYPE_B2_ROCKET = 21; // 火箭+（1-2个）2 //1炸，2-3道 或
													// 0炸，3-5道
	public static final int TYPE_B2_42 = 22; // 4个2 //1炸，1道 或 0炸，4道
	public static final int TYPE_B2_DW = 23; // 大王+（2-4个）2 //1炸，2道 或 0炸，2-4道
	public static final int TYPE_B2_XW = 24; // 小王+（3-4个）2 //1炸，1道 或 0炸，2-4道
	public static final int TYPE_B2_DW_BOMB = 25; // 大王+（0-1个）2+炸弹 //1炸，2道
	public static final int TYPE_B2_XW_BOMB = 26; // 小王+（0-2个）2+炸弹 //1炸，1-3道
	public static final int TYPE_B2_2_BOMB = 27; // 3个2+炸弹 //1炸，2道

	// 通用中等配牌
	public static final int TYPE_M_AIRPLANE = 1001; // 飞机
	public static final int TYPE_M_LIANDUI = 1002; // 连对
	public static final int TYPE_M_SHUN = 1003; // 顺子

	// 通用低等配牌
	public static final int TYPE_S_SAN = 2001; // 三张
	public static final int TYPE_S_DUI = 2004; // 对子
	public static final int TYPE_S_DAN = 2005; // 单牌

	private Random rand = new Random();

	// 随机3星大牌
	public List<Byte> makeRandomStar3BigCards(List<Byte> iCardList) {
		List<Integer> types = getStar3Types();
		int index = random(types.size());
		int type = types.get(index);
		logger.info(">>生成3星大牌配型：" + type);
		return makeStar3BigCards(iCardList, type);
	}

	// 3星大牌
	private List<Byte> makeStar3BigCards(List<Byte> iCardList, int type) {
		List<Byte> iResultCard = new ArrayList<Byte>();
		switch (type) {
		case TYPE_B3_ROCKET_2:// 火箭+(2-4个)2 //2炸，2道 或 1炸，3-5道
		{
			iResultCard.addAll(makeRocket());
			iResultCard.addAll(makeRandomSameCards(15, 2, 4));
			break;
		}
		case TYPE_B3_ROCKET_BOMB:// 火箭+（0-1个）2+炸弹 //2炸，2-3道 或 1炸，3-4道
		{
			iResultCard.addAll(makeRocket());
			iResultCard.addAll(makeRandomSameCards(15, 0, 1));
			iResultCard.addAll(makeBombs(1));
			break;
		}
		case TYPE_B3_42_BOMB:// 4个2+炸弹 //2炸，2道 或 1炸，5道
		{
			iResultCard.addAll(make42());
			iResultCard.addAll(makeBombs(1));

			break;
		}
		case TYPE_B3_DW_BOMB:// 大王+（1-4个）2+炸弹 //2炸，3道 或 1炸，2-5道
		{
			iResultCard.add((byte) GameConstant.SPECIAL_CARD_DAWANG);

			iResultCard.addAll(makeRandomSameCards(15, 1, 4));

			iResultCard.addAll(makeBombs(1));
			break;
		}
		case TYPE_B3_XW_BOMB:// 小王+（2-4个）2+炸弹 //2炸，2道 或 1炸，3-5道
		{
			iResultCard.add((byte) GameConstant.SPECIAL_CARD_XIAOWANG);

			iResultCard.addAll(makeRandomSameCards(15, 2, 4));

			iResultCard.addAll(makeBombs(1));
			break;
		}
		case TYPE_B3_2_BOMB2:// （0-2个）2+炸弹*2 //2炸，2-4道
		{
			iResultCard.addAll(makeRandomSameCards(15, 0, 2));

			iResultCard.addAll(makeBombs(2));
			break;
		}
		case TYPE_B3_DW_BOMB2:// 大王+炸弹*2 //2炸，3道
		{
			iResultCard.add((byte) GameConstant.SPECIAL_CARD_DAWANG);

			iResultCard.addAll(makeBombs(2));
			break;
		}
		case TYPE_B3_XW_BOMB2:// 小王+2+炸弹*2 //2炸，3道
		{
			iResultCard.add((byte) GameConstant.SPECIAL_CARD_XIAOWANG);

			iResultCard.addAll(makeSameCards(15, 1));

			iResultCard.addAll(makeBombs(2));
			break;
		}

		default:
			break;
		}
		return iResultCard;
	}

	// 随机2星大牌
	public List<Byte> makeRandomStar2BigCards(List<Byte> iCardList) {
		List<Integer> types = getStar2Types();
		int index = random(types.size());
		int type = types.get(index);
		return makeStar2BigCards(iCardList, type);
	}

	// 2星大牌
	private List<Byte> makeStar2BigCards(List<Byte> iCardList, int type) {
		List<Byte> iResultCard = new ArrayList<Byte>();
		switch (type) {
		case TYPE_B2_ROCKET:// 火箭+（1-2个）2 //1炸，2-3道 或 0炸，3-5道
		{
			iResultCard.addAll(makeRocket());
			iResultCard.addAll(makeRandomSameCards(15, 1, 2));
			break;
		}
		case TYPE_B2_42:// 4个2 //1炸，1道 或 0炸，4道
		{
			iResultCard.addAll(make42());
			break;
		}
		case TYPE_B2_DW:// 大王+（2-4个）2 //1炸，2道 或 0炸，2-4道
		{
			iResultCard.add((byte) GameConstant.SPECIAL_CARD_DAWANG);
			iResultCard.addAll(makeRandomSameCards(15, 2, 4));

			break;
		}
		case TYPE_B2_XW:// 小王+（3-4个）2 //1炸，1道 或 0炸，2-4道
		{
			iResultCard.add((byte) GameConstant.SPECIAL_CARD_XIAOWANG);

			iResultCard.addAll(makeRandomSameCards(15, 3, 4));
			break;
		}
		case TYPE_B2_DW_BOMB:// 大王+（0-1个）2+炸弹 //1炸，2道
		{
			iResultCard.add((byte) GameConstant.SPECIAL_CARD_DAWANG);

			iResultCard.addAll(makeRandomSameCards(15, 0, 1));

			iResultCard.addAll(makeBombs(1));
			break;
		}
		case TYPE_B2_XW_BOMB:// 小王+（0-2个）2+炸弹 //1炸，1-3道
		{
			iResultCard.add((byte) GameConstant.SPECIAL_CARD_XIAOWANG);

			iResultCard.addAll(makeRandomSameCards(15, 0, 2));

			iResultCard.addAll(makeBombs(1));
			break;
		}
		case TYPE_B2_2_BOMB:// 3个2+炸弹 //1炸，2道
		{
			iResultCard.addAll(makeSameCards(15, 3));

			iResultCard.addAll(makeBombs(1));
			break;
		}

		default:
			break;
		}
		return iResultCard;
	}

	// 生成3星随机中等配牌
	public List<Byte> makeRandomMiddle3Cards(List<Byte> iCardList, int least) {
		List<Integer> types = getAllowMiddle3Types(least);
		if (types.size() == 0) {
			return new ArrayList<Byte>();
		}
		int idx = random(types.size());
		int type = types.get(idx);
		logger.info(">>生成中等牌类型：" + type);
		return makeMiddleCards(iCardList, type, least);
	}

	// 生成2星随机中等配牌
	public List<Byte> makeRandomMiddle2Cards(List<Byte> iCardList, int least) {
		List<Integer> types = getAllowMiddle2Types(least);
		if (types.size() == 0) {
			return new ArrayList<Byte>();
		}
		int idx = random(types.size());
		int type = types.get(idx);
		logger.info(">>max count[" + least + "] 生成中等牌类型：" + type);
		return makeMiddleCards(iCardList, type, least);
	}

	// 中等配牌
	private List<Byte> makeMiddleCards(List<Byte> iCardList, int type, int max) {
		List<Byte> iResultCard = new ArrayList<Byte>();
		switch (type) {
		case TYPE_M_AIRPLANE:// 飞机
		{
			int xSeqMin = 2;
			int xSeqMax = max / 3;
			int xSeq = xSeqMax == xSeqMin ? xSeqMin : random(xSeqMin, xSeqMax);
			iResultCard.addAll(makeSequences(iCardList, xSeq, 3));
			break;
		}
		case TYPE_M_LIANDUI:// 连对
		{
			int xSeqMin = 3;
			int xSeqMax = max / 2;
			int xSeq = xSeqMax == xSeqMin ? xSeqMin : random(xSeqMin, xSeqMax);
			iResultCard.addAll(makeSequences(iCardList, xSeq, 2));
			break;
		}
		case TYPE_M_SHUN:// 顺子
		{
			int xSeqMin = 5;
			int xSeqMax = max;
			int xSeq = xSeqMax == xSeqMin ? xSeqMin : random(xSeqMin, xSeqMax);
			iResultCard.addAll(makeSequences(iCardList, xSeq, 1));
			break;
		}

		default:
			break;
		}
		return iResultCard;
	}

	// 生成随机3星低等配牌
	public List<Byte> makeRandomSmall3Cards(List<Byte> iCardList, int least) {
		List<Integer> types = getAllowSmall3Types(least);
		if (types.size() == 0) {
			return new ArrayList<Byte>();
		}
		int idx = random(types.size());
		int type = types.get(idx);
		logger.info(">>生成小牌类型：" + type);
		return makeSmallCards(iCardList, type, least);
	}

	// 生成随机2星低等配牌
	public List<Byte> makeRandomSmall2Cards(List<Byte> iCardList, int least) {
		List<Integer> types = getAllowSmall2Types(least);
		if (types.size() == 0) {
			return new ArrayList<Byte>();
		}
		int idx = random(types.size());
		int type = types.get(idx);
		logger.info(">>生成小牌类型：" + type);
		return makeSmallCards(iCardList, type, least);
	}

	// 低等配牌
	private List<Byte> makeSmallCards(List<Byte> iCardList, int type, int max) {
		List<Byte> iResultCard = new ArrayList<Byte>();
		switch (type) {
		case TYPE_S_SAN:// 三张
		{
			iResultCard.addAll(makeSequences(iCardList, 1, 3));
			break;
		}
		case TYPE_S_DUI:// 对子
		{
			iResultCard.addAll(makeSequences(iCardList, 1, 2));
			break;
		}
		case TYPE_S_DAN:// 单牌
		{
			int idx = random(iCardList.size());
			iResultCard.add(iCardList.get(idx));
			break;
		}

		default:
			break;
		}
		return iResultCard;
	}

	// 3星中等
	private List<Integer> getAllowMiddle3Types(int least) {
		List<Integer> list = new ArrayList<Integer>();

		int countAirplane = getMinCount(TYPE_M_AIRPLANE);
		int countLiandui = getMinCount(TYPE_M_LIANDUI);
		int countShunzi = getMinCount(TYPE_M_SHUN);

		if (least >= countAirplane) {
			list.add(TYPE_M_AIRPLANE);
		}
		if (least >= countLiandui) {
			list.add(TYPE_M_LIANDUI);
		}
		if (least >= countShunzi) {
			list.add(TYPE_M_SHUN);
			list.add(TYPE_M_SHUN);
			list.add(TYPE_M_SHUN);
		}
		return list;
	}

	// 2星中等
	private List<Integer> getAllowMiddle2Types(int least) {
		List<Integer> list = new ArrayList<Integer>();

		int countLiandui = getMinCount(TYPE_M_LIANDUI);
		int countShunzi = getMinCount(TYPE_M_SHUN);

		if (least >= countLiandui) {
			list.add(TYPE_M_LIANDUI);
		}
		if (least >= countShunzi) {
			// 顺子几率大点
			list.add(TYPE_M_SHUN);
			list.add(TYPE_M_SHUN);
			list.add(TYPE_M_SHUN);
		}
		return list;
	}

	// 3星低等
	private List<Integer> getAllowSmall3Types(int least) {
		List<Integer> list = new ArrayList<Integer>();

		int countSan = getMinCount(TYPE_S_SAN);
		int countDui = getMinCount(TYPE_S_DUI);
		int countDan = getMinCount(TYPE_S_DAN);

		// if(least >= countSan){
		// list.add(TYPE_S_SAN);
		// }
		// if(least >= countDui){
		// list.add(TYPE_S_DUI);
		// }
		// if(least >= countDan){
		// list.add(TYPE_S_DAN);
		// }

		if (least >= countSan) {
			list.add(TYPE_S_SAN);
		}
		if (least >= countDui) {
			list.add(TYPE_S_DUI);
			list.add(TYPE_S_DUI);
		}
		if (least >= countDan) {
			list.add(TYPE_S_DAN);
			list.add(TYPE_S_DAN);
			list.add(TYPE_S_DAN);
		}
		return list;
	}

	// 2星低等
	private List<Integer> getAllowSmall2Types(int least) {
		List<Integer> list = new ArrayList<Integer>();

		int countSan = getMinCount(TYPE_S_SAN);
		int countDui = getMinCount(TYPE_S_DUI);
		int countDan = getMinCount(TYPE_S_DAN);

		if (least >= countSan) {
			list.add(TYPE_S_SAN);
		}
		if (least >= countDui) {
			list.add(TYPE_S_DUI);
			list.add(TYPE_S_DUI);
		}
		if (least >= countDan) {
			list.add(TYPE_S_DAN);
			list.add(TYPE_S_DAN);
			list.add(TYPE_S_DAN);
		}
		return list;
	}

	// 配牌最小长度
	private int getMinCount(int type) {
		int count = 0;
		switch (type) {
		case TYPE_M_AIRPLANE:// 飞机
		{
			// 飞机至少6张
			count = 6;
			break;
		}
		case TYPE_M_LIANDUI:// 连对
		{
			// 连对至少6张
			count = 6;
			break;
		}
		case TYPE_M_SHUN:// 顺子
		{
			// 顺子至少5张
			count = 5;
			break;
		}
		case TYPE_S_SAN:// 三张
		{
			count = 3;
			break;
		}
		case TYPE_S_DUI:// 对子
		{
			count = 2;
			break;
		}
		case TYPE_S_DAN:// 单牌
		{
			count = 1;
			break;
		}
		default:
			break;
		}
		return count;
	}

	// 生成序列 author by Qhs
	// 手牌、序列长度、（单、双、仨）
	private List<Byte> makeSequences(List<Byte> iCardList, int xSequence,
			int xCount) {

		// 备份
		List<Byte> iTempCard = new ArrayList<Byte>(iCardList.size());
		iTempCard.addAll(iCardList);
		// 移除2、王
		iTempCard = remove2X(iTempCard);

		List<List<Integer>> iSeqGroups = groupBySequence(iTempCard, xCount);
		// logger.info("-->序列分组："+iSeqGroups);
		List<List<Integer>> steps = new ArrayList<List<Integer>>();

		for (int i = 0; i < iSeqGroups.size(); i++) {
			List<Integer> list = iSeqGroups.get(i);
			//
			if (list.size() >= xSequence) {
				for (int j = 0; j < list.size(); j++) {
					if (j + xSequence <= list.size()) {
						List<Integer> step = new ArrayList<Integer>();
						for (int m = j; m < j + xSequence; m++) {
							int num = list.get(m);
							step.add(num);
						}
						steps.add(step);
					}
				}
			}
		}

		// logger.info("-->待选序列："+steps);
		List<Byte> iResultCard = new ArrayList<Byte>();

		if (steps.size() > 0) {
			int idx = random(steps.size());
			// logger.info("-->所选选序列："+steps);
			List<Integer> cardNums = steps.get(idx);
			for (int i = 0; i < cardNums.size(); i++) {
				int num = cardNums.get(i);
				iResultCard.addAll(takeCardsByCardNum(iTempCard, num, xCount));
			}
		}

		return iResultCard;
	}

	private List<Byte> takeCardsByCardNum(List<Byte> iCardList, int cardNum,
			int count) {
		List<Byte> iResultCard = new ArrayList<Byte>();
		for (int i = 0; i < iCardList.size(); i++) {
			byte card = iCardList.get(i);
			int num = CardUtil.getCardNum(card);
			if (num == cardNum && iResultCard.size() <= count) {
				iResultCard.add(card);
			}
			if (iResultCard.size() >= count) {
				break;
			}
		}
		return iResultCard;
	}

	// 转换成牌点数量集合逻辑抽出
	private Map<Integer, Integer> toCardNumCountMap(List<Byte> iCardList) {
		Map<Integer, Integer> temp = new TreeMap<Integer, Integer>();
		for (int i = 0; i < iCardList.size(); i++) {
			int cardNum = CardUtil.getCardNum(iCardList.get(i));
			Integer count = temp.get(cardNum);
			if (count == null) {
				count = 0;
			}
			count++;
			temp.put(cardNum, count);
		}
		return temp;
	}

	private List<List<Integer>> groupBySequence(List<Byte> iCardList, int xCount) {
		Map<Integer, Integer> cardNumMap = toCardNumCountMap(iCardList);
		// logger.info(">>按牌点分组："+cardNumMap);
		return groupBySequence(cardNumMap, xCount);
	}

	// 按序列分组
	private List<List<Integer>> groupBySequence(
			Map<Integer, Integer> cardNumMap, int xCount) {
		List<List<Integer>> iResults = new ArrayList<List<Integer>>();

		List<Integer> keyList = new ArrayList<Integer>();
		Set<Integer> keySet = cardNumMap.keySet();
		keyList.addAll(keySet);

		// logger.info("@@>keySet："+keySet);

		List<String> slist = new ArrayList<String>();
		StringBuffer sb = new StringBuffer(256);
		for (int i = 0; i < keyList.size(); i++) {
			int cardNum = keyList.get(i);
			int count = cardNumMap.get(cardNum);

			if (count >= xCount) {
				sb.append(cardNum).append("、");
			}

			if (i + 1 < keyList.size()) {
				// 下一个
				int nextCardNum = keyList.get(i + 1);
				int nextCount = cardNumMap.get(nextCardNum);
				if (nextCardNum == cardNum + 1 && nextCount > xCount) {
					// 下一个可以
				} else {
					// 下一个不可以
					slist.add(sb.toString());
					sb.setLength(0);
				}
			} else {
				slist.add(sb.toString());
				sb.setLength(0);
			}
		}

		// logger.info("==>待选序列字符"+slist);

		for (int i = 0; i < slist.size(); i++) {
			String str = slist.get(i);
			List<Integer> list = new ArrayList<Integer>();

			String[] ss = str.split("、");
			if (ss.length > 0) {
				for (int j = 0; j < ss.length - 1; j++) {
					String s = ss[j];
					Integer num = Integer.parseInt(s);
					list.add(num);
				}
			}

			iResults.add(list);
		}

		// logger.info("==>>待选序列"+slist);
		return iResults;
	}

	private List<Byte> make42() {
		return makeBomb(15);
	}

	public List<Byte> makeBombs(int count) {
		int cnt = 0;
		Set<Integer> withOut = new HashSet<Integer>();
		List<Byte> iResultCard = new ArrayList<Byte>();

		while (cnt < count) {
			int cardNum = getRandoomCardNum(withOut);
			iResultCard.addAll(makeBomb(cardNum));
			withOut.add(cardNum);
			cnt++;
		}
		return iResultCard;
	}

	private List<Byte> makeBomb(int cardNum) {
		return makeSameCards(cardNum, 4);
	}

	private List<Byte> makeRocket() {
		List<Byte> iResultCard = new ArrayList<Byte>();
		iResultCard.add((byte) GameConstant.SPECIAL_CARD_DAWANG);
		iResultCard.add((byte) GameConstant.SPECIAL_CARD_XIAOWANG);
		return iResultCard;
	}

	private int getRandoomCardNum(Set<Integer> without) {
		List<Integer> baseCardNums = getBaseCardNums();
		if (without != null && without.size() > 0) {
			baseCardNums.removeAll(without);
		}
		int idx = random(baseCardNums.size());
		return baseCardNums.get(idx);
	}

	private List<Byte> makeRandomSameCards(int cardNum, int min, int max) {
		int count = random(min, max);
		return makeSameCards(cardNum, count);
	}

	private List<Byte> makeSameCards(int cardNum, int count) {
		List<Byte> iResultCards = new ArrayList<Byte>();
		Set<Integer> rColors = makeColors(count);

		for (Integer color : rColors) {
			byte b = (byte) (color + cardNum);
			iResultCards.add(b);
		}

		return iResultCards;
	}

	private Set<Integer> makeColors(int count) {
		Set<Integer> rColors = new HashSet<Integer>(count);
		List<Integer> iColors = getBaseColors();
		while (rColors.size() < count) {
			int i = random(4);
			int color = iColors.get(i);
			rColors.add(color);
		}
		return rColors;
	}

	private List<Integer> getStar3Types() {
		List<Integer> star3Types = new ArrayList<Integer>();
		star3Types.add(TYPE_B3_ROCKET_2);
		star3Types.add(TYPE_B3_ROCKET_BOMB);
		star3Types.add(TYPE_B3_42_BOMB);
		star3Types.add(TYPE_B3_DW_BOMB);
		star3Types.add(TYPE_B3_XW_BOMB);
		star3Types.add(TYPE_B3_2_BOMB2);
		star3Types.add(TYPE_B3_DW_BOMB2);
		star3Types.add(TYPE_B3_XW_BOMB2);
		return star3Types;
	}

	private List<Integer> getStar2Types() {
		List<Integer> star2Types = new ArrayList<Integer>();
		star2Types.add(TYPE_B2_ROCKET);
		star2Types.add(TYPE_B2_42);
		star2Types.add(TYPE_B2_DW);
		star2Types.add(TYPE_B2_XW);
		star2Types.add(TYPE_B2_DW_BOMB);
		star2Types.add(TYPE_B2_XW_BOMB);
		star2Types.add(TYPE_B2_2_BOMB);
		return star2Types;
	}

	private List<Integer> getBaseCardNums() {
		List<Integer> cardNums = new ArrayList<Integer>();
		for (int i = 3; i < 15; i++) {
			cardNums.add(i);
		}
		return cardNums;
	}

	private List<Integer> getBaseColors() {
		List<Integer> colors = new ArrayList<Integer>();
		for (int i = 0; i < 4; i++) {
			colors.add(i * GameConstant.DDZ_COLOR_STEP);
		}
		return colors;
	}

	private int random(int min, int max) {
		return rand.nextInt(max - min) + min;
	}

	private int random(int max) {
		return random(0, max);
	}

	// 移除所有的2和王
	private List<Byte> remove2X(List<Byte> iCardList) {
		List<Byte> iRemoved = new ArrayList<Byte>();
		for (int i = 0; i < iCardList.size(); i++) {
			byte b = iCardList.get(i);
			int num = CardUtil.getCardNum(b);
			if (num >= 15) {
				iRemoved.add(b);
			}
		}
		iCardList.removeAll(iRemoved);
		return iCardList;
	}

}
