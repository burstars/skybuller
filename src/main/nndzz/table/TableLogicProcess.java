package com.chess.nndzz.table;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.common.UUIDGenerator;
import com.chess.common.bean.ClubMember;
import com.chess.common.bean.GameConfig;
import com.chess.common.bean.MallItem;
import com.chess.common.bean.ProxyVipRoomRecord;
import com.chess.common.bean.SystemConfigPara;
import com.chess.common.bean.TClub;
import com.chess.common.bean.TClubLevel;
import com.chess.common.bean.User;
import com.chess.common.bean.VipRoomRecord;
import com.chess.common.bean.club.TableUserInfo;
import com.chess.common.constant.ConfigConstant;
import com.chess.common.constant.ErrorCodeConstant;
import com.chess.common.constant.GameConstant;
import com.chess.common.constant.LogConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.msg.struct.club.PlayerDownOrUpMsgAck;
import com.chess.common.msg.struct.friend.BeInvitedEnterVipRoomMsg;
import com.chess.common.msg.struct.friend.InviteFriendEnterVipRoomMsg;
import com.chess.common.msg.struct.other.PlayerAskCloseVipRoomMsg;
import com.chess.common.msg.struct.other.PlayerAskCloseVipRoomMsgAck;
import com.chess.common.msg.struct.other.ProxyVipRoomRecordMsg;
import com.chess.common.msg.struct.other.TalkingInGameMsg;
import com.chess.common.service.ICacheGameConfigService;
import com.chess.common.service.IClubService;
import com.chess.common.service.ISystemConfigService;
import com.chess.common.service.impl.CacheUserService;
import com.chess.nndzz.bean.UserGameHander;
import com.chess.nndzz.msg.struct.other.EnterVipRoomMsg;
import com.chess.nndzz.msg.struct.other.GameReadyMsg;
import com.chess.nndzz.msg.struct.other.GameSiteDownMsg;
import com.chess.nndzz.msg.struct.other.GameStartMsg;
import com.chess.nndzz.msg.struct.other.HuDongBiaoQingMsgNNDZZ;
import com.chess.nndzz.msg.struct.other.PlayerGameOpertaionAckMsg;
import com.chess.nndzz.msg.struct.other.PlayerOperationNotifyMsg;
import com.chess.nndzz.msg.struct.other.PlayerTableOperationMsg;
import com.chess.nndzz.msg.struct.other.RequestStartGameMsg;
import com.chess.nndzz.msg.struct.other.RequestStartGameMsgAck;
import com.chess.nndzz.msg.struct.other.SearchVipRoomMsgAck;

public class TableLogicProcess {
	private static Logger logger = LoggerFactory.getLogger(TableLogicProcess.class);

	private GameConfig gameConfig;
	private List<GameRoom> roomList = new ArrayList<GameRoom>();
	private Map<Integer, GameRoom> gameRoomMap = new HashMap<Integer, GameRoom>();

	// key:桌子id value:桌子管理线程
	// 方便消息投递
	private Map<String, GameProcessThread> table_mgr_map = new HashMap<String, GameProcessThread>();
	//
	private CacheUserService playerService = null;
	private ISystemConfigService cfgService = null;

	// 桌子管理线程池,暂时生成10条线程,每条承载500桌
	private GameProcessThreadPool tmgr_thread_pool = null;

	private int paused_choose_wait_time = 0;
	
	private int table_state_before_apply_close_room = 0;

	// private ITaskService taskService = null;
	//
	public TableLogicProcess(CacheUserService ps) {
		playerService = ps;
		GameContext.tableLogic_nndzz = this;
	}

	//
	public void init() {

	}

	private void pushMsgTableMgr(String table_id, PlayerTableOperationMsg msg,
			User pl, int opt_id) {
		TableMessage tmsg = new TableMessage();
		tmsg.operation_id = opt_id;
		tmsg.pl = pl;
		tmsg.msg = msg;

		if (pl != null) {
        	//2017-9-26
        	if(pl.getPvrrs() != null && pl.getPvrrs().size() > 0){
        		List<ProxyVipRoomRecord> pvrrs = pl.getPvrrs();
        		for(ProxyVipRoomRecord pvrr : pvrrs){
        			if(pvrr.getRoomID() == table_id){
        				if(pvrr.getHostID() != null && !pvrr.getHostID().equals("")){
        					pl = playerService.getPlayerByPlayerID(pvrr.getHostID());
        				}
        			}
        		}
        	}
			GameRoom gr = this.getRoom(pl.getRoomID());
			if (gr != null) {
				tmsg.gt = gr.getTable(table_id, false);
				tmsg.gr = gr;
			}else{
            	tmsg.gt = getGameTableByTableId(table_id);
            }
		}
		GameProcessThread tmgr = null;
		if (pl != null) {
			tmgr = table_mgr_map.get(table_id);
		}
		// TableManager tmgr = tmsg.gt.table_manager;
		if (tmgr != null) {
			tmgr.pushMsg(tmsg);
		}
	}

	// 房间配置规则在t_global表中，字段PARA_ID 2001到2050，VALUE_INT是底注，PRO_2是最小携带，PRO_3是最大携带
	// PRO_1房间类型
	//
	public void loadRoomSettings() {
		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		//
		for (int i = ConfigConstant.ROOM_OF_PRIMARY; i < 2050; i++) {
			//  临时屏蔽练习场
			if (i == 2001) {

			} else {
				SystemConfigPara cfg = cfgService.getPara(i);
				if (cfg == null)
					continue;
				//

				GameRoom gr = new GameRoom();

				gr.price = cfg.getValueInt();// 底注
				gr.cfgId = cfg.getParaID();
				gr.maxGold = cfg.getPro_3();
				gr.minGold = cfg.getPro_2();
				gr.roomType = cfg.getPro_1();
				gr.roomID = i;
				gr.serviceFee = cfg.getPro_4();// 台费

				if (gr.roomType == GameConstant.ROOM_TYPE_VIP
						|| gr.roomType == GameConstant.ROOM_TYPE_COUPLE) {
					gr.fixedGold = cfg.getPro_4(); // VIP房间固定带入金币数//修改为取pro_4的值 
				}

				//
				roomList.add(gr);
				gameRoomMap.put(i, gr);
			}

		}

		tmgr_thread_pool = new GameProcessThreadPool(playerService, 5, 1000);

		if (paused_choose_wait_time == 0) {
			paused_choose_wait_time = cfgService.getPara(
					ConfigConstant.VIP_PAUSED_CHOOSE_WAIT_TIME).getValueInt();
		}
		cfgService.get_zhidui_support();
	}

	public void updateRoom(SystemConfigPara cfg) {
		if (cfg.getParaID() >= ConfigConstant.ROOM_OF_PRIMARY
				&& cfg.getParaID() < 2050) {
			for (GameRoom gr : roomList) {
				if (gr.cfgId == cfg.getParaID()) {
					gr.price = cfg.getValueInt();// 底注
					gr.maxGold = cfg.getPro_3();
					gr.minGold = cfg.getPro_2();
					gr.roomType = cfg.getPro_1();
					gr.serviceFee = cfg.getPro_4();// 台费

					if (gr.roomType == GameConstant.ROOM_TYPE_VIP
							|| gr.roomType == GameConstant.ROOM_TYPE_COUPLE) {
						gr.fixedGold = cfg.getPro_4(); // VIP房间固定带入金币数//修改为取pro_4的值 cc modify 2017-11-28
					}
					break;
				}
			}
		}
	}

	// kn add parameter
	public GameTable popFreeTable(int roomType) {
		GameTable gt = new GameTable(roomType);

		logger.debug("create table=" + gt.getTableID());// System.out.println("create table="
														// + gt.getTableID());
		//
		return gt;
	}

	//
	public GameRoom getRoom(int roomID) {
		return gameRoomMap.get(roomID);

	}

	public int getRoomPlayerNum(int roomID) {
		if (roomID < 2000) {
			roomID = 2000 + roomID;
		}
		GameRoom gr = gameRoomMap.get(roomID);
		if (gr != null)
			return gr.getPlayerNum();

		return 0;
	}

	/**
	 * 客户端通知服务器，游戏结束，玩家继续游戏*
	 */
	public void gameContinue(User pl) {
		// 返回给玩家，
		RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
		ack.gold = pl.getGold();

		int roomID = pl.getRoomID();

		GameRoom gr = getRoom(roomID);
		if (gr == null)
			return;

		//
		GameTable gt = gr.getTable(pl.getTableID(), false);
		if (gt == null) {
			// 重新进新桌吧，老桌子没了
			logger.debug("【电子庄牛牛】继续游戏:" + pl.getPlayerName() + "重新进新桌吧，老桌子没了");// System.out.println("继续游戏:"
																		// +
																		// pl.getPlayerName()
																		// +
																		// "重新进新桌吧，老桌子没了");
			this.enter_room(pl, roomID);
			return;
		}

		// 玩家的状态必须是在等待继续状态
		if (pl.getGameState() != GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE)
			return;

		// 如果VIP房间的圈数用完了，就不能继续了
		if ( gt.isVipTableTimeOver()) {
			ack.result = ErrorCodeConstant.VIP_TABLE_IS_GAME_OVER; // VIP桌子已经结束
			GameContext.gameSocket.send(pl.getSession(), ack);
			return;
		}

        ack.isRecord = gt.getIsRecordTable();
		//
		pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
		//
		ack.result = ErrorCodeConstant.CMD_EXE_OK;
		//
		ack.init_players(gt.getPlayers(), gt.isVipTable());
		//
		ack.gold = pl.getGold();
		//
		if (gt.isVipTable()) {
			ack.vipTableID = gt.getVipTableID();
			ack.tablePassword = gt.getVipPswMd5();
			ack.tableRules = gt.getTableRuleOption();// STiV modify
		} else
			ack.vipTableID = 0;
		//
		ack.roomID = roomID;
		ack.coinBaseNum = gt.getCoinBaseNum();//gt.getDizhu();
		ack.juCount = gt.getJuCount();//gt.getHandNum();
		ack.totalJuNum = gt.getTotalJuNum();//gt.getQuanTotal();
		
		ack.playerNum = gt.getPlayerCount();
		ack.roomType = gr.roomType;

		ack.tablePos = pl.getTablePos();
		ack.openRoomType = gt.getProxyState();
		
		ack.isSelectSite = gt.getIsSelectSite() ;
		ack.gameStart = gt.isStartGame() ? 1 : 0;
		ack.clubCode = gt.getClubCode();
		//
		// 进入桌子，返回消息给玩家
		GameContext.gameSocket.send(pl.getSession(), ack);
		//

		// 给桌子上的其他玩家发个进入新玩家的消息
		PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();
		axk.opertaionID = GameConstant.OPT_TABLE_ADD_NEW_PLAYER;
		axk.playerName = pl.getPlayerName();
		//
		if (gt.isVipTable())
			axk.gold = pl.getVipTableGold();
		else
			axk.gold = pl.getGold();
		//
		axk.headImg = pl.getHeadImg();
		axk.sex = pl.getSex();
		axk.tablePos = pl.getTablePos();
		axk.playerID = pl.getPlayerID();
		axk.playerIndex = pl.getPlayerIndex();
		axk.canFriend = pl.getCanFriend();
		axk.ip = pl.getClientIP();
		axk.location = pl.getLocation();
		axk.headImgUrl = pl.getHeadImgUrl();
		gt.sendMsgToTableExceptMe(axk, pl.getPlayerID());
	}

	//
	private void player_recover(GameTable gt, User pl) {
		// 如果断线并且是单局结算时断线，就不能发gameStartMsg了
		if ( gt.isSingleRoundOver() ) {
			return;
		}
		
		// 如果在等待状态下，也不能发gameStartMsg
		if( gt.getState() == GameConstant.TABLE_STATE_WAITING_PLAYER ){
			logger.info("-----------true---------");
			return;
		}
		
		// 把牌发给玩家
		GameStartMsg msg = new GameStartMsg();
		msg.myTablePos = pl.getTablePos();
		msg.myCards = pl.getCardsInHand();
		msg.dealerPos = gt.getRealDealerPos();
		msg.juCount = gt.getJuCount();
		msg.totalJuNum = gt.getTotalJuNum();
		
		msg.playerCountMax = gt.getMaxPlayer();
		msg.playerCount = gt.getPlayerNum();
		msg.recover = 1;
		
		logger.info("【电子庄牛牛】玩家【" + pl.getPlayerName() + "】，id【" + pl.getPlayerIndex()
				+ "】，断线重连恢复游戏状态：VIP房ID【" + gt.getVipTableID() + "】，总局数【"
				+ gt.getTotalJuNum() + "】，当前局数【" + gt.getJuCount() + "】");
		
		// 循环玩家
		for (int i = 0; i < gt.getPlayers().size(); i++) {
			User pxx = gt.getPlayers().get(i);

			if((pxx != null) && (!pxx.getOnTable())){
				msg.setOffLine(pxx.getTablePos(), gt.getMaxPlayer(), 1);
			}else{
				msg.setOffLine(pxx.getTablePos(), gt.getMaxPlayer(), 0);
			}

			// 设置金币
			if (pxx != null) {
				int plPos = pxx.getTablePos();
				UserGameHander hander = pxx.getUserGameHanderForNNDZZ();
				msg.setPlayersPingBeiCoinNum(plPos, gt.getMaxPlayer(), hander.getPingBeiCoin());
				msg.setPlayersFanBeiCoinNum(plPos, gt.getMaxPlayer(), hander.getFanBeiCoin());
				msg.setPlayersLiangNiuNum(plPos, gt.getMaxPlayer(), hander.isLiangNiu());
				
				List<Byte> ch = pxx.getCardsInHand();
				if(plPos ==0)
				{
					msg.player0Cards = ch;
				}
				else if(plPos == 1)
				{
					msg.player1Cards = ch;
				}
				else if(plPos == 2)
				{
					msg.player2Cards = ch;
				}
				else if(plPos == 3)
				{
					msg.player3Cards = ch;
				}
				else if(plPos == 4)
				{
					msg.player4Cards = ch;
				}
				else if(plPos == 5)
				{
					msg.player5Cards = ch;
				}
			}
		}
		
		if(gt.getClubCode() > 0){
			// 庄家的牌
			msg.setPlayersCard(5, gt.getDealerCards());
			msg.coinItems = gt.getCoinItemsByLevel();
			msg.allowCoinItems = gt.getPlayerAllowCoinItems(msg.coinItems, pl);
			
			msg.setGold(gt.getPlayers(), gt.getMaxPlayer(), true);
		}
		
		GameContext.gameSocket.send(pl.getSession(), msg);
	}

	//
	public void inviteFriend(InviteFriendEnterVipRoomMsg msg, User pl) {
		User friend = playerService.getPlayerByPlayerID(msg.playerID);
		if (friend == null)
			return;
		//
		GameRoom gr = getRoom(msg.roomID);
		if (gr == null)
			return;
		//
		GameTable gt = gr.getVipTableByVipTableID(msg.vipTableID);
		if (gt == null)
			return;
		//
		// 只有房主可以邀请
		if (gt.getCreatorPlayerID().equals(pl.getPlayerID())) {
			//
			BeInvitedEnterVipRoomMsg msx = new BeInvitedEnterVipRoomMsg();
			msx.tableID = gt.getTableID();
			msx.playerID = pl.getPlayerID();
			msx.roomID = msg.roomID;
			msx.vipTableID = msg.vipTableID;
			msx.password = gt.getVipPswMd5();
			msx.openRoomType = gt.getProxyState();
			//
			GameContext.gameSocket.send(friend.getSession(), msx);
		}
	}

	/**
	 * 加入房间
	 * @author  2017-11-15
	 * TableLogicProcess.java
	 * @param msg
	 * @param pl
	 * void
	 */
	public void enterVipRoom(EnterVipRoomMsg msg, User pl) {
		GameRoom rm = gameRoomMap.get(msg.roomID);
		if (rm == null) {
			logger.error("【电子庄牛牛】enterVipRoom,rm==null, playerIndex="
					+ pl.getPlayerIndex());
			return;
		}
		//
		GameTable gt = rm.getTable(msg.tableID, false);
		if (gt == null) {
			logger.error("【电子庄牛牛】enterVipRoom,gt==null, playerIndex="
					+ pl.getPlayerIndex());
			return;
		}
		
		// 验证俱乐部金币不足
		ClubMember clubMember = null;
		if(msg.clubCode > 0){
			clubMember = validateClubGold(rm, pl, msg.clubCode, gt.getRoomLevel());
			if(clubMember == null){
				return;
			}
		}
		
		// 
		if( gt.isStartGame() ){
			boolean isInTable = false;
			for( User plx:gt.getPlayers() ){
				if( plx.getTablePos() == pl.getTablePos() ){
					isInTable = true;
					break;
				}
			}
			if( !isInTable ){
				SearchVipRoomMsgAck ack = new SearchVipRoomMsgAck();
				ack.vipTableID = 1;
				ack.dizhu = -1;
				// 进入桌子失败
				GameContext.gameSocket.send(pl.getSession(), ack);
				return;
			}
		}
		pl.setGameId(gt.getGameId());
		
		int kaKouNum = 0;
		GameConfig config = GameContext.tableLogic_nndzz.getGameConfig();
		if(msg.clubCode > 0){
			// 场次级别
			String level = gt.getRoomLevel();
			Map<String, Integer> mapKa = config.getPokerClubWanfa(gt.getRoomModel());
			logger.info("==========>俱乐部扣卡配置："+mapKa);
			kaKouNum = mapKa.get("cost_self_"+level);
			if (gt.getProxyState() == 2) {
				kaKouNum = mapKa.get("cost_aa_"+level);
			} else if (gt.getProxyState() == 1) {
				kaKouNum = mapKa.get("cost_proxy_"+level);
			}
		}else{
			Map<String, Integer> mapKa = config.getPokerVipWanfa(gt.getRoomModel());
			logger.info("==========>VIP房扣卡配置："+mapKa);
			kaKouNum = mapKa.get("cost_self");
			if (gt.getProxyState() == 2) {
				kaKouNum = mapKa.get("cost_aa");
			} else if (gt.getProxyState() == 1) {
				kaKouNum = mapKa.get("cost_proxy");
			}
		}
		
		if(gt.getProxyState() == 2){
	        // 验证扣卡
			if (!(pl.getVipTableID().length() > 0) && playerService.commonPreuse_fanka(pl, kaKouNum, false) == false) {
				return;
			}
		}
//		// 密码错误
//		if (gt.getVipPswMd5().equals(msg.psw) == false) {
//			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
//			ack.gold = pl.getGold();
//			//
//			ack.roomType = rm.roomType; // kn add
//			ack.playerNum = gt.getPlayerCount();
//			ack.result = ErrorCodeConstant.WRONG_PASSWORD;
//			// 进入桌子，返回消息给玩家
//			GameContext.gameSocket.send(pl.getSession(), ack);
//			return;
//		}
		//
		gt.setDizhu(rm.price);

		//
		int old_tablePos = gt.findPlayerIndex(pl);

		boolean old_table = false;
		if (old_tablePos >= 0)// 已经存在，离开又进来
		{
			old_table = true;
			//
			pl.setTableID(gt.getTableID());
			pl.setVipTableID(String.valueOf(gt.getVipTableID()));//(gt.getTableID());

			// 给桌子上的其他玩家发个玩家上线的消息
			PlayerOperationNotifyMsg axk = new PlayerOperationNotifyMsg();
			axk.operationType = GameConstant.MJ_OPT_ONLINE;
			axk.playerTablePos = pl.getTablePos();
			gt.sendMsgToTableExceptMe(axk, pl.getPlayerID());

		} else {
			if (gt.isFull()) {
				RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
				ack.gold = pl.getGold();
				ack.playerNum = gt.getPlayerCount();
				ack.result = ErrorCodeConstant.VIP_TABLE_IS_FULL;
				ack.roomType = rm.roomType; // kn add
				// 进入桌子，返回消息给玩家
				GameContext.gameSocket.send(pl.getSession(), ack);

				return;
			}

			// 新玩家进来，设置VIP房间金币
				GameRoom gr = getRoom(gt.getRoomID());
				if ((gr != null)
						&& (gr.roomType == GameConstant.ROOM_TYPE_VIP || gr.roomType == GameConstant.ROOM_TYPE_COUPLE)) {
					if(clubMember != null){
						pl.setVipTableGold(clubMember.getGoldNum());
					}else{
						pl.setVipTableGold(gr.fixedGold);
					}
				}

			if (gt.getProxyCreator() != null && gt.getPlayers().size() == 0) {
				gt.setCreatorPlayerID(pl.getPlayerID());
				gt.setCreatorPlayerName(pl.getPlayerName());
				int playerIndex = pl.getPlayerIndex();
				//代开优化 cc modify 2017-9-26
				List<ProxyVipRoomRecord> pvrrs = gt.getProxyCreator().getPvrrs();
				if(pvrrs != null){
					for(int i=pvrrs.size()-1;i>=0;i--){
						ProxyVipRoomRecord pvrr = pvrrs.get(i);
						if(pvrr.getRoomID().equals(gt.getTableID())){
							pvrr.setHostID(gt.getCreatorPlayerID());
							pvrr.setHostName(gt.getCreatorPlayerName());
							pvrr.setPlayer1Index(playerIndex);
							pvrr.setPlayer1Name(gt.getCreatorPlayerName());
							playerService.updateProxyVipRoomRecord(pvrr);
						}
					}
				}
			}
			gt.enterTable(pl);
		}
		// 人满不开局，需要确认准备状态
//		// 如等待玩家中，满了就开始，如果中间断线再进入，就不要这个状态切换
//		if (gt.isFull()
//				&& gt.getState() == GameConstant.TABLE_STATE_WAITING_PLAYER) {
//			long ct = DateService.getCurrentUtilDate().getTime();
//			gt.setReadyTime(ct);
//			gt.setState(GameConstant.TABLE_STATE_READY_GO);
//		}
		//
		pl.setRoomID(msg.roomID);
		pl.setTableID(gt.getTableID());
		pl.setVipTableID(String.valueOf(gt.getVipTableID()));//(gt.getTableID());
		
		//
		boolean re_enter = false;
		if (old_table )//&& gt.isFull())    人满不开局，需要确认准备状态
			re_enter = true;

		// tieguo
		pl.setPlayerType(rm.roomType);
		playerService.updatePlayerType(pl.getPlayerID(), rm.roomType);

		enter_table_done(pl, gt, re_enter);

	}

	// STiV modify add param tableRule
	// vip开房间，在服务器理解为创建一个vip桌子
	public void create_vip_table(User pl, String psw, int quanNum, int roomID,
			int itemBaseID, List<Integer> tableRule, int isRecord, int isProxy ,int isSelectSite,String gameId
			,int userNum,int kaKouNum, int clubCode, String roomLevel) {
		GameRoom rm = gameRoomMap.get(roomID);
		if (rm == null)
			return;

		GameTable old = rm.getPlayingTablesMap().get(pl.getVipTableID());
		
		if (isProxy != 1){
			// 2017.9.19 老房子不能是代开的房间。代开房间不能自动进入
			if (old != null && (old.getProxyCreator() == null || (old.getProxyCreator() != null && !old.getProxyCreator().getPlayerID().equals(pl.getPlayerID())))){
				if(old.getPlayers().contains(pl)){
					enter_room(pl, roomID);
					return;
				}
			}
		}
		
		ClubMember clubMember = null;
		// 验证俱乐部金币
		if(clubCode > 0){
			clubMember = validateClubGold(rm, pl, clubCode, roomLevel);
			if(clubMember == null){
				return;
			}
		}
		
		GameTable gt = rm.createVipTable(psw, pl, quanNum, tableRule, gameId, userNum, isProxy, clubCode, roomLevel);
		if (gt == null)
			return;

        gt.setProxyState(isProxy);//记录开房模式 cc modify 2017-8-15
        
		Date ct = DateService.getCurrentUtilDate();
		gt.setVipCreateTime(ct.getTime());
        gt.setIsRecordTable(isRecord);
        gt.setGameId(gameId);
        gt.setProxyState(isProxy);//记录开房模式 cc modify 2017-8-15
        gt.setPlayerCount(userNum);//lxw 20170919
		// 总圈数
		// kn add
		GameRoom gr = getRoom(gt.getRoomID());
		gt.setTotalJuNum(quanNum);
		gt.setRoomModel(quanNum);
		gt.setRoomLevel(roomLevel);
		gt.setIsSelectSite(isSelectSite) ;
		gt.setJuCount(0);
		//
		gt.setItemBaseID(itemBaseID);

		// 设置为未支付房卡
		gt.setPayVipKa(false);

		gt.setDizhu(gr.price);

		// 设置VIP房间带入金额
		if ((gr != null)
				&& (gr.roomType == GameConstant.ROOM_TYPE_VIP || gr.roomType == GameConstant.ROOM_TYPE_COUPLE)) {
			
			if(clubMember != null){
				pl.setVipTableGold(clubMember.getGoldNum());
			}else{
				pl.setVipTableGold(gr.fixedGold);
			}
			
			pl.setPlayerType(gr.roomType);
			playerService.updatePlayerType(pl.getPlayerID(), gr.roomType);
			logger.info("【电子庄牛牛】玩家昵称【" + pl.getPlayerName() + "】，ID【"
					+ pl.getPlayerIndex() + "】，create_vip_table:" + gr.roomType
					+ "，房间ID【" + gt.getVipTableID() + "】，玩家位置【" + pl.getTablePos() + "】，房间局数【" + quanNum
					+ "】，房间规则【" + tableRule + "】");
		}

		if(clubCode > 0){
        	gt.setClubTableState(2);//俱乐部的私密桌子
        	//TODO 发消息通知前台俱乐部的所有人，当前俱乐部各种牌桌状态
			playerService.sendMsgToClubPlayer(gt.getClubCode(), playerService.newGetSingleClubInfoMsgAck(gt.getClubCode()));
        }
		add_table_to_tmgr(gt, gr);

        // TODO 代理开房处理
        if(isProxy == 1){
        	gt.setProxyCreator(pl);					
			// 创建一条代开房间记录
			ProxyVipRoomRecord pvrr = new ProxyVipRoomRecord();
			pvrr.setProxyStartTime(ct);
			pvrr.setStartTime(ct);
			pvrr.setEndTime(ct);
			pvrr.setRoomID(gt.getTableID());
			pvrr.setRoomIndex(gt.getVipTableID());
			pvrr.setProxyID(pl.getPlayerID());
			pvrr.setProxyName(gt.getCreatorPlayerName());			
			pvrr.setRecordID(UUIDGenerator.generatorUUID());
			pvrr.setVipState(GameConstant.PROXY_NO_START_GAME);
			pvrr.setProxyCardCount(kaKouNum);
			pvrr.setRoomType(roomID);
			pvrr.setGameId(gameId);
			String tr = "";
			for(int i:gt.getTableRuleOption()){
				tr = tr + i + ",";
			}
			pvrr.setTableRule(tr);
			playerService.createProxyVipRoomRecord(pvrr);
			//
			if(pl.getPvrrs() != null){
				pl.getPvrrs().add(pvrr);
			}
			//给代理发刷新列表消息
			ProxyVipRoomRecordMsg prvm = new ProxyVipRoomRecordMsg();
			prvm.playerID = gt.getProxyCreator().getPlayerID();
			prvm.gameID = gt.getGameId();
			playerService.getProxyVipRoomRecord(prvm, gt.getProxyCreator().getSession());
 		}else{
			pl.setOnTable(true);
			pl.getUserGameHanderForNNDZZ().setTableRules(tableRule);
			//
			// 返回给玩家，
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
			ack.gold = pl.getGold();
			//
			ack.result = ErrorCodeConstant.CMD_EXE_OK;
			//
			ack.init_players(gt.getPlayers(), gt.isVipTable());
			//
			ack.roomID = roomID;
			ack.roomType = rm.roomType; // kn add
			ack.tablePos = pl.getTablePos();
	
			ack.coinBaseNum = gt.getCoinBaseNum();//gt.getDizhu();
			ack.juCount = gt.getJuCount();
			ack.totalJuNum = gt.getTotalJuNum();
			ack.playerNum = gt.getPlayerCount();
			
			ack.vipTableID = gt.getVipTableID();
			ack.creatorID = gt.getCreatorPlayerID();
			ack.creatorName = gt.getCreatorPlayerName();
			if(gt.getProxyCreator() != null){
				ack.proxyName = gt.getProxyCreator().getPlayerName();
			}
	
	    	ack.isRecord = gt.getIsRecordTable();
			ack.tablePassword = gt.getVipPswMd5();
			ack.tableRules = tableRule;
			ack.isSelectSite = gt.getIsSelectSite() ;
			ack.gameStart = 0;
			ack.clubCode = gt.getClubCode();
			ack.openRoomType = gt.getProxyState();
			// 进入桌子,发消息给玩家
			GameContext.gameSocket.send(pl.getSession(), ack);
 		}
		//
		logger.debug("【电子庄牛牛】vip table id=" + gt.getVipTableID());
		// 测试允许机器人进入vip
		// machineService.newset_machinetime(pl,gt);
	}
	
	private ClubMember validateClubGold(GameRoom rm, User pl, int clubCode, String roomLevel){
		IClubService clubService = (IClubService) SpringService.getBean("clubService");
		ClubMember clubMember = clubService.getClubMember(clubCode, pl.getPlayerID());
		if(clubMember == null){
			TClub club = clubService.getClub(clubCode);
			if(pl.getPlayerID().equals(club.getCreatePlayerId())){
				// 代理无限金币
				clubMember = new ClubMember();
				clubMember.setClubCode(String.valueOf(clubCode));
				// clubMember.setGoldNum(2000);
				return clubMember;
			}else{
				// 返回给玩家，
				RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
				ack.gold = pl.getGold();
				ack.roomType = rm.roomType; // kn add
				ack.result = ErrorCodeConstant.GOLD_LOW_THAN_MIN_LIMIT;
				// 进入桌子失败
				GameContext.gameSocket.send(pl.getSession(), ack);
				return null;
			}
		}
		TClubLevel clubLevel = clubService.getClubLevel(roomLevel);
		int gold = clubMember.getGoldNum();
		if(gold < clubLevel.getMinGold()){
			// 返回给玩家，
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
			ack.gold = pl.getGold();
			ack.roomType = rm.roomType; // kn add
			ack.result = ErrorCodeConstant.GOLD_LOW_THAN_MIN_LIMIT;
			// 进入桌子失败
			GameContext.gameSocket.send(pl.getSession(), ack);
			return null;
		}
		return clubMember;
	}

	// 看看是否有老桌需要他进去，牌局未结束，不能离开
	private boolean enter_old_table(User pl) {
		int roomID = pl.getRoomID();
		GameRoom rm = gameRoomMap.get(roomID);
		if (rm == null)
			return false;

		GameTable gt = rm.getTable(pl.getTableID(), pl.isPlayingSingleTable());

		if (gt != null && (gt.findPlayerIndex(pl) >= 0)) {
        	if(gt.getProxyCreator() != null && gt.getPlayers().size() == 0){//此房间为代开，不进入
        		return false;
            }else{//正常进入vip房间流程
				if ((gt.getState() == GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE || gt
						.getState() != GameConstant.TABLE_STATE_WAITING_PLAYER)) {
					if (!validatePlayerGold(rm, pl))
						return false;
				}
	
				enter_table_done(pl, gt, true);
	
				return true;
            }
		}

		return false;
	}

	//
	public void enter_room(User pl, int roomID) {
		// 重置异常标志
		pl.setNeedCopyGameState(false);

		// 当前正在vip房间中，不能进其他房间,自动进vip房
		if (pl.getVipTableID().length() > 0) {
			GameRoom rm = gameRoomMap.get(pl.getRoomID());
			if (rm == null)
				return;
			GameTable gt = rm.getTable(pl.getTableID(), false);

			if (gt != null) {
				List<User> gtPlayers = gt.getPlayers();
				for (int j = 0 ;j < gtPlayers.size(); j++){
					if (gtPlayers.get(j).getPlayerID().equals(pl.getPlayerID())){
						//有房间；还需判断玩家是否为代开者，如果是代开者，那么房间内超过两人，则为普通房间，即不能再代开，不能再开房间
						if(gt.getProxyCreator() == null || 
								(gt.getProxyCreator() != null && gtPlayers.size()>0)){
							EnterVipRoomMsg mxg = new EnterVipRoomMsg();
							mxg.tableID = gt.getTableID();
							mxg.psw = gt.getVipPswMd5();
							mxg.roomID = pl.getRoomID();
							enterVipRoom(mxg, pl);
							return;
						}
					}
				}
			}
		}

		GameRoom rm = gameRoomMap.get(roomID);
		// 看看是否进老桌子
		if (enter_old_table(pl)) {
			return;
		} else {

			// 看看是否还在桌子上
			List<GameTable> oldTables = this.getPlayingTables(pl
					.getPlayerIndex());
			if (oldTables != null && oldTables.size() >= 1) {
				// 还在老桌子中
				GameTable oldTable = oldTables.get(0);
	            if (oldTable != null && oldTable.getProxyCreator() == null || 
		            			(oldTable.getProxyCreator() != null && oldTable.getPlayers().size()>0)) {//代开优化 cc modify 2017-9-26
					logger.error("【"+oldTable.getGameId()+"】玩家:" + pl.getPlayerIndex()
							+ ",断线重连，遍历房间才找到老桌子,roomID=" + oldTable.getRoomID());

					pl.setRoomID(oldTable.getRoomID());
					pl.setTableID(oldTable.getTableID());
					pl.setNeedCopyGameState(true); // 玩家数据异常需要恢复游戏数据

					if (oldTable.isVipTable()) {
						pl.setVipTableID(String.valueOf(oldTable.getVipTableID()));

						// VIP房间处理
						EnterVipRoomMsg mxg = new EnterVipRoomMsg();
						mxg.tableID = oldTable.getTableID();
						mxg.psw = oldTable.getVipPswMd5();
						mxg.roomID = pl.getRoomID();
						enterVipRoom(mxg, pl);
						return;
					}
				}
			}

//			// 校验金币是否足够
//			if (!validatePlayerGold(rm, pl))
//				return;
		}

		// tieguo
		pl.setPlayerType(rm.roomType);
		playerService.updatePlayerType(pl.getPlayerID(), rm.roomType);
		//
		GameTable gt = rm.enterRoom(pl);
		
		enter_table_done(pl, gt, false);
	}

	public boolean validatePlayerGold(GameRoom rm, User pl) {

		if (rm == null)
			return false;

		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
		//
		// 进新桌,需要校验金币
		int minGold = rm.minGold;

		// vip房间要够固定带入的金币
		if (rm.roomType == GameConstant.ROOM_TYPE_VIP
				|| rm.roomType == GameConstant.ROOM_TYPE_COUPLE) {
			minGold = rm.fixedGold;
		}

		if (pl.getGold() < minGold) {
			// 返回给玩家，
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
			ack.gold = pl.getGold();
			ack.roomType = rm.roomType; // kn add
			ack.result = ErrorCodeConstant.GOLD_LOW_THAN_MIN_LIMIT;
			// 进入桌子失败
			GameContext.gameSocket.send(pl.getSession(), ack);
			return false;
		} else if (pl.getGold() > rm.maxGold) {
			// 返回给玩家，
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();
			ack.gold = pl.getGold();
			ack.roomType = rm.roomType; // kn add
			ack.result = ErrorCodeConstant.GOLD_HIGH_THAN_MAX_LIMIT;
			// 进入桌子失败
			GameContext.gameSocket.send(pl.getSession(), ack);
			return false;
		}

		// 如果是进入VIP房间，只要校验完金币就可以了
		if (rm.roomType == GameConstant.ROOM_TYPE_VIP
				|| rm.roomType == GameConstant.ROOM_TYPE_COUPLE) {
			// 返回给玩家，
			RequestStartGameMsgAck ack = new RequestStartGameMsgAck();

			ack.gold = pl.getGold();
			ack.roomID = rm.roomID;
			ack.roomType = rm.roomType; // kn add
			ack.result = ErrorCodeConstant.CAN_ENTER_VIP_ROOM;

			// 可以进入VIP房间
			GameContext.gameSocket.send(pl.getSession(), ack);
			return true;
		}
		return false;
	}

	/**
	 * 离开重新进入
	 * @author  2017-11-15
	 * TableLogicProcess.java
	 * @param pl
	 * @param gt
	 * void
	 */
	private void player_re_enter(User pl, GameTable gt) {
		// 打印所有玩家的状态
		logger.info("【电子庄牛牛】玩家:" + pl.getPlayerIndex() + "断线重回1111，房间ID:"
				+ gt.getVipTableID() + ",桌子状态：" + gt.getState() + ",前一个状态："
				+ gt.getBackup_state());
		// 恢复状态
		if (gt.getBackup_state() == GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE) {
			pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);

			// 给桌子上的其他玩家发个进入新玩家的消息
			PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();
			axk.opertaionID = GameConstant.OPT_TABLE_ADD_NEW_PLAYER;
			axk.playerName = pl.getPlayerName();
			if (gt.isVipTable()) {
				axk.gold = pl.getVipTableGold();
			} else {
				axk.gold = pl.getGold();
			}
			axk.headImg = pl.getHeadImg();
			axk.sex = pl.getSex();
			axk.tablePos = pl.getTablePos();
			axk.playerID = pl.getPlayerID();
			axk.playerIndex = pl.getPlayerIndex();
			axk.canFriend = pl.getCanFriend();
			axk.ip = pl.getClientIP();
			axk.location = pl.getLocation();
			axk.headImgUrl = pl.getHeadImgUrl();
			gt.sendMsgToTableExceptMe(axk, pl.getPlayerID());
		} else {
			pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_PLAYING);

			// 给桌子上的其他玩家发个玩家上线的消息
			PlayerOperationNotifyMsg axk = new PlayerOperationNotifyMsg();
			axk.operationType = GameConstant.DDZ_OPT_ONLINE;
			axk.playerTablePos = pl.getTablePos();
			gt.sendMsgToTableExceptMe(axk, pl.getPlayerID());
		}

		// 取消托管
		pl.setAutoOperation(0);

		// 发送断线重连消息
		player_recover(gt, pl);

		if (gt.getRoomType() == GameConstant.ROOM_TYPE_COUPLE) {
			pl.setTwoVipOnline(true);
		}

		// 看看桌子上玩家是否都已经到齐
		gt.pauseContinue();

		// 打印所有玩家的状态
		logger.info("【电子庄牛牛】玩家:" + pl.getPlayerIndex() + "断线重回2222，房间ID:"
				+ gt.getVipTableID() + ",桌子状态：" + gt.getState() + ",前一个状态："
				+ gt.getBackup_state());
		for (User onePl : gt.getPlayers()) {
			logger.info("【电子庄牛牛】玩家ID：" + onePl.getPlayerIndex() + " 状态："
					+ onePl.getGameState());
			long ctt = DateService.getCurrentUtilDate().getTime();
			onePl.setOpStartTime(ctt);
		}

		if (gt.getState() == GameConstant.TABLE_STATE_PLAYING ) {
			logger.info("【电子庄牛牛】重新提醒玩家操作，当前桌子SubState：" + gt.getPlaySubstate());
			re_notify_current_operation_player(gt, pl);
		}else if (gt.getState() == GameConstant.TABLE_STATE_PAUSED){

		}

		// pl.setOnTable(true);

		// 发送宝箱信息
		// playerService.sendBaoXiangCondition(pl);

		logger.info("【电子庄牛牛】back:" + pl.getPlayerName() + ",player num="
				+ gt.getPlayers().size());
	}

	private boolean add_table_to_tmgr(GameTable gt, GameRoom gr) {
		gt.game_room = gr;

		GameProcessThread tmgr = tmgr_thread_pool.addTable(gt);
		if (tmgr == null) {
			logger.error("【电子庄牛牛】add_table_to_tmgr faild! [tmgr == null]");
			return false;
		}else{
			if (table_mgr_map.get(gt.getTableID()) == null) {
				table_mgr_map.put(gt.getTableID(), tmgr);
				if (gt.gameProcessThread == null) {
					gt.gameProcessThread = tmgr;
				}
			}
			return true;
		}
	}
	
	/**
	 * 获取所有游戏房间
	 */
	public Map<Integer, GameRoom> getAllGameRoomMap() {
		return gameRoomMap;
	}

	/**
	 * 确定进入房间
	 * @author  2017-11-15
	 * TableLogicProcess.java
	 * @param pl
	 * @param gt
	 * @param re_enter
	 * void
	 */
	public void enter_table_done(User pl, GameTable gt, boolean re_enter) {
		// 玩家坐在桌子上
		pl.setOnTable(true);

		pl.setAutoOperation(0);
		if (gt.getRoomType() == GameConstant.ROOM_TYPE_COUPLE) {
			pl.setTwoVipOnline(true);
		}

		RequestStartGameMsgAck ack = new RequestStartGameMsgAck();

		if (gt.getState() == GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE) {
			pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
			// gt.set_not_ready_player_ready();
		} else if (gt.getState() == GameConstant.TABLE_STATE_PAUSED) {
			if (gt.getBackup_state() == GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE) {
				pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
			} else {
				pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_PLAYING);
			}
		}

		//
		ack.result = ErrorCodeConstant.CMD_EXE_OK;
		//
		ack.init_players(gt.getPlayers(), gt.isVipTable());
		//
		ack.gold = pl.getGold();
		//
		ack.roomID = pl.getRoomID();

		ack.coinBaseNum = gt.getCoinBaseNum();//gt.getDizhu();//CommonUtil.getTeamPlayerTablePos(pl.getTablePos());//

		ack.juCount = gt.getJuCount();

		ack.totalJuNum = gt.getTotalJuNum();

		ack.playerNum = gt.getPlayerCount();
		
		GameRoom rm = gameRoomMap.get(pl.getRoomID());
		if (rm == null)
			return;
		ack.roomType = rm.roomType;
		// end
		ack.gameStart = gt.isStartGame() ? 1 : 0;
		ack.clubCode = gt.getClubCode();

		ack.openRoomType = gt.getProxyState();
		ack.tablePos = pl.getTablePos();
        ack.isRecord = gt.getIsRecordTable();
        
		GameRoom gr = getRoom(gt.getRoomID());
		// 多线程支持
		if (gr != null) {
			add_table_to_tmgr(gt, gr);
		}
		// add end

		if ((gr != null)
				&& (gr.roomType == GameConstant.ROOM_TYPE_VIP || gr.roomType == GameConstant.ROOM_TYPE_COUPLE)) {
			ack.vipTableID = gt.getVipTableID();
			ack.tablePassword = gt.getVipPswMd5();
			ack.tableRules = gt.getTableRuleOption();

			logger.info("【电子庄牛牛】玩家昵称【" + pl.getPlayerName() + "】，ID【"
					+ pl.getPlayerIndex() + "】，加入VIP房:" + gr.roomType
					+ "，座位号【" + pl.getTablePos() + "】，房间ID【" + gt.getVipTableID() + "】，房间局数【"
					+ gt.getTotalJuNum() + "】");
		}
		ack.creatorID = gt.getCreatorPlayerID();
		ack.creatorName = gt.getCreatorPlayerName();
		if(gt.getProxyCreator() != null){
			ack.proxyName = gt.getProxyCreator().getPlayerName();
		}
		ack.isSelectSite = gt.getIsSelectSite() ;
		//
//		Date ct = DateService.getCurrentUtilDate();
		// 进入桌子，返回消息给玩家
		GameContext.gameSocket.send(pl.getSession(), ack);
		//
		//TODO 如果进入的是俱乐部房间，则给所有俱乐部玩家，发一个有玩家坐下的消息
		if(gt.getClubCode() != 0){
			PlayerDownOrUpMsgAck dmsg = new PlayerDownOrUpMsgAck();
			dmsg.clubCode = gt.getClubCode();
			dmsg.tableNo = gt.getVipTableID();
			dmsg.playerCount = gt.getPlayerCount();
			dmsg.type = 1;
			TableUserInfo userInfo = new TableUserInfo();
			userInfo.playerName = pl.getPlayerName();
			userInfo.playerId = pl.getPlayerID();
			userInfo.sex = pl.getSex() == 1 ? 1 : 0;
			userInfo.headImgUrl = pl.getHeadImgUrl();
			userInfo.tablePos = pl.getTablePos();
			dmsg.userInfo = userInfo;
			playerService.sendMsgToClubPlayer(gt.getClubCode(), dmsg);
		}

		if (re_enter){
        	//ask_recover(pl, pl.getRoomID(), gt.getGameId());
        	int state = gt.getState();
        	if(state == GameConstant.TABLE_STATE_WAITING_VIP_EXTEND_CARD){// 等待续卡，重新发提示显示当前续卡情况
//    			gt.re_extend_card_remind(pl);
    		}
        	
        	//判断解散房间操作，重新提醒；
        	int substate = gt.getPlaySubstate();
    		if (substate == GameConstant.TABLE_SUB_STATE_WAIT_APPLY_CLOSE_ROOM) {
    			//房间正在进行解散房间倒计时，给玩家推送解散消息 2017.2.23
    			Date dt = DateService.getCurrentUtilDate();
    			
    			int result = gt.checkIsClose(gt,pl);
    			
    			PlayerAskCloseVipRoomMsgAck ackAgree = new PlayerAskCloseVipRoomMsgAck();
            	ackAgree.table_id = gt.getVipTableID();
            	ackAgree.wait_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
            	ackAgree.lost_time = (int) (cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt() - ((dt.getTime() - gt.getApplyCloseStartTime())/1000));
            	ackAgree = gt.playerIsAgreeCloseRoom(ackAgree);
            	ackAgree.roomType = gt.getRoomType();
            	ackAgree.isOperate = pl.isAgreeCloseRoom();
            	if (pl.isApplyCloseRoom()){
            		ackAgree.tablePos = pl.getTablePos();
            	}else{
            		ackAgree.tablePos = pl.getTablePos() + 1;
            	}
            	
            	if (result == 2) {
            		ackAgree.result= 1;//继续游戏
            	}
    			GameContext.gameSocket.send(pl.getSession(), ackAgree);
    			logger.info("============================>【电子庄牛牛】房间ID【"+gt.getVipTableID()+"】解散房间请求过程中断线重连，取当前操作玩家，玩家位置【"+pl.getTablePos()
    					+"】，玩家ID【"+pl.getPlayerIndex()
    					+"】，玩家昵称【"+pl.getPlayerName()
    					+"】，牌桌状态【"+result+"】");
    		}
        }

		if (re_enter && (gt.getState() != GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE))// 断线返回，VIP房间在结算的时候断线回来要走下面的分支
		{
			player_re_enter(pl, gt);
		} else {
			// 给桌子上的其他玩家发个进入新玩家的消息
			PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();
			axk.opertaionID = GameConstant.OPT_TABLE_ADD_NEW_PLAYER;
			axk.playerName = pl.getPlayerName();
			if (gt.isVipTable()) {
				axk.gold = pl.getVipTableGold();
			} else {
				axk.gold = pl.getGold();
			}
			axk.headImg = pl.getHeadImg();
			axk.sex = pl.getSex();
			axk.tablePos = pl.getTablePos();
			axk.playerID = pl.getPlayerID();
			axk.playerIndex = pl.getPlayerIndex();
			axk.canFriend = pl.getCanFriend();
			axk.ip = pl.getClientIP();
			axk.location = pl.getLocation();
			axk.headImgUrl = pl.getHeadImgUrl();
			gt.sendMsgToTableExceptMe(axk, pl.getPlayerID());

		}
	}

	// 游戏循环
	public void run_one_loop() {
		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService
					.getBean("sysConfigService");
	}

	//
	public void use_item(User pl, MallItem ib) {

	}

	public void search_vip_room_players(RequestStartGameMsg msg, User pl) {
		int roomID = msg.roomID;// room id存在card_value中
		GameRoom gr = this.getRoom(roomID);
		if (gr == null)
			return;
		//
		GameTable gt = null;
		
		GameRoom rm = gameRoomMap.get(roomID);
		gt = gr.getVipTableByVipTableID(Integer.parseInt(msg.tableID));
		
		if (rm == null)
			return;
		
		SearchVipRoomMsgAck axk = new SearchVipRoomMsgAck();
		//
		if (gt != null) {
			// 房主回来
			if (gt.getCreatorPlayerID().equals(pl.getPlayerID())) {
				EnterVipRoomMsg msgx = new EnterVipRoomMsg();
				msgx.tableID = gt.getTableID();
				msgx.psw = gt.getVipPswMd5();
				msgx.roomID = roomID;
				enterVipRoom(msgx, pl);
				return;
			} else {
				// 判断是否其他玩家离开又回来
				int old_idx = gt.findPlayerIndex(pl);

				if (old_idx >= 0)// 已经存在，离开又进来
				{
					// 直接让他进入房间
					EnterVipRoomMsg msgx = new EnterVipRoomMsg();
					msgx.tableID = gt.getTableID();
					msgx.psw = gt.getVipPswMd5();
					msgx.roomID = roomID;
					enterVipRoom(msgx, pl);
					return;
				}
			}
			axk.vipTableID = gt.getVipTableID();
			axk.psw = gt.getVipPswMd5();
			axk.numPlayer = gt.getPlayerNum();
			axk.tableID = gt.getTableID();
			axk.dizhu = gr.price;
			axk.createName = gt.getCreatorPlayerName();
			axk.minGold = gr.fixedGold;
			axk.tableRule = gt.getTableRuleOption();
			axk.players = gt.getPlayers();
			axk.type=1;
		}
	
		GameContext.gameSocket.send(pl.getSession(), axk);

	}

	//
	private boolean search_vip_room(com.chess.nndzz.msg.struct.other.PlayerTableOperationMsg msg, User pl) {
		int roomID = msg.vipTableID;// room id存在card_value中
		GameRoom gr = this.getRoom(roomID);
		if (gr == null)
			return false;
		//
		GameTable gt = null;

		if (msg.opValue == 0) {
			gt = gr.getTable(pl.getVipTableID(), false);
			if (gt == null)
				return false;// 默认查找自己开的房间，如果没有，不返回数据
		} else {
			gt = gr.getVipTableByVipTableID(msg.opValue);
		}
		//
		SearchVipRoomMsgAck axk = new SearchVipRoomMsgAck();
		//
		if (gt != null) {
			// 房主回来
			if (gt.getCreatorPlayerID().equals(pl.getPlayerID())) {
				EnterVipRoomMsg msgx = new EnterVipRoomMsg();
				msgx.tableID = gt.getTableID();
				msgx.psw = gt.getVipPswMd5();
				msgx.roomID = roomID;
				enterVipRoom(msgx, pl);
				return true;
			} else {
				// 判断是否其他玩家离开又回来
				int old_idx = gt.findPlayerIndex(pl);

				if (old_idx >= 0)// 已经存在，离开又进来
				{
					// 直接让他进入房间
					EnterVipRoomMsg msgx = new EnterVipRoomMsg();
					msgx.tableID = gt.getTableID();
					msgx.psw = gt.getVipPswMd5();
					msgx.roomID = roomID;
					enterVipRoom(msgx, pl);
					return true;
				}
			}
			axk.vipTableID = gt.getVipTableID();
			axk.psw = gt.getVipPswMd5();
			axk.numPlayer = gt.getPlayerNum();
			axk.tableID = gt.getTableID();
			if (gt.isStartGame() )
				axk.dizhu = -1;
			else
				axk.dizhu = gr.price;
			axk.createName = gt.getCreatorPlayerName();
			axk.minGold = gr.fixedGold;
			axk.tableRule = gt.getTableRuleOption();
			axk.type = roomID;
			axk.players = gt.getPlayers();
			axk.openRoomType = gt.getProxyState();
			GameContext.gameSocket.send(pl.getSession(), axk);
			return true;
		}
		GameContext.gameSocket.send(pl.getSession(), axk);
		return false;
	}

//	// 玩家申请解散房间
//	public void player_close_vip_room(PlayerAskCloseVipRoomMsg msg, User pl) {
//		if (pl == null)
//			return;
//		GameRoom gr = this.getRoom(pl.getRoomID());
//		if (gr == null)
//			return;
//		GameTable gt = gr.getTable(pl.getTableID(), pl.isPlayingSingleTable());
//		if (gt == null) {
//			return;
//		}
//
//		if (gt.getVipTableID() != msg.table_id) {
//			return;
//		}
//
//		logger.error("玩家:" + pl.getPlayerIndex() + ",申请解散VIP房间:"
//				+ gt.getVipTableID());
//		String detail = "";
//		switch (msg.OPT_id) {
//		case GameConstant.OPERATION_PLAYER_APPLY_CLOSE_VIP_ROOM:
//			// 有玩家申请解散，把消息发给其它玩家
//			Date ct = DateService.getCurrentUtilDate();
//			String playerID = pl.getPlayerID();
//			Long lastTime = gt.getPlayerPrevApplyCloseVipRoomTime(playerID);
//			if (lastTime == null || lastTime == 0L) {
//				gt.playerPrevApplyCloseVipRoom(playerID, ct.getTime());
//			} else {
//				long delta = ct.getTime() - lastTime;
//				if (paused_choose_wait_time == 0) {
//					paused_choose_wait_time = cfgService.getPara(
//							ConfigConstant.VIP_PAUSED_CHOOSE_WAIT_TIME)
//							.getValueInt();
//				}
//				if (delta < paused_choose_wait_time * 1000) {
//					// 太频繁了，提示
//					PlayerAskCloseVipRoomMsgAck ack = new PlayerAskCloseVipRoomMsgAck();
//					ack.table_id = gt.getVipTableID();
//					ack.isoffen = 1;
//					ack.tablePos = pl.getTablePos();
//					GameContext.gameSocket.send(pl.getSession(), ack);
//					return;
//				} else {
//					gt.playerPrevApplyCloseVipRoom(playerID, ct.getTime());
//				}
//			}
//			PlayerAskCloseVipRoomMsgAck ack = new PlayerAskCloseVipRoomMsgAck();
//			ack.askerName = pl.getPlayerName();
//			ack.table_id = gt.getVipTableID();
//			ack.tablePos = pl.getTablePos();
//			ack.isoffen = 0;
//			
//			
//			if (cfgService == null)
//				cfgService = (ISystemConfigService) SpringService
//						.getBean("sysConfigService");
//			ack.wait_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
//			ack.lost_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
//
//        	
//			gt.setCloseVipRoomNum(0);
//			gt.sendMsgToTableExceptMe(ack, pl.getPlayerID());
//			// gt.sendMsgToTable(ack);
//			detail = "玩家:" + msg.player_id + " 申请解散房间！";
//			pl.setApplyCloseRoom(true);
//			playerService.createPlayerLog(pl.getPlayerID(),
//					pl.getPlayerIndex(), pl.getPlayerName(),
//					gt.getVipTableID(),
//					LogConstant.OPERATION_TYPE_APPLY_CLOSE_VIP_ROOM,
//					LogConstant.OPERATION_TYPE_PLAYER_AGREE_CLOSE_VIP_ROOM,
//					pl.getVipTableGold(), detail, LogConstant.MONEY_TYPE_GOLD);
//
//			break;
//		case GameConstant.OPERATION_CLOSE_VIP_ROOM_FEEDBACK:
//			switch (msg.agree) {
//			case 1:
//				gt.setCloseVipRoomNum(gt.getCloseVipRoomNum() + 1);
//				detail = "玩家:" + pl.getPlayerIndex().toString() + " 同意解散房间！";
//				playerService.createPlayerLog(pl.getPlayerID(),
//						pl.getPlayerIndex(), pl.getPlayerName(),
//						gt.getVipTableID(),
//						LogConstant.OPERATION_TYPE_APPLY_CLOSE_VIP_ROOM,
//						LogConstant.OPERATION_TYPE_PLAYER_AGREE_CLOSE_VIP_ROOM,
//						pl.getVipTableGold(), detail,
//						LogConstant.MONEY_TYPE_GOLD);
//				// kn add
//				if (gr.roomType != GameConstant.ROOM_TYPE_COUPLE) {
//					if (gt.getCloseVipRoomNum() >= 1) {
//						logger.error("同意人数达到2人，解散VIP房间:" + gt.getVipTableID());
//						// 结束VIP房间
//						vipTableEnd(gt, pl);
//						gt.resetIsAgreeCloseRoom();
//					}
//				} else {
//					if (gt.getCloseVipRoomNum() >= 1) {
//						logger.error("2人场，同意人数达到1人，解散VIP房间:"
//								+ gt.getVipTableID());
//						// 结束VIP房间
//						// 2人vip
//						for (User plx : gt.getPlayers()) {
//							plx.setTwoVipOnline(false);
//						}
//
//						pl.setPlayerType(0);
//						playerService.updatePlayerType(pl.getPlayerID(), 0);
//						logger.info("player_close_vip_room:");
//						vipTableEnd(gt, pl);
//						gt.resetIsAgreeCloseRoom();
//					}
//				}
//				break;
//			case 0:
//				gt.setDisagreeCloseVipRoomNum(gt.getDisagreeCloseVipRoomNum() + 1);
//				detail = "玩家:" + pl.getPlayerIndex().toString() + " 不同意解散房间！";
//				gt.resetIsAgreeCloseRoom();
//				playerService
//						.createPlayerLog(
//								pl.getPlayerID(),
//								pl.getPlayerIndex(),
//								pl.getPlayerName(),
//								gt.getVipTableID(),
//								LogConstant.OPERATION_TYPE_APPLY_CLOSE_VIP_ROOM,
//								LogConstant.OPERATION_TYPE_PLAYER_DISAGREE_CLOSE_VIP_ROOM,
//								pl.getVipTableGold(), detail,
//								LogConstant.MONEY_TYPE_GOLD);
//				break;
//			}
//
//		}
//	}

    //玩家申请解散房间
    public void player_close_vip_room(PlayerAskCloseVipRoomMsg msg, User pl) {
        if (pl == null) return;
        GameRoom gr = this.getRoom(pl.getRoomID());
        if (gr == null)
            return;
        GameTable gt = gr.getTable(pl.getTableID(), pl.isPlayingSingleTable());
        if (gt == null) {
            return;
        }

        if (gt.getVipTableID() != msg.table_id) {
            return;
        }
        Date dt = DateService.getCurrentUtilDate();
        logger.error("【电子庄牛牛】玩家:" + pl.getPlayerIndex() + ",申请解散VIP房间:" + gt.getVipTableID());
        String detail = "";
        switch (msg.OPT_id) {
            case GameConstant.OPERATION_PLAYER_APPLY_CLOSE_VIP_ROOM:
            	logger.info("【电子庄牛牛】------ 申请解散房间间隔时间是"+cfgService.getPara(ConfigConstant.VIP_PAUSED_CHOOSE_WAIT_TIME).getValueInt());
                //如果已经申请解散不要重复申请
            	if (gt.getState() != GameConstant.TABLE_STATE_PLAYING  && gt.getState() != GameConstant.TABLE_STATE_PAUSED){
            		//可以解散
            		return ;
            	}
            	//有玩家申请解散，把消息发给其它玩家	
                Date ct = DateService.getCurrentUtilDate();
                if (gt.getPrevApplyCloseVipRoomTime() == 0) {
                    gt.setPrevApplyCloseVipRoomTime(ct.getTime());
                    logger.info("====================【电子庄牛牛】记录解散房间的时间："+ gt.getPrevApplyCloseVipRoom());
                } else {
                	logger.info("====================【电子庄牛牛】申请解散房间，上次申请时间为："+ gt.getPrevApplyCloseVipRoom());
                    long delta = ct.getTime() - gt.getPrevApplyCloseVipRoomTime();
                    if (paused_choose_wait_time == 0) {
                        paused_choose_wait_time = cfgService.getPara(ConfigConstant.VIP_PAUSED_CHOOSE_WAIT_TIME).getValueInt();
                    }
                    if (delta < paused_choose_wait_time * 1000) {
                    	// 太频繁了，提示
                    	PlayerAskCloseVipRoomMsgAck ack = new PlayerAskCloseVipRoomMsgAck();
                    	ack.table_id = gt.getVipTableID();
                    	ack.isoffen = 1;
                    	ack.tablePos = pl.getTablePos();
                    	GameContext.gameSocket.send(pl.getSession(), ack);
                        return;
                    }else{
                    	
                    	gt.setPrevApplyCloseVipRoomTime(0L);
                    }
                }
              //如果已经申请解散不要重复申请
            	if(gt.getPlaySubstate() == GameConstant.TABLE_SUB_STATE_WAIT_APPLY_CLOSE_ROOM) {
                	return;
                }
                //修改玩家关闭房间的状态，并开始倒计时
                if (pl.isAgreeCloseRoom() >0 ){
            		
            		return ;
            	}
                gt.setPrevApplyCloseVipRoomTime(ct.getTime());
                logger.info("=======================【电子庄牛牛】记录解散房间的时间："+ gt.getPrevApplyCloseVipRoom());
                table_state_before_apply_close_room = gt.getPlaySubstate();
                logger.info("=======================【电子庄牛牛】玩家申请解散房间，记录房间状态："+ table_state_before_apply_close_room);
                gt.setPlaySubstate(GameConstant.TABLE_SUB_STATE_WAIT_APPLY_CLOSE_ROOM);
                pl.setAgreeCloseRoom(1);
                pl.setApplyCloseRoom(true);
                gt.setCloseVipRoomNum(0);
                gt.setDisAgreeCloseVipRoomNum(0);
                //等待玩家选择是否同意解散房间的时间
                
        		gt.setApplyCloseStartTime(dt.getTime());
        		
                PlayerAskCloseVipRoomMsgAck ack = new PlayerAskCloseVipRoomMsgAck();
                ack.askerName = pl.getPlayerName();
                ack.table_id = gt.getVipTableID();
                ack.wait_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
                //ack.lost_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
                ack.lost_time = (int) (cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt() - ((dt.getTime() - gt.getApplyCloseStartTime())/1000));
                
                // 2017.2.17 解散房间添加状态显示
                ack = gt.playerIsAgreeCloseRoom(ack);
                //发起解散申请的申请人的tablepos
                ack.tablePos = pl.getTablePos();
                ack.roomType = gt.getRoomType();
                gt.sendMsgToTable(ack);
                detail = "玩家:" + msg.player_id + " 申请解散房间！";
                playerService.createPlayerLog(pl.getPlayerID(),
                        pl.getPlayerIndex(),
                        pl.getPlayerName(),
                        gt.getVipTableID(),
                        LogConstant.OPERATION_TYPE_APPLY_CLOSE_VIP_ROOM,
                        LogConstant.OPERATION_TYPE_PLAYER_AGREE_CLOSE_VIP_ROOM,
                        pl.getVipTableGold(),
                        detail,
                        LogConstant.MONEY_TYPE_GOLD,pl.getGameId());

                break;
            case GameConstant.OPERATION_CLOSE_VIP_ROOM_FEEDBACK:
                switch (msg.agree) {
                    case 1:
                    	// 解散房间返回，通知所有玩家 xuyingquan 2017-4-12
                    	/// 2017.2.17由玩家同意解散房间，发送消息通知所有玩家；
                    	if (pl.isAgreeCloseRoom() >0 ){
                    		
                    		return ;
                    	}
                    	pl.setAgreeCloseRoom(1);
                    	gt.setCloseVipRoomNum(gt.getCloseVipRoomNum() + 1);
                    	int result = gt.checkIsClose(gt,pl);
                    	
                    	PlayerAskCloseVipRoomMsgAck ackAgree = new PlayerAskCloseVipRoomMsgAck();
	                	ackAgree.table_id = gt.getVipTableID();
	                	ackAgree.wait_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
	                	//ackAgree.lost_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
	                	ackAgree.lost_time = (int) (cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt() - ((dt.getTime() - gt.getApplyCloseStartTime())/1000));
	                    
	                	ackAgree = gt.playerIsAgreeCloseRoom(ackAgree);
	                	ackAgree.roomType = gt.getRoomType();
	                	ackAgree.tablePos = pl.getTablePos();
	                	if (result == 2) {
	                		ackAgree.result= 1;//继续游戏
	                	}
	                    gt.sendMsgToTable(ackAgree);
                    	
                        detail = "玩家:" + pl.getPlayerIndex().toString() + " 同意解散房间！";
                        playerService.createPlayerLog(pl.getPlayerID(),
                                pl.getPlayerIndex(),
                                pl.getPlayerName(),
                                gt.getVipTableID(),
                                LogConstant.OPERATION_TYPE_APPLY_CLOSE_VIP_ROOM,
                                LogConstant.OPERATION_TYPE_PLAYER_AGREE_CLOSE_VIP_ROOM,
                                pl.getVipTableGold(),
                                detail,
                                LogConstant.MONEY_TYPE_GOLD,pl.getGameId());
                        try   
                        {   
                        	//Thread.currentThread();
							Thread.sleep(500);//毫秒   
                        }   
                        catch(Exception e){}  
                        if(result == 1 ){
                        	// 结束VIP房间
                            vipTableEnd(gt, pl);
                            gt.resetIsAgreeCloseRoom();
                            logger.info("=======================【电子庄牛牛】解散vip，状态更新为："+ table_state_before_apply_close_room);
                    		gt.setPlaySubstate(table_state_before_apply_close_room);
                        }else if (result == 2) {
                        	//继续游戏
                        	gt.resetIsAgreeCloseRoom();
                    		gt.setPlaySubstate(table_state_before_apply_close_room);
                    		logger.info("=======================【电子庄牛牛】继续游戏vip，状态更新为："+ table_state_before_apply_close_room);
                        }
                        break;
                    case 0:	// 解散房间不同意
                    	if (pl.isAgreeCloseRoom() > 0) {
                    		
                    		return ;
                    	}
                    	pl.setAgreeCloseRoom(2);
                    	gt.setDisAgreeCloseVipRoomNum(gt.getDisAgreeCloseVipRoomNum() + 1);
                        int result1 = gt.checkIsClose(gt,pl);
                        detail = "玩家:" + pl.getPlayerIndex().toString() + " 不同意解散房间！";
                        
                        PlayerAskCloseVipRoomMsgAck ackNotAgree = new PlayerAskCloseVipRoomMsgAck();
                        ackNotAgree.table_id = gt.getVipTableID();
                        ackNotAgree.wait_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
                        //ackNotAgree.lost_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
                        ackNotAgree.lost_time = (int) (cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt() - ((dt.getTime() - gt.getApplyCloseStartTime())/1000));

                        ackNotAgree = gt.playerIsAgreeCloseRoom(ackNotAgree);
                        ackNotAgree.roomType = gt.getRoomType();
                        ackNotAgree.tablePos = pl.getTablePos();
                        if (result1 == 2) {
                        	ackNotAgree.result= 1;//继续游戏
	                	}
	                    gt.sendMsgToTable(ackNotAgree);
	                    
                        playerService.createPlayerLog(pl.getPlayerID(),
                                pl.getPlayerIndex(),
                                pl.getPlayerName(),
                                gt.getVipTableID(),
                                LogConstant.OPERATION_TYPE_APPLY_CLOSE_VIP_ROOM,
                                LogConstant.OPERATION_TYPE_PLAYER_DISAGREE_CLOSE_VIP_ROOM,
                                pl.getVipTableGold(),
                                detail,
                                LogConstant.MONEY_TYPE_GOLD,pl.getGameId());
                        
                        if(result1 == 1 ){
                        	// 结束VIP房间
                            vipTableEnd(gt, pl);
                            gt.resetIsAgreeCloseRoom();
                    		gt.setPlaySubstate(table_state_before_apply_close_room);
                    		logger.info("=======================【电子庄牛牛】解散vip，状态更新为："+ table_state_before_apply_close_room);
                        }else if (result1 == 2) {
                        	//继续游戏
                        	gt.resetIsAgreeCloseRoom();
                    		gt.setPlaySubstate(table_state_before_apply_close_room);
                    		logger.info("=======================【电子庄牛牛】继续游戏vip，状态更新为："+ table_state_before_apply_close_room);
                        }
                        break;
                }
                

        }
    }

    /**
     * 解散VIP房间超时，默认同意
     */
    public void playerCloseRoomOverTime(GameTable gt){
    	gt.setPlaySubstate(table_state_before_apply_close_room);
    	Date dt = DateService.getCurrentUtilDate();
		String detail = "";
		List<User> players = gt.getPlayers();
		for (int i = 0; i < players.size(); i++) {
			User user = players.get(i);
			if (user.isAgreeCloseRoom() == 0 ){
				//等待选择的玩家，超时默认同意解散
				user.setAgreeCloseRoom(1);
				gt.setCloseVipRoomNum(gt.getCloseVipRoomNum() + 1);
				int result = gt.checkIsClose(gt,user);
				
            	PlayerAskCloseVipRoomMsgAck ackAgree = new PlayerAskCloseVipRoomMsgAck();
            	ackAgree.table_id = gt.getVipTableID();
            	ackAgree.wait_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
            	//ackAgree.lost_time = cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt();
            	ackAgree.lost_time = (int) (cfgService.getPara(ConfigConstant.VIP_GAME_REQUEST_CLOSE_ROOM_WAIT_TIME).getValueInt() - ((dt.getTime() - gt.getApplyCloseStartTime())/1000));

            	ackAgree = gt.playerIsAgreeCloseRoom(ackAgree);
            	ackAgree.roomType = gt.getRoomType();
            	ackAgree.tablePos = user.getTablePos();
            	if (result == 2) {
            		ackAgree.result= 1;//继续游戏
            	}
            	gt.sendMsgToTable(ackAgree);
            	
                detail = "玩家:" + user.getPlayerIndex().toString() + " 超时默认同意解散房间！";
                playerService.createPlayerLog(user.getPlayerID(),
                		user.getPlayerIndex(),
                		user.getPlayerName(),
                		gt.getVipTableID(),
                        LogConstant.OPERATION_TYPE_APPLY_CLOSE_VIP_ROOM,
                        LogConstant.OPERATION_TYPE_PLAYER_AGREE_CLOSE_VIP_ROOM,
                        user.getVipTableGold(),
                        detail,
                        LogConstant.MONEY_TYPE_GOLD,user.getGameId());
                
                if(result == 1 ){
                	// 结束VIP房间
                    vipTableEnd(gt, user);
                    gt.resetIsAgreeCloseRoom();
            		gt.setPlaySubstate(table_state_before_apply_close_room);
            		return ;
                }else if (result == 2) {
                	//继续游戏
                	gt.resetIsAgreeCloseRoom();
            		gt.setPlaySubstate(table_state_before_apply_close_room);
            		return ;
                }
			}
		}
	}
    
	/**
	 * 房主申请解散VIP房间
	 */
	public void applyCloseVipTable(User pl) {
		GameRoom gr = this.getRoom(pl.getRoomID());
		if (gr == null) {
			logger.info("【电子庄牛牛】房主:" + pl.getPlayerIndex() + ",申请解散VIP房间 gr==null");
			return;
		}

		GameTable gt = gr.getTable(pl.getTableID(), pl.isPlayingSingleTable());
		if (gt == null) {
			logger.info("【电子庄牛牛】房主" + pl.getPlayerIndex() + ",申请解散VIP房间 gt==null");
			return;
		}
		if (!gt.getCreatorPlayerID().equals(pl.getPlayerID())) {
			// logger.info("房主:" + gt.getCreator().getPlayerIndex() + ",申请者:" +
			// pl.getPlayerIndex());
			return;
		}

		logger.info("【电子庄牛牛】房主" + pl.getPlayerIndex() + ",申请解散VIP房间:"
				+ gt.getVipTableID() + ",成功!");
		if (!gt.canCloseVipRoom) {
			return;
		}
		
		
		
		// 结束VIP房间
		vipTableEnd(gt, pl);
	}

	public void player_left_table(User pl, Boolean isLeft, boolean isForce) {
		if (pl == null)
			return;
		GameRoom gr = this.getRoom(pl.getRoomID());
		if (gr == null)
			return;
		//
		GameTable gt = gr.getTable(pl.getTableID(), pl.isPlayingSingleTable());
		if (gt == null) {
			return;
		}
		// 判断玩家是否是在桌子上的，如果不是就返回，防止出现开局后其他玩家查找房间没进去后关闭客户端导致错发短线消息
		boolean isInTable = false;
		for( User user:gt.getPlayers() ){
			if( user.getPlayerID() == pl.getPlayerID() )
				isInTable = true;
		}
		if( !isInTable ){
			return;
		}
		
		int clubCode = gt.getClubCode();
		int leftTablePos = pl.getTablePos();

		Boolean canLeft = false;
		if(isForce){
			// 验证游戏中如果押注不能退出，必须不压住才能退出
			if(pl.getUserGameHanderForNNDZZ() != null){
				if(pl.getUserGameHanderForNNDZZ().isStakeCoin()){
					PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();
					axk.opertaionID = GameConstant.FORCE_EXIT_GAME_FAILED;
					GameContext.gameSocket.send(pl.getSession(), axk);
					return;
				}
			}
			
			// 可以退出，通知前台
			PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();
			axk.opertaionID = GameConstant.FORCE_EXIT_GAME_SUCCESS;
			GameContext.gameSocket.send(pl.getSession(), axk);
			
			canLeft = true;
		}else{
			if (isLeft) {
				canLeft = false;
			} else {
				canLeft = gt.couldLeft(pl);
			}
		}
		
		// tieguo
		pl.setTwoVipOnline(false);
		pl.setPlayerType(0);
		playerService.updatePlayerType(pl.getPlayerID(), 0);
		logger.info("==========>【电子庄牛牛】玩家离开房间：房间ID【" + gt.getVipTableID() + "】，玩家ID【"
				+ pl.getPlayerIndex() + "】玩家位置【" + pl.getTablePos() + "】，玩家昵称【"
				+ pl.getPlayerName() + "】");

		// 玩家不在桌子上
		pl.setOnTable(false);

		if (canLeft) {
			//
			boolean found = gt.removePlayer(pl.getPlayerID());

			//
			if (found) {
				gr.setPlayerNum(gr.getPlayerNum() - 1);
				gt.addFreePos(pl.getTablePos());
			}

			User newCreator = null;
			List<User> players = gt.getPlayers();
			// 如果vip离开桌子,全部清理
			if (gt.isVipTable() && pl.getPlayerID().equals(gt.getCreatorPlayerID())) {
				if (gt.getPlayerNum() > 0) {
					// 房主离开，还有其他玩家，把下一个玩家设为房主
					newCreator = players.get(0);
					gt.setCreatorPlayerID(newCreator.getPlayerID());
					gt.setCreatorPlayerName(newCreator.getPlayerName());
					logger.info("==========>【电子庄牛牛】玩家离开房间：房间ID【" + gt.getVipTableID() + "】，玩家ID【"
							+ pl.getPlayerIndex() + "】玩家位置【" + pl.getTablePos() + "】，玩家昵称【"
							+ pl.getPlayerName() + "】，房主离开还有其他人，把房主之位传给玩家【"+newCreator.getPlayerName()+"，"+newCreator.getPlayerIndex()+"】");
					
					// 重进一下桌，变成房主，好开始游戏
					enter_table_done(newCreator, gt, true);
				}else{
					// 没有其他玩家
					if(gt.isStartGame()){
						logger.info("==========>【电子庄牛牛】玩家离开房间：房间ID【" + gt.getVipTableID() + "】，玩家ID【"
								+ pl.getPlayerIndex() + "】玩家位置【" + pl.getTablePos() + "】，玩家昵称【"
								+ pl.getPlayerName() + "】，房主离开且已经开局，房间没人了，解散房间");
						
						// 开始游戏了，解散房间，要不其他人进来局数不对
						gt.clear_all();
						
						if(clubCode > 0){
							logger.info("==========>【电子庄牛牛】玩家离开房间：房间ID【" + gt.getVipTableID() + "】，玩家ID【"
									+ pl.getPlayerIndex() + "】玩家位置【" + pl.getTablePos() + "】，玩家昵称【"
									+ pl.getPlayerName() + "】，俱乐部ID【"+clubCode+"】，通知其他玩家最新的牌桌情况");
							
				        	//TODO 发消息通知前台俱乐部的所有人，当前俱乐部各种牌桌状态
							playerService.sendMsgToClubPlayer(clubCode, playerService.newGetSingleClubInfoMsgAck(clubCode));
				        }
					}else{
						// 没开始游戏，不处理，等待其他人进入
					}
				}
			}
			//
			if (gt.getPlayerNum() > 0) {
				// 如果还有玩家，通知他们有玩家离开
				if (found) {
					PlayerGameOpertaionAckMsg axk = new PlayerGameOpertaionAckMsg();
					axk.opertaionID = GameConstant.OPT_PLAYER_LEFT_TABLE;
					axk.playerName = pl.getPlayerName();
					axk.gold = pl.getGold();
					axk.headImg = pl.getHeadImg();
					axk.sex = pl.getSex();
					axk.playerID = pl.getPlayerID();
					axk.playerIndex = pl.getPlayerIndex();
					axk.canFriend = pl.getCanFriend();
					axk.tablePos = pl.getTablePos();
					axk.ip = pl.getClientIP();
					axk.location = pl.getLocation();
					axk.headImgUrl = pl.getHeadImgUrl();
					for(User px : players){
						if(px.getPlayerID().equals(pl.getPlayerID())){
							continue;
						}
						if(newCreator != null && px.getPlayerID().equals(newCreator.getPlayerID())){
							continue;
						}
						GameContext.gameSocket.send(px.getSession(), axk);
					}
				}
			}

			//
			if (gt.shouldRecycleTable()) {
				// death_machine(gt);
				// 空桌回收
				gt.setState(GameConstant.TABLE_STATE_INVALID);
			}

			//如果退出的是俱乐部房间，则给所有俱乐部玩家，发一个有离开的消息
			if(clubCode != 0){
				logger.info("有玩家离开俱乐部牌桌，离开的玩家位置是：【" + leftTablePos + "】");
				PlayerDownOrUpMsgAck dmsg = new PlayerDownOrUpMsgAck();
				dmsg.clubCode = clubCode;
				dmsg.tableNo = gt.getVipTableID();
				dmsg.playerCount = gt.getPlayerCount();
				dmsg.type = 0;
				TableUserInfo userInfo = new TableUserInfo();
				userInfo.playerName = pl.getPlayerName();
				userInfo.playerName = pl.getPlayerID();
				userInfo.sex = pl.getSex() == 1 ? 1 : 0;
				userInfo.headImgUrl = pl.getHeadImgUrl();
				userInfo.tablePos = leftTablePos;
				dmsg.userInfo = userInfo;
				playerService.sendMsgToClubPlayer(clubCode, dmsg);
			}
			
			pl.clear_game_state();
		} else {
			logger.info("TableLogicProcess->VIP房【" + gt.getVipTableID()
					+ "】玩家离开牌桌，设置为暂停，玩家ID【" + pl.getPlayerIndex() + "】，玩家昵称【"
					+ pl.getPlayerName() + "】，牌桌状态【" + gt.getState() + "】");
			// 修改：清理离开玩家开局准备状态  并发送前台 20170209
			pl.setReadyFlag(0);
			GameReadyMsg msgReady = new GameReadyMsg();
			msgReady.game_id = gt.getGameId();
			msgReady.opt_id = 0;
			msgReady.room_id = gt.getRoomType();
			msgReady.table_id = gt.getRoomID();
			msgReady.player_id = pl.getPlayerID();
			logger.error("---------->开局准备gameReday，玩家ID【" + pl.getPlayerIndex()
					+ "】, 玩家昵称【" + pl.getPlayerName() + "】, gameID="
					+ msgReady.game_id + ", roomID=" + msgReady.room_id
					+ ", tableID=" + msgReady.table_id + ", playerID="
					+ msgReady.player_id + ", 是否准备【" + msgReady.opt_id + "】");
			gt.sendMsgToTable(msgReady);
			
			// 玩家正在游戏，保持数据，等待一定时间，等他回来
			if (gt.isVipTable()) {
				// 游戏暂停
				if ((gt.getState() != GameConstant.TABLE_STATE_PAUSED)
						&& gt.getState() != GameConstant.TABLE_STATE_WAITING_VIP_EXTEND_CARD//等待续卡
				){
						//&& (gt.getState() != GameConstant.TABLE_STATE_WAITING_PLAYER)) {
					// 保存当前状态
					gt.setBackup_state(gt.getState());
					Date ct = DateService.getCurrentUtilDate();
					gt.setPausedTime(ct.getTime());
					gt.setState(GameConstant.TABLE_STATE_PAUSED);
				}
			} else {
				// 普通房间设置成托管
				pl.setAutoOperation(1);
			}
			// 玩家状态，暂停
			pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_PAUSED);
			//
			// 玩家离开
			PlayerOperationNotifyMsg msg = new PlayerOperationNotifyMsg();
			msg.operationType = GameConstant.DDZ_OPT_OFFLINE;
			msg.playerTablePos = pl.getTablePos();
			gt.sendMsgToTableExceptMe(msg, pl.getPlayerID());
		}

		// debug,如果玩家都离开了，把桌子回收；
		if (gt.isAllPlayerLeft()) {
			if (gt.isVipTable()) {
				// 不马上结束，保留一段时间
				// vip_table_end(gr, gt);
			} else {
				gt.clear_all();
				gt.setState(GameConstant.TABLE_STATE_INVALID);
			}
		}
	}

	//
	public void playerOperation(PlayerTableOperationMsg msg, User pl) {
		if (msg == null || pl == null)
			return;

		// TableManager tmgr = get_table_manager(pl.getTableID());
		// if(tmgr == null) {
		// return;
		// }
		try {
			switch (msg.operationType) {
			case GameConstant.DDZ_OPT_SEARCH_VIP_ROOM:
				search_vip_room(msg, pl);
				return;
			case GameConstant.DDZ_OPT_GAME_OVER_CONTINUE:
				gameContinue(pl);
				return;
			case GameConstant.DDZ_OPT_GAME_OVER_CHANGE_TABLE:
				// 记录roomid
				int roomID = pl.getRoomID();
				// 先离开
				player_left_table(pl, true, false);
				// 再加入
				enter_room(pl, roomID);
				return;
			}
			pushMsgTableMgr(pl.getTableID(), msg, pl, msg.operationType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return;
	}
	
	private void re_notify_current_operation_player(GameTable gt, User pl) {
		pushMsgTableMgr(gt.getTableID(), null, pl,
				GameConstant.OPERATION_RE_NOTIFY_CURRENT_OPERATION_PLAYER);
	}

	// vip 结束
	private void vipTableEnd(GameTable gt, User pl) {
		List<User> players = gt.getPlayers();
    	String endWay = "";
		for (int i = 0; i < players.size(); i++) {
			User user = players.get(i);
			// 玩家不在桌子上
			user.setOnTable(false);
			if(user.isApplyCloseRoom()){
				endWay = "玩家"+user.getPlayerName()+"申请解散";
			}
		}
		gt.setTableEndWay(endWay);
		pushMsgTableMgr(gt.getTableID(), null, pl,
				GameConstant.OPERATION_VIP_TABLE_END);
		table_mgr_map.remove(gt.getTableID());
	}

	public ISystemConfigService getCfgService() {
		return cfgService;
	}

	public void setCfgService(ISystemConfigService cfgService) {
		this.cfgService = cfgService;
	}

	public List<GameRoom> getGameRoomList() {
		return roomList;
	}

	public void setGameRoomList(List<GameRoom> gameRoomList) {
		this.roomList = gameRoomList;
	}

	public CacheUserService getPlayerService() {
		return playerService;
	}

	/**
	 * 强制结束游戏，用于从后台结束非法桌子
	 */
	public void gameOverForce(User pl) {
		logger.info("【电子庄牛牛】强制清理玩家信息，结束非法桌子");

		int roomID = pl.getRoomID();
		GameRoom rm = gameRoomMap.get(roomID);
		if (rm == null) {
			logger.info("【电子庄牛牛】找不到房间，不需要结束");
			return;
		}

		GameTable gt = rm.getTable(pl.getTableID(), pl.isPlayingSingleTable());
		if (gt != null) {
			logger.error("【电子庄牛牛】房间ID:" + roomID + " 桌子ID：" + pl.getTableID()
					+ " 桌子状态State：" + gt.getState() + " 当前人数："
					+ gt.getPlayerNum());
			logger.error("【电子庄牛牛】本局开始时间：" + new Date(gt.getHandStartTime())
					+ " 当前操作人currentOpertaionPlayerIndex:"
					+ gt.getCurrentOpertaionPlayerIndex());
			for (User ply : gt.getPlayers()) {
				if (null != ply) {
					String cards = "";
					for (Byte card : ply.getCardsInHand()) {
						cards += card;
						cards += "-";
					}
					logger.error("【电子庄牛牛】玩家" + ply.getPlayerIndex() + " 座位号："
							+ ply.getTablePos() + " 手牌：" + cards);
				}
			}

			gt.clearGameState();
			gt.setState(GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE);
			gt.set_all_player_state(GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE);
			gt.setHandEndTime(System.currentTimeMillis());

			// 设为非法状态，系统会自动清理
			gt.setState(GameConstant.TABLE_STATE_INVALID);
		} else {
			logger.info("【电子庄牛牛】找不到桌子，不需要结束");
		}
	}

	/**
	 * 强制结束游戏，用于从后台结束非法桌子
	 */
	public void gameOverForceTable(String table_id) {
		logger.info("【电子庄牛牛】强制清理单个桌子");

		GameTable gt = null;
		for (Integer rid : gameRoomMap.keySet()) {
            GameRoom gr = gameRoomMap.get(rid);

            Map<String, GameTable> playingTables = gr.getPlayingTablesMap();
            
            for(Map.Entry<String, GameTable> entry : playingTables.entrySet()){
            	GameTable tt = entry.getValue();
            	if(tt.getVipTableID() != 0 && String.valueOf(tt.getVipTableID()).equals(table_id.trim())){
            		gt = tt;
            		break;
            	}
            }
        }

		if (gt != null) {
			logger.error("【电子庄牛牛】vip房间ID:" + gt.getVipTableID() + " 桌子ID：" + table_id
					+ " State：" + gt.getState() + " Backup_state:"
					+ gt.getBackup_state() + " 当前人数：" + gt.getPlayerNum());
			logger.error("【电子庄牛牛】本局开始时间：" + new Date(gt.getHandStartTime())
					+ " 当前操作人currentOpertaionPlayerIndex:"
					+ gt.getCurrentOpertaionPlayerIndex());
	    	VipRoomRecord vrr = null;
			for (User ply : gt.getPlayers()) {
				if (null != ply) {
					vrr = ply.getVrr();
					String cards = "";
					for (Byte card : ply.getCardsInHand()) {
						cards += card;
						cards += "-";
					}
					logger.error("【电子庄牛牛】玩家" + ply.getPlayerIndex() + "座位号："
							+ ply.getTablePos() + ",玩家状态：" + ply.getGameState()
							+ ",手牌：" + cards);
					 // 给玩家发送消息
	                PlayerOperationNotifyMsg msg = new PlayerOperationNotifyMsg();
	                msg.playerTablePos = ply.getTablePos();
	                msg.operationType= GameConstant.DDZ_OPT_NO_START_CLOSE_VIP;
//	                msg.forceClose = 1;
	                GameContext.gameSocket.send(ply.getSession(), msg);
				}
			}
			
			 if(vrr != null){
	             vrr.setEndWay("【电子庄牛牛】群主解散");
	             vrr.setEndTime(new Date());
	             playerService.endVipRoomRecord(vrr);
	        }

	        if(gt.getProxyCreator() != null){
	        	List<ProxyVipRoomRecord> pvrrs =gt.getProxyCreator().getPvrrs();
	        	if(pvrrs != null && pvrrs.size()>0){
	        		for(ProxyVipRoomRecord pvrr : pvrrs){
	        			if(pvrr.getRoomIndex() == gt.getVipTableID()){
	        				if(gt.isPayVipKa()){
		    					pvrr.setVipState(GameConstant.PROXY_FORCE_END_GAME_SERVER_START);
		    					pvrr.setEndWay("牌桌已扣卡，群主解散");
	        				}else{
		    					pvrr.setVipState(GameConstant.PROXY_FORCE_END_GAME_SERVER);
		    					pvrr.setEndWay("牌桌未扣卡，群主解散");
	        				}
	    					pvrr.setEndTime(new Date());
	    					playerService.updateProxyVipRoomRecord(pvrr);
	    					break;
	        			}
	        		}
	        	}
	        }
			gt.clearGameState();
			gt.setState(GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE);
			gt.set_all_player_state(GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE);
			gt.setHandEndTime(System.currentTimeMillis());

			// 设为非法状态，系统会自动清理
			gt.setState(GameConstant.TABLE_STATE_INVALID);
		} else {
			logger.info("【电子庄牛牛】找不到桌子，不需要结束");
		}
	}

	/**
	 * 游戏中玩家发送快捷聊天语句
	 */
	public void playerTalkingInGame(User pl, TalkingInGameMsg msg) {
		if (pl == null)
			return;

		GameRoom gr = this.getRoom(pl.getRoomID());
		if (gr == null)
			return;

		GameTable gt = gr.getTable(pl.getTableID(), pl.isPlayingSingleTable());
		if (gt == null)
			return;

		msg.playerSex = pl.getSex();

		// gt.doChit(msg);

		// 转发给桌子上其他人
		gt.sendMsgToTable(msg);
	}
	

	public void playerSendNNQZHuDongBiaoQing(User pl, HuDongBiaoQingMsgNNDZZ msg) {
		if (pl == null)
			return;

		GameRoom gr = this.getRoom(pl.getRoomID());
		if (gr == null)
			return;

		GameTable gt = gr.getTable(pl.getTableID(), pl.isPlayingSingleTable());
		if (gt == null)
			return;

		// 转发给桌子上其他人
		gt.sendMsgToTable(msg);
		
	}

	/**
	 * 获取正在进行游戏的桌子
	 */
	public List<GameTable> getPlayingTables(int index) {
		List<GameTable> tables = new ArrayList<GameTable>();

		for (Integer rid : gameRoomMap.keySet()) {
			GameRoom gr = gameRoomMap.get(rid);

			Map<String, GameTable> playingTables = gr.getPlayingTablesMap();

			for (String tid : playingTables.keySet()) {

				if (index > 0) {
					if (playingTables.get(tid).isHavePlayer(index)) {
						tables.add(playingTables.get(tid));
					}
				} else {
					tables.add(playingTables.get(tid));
				}
			}
		}
		return tables;
	}
	/**
	 * 开局准备
	 * 
	 * @param msg
	 * @param pl
	 */
	public void gameReday(GameReadyMsg msg, User pl) {
		GameRoom rm = gameRoomMap.get(pl.getRoomID());
		GameTable gt = rm.getTable(pl.getTableID(), false);

		// 通知其他玩家，当前玩家准备操作
		gt.sendMsgToTable(msg);

		// 判断如果当前牌桌所有人都已经准备，包括房主，此时游戏允许开始
		List<User> listPlayers = gt.getPlayers();
		int readyNum = 0;
		for (int i = 0; i < listPlayers.size(); i++) {
			if (listPlayers.get(i).getReadyFlag() == 1) {
				readyNum++;
			}
		}
		if (readyNum == gt.getMaxPlayer()
				&& gt.getState() == GameConstant.TABLE_STATE_WAITING_PLAYER) {
			gt.setState(GameConstant.TABLE_STATE_READY_GO);
			for (int i = 0; i < listPlayers.size(); i++) {
				listPlayers.get(i).setReadyFlag(0);// 回去初始准备状态
			}
		}
	}
	
	/**
	 * 开始游戏（房主发送准备消息）
	 * TableLogicProcess.java
	 * @param msg
	 * @param pl
	 * void
	 */
	public void gameStart(GameReadyMsg msg, User pl) {
		GameRoom rm = gameRoomMap.get(pl.getRoomID());
		GameTable gt = rm.getTable(pl.getTableID(), false);

		// 判断如果当前牌桌所有人都已经准备，包括房主，此时游戏允许开始
		List<User> listPlayers = gt.getPlayers();
		
		int minPlayer = 2;
		if(gt.getClubCode() > 0){
			minPlayer = 1;
		}
		if(listPlayers.size() < minPlayer){
			//房间不足1人，不能开局
			msg.state = 0;
			gt.sendMsgToTable(msg);
			return;
		}
		if (gt.getState() == GameConstant.TABLE_STATE_WAITING_PLAYER) {
			gt.setState(GameConstant.TABLE_STATE_READY_GO);
			for (int i = 0; i < listPlayers.size(); i++) {
				User plx = listPlayers.get(i);// 回去初始准备状态
				plx.setReadyFlag(0);
				
				plx.getUserGameHanderForNNDZZ().setTableRules(gt.getTableRule());
			}
		}
	}
	
	
	/***
	 * 换座：玩家进入牌桌之后准备之前，可以选择空闲的位置进行换座
	 *   2017.8.2
	 * 
	 * ****/
	public void change_site(GameSiteDownMsg msg, User pl) {
		
		GameRoom rm = gameRoomMap.get(pl.getRoomID());
		GameTable gt = rm.getTable(pl.getTableID(), false);
		
		// 如果此玩家已经准备，不能换座
		List<User> listPlayers = gt.getPlayers();
		Boolean isReady = true;
		int tablePlayerCount = listPlayers.size();
		for (int i = 0; i < tablePlayerCount; i++) {
			User pUser = listPlayers.get(i);
			if (pUser.getPlayerID() == pl.getPlayerID()){
				if (pUser.getReadyFlag()  ==1){
					isReady = false;
				}
			}
			
			if (pUser.getTablePos() == msg.player_site_new){
				//新座位有人，不能换坐
				isReady = false;
			}
		}
		
		Boolean isSite = gt.change_site(msg.player_site_old, msg.player_site_new , pl);
		if (!isSite || !isReady ){
			msg.opt_id = 0 ;//换坐操作失败
			GameContext.gameSocket.send(pl.getSession(), msg);
		}else{
			
			msg.opt_id = 1 ;//换坐操作成功
			gt.sendMsgToTable(msg);
		}
	}

	public GameTable getGameTable(String tableId) {
		GameRoom room = gameRoomMap.get(2004);

		GameTable table = room.getVipTableByVipTableID(Integer
				.parseInt(tableId));
		if (table == null) {
			room = gameRoomMap.get(2006);
			table = room.getVipTableByVipTableID(Integer.parseInt(tableId));
		}
		if (table == null) {
			room = gameRoomMap.get(2007);
			if(room != null){
				table = room.getVipTableByVipTableID(Integer.parseInt(tableId));
			}
		}
		return table;
	}
	
	public GameTable getGameTableByTableId(String tableId) {
		GameRoom room = gameRoomMap.get(2004);

		GameTable table = room.getTable(tableId,false);
		if (table == null) {
			room = gameRoomMap.get(2006);
			table = room.getTable(tableId,false);
		}
		if (table == null) {
			room = gameRoomMap.get(2007);
			if(room != null){
				table = room.getTable(tableId,false);
			}
		}
		return table;
	}
	
    /**
     * 代开强制解散VIP房间
     */
    public void proxyCloseVipTable(String playerID, String tableID) {
    	GameTable gt = null;
    	User pl = playerService.getPlayerByPlayerID(playerID);
    	List<ProxyVipRoomRecord> pvrrs = pl.getPvrrs();
    	if(pvrrs != null && pvrrs.size()>0){
    		for(ProxyVipRoomRecord pvrr : pvrrs){
    			if(pvrr.getRoomIndex() == Integer.parseInt(tableID)){
    				gt = this.getGameTable(tableID);
					pvrr.setVipState(GameConstant.PROXY_FORCE_END_GAME);
					playerService.updateProxyVipRoomRecord(pvrr);
					break;
    			}
    		}
    	}
    	
    	if(gt != null){
    		//给代理发刷新列表消息
			ProxyVipRoomRecordMsg prvm = new ProxyVipRoomRecordMsg();
			prvm.playerID = gt.getProxyCreator().getPlayerID();
			prvm.gameID = gt.getGameId();
			playerService.getProxyVipRoomRecord(prvm, gt.getProxyCreator().getSession());
	        // 结束VIP房间
        	gt.setProxyForceClose(true);
			logger.info("房间ID【" + gt.getVipTableID() + "】，代开房间，代理强制解散");
			gt.setTableEndWay("强制解散");
	        vipTableEnd(gt, pl);
    	}else{
    		return;
    	}
	}

	public GameConfig getGameConfig() {
		if(gameConfig == null){
			ICacheGameConfigService service = (ICacheGameConfigService) SpringService
			.getBean("gameConfigService");
			gameConfig = service.getGameConfig("nndzz");
		}
		return gameConfig;
	}
	
	/**
	 * 创建俱乐部模板房间
	 * @param pl
	 * @param psw
	 * @param quanNum
	 * @param roomID
	 * @param tableRule
	 * @param gameId
	 * @param isRecord
	 * @param isProxy
	 * @param clubCode
	 */
	public void createClubTemplateTable(User pl, String psw, int quanNum, int roomID,List<Integer> tableRule, String gameId, int userNum, int isRecord, int isProxy,int clubCode,int clubTemplateIndex,int quantityType,String roomLevel) {
		GameRoom rm = gameRoomMap.get(roomID);
		if (rm == null){
			return;
		}
		GameTable gt = rm.createVipTable(psw, pl, quanNum, tableRule, gameId, userNum, isProxy,clubCode, roomLevel);
		if (gt == null){
			return;
		}
        gt.setProxyState(isProxy);//记录开房模式
        gt.setTotalJuNum(quanNum);
		// TODO 是否判断俱乐部房卡，代定
		Date ct = DateService.getCurrentUtilDate();
		gt.setVipCreateTime(ct.getTime());
		gt.setIsRecordTable(isRecord);
		gt.setGameId(gameId);
		gt.setProxyState(isProxy);
		//设置俱乐部相关信息
		gt.setClubCode(clubCode);
		gt.setClubTableState(1);
		gt.setClubTemplateIndex(clubTemplateIndex);
		gt.setQuantityType(quantityType);
		gt.setRoomLevel(roomLevel);
		// 总圈数
		GameRoom gr = getRoom(gt.getRoomID());
		// 设置为未支付房卡
		gt.setPayVipKa(false);
		// 设置VIP房间带入金额
        if (gr != null) {
        	// 代理无限金币
			pl.setVipTableGold(GameContext.getConfigParamToInt("club_creator_gold", 2000));
			pl.setPlayerType(gr.roomType);
			playerService.updatePlayerType(pl.getPlayerID(), gr.roomType);
			logger.info("create_vip_table:" + gr.roomType);
		}
		add_table_to_tmgr(gt, gr);
        // TODO 代理开房处理
//        if(isProxy == 1){
        	gt.setProxyCreator(pl);					
			// 创建一条代开房间记录
			ProxyVipRoomRecord pvrr = new ProxyVipRoomRecord();
			pvrr.setProxyStartTime(ct);
			pvrr.setStartTime(ct);
			pvrr.setEndTime(ct);
			pvrr.setRoomID(gt.getTableID());
			pvrr.setRoomIndex(gt.getVipTableID());
			pvrr.setProxyID(pl.getPlayerID());
			pvrr.setProxyName(gt.getCreatorPlayerName());			
			pvrr.setRecordID(UUIDGenerator.generatorUUID());
			pvrr.setVipState(GameConstant.PROXY_NO_START_GAME);
			pvrr.setRoomType(roomID);
			pvrr.setGameId(gameId);
			pvrr.setTableRule("");
			pvrr.setClubCode(gt.getClubCode());// 2018-9-11
			playerService.createProxyVipRoomRecord(pvrr);
			//
			if(pl.getPvrrs() != null){
				pl.getPvrrs().add(pvrr);
			}
// 		}else{
//			pl.setOnTable(true);
// 		}
		logger.debug("vip table id=" + gt.getVipTableID());
		logger.info("==>创建俱乐部固定房，俱乐部编号：【"+clubCode+"】，TableID：【" + gt.getVipTableID() + "】，俱乐部拥有者ID：【"+ pl.getPlayerIndex() + "】，俱乐部拥有者昵称：【" + pl.getPlayerName()+ "】，所选规则：【" + tableRule + "】");

	}
	/**
	 * @description 分组获取俱乐部中的牌桌
	 * /
	 * @param clubNo 俱乐部编号
	 * @return
	 */
	public Map<String,List<GameTable>> getClubGameTables(int clubNo){
		Map<String, List<GameTable>> rs = new HashMap<String, List<GameTable>>();
		List<GameTable> staticList = new ArrayList<GameTable>();
		List<GameTable> userList = new ArrayList<GameTable>();
		List<GameTable> playingList = new ArrayList<GameTable>();
		for(Integer key : this.gameRoomMap.keySet()){
			GameRoom gr = this.gameRoomMap.get(key);
			for (String tableId : gr.getPlayingTablesMap().keySet()){
				GameTable gt = gr.getPlayingTablesMap().get(tableId);
				if(gt.getState() == GameConstant.TABLE_STATE_INVALID){
					continue;
				}
				if(gt.getClubCode() == clubNo) {
					if(gt.getClubTableState() == 1){ //固定空桌
						staticList.add(gt);
					}else if(gt.getClubTableState() == 2 || gt.getClubTableState() == 4 ) { //用户在俱乐部创建的私密牌桌
						userList.add(gt);
					}else if(gt.getClubCode() != 0){
						playingList.add(gt);
					}
				}
			}
		}
		rs.put(GameConstant.STATIC_CLUB_TABLE, staticList);
		rs.put(GameConstant.USER_CLUB_TABLE, userList);
		rs.put(GameConstant.STATIC_CLUB_PLAYING_TABLE, playingList);
		return rs;
	}
    
}
