package com.chess.nndzz.table;

import com.chess.nndzz.msg.struct.other.PlayerTableOperationMsg;
import com.chess.common.bean.User;

/**
 */
public class TableMessage {
    int operation_id = 0;
    int table_id = 0;
    PlayerTableOperationMsg msg = null;
    User pl = null;
    GameRoom gr = null;
    GameTable gt = null;
}
