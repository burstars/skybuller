package com.chess.nndzz.table;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.chess.common.DateService;
import com.chess.common.SpringService;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;
import com.chess.common.bean.User;
import com.chess.common.constant.GameConstant;
import com.chess.common.framework.GameContext;
import com.chess.common.service.ISystemConfigService;

public class GameRoom extends NetObject {

	public int cfgId = 0;
	public int roomID;
	public int price;// 多少金币的场
	public int minGold = 0;// 最低携带量
	public int maxGold = 0;// 最高携带量
	public int roomType = 0;// 1初级场，2中级，3高级，4vip，5单机, 6:2人
	//
	public int serviceFee = 0;// 每一局收台费多少
	//
	private GameTable workingTable = null;

	private ISystemConfigService cfgService = null;

	// 所有的GameTable只在这个room类里面存储引用，其他地方没有引用，这样创建删除查找不会出错
	// 桌子map，一张桌子2-4名玩家，key 为tableid
	private Map<String, GameTable> playingTablesMap = new ConcurrentHashMap<String, GameTable>();

	//
	// 玩家在这个场子里面的人数
	private int base_num = 0;
	private int playerNum = 0;
	// vip桌子创建有个id，一直增加
	private int vipTableID = 100;

	// VIP房间固定带入金币
	public int fixedGold = 0;

	//
	public GameRoom() {

	}

	@Override
	public void serialize(ObjSerializer ar) {
		roomID = ar.sInt(roomID);
		price = ar.sInt(price);
		minGold = ar.sInt(minGold);
		maxGold = ar.sInt(maxGold);
		roomType = ar.sInt(roomType);
		playerNum = ar.sInt(playerNum);
		fixedGold = ar.sInt(fixedGold);
	}

	//

	//
	public GameTable getTable(String tableID, boolean isSinglePlayer) {
		if (tableID == null || tableID.length() < 1)
			return null;
		//

		//
		GameTable gt = null;
		gt = playingTablesMap.get(tableID);

		return gt;
	}

	public GameTable getVipTableByVipTableID(int vipTableID) {
		for (String tid : playingTablesMap.keySet()) {
			GameTable gt = playingTablesMap.get(tid);
			if (gt.getVipTableID() == vipTableID)
				return gt;
		}
		//
		return null;
	}

	// STiV modify add param tableRule
	public GameTable createVipTable(String psw, User pl, int quanNum,
			List<Integer> tableRule, String gameId,int userNum, int isProxy, int clubCode, String roomLevel) {
		GameTable newGT = GameContext.tableLogic_nndzz.popFreeTable(roomType); // kn add parameter
		// 设置座位
		newGT.setPlayerCount(userNum);
		newGT.resetFreePos();
		//
		playingTablesMap.put(newGT.getTableID(), newGT);
		//
		newGT.setState(GameConstant.TABLE_STATE_WAITING_PLAYER);

		int max = 899999;
		int min = 100000;
		Random random = new Random();
		
		int s = random.nextInt(max) + min;

		vipTableID = s;
		while (this.getVipTableByVipTableID(vipTableID) != null){
			vipTableID = random.nextInt(max) + min;			
		}
		
		newGT.setVipTableID(vipTableID);
		newGT.setVipPswMd5(psw);
		newGT.setCreatorPlayerID(pl.getPlayerID());
		newGT.setCreatorPlayerName(pl.getPlayerName());
		//
		newGT.setRoomID(roomID);
		newGT.setProxyState(isProxy);
		newGT.setRoomModel(quanNum);
		newGT.setClubCode(clubCode);
		newGT.setRoomLevel(roomLevel);
		//代开优化
		if(newGT.getProxyState() == 1){
			newGT.setProxyCreator(pl);
		}else{
			newGT.enterTable(pl);
			//
			pl.setVipTableID(String.valueOf(vipTableID));//(newGT.getTableID());
			pl.setRoomID(roomID);
			pl.setTableID(newGT.getTableID());
		}
		// STiV modify
		newGT.setTableRuleOption(tableRule);

		//
		return newGT;
	}

	// 玩家进入桌子,多人桌
	public GameTable enterRoom(User pl) {
		GameTable gt = null;

		// 是否单机场，普通玩家只进一个，需要创建新桌
		boolean single_player_new_table = false;
		if (pl.isRobot() == false
				&& this.roomType == GameConstant.ROOM_TYPE_SINGLE) {
			if (workingTable != null && workingTable.getPlayerNum() > 0)
				single_player_new_table = true;
		} else {
			// 看看以前的老桌子是否有空闲
			// playingTablesMap.entrySet()
			for (Entry<String, GameTable> set : playingTablesMap.entrySet()) {
				GameTable temGt = set.getValue();
				if (!temGt.isFull()) {
					if ((temGt.getState() != GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE && temGt
							.getState() != GameConstant.TABLE_STATE_WAITING_PLAYER)) {
						break;
					}
					workingTable = temGt;
					break;
				}
			}
		}

		// 单机场只进一个普通玩家，其他都是机器人
		if (workingTable == null || single_player_new_table) {
			workingTable = GameContext.tableLogic_nndzz.popFreeTable(roomType); // kn add parameter
			playingTablesMap.put(workingTable.getTableID(), workingTable);
			workingTable.setState(GameConstant.TABLE_STATE_WAITING_PLAYER);
		} else {
			if (workingTable.findPlayerIndex(pl) >= 0) {
				workingTable.removePlayer(pl.getPlayerID());
				workingTable.addFreePos(pl.getTablePos());
			}
		}

		// 先清除
		pl.clear_game_state();
		//
		workingTable.setDizhu(this.price);
		//
		workingTable.enterTable(pl);
		//workingTable.setRoomType(roomType); // kn del
		//
		//
		gt = workingTable;

		//

		gt.setRoomID(this.roomID);
		pl.setTableID(gt.getTableID());
		pl.setRoomID(roomID);
		
		//
		pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_READY);
		// pl.setGameState(GameConstant.USER_GAME_STATE_IN_TABLE_GAME_OVER_WAITING_TO_CONTINUE);

		// 游戏房间人数加1
		this.setPlayerNum(this.getPlayerNum() + 1);

		//
		if (workingTable.isFull()) {
			long ct = DateService.getCurrentUtilDate().getTime();
			workingTable.setReadyTime(ct);
			if (gt.getState() != GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE) {
				workingTable.setState(GameConstant.TABLE_STATE_READY_GO);
				if (this.roomType != GameConstant.ROOM_TYPE_VIP && this.roomType != GameConstant.ROOM_TYPE_COUPLE) {
					// 第一局随机庄家
					Random rand = new Random();
					if (GameTable.disable_test || GameTable.test_cards == null){
						workingTable.setDealerPos(gt.getPlayers().get(rand.nextInt(workingTable.getPlayers().size())).getTablePos());
					}else{
						workingTable.setDealerPos(GameTable.test_cards_dealer);
					}
				}

			}

			// 清理掉当前的工作桌
			workingTable = null;

		} else {
			if (gt.getState() != GameConstant.TABLE_STATE_WAITING_PALYER_TO_CONTINUE) {
				workingTable.setState(GameConstant.TABLE_STATE_WAITING_PLAYER);
			}
		}
		//
		return gt;
	}

	// 在回收前看看是不是当前正在工作的桌子
	public void beforeRecycle(GameTable gt) {
		if ((workingTable != null)
				&& (workingTable.getTableID().equals(gt.getTableID()))) {
			workingTable = null;
		}
	}

	//
	public Map<String, GameTable> getPlayingTablesMap() {
		return playingTablesMap;
	}

	//
	public void setPlayingTablesMap(Map<String, GameTable> playingTablesMap) {
		this.playingTablesMap = playingTablesMap;
	}

	//
	public int getPlayerNum() {
		if (cfgService == null)
			cfgService = (ISystemConfigService) SpringService.getBean("sysConfigService");
		if (playerNum == 0) {
			base_num = 51 / (roomID + 1) + (int) (Math.random() * 37);
			playerNum = base_num * 13;
		}
		//1初级场，2中级，3高级，4vip，5单机
		int num = 0;
		switch (roomType) {
			case 1:
				num = cfgService.get_room_of_primary_player_num();
				/*
				if(playerNum < 31560) {
					playerNum = playerNum + 31560;
				}
				*/
				if(playerNum < num) {
					playerNum = playerNum + num;
				} else {
					base_num = 51 / (roomID + 1) + (int) (Math.random() * 37);
					playerNum = base_num * 13;
					playerNum = playerNum + num;
				}
				break;
			case 2:
				num = cfgService.get_room_of_intermediate_player_num();
				if(playerNum < num) {
					playerNum = playerNum + num;
				} else {
					base_num = 51 / (roomID + 1) + (int) (Math.random() * 37);
					playerNum = base_num * 13;
					playerNum = playerNum + num;
				}
				/*
				if(playerNum < 10950) {
					playerNum = playerNum + 10950;
				}
				*/
				break;
			case 3:
				num = cfgService.get_room_of_advanced_player_num();
				/*
				if(playerNum < 5250) {
					playerNum = playerNum + 5250;
				}
				*/
				if(playerNum < num) {
					playerNum = playerNum + num;
				} else {
					base_num = 51 / (roomID + 1) + (int) (Math.random() * 37);
					playerNum = base_num * 13;
					playerNum = playerNum + num;
				}
				break;
		}

		return playerNum;
	}

	public void setPlayerNum(int playerNum) {
		if (playerNum == 0) {
			base_num = 51 / (roomID + 1) + (int) (Math.random() * 37);
			playerNum = base_num * 13;
		}
		//
		if (playerNum >= 0)
			this.playerNum = playerNum;
		//
		if (playerNum < this.base_num)
			playerNum = base_num;
	}

}
