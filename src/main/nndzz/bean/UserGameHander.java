package com.chess.nndzz.bean;

import java.util.ArrayList;
import java.util.List;

import com.chess.common.bean.User;
import com.chess.common.constant.GameConstant;
import com.chess.nndzz.util.CardUtil;

/**
 * 玩家游戏缓存控制器
 * 
 * 
 */
public class UserGameHander {
	/** 平倍押注 */
	private int pingBeiCoin = -1;
	/** 翻倍押注 */
	private int fanBeiCoin = -1;
	/** 是否选择庄*/
	private boolean isChooseZhuang = false;
	/** 是否亮牛了 */
	private boolean isLiangNiu = false;
	/** 是否赢了 */
	private boolean isWin = false;
	/** 平倍押注得分 */
	private int pingBeiScore = 0;
	/** 翻倍押注得分 */
	private int fanBeiScore = 0;
	/** 对应的是哪一个玩家 */
	private User player;
	/** 最大牌型 */
	private int maxCardShape = 1;
	/** 牌型等级 */
	private int maxCardShapeLevel = 0;
	/** 单局最高分 */
	private int maxCoinNum = -100000;
	/** 赢牌次数 */
	private int winNum = 0;
	/** 输牌次数 */
	private int loseNum = 0;
	/** 玩法规则 */
	private List<Integer> tableRules = new ArrayList<Integer>();

	public UserGameHander(User user) {
		this.player = user;
	}

	public void clear_game_state() {
		pingBeiCoin = -1;
		fanBeiCoin = -1;
		pingBeiScore = 0;
		fanBeiScore = 0;
		isChooseZhuang = false;
		isLiangNiu = false;
		isWin = false;
		pingBeiScore = 0;
		fanBeiScore = 0;
		maxCardShape = 1;
		maxCardShapeLevel = 0;
		maxCoinNum = 0;
		winNum = 0;
		loseNum = 0;
	}

	/**
	 * 进入下一局
	 */
	public void playerContinue() {
		pingBeiCoin = -1;
		fanBeiCoin = -1;
		pingBeiScore = 0;
		fanBeiScore = 0;
		isChooseZhuang = false;
		isLiangNiu = false;
		isWin = false;
	}

	/**
	 * 获取牌型
	 * 
	 * @return int
	 */
	public int getCardShape() {
		return CardUtil.getCardShape(this.player.getCardsInHand());
	}

	public boolean isWin() {
		return isWin;
	}

	public void setWin(boolean isWin) {
		this.isWin = isWin;
	}

	public User getPlayer() {
		return player;
	}

	public void setPlayer(User player) {
		this.player = player;
	}

	public void setWinNum(int winNum) {
		this.winNum = winNum;
	}

	public int getWinNum() {
		return winNum;
	}

	public int getMaxCardShape() {
		return maxCardShape;
	}

	public void setMaxCardShape(int maxCardShape) {
		this.maxCardShape = maxCardShape;
	}

	public int getMaxCoinNum() {
		return maxCoinNum;
	}

	public void setMaxCoinNum(int maxCoinNum) {
		this.maxCoinNum = maxCoinNum;
	}

	public List<Integer> getTableRules() {
		return tableRules;
	}

	public void setTableRules(List<Integer> tableRules) {
		this.tableRules = tableRules;
	}

	public int getLoseNum() {
		return loseNum;
	}

	public void setLoseNum(int loseNum) {
		this.loseNum = loseNum;
	}

	public int getMaxCardShapeLevel() {
		return maxCardShapeLevel;
	}

	public void setMaxCardShapeLevel(int maxCardShapeLevel) {
		this.maxCardShapeLevel = maxCardShapeLevel;
	}

	public int getPingBeiCoin() {
		return pingBeiCoin;
	}

	public void setPingBeiCoin(int pingBeiCoin) {
		this.pingBeiCoin = pingBeiCoin;
	}

	public int getFanBeiCoin() {
		return fanBeiCoin;
	}

	public void setFanBeiCoin(int fanBeiCoin) {
		this.fanBeiCoin = fanBeiCoin;
	}

	public boolean isStakeCoin() {
		return this.pingBeiCoin != -1 || this.fanBeiCoin != -1;
	}

	public boolean isLiangNiu() {
		return isLiangNiu;
	}

	public void setLiangNiu(boolean isLiangNiu) {
		this.isLiangNiu = isLiangNiu;
	}

	public int getPingBeiScore() {
		return pingBeiScore;
	}

	public void setPingBeiScore(int pingBeiScore) {
		this.pingBeiScore = pingBeiScore;
	}

	public int getFanBeiScore() {
		return fanBeiScore;
	}

	public void setFanBeiScore(int fanBeiScore) {
		this.fanBeiScore = fanBeiScore;
	}

	public boolean isChooseZhuang() {
		return isChooseZhuang;
	}

	public void setChooseZhuang(boolean isChooseZhuang) {
		this.isChooseZhuang = isChooseZhuang;
	}

	
}
