/**
 * Description:VIP战绩的每锅记录
 */
package com.chess.nndzz.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.chess.common.DateService;
import com.chess.common.StringUtil;
import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class VipRoomRecord extends NetObject {

	private String recordID="";
	/** 房间ID */
	private String roomID="";
	/** 房间号 */
	private int roomIndex=0;
	/** 玩家ID */
	private String player1ID="";
	/** 玩家总分 */
	private int score1=0;
	/** 下家玩家ID */
	private String player2Name="";
	/** 下家总分 */
	private int score2=0;
	/** 对家玩家ID */
	private String player3Name="";
	/** 对家总分 */
	private int score3=0;
	/** 上家玩家ID */
	private String player4Name="";
	/** 上家总分 */
	private int score4=0;
	/** 房主ID */
	private String hostName="";
	/** 房间开始时间 */
	private Date startTime;
	/** 房间结束时间 */
	private Date endTime;
	
	private String start;
	
	private String end;
	/** 结束方式 */
	private String endWay="";
	/** 表名后缀 */
	private String tableNameSuffix = DateService.getTableSuffixByType("", DateService.DATE_BY_DAY);
	/** 表名前缀 gameId*/
	private String tableNamePrefix =  "";

	@Override
	public void serialize(ObjSerializer ar) {
		// TODO Auto-generated method stub
		super.serialize(ar);
		roomID = ar.sString(roomID);
		roomIndex = ar.sInt(roomIndex);
		player1ID = ar.sString(player1ID);
		score1 = ar.sInt(score1);
		player2Name = ar.sString(player2Name);
		score2 = ar.sInt(score2);
		player3Name = ar.sString(player3Name);
		score3 = ar.sInt(score3);
		player4Name = ar.sString(player4Name);
		score4 = ar.sInt(score4);
		hostName = ar.sString(hostName);
		ar.sString(StringUtil.date2String(startTime));
		ar.sString(StringUtil.date2String(endTime));
		endWay =  ar.sString(endWay);
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public int getRoomIndex() {
		return roomIndex;
	}

	public void setRoomIndex(int roomIndex) {
		this.roomIndex = roomIndex;
	}

	public String getPlayer1ID() {
		return player1ID;
	}

	public void setPlayer1ID(String player1ID) {
		this.player1ID = player1ID;
	}

	public long getScore1() {
		return score1;
	}

	public void setScore1(int score1) {
		this.score1 = score1;
	}

	public String getPlayer2Name() {
		return player2Name;
	}

	public void setPlayer2Name(String player2Name) {
		this.player2Name = player2Name;
	}

	public long getScore2() {
		return score2;
	}

	public void setScore2(int score2) {
		this.score2 = score2;
	}

	public String getPlayer3Name() {
		return player3Name;
	}

	public void setPlayer3Name(String player3Name) {
		this.player3Name = player3Name;
	}

	public long getScore3() {
		return score3;
	}

	public void setScore3(int score3) {
		this.score3 = score3;
	}

	public String getPlayer4Name() {
		return player4Name;
	}

	public void setPlayer4Name(String player4Name) {
		this.player4Name = player4Name;
	}

	public long getScore4() {
		return score4;
	}

	public void setScore4(int score4) {
		this.score4 = score4;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
		this.start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTime);
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
		this.end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endTime);
	}

	public String getRecordID() {
		return recordID;
	}

	public void setRecordID(String recordID) {
		this.recordID = recordID;
	}

	public String getTableNameSuffix() {
		return tableNameSuffix;
	}

	public void setTableNameSuffix(String tableNameSuffix) {
		this.tableNameSuffix = tableNameSuffix;
	}
	
	public String getTableNamePrefix() {
		return tableNamePrefix;
	}

	public void setTableNamePrefix(String tableNamePrefix) {
		this.tableNamePrefix = tableNamePrefix;
	}
	
	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}
	
	public String getEndWay() {
		return endWay;
	}

	public void setEndWay(String endWay) {
		this.endWay = endWay;
	}

}
