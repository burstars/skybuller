package com.chess.nndzz.bean;

import java.util.ArrayList;
import java.util.List;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

public class GameMultiple extends NetObject{

	// 底分
	public int basePoint = 1;
	// 叫分
	public int callScore = 0;
	// 炸弹个数
	public int bombCount = 0;
	// 春天
	public int springMultiple = 1;
	// 加倍（tablePos:value,） value:1、加倍	0、不加倍
	public List<Integer> doubleMultiples = new ArrayList<Integer>();
	
	
	public GameMultiple(){
		for(int i=0; i<3; i++){
			doubleMultiples.add(0);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void serialize(ObjSerializer ar)
	{
		basePoint = ar.sInt(basePoint);
		
		callScore = ar.sInt(callScore);
		
		bombCount = ar.sInt(bombCount);
		
		springMultiple = ar.sInt(springMultiple);
		
		doubleMultiples = (List<Integer>) ar.sIntArray(doubleMultiples);
	}

	public int getPublicMultiple(){
		int bombMultiple = getBombMultiple();
		
		return bombMultiple * springMultiple * callScore;
		
	}
	
	public void addBombCount(){
		this.bombCount++;
	}
	
	public int getBombMultiple(){
		Double dBombMultiple = Math.pow(2, bombCount);
		return dBombMultiple.intValue();
	}
	
}
