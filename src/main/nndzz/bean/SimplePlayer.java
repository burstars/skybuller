package com.chess.nndzz.bean;

import com.chess.core.net.msg.NetObject;
import com.chess.core.net.msg.ObjSerializer;

//对战发给客户端的
public class SimplePlayer  extends NetObject {

	/**玩家ID*/
	public String playerID="";
	/**索引号,玩家编号*/
	public int palyerIndex=0;
	/**玩家游戏中昵称**/
	public String playerName="";
	/**头像索引**/
	public int headImg=0;
	/** 玩家货币0 金币*/
	public int gold=0;
	/**性别*/
	public int sex=0;
	/**总输赢得分*/
	public int winLosePoint=0;
	/**本局输赢得分*/
	public int onceWinLosePoint=0;
	/**玩家位置*/
	public int tablePos=0;
	
	public String desc="";
	
	//牌局结局
	public int gameResult=0;
	//是否可以加为好友
	public int canFriend=0;
	/**是否坐在桌子上，1：在桌上；0：在大厅或者离线*/
	public int inTable=0;
	/**玩家IP*/
	public String ip = "";
	/** 开局准备状态 20170801 */
	public int readyFlag = 0;
	public String location = "";
	public String headImgUrl = "";
	
	public int card = 0;
	/** 平倍得分 */
	public int pingBeiScore = 0;
	/** 翻倍得分 */
	public int fanBeiScore = 0;
	/** 最大牌型 */
	public int maxCardShape = 1;
	public int maxCardShapeLevel = 0;
	/** 单局最高分 */
	public int maxCoinNum = 0;
	/** 赢牌次数 */
	public int winNum = 0;
	/** 输牌次数 */
	public int loseNum = 0;
	/** 玩家是否赢 **/
	public int isWin = 0;
	/** 本局玩家打喜分数 **/
	public int specialCoinNum = 0 ;
	/** 玩家总分 */
	public int winLoseTotalCoinNum = 0;
	
	public SimplePlayer()
	{}
 
	@Override
	public void serialize(ObjSerializer ar)
	{
		playerID = ar.sString(playerID);
		palyerIndex = ar.sInt(palyerIndex);
		playerName = ar.sString(playerName);
		headImg = ar.sInt(headImg);
		sex = ar.sInt(sex);
		gold = ar.sInt(gold);
		winLosePoint=ar.sInt(winLosePoint);
		onceWinLosePoint=ar.sInt(onceWinLosePoint);
		
		tablePos=ar.sInt(tablePos);
		desc = ar.sString(desc);
		gameResult = ar.sInt(gameResult);
		canFriend = ar.sInt(canFriend);
		inTable = ar.sInt(inTable);
		
		ip = ar.sString(ip);
		readyFlag = ar.sInt(readyFlag);
		location = ar.sString(location);
		headImgUrl = ar.sString(headImgUrl);
		
		card = ar.sInt(card);
		pingBeiScore = ar.sInt(pingBeiScore);
		fanBeiScore = ar.sInt(fanBeiScore);
		maxCardShape = ar.sInt(maxCardShape);
		maxCoinNum = ar.sInt(maxCoinNum);
		winNum = ar.sInt(winNum);
		loseNum = ar.sInt(loseNum);
		isWin = ar.sInt(isWin);
		specialCoinNum = ar.sInt(specialCoinNum);
		winLoseTotalCoinNum = ar.sInt(winLoseTotalCoinNum);
		
		maxCardShapeLevel = ar.sInt(maxCardShapeLevel);
	}
}